<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'six-clouds-wp-live' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'sixclouds8899' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'D.po@Eqq);qsC]BCR;::$eZ b?M)6U0&M32~//5*vZN-B5aSsu=Ghan<MoUn}z8:' );
define( 'SECURE_AUTH_KEY',  '2-.e+yK7+fz@:2ZvJrKi_;AZOHq(A*34`Qp|}wArIdiVSC7%:r1nz1VDqCYMD;~2' );
define( 'LOGGED_IN_KEY',    '-J{q;V=}!t,1w*_Mj&;L7U.u)wG=T-yX.*&oHDYQt=:=SAtd,t4h`:5p:hpK!BBW' );
define( 'NONCE_KEY',        'dYC#f,X=_6 >d6g/[,F3KF>R/:0PMU(Mb{0gUKT;$dOlXjx0|2FUxJp$gbe)6@:v' );
define( 'AUTH_SALT',        's5VTa0f?+Ng6u}IAhkC-jFw7O>gzi~B5m_e`t~BR8g4</47W9be8jAF4PfZ,,j!a' );
define( 'SECURE_AUTH_SALT', 'N<0]]L/fVnd6lBhZS1O3dz`J>K%L)JU1Vt;F:ASs}ju+lgtWLlkjb#!VW[e2w8&b' );
define( 'LOGGED_IN_SALT',   'S9xgxTOb{D<|7:)5=Vh.!~1`&$9Bk8%m?QBCJ x{GN#~l2oKnO0!EegKx2i,<Y9E' );
define( 'NONCE_SALT',       'RQY^Ax?H)WiM=Fw)vKGh|36VDd|%ZtHk98uH?{+OrK:57rg6k@ji(*V]a>gMp)Xw' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_2jcbg_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );
//define('FS_METHOD', 'direct');
define( 'WP_DEBUG_LOG', true );
define( 'WP_DEBUG_DISPLAY', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

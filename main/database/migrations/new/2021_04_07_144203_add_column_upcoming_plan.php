<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnUpcomingPlan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mp_payments', function(Blueprint $table) {
            $table->integer('upcoming_plan')->default(0)->comment('0: default, 1: upcoming plan')->after('renewal_id');
            $table->dateTime('upcoming_plan_date')->nullable()->after('upcoming_plan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mp_payments', function(Blueprint $table) {
            $table->dropColumn('upcoming_plan');
            $table->dropColumn('upcoming_plan_date');    
        });
    }
}

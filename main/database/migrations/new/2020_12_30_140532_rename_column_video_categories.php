<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnVideoCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('video_categories', function(Blueprint $table) {
            $table->renameColumn('category_price_12month', 'category_price');
            $table->renameColumn('category_price_sgd_12month', 'category_price_sgd');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('video_categories', function(Blueprint $table) {
            $table->renameColumn('category_price', 'category_price_12month');
            $table->renameColumn('category_price_sgd', 'category_price_sgd_12month');     
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnJobManage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job_manage', function(Blueprint $table) {
            $table->integer('status')->nullable()->comment('0:disable 1:enable')->after('user_id');
        });

        Schema::table('store_email', function(Blueprint $table) {
            $table->integer('status')->nullable()->comment('0:disable 1:enable')->after('job_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('job_manage', function(Blueprint $table) {
            $table->dropColumn('status');    
        });

        Schema::table('store_email', function(Blueprint $table) {
            $table->dropColumn('status');    
        });
    }
}

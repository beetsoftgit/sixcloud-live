<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnMppayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mp_payments', function(Blueprint $table) {
            $table->integer('is_cancelsubscription')->default(0)->comment('0: default, 1: cancel')->after('plan_duration');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mp_payments', function(Blueprint $table) {
            $table->dropColumn('is_cancelsubscription');    
        });
    }
}

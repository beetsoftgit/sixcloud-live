<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnMppayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mp_payments', function(Blueprint $table) {
            $table->integer('plan_status')->default(0)->comment('0: Old, 1: New')->after('slug');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mp_payments', function(Blueprint $table) {
            $table->dropColumn('plan_status');    
        });
    }
}

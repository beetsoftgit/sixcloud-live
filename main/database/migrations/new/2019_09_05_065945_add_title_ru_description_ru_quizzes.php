<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTitleRuDescriptionRuQuizzes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quizzes', function(Blueprint $table)
        {
            $table->string('title_ru', 255)->nullable()->after('description_chi');
            $table->text('description_ru')->nullable()->after('title_ru');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quizzes', function(Blueprint $table)
        {
            $table->dropColumn('title_ru');
            $table->dropColumn('description_ru');
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQuizStartStopDatetimeToQuizAttempts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quiz_attempts', function(Blueprint $table)
        {
            $table->dateTime('start_time')->nullable()->after('is_correct');
            $table->dateTime('stop_time')->nullable()->after('start_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quiz_attempts', function(Blueprint $table)
        {
            $table->dropColumn('start_time');
            $table->dropColumn('stop_time');
        });
    }
}

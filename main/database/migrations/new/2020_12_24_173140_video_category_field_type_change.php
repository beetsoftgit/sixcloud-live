<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VideoCategoryFieldTypeChange extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('video_categories', function(Blueprint $table) {
            $table->float('category_price_3month', 10, 2)->default(0.00)->after('subcategory_description_ru');
            $table->float('category_price_sgd_3month', 10, 2)->default(0.00)->after('category_price_3month');
            $table->float('category_price_6month', 10, 2)->default(0.00)->after('category_price_sgd_3month');
            $table->float('category_price_sgd_6month', 10, 2)->default(0.00)->after('category_price_6month');
            $table->renameColumn('category_price', 'category_price_12month');
            $table->renameColumn('category_price_sgd', 'category_price_sgd_12month');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('video_categories', function(Blueprint $table) {
            $table->dropColumn('category_price_3month');
            $table->dropColumn('category_price_sgd_3month');
            $table->dropColumn('category_price_6month');
            $table->dropColumn('category_price_sgd_6month');
            $table->renameColumn('category_price_12month', 'category_price');
            $table->renameColumn('category_price_sgd_12month', 'category_price_sgd');     
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideocategoryhistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_categories_history', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('category_id', 255)->nullable();
            $table->string('category_name', 255)->nullable();
            $table->integer('category_price')->nullable();
            $table->integer('category_sgd_price')->nullable();
            $table->integer('category_price_3month')->nullable();
            $table->integer('category_sgd_price_3month')->nullable();
            $table->integer('category_price_6month')->nullable();
            $table->integer('category_sgd_price_6month')->nullable();
            $table->integer('version')->nullable()->comment('Version are related video category pricing plan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_categories_history');
    }
}

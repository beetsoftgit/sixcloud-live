<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMultipleStoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('multiple_store', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('name', 255)->nullable();
            $table->string('name_chi', 255)->nullable();
            $table->string('name_ru', 255)->nullable();
            $table->string('version', 15)->nullable();
            $table->tinyInteger('is_hard_update')->nullable()->comment = "0:update can be ignored; 1: soft update(good for user if updates); 2: force update(App won't work without update)";
            $table->text('url')->nullable();
            $table->tinyInteger('isSixCloudsServer')->nullable()->comment = "0:false; 1:true";
            $table->text('icon')->nullable();
            $table->tinyInteger('is_live')->nullable()->comment = "0:not live; 1: live;";
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('multiple_store');
    }
}

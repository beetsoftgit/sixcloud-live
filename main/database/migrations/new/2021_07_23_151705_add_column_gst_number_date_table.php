<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnGstNumberDateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tax_rate', function(Blueprint $table) {
            $table->string('gst_number')->nullable()->after('tax_rate');
            $table->integer('admin_id')->nullable()->after('gst_number');
            $table->dateTime('applicable_from')->nullable()->after('admin_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tax_rate', function(Blueprint $table) {
            $table->dropColumn('gst_number');
            $table->dropColumn('admin_id');
            $table->dropColumn('applicable_from');    
        });
    }
}

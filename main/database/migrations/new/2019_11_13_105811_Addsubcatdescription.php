<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Addsubcatdescription extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('video_categories', function(Blueprint $table)
        {
            $table->text('subcategory_description')->nullable()->after('description_ru');
            $table->text('subcategory_description_chi')->nullable()->after('subcategory_description');
            $table->text('subcategory_description_ru')->nullable()->after('subcategory_description_chi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('video_categories', function(Blueprint $table)
        {
            $table->dropColumn('subcategory_description');
            $table->dropColumn('subcategory_description_chi');
            $table->dropColumn('subcategory_description_ru');
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubcategoryNameForChiRu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('video_categories', function(Blueprint $table)
        {
            $table->text('subcategory_name_chi')->nullable()->after('subcategory_name');
            $table->text('subcategory_name_ru')->nullable()->after('subcategory_name_chi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('video_categories', function(Blueprint $table)
        {
            $table->dropColumn('subcategory_name_chi');
            $table->dropColumn('subcategory_name_ru');
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTaxRateCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('video_categories', function(Blueprint $table) {
            $table->integer('tax_rate')->nullable()->after('category_price_sgd');
        });

        Schema::table('video_categories_history', function(Blueprint $table) {
            $table->integer('tax_rate')->nullable()->after('category_sgd_price_6month');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('video_categories', function(Blueprint $table) {
            $table->dropColumn('tax_rate');    
        });

        Schema::table('video_categories_history', function(Blueprint $table) {
            $table->dropColumn('tax_rate');    
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MppaymentAddColomnPlanDuration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mp_payments', function(Blueprint $table) {
            $table->integer('plan_duration')->default(12)->comment('The plan duration is determined by month')->after('payment_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mp_payments', function(Blueprint $table) {
            $table->dropColumn('plan_duration');     
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOnLandingOnDetailField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('video_categories', function(Blueprint $table)
        {
            $table->tinyInteger('on_landing_page')->default('0')->comment("0:Not Display on landing page; 1:Display on landing page")->after('sub_category_icon');
            $table->tinyInteger('on_detail_page')->default('0')->comment("0:Not Display on details page; 1:Display on detail page")->after('on_landing_page');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('video_categories', function(Blueprint $table)
        {
            $table->dropColumn('on_landing_page');
            $table->dropColumn('on_detail_page');
        });
    }
}

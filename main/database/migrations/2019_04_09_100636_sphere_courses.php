<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SphereCourses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sphere_courses', function (Blueprint $table) {
            $table->unsignedInteger('id', true);
            $table->string('course_name');
            $table->integer('grade_id');
            $table->float('course_price');
            $table->integer('total_sesions');
            $table->boolean('course_type')->nullable()->comment('1: one-to-one; 2: one-to-two, 3: one-to-four, 4: one-to-six');
            $table->float('total_ratings')->nullable();
            $table->text('course_description');
            $table->boolean('status')->comment('0: In-Active; 1: Active, 2: Deleted');
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('grade_id')->references('id')->on('grades')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sphere_courses');
    }
}

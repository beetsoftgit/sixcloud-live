<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTitleRuDescriptionRuVideos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('videos', function(Blueprint $table)
        {
            $table->string('title_ru')->nullable()->after('description_chi');
            $table->text('description_ru')->nullable()->after('title_ru');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('videos', function(Blueprint $table)
        {
            $table->dropColumn('title_ru');
            $table->dropColumn('description_ru');
        });
    }
}

<?php

//###############################################################
//File Name : MarketplaceSellerPayments.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to designer payment
//Date : 10th May, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketplaceSellerPayments extends Model
{
    protected $table = 'mp_seller_payments';

    
}

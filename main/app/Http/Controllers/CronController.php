<?php

/**
 * Short description for file
 *
 * PHP version 5 and 7
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 */
/*
 * Place includes controller for login & forgot password.
 */

/**
  Pre-Load all necessary library & name space
 */

namespace App\Http\Controllers;

//load required library by use
use App\Userprcase;
use App\Proofreadingpackage;
use App\Accessor;
use App\Userprcaselog;
//load session & other useful library
use Auth;
use Validator;
use Image;
use Carbon\Carbon;
//define model
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use OSS\OssClient;
use OSS\Core\OssException;
use OSS\Core\OssUtil;

/**
 * Photos
 * @package    CronController
 * @subpackage Controller
 * @author     Senil Shah <senil@creolestudios.com>
 */
class CronController extends BaseController {

    public function __construct() {
        //Artisan::call('cache:clear');
    }

    public function index() {
        
    }

    //###############################################################
    //Function Name : Delayedcase
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To notify the accessor for the delay status and also maintaining the flag
    //In Params : Void
    //Return : json
    //Date : 27th December 2017
    //###############################################################
    public function Delayedcase(Request $request) {
        try{
            $getCases = Userprcase::whereHas('accessment_time')->with([
                        'accessment_time' => function($query){
                            $query->orderBy('id','desc')->limit(1)->with(
                                [
                                    'accessor_information' => function($query){
                                        $query->select('*');       
                                    }
                                ]);
                        }])->with([
                        'proof_reading_package' => function($query){
                            $query->select('*');
                        },
                        'case_user' => function($query){
                            $query->select('*');
                        }
                    ])->get();
            $getCases = json_decode(json_encode($getCases),true);
            foreach ($getCases as $key => $value) {
                foreach ($value['accessment_time'] as $keyInner => $valueInner) {
                    $date = Carbon::parse($valueInner['case_accept_time'])->format('g');
                    $current = Carbon::now()->format('g');
                    if($current < 23 && $current >= 8){
                        if($date < $current){
                            if($current - $date == 4){
                                #send mail to accessor
                                $data['email'] = $valueInner['accessor_information']['email_address'];
                                $data['file_name'] = $value['case_original_file_name'];
                                $data['accessor_name'] = $valueInner['accessor_information']['first_name'] . ' ' . $valueInner['accessor_information']['last_name'];
                                Mail::send('emails.case_delay_mail', $data, function($message) use ($data) {
                                    $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                                    $message->to($data['email'])->subject('Regarding : Case accessment delay');
                                });
                                $responseArray = UtilityController::Generateresponse(true,'NOTIFY_ACCESSOR_FOR_DELAY_SENT',1);
                            }
                            if($current - $date == $value['proof_reading_package']['level1_assessment_time_limit']){
                                #set status of case to delayed 
                                $case['case_status'] = 5;
                                $result = UtilityController::Makemodelobject($case,'Userprcase','id',$value['id']);
                                $responseArray = UtilityController::Generateresponse(true,'CASE_DELAY_STATUS_CHANGED',1);
                            }
                        }
                    }
                    $responseArray = UtilityController::Generateresponse(false,'NO_CHANGES',0);
                }
            }
            $responseArray = UtilityController::Generateresponse(false,'NO_CHANGES',0);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }
}
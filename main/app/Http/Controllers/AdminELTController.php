<?php

/**
 * Short description for file
 *
 * PHP version 5 and 7
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @package    AdminELTController
 * @author     Komal Kapadi (komal@creolestudios.com)
 * @copyright  2017 Sixcloud Group
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @since      File available since Release 1.0.0
 * @deprecated N/A
 */
/*
 */

/**
Pre-Load all necessary library & name space
 */

namespace App\Http\Controllers;

use App\Admin;
use App\Http\Controllers\UtilityController;
use App\VideoCategory;
use App\Videos;
use Auth;
use Illuminate\Http\Request;
use Mail;
use OSS\OssClient;

class AdminELTController extends Controller
{

    //###############################################################
    //Function Name : Getadmineltsections
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get ELT admin sections with videos
    //In Params : Void
    //Return : json
    //###############################################################
    public function Getadmineltsections(Request $request)
    {
        try {
            $result = VideoCategory::with([
                'videos' => function ($query) {
                    $query->select('*')->where('status', 1);
                },
            ])->where('status', 1)->get();
            if ($result) {
                if (!empty($result)) {
                    $ossClient = new OssClient(UtilityController::Getmessage('ACCESS_KEY_ID'), UtilityController::Getmessage('ACCESS_KEY_SECRET'), UtilityController::Getmessage('END_POINT'));
                    foreach ($result as $key => $value) {
                        foreach ($value['videos'] as $keyInner => $val) {
                            $result[$key]['videos'][$keyInner]['video_url'] = UtilityController::getSignedUrlForGettingObject($ossClient, UtilityController::Getmessage('BUCKET'), UtilityController::Getpath('BUCKET_ELT_FOLDER') . $val['file_name']);
                        }
                    }
                }
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $result);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 400, '');
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Addnewsection
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To add new section
    //In Params : Void
    //Return : json
    //###############################################################
    public function Addnewsection(Request $request)
    {
        try {
            $validator  = UtilityController::ValidationRules($request->all(), 'VideoCategory');
            $returnData = $validator;
            if (!$validator['status']) {
                $returnData = UtilityController::Setreturnvariables();
            } else {
                $categoryData = UtilityController::MakeObjectFromArray($request->all(), 'VideoCategory')->toArray();
                $video        = new VideoCategory;
                $newCategory  = VideoCategory::insert($categoryData);
                $returnData   = UtilityController::Generateresponse(true, 'SECTION_ADDED', 200, '');
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Getallvideoposition
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get all video's position of particular section
    //In Params : Void
    //Return : json
    //###############################################################
    public function Getallvideoposition(Request $request)
    {
        try {
            $result['position']      = Videos::where('video_category_id', request('video_category_id'))->get();
            $result['last_position'] = Videos::where('video_category_id', request('video_category_id'))->count() + 1;
            $returnData              = UtilityController::Generateresponse(true, 'SECTION_ADDED', 200, $result);
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Addnewvideo
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To add new video
    //In Params : Void
    //Return : json
    //###############################################################
    public function Addnewvideo(Request $request)
    {
        try {
            $validator  = UtilityController::ValidationRules($request->all(), 'Videos');
            $returnData = $validator;
            if (!$validator['status']) {
                $returnData = UtilityController::Setreturnvariables();
            } else {
                $categoryData = UtilityController::MakeObjectFromArray($request->all(), 'Videos')->toArray();
                // // create image unique name
                $videoName = str_replace(" ", "_", $categoryData['title']) . '_' . time() . '.' . $request->file_name->getClientOriginalExtension();

                // // if file is moved to destination. store file name and type in array.
                $categoryData['file_name'] = $videoName;
                // Check whether video with same order exists or not
                $videoExists = Videos::where('video_ordering', $categoryData['video_ordering'])->where('status', 1)->first();
                if (!empty($videoExists)) {
                    Videos::where('video_ordering', '>=', $categoryData['video_ordering'])->where('status', 1)->increment('video_ordering');
                }
                // Insert newly added video
                // OSS Code Start

                $categoryData['slug'] = UtilityController::slugify($categoryData['title']);
                $bucket    = UtilityController::Getmessage('BUCKET');
                $object    = UtilityController::Getpath('BUCKET_ELT_FOLDER') . $videoName;
                $filePath  = $_FILES['file_name']['tmp_name'];
                $ossClient = new OssClient(UtilityController::Getmessage('ACCESS_KEY_ID'), UtilityController::Getmessage('ACCESS_KEY_SECRET'), UtilityController::Getmessage('END_POINT'));
                $ossClient->uploadFile($bucket, $object, $filePath);

                // $ossClient = new OssClient($accessKeyId, $accessKeySecret, $endpoint);
                $test      = $ossClient->uploadFile($bucket, $object, $filePath);
                if (!empty($test) && $test['info']['url']) {
                    $data['username'] = 'komal';
                    $data['link']     = $test['info']['url'];
                    $data['email']    = 'komal@creolestudios.com';
                    $mailResult       = Mail::send('front.emails.forgotpassword', $data, function ($message) use ($data) {
                        $message->from(Config('constants.messages.MAIL_ID'), 'Sixclouds');
                        $message->to('komal@creolestudios.com')->subject('Sixclouds : Forgot password');
                    });
                    $returnData = UtilityController::Generateresponse(true, 'FORGOT_PASSWORD_SUCCESS', 200, '');
                }
                // OSS Code Over
                $newCategory = UtilityController::Makemodelobject($categoryData, 'Videos', 'id');
                $returnData  = UtilityController::Generateresponse(true, 'VIDEO_ADDED', 200, '');
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Getreportingofficers
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get all reporting officers
    //In Params : Void
    //Return : json
    //###############################################################
    public function Getreportingofficers(Request $request)
    {
        try {
            $result     = Admin::where('status', 1)->get();
            $returnData = UtilityController::Generateresponse(true, 'ADMIN_ADDED', 200, $result);
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
}

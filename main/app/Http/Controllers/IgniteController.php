<?php

/**
 * Short description for file
 *
 * PHP version 5 and 7
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 */
/*
 * Place includes controller for login & forgot password.
 */

/**
Pre-Load all necessary library & name space
 */

namespace App\Http\Controllers;
//load required library by use
use App\Distributors;
use App\User;
use App\DistributorsTeamMembers;
use App\DistributorsReferrals;
use App\VideoCategory;
use App\Videos;
use App\VideoLanguageUrl;
use App\VideoViews;
use App\VideoViewsHistory;
use App\EventLog;
use App\Worksheets;
use App\Quizzes;
use App\QuizAttempts;
use App\MarketPlacePayment;
use App\IgniteCommissions;
use App\PromoCode;
use App\Subject;
use App\Zone;
use App\Language;
use App\ZoneLanguage;
use App\ZoneVideo;
use App\ZoneQuiz;
use App\ZoneWorksheet;
use App\VersionControl;
use App\TaxRate;
use App\JobManage;
use App\StoreEmail;
use App\StorePromocode;
use App\VideoCategoryHistory;
use Validator;
use Auth;
use Image;
use Carbon\carbon;
use PDF;
use App\Jobs\ChangeSalespersonCommission;
use App\Jobs\IgniteCategoryPayment;
use App\Jobs\EmailForNotify;
use App\Jobs\SubscriptionPlanPayment;
##for sms gatway start
use Qcloud\Sms\SmsSingleSender;
use Qcloud\Sms\SmsMultiSender;
use Qcloud\Sms\SmsVoiceVerifyCodeSender;
use Qcloud\Sms\SmsVoicePromptSender;
use Qcloud\Sms\SmsStatusPuller;
use Qcloud\Sms\SmsMobileStatusPuller;

use Qcloud\Sms\VoiceFileUploader;
use Qcloud\Sms\FileVoiceSender;
use Qcloud\Sms\TtsVoiceSender;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Hash;
use QrCode;
use \Firebase\JWT\JWT;
use URL;


##strip
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;
use Stripe\Token;
/**
 * Photos
 * @package    IgniteController
 * @subpackage Controller
 * @author     Zalak Kapadia <zalak@creolestudios.com>
 */
class IgniteController extends BaseController
{

    public function __construct()
    {
        //Artisan::call('cache:clear');
    }

    public function index()
    {

    }

    //###############################################################
    //Function Name : Getbuzzdata
    //Author : Nivedita <nivedita@creolestudios.com>
    //Purpose : To get detaiils of all the buzzes
    //In Params : Void
    //Return : json
    //Date : 13th Sept 2018
    //###############################################################
    public function Getbuzzdata(Request $request)
    {
        try {
            $allBuzzData = VideoCategory::select("*")->get();
            if (!empty($allBuzzData)) {
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $allBuzzData);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
        } catch (Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getFile().':'.$e->getLine().':'.$e->getMessage(), '', '');
        }
        return response($returnData);
        
    }


    //###############################################################
    //Function Name : Getloginuserdatadistributor
    //Author : Nivedita <nivedita@creolestudios.com>
    //Purpose : To get login user data of distributor
    //In Params : Void
    //Return : json
    //Date : 22nd Aug 2018
    //###############################################################
    public function Getloginuserdatadistributor(Request $request)
    {
        try {
            if (Auth::guard('distributors')->check()) {
                $loginDistributorData = Distributors::select('*')->where('id',Auth::guard('distributors')->user()->id)->whereIn('status', [1,0])->get()->toArray();
                $loginDistributorData = $loginDistributorData[0];
                $loginDistributorData['dob'] = Carbon::parse($loginDistributorData['dob'])->format('d/m/Y');
                $loginDistributorData['userType'] = 'distributor';
                $returnData = UtilityController::Generateresponse(true, '', '', $loginDistributorData);
                return $returnData;
            } else{
                if (Auth::guard('distributor_team_members')->check()) {
                    $loginDistributorData = DistributorsTeamMembers::select('*')->where('id',Auth::guard('distributor_team_members')->user()->id)->whereIn('status', [1,4,5])->get()->toArray();
                    $loginDistributorData = $loginDistributorData[0];
                    // $loginDistributorData['dob'] = Carbon::parse($loginDistributorData['dob'])->format('d/m/Y');
                    $loginDistributorData['userType'] = 'distributor_team_member';
                    $returnData = UtilityController::Generateresponse(true, '', '', $loginDistributorData);
                    return $returnData;
                }
                if (Auth::check()) {
                    Auth::user()->userType = 'ignite_user';
                    $loginDistributorData = Auth::user();
                    $loginDistributorData['created_at'] = Carbon::parse($loginDistributorData->created_at)->timezone($loginDistributorData->timezone);
                    $loginDistributorData['updated_at'] = Carbon::parse($loginDistributorData->updated_at)->timezone($loginDistributorData->timezone);
                    $subscription_allowed = Zone::select('is_subscription_allowed')->where('id', $loginDistributorData['zone_id'])->first()->toArray();
                    $loginDistributorData['is_subscription_allowed'] = $subscription_allowed['is_subscription_allowed'];
                    if(isset($loginDistributorData['dob']))
                        $loginDistributorData['dob'] = Carbon::parse($loginDistributorData['dob'])->format('d/m/Y');
                    $loginDistributorData['userType'] = 'ignite_user';
                    if($loginDistributorData['userType'] == 'ignite_user' && !is_null($loginDistributorData['video_preference'])){
                        $loginDistributorData['video_url'] = explode(':', base64_decode($loginDistributorData['video_preference']))[0];
                        $loginDistributorData['video_language_preference'] = explode(':', base64_decode($loginDistributorData['video_preference']))[1];
                    } else {
                        $loginDistributorData['video_language_preference'] = null;
                    }
                    $returnData = UtilityController::Generateresponse(true, '', '', $loginDistributorData);
                    return $returnData;
                }
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, $e->getFile().':'.$e->getLine().':'.$e->getMessage(), '', '');
            return $returnData;
        }
        
    }
    

    //###############################################################
    //Function Name : Logoutdistributor
    //Author : Nivedita Mitra <nivedita@creolestudios.com>
    //Purpose : To logout user from distributor section
    //In Params : void
    //Return : logout & redirect user to login screen of ignite,
    //###############################################################
    public function Logoutdistributor() {

        try {
            if(Auth::guard('distributors')->check()){
                if (!Auth::guard('distributors')->logout()) {
                    $returnData = UtilityController::Generateresponse(true, 'LOGOUT_SUCCESS', 1,'distributor');
                    return $returnData;
                }
            } else {
                if(Auth::guard('distributor_team_members')->check()){
                    if (!Auth::guard('distributor_team_members')->logout()) {
                        $returnData = UtilityController::Generateresponse(true, 'LOGOUT_SUCCESS', 1,'team');
                        return $returnData;
                    }
                }
                if(Auth::check()){
                    if (!Auth::logout()) {
                        \Session::forget('user_mdstr');
                        $returnData = UtilityController::Generateresponse(true, 'LOGOUT_SUCCESS', 1,'user');
                        return $returnData;
                    }   
                }
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
        //logout user
       /* $responseArray = array();
        $responseArray['success'] = false;
        if (!Auth::guard('distributors')::logout()) {
            $responseArray['status'] = true;
            $responseArray['message'] = UtilityController::Getmessage('LOGOUT_SUCCESS');
            //redirect user to login screen
        }
        return $responseArray;*/
    }


    //###############################################################
    //Function Name : Dodistributorsignin
    //Author : Nivedita <nivedita@creolestudios.com>
    //Purpose : To login the distributor
    //In Params : email,password
    //Return : success & redirect to referral page
    //###############################################################
    public function Dodistributorsignin(Request $request) {
        try {
            /*$validator  = UtilityController::ValidationRules($request->all(), 'Distributors', array('first_name', 'last_name', 'contact','password'));
            $returnData = $validator;*/
            $input      = Input::all();
            $isLoggedin = 0;
            $checkFor = Input::get("check_for");
            /*$isLoggedin = 0;
            if (!$validator['status']) {
                $returnData = UtilityController::Setreturnvariables();
                echo "string";
                exit();
            } else {*/

                //Code to update zone id if not available
                $zones = Zone::get()->toArray();
                $userData = User::where('email_address', $request->email_address)->first();
                if($userData) {
                    if(isset($userData) && isset($userData->timezone)){
                        $userData->timezone = $input['timezone'];
                    }
                    foreach ($zones as $key => $value) {
                        if(in_array($userData->country_id, explode(',', $value['country_id']))) {
                            $userData->zone_id = $value['id'];
                            $userData->save();
                        }
                    }
                }

                if($checkFor=='distributor'){
                    if (Auth::guard('distributors')->check()) {
                        $isLoggedin = 1;
                    }
                    if ($isLoggedin == '1') {
                        //user already logged in then return success
                        $data = Auth::guard('distributors')->user();
                        $data = Distributors::where('id',$data->id)->first();
                        $data['userType'] = 'distributor';
                        $returnData = UtilityController::Generateresponse(true, 'LOGIN_SUCCESS', 1, $data);
                    } else {
                        //check user's credentials
                        $credentials = [
                            "email_address" => Input::get("email_address"),
                            "password"      => Input::get("password"),
                        ];
                        if (Auth::guard('distributors')->attempt($credentials)) {
                            $data = Auth::guard('distributors')->user();
                            $data = Distributors::where('id',$data->id)->first();
                            $data['userType'] = 'distributor';
                            $returnData = UtilityController::Generateresponse(true, 'LOGIN_SUCCESS', 1, $data);
                        } else {
                            $returnData = UtilityController::Generateresponse(false, 'LOGIN_FAIL', 204);
                        }
                    }
                } else if($checkFor=='team') {
                    if (Auth::guard('distributor_team_members')->check()) {
                        $isLoggedin = 1;
                    }
                    if ($isLoggedin == '1') {
                        //user already logged in then return success
                        $data = Auth::guard('distributor_team_members')->user();
                        $data = DistributorsTeamMembers::where('id',$data->id)->first();
                        $data['userType'] = 'team';
                        $returnData = UtilityController::Generateresponse(true, 'LOGIN_SUCCESS', 1, $data);
                    } else {
                        //check user's credentials
                        $credentials = [
                            "email_address" => Input::get("email_address"),
                            "password"      => Input::get("password"),
                            // "status"        => 1,
                        ];
                        if (Auth::guard('distributor_team_members')->attempt($credentials) && Auth::guard('distributor_team_members')->user()->status==1) {
                            $data = Auth::guard('distributor_team_members')->user();
                            $data = DistributorsTeamMembers::where('id',$data->id)->first()->toArray();
                            $data['userType'] = 'team';
                            $returnData = UtilityController::Generateresponse(true, 'LOGIN_SUCCESS', 200, $data);
                            return $returnData;
                        } else if (Auth::guard('distributor_team_members')->attempt($credentials) && Auth::guard('distributor_team_members')->user()->status==4) {
                            $data = Auth::guard('distributor_team_members')->user();
                            $data = DistributorsTeamMembers::where('id',$data->id)->first()->toArray();
                            $data['userType'] = 'team';
                            $returnData = UtilityController::Generateresponse(true, 'UNVERIFIED_TEAM_MEMBER', 401, $data);
                            return $returnData;
                        } else if (Auth::guard('distributor_team_members')->attempt($credentials) && Auth::guard('distributor_team_members')->user()->status==5) {
                            $data = Auth::guard('distributor_team_members')->user();
                            $data = DistributorsTeamMembers::where('id',$data->id)->first()->toArray();
                            $data['userType'] = 'team';
                            $returnData = UtilityController::Generateresponse(true, 'INCOMPLETE_COMMOM_DETAILS', 401, $data);
                            return $returnData;
                        } else {
                            $returnData = UtilityController::Generateresponse(false, 'LOGIN_FAIL', 204);
                        }
                    }
                } else {

                    if (Auth::check()) {
                        $isLoggedin = 1;
                    }
                    if ($isLoggedin == '1') {
                        //user already logged in then return success
                        $data = Auth::user();
                        $data = User::where('id',$data->id)->first();
                        if(!isset($data->zone_id) || empty($data->zone_id) || is_null($data->zone_id)) {
                            $zones = Zone::select('id','country_id')->get()->toArray();
                            foreach ($zones as $key => $zone) {
                                $zoneArray = explode(',', $zone['country_id']);
                                if(in_array($data->country_id, $zoneArray)) {
                                    $data->zone_id = $zone['id'];
                                    $data->save();
                                    break;
                                }
                            }
                        }
                        Auth::user()->userType = 'ignite_user';
                        $data['userType'] = 'ignite_user';
                        if($data['country_id']==0 && ($data['city_id']==0 || $data['provience_id']==0))/*||!isset($data['dob'])*/{
                            $request->session()->push('user_mdstr', md5(Input::get("password")));
                            $returnData = UtilityController::Generateresponse(true, 'LOGIN_SUCCESS_TO_MY_ACCOUNT', 5, $data);
                            /* status code 5 for redirection to my-account page. If changing from 5 to anything, please change in IgniteLoginController.js */
                        } else {
                            $request->session()->push('user_mdstr', md5(Input::get("password")));
                            $returnData = UtilityController::Generateresponse(true, 'LOGIN_SUCCESS', 1, $data);
                        }
                        $request->session()->push('user_mdstr', md5(Input::get("password")));
                        $returnData = UtilityController::Generateresponse(true, 'LOGIN_SUCCESS', 1, $data);
                    } else {
                        //check user's credentials
                        $credentials = [
                            "email_address" => Input::get("email_address"),
                            "password"      => Input::get("password"),
                            "status"        => 1,
                        ];
                        /*if (Auth::attempt($credentials)) { 
                            $new_sessid = \Session::getId(); 
                            $data = Auth::user(); 
                            $data = User::where('id',$data->id)->first(); 
                            if($data['session_id'] != '') { 
                                \Session::getHandler()->destroy($data['session_id']);
                            } 
                            User::where('id',$data->id)->update(['session_id'=>$new_sessid]); 
                            Auth::user()->userType = 'ignite_user';*/

                        if (Auth::attempt($credentials)) {
                            $new_sessid = \Session::getId();
                            $data = Auth::user();
                            $data = User::where('id',$data->id)->first();
                            if(!isset($data->zone_id) || empty($data->zone_id) || is_null($data->zone_id)) {
                                $zones = Zone::select('id','country_id')->get()->toArray();
                                foreach ($zones as $key => $zone) {
                                    $zoneArray = explode(',', $zone['country_id']);
                                    if(in_array($data->country_id, $zoneArray)) {
                                        $data->zone_id = $zone['id'];
                                        $data->save();
                                        break;
                                    }
                                }
                            }
                            if($data['session_id'] != '') { 
                                \Session::getHandler()->destroy($data['session_id']);
                            } 
                            User::where('id',$data->id)->update(['session_id'=>$new_sessid]);
                            Auth::user()->userType = 'ignite_user';
                            $data['userType'] = 'ignite_user';
                            if($data['country_id']==0 && ($data['city_id']==0 || $data['provience_id']==0))/*||!isset($data['dob'])*/{
                                $request->session()->push('user_mdstr', md5(Input::get("password")));
                                $returnData = UtilityController::Generateresponse(true, 'LOGIN_SUCCESS_TO_MY_ACCOUNT', 5, $data);
                                /* status code 5 for redirection to my-account page. If changing from 5 to anything, please change in IgniteLoginController.js */
                            } else {
                                $request->session()->push('user_mdstr', md5(Input::get("password")));
                                $returnData = UtilityController::Generateresponse(true, 'LOGIN_SUCCESS', 1, $data);
                            }
                        } else {
                            if(UtilityController::getMessage('CURRENT_DOMAIN')=='cn'){
                                $credentials = [
                                    "contact" => Input::get("email_address"),
                                    "password"      => Input::get("password"),
                                    "status"        => 1,
                                ];
                                if (Auth::attempt($credentials)) {
                                    $new_sessid = \Session::getId();
                                    $data = Auth::user();
                                    $data = User::where('id',$data->id)->first();
                                    if($data['session_id'] != '') { 
                                        \Session::getHandler()->destroy($data['session_id']);
                                    } 
                                    User::where('id',$data->id)->update(['session_id'=>$new_sessid]);
                                    Auth::user()->userType = 'ignite_user';
                                    $data['userType'] = 'ignite_user';
                                    if($data['country_id']==0 && ($data['city_id']==0 || $data['provience_id']==0))/*||!isset($data['dob'])*/{
                                        $request->session()->push('user_mdstr', md5(Input::get("password")));
                                        $returnData = UtilityController::Generateresponse(true, 'LOGIN_SUCCESS_TO_MY_ACCOUNT', 5, $data);
                                        /* status code 5 for redirection to my-account page. If changing from 5 to anything, please change in IgniteLoginController.js */
                                    } else {
                                        $request->session()->push('user_mdstr', md5(Input::get("password")));
                                        $returnData = UtilityController::Generateresponse(true, 'LOGIN_SUCCESS', 1, $data);
                                    }
                                } else {
                                    if(User::where('contact',$credentials['contact'])->where('status',5)->exists()&&is_numeric($credentials['contact'])){
                                        $user = User::where('contact',$credentials['contact'])->where('status',5)->first();
                                        $otpInput['country_code']  = $user['phonecode'];
                                        $otpInput['phone_number']  = $user['contact'];
                                        $otpInput['email_address'] = base64_encode($user['email_address']);
                                        $otpInput['language'] = $user['current_language'];
                                        $otpInput['user_id'] = $user['id'];
                                        $otpInput['status'] = 5;
                                        $otpReturn = UserController::Sendotp($otpInput);
                                        $returnMessage = Input::get('language')=='en'?'USER_CREATED_CN':'USER_CREATED_CN_CHINESE';
                                        $returnData = UtilityController::Generateresponse(false, $returnMessage, 204, $otpInput);
                                    } elseif(User::where('email_address',$credentials['contact'])->where('status',5)->exists()&&!is_numeric($credentials['contact'])){
                                        $user = User::where('email_address',$credentials['contact'])->where('status',5)->first();
                                        $otpInput['country_code']  = $user['phonecode'];
                                        $otpInput['phone_number']  = $user['contact'];
                                        $otpInput['email_address'] = base64_encode($user['email_address']);
                                        $otpInput['language'] = $user['current_language'];
                                        $otpInput['user_id'] = $user['id'];
                                        $otpInput['status'] = 5;
                                        $otpReturn = UserController::Sendotp($otpInput);
                                        $returnMessage = Input::get('language')=='en'?'USER_CREATED_CN':'USER_CREATED_CN_CHINESE';
                                        $returnData = UtilityController::Generateresponse(false, $returnMessage, 204, $otpInput);
                                    } else {
                                        $returnMessage = Input::get('language')=='en'?'LOGIN_FAIL':(Input::get('language')=='chi'?'LOGIN_FAIL_CHINESE':'LOGIN_FAIL_RU');
                                        //$returnMessage = Input::get('language')=='en'?'LOGIN_FAIL':'LOGIN_FAIL_CHINESE';
                                        $returnData = UtilityController::Generateresponse(false, $returnMessage, 204);
                                    }    
                                }
                            } else {
                                if(User::where('email_address',$credentials['email_address'])->where('status',4)->exists()){
                                    $otpInput['status'] = 4;
                                    //$returnMessage = Input::get('language')=='en'?'USER_CREATED':'USER_CREATED_CHINESE';
                                    $returnMessage = Input::get('language')=='en'?'USER_CREATED':(Input::get('language')=='chi'?'USER_CREATED_CHINESE':'USER_CREATED_RU');
                                    $returnData = UtilityController::Generateresponse(false, $returnMessage, 204, $otpInput);
                                } else {
                                    $returnMessage = Input::get('language')=='en'?'LOGIN_FAIL':(Input::get('language')=='chi'?'LOGIN_FAIL_CHINESE':'LOGIN_FAIL_RU');
                                    //$returnMessage = Input::get('language')=='en'?'LOGIN_FAIL':'LOGIN_FAIL_CHINESE';
                                    $returnData = UtilityController::Generateresponse(false, $returnMessage, 204);
                                }    
                                // $returnData = UtilityController::Generateresponse(false, 'LOGIN_FAIL', 204);
                            }
                        }
                    }
                }
            //}
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData     = UtilityController::Generateresponse(false, $e->getFile().':'.$e->getLine().':'.$e->getMessage(), 0);
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Addteammember
    //Author : Nivedita <nivedita@creolestudios.com>
    //Purpose : To add new team member for distributor
    //In Params : details
    //Return : 
    //###############################################################
    public function Addteammember(Request $request) {
        try {
            if (Auth::guard('distributors')->user()->id) {
                \DB::beginTransaction();
                $Input = Input::all();
                if(array_key_exists('memberId', $Input)){
                    $Input['slug'] = str_slug($Input['first_name'], '-') . ':' . base64_encode($Input['memberId']);
                    if(array_key_exists('effective_date', $Input)&&isset($Input['effective_date'])){
                        $nextMonth = Carbon::today()->addMonth()->startOfMonth();
                        $Input['effective_date'] = Carbon::parse($Input['effective_date']);
                        if($Input['effective_date'] < $nextMonth)
                            throw new \Exception($Input['language']=='en'?'Effective Date should be from next month':'有效的日期应该是从下一个月的');
                        $diffInMinutes = Carbon::now()->diffInMinutes($Input['effective_date']);
                        $changeCommission = (new ChangeSalespersonCommission($Input['commission_percentage'],$Input['renewal_commission_percentage'],$Input['memberId']))->delay(Carbon::now()->addMinutes($diffInMinutes));
                        dispatch($changeCommission);
                        unset($Input['commission_percentage']);
                        unset($Input['renewal_commission_percentage']);
                    }
                    $result        = UtilityController::Makemodelobject($Input,'DistributorsTeamMembers','',$Input['memberId']);
                    $returnData = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 200, $result);
                } else { 
                    $password                = str_random(10);
                    $distributorsId          = Auth::guard('distributors')->user();
                    $Input['distributor_id'] = $distributorsId->id;
                    $Input['password']       = Hash::make($password);
                    $result                  = UtilityController::Makemodelobject($Input, 'DistributorsTeamMembers');


                    if (!empty($result)) {
                        //Generate the password here
                        /*$password             = str_random(10);
                        $hashPassword         = Hash::make($password);
                        $slug['password']     = $hashPassword;*/
                        $nameSlug             = $result['first_name'].' '.$result['last_name'];
                        //$slug['slug']         = str_slug($nameSlug, '-') . ':' . base64_encode($result['id']);
                        $slug['slug']         = UtilityController::Makeslug($nameSlug) . ':' . base64_encode($result['id']);
                        $slug['qrcode_token'] = date("YmdHis").str_random(10);

                        ##Code to send the Login credentials to the Distributor starts here                        
                        $Input['User']['user_name']     = $result['first_name'] . " " . $result['last_name'];
                        $Input['User']['email_address'] = $result['email_address'];
                        $Input['User']['password']      = $password;       
                        $toLogin = url('/').'/distributor/team-login';
                        if($Input['language']=='en'){
                            $Input['User']['message'] = "Here are your Login Credentials:";
                            $Input['footer_content']  = "From,<br /> SixClouds";
                            $Input['footer']          = "SixClouds";
                            $Input['subject']         = 'You have been added as a Team Member';
                            $Input['content']         = "Hi <strong>".$Input['User']['user_name']."</strong>,<br/><br/>Here are your account login details:<br/><br/><span style='font-size: 15px;''><strong>Email Address : </strong>".$Input['User']['email_address']."</span><br/><span style='font-size: 15px;'><strong>Password : </strong>".$password."</span><br><br><br><br><a href='".$toLogin."' class='' style='color: #fff;
                                                   background-color: #009bdd;
                                                   border-color: #bd383e;
                                                   text-decoration: none;display: inline-block;
                                                   padding: 5px 10px;
                                                   margin-bottom: 0;
                                                   margin-top: 3px;
                                                   font-size: 14px;
                                                   font-weight: 400;
                                                   line-height: 1.42857143;
                                                   text-align: center;
                                                   white-space: nowrap;
                                                   vertical-align: middle;
                                                   -ms-touch-action: manipulation;
                                                   touch-action: manipulation;
                                                   cursor: pointer;
                                                   -webkit-user-select: none;
                                                   -moz-user-select: none;
                                                   -ms-user-select: none;
                                                   user-select: none;
                                                   background-image: none;
                                                   border: 1px solid transparent;
                                                   border-radius: 4px;'>Login</a><br><br>If you are not able to click the link successfully, try the link below or copy the link to the browser address bar.<br>".$toLogin."";
                        } else if($Input['language']=='chi') {
                            $Input['User']['message'] = "请用以下的登录凭据，";
                            $Input['footer_content']  = "From,<br /> 六云";
                            $Input['footer']          = "六云";
                            $Input['subject']         = '欢迎！您已被添加为销售员';
                            $Input['content']         = "您好 <strong>".$Input['User']['user_name']."</strong>,<br/><br/>请用以下的登录凭据：<br/><br/><span style='font-size: 15px;''><strong>电邮： </strong>".$Input['User']['email_address']."</span><br/><span style='font-size: 15px;'><strong>密码： </strong>".$password."</span><br><br><br><br><a href='".$toLogin."' class='' style='color: #fff;
                                                   background-color: #009bdd;
                                                   border-color: #bd383e;
                                                   text-decoration: none;display: inline-block;
                                                   padding: 5px 10px;
                                                   margin-bottom: 0;
                                                   margin-top: 3px;
                                                   font-size: 14px;
                                                   font-weight: 400;
                                                   line-height: 1.42857143;
                                                   text-align: center;
                                                   white-space: nowrap;
                                                   vertical-align: middle;
                                                   -ms-touch-action: manipulation;
                                                   touch-action: manipulation;
                                                   cursor: pointer;
                                                   -webkit-user-select: none;
                                                   -moz-user-select: none;
                                                   -ms-user-select: none;
                                                   user-select: none;
                                                   background-image: none;
                                                   border: 1px solid transparent;
                                                   border-radius: 4px;'>登录</a><br><br>如果链接不起作用，请将链接网址复制到浏览器地址栏。<br>".$toLogin."";
                        }else{
                            $Input['User']['message'] = "Вот ваши данные для входа в аккаунт:";
                            $Input['footer_content']  = "From,<br /> SixClouds";
                            $Input['footer']          = "SixClouds";
                            $Input['subject']         = 'Вы были добавлены в качестве члена команды';
                            $Input['content']         = "Привет <strong>".$Input['User']['user_name']."</strong>,<br/><br/>Вот ваши данные для входа в аккаунт:<br/><br/><span style='font-size: 15px;''><strong>Адрес электронной почты: </strong>".$Input['User']['email_address']."</span><br/><span style='font-size: 15px;'><strong>Пароль: </strong>".$password."</span><br><br><br><br><a href='".$toLogin."' class='' style='color: #fff;
                                                   background-color: #009bdd;
                                                   border-color: #bd383e;
                                                   text-decoration: none;display: inline-block;
                                                   padding: 5px 10px;
                                                   margin-bottom: 0;
                                                   margin-top: 3px;
                                                   font-size: 14px;
                                                   font-weight: 400;
                                                   line-height: 1.42857143;
                                                   text-align: center;
                                                   white-space: nowrap;
                                                   vertical-align: middle;
                                                   -ms-touch-action: manipulation;
                                                   touch-action: manipulation;
                                                   cursor: pointer;
                                                   -webkit-user-select: none;
                                                   -moz-user-select: none;
                                                   -ms-user-select: none;
                                                   user-select: none;
                                                   background-image: none;
                                                   border: 1px solid transparent;
                                                   border-radius: 4px;'>Логин</a><br><br>Если вы не можете нажать на ссылку успешно, попробуйте ссылку ниже или скопируйте ссылку в адресную строку браузера.<br>".$toLogin.""; 
                        }

                        ##Code to generate the QRcode starts here                                 
                        //$qrCodeString = str_slug($Input['User']['user_name'])."*".$slug['slug']."*".$slug['qrcode_token'];
                        $fileName     = date("YmdHis").str_random(10).".png";
                        $filePath = public_path().'/uploads/distributor_qr_codes/team/'.$fileName;

                        ##Generation of Referal Code starts here                                 
                        // $referalCode = $this->GenerateReferalCode("TM",UtilityController::Makeslug($result['first_name']),UtilityController::Makeslug($result['last_name']));
                        repeat:
                        $referalCode = $this->GenerateReferalCode("TM",AdminIgniteController::Generaterandominitial(),AdminIgniteController::Generaterandominitial());
                        if(DistributorsTeamMembers::where('referal_code',$referalCode)->exists())
                            goto repeat;
                        else
                            $slug['referal_code'] = $referalCode;
                        ##Code to generate the Unique Link starts here
                        // $backUrl          = date("dmYHis").rand().'_'.base64_encode($slug['slug'].'_'.base64_encode(2).'_'.base64_encode($distributorsId));
                        
                        /*$backUrl             = base64_encode(date("dmYHis").rand().'_'.$slug['slug'].'_'.base64_encode(2).'_'.base64_encode($distributorsId));
                        $backUrl             = str_replace('/', '_', $backUrl);
                        $uniqueLink          = url('')."/ignite-signup/".$backUrl;
                        $slug['unique_link'] = $uniqueLink;*/

                        $backUrl                = base64_encode(date("dmYHis").rand().'_'.$slug['slug'].'_'.base64_encode(2).'_'.base64_encode($distributorsId->id));
                        $backUrl                = str_replace('/', '@', $backUrl);
                        $backUrlLink            = url('')."/ignite-signup/".$backUrl.'/'.base64_encode(3);
                        $backUrlQr              = url('')."/ignite-signup/".$backUrl.'/'.base64_encode(2);
                        $slug['unique_link']    = $backUrlLink;
                        $slug['unique_link_qr'] = $backUrlQr;
                        
                        QrCode::format('png')->size(500)->generate($backUrlQr, public_path().'/uploads/distributor_qr_codes/team/'.$fileName);

                        $slug['qrcode_file_name'] = $fileName;                         
                        $resultslug               = UtilityController::Makemodelobject($slug, 'DistributorsTeamMembers', '', $result['id']);
                        /* Add referal code as promo code */
                        if(PromoCode::where('code_for',1)->where('distributor_id',$resultslug['distributor_id'])->exists()){
                            $promoDetail = PromoCode::where('code_for',1)->where('distributor_id',$resultslug['distributor_id'])->orderBy("id","DESC")->first();
                            $addReferaltoPromo['promo_code']                 = $referalCode;
                            $addReferaltoPromo['distributor_id']             = $distributorsId->id;
                            $addReferaltoPromo['distributor_team_member_id'] = $resultslug['id'];
                            $addReferaltoPromo['discount_type']              = $promoDetail['discount_type'];
                            $addReferaltoPromo['promo_type']                 = 0;
                            $addReferaltoPromo['discount_of']                = $promoDetail['discount_of'];
                            $addReferaltoPromo['redemption_type']            = 1;
                            $addReferaltoPromo['status']                     = 1;
                            $addReferaltoPromo['code_for']                   = 2;
                            $addReferaltoPromoResult = UtilityController::Makemodelobject($addReferaltoPromo,'PromoCode');
                        }

                        if (isset($Input['User']['email_address']) && $Input['User']['email_address'] != "") {
                            Mail::send('emails.email_template', $Input, function ($message) use ($Input, $filePath) {
                                $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                                $message->to($Input['User']['email_address']);
                                $message->subject($Input['subject']);
                                $message->attach($filePath);
                            });
                        }

                        //$returnMessage=$Input['language']=='en'?'WITHDRAW_SUBMITED':'WITHDRAW_SUBMITED_CHINESE';
                        $returnData = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 200, $resultslug);
                    } else {
                        $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
                    }
                }        
                \DB::commit();
                return $returnData;
            }else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, $e->getFile().':'.$e->getLine().':'.$e->getMessage(), '', '');
            return $returnData;
        }
    }


    //###############################################################
    //Function Name : GenerateReferalCode
    //Author        : Ketan Solank <ketan@creolestudios.com>
    //Purpose       : TO Generate teh Referal Code 
    //In Params     :
    //Return        : Json Encoded Data
    //Date          : 20th August 2018
    //###############################################################
    public function GenerateReferalCode($prefix,$firstName, $lastName)
    {
        try {           
            $dateString = date("YmdHis");
            $randomChar = substr(str_shuffle($dateString), 0, 4);
            $referalCode = $prefix.strtoupper(mb_substr($firstName,0,1,'utf-8')).strtoupper(mb_substr($lastName,0,1,'utf-8')).$randomChar; 
            return $referalCode;
            /*$checkedReferalCode = $this->CheckReferalCode($referalCode);
            if($checkedReferalCode === true){
               return $referalCode;             
            }else{*/
            //$this->GenerateReferalCode($prefix,$firstName,$lastName);
            //}            
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray = UtilityController::Generateresponse(0, $e->getFile().':'.$e->getLine().':'.$e->getMessage(), '', '');
        }
        return response()->json($responseArray); 
    }


    //###############################################################
    //Function Name: Getallteamdata
    //Author:        Nivedita <nivedita@creolestudios.com>
    //Purpose:       To get all team members Data for the logged in Distributor
    //In Params:     
    //Return:        Listing of team members Data
    //###############################################################
    public function Getallteamdata(Request $request) {
        try { 
            $Input = Input::all();
            if(Auth::guard('distributors')->check()){
                $distributorId = Auth::guard('distributors')->user()->id;
                if(isset($distributorId) && is_numeric($distributorId) && $distributorId > 0){                             
                    $allTeamMembersData = DistributorsTeamMembers::with('distributor')->withCount('distributor_referrals_count')->whereIn('status',[1,3,4,5]);
                    if(array_key_exists('searchby', $Input)&&isset($Input['searchby'])&&!is_array($Input['searchby'])){
                        $allTeamMembersData = $allTeamMembersData->where('first_name','like','%'.$Input['searchby'].'%')->orWhere('last_name','like','%'.$Input['searchby'].'%')->orWhere('contact','like','%'.$Input['searchby'].'%')->orWhere('email_address','like','%'.$Input['searchby'].'%');
                    }
                    $allTeamMembersData = $allTeamMembersData->where('distributor_id', $distributorId)->orderBy("created_at","DESC")
                    ->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))
                    ->toArray();
                    if (!empty($allTeamMembersData)) {
                        $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $allTeamMembersData);
                    } else {
                        $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
                    }
                }else{
                    $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
                }            
            } else{
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }           
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);            
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name: Teammemberdetail
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To get team members details for the logged in Distributor
    //In Params:     team member slug
    //Return:        Team members details
    //Date:          9th Oct 2018
    //###############################################################
    public function Teammemberdetail(Request $request) {
        try { 
            $Input = Input::all();
            if(Auth::guard('distributors')->check()){
                $distributorId = Auth::guard('distributors')->user()->id;                             
                $teamMemberData = DistributorsTeamMembers::with('distributor')->withCount('distributor_referrals_count')->where('slug', $Input['slug'])->whereIn('status',[1,3,4,5])->first();
                if (!empty($teamMemberData)) {
                    $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $teamMemberData);
                } else {
                    $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
                }  
            } else{
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }           
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);            
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Getvideoquizworkisheets
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : Get all video details
    //In Params : Void
    //Return : json
    //Date : 6th August 2018
    //###############################################################
    public function Getvideoquizzworksheetdetails(Request $request)
    {
        try {
            $result=VideoCategory::with('videos','quizzes','worksheets')->get();
            if(!empty($result)){
                echo "<pre>";
                print_r($result->toArray());
                exit;
            }
        } catch (\Exception $e) {
            
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, $e->getFile().':'.$e->getLine().':'.$e->getMessage(), '', '');
        }
        
    }

    //###############################################################
    //Function Name: GetallReferalData
    //Author:        Ketan Solanki <ketan@creolestudios.com>
    //Purpose:       To get all referal Data for the logged in Distributor
    //In Params:     
    //Return:        Listing of Data
    //###############################################################
    public function GetallReferalData(Request $request) {
        try {
            $Input = Input::all();
            if(Auth::guard('distributors')->check()){
                $distributorId = Auth::guard('distributors')->user()->id;
                if(isset($distributorId) && is_numeric($distributorId) && $distributorId > 0){
                    if(!array_key_exists('getDataFor', $Input)){
                        $allReferalData = DistributorsReferrals::select("*")
                        ->where('distributors_id',$distributorId)
                        /*->whereHas('referal_user', function ($query){                    
                                $query->where('is_ignite',1);                    
                        })*/
                        // ->whereHas('distributor_member', function ($query) use ($distributorId){                    
                        //         $query->whereHas('distributor', function ($query) use ($distributorId){
                        //             $query->where('id',$distributorId);                    
                        //         });                    
                        // })
                        ->with('distributor_member')
                        ->with([
                            'referal_user' => function ($query) {
                                $query->select("*",DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as joining_date"))->get();
                            },
                        ])
                        ->orderBy("created_at","DESC");
                    } else {
                        $allReferalData = DistributorsReferrals::select("*")
                        ->where('distributor_team_member_id',$Input['getDataFor'])
                        ->with('distributor_member')
                        ->with([
                            'referal_user' => function ($query) {
                                $query->select("*",DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as joining_date"))->get();
                            },
                        ])
                        ->orderBy("created_at","DESC");
                    }

                    if(array_key_exists('searchby', $Input)&&$Input['searchby']!=''){
                        $allReferalData = $allReferalData->whereHas('referal_user', function ($query) use($Input) {
                            $query->where('is_ignite',1)->where(function ($query) use($Input){
                                $query->where('first_name','like','%'.$Input['searchby'].'%')->orWhere('last_name','like','%'.$Input['searchby'].'%')->orWhere('contact','like','%'.$Input['searchby'].'%')->orWhere('email_address','like','%'.$Input['searchby'].'%');
                            });
                        });
                    } else {
                        $allReferalData = $allReferalData->whereHas('referal_user', function ($query){
                            $query->where('is_ignite',1);
                        });
                    }
                    $allReferalData = $allReferalData->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))->toArray();
                    if (!empty($allReferalData)) {
                        $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $allReferalData);
                    } else {
                        $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
                    }
                } else {
                    $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
                }
            } else {
                if(Auth::guard('distributor_team_members')->check()){
                    $teamMemberId = Auth::guard('distributor_team_members')->user()->id;
                    if(isset($teamMemberId) && is_numeric($teamMemberId) && $teamMemberId > 0){                             
                        $allReferalData = DistributorsReferrals::select("*")
                        ->where('distributor_team_member_id',$teamMemberId)
                        ->with([
                            'referal_user' => function ($query) {
                                $query->select("*",DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as joining_date"))->get();
                            },
                        ]) 
                        ->with('distributor_member')  
                        ->orderBy("created_at","DESC");
                        if(array_key_exists('searchby', $Input)){
                            $allReferalData = $allReferalData->whereHas('referal_user', function ($query) use($Input) {                    
                                $query->where('is_ignite',1)->where(function ($query) use($Input){                    
                                    $query->where('first_name','like','%'.$Input['searchby'].'%')->orWhere('last_name','like','%'.$Input['searchby'].'%')->orWhere('contact','like','%'.$Input['searchby'].'%')->orWhere('email_address','like','%'.$Input['searchby'].'%');
                                });                    
                            });
                        } else {
                            $allReferalData = $allReferalData->whereHas('referal_user', function ($query) {
                                $query->where('is_ignite',1);                    
                            });
                        }
                        $allReferalData = $allReferalData->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))->toArray();
                        if (!empty($allReferalData)) {
                            $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $allReferalData);
                        } else {
                            $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
                        }
                    }else{
                        $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
                    }
                }
            }
            if(!Auth::guard('distributor_team_members')->check()&&!Auth::guard('distributors')->check()){
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);            
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Deleteteammember
    //Author        : Senil Shah <senil@creolestudios.com>
    //Purpose       : To delete distributor's team member
    //In Params     :
    //Return        : Json Encoded Data
    //Date          : 22nd August 2018
    //###############################################################
    public function Deleteteammember(Request $request)
    {
        try {        
            if (Auth::guard('distributors')) {
                \DB::beginTransaction();
                $Input = Input::all();
                $deleteTeamMember = DistributorsTeamMembers::where('id',$Input['id'])->update(['status'=>3]);
                $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1);
                \DB::commit();
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, $e->getFile().':'.$e->getLine().':'.$e->getMessage(), 0);
        }
        return response()->json($responseArray); 
    }

    //###############################################################
    //Function Name : Updatedistributorninfo
    //Author        : Senil Shah <senil@creolestudios.com>
    //Purpose       : To update distributor's info
    //In Params     :
    //Return        : Json Encoded Data
    //Date          : 22nd August 2018
    //###############################################################
    public function Updatedistributorninfo(Request $request)
    {
        try {
            if ( Auth::guard('distributors')||Auth::guard('distributor_team_members')||Auth::check() ) {
                \DB::beginTransaction();
                $Input = Input::all();
                if(!isset($Input['email_address']))
                    throw new \Exception($Input['language']=='en'?"Email Address is required":'请添加 “电邮“');
                if(array_key_exists('dob', $Input)){
                    if(!isset($Input['dob']))
                        unset($Input['dob']);
                    else
                        $Input['dob'] = Carbon::createFromFormat('d/m/Y',$Input['dob']);
                }
                if(array_key_exists('image', $Input)){
                    if (!empty($Input['image']) && !is_string($Input['image'])) {
                        if ($Input['image']->getClientSize() <= UtilityController::Getmessage('FILE_SIZE_2')) {
                            $fileName  = 
                            $extension = \File::extension($Input['image']->getClientOriginalName());
                            if (in_array($extension, array('jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG'))) {
                                $file            = $Input['image']->getMimeType();
                                $imageName       = date("dmYHis").rand();
                                if($Input['updateFor']=='Distributors'||$Input['updateFor']=='DistributorsTeamMembers')
                                    $DestinationPath = public_path().config('constants.path.DISTRIBUTOR_PROFILE_IMAGE_PATH');
                                else
                                    $DestinationPath = public_path().config('constants.path.USER_PROFILE_PATH');
                                /*if (request('x1') && request('y1') && request('w') && request('h') == 0) {
                                $w = $h = 290;
                                $x = $y = 294;
                                } else {
                                $x = (int) request('x1');
                                $y = (int) request('y1');
                                $w = (int) request('w');
                                $h = (int) request('h');
                                }
                                Image::make(Input::file('image'))->crop($w, $h, $x, $y)->resize(300, 300, function ($constraint) {*/
                                Image::make(Input::file('image'))->resize(300, 300, function ($constraint) {
                                $constraint->aspectRatio();
                                $constraint->upsize();
                                })->save($DestinationPath . $imageName);
                                $Input['image'] = $imageName;
                            } else {
                                throw new \Exception(UtilityController::Getmessage('ONLY_JPG_PNG_JPEG'));
                            }
                        } else {
                            throw new \Exception($Input['image']->getClientOriginalName() . UtilityController::Getmessage('PROFILE_PHOTO_SIZE_LARGE'));
                        }
                    }
                }
                if($Input['updateFor']!='User'){
                    $nameSlug                  = $Input['updateFor'] == 'Distributors'?$Input['company_name']:$Input['first_name'].' '.$Input['last_name'];
                    $Input['slug']             = UtilityController::Makeslug($nameSlug, '-') . ':' . base64_encode($Input['user_id']);
                }
                // $backUrl              = $Input['updateFor']=='Distributors'?base64_encode(date("dmYHis").rand().'_'.$Input['slug'].'_'.base64_encode(1)):$Input['updateFor']=='DistributorsTeamMembers'?base64_encode(date("dmYHis").rand().'_'.$Input['slug'].'_'.base64_encode(2).'_'.base64_encode(Auth::guard('distributor_team_members')->user()->distributor_id)):'';
                if($Input['updateFor']=='Distributors')
                    $backUrl              = base64_encode(date("dmYHis").rand().'_'.$Input['slug'].'_'.base64_encode(1));
                else if($Input['updateFor']=='DistributorsTeamMembers')
                    $backUrl              = base64_encode(date("dmYHis").rand().'_'.$Input['slug'].'_'.base64_encode(2).'_'.base64_encode(Auth::guard('distributor_team_members')->user()->distributor_id));
                else
                    $backUrl              = '';

                if(isset($backUrl))
                    $backUrl = str_replace('/', '@', $backUrl);


                $backUrlLink = url('')."/ignite-signup/".$backUrl.'/'.base64_encode(3);
                $backUrlQr   = url('')."/ignite-signup/".$backUrl.'/'.base64_encode(2);

                $Input['unique_link']    = $backUrlLink;
                $Input['unique_link_qr'] = $backUrlQr;

                $fileName     = date("YmdHis").str_random(10).".png";
                if($Input['updateFor']=='Distributors')
                    QrCode::format('png')->size(500)->generate($backUrlQr, public_path().'/uploads/distributor_qr_codes/'.$fileName);
                if($Input['updateFor']=='DistributorsTeamMembers')
                    QrCode::format('png')->size(500)->generate($backUrlQr, public_path().'/uploads/distributor_qr_codes/team/'.$fileName);
                $Input['qrcode_file_name'] = $fileName;
                if($Input['updateFor']=='User')
                    $Input['video_preference'] = base64_encode($Input['see_videos_for'].':'.$Input['zone_language_dropdown']);
                $result               = UtilityController::Makemodelobject($Input,$Input['updateFor'],'',$Input['user_id']);

                if($Input['updateFor']=='User')
                    $result['userType'] = 'ignite_user';
                elseif ($Input['updateFor']=='Distributors')
                    $result['userType'] = 'distributor';
                else
                    $result['userType'] = 'distributor_team_member';
                if($Input['updateFor']!='DistributorsTeamMembers'&&$result['dob']!='')
                    $result['dob'] = Carbon::parse($result['dob'])->format('d/m/Y');
                $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1,$result);
                \DB::commit();
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2,$Input['updateFor']);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, $e->getFile().':'.$e->getLine().':'.$e->getMessage(), 0);
        }
        return response()->json($responseArray); 
    }

    //###############################################################
    //Function Name : Updatedistributorpassword
    //Author        : Senil Shah <senil@creolestudios.com>
    //Purpose       : To update distributor's password
    //In Params     :
    //Return        : Json Encoded Data
    //Date          : 23rd August 2018
    //###############################################################
    public function Updatedistributorpassword(Request $request)
    {
        try {        
            if (Auth::guard('distributors')||Auth::guard('distributor_team_members')||Auth::check() ) {
                \DB::beginTransaction();
                $Input = Input::all();
                $returnMessage = $Input['language']=='en'?'PASSWORD_CHANGES_SUCCESSFULLY':'PASSWORD_CHANGES_SUCCESSFULLY_CHINESE';
                if($Input['updateFor']=='Distributors'){
                    if(Hash::check($Input['old_password'],Auth::guard('distributors')->user()->password)) {
                        if($Input['new_password']==$Input['confirm_password']){
                            $result = Distributors::where('id',Auth::guard('distributors')->user()->id)->update(['password'=>Hash::make($Input['new_password'])]);
                            $responseArray = UtilityController::Generateresponse(true, $returnMessage, 1,$result);
                            \DB::commit();
                        } else {
                            throw new \Exception(UtilityController::getMessage($Input['language']=='en'?'PASSWORD_AND_CONFIRM_PASSWORD_NOT_SAME':'PASSWORD_AND_CONFIRM_PASSWORD_NOT_SAME_CHINESE'));
                        }
                    } else {
                        throw new \Exception(UtilityController::getMessage($Input['language']=='en'?'PASSWORD_OLD_INCORRECT':($Input['language']=='chi'?'PASSWORD_OLD_INCORRECT_CHINESE':'PASSWORD_OLD_INCORRECT_RU')));
                    }
                }
                if($Input['updateFor']=='DistributorsTeamMembers'){
                    if(Hash::check($Input['old_password'],Auth::guard('distributor_team_members')->user()->password)) {
                        if($Input['new_password']==$Input['confirm_password']){
                            $result = DistributorsTeamMembers::where('id',Auth::guard('distributor_team_members')->user()->id)->update(['password'=>Hash::make($Input['new_password'])]);
                            $responseArray = UtilityController::Generateresponse(true, $returnMessage, 1,$result);
                            \DB::commit();
                        } else {
                            throw new \Exception(UtilityController::getMessage($Input['language']=='en'?'PASSWORD_AND_CONFIRM_PASSWORD_NOT_SAME':'PASSWORD_AND_CONFIRM_PASSWORD_NOT_SAME_CHINESE'));
                        }
                    } else {
                        throw new \Exception(UtilityController::getMessage($Input['language']=='en'?'PASSWORD_OLD_INCORRECT':($Input['language']=='chi'?'PASSWORD_OLD_INCORRECT_CHINESE':'PASSWORD_OLD_INCORRECT_RU')));
                    }
                }
                if($Input['updateFor']=='User'){
                    if(Hash::check($Input['old_password'],Auth::user()->password)) {
                        if($Input['new_password']==$Input['confirm_password']){
                            $result = User::where('id',Auth::user()->id)->update(['password'=>Hash::make($Input['new_password'])]);
                            $responseArray = UtilityController::Generateresponse(true, $returnMessage, 1,$result);
                            \DB::commit();
                        } else {
                            throw new \Exception(UtilityController::getMessage($Input['language']=='en'?'PASSWORD_AND_CONFIRM_PASSWORD_NOT_SAME':'PASSWORD_AND_CONFIRM_PASSWORD_NOT_SAME_CHINESE'));
                        }
                    } else {
                        throw new \Exception(UtilityController::getMessage($Input['language']=='en'?'PASSWORD_OLD_INCORRECT':($Input['language']=='chi'?'PASSWORD_OLD_INCORRECT_CHINESE':'PASSWORD_OLD_INCORRECT_RU')));
                    }
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2,$Input['updateFor']);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, $e->getMessage(), 0);
        }
        return response()->json($responseArray); 
    }

    //###############################################################
    //Function Name: Getallcontent
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To get all content(videos,quizes and worksheet) list
    //In Params:     category id
    //Return:        json
    //Date:          29th August 2018
    //###############################################################
    public function Getallcontent(Request $request){
        try {
            $Input = Input::all();
            $categoryId = $Input['categoryId'];
            $Input['categoryId'] = Auth::user()->video_preference;
            if($Input['categoryId']!=''){
                $Input['categoryId'] = explode(':', base64_decode($Input['categoryId']));
                $Input['language'] = $Input['categoryId'][1];
                $Input['subject_id'] = $Input['categoryId'][0];
                $Input['subject_id'] = explode('_', base64_decode($Input['subject_id']))[1];
                // print_r($categoryId);die;
                // $categoryId = 0;
                $userId = Auth::user()->id;
                $userZone = Auth::user()->zone_id;
                /*$userCountry = Auth::user()->country_id;
                $zones = Zone::where('is_subscription_allowed',0)->get()->toArray();
                $flag = false;
                foreach ($zones as $key => $zone) {
                    $countriesNotAllowed = explode(',', $zone['country_id']);
                    if(in_array($userCountry, $countriesNotAllowed)) {
                        $flag = true;
                        break;
                    }
                }
                if(!$flag) {*/
                    $subjects = Subject::pluck('zones')->toArray();
                    if(count($subjects) > 0) {
                        foreach ($subjects as $key1 => $subject) {
                            $zoneArray = explode(',', $subject);
                            if(in_array($userZone, $zoneArray)) {
                                foreach ($zoneArray as $key => $zone) {
                                    $zoneSubjects[] = $zone;
                                }
                            }
                        }
                    }
                    $zoneSubjects = array_unique($zoneSubjects);
                    foreach ($zoneSubjects as $key => $value) {
                        $zoneSubjects[$key] = (int)$value;   
                    }
                    if(isset($zoneSubjects)) {
                        $subjects = Subject::whereIn('zones', $zoneSubjects)->pluck('id')->toArray();
                    }
                    if($subjects) {
                        // $zoneSubjects = $userZone;
                        $paymentsDoneFor = MarketPlacePayment::select('video_category_id')->where('paid_by',$userId)->where('paid_for',3)->where('subscription_end_date','>=',Carbon::now()->timezone('Asia/Singapore'))->where('payment_status',1)->pluck('video_category_id');
                        if(!empty($paymentsDoneFor)){
                            $paidCategoryId = '';
                            foreach ($paymentsDoneFor as $key => $value)
                                $paidCategoryId = $paidCategoryId.','.$value;
                            $paidCategoryId = array_unique(array_filter(explode(',', $paidCategoryId)));
                        }
                        if(isset($Input['subject_id']) && is_numeric($Input['subject_id']) && $Input['subject_id'] > 0){
                            // $categoryId = $Input['categoryId'];
                            $parentCategoryId = VideoCategory::where('id',$categoryId)->first();
                            if(!is_null($parentCategoryId)) {
                                $parentCategoryId = $parentCategoryId->toArray();
                                $parentCategoryId = $parentCategoryId['parent_id']!=''?$parentCategoryId['parent_id']:0;
                            } else {
                                $parentCategoryId = 0;
                            }
                        }
                        $categoryPurchaseStatus = $parentCategoryId==0?(in_array($categoryId, $paidCategoryId)?$categoryPurchaseStatus = 1:0):(in_array($parentCategoryId, $paidCategoryId)?$categoryPurchaseStatus = 1:0);
                        $videoData = $quizData = $worksheetData = collect();
                        // $getVideoIds = ZoneVideo::where('zone_id',$userZone)->get()->pluck('video_id');
                        // if($getVideoIds->count()>0){

                        /*$videoData = Videos::select("id", "subject_id", "title", "title_chi", "title_ru", "description", "description_chi", "description_ru", "video_category_id", "video_ordering as Order", "slug", DB::Raw('IF(slug != "", "1", "1") AS content_type'), 'thumbnail_160_90', 'thumbnail_128_72','thumbnail_web','video_status AS content_status','zone_id')
                            ->whereIn('subject_id', $subjects)
                            ->where(function ($query) use($Input,$categoryId){
                                $query->where('video_category_id',$categoryId)->orWhere('subcategory_id',$categoryId);
                            })
                            ->where('status',1)
                            ->whereHas(
                                'video_url', function($query) use($Input,$userId) {
                                    $query->where('video_language',$Input['language']);
                                }
                            )
                            ->whereHas(
                                'videoszones', function($query) use($userZone) {
                                    $query->where('zone_id',$userZone);
                                }
                            )
                            ->with(['video_url' =>function($query) use($Input,$userId){
                                    $query->select('*')->where('video_language',$Input['language'])->with([
                                        'views_detail' => function($query) use($userId){
                                            $query->select('*')->where('user_id',$userId);
                                        }
                                    ]);
                                }
                            ])->get();*/

                        // }
                        // $getQuizIds = ZoneQuiz::where('zone_id',$userZone)->get()->pluck('quiz_id');
                        // if($getQuizIds->count()>0){
                        
                            /*$quizData = Quizzes::select("id","quiz_status AS content_status","title", "title_chi", "title_ru", "description", "description_chi", "description_ru", "category_id", "quiz_ordering as Order", "slug", DB::Raw('IF(slug != "", "2", "2") AS content_type'))
                                    ->where(function ($query) use($Input,$categoryId){
                                        $query->where('category_id',$categoryId)->orWhere('subcategory_id',$categoryId);
                                    })->whereHas('zones', function ($query) use ($userZone) {
                                        $query->where('zone_id', $userZone);
                                    })
                                    ->where("quiz_published_status",2)
                                    ->whereIn('subject_id', $subjects)
                                    ->withCount('quizquestions')
                                    ->with([
                                        'quiz_attempt' => function($query) use($userId){
                                            $query->where('user_id',$userId)->where('quiz_completed_status',1)->exists();
                                        },
                                        // 'quizlanguage' => function($query) use($Input,$userId){
                                        //     $query->where('language',$Input['language']);
                                        // }
                                    ])->with(['alreadyattemptedquiz'=>function($query){
                                        $query->select('quiz_id','question_id','attempted_answer','is_correct','user_id')->where('user_id',Auth::user()->id)->where('quiz_completed_status',2);
                                    }])->where("status", 1)->get();*/
                        // }
                        // $getWorksheetIds = ZoneWorksheet::where('zone_id',$userZone)->get()->pluck('worksheet_id');
                        // if($getWorksheetIds->count()>0){
                            
                            /*$worksheetData = Worksheets::select("id","title", "title_chi", "title_ru", "description", "description_chi", "description_ru", "worksheet_category_id", "worksheet_ordering as Order", "slug", DB::Raw('IF(slug != "", "3", "3") AS content_type'), 'worksheet_status AS content_status', 'file_name','worksheet_status')
                                    ->where(function ($query) use($Input,$categoryId){
                                        $query->where('worksheet_category_id',$categoryId)->orWhere('subcategory_id',$categoryId);
                                    })
                                    ->whereHas('zones', function ($query) use ($userZone) {
                                        $query->where('zone_id', $userZone);
                                    })
                                    ->whereIn('subject_id', $subjects)
                                    ->with([
                                        'category'=>function($query){
                                            $query->select('id','category_name');
                                        },
                                        // 'worksheetlanguage' => function($query) use($Input,$userId){
                                        //     $query->where('language',$Input['language']);
                                        // }
                                    ])->where("status",1)->get();*/
                                    
                        // }

                            $videoData = Videos::select("id", "subject_id", "title", "title_chi", "title_ru", "description", "description_chi", "description_ru", "video_category_id", "video_ordering as Order", "slug", DB::Raw('IF(slug != "", "1", "1") AS content_type'), 'thumbnail_160_90', 'thumbnail_128_72','thumbnail_web','video_status AS content_status')
                                ->whereHas(
                                    'video_url', function($query) use($Input,$userId) {
                                        $query->where('video_language',$Input['language']);
                                    }
                                )
                                ->with(['video_url' =>function($query) use($Input,$userId){
                                        $query->select('*')->where('video_language',$Input['language'])->with([
                                            'views_detail' => function($query) use($userId){
                                                $query->select('*')->where('user_id',$userId);
                                            }
                                        ]);
                                    }
                                ])
                                ->whereIn('subject_id', $subjects)
                                ->where('video_category_id',$categoryId)
                                ->orWhere('subcategory_id',$categoryId)
                                ->where('status',1)
                                ->get();
                        // }
                        // $getQuizIds = ZoneQuiz::where('zone_id',$userZone)->get()->pluck('quiz_id');
                        // if($getQuizIds->count()>0){
                        
                            $quizData = Quizzes::select("id","quiz_status AS content_status","title", "title_chi", "title_ru", "description", "description_chi", "description_ru", "category_id", "quiz_ordering as Order", "slug", DB::Raw('IF(slug != "", "2", "2") AS content_type'))
                                    ->where(function ($query) use($Input,$categoryId){
                                        $query->where('category_id',$categoryId)->orWhere('subcategory_id',$categoryId);
                                    })
                                    ->where("quiz_published_status",2)
                                    ->whereIn('subject_id', $subjects)
                                    ->withCount('quizquestions')
                                    ->with([
                                        'quiz_attempt' => function($query) use($userId){
                                            $query->where('user_id',$userId)->where('quiz_completed_status',1)->exists();
                                        },
                                        // 'quizlanguage' => function($query) use($Input,$userId){
                                        //     $query->where('language',$Input['language']);
                                        // }
                                    ])->with(['alreadyattemptedquiz'=>function($query){
                                        $query->select('quiz_id','question_id','attempted_answer','is_correct','user_id')->where('user_id',Auth::user()->id)->where('quiz_completed_status',2);
                                    }])->where("status", 1)->get();
                        // }
                        // $getWorksheetIds = ZoneWorksheet::where('zone_id',$userZone)->get()->pluck('worksheet_id');
                        // if($getWorksheetIds->count()>0){
                            $worksheetData = Worksheets::select("id","title", "title_chi", "title_ru", "description", "description_chi", "description_ru", "worksheet_category_id", "worksheet_ordering as Order", "slug", DB::Raw('IF(slug != "", "3", "3") AS content_type'), 'worksheet_status AS content_status', 'file_name','worksheet_status','thumbnail_web','download_button_text')
                                    ->where(function ($query) use($Input,$categoryId){
                                        $query->where('worksheet_category_id',$categoryId)->orWhere('subcategory_id',$categoryId);
                                    })
                                    ->whereIn('subject_id', $subjects)
                                    ->with([
                                        'category'=>function($query){
                                            $query->select('id','category_name');
                                        },
                                        // 'worksheetlanguage' => function($query) use($Input,$userId){
                                        //     $query->where('language',$Input['language']);
                                        // }
                                    ])->where("status",1)->get();
                                    
                        // }

                        $mergedArray = array_merge($videoData->toArray(), $quizData->toArray(), $worksheetData->toArray());
                        $sortedArray = collect($mergedArray)->sortBy('Order')->values()->toArray();
                        $listing['category_purchase_status'] = $categoryPurchaseStatus;
                        $listing['allData'] = self::GenerateListingArray($sortedArray,$categoryPurchaseStatus);
                        $listing['selectedCategory'] = VideoCategory::where('id',$categoryId)->get();
                        if(!array_key_exists('status', $listing['allData'])){
                            if(!empty($listing['allData'])) {
                                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $listing);
                            } else {
                                $emptyCategory['topic'] = VideoCategory::where('id',$categoryId)->get()->toArray();
                                $returnData = UtilityController::Generateresponsewithmessage(false, ['en'=>'NO_DATA','chi'=>'NO_DATA_CHI','ru'=>'NO_DATA_RU'], $emptyCategory);
                            }
                        } else
                            throw new \Exception($listing['allData']['message']);
                    } else {
                        $returnData = UtilityController::Generateresponse(false, 'NO_SUBJECTS', 200);
                    }
                /*} else {
                    $returnData = UtilityController::Generateresponse(false, 'Your Country Doesn\'t have access.' , Response::HTTP_BAD_REQUEST);    
                }*/
            } else {
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', Response::HTTP_ERROR);    
            }
        } catch (Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getFile().':'.$e->getLine().':'.$e->getMessage(), '', '');
        }
        return $returnData;
    }

    //###############################################################
    //Function Name: GenerateListingArray
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to create an array according to purchase made and watch previous status. In short to make a particular content decide if the user can view 
    // or not. If not, return with appropriate status
    //In Params:     combined listing array of all data and purchase status for particular category
    //Return:        json
    //Date:          19th Feb, 2019
    //###############################################################
    public function GenerateListingArray($allDataArray,$categoryPurchaseStatus){     
        try {
            foreach ($allDataArray as $key =>$value) {
                if($key == 0){
                    if($value['content_status']==2){
                        $allDataArray[$key]['can_view'] = 1;
                    } elseif ($value['content_status']==1&&$categoryPurchaseStatus==1){
                        $allDataArray[$key]['can_view'] = 1;
                    } else {
                        $allDataArray[$key]['can_view'] = 0;
                    }
                    $allDataArray[$key]['watch_previous'] = 0;
                }
                if($key != 0){
                    $previousValue = $allDataArray[$key-1];

                    if($previousValue['content_type']==1) {
                        if($value['content_status']==2){
                            $allDataArray[$key]['can_view'] = ($previousValue['video_url']['views_detail'] && $previousValue['video_url']['views_detail']['video_viewing_app_status_permanent']==1?1:0);
                            if($allDataArray[$key]['can_view']==0){
                                $allDataArray[$key]['watch_previous'] = 1;
                            }
                        }
                        if($value['content_status']==1){
                            $allDataArray[$key]['can_view'] = (($previousValue['video_url']['views_detail'] && $previousValue['video_url']['views_detail']['video_viewing_app_status_permanent']==1&&$categoryPurchaseStatus==1)?1:0);
                            if($allDataArray[$key]['can_view']==0){
                                if($categoryPurchaseStatus==1)
                                    $allDataArray[$key]['watch_previous'] = 1;
                                else
                                    $allDataArray[$key]['watch_previous'] = 0;
                            }
                        }
                    }
                    if($previousValue['content_type']==2) {
                        if($value['content_status']==2){
                            $allDataArray[$key]['can_view'] = ($previousValue['quiz_attempt']?1:0);
                            if($allDataArray[$key]['can_view']==0){
                                $allDataArray[$key]['watch_previous'] = 1;
                            }
                        }
                        if($value['content_status']==1){
                            $allDataArray[$key]['can_view'] = ($previousValue['quiz_attempt']&&$categoryPurchaseStatus==1?1:0);
                            if($allDataArray[$key]['can_view']==0){
                                if($categoryPurchaseStatus==1)
                                    $allDataArray[$key]['watch_previous'] = 1;
                                else
                                    $allDataArray[$key]['watch_previous'] = 0;
                            }
                        }
                    }
                    if($previousValue['content_type']==3) {
                        if($value['content_status'] == 1) {
                            $allDataArray[$key]['can_view'] = 0;
                            if($categoryPurchaseStatus!=1)
                                $allDataArray[$key]['watch_previous'] = 0;
                            else
                                $allDataArray[$key]['watch_previous'] = 1;
                        } else if($value['content_status'] == 2) {
                            $allDataArray[$key]['can_view'] = 1;
                        }
                        /*if($previousValue['can_view']==1)
                            $allDataArray[$key]['can_view'] = 1;
                        else
                            $allDataArray[$key]['can_view'] = 0;

                        if($allDataArray[$key]['can_view']==0){
                            if($value['content_status']==1&&$categoryPurchaseStatus!=1)
                                $allDataArray[$key]['watch_previous'] = 0;
                            else
                                $allDataArray[$key]['watch_previous'] = 1;
                        }*/
                    }
                }
            }
            return $allDataArray;
        } catch (\Exception $e) {
            $returnData = UtilityController::Generateresponse(false, $e->getMessage(), 0);
            return $returnData;
        }            
    }

    //###############################################################
    //Function Name: Contentdetail
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To get content detail (videos,quizes and worksheet)
    //In Params:     content id, content type
    //Return:        json
    //Date:          29th August 2018
    //###############################################################
    public function Contentdetail(Request $request){
        try {
            $Input = Input::all();
            $id = 0;
            $userId = Auth::user()->id;
            $videoPreference = Auth::user()->video_preference;
            if($videoPreference!=''){
                $videoPreference = explode(':', base64_decode($videoPreference));
                $videoPreference = $videoPreference[1];
                if(array_key_exists('id', $Input)&&array_key_exists('content_type', $Input)){
                    $Input['id'] = explode(":", $Input['id']);
                    $Input['id'] = base64_decode($Input['id'][1]);
                    if(isset($Input['id']) && is_numeric($Input['id']) && $Input['id'] > 0){
                        $id = $Input['id'];
                    }
                    if(isset($Input['content_type']) && ($Input['content_type']=='1'||$Input['content_type']=='2'||$Input['content_type']=='3')){
                        $contentType = $Input['content_type'];
                    }

                    if($contentType=="1"){
                        repeat:
                        $detailData = Videos::select('id','video_category_id AS category_id','subcategory_id','title','title_chi','title_ru','description','description_chi','description_ru','thumbnail_160_90','thumbnail_128_72','thumbnail_web','video_ordering AS order')
                            ->with([
                                'video_url' => function ($query) use ($videoPreference, $userId) {
                                    $query->select("*", "video_language AS language")->where('video_language',$videoPreference)->with([ 'views_detail' => function($query) use ($userId){
                                            $query->select('id','video_id','video_url_id','video_viewed_duration AS resume_from','video_app_download','video_viewing_app_status AS view_status')->where('user_id',$userId)->orderBy('id','DESC')->first();
                                        }
                                    ])->get();
                                },
                            ])->where('id',$id)->first()->toArray();
                        $detailData['encoded_string'] = base64_encode('category_'.$detailData['category_id']);
                        $detailData['video_url']['video_duration'] = explode(":", $detailData['video_url']['video_duration']);
                        $detailData['video_url']['video_duration'] = ($detailData['video_url']['video_duration'][0]==00)?$detailData['video_url']['video_duration'][1].':'.$detailData['video_url']['video_duration'][2]:$detailData['video_url']['video_duration'][0].':'.$detailData['video_url']['video_duration'][1].':'.$detailData['video_url']['video_duration'][2];
                        if(!empty($detailData['video_url']['views_detail'])){
                            $splitTime = explode(':', $detailData['video_url']['views_detail']['resume_from']);
                            if(!empty(array_filter($splitTime))){
                                $hourToSeconds = $splitTime[0]*3600;
                                $minutesToSeconds = $splitTime[1]*60;
                                $seconds = $splitTime[2];
                                $detailData['video_url']['views_detail']['resume_from_seconds'] = $hourToSeconds+$minutesToSeconds+$seconds;
                            }
                        }
                        if(empty($detailData['video_url']['views_detail'])){
                            $insertVideoView['video_id'] = $detailData['video_url']['video_id'];
                            $insertVideoView['video_url_id'] = $detailData['video_url']['id'];
                            $insertVideoView['user_id'] = Auth::user()->id;
                            $insertVideoViewResult = UtilityController::Makemodelobject($insertVideoView,'VideoViews');
                            goto repeat;
                        }
                    }
                    if($contentType=="2"){
                        $detailData = Quizzes::select('id','category_id AS category_id','subcategory_id','title','title_chi', 'title_ru','description','description_chi','description_ru' ,'quiz_status','attempts_per_user','quiz_ordering AS order','slug')
                            ->with([
                                'quizquestions' => function ($query) {
                                    $query->select("id", "quiz_id","question_title", "question_image", "question_audio")->with([
                                        'quizquestionsoptions' => function ($query) {
                                            $query->select("id", "quiz_question_id","options_value AS answer", "options_type AS answer_type", DB::Raw('IF(correct_answer = 1, 1, 0) AS correct_answer'))->get();
                                        }
                                    ])->get();
                                },
                                // 'quizlanguage' => function($query) use($videoPreference){
                                //     $query->where('language',$videoPreference);
                                // }
                            ])->with(['alreadyattemptedquiz'=>function($query) use ($id){
                                    $query->select('quiz_id','question_id','attempted_answer','is_correct','user_id')->where('user_id',Auth::user()->id)->where('quiz_completed_status',2)->where('quiz_id',$id);
                                }])
                            ->where('id',$id)              
                            ->first();
                        $detailData['encoded_string'] = base64_encode('category_'.$detailData->category_id);
                        $detailData->quizquestions = $detailData->quizquestions->map(function($value, $key) {
                            if(isset($value->question_image))
                                if(file_exists(public_path().UtilityController::Getpath('QUIZ_OPTION_UPLOAD_PATH').$value->question_image))
                                    $value->question_image = UtilityController::Getpath('QUIZ_OPTION_UPLOAD_URL').$value->question_image;
                                else
                                    $value->question_image = UtilityController::Getpath('NO_IMAGE_URL');
                            if(isset($value->question_audio))
                                if(file_exists(public_path().UtilityController::Getpath('QUIZ_OPTION_UPLOAD_PATH').$value->question_audio))
                                    $value->question_audio = UtilityController::Getpath('QUIZ_OPTION_UPLOAD_URL').$value->question_audio;
                                else
                                    $value->question_audio = UtilityController::Getpath('NO_IMAGE_URL');
                            $value->quizquestionsoptions = $value->quizquestionsoptions->map(function($valueInner, $keyInner){
                                if($valueInner->answer_type==2||$valueInner->answer_type==3)
                                    if(file_exists(public_path().UtilityController::Getpath('QUIZ_OPTION_UPLOAD_PATH').$valueInner->answer))
                                        $valueInner->answer = UtilityController::Getpath('QUIZ_OPTION_UPLOAD_URL').$valueInner->answer;
                                    else
                                        $valueInner->answer = UtilityController::Getpath('NO_IMAGE_URL');
                                return $valueInner;
                            });
                            return $value;
                        });
                    }
                    if($contentType=="3"){
                        $detailData = Worksheets::select('id','worksheet_category_id','worksheet_category_id AS category_id','subcategory_id','title','title_chi','title_ru' ,'description','description_chi','description_ru' ,'file_name', 'original_file_name','worksheet_ordering AS order','download_button_text','thumbnail_web')
                            ->with([
                                'category'=>function($query) {
                                    $query->select('id','category_name');
                                },
                                // 'worksheetlanguage' => function($query) use($videoPreference){
                                //     $query->where('language',$videoPreference);
                                // }
                            ])
                            ->where('id',$id)
                            ->first();
                        $detailData['encoded_string'] = base64_encode('category_'.$detailData->category_id);
                    }
                    if(!empty($detailData)) {
                        $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $detailData);
                    } else {
                        $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
                    }
                } else {
                    throw new \Exception(UtilityController::Getmessage('ID_CONTENT_TYPE_REQUIRED'));
                    
                }
            } else {
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', Response::HTTP_PERMANENTLY_REDIRECT);    
            }
        } catch (Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: CategoryExist
    //Author:        Karan Kantesariya <karan@creolestudios.com>
    //Purpose:       To get category exist
    //In Params:     category id, subcategory id
    //Return:        json
    //Date:          21th Oct 2020
    //###############################################################
    public function CategoryExist(Request $request){
        try {
            $Input = Input::all();
            $parentCategoryId = VideoCategory::where('id',$Input['id'])->first();
            $returnData = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$parentCategoryId);
        } catch (Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: getquizdetail
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       Get quiz details to allow subscriber to attempt quiz
    //In Params:     quiz slug
    //Return:        json
    //Date:          31st Aug 2018
    //###############################################################
    public function getquizdetail(Request $request){     
        try {
            if(Auth::check()){
                $Input = Input::all();
                $quizId = base64_decode(explode(":", $Input['id'])[1]);
                $userId = Auth::user()->id;
                $alreadyAttempted = QuizAttempts::select('no_of_attempt')->where('quiz_id',$quizId)->where('user_id',$userId)->where('quiz_completed_status',1)->orderBy('id','DESC')->first();
                if($Input['isDetele']){
                    $findQuiz = QuizAttempts::where('quiz_id',$quizId)->where('user_id',$userId)->where('quiz_completed_status',2)->delete();
                }
                $resumeQuiz = QuizAttempts::select('*')->where('quiz_id',$quizId)->where('user_id',$userId)->where('quiz_completed_status',2)->get();
                $count = QuizAttempts::where('is_correct',1)->where('quiz_id',$quizId)->where('user_id',$userId)->where('quiz_completed_status',2)->get()->count();
                $attemptQuestioncount = QuizAttempts::where('quiz_id',$quizId)->where('user_id',$userId)->where('quiz_completed_status',2)->get()->count();
                if(isset($count)){
                    $detailData['quiz_count'] = $count;
                }
                if(isset($attemptQuestioncount))
                {
                    $detailData['attemptQuestioncount'] = $attemptQuestioncount;  
                }
                if($resumeQuiz->count()>0){
                    $detailData['resume_quiz'] = 1;
                }else{
                    $detailData['resume_quiz'] = 0;
                }
                if(!empty($alreadyAttempted))
                    $detailData['no_of_attempt'] = $alreadyAttempted['no_of_attempt']+1;
                else
                    $detailData['no_of_attempt'] = 1;
                $detailData['data'] = Quizzes::select('id','category_id AS category_id','title','description','quiz_status','attempts_per_user','quiz_ordering AS order','slug')
                        ->with([
                            'quizquestions' => function ($query) use ($quizId) {
                                $query->select("id", "quiz_id","question_title", "question_image", "question_audio")->with([
                                    'quizquestionsoptions' => function ($query) {
                                        $query->select("id", "quiz_question_id","options_value AS answer", "options_type AS answer_type", DB::Raw('IF(correct_answer = 1, 1, 0) AS correct_answer'))->get();
                                    }
                                ])->with(['alreadyattempted'=>function($query) use ($quizId){
                                    $query->select('quiz_id','question_id','attempted_answer','is_correct')->where('user_id',Auth::user()->id)->where('quiz_completed_status',2)->where('quiz_id',$quizId);
                                }])->orderBy('question_order','ASC')->get();
                            }
                        ])
                        ->where('slug',$Input['id'])              
                        ->first();
                    $detailData['data']['encoded_string'] = base64_encode('category_'.$detailData['data']['id']);
                    $returnData = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$detailData);
                    $detailData['data']->quizquestions = $detailData['data']->quizquestions->map(function($value, $key) {
                        $value->number_of_correct_answers = 0;
                        $value->quizquestionsoptions = $value->quizquestionsoptions->map(function($valueInner, $keyInner) use($value){
                            if($valueInner->correct_answer==1)
                                $value->number_of_correct_answers++;
                            $value->question_type = $valueInner->answer_type;
                            return $valueInner;
                        });
                        return $value;
                    });
                // } else {
                //     $detailData = Quizzes::select('category_id','slug')->where('slug',$Input['id'])->first();
                //     $returnData = UtilityController::Generateresponse(false, 'QUIZ_ALREADY_ATTEMPTED', 3, $detailData);    
                // }
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: Storeattempt
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To store the question details of answer's given for the quiz
    //In Params:     question details, answers given in array, quiz details, user id
    //Return:        json
    //Date:          6th Sept 2018
    //###############################################################
    public function Storeattempt(Request $request){     
        try {
            $Input = Input::all();
            $Input['user_id'] = Auth::user()->id;
            $attempted_answer = $Input['attempted_answer'];
            $Input['attempted_answer'] = implode(',', $Input['attempted_answer']);
            $validator = UtilityController::ValidationRules($Input, 'QuizAttempts');
            if (!$validator['status']) {
                $errorMessage = explode('.', $validator['message']);
                throw new \Exception($errorMessage['0']);
            } else {
                \DB::beginTransaction();
                $resultAttempt = UtilityController::Makemodelobject($Input,'QuizAttempts');
                $resultAttempt['unselectedAnswer'] = array_diff($Input['correct_answers_array'],$attempted_answer);
                $returnData = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $resultAttempt);
                \DB::commit();
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: Endquiz
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to terminate the quiz
    //In Params:     quiz id, quiz attempt status which is to be changed
    //Return:        json
    //Date:          6th Sept 2018
    //###############################################################
    public function Endquiz(Request $request){     
        try {
            $Input = Input::all();
            $quizId = base64_decode(explode(":", $Input['id'])[1]);
            $userId = Auth::user()->id;
            if((isset($Input['start_time']) && $Input['start_time'] != '') && (isset($Input['end_time']) && $Input['end_time'] != '')){
                $start_time = date('Y-m-d H:i:s', strtotime($Input['start_time']));
                $stop_time = date('Y-m-d H:i:s', strtotime($Input['end_time']));
                /*$eventLog = new EventLog;
                $eventLog->user_id = $userId;
                $eventLog->quiz_id = $quizId;
                $eventLog->start_time = $start_time;
                $eventLog->stop_time = $stop_time;
                $eventLog->save();*/
            }

            \DB::beginTransaction();
            $correctAnswersForThisAttempt = QuizAttempts::where('quiz_id',$quizId)->where('user_id',$userId)->where('no_of_attempt',$Input['no_of_attempt'])->where('is_correct',1)->count();
            if((isset($Input['start_time']) && $Input['start_time'] != '') && (isset($Input['end_time']) && $Input['end_time'] != '')){
                $endQuizResult  = QuizAttempts::where('quiz_id',$quizId)->where('user_id',$userId)->where('no_of_attempt',$Input['no_of_attempt'])->update(['quiz_completed_status'=>1,'score_per_attempt'=>$correctAnswersForThisAttempt,'start_time'=>$start_time,'stop_time'=>$stop_time]);
            }else{
                $endQuizResult  = QuizAttempts::where('quiz_id',$quizId)->where('user_id',$userId)->where('no_of_attempt',$Input['no_of_attempt'])->update(['quiz_completed_status'=>1,'score_per_attempt'=>$correctAnswersForThisAttempt]);
            }
            $returnData = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $endQuizResult);
            \DB::commit();
        } catch (\Exception $e) {
            print_r($e->getLine()." - ".$e->getMessage());
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    public function DownloadWorksheet(Request $request)
    {
        try{
            $Input = Input::all();
            $worksheetId = $Input['id'];
            $userId = Auth::user()->id;
            $eventLog = new EventLog;
            $eventLog->user_id = $userId;
            $eventLog->worksheet_id = $worksheetId;
            if($eventLog->save()){
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_SUCCESS', 1, ['url' => url('/'),'path'=>Config('constants.path.WORKSHEET_FILE_UPLOAD_URL')]);
            }
        }catch (\Exception $e) {
            print_r($e->getLine()." - ".$e->getMessage());
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: Getquizresult
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to get the last attempted quiz result
    //In Params:     quiz slug
    //Return:        json
    //Date:          6th Sept 2018
    //###############################################################
    public function Getquizresult(Request $request){     
        try {
            if(Auth::check()){
                $Input                      = Input::all();
                $quizId                     = base64_decode(explode(":", $Input['id'])[1]);
                $userId                     = Auth::user()->id;
                $alreadyAttempted = QuizAttempts::select('no_of_attempt')->where('quiz_id',$quizId)->where('user_id',$userId)->where('quiz_completed_status',1)->orderBy('id','DESC')->first();
                if(!empty($alreadyAttempted))
                    $alreadyAttempted = $alreadyAttempted['no_of_attempt'];
                else
                    $alreadyAttempted = 0;
                if($alreadyAttempted!=0) {
                    $resultData['attempts'] = QuizAttempts::where('quiz_id',$quizId)->where('user_id',$userId)->where('quiz_completed_status',1)->where('no_of_attempt',$alreadyAttempted)->get()->toArray();
                    $resultData['score'] = QuizAttempts::where('quiz_id',$quizId)->where('user_id',$userId)->where('quiz_completed_status',1)->where('no_of_attempt',$alreadyAttempted)->where('is_correct',1)->count();
                    
                    $resultData['quiz_details'] = Quizzes::where('id',$quizId)->first();
                    $resultData['quiz_details']['encoded_string'] = $resultData['quiz_details']['subcategory_id']==''?base64_encode('category_'.$resultData['quiz_details']['category_id']):base64_encode('category_'.$resultData['quiz_details']['subcategory_id']);

                    $resultData['score_per_attempts'] = QuizAttempts::select('no_of_attempt','score_per_attempt')->where('quiz_id',$quizId)
                    ->where('user_id',$userId)
                    ->where('quiz_completed_status',1)
                    ->where('no_of_attempt','!=',$alreadyAttempted)
                    ->distinct()->groupBy('no_of_attempt')
                    ->get()->toArray();
                    if(!empty($resultData['attempts']))
                        $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $resultData);
                    else
                        $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 3);
                } else {
                    $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 3);    
                }
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getFile().'-:-'.$e->getLine().'-:-'.$e->getMessage(), '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: GetallPerformanceData
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To get subscribers's performace page data
    //In Params:     
    //Return:        json
    //Date:          7th Sept 2018
    //###############################################################
    public function GetallPerformanceData(Request $request){     
        try {
            if(Auth::check()){
                $Input             = Input::all();
                $userId            = Auth::user()->id;

                $userZoneId = Auth::user()->zone_id;
                $subject = Subject::all();
                foreach($subject as $sub) {
                    $zones = explode(',', $sub['zones']);
                    if(in_array($userZoneId, $zones)) {
                        $subjectId[] = $sub['id'];
                    }
                }
                $subjectId = Subject::where('status',1)->whereIn('id', $subjectId)->pluck('id')->toArray();
                $quizId    = Quizzes::whereIn('subject_id', $subjectId)->pluck('id')->toArray();
                $videoId   = Videos::whereIn('subject_id', $subjectId)->pluck('id')->toArray();

                $data['quizCount'] = QuizAttempts::whereIn('quiz_id', $quizId)->where('user_id',$userId)->where('quiz_completed_status',1)->distinct()->groupBy('quiz_id')->pluck('quiz_id');

                $data['attemptedQuiz'] = Quizzes::withCount('quizquestions','total_questions')->with(['subcategoryWithOnlyName','categoryWithOnlyName',
                    'quiz_attempt'=>function($query) use($userId, $quizId){
                        $query->select('user_id','quiz_id','created_at','no_of_attempt','score_per_attempt')
                        ->where('user_id',Auth::user()->id)
                        ->whereIn('quiz_id', $quizId)
                        ->orderBy('created_at','Desc');
                    },
                    'quiz_attempt_highest'=>function($query) use($userId){
                        $query->select('quiz_id','no_of_attempt','score_per_attempt')
                        ->where('user_id',Auth::user()->id)
                        ->orderBy('created_at','Desc');
                    },
                ])
                ->whereIn('id',$data['quizCount'])
                ->orderBy('id','DESC')
                ->get();
                
                $data['videoCount'] = VideoViews::whereIn('video_id', $videoId)->where('user_id',$userId)->where('video_viewing_app_status',1)->distinct()->groupBy('video_id')->get()->count();

                $data['viewedVideos'] = VideoViews::whereIn('video_id', $videoId)->with('video_details_with_category')->where('user_id',$userId)->where('video_viewing_app_status',1)->orderBy('updated_at','DESC')->get();
                
                $data['totalVideoQuizCount'] = $data['videoCount'] + $data['quizCount']->count();
                $data['quizResultArray']     = 0;
                $data['attemptedQuiz']       = $data['attemptedQuiz']->map(function($value, $key) use($data){
                    if(Carbon::now()->diffInHours($value->quiz_attempt->created_at) > 24){  
                        $value->attempted_at = Carbon::now()->diffInHours($value->quiz_attempt->created_at);
                        $value->attempted_at = Carbon::now()->subHours($value->attempted_at)->diffForHumans();
                    } else {
                        $value->attempted_at = Carbon::now()->diffInMinutes($value->quiz_attempt->created_at);
                        $value->attempted_at = Carbon::now()->subMinutes($value->attempted_at)->diffForHumans();
                    }
                    $value->correct_answers_count = 0;
                    $value->total_attempts_count = 0;
                    $value->forSort = $value->quiz_attempt->created_at;
                    $value->quiz_attempt_highest = $value->quiz_attempt_highest->toArray();
                    foreach ($value->quiz_attempt_highest as $keyInner => $valueInner) {
                        if($valueInner['score_per_attempt']>$value->correct_answers_count)
                            $value->correct_answers_count = $valueInner['score_per_attempt'];
                        if($valueInner['no_of_attempt']>$value->total_attempts_count)
                            $value->total_attempts_count = $valueInner['no_of_attempt'];
                    }
                    unset($value->quiz_attempt_highest);
                    $quizResult = ($value->correct_answers_count/$value->quizquestions_count)*100;
                    $value['quizResultArray'] = $quizResult;
                    $timezone = Auth::user()->timezone;
                    $value->quiz_attempt['created_at'] = Carbon::parse($value->quiz_attempt['created_at'])->timezone($timezone);
                    // $value->updated_at = Carbon::parse($value->updated_at)->timezone($timezone);
                    return $value;
                });
                $data['attemptedQuiz'] = $data['attemptedQuiz']->sortByDesc('forSort')->values();
                $data['attemptedQuiz']       = $data['attemptedQuiz']->map(function($value, $key) use($data){
                    unset($value->forSort);
                    return $value;
                });
                $averagePercentage = $data['attemptedQuiz'];
                $data['quizResultArray'] = $averagePercentage->map(function($value, $key) use($data){
                    $data['quizResultArray'] += $value->quizResultArray;
                    return $data['quizResultArray'];
                });
                if(array_sum($data['quizResultArray']->toArray())>0){
                    $data['averageQuizPerformance'] = array_sum($data['quizResultArray']->toArray())/$data['quizCount']->count();
                    $data['averageQuizPerformance'] = (is_float($data['averageQuizPerformance'])?round( $data['averageQuizPerformance'], 1, PHP_ROUND_HALF_UP):$data['averageQuizPerformance']);
                } else {
                    $data['averageQuizPerformance'] = 0;
                }
                unset($data['quizResultArray']);
                $data['viewedVideos'] = $data['viewedVideos']->map(function($value, $key){
                    $value->isSubCat = (isset($value->video_details_with_category->subcategory_with_only_name->subcategory_name)) ? 1 : 0;
                        
                    if(Carbon::now()->diffInHours($value->updated_at) > 24){  
                        $value->viewed_at = Carbon::now()->diffInHours($value->updated_at);
                        $value->viewed_at = Carbon::now()->subHours($value->viewed_at)->diffForHumans();
                    } else {
                        $value->viewed_at = Carbon::now()->diffInMinutes($value->updated_at);
                        $value->viewed_at = Carbon::now()->subMinutes($value->viewed_at)->diffForHumans();
                    }
                    $timezone = Auth::user()->timezone;
                    $value->created_at = Carbon::parse($value->created_at)->timezone($timezone);
                    $value->updated_at = Carbon::parse($value->updated_at)->timezone($timezone);
                    return $value;
                });
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $data);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            print($e->getMessage());die;
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getFile().'-:-'.$e->getLine().'-:-'.$e->getMessage(), '', '');
        }            
        return response($returnData);
    }


    //###############################################################
    //Function Name : Ignitepayemnt
    //Author : Nivedita <nivedita@creolestudios.com>
    //Purpose : To add payment data in table
    //In Params : Void
    //Return : json
    //Date : 13th Sept 2018
    //###############################################################
    public function Ignitepayemnt(Request $request) {
        try {
            if(Auth::check()){
                $Input = Input::all();
                $userId = Auth::user()->id;
                \DB::beginTransaction();
                //Code to insert Running transaction number starts here
                $runningTransactionNumber           = UtilityController::GenerateRunningNumber('MarketPlacePayment','system_transaction_number',"VL");                
                $Input['system_transaction_number'] = $runningTransactionNumber;
                $Input['paid_by']                   = $userId;
                $Input['subscription_end_date']     = Carbon::now()->addYears(1);
                if(!empty($Input['video_category_id']))
                    $getBuzz = VideoCategory::whereIn('id', $Input['video_category_id'])->pluck('category_name')->toArray();

                if(!empty($getBuzz))
                    $Input['video_category_name'] = implode(', ', $getBuzz);

                if(is_array($Input['video_category_id']))
                    $Input['video_category_id'] = implode(',', $Input['video_category_id']);

                $current_domain = UtilityController::Getmessage('CURRENT_DOMAIN'); 
                if($current_domain=='cn'){
                    $Input['payment_method']=1;
                }else{
                    $Input['payment_method']=2;
                }

                $hasDistributor = DistributorsReferrals::where('user_id',$userId)->orderBy('id','DESC')->first();
                if(!empty($hasDistributor)){
                    $Input['distributor_id'] = $hasDistributor['distributors_id'];
                    $Input['distributer_team_member_id'] = $hasDistributor['distributor_team_member_id'];
                }
                if(array_key_exists('promo_code', $Input) && isset($Input['promo_code']) && isset($Input['promoStatus']) && $Input['promoStatus']==1){
                    $promoData              = PromoCode::select('id','discount_type','discount_of','code_for')->where('promo_code',$Input['promo_code'])->where('to_date','>=',Carbon::now())->where('status',1)->first();
                    $Input['discount_type'] = $promoData['discount_type'];
                    $Input['discount_of']   = $promoData['discount_of'];
                    if($promoData['code_for']==0){
                        $stringToIntArray = array_map('intval', explode(',', $Input['video_category_id']));
                        $Input['promo_code_id'] = PromoCode::select(DB::RAW('GROUP_CONCAT(id) AS ids'))->where('promo_code',$Input['promo_code'])->whereIn('video_category_id',$stringToIntArray)->where('to_date','>=',Carbon::now())->where('status',1)->first()->toArray();
                        $Input['promo_code_id'] = $Input['promo_code_id']['ids'];
                    }
                } else {
                    unset($Input['promo_code']);
                }
                $result = UtilityController::Makemodelobject($Input, 'MarketPlacePayment');
                $name         = Auth::user()->first_name.' '.Auth::user()->last_name;
                $slug['slug'] = str_slug($name, '-');
                $slug['slug'] = $slug['slug'].':'.base64_encode($result['id']);                
                $resultUpdate = UtilityController::Makemodelobject($slug, 'MarketPlacePayment','',$result['id']);
                
                if($result){
                    \DB::commit();
                    $responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$resultUpdate);
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name: Generatecommissionsarray
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to generate an array for inserting data into commissions table for distributors
    //In Params:     comma separated categories string
    //Return:        json
    //Date:          18th Dec, 2018
    //###############################################################
    public function Generatecommissionsarray($Input,$hasDistributor,$paymentDetails){
        try {
            $commissions_category       = explode(',', $Input);
            $distributorNewCommission   = $hasDistributor['distributor']['commission_percentage'];
            $distributorRenewCommission = $hasDistributor['distributor']['renewal_commission_percentage'];
            $distributorCommissionAmount = ($paymentDetails['payment_amount']*$distributorNewCommission)/100;
            if($paymentDetails['distributer_team_member_id']!=''){
                $teamMemberInfo = DistributorsTeamMembers::where('id',$paymentDetails['distributer_team_member_id'])->first();
                $teamMemberNewCommission    = $teamMemberInfo['commission_percentage'];
                $teamMemberRenewCommission  = $teamMemberInfo['renewal_commission_percentage'];
                $TeamMemberCommissionAmount = ($distributorCommissionAmount*$teamMemberNewCommission)/100;
            } else {
                if(!empty($hasDistributor['salesperson'])){
                    $teamMemberNewCommission    = $hasDistributor['salesperson']['commission_percentage'];
                    $teamMemberRenewCommission  = $hasDistributor['salesperson']['renewal_commission_percentage'];
                    $TeamMemberCommissionAmount = ($distributorCommissionAmount*$teamMemberNewCommission)/100;
                }
            }
            foreach ($commissions_category as $key => $value) {
                if($paymentDetails['distributor_id']!=''){
                    $addCommissions[$key]['distributor_id']             = $paymentDetails['distributor_id'];
                    $addCommissions[$key]['distributer_team_member_id'] = $paymentDetails['distributer_team_member_id'];
                } else {
                    $addCommissions[$key]['distributor_id']             = $hasDistributor['distributors_id'];
                    $addCommissions[$key]['distributer_team_member_id'] = $hasDistributor['distributor_team_member_id'];   
                }
                $addCommissions[$key]['distributor_commission_amount'] = $distributorCommissionAmount;
                if($paymentDetails['distributer_team_member_id']!='')
                    $addCommissions[$key]['team_member_commission_amount'] = $TeamMemberCommissionAmount;
                $addCommissions[$key]['video_category_id']          = $value;
                $addCommissions[$key]['subscription_month']         = Carbon::now()->format('m');
                $addCommissions[$key]['subscription_year']          = Carbon::now()->format('Y');
                $addCommissions[$key]['subscribers_count']          = 1;
                $addCommissions[$key]['subscription_type']          = 1;
                $addCommissions[$key]['created_at']                 = Carbon::now();
                $addCommissions[$key]['updated_at']                 = Carbon::now();
            }
            if(!empty($addCommissions))
                $returnData = $addCommissions;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return $returnData;
    }

    //###############################################################
    //Function Name : Paymentinfodata
    //Author : Nivedita Mitra <nivedita@creolestudios.com>
    //Purpose : To get Information of the payment data to send to paypal/alipay form submit
    //In Params : Void
    //Return : json
    //Date : 14th Sept 2018
    //###############################################################
    public function Paymentinfodata(Request $request)
    {
        try {
            \DB::beginTransaction();
            $input        = Input::all();
            $paymentsId   = substr($input['payment_id'], strpos($input['payment_id'], ":") + 1);
            $paymentsId   = base64_decode($paymentsId);

            $paymentsData = MarketPlacePayment::where('id', $paymentsId)->with('projectpaymentuserdetailinfo')->get();
            
            $paymentsData['domain'] = UtilityController::Getmessage('CURRENT_DOMAIN'); 
            $paymentsData['invoice'] = $paymentsData[0]['id'].'_'.rand();
            /*echo("<pre>");print_r($paymentsData->toArray());echo("</pre>");
            exit();*/
            if ($paymentsData) { 
                \DB::commit();
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $paymentsData);
                return $returnData;
            }
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    public function Paymepaymentdata(Request $request)
    {
        try {
            //\DB::beginTransaction();
            $paymentsData['domain'] = UtilityController::Getmessage('CURRENT_DOMAIN'); 
            $paymentsData['invoice'] = rand();
            //if ($paymentsData) { 
                //\DB::commit();
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $paymentsData);
                return $returnData;
            //}
        } catch (Exception $e) {
            //\DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Paypalpaymentipndata
    //Author : Nivedita <nivedita@creolestudios.com>
    //Purpose : Get the IPN data returned from paypal 
    //In Params : Void
    //Return :
    //Date : 14th Sept 2018
    //###############################################################
    public function Paypalpaymentipndata(Request $request){

        
        try {
            $data = Input::all();
            /*echo("<pre>");print_r($data);echo("</pre>");
            exit();*/
            \DB::beginTransaction();

            if(!empty($data) && isset($data['txn_id'])){
                $getInvoice = explode('_', $data['invoice']);
                $mpPaymentId          = $getInvoice[0];
                $transactionReference = $data['txn_id'];
                if($data['payment_status'] == 'Completed')
                    $paymentStatus = 1;
                else if($data['payment_status'] == 'Failed')
                    $paymentStatus = 3; 

                $updateData                          = array();
                $updateData['transaction_reference'] = $transactionReference;
                $updateData['payment_status']        = $paymentStatus;
                $updateDataResult                    = UtilityController::Makemodelobject($updateData, 'MarketPlacePayment', '', $mpPaymentId);
                self::Afterpaymentprocess($mpPaymentId);
            }

            if($updateDataResult){
                \DB::commit();
                $responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$updateDataResult);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Returnalipayurldetails
    //Author : Nivedita <nivedita@creolestudios.com>
    //Purpose : To store the data that alipay will return after payment
    //In Params : Void
    //Return :
    //Date : 14th Sept 2018
    //###############################################################
    public function Returnalipayurldetails(Request $request)
    {
        try {
            $input = Input::all();
           
            \DB::beginTransaction();

            if(!empty($input)){
                $outTradeNo = explode('-', $input['out_trade_no']);

                $paymentId          = $outTradeNo[0];
                                
                $updateData                        = array();
                $updateData['alipay_out_trade_no'] = $input['out_trade_no'];
                $updateData['alipay_trade_no']     = $input['trade_no'];
                $updateData['payment_status']      = 1;
                $updateData['alipay_timestamp']    = $input['timestamp'];
                $updateDataResult                  = UtilityController::Makemodelobject($updateData, 'MarketPlacePayment', '', $paymentId);

                self::Afterpaymentprocess($paymentId);

            }
            
            if($updateDataResult){
                \DB::commit();
                return redirect(url('/') . '/my-subscription');
                //$responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$updateDataResult);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
            return response()->json($responseArray);
        }
        
    }

    //###############################################################
    //Function Name : Afterpaymentprocess
    //Author : Nivedita <nivedita@creolestudios.com>
    //Purpose : Ater payment process emails should be sent
    //In Params : Void
    //Return :
    //Date : 14th Sept 2018
    //###############################################################
    public function Afterpaymentprocess($mpPaymentId){
        
        $paymentDetails = MarketPlacePayment::select('*')->where('id', $mpPaymentId)->with('projectpaymentuserdetailinfo')->first();
        //$category_name = VideoCategory::select('category_name','category_price')->whereIn('id',$paymentDetails['category_id'])->get()->toArray();
        $cat['category_id'] = explode(',', $paymentDetails['video_category_id']);
        $category_name['categories'] = VideoCategory::whereIn('id',$cat['category_id'])->with('subject')->get()->toArray();
        if($paymentDetails['promo_code']!=''){
            $promoData = PromoCode::where('promo_code',$paymentDetails['promo_code'])->where('to_date','>=',Carbon::now())->where('status',1)->first();
            
            if($promoData['code_for']==1||$promoData['code_for']==2){
                $checkIfdistributorExists = DistributorsReferrals::where('user_id',$paymentDetails['paid_by'])->first();
                if(empty($checkIfdistributorExists)){
                    if($promoData['code_for']==2)
                        $addReferal['distributor_team_member_id'] = $promoData['distributor_team_member_id'];
                    $addReferal['distributors_id'] = $promoData['distributor_id'];
                    $addReferal['user_id'] = $paymentDetails['paid_by'];
                    $addReferal['referal_through'] = $promoData['code_for']==2?4:1;
                    $addReferalResult = UtilityController::Makemodelobject($addReferal,'DistributorsReferrals');
                }
            }

            $hasDistributor = DistributorsReferrals::with('distributor','salesperson')->where('user_id',$paymentDetails['paid_by'])->orderBy('id','DESC')->first();
            if(!empty($hasDistributor)){
                $addCommissions = self::Generatecommissionsarray($paymentDetails['video_category_id'],$hasDistributor,$paymentDetails);
                IgniteCommissions::insert($addCommissions);
            }
            
            if($promoData['code_for']==0){
                /*$promoCountLimitedInc  = PromoCode::whereIn('id',explode(',', $paymentResult['promo_code_id']))->where('redemption_type',0)->increment('redeemed_count');*/
                $promoCount = PromoCode::whereIn('id',explode(',', $paymentResult['promo_code_id']))->where('to_date','>=',Carbon::now())->where('status',1)->increment('redeemed_count');
            } else {
                $promoCount = PromoCode::whereIn('promo_code',$Input['promo_code'])->where('to_date','>=',Carbon::now())->where('status',1)->increment('redeemed_count');
            }
            $category_name['isDiscount'] = 1;
            $category_name['promoCode'] = $paymentResult['promo_code'];
        } else
            $category_name['isDiscount'] = 0;

        $userFullName = $paymentDetails['projectpaymentuserdetailinfo']['first_name'] . $paymentDetails['projectpaymentuserdetailinfo']['last_name'];
        
        $email_address = $paymentDetails['projectpaymentuserdetailinfo']['email_address'];

        // $info['pathToFile']    = self::Receiptattachement($paymentDetails['payment_amount'],$email_address,$paymentDetails['projectpaymentuserdetailinfo']['first_name'],$paymentDetails['projectpaymentuserdetailinfo']['last_name'],$hasDistributor,$paymentDetails['subscription_end_date'],$paymentDetails['system_transaction_number'],$category_name); 
        $info['pathToFile']    = self::Receiptattachement($paymentDetails['payment_amount'],$email_address,$paymentDetails['projectpaymentuserdetailinfo']['first_name'],$paymentDetails['projectpaymentuserdetailinfo']['last_name'],$hasDistributor,Carbon::now(),$paymentDetails['system_transaction_number'],$category_name); 
        
        $info['email_address'] = $email_address;
        $info['current_language'] = $paymentDetails['projectpaymentuserdetailinfo']['current_language'];
        
        $info['subject']=$info['current_language']==1?'Thank you for your payment. Enclosed is your project receipt.':($info['current_language']==2?'谢谢您的付款。 附件的是您的项目收据。':'Спасибо за оплату. Ваша квитанция прилагается.');

        $info['content']=$info['current_language']==1?'Hello <strong>'.$userFullName.' ,
                    </strong><br/><br/>
                    Thank you for your payment. Enclosed is your payment receipt.<br/><br/>':($info['current_language']==2?'你好 <strong>'.$paymentDetails['projectpaymentuserdetailinfo']['first_name'].' '.$paymentDetails['projectpaymentuserdetailinfo']['last_name'].'</strong> ,
                    <br/><br/>
                    谢谢您的付款。随函附上您的付款收据。<br/><br/><br/>':'Привет <strong>'.$userFullName.' ,
                    </strong><br/><br/>
                    Спасибо за оплату. Ваша квитанция прилагается.<br/><br/>');

        $info['footer_content']=$info['current_language']==1?'From,<br /> SixClouds':($info['current_language']==2?'六云 ':'SixClouds');

        $info['footer']=$info['current_language']==1?'SixClouds':($info['current_language']==2?'六云 ':'SixClouds');

        Mail::send('emails.email_template', $info, function ($message) use ($info) {
            $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
            $message->attach($info['pathToFile']);
            $message->to($info['email_address'])->subject($info['subject']);
        });
    }

    //###############################################################
    //Function Name : Receiptattachement
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : generate invoice of the payment
    //In Params : Void
    //Return :
    //Date : 12th Sept 2018
    //###############################################################
    public function Receiptattachement($price_type, $totalAmount, $email_address, $first_name,$last_name,$hasDistributor, $date, $tradeNo,$projectTitle='',$serviceExtra='',$current_language=1)
    {
        $current_domain = UtilityController::Getmessage('CURRENT_DOMAIN'); 
        if($current_domain=='cn'){
            $priceUnit='¥';
        }else{
            if($price_type == 1) {
                $priceUnit='$';
            } else if($price_type == 2) {
                $priceUnit='S$';
            }
        }
        if($current_language==1){
            $title      = 'Your Receipt From SixClouds';
            $invoice    = 'Invoice';
            $date_label = 'Date';
            $item       = 'Item';
            $price      = 'Price';
            $ignite     = 'IGNITE';
            $name       = $first_name.' '.$last_name;
        }else{
            $title      = '收据';
            $invoice    = '发票';
            $date_label = '日期';
            $item       = '项目';
            $price      = '价格';
            $ignite     = 'IGNITE';
            $name       = $first_name.' '.$last_name;
        }
        $date=UtilityController::Changedateformat($date, 'd/m/Y');
            
        $html='<div class="container" style="font-size:13px; height:100%;">
                    <table style="width:100%">
                        <tr>
                            <td rowspan="6"><img src="' . url('/') . '/resources/assets/images/logo-blue.png" height="60" /></td>
                            <td style="text-align:right"><strong>SixClouds Pte. Ltd.</strong></td>
                        </tr>
                        <tr>
                            <td style="text-align:right">Company Registration No:</td>
                        </tr>
                        <tr>
                            <td style="text-align:right">201720404G</td>
                        </tr>
                        <tr>
                            <td style="text-align:right">6001 Beach Road</td>
                        </tr>
                        <tr>
                            <td style="text-align:right">#09-09 Golden Mile Tower</td>
                        </tr>
                        <tr>
                            <td style="text-align:right">Singapore 199589</td>
                        </tr>
                    </table>
                    <br/>
                    <br/>
                <div class="row" style="font-size:16px; ">
                    <div class="col-md-12" align="center">
                        '.$title.'
                    </div>
                </div>
                <hr>
                <br/>
                <div class="row" style="font-size:16px;">
                    <table style="width:100%">
                        <tr>
                            <td><b> '. $name .' </b></td>
                            <td style="text-align:right"><b> '.$invoice.' : '.$tradeNo.' </b></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="text-align:right">'.$date_label.' : '. $date .'</td>
                        </tr>
                    </table>
                </div>
                <br>
                <div class="row">
                    <div class="table">
                        <table  width="100%" cellspacing="0" style="border-collapse: separate;border-spacing: 0px 5px !important; ">
                            <tr  class="col-md-12">
                                <th class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:16px; text-align:left;"><b>'.$item.'</b></th>
                                <th class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:16px; text-align:left;"></th>
                                <th class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:16px; text-align:right;"><b>'.$price.'</b></th>
                            </tr>
                            <tr  class="col-md-12">
                                <th class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:16px; text-align:left;"><b>IGNITE</b></th>
                                <th class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:16px; text-align:left;"></th>
                                <th class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:16px; text-align:right;"><b></b></th>
                            </tr>
                        
                            <tbody class="row" style="margin-top:5px;">';
                            $subTotal = 0;
                            foreach ($projectTitle['categories'] as $key => $value) {
                                if($price_type == 1) {
                                    $html.='<tr  class="col-md-12">
                                        <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:13px; text-align:left;"><b> '.$value['subject']['subject_display_name'].' '.$value['category_name'].' (1 year) </b></td>
                                        <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:13px; text-align:left;"></td>
                                        <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:13px; text-align:right;"><b> '.$priceUnit.' '.$value['category_price'].' </b></td>
                                    </tr>';
                                    $subTotal += $value['category_price'];
                                } else if($price_type == 2) {
                                    $html.='<tr  class="col-md-12">
                                        <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:13px; text-align:left;"><b> '.$value['subject']['subject_display_name'].' '.$value['category_name'].' (1 year) </b></td>
                                        <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:13px; text-align:left;"></td>
                                        <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:13px; text-align:right;"><b> '.$priceUnit.' '.$value['category_price_sgd'].' </b></td>
                                    </tr>';
                                    $subTotal += $value['category_price_sgd'];
                                }
                            }
                            if($projectTitle['isDiscount']==1){
                                $discountOf = floatval($subTotal)-floatval($totalAmount);
                                $html.= '<tr><td class="col-md-4" style="background-color: #f5f5f5;text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> Sub-Total : '.$priceUnit.' '. $subTotal.' </td></tr>';
                                $html.= '<tr><td class="col-md-4" style="background-color: #f5f5f5;text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> Code Applied : '. $projectTitle['promoCode'].' </td></tr>';
                                $html.= '<tr><td class="col-md-4" style="background-color: #f5f5f5;text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> Discount : '.$priceUnit.' '. $discountOf .' </td></tr>';
                            }
                            $html.='<tr><td class="col-md-4" style="background-color: #f5f5f5;text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> Total : '.$priceUnit.' '. $totalAmount.' </td></tr>
                            </tbody> 
                        </table>   
                    </div>
                </div>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>';
                if(!empty($hasDistributor)){
                        $html.= '<div class="row" style="position: absolute; bottom: 0; width: 100%;">
                                    <table style="width:100%">
                                        <tr>
                                            <td style="text-align:left">'.$hasDistributor->distributor->company_name.' is an authorised distributor of </td>
                                            <td style="text-align:right">Fees paid are non-refundable.</td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:left">SixClouds Pte. Ltd.</td>
                                            <td style="text-align:right">Please refer to our Terms of Service.</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>';
                } else {
                    $html.= '<div class="row" style="position: absolute; bottom: 0; width: 100%;">
                                <table style="width:100%">
                                    <tr>
                                        <td></td>
                                        <td style="text-align:right">Fees paid are non-refundable.</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td style="text-align:right">Please refer to our Terms of Service.</td>
                                    </tr>
                                </table>
                            </div>
                        </div>';
                }
        $filename   = "payment_receipt_" . $tradeNo . ".pdf";
        try{
            //$pdf = App::make('dompdf.wrapper');
            $pdf = PDF::loadHtml($html);
        }  catch (\Exception $e) {
            // $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            print_r($e->getMessage()); die;
        }  
        $finalPath = public_path() . UtilityController::Getpath('MP_RECEIPT_ATTACHMENT_UPLOAD_PATH') . $filename;
        $pdf->save($finalPath); 
        return $finalPath;
    }

    //###############################################################
    //Function Name : Mysubscriptiondata
    //Author : Nivedita <nivedita@creolestudios.com>
    //Purpose : get the data of the subscription purchased by the user
    //In Params : Void
    //Return :
    //Date : 18th Sept 2018
    //###############################################################
    public function Mysubscriptiondata(){

        try {
            if(Auth::check()){
                $todaysDate = Carbon::now()->timezone('Asia/Singapore');
                $result     = MarketPlacePayment::select('*')->with(['video_categories' => function ($query) {
                                $query->with('subject')->get();
                            },
                ])->where('paid_by', Auth::user()->id)->where('paid_for', 3)->where('payment_status', 1)->where('subscription_end_date', '>', $todaysDate)->get()->toArray();
                if($result){
                    foreach ($result as $key => $value) {
                        $date = Carbon::parse($value['created_at'])->timezone(Auth::user()->timezone);
                        $result[$key]['created_at'] = UtilityController::Changedateformat($date, 'd F, Y');
                        $result[$key]['paymentstatus'] = ($value['payment_status'] == 1 ? 'Successful' :  'Failed');
                        // $to = Carbon::createFromFormat('Y-m-d H:s:i', $value['subscription_end_date']);
                        // $from = Carbon::createFromFormat('Y-m-d H:s:i', $value['created_at']);
                        // $diff_in_months = $to->diffInMonths($from);
                        // $result[$key]['plan_duration'] = $diff_in_months;
                    }
                    // $result['date'] = MarketPlacePayment::select('*')->where('paid_by', Auth::user()->id)->where('paid_for', 3)->where('payment_status', 1)->whereRaw('DATEDIFF(subscription_end_date,created_at) = 365')->get()->toArray();
                    $responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$result);
                }
                else{
                    $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
        
    }

    //###############################################################
    //Function Name: Videoseen
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to update the status of video to seen when subscriber sees the whole videos
    //In Params:     video id
    //Return:        json
    //Date:          27 Sept, 2018
    //###############################################################
    public function Videoseen(Request $request){
        try {
            $Input              = Input::all();
            if(isset($Input['currentTime']) && $Input['currentTime'] != ''){
                $Input['currentTime'] = gmdate('H:i:s',$Input['currentTime']);                
            }
            if(isset($Input['wholeData']['video_duration']) && $Input['wholeData']['video_duration'] != ''){
                $Input['totalVideoDuration'] = $Input['wholeData']['video_duration'];
            }
            if(isset($Input['totalVideoDuration']) && $Input['totalVideoDuration'] != ''){
                if(count(explode(':', $Input['totalVideoDuration']))==2){
                    $Input['totalVideoDuration'] = '00:'.$Input['totalVideoDuration'];
                }
            }
            if(isset($Input['id']) && $Input['id']!=''){
                $videoId            = base64_decode(explode(":", $Input['id'])[1]);
                $videoUrlId         = $Input['videoUrlId'];
                $userId             = Auth::user()->id;
                if (isset($Input['wholeData']['views_detail']['id']) && $Input['wholeData']['views_detail']['id'] != '') {
                    $videoViewId = $Input['wholeData']['views_detail']['id'];                
                }
                \DB::beginTransaction();
                    $resume['video_viewed_duration']              = $Input['watchStatus']==1?'00:00:00':(isset($Input['currentTime'])?$Input['currentTime']:'00:00:00');
                    $resume['video_viewing_app_status']           = $Input['watchStatus'];
                    $resume['video_id']                           = $videoId;
                    $resume['video_url_id']                       = $videoUrlId;
                    $resume['user_id']                            = $userId;
                    if(VideoViews::where('user_id',$userId)->where('video_id',$videoId)->where('video_url_id',$videoUrlId)->where('video_viewing_app_status_permanent',1)->exists())
                        $resume['video_viewing_app_status_permanent'] = 1;
                    if($Input['watchStatus']==2 && $Input['currentTime'] < $Input['totalVideoDuration']){
                        $checkCompleteExists = VideoViews::where('user_id',$userId)->where('video_id',$videoId)->where('video_url_id',$videoUrlId)->orderBy('id','DESC')->get();
                        
                        $checkCompleteExists = $checkCompleteExists[0];

                        if((isset($Input['start_time']) && $Input['start_time'] !='') && (isset($Input['end_time']) && $Input['end_time'] !='')){
                            $VideoViewsHistory = new VideoViewsHistory;
                            $VideoViewsHistory->video_views_id = $checkCompleteExists['id'];
                            $VideoViewsHistory->start_time = date('Y-m-d H:i:s', strtotime($Input['start_time']));
                            $VideoViewsHistory->stop_time = date('Y-m-d H:i:s', strtotime($Input['end_time']));
                            $VideoViewsHistory->save();

                            $eventLog = new EventLog;
                            $eventLog->user_id = $userId;
                            $eventLog->video_id = $videoId;
                            $eventLog->start_time = date('Y-m-d H:i:s', strtotime($Input['start_time']));
                            $eventLog->stop_time = date('Y-m-d H:i:s', strtotime($Input['end_time']));
                            $eventLog->save();
                        }

                        if(!empty($checkCompleteExists)&&$checkCompleteExists['video_viewing_app_status']==1&&$checkCompleteExists['video_viewing_app_status_permanent']==1)
                            $resumeResult = UtilityController::Makemodelobject($resume,'VideoViews');
                        else{
                            unset($resume['video_id']);
                            unset($resume['video_url_id']);
                            unset($resume['user_id']);
                            $resumeResult = UtilityController::Makemodelobject($resume,'VideoViews','',$checkCompleteExists['id']);
                        }
                        $returnData = UtilityController::Generateresponse(true, 'VIDEO_SEEN', 1,$resumeResult);
                    } else 
                        $returnData = UtilityController::Generateresponse(true, 'NOTHING_TO_DO', 1);
                    if($Input['watchStatus']==1){
                        $checkCompleteExists = VideoViews::where('user_id',$userId)->where('video_id',$videoId)->where('video_url_id',$videoUrlId)->whereIn('video_viewing_app_status',[2,0])->orderBy('id','DESC')->first();
                        if(!empty($checkCompleteExists)){
                            unset($resume['video_id']);
                            unset($resume['video_url_id']);
                            unset($resume['user_id']);
                            $resume['video_viewing_app_status_permanent'] = 1;
                            $resumeResult = UtilityController::Makemodelobject($resume,'VideoViews','',$checkCompleteExists['id']);
                            //print_r($Input)
                            if((isset($Input['start_time']) && $Input['start_time'] !='') && (isset($Input['end_time']) && $Input['end_time'] !='')){
                                $VideoViewsHistory = new VideoViewsHistory;
                                $VideoViewsHistory->video_views_id = $checkCompleteExists['id'];
                                $VideoViewsHistory->start_time = date('Y-m-d H:i:s', strtotime($Input['start_time']));
                                $VideoViewsHistory->stop_time = date('Y-m-d H:i:s', strtotime($Input['end_time']));
                                $VideoViewsHistory->save();

                                $eventLog = new EventLog;
                                $eventLog->user_id = $userId;
                                $eventLog->video_id = $videoId;
                                $eventLog->start_time = date('Y-m-d H:i:s', strtotime($Input['start_time']));
                                $eventLog->stop_time = date('Y-m-d H:i:s', strtotime($Input['end_time']));
                                $eventLog->save();
                            }
                        } else {
                            $resume['video_viewing_app_status_permanent'] = 1;
                            $resumeResult = UtilityController::Makemodelobject($resume,'VideoViews');
                        }
                        $returnData = UtilityController::Generateresponse(true, 'VIDEO_SEEN', 1,$resumeResult);
                    }
                \DB::commit();
            }else{
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            }
            
        } catch (\Exception $e) {
            print_r($e->getLine());
            print_r($e->getMessage());die;
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name :   ExportSubscribers
    //Author :          Senil Shah <senil@creolestudios.com>
    //Purpose :         To export Subscrbers data
    //In Params :       Void
    //Return :          json
    //Date :            8th Oct, 2018
    //###############################################################
    public function ExportSubscribers(Request $request){
        try {
            if(Auth::guard('distributors')->check()){
                $Input['export_type'] = $request->export_type;
                $distributorId = Auth::guard('distributors')->user()->id;
                $allReferalData = DistributorsReferrals::select("*")
                ->where('distributors_id',$distributorId)
                ->whereHas('referal_user', function ($query){                    
                        $query->where('is_ignite',1);                    
                })
                // ->whereHas('distributor_member', function ($query) use ($distributorId){                    
                //         $query->whereHas('distributor', function ($query) use ($distributorId) {
                //             $query->where('id',$distributorId);                    
                //         });                    
                // })                
                ->with('distributor_member')
                ->with([
                    'referal_user' => function ($query) {
                        $query->select("*",DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as joining_date"))->get();
                    },
                ])   
                ->orderBy("created_at","DESC")->get()->toArray();
                if (!empty($allReferalData)) {
                    $dataArray[] = ['S/N', 'Users', 'Email Address', 'Contact Number', 'Salesperson', 'Mode'];
                    $dataArray = self::createExportData($allReferalData,$dataArray,1,'Ignite-Subscribers','Ignite-Subscribers','Subscribers Data',$Input['export_type']);
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $allReferalData);
                } else {
                    $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
                }
            } else if(Auth::guard('distributor_team_members')->check()){
                $Input['export_type'] = $request->export_type;
                $teamMemberId = Auth::guard('distributor_team_members')->user()->id;
                $allReferalData = DistributorsReferrals::select("*")
                ->where('distributor_team_member_id',$teamMemberId)
                ->whereHas('referal_user', function ($query){                    
                        $query->where('is_ignite',1);                    
                })            
                ->with('distributor_member')
                ->with([
                    'referal_user' => function ($query) {
                        $query->select("*",DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as joining_date"))->get();
                    },
                ])   
                ->orderBy("created_at","DESC")->get()->toArray();
                if (!empty($allReferalData)) {
                    $dataArray[] = ['S/N', 'Users', 'Email Address', 'Contact Number', 'Salesperson', 'Mode'];
                    $dataArray = self::createExportData($allReferalData,$dataArray,1,'Ignite-Subscribers','Ignite-Subscribers','Subscribers Data',$Input['export_type']);
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $allReferalData);
                } else {
                    $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
                }
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name :   ExportDistributorTeam
    //Author :          Senil Shah <senil@creolestudios.com>
    //Purpose :         To export Distributor's team data
    //In Params :       Void
    //Return :          json
    //Date :            9th Oct, 2018
    //###############################################################
    public function ExportDistributorTeam(Request $request){
        try {
            if(Auth::guard('distributors')->check()){
                $Input['export_type'] = $request->export_type;
                $distributorId = Auth::guard('distributors')->user()->id;                             
                $allTeamMembersData = DistributorsTeamMembers::with('distributor')->withCount('distributor_referrals_count')->where('distributor_id', $distributorId)->whereIn('status',[1,3])->orderBy("created_at","DESC")->get()->toArray();
                if (!empty($allTeamMembersData)) {
                    $dataArray[] = ['S/N', 'Name', 'Email Address', 'Contact Number', 'Total Users'];
                    $dataArray = self::createExportData($allTeamMembersData,$dataArray,2,'My-Team','My-Team','Team Data',$Input['export_type']);
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $allTeamMembersData);
                } else {
                    $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
                }
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name: createExportData
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To create an array from data into export sheet array
    //In Params:     original data, data to export, which foreach to user 
    //Return:        json
    //Date:          9th Oct 2018
    //###############################################################
    public function createExportData($data, $dataArray, $which, $fileName, $title, $description, $type){     
        try {
            switch ($which) {
                case 1:
                    foreach ($data as $key => $value) {
                        $tempArray = array();
                        array_push($tempArray, $key+1);
                        array_push($tempArray, $value['referal_user']['first_name']." ".$value['referal_user']['last_name']);
                        array_push($tempArray, $value['referal_user']['email_address']);
                        array_push($tempArray, $value['referal_user']['contact']);
                        if($value['distributor_member']!='')
                            array_push($tempArray, $value['distributor_member']['first_name']." ".$value['distributor_member']['last_name']);
                        else
                            array_push($tempArray, "N/A");
                        
                        if($value['referal_through'] == 1)
                            array_push($tempArray, 'Distributor Code');
                        elseif ($value['referal_through'] == 2)
                            array_push($tempArray, 'Qr Code');
                        elseif ($value['referal_through'] == 3)
                            array_push($tempArray, 'Unique URL');
                        else
                            array_push($tempArray, 'Salesperson Code');
                        
                        array_push($dataArray, $tempArray);
                    }
                    if ($type == 1) 
                        UtilityController::ExportData($dataArray, $fileName, $title, $description, "xlsx");
                    elseif ($type == 2)
                        UtilityController::ExportData($dataArray, $fileName, $title, $description, "csv");
                    break;
                case 2:
                    foreach ($data as $key => $value) {
                        $tempArray = array();
                        array_push($tempArray, $key+1);
                        array_push($tempArray, $value['first_name']." ".$value['last_name']);
                        array_push($tempArray, $value['email_address']);
                        array_push($tempArray, $value['contact']);
                        array_push($tempArray, $value['distributor_referrals_count_count']);
                        array_push($dataArray, $tempArray);
                    }
                    if ($type == 1) 
                        UtilityController::ExportData($dataArray, $fileName, $title, $description, "xlsx");
                    elseif ($type == 2)
                        UtilityController::ExportData($dataArray, $fileName, $title, $description, "csv");
                    break;
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return $dataArray;
    }

    //###############################################################
    //Function Name: GetIgniteSubscriberDetailsData
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To get all subscriber details data
    //In Params:     Slug
    //Return:        json
    //Date:          8th Oct 2018
    //###############################################################
    public function GetIgniteSubscriberDetailsData(Request $request)
    {
        try
        {
            $input = Input::all();
            $slug = ($input['slug'] != '' ? $input['slug'] : '');
            if (isset($slug) && $slug != "") {
                $distributorData = User::select("*", DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as joining_date"), DB::Raw("DATE_FORMAT(dob, '%d %M, %Y') as dob"))
                    ->with('subscriber_country', 'subscriber_state', 'subscriber_city')
                    ->where("slug", $slug)
                    ->first()
                    ->toArray();
                /*$id = base64_decode(explode(":", $slug)[1]);
                $transactions = MarketPlacePayment::select('id','promo_code','payment_method','paid_by','system_transaction_number','payment_amount','access_platform','remarks','distributor_id','distributer_team_member_id','video_category_id','video_category_name', DB::Raw("DATE_FORMAT(created_at, '%d-%b-%y') as transaction_date"))->with([
                    'video_categories' => function($query){
                        $query->select('id','category_name');
                    },
                    'distributor' => function($query){
                        $query->select('id','company_name');
                    },
                    'distributor_team_member' => function($query){
                        $query->select('id','first_name','last_name');
                    }
                ])->where('paid_by',$id)->where('paid_for',3)->get();*/
                if (!empty($distributorData)) {
                    // $data['transactions'] = $transactions;
                    $data['distributorData'] = $distributorData;
                    $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $data);
                } else {
                    $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
                }
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name: GetSubscriberTransaction
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To get subscribers transactions through particular distributor
    //In Params:     subscribers slug
    //Return:        json
    //Date:          4th Dec, 2018
    //###############################################################
    public function GetSubscriberTransaction(Request $request){     
        try {
            $Input = Input::all();
            $id = base64_decode(explode(":", $Input['slug'])[1]);
            $distributorId = Auth::guard('distributors')->user()->id;
            $transactions = MarketPlacePayment::select('id','promo_code','payment_method','paid_by','system_transaction_number','payment_amount','access_platform','remarks','distributor_id','distributer_team_member_id','video_category_id','video_category_name', DB::Raw("DATE_FORMAT(created_at, '%d-%b-%y') as transaction_date"))->with([
                'video_categories' => function($query){
                    $query->select('id','category_name');
                },
                'distributor' => function($query){
                    $query->select('id','company_name');
                },
                'distributor_team_member' => function($query){
                    $query->select('id','first_name','last_name');
                }
            ])->where('distributor_id',$distributorId)->where('paid_by',$id)->where('paid_for',3)->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))->toArray();
            $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $transactions);
         } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getMessage(), '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: GetSubscriberCurrentSubscription
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To get the current subscriptions
    //In Params:     subscriber's id
    //Return:        json
    //Date:          8th Oct 2018
    //###############################################################
    public function GetSubscriberCurrentSubscription(Request $request){
        try {
            $Input = Input::all();
            $currentSubscription = MarketPlacePayment::select('id','paid_by','video_category_id','video_category_name', DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as from_date"), DB::Raw("DATE_FORMAT(subscription_end_date, '%d %M, %Y') as to_date"))->with([
                    'video_categories' => function($query){
                        $query->select('id','category_name');
                    }
                ])->where('paid_by',$Input['paid_by'])->where('paid_for',3)->where('subscription_end_date', '>=', Carbon::now()->toDateString())->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))->toArray();
            $returnData = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $currentSubscription);
        } catch (Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }
        return response($returnData);
    }

    //###############################################################
    //Function Name: GetSubscriberHistorySubscription
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To get the past subscriptions
    //In Params:     subscriber's id
    //Return:        json
    //Date:          8th Oct 2018
    //###############################################################
    public function GetSubscriberHistorySubscription(Request $request){
        try {
            $Input = Input::all();
            $historySubscription = MarketPlacePayment::select('id','paid_by','video_category_id','video_category_name', DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as from_date"), DB::Raw("DATE_FORMAT(subscription_end_date, '%d %M, %Y') as to_date"))->with([
                    'video_categories' => function($query){
                        $query->select('id','category_name');
                    }
                ])->where('paid_by',$Input['paid_by'])->where('paid_for',3)->where('subscription_end_date', '<', Carbon::now()->toDateString())->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))->toArray();
            $returnData = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $historySubscription);
        } catch (Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }
        return response($returnData);
    }

    //###############################################################
    //Function Name: Getteammembersubscribers
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to get the team members referals/subscribers
    //In Params:     team member id
    //Return:        json
    //Date:          9th Oct, 2018
    //###############################################################
    public function Getteammembersubscribers(Request $request){     
        try {
            if(Auth::guard('distributors')->check()){
                $Input = Input::all();
                $teamMemberId = Auth::guard('distributors')->user()->id;
                $subscribers = MarketPlacePayment::select('paid_by','video_category_id', DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as from_date"), DB::Raw("DATE_FORMAT(subscription_end_date, '%d %M, %Y') as expiry_date"),'remarks')->with([
                    'user_detail' => function($query){
                        $query->select('id','display_name','email_address','contact');
                    },
                    'video_categories' => function($query){
                        $query->select('id','category_name');
                    },
                ])->where('distributer_team_member_id',$Input['team_member_id'])->where('paid_for',3)->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))->toArray();
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $subscribers);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getMessage(), '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: Sendotp
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to send the otp for phone verification
    //In Params:     country_code, phone_number
    //Return:        json
    //Date:          10th Oct 2018
    //###############################################################
    public function Sendotp(Request $request){
        try {
            if(Auth::guard('distributor_team_members')->check()){
                $Input = Input::all();
                $userId = Auth::guard('distributor_team_members')->user()->id;
                if(!array_key_exists('country_code', $Input)){
                    throw new \Exception(UtilityController::getMessage('COUNTRY_CODE_REQUIRED'));
                }
                if(!array_key_exists('phone_number', $Input)){
                    throw new \Exception(UtilityController::getMessage('PHONE_NUMBER_REQUIRED'));
                }
                if(isset($Input['phone_number']) && isset($Input['country_code'])) {
                    \DB::beginTransaction();
                        ## SMS application SDK AppID
                        $appid = UtilityController::Getmessage('APP_ID_SMS'); // Starting with 1400
                        ## SMS application SDK AppKey
                        $appkey = UtilityController::Getmessage('APP_KEY_SMS');
                        ## SMS template ID, you need to apply in the SMS application
                        $templateId = UtilityController::Getmessage('APP_TEMPLATE_ID_SMS');
                        ## signature
                        $smsSign = UtilityController::Getmessage('SMS_SIGN');
                        ## Mobile number that needs to send a text message
                        // $phoneNumbers = ["18883708501"];
                        $phoneNumber = $Input['phone_number'];
                        $countryCode  = $Input['country_code'];
                        $ssender   = new SmsSingleSender($appid, $appkey);
                        $randomOtp = rand(1000,9999);
                        $params    = [$randomOtp, "5"];
                        $returnData = $ssender->sendWithParam($countryCode, $phoneNumber, $templateId, $params, $smsSign, "", "");
                        $returnData = json_decode($returnData,true);
                        if($returnData['errmsg']=='OK'||$returnData['errmsg']=='ok') {
                            $storeOtp = DistributorsTeamMembers::where('id',$userId)->update(['otp'=>$randomOtp,'contact'=>$phoneNumber,'phonecode'=>$countryCode]);
                            if($storeOtp){
                                $returnData = UtilityController::Generateresponse(true, 'OTP_SENT', 1);
                                \DB::commit();
                            }
                        } else {
                            $returnData = UtilityController::Generateresponse(false, 'INVALID_PHONE_NUMBER', 0);
                        }
                } else {
                    $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 400, '');
                }
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: Verifyotp
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to verify the otp sent to user
    //In Params:     country code, phone number, otp
    //Return:        json
    //Date:          10th Oct, 2018
    //###############################################################
    public function Verifyotp(Request $request){     
        try {
            if(Auth::guard('distributor_team_members')->check()){
                $Input = Input::all();
                $userId = Auth::guard('distributor_team_members')->user()->id;
                $correctOtp = DistributorsTeamMembers::where('id',$userId)->where('contact',$Input['phone_number'])->where('otp',$Input['otp'])->exists();
                if($correctOtp){
                    \DB::beginTransaction();
                        $memberVerified = DistributorsTeamMembers::where('id',$userId)->where('contact',$Input['phone_number'])->where('otp',$Input['otp'])->update(['status'=>5,'otp'=>'']);
                        if($memberVerified){
                            $returnData = UtilityController::Generateresponse(true, 'VERIFIED', 1);
                        } else {
                            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
                        }
                    \DB::commit();
                } else {
                    $returnData = UtilityController::Generateresponse(false, 'INVALID_OTP', 401);
                }
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: Gettypeandyear
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to get the type and year for commissions page
    //In Params:     void
    //Return:        json
    //Date:          15th Nov, 2018
    //###############################################################
    public function Gettypeandyear(Request $request){     
        try {
            $data['years'] = array_combine(range(date("Y"), 2015), range(date("Y"), 2015));
            $data['type'] = array('0' => 'Summary','1' => 'New Sales','2' =>'Renewal Sales');
            $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $data);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }
    
    //###############################################################
    //Function Name: Getcommissions
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to get distributors and team commissions
    //In Params:     void
    //Return:        json
    //Date:          31st Oct, 2018
    //###############################################################
    public function Getcommissions(Request $request){     
        try {
            $Input = Input::all();
            if(Auth::guard('distributors')->check()||Auth::guard('distributor_team_members')->check()){
                $distributorsId = Auth::guard('distributors')->check()?Auth::guard('distributors')->user()->id:Auth::guard('distributor_team_members')->user()->id;
                if(array_key_exists('team_member', $Input)&&isset($Input['team_member'])) {
                    $teamCommission  = DistributorsTeamMembers::select('commission_percentage','renewal_commission_percentage')->where('id',$Input['team_member'])->first();
                    $distributorCommission        = $teamCommission['commission_percentage'];
                    $distributorRenewalCommission = $teamCommission['renewal_commission_percentage'];
                } else {
                    if(Auth::guard('distributors')->check()){
                        $distributorCommission        = Auth::guard('distributors')->user()->commission_percentage;
                        $distributorRenewalCommission = Auth::guard('distributors')->user()->renewal_commission_percentage;
                    }
                    if(Auth::guard('distributor_team_members')->check()){
                        $distributorCommission        = Auth::guard('distributor_team_members')->user()->commission_percentage;
                        $distributorRenewalCommission = Auth::guard('distributor_team_members')->user()->renewal_commission_percentage;
                    }
                }
                $videoCategory = VideoCategory::select('id','category_name','category_price','promotional_price')->get()->toArray();
                $monthTill = $Input['year']==Carbon::now()->format('Y')?Carbon::now()->format('m'):12;
                $static100 = 100;
                for ($i=1; $i <= $monthTill ; $i++) {
                    if($Input['type']==0)
                        $commissions[$i] = IgniteCommissions::where(Auth::guard('distributors')->check()?'distributor_id':'distributer_team_member_id',$distributorsId)->where('subscription_year',$Input['year']);
                    else{
                        $commissions[$i] = IgniteCommissions::where(Auth::guard('distributors')->check()?'distributor_id':'distributer_team_member_id',$distributorsId)->where('subscription_year',$Input['year'])->where('subscription_type',$Input['type']);
                    }
                    if(array_key_exists('team_member', $Input)&&isset($Input['team_member'])){
                        $commissions[$i] = $commissions[$i]->where('distributer_team_member_id',$Input['team_member']);
                    }
                    $commissions[$i] = $commissions[$i]->select('id','video_category_id','subscribers_count','subscription_type','subscription_month','distributor_commission_amount','team_member_commission_amount')
                        // ->with([
                        // 'video_details' => function($query) use($distributorCommission,$distributorRenewalCommission,$static100){
                        //     $query->select('id','category_name',DB::Raw("category_price*".$distributorCommission."/".$static100." as commissionPriceNew"),DB::Raw("category_price*".$distributorRenewalCommission."/".$static100." as commissionPriceRenew"));
                        // }])
                    ->where('subscription_month',$i)->get()->toArray();
                }
                $totalYearCommission = 0;
                if($Input['type']==0){
                    foreach ($commissions as $key => $value){
                        $count = 0;
                        $commissions[$key]['totalNewCount'] = 0;
                        $commissions[$key]['totalRenewCount'] = 0;
                        $commissions[$key]['totalNewCountPrice'] = 0;
                        $commissions[$key]['totalRenewCountPrice'] = 0;
                        $commissions[$key]['current_month'] = date("M", mktime(0, 0, 0, $key, 10));
                        foreach ($value as $keyInner => $valueInner){
                            // $valueInner['subscription_type']==1?$commissionsNew[$key][] = $valueInner:$commissionsRenewal[$key][] = $valueInner;
                            $valueInner['subscription_type']==1?$commissions[$key]['totalNewCount'] += $valueInner['subscribers_count']:$commissions[$key]['totalRenewCount'] += $valueInner['subscribers_count'];
                            /*$valueInner['subscription_type']==1?$commissions[$key]['totalNewCountPrice'] += $valueInner['video_details']['commissionPriceNew']:$commissions[$key]['totalRenewCountPrice'] += $valueInner['video_details']['commissionPriceRenew'];*/
                            if(array_key_exists('team_member', $Input)&&isset($Input['team_member'])){
                                $valueInner['subscription_type']==1?$commissions[$key]['totalNewCountPrice'] += $valueInner['team_member_commission_amount']:$commissions[$key]['totalRenewCountPrice'] += $valueInner['team_member_commission_amount'];
                            } else {
                                $valueInner['subscription_type']==1?$commissions[$key]['totalNewCountPrice'] += $valueInner['distributor_commission_amount']:$commissions[$key]['totalRenewCountPrice'] += $valueInner['distributor_commission_amount'];
                            }

                            $valueInner['subscription_type']==1?$commissions[$key]['totalNewCountPrice'] = round($commissions[$key]['totalNewCountPrice'],2):$commissions[$key]['totalRenewCountPrice'] = round($commissions[$key]['totalRenewCountPrice'],2);

                            // $totalYearCommission += $valueInner['subscription_type']==1?$valueInner['video_details']['commissionPriceNew']:$valueInner['video_details']['commissionPriceRenew'];
                            // $totalYearCommission = $valueInner['subscription_type']==1?$commissions[$key]['totalNewCountPrice']:$commissions[$key]['totalRenewCountPrice'];
                            if($valueInner['subscription_type']==1) {
                                $count += 1;
                            } else {
                                $count = 0;
                            }
                        }
                        if(count($value) == $count) {
                            $totalYearCommission += $commissions[$key]['totalNewCountPrice'];
                        }
                    }
                } else {
                    foreach ($commissions as $key => $value){
                        $count = 0;
                        if($Input['type']==1){
                            $commissions[$key]['totalNewCount'] = 0;
                            $commissions[$key]['totalNewCountPrice'] = 0;
                        } else {
                            $commissions[$key]['totalRenewCount'] = 0;
                            $commissions[$key]['totalRenewCountPrice'] = 0;
                        }
                        $commissions[$key]['current_month'] = date("M", mktime(0, 0, 0, $key, 10));
                        foreach ($value as $keyInner => $valueInner){
                            $Input['type']==1?$commissions[$key]['totalNewCount'] += $valueInner['subscribers_count']:$commissions[$key]['totalRenewCount'] += $valueInner['subscribers_count'];
                            if(array_key_exists('team_member', $Input)&&isset($Input['team_member'])){
                                $Input['type']==1?$commissions[$key]['totalNewCountPrice'] += $valueInner['team_member_commission_amount']:$commissions[$key]['totalRenewCountPrice'] += $valueInner['team_member_commission_amount'];
                            } else {
                                $Input['type']==1?$commissions[$key]['totalNewCountPrice'] += $valueInner['distributor_commission_amount']:$commissions[$key]['totalRenewCountPrice'] += $valueInner['distributor_commission_amount'];
                            }

                            $Input['type']==1?$commissions[$key]['totalNewCountPrice'] = round($commissions[$key]['totalNewCountPrice'],2):$commissions[$key]['totalRenewCountPrice'] = round($commissions[$key]['totalRenewCountPrice'],2);

                            // $totalYearCommission += $Input['type']==1?$valueInner['video_details']['commissionPriceNew']:$valueInner['video_details']['commissionPriceRenew'];
                            // $totalYearCommission = $Input['type']==1?$commissions[$key]['totalNewCountPrice']:$commissions[$key]['totalRenewCountPrice'];
                            if($Input['type']==1) {
                                $count += 1;
                            } else {
                                $count = 0;
                            }

                            $commissions[$key]['counts'][$valueInner['video_category_id']][] = $valueInner['subscribers_count'];

                            // $commissions[$key]['counts'][$valueInner['video_category_id']]   = array_sum($commissions[$key]['counts'][$valueInner['video_category_id']]);
                        
                        }
                        if(count($value) == $count) {
                            if($Input['type']==1) {
                                $totalYearCommission += $commissions[$key]['totalNewCountPrice'];
                            } else {
                                $totalYearCommission += $commissions[$key]['totalRenewCountPrice'];
                            }
                        }
                        foreach ($videoCategory as $keyCategory => $valueCategory) {
                            if(array_key_exists('counts', $commissions[$key])&&!array_key_exists($valueCategory['id'], $commissions[$key]['counts']))
                                $commissions[$key]['counts'][$valueCategory['id']] = 0;
                        }
                    }
                }
                $data['commissions'] = $commissions;
                $totalYearCommission = round($totalYearCommission, 2);
                $data['totalYearCommission'] = $totalYearCommission;
                $data['current_domain'] = UtilityController::getMessage('CURRENT_DOMAIN');
                $data['video_categories'] = $videoCategory;
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $data);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getMessage().' - '.$e->getLine(), '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: Checkpromo
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to check the applied promo code is valid or not
    //In Params:     promocode
    //Return:        json
    //Date:          2nd Nov, 2018
    //###############################################################
    public function Checkpromo(Request $request){
        try {
            if(Auth::check()) {
                $Input = Input::all();
                $user = Auth::user();
                if(!array_key_exists('promo', $Input))
                    throw new \Exception($Input['language']==1?'PROMO_CODE_REQUIRED':'PROMO_CODE_REQUIRED_CHINESE');
                if(!array_key_exists('for_category', $Input))
                    throw new \Exception($Input['language']==1?'SELECT_CATEGORY':'SELECT_CATEGORY_CHINESE');
                // foreach ($Input['for_category'] as $key => $value) {
                //     if(!is_numeric($value))
                //         unset($Input['for_category'][$key]);
                // }
                if(array_key_exists('already_paid', $Input) && !is_null($Input['already_paid'])){
                    foreach ($Input['for_category'] as $key => $value) {
                        if(in_array($value, $Input['already_paid']))
                            unset($Input['for_category'][$key]);
                    }
                }
                $whatPromo = PromoCode::where('promo_code',$Input['promo'])->where('status',1)->where('to_date','>=',Carbon::now())->where('status',1)->first();
                if(empty($whatPromo)){
                    //$returnMessage = $Input['language']==1?'PROMO_NOT_APPLICABLE':'PROMO_NOT_APPLICABLE_CHINESE';
                    $returnMessage = $Input['language']==1?'PROMO_NOT_APPLICABLE':($Input['language']==2?'PROMO_NOT_APPLICABLE_CHINESE':'PROMO_NOT_APPLICABLE_RU');
                    $returnData = UtilityController::Generateresponse(false, $returnMessage, Response::HTTP_BAD_REQUEST);
                } elseif ($whatPromo['code_for']==1||$whatPromo['code_for']==2) {
                    $returnData = self::Checkdistributorcode($Input);
                } else {
                    $checkIfdistributorExists = DistributorsReferrals::select('distributors_id')->where('user_id',$user->id)->first();
                    $checkExists = (!empty($checkIfdistributorExists)?PromoCode::where(function ($query) use($checkIfdistributorExists){
                            $query->where('distributor_id',$checkIfdistributorExists['distributors_id'])->orWhereNull('distributor_id');
                        }):PromoCode::whereNull('distributor_id'));

                    $checkExists = $checkExists->where('promo_code',$Input['promo'])->where('to_date','>=',Carbon::now())->where('status',1)->exists();
                    if($checkExists){
                        $finalPrice = 0;
                        if(PromoCode::where('promo_code',$Input['promo'])->where('promo_type',1)->where('to_date','>=',Carbon::now())->where('status',1)->count()==1){
                            $promoData = PromoCode::where('promo_code',$Input['promo'])->where('to_date','>=',Carbon::now())->where('status',1)->first()->toArray();
                            $promoData['video_category_id'] = array_values(explode(',', $promoData['video_category_id']));
                            if($Input['module'] == 0){
                                foreach($Input['for_category'] as $key => $value) {
                                    $category_id[] = $value['id'];
                                }
                            }
                            $Input['category_id'] = array_values($category_id);
                            $arraysAreEqual = (sort($Input['category_id']) == sort($promoData['video_category_id']));
                            ## Get taxrate Data
                            $taxrateData = TaxRate::latest()->first();
                            if($arraysAreEqual){
                                if($Input['price_type'] == 1) {
                                    if($Input['module'] == 1)
                                    {
                                        $price = array_sum(TwinklePricingPlan::whereIn('id',$Input['for_category'])->get()->pluck('usd_price')->toArray());
                                    }
                                    else
                                    {
                                        foreach($Input['for_category'] as $key => $value) {
                                            $details = VideoCategory::where('id',$value['id'])->get();
                                            if($value['plan_duration'] == 3){
                                                $total[] = $details[0]['category_price_3month'];
                                            } elseif ($value['plan_duration'] == 6) {
                                                $total[] = $details[0]['category_price_6month'];
                                            } else {
                                                $total[] = $details[0]['category_price'];
                                            }
                                        }
                                        $subtotal = array_sum($total);
                                        // if($taxrateData->tax_rate > 0){
                                        //     $taxrate_price = ($subtotal * $taxrateData->tax_rate)/100;
                                        //     $price = $subtotal + $taxrate_price;
                                        // } else {
                                        //     $price = array_sum($total);
                                        // }                     
                                    }
                                } else if($Input['price_type'] == 2) {
                                    if($Input['module'] == 1)
                                    {
                                        $price = array_sum(TwinklePricingPlan::whereIn('id',$Input['for_category'])->get()->pluck('sgd_price')->toArray());
                                    }
                                    else
                                    {
                                        foreach($Input['for_category'] as $key => $value) {
                                            $details = VideoCategory::where('id',$value['id'])->get();
                                            if($value['plan_duration'] == 3){
                                                $total[] = $details[0]['category_price_sgd_3month'];
                                            } elseif ($value['plan_duration'] == 6) {
                                                $total[] = $details[0]['category_price_sgd_6month'];
                                            } else {
                                                $total[] = $details[0]['category_price_sgd'];
                                            }
                                        }
                                        $subtotal = array_sum($total);
                                        // if($taxrateData->tax_rate > 0){
                                        //     $taxrate_price = ($subtotal * $taxrateData->tax_rate)/100;
                                        //     $price = $subtotal + $taxrate_price;
                                        // } else {
                                        //     $price = array_sum($total);
                                        // }
                                    }
                                }
                                if($promoData['discount_type']==0){
                                    $discount_of    = (($subtotal*$promoData['discount_of'])/100);
                                    $price          = $subtotal - $discount_of;
                                    if(!is_null($taxrateData)){
                                        $taxrate_price = ($subtotal * $taxrateData->tax_rate)/100;
                                        $finalPrice = $price + $taxrate_price;
                                    } else {
                                        $finalPrice = $price;
                                    }
                                } else {
                                    $discount_of    = $promoData['discount_of'];
                                    $price          = $price - $promoData['discount_of'];
                                    if(!is_null($taxrateData)){
                                        $taxrate_price = ($subtotal * $taxrateData->tax_rate)/100;
                                        $finalPrice = $price + $taxrate_price;
                                    } else {
                                        $finalPrice = $price;
                                    }
                                }
                                $promo['discount_of']   = $discount_of;
                                $promo['finalPrice']    = round($finalPrice,2);
                                if($finalPrice>0)
                                    $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1,$promo);
                                else {
                                    //$returnMessage = $Input['language']==1?'PROMO_NOT_APPLICABLE':'PROMO_NOT_APPLICABLE_CHINESE';
                                    $returnMessage = $Input['language']==1?'PROMO_NOT_APPLICABLE':($Input['language']==2?'PROMO_NOT_APPLICABLE_CHINESE':'PROMO_NOT_APPLICABLE_RU');
                                    $returnData = UtilityController::Generateresponse(false, $returnMessage, Response::HTTP_BAD_REQUEST);
                                }
                            } else {
                                if($Input['module'] == 0){
                                    $arraysAreEqual = array_diff($promoData['video_category_id'], $Input['category_id']);
                                    // $alsoBuy = implode(', ', VideoCategory::whereIn('id',$arraysAreEqual)->get()->pluck('category_name')->toArray());
                                    // $returnMessage = $Input['language']==1?'This Promo Code is only applicable if you purchase '.$alsoBuy.' to your cart.':'此优惠吗只限于认购 '.$alsoBuy.' 您的购物车。';
                                    $allPromoCodes = array_merge($promoData['video_category_id'], $Input['category_id']);
                                } else {
                                    $arraysAreEqual = array_diff($promoData['video_category_id'], $Input['for_category']);
                                    // $alsoBuy = implode(', ', VideoCategory::whereIn('id',$arraysAreEqual)->get()->pluck('category_name')->toArray());
                                    // $returnMessage = $Input['language']==1?'This Promo Code is only applicable if you purchase '.$alsoBuy.' to your cart.':'此优惠吗只限于认购 '.$alsoBuy.' 您的购物车。';
                                    $allPromoCodes = array_merge($promoData['video_category_id'], $Input['for_category']);
                                }
                                $alsoBuy = implode(', ', VideoCategory::whereIn('id',$allPromoCodes)->get()->pluck('category_name')->toArray());
                                $returnMessage = $Input['language']==1?'This Promo Code is only applicable if you purchase '.$alsoBuy.'.':'此优惠吗只限于认购'.$alsoBuy.'。';
                                $returnData = UtilityController::Generateresponse(false, $returnMessage, Response::HTTP_BAD_REQUEST);
                            }   
                        } else {
                            if($Input['module'] == 1)
                            {
                                $promoData = PromoCode::with('twinkle_category_detail')->where('promo_code',$Input['promo'])->whereIn('video_category_id',$Input['for_category'])->where('to_date','>=',Carbon::now())->where('status',1)->get()->toArray();
                            }
                            else
                            {
                                foreach ($Input['for_category'] as $key => $value) {
                                    $category_id[] = $value['id'];
                                }                                
                                $promoData = PromoCode::with('category_details_with_price')->where('promo_code',$Input['promo'])->whereIn('video_category_id',$category_id)->where('to_date','>=',Carbon::now())->where('status',1)->get()->toArray();
                            }
                            if(!empty($promoData) && count($category_id)==count($promoData)){
                                foreach ($promoData as $key => $value){
                                    if($Input['module'] == 1)
                                    {
                                        if($Input['price_type'] == 1) {
                                            if($value['discount_type']==0){
                                                $discount_of    = ($value['twinkle_category_detail']['usd_price']*$value['discount_of'])/100;
                                                $finalPrice += $value['twinkle_category_detail']['usd_price'] - $discount_of;
                                            } elseif ($value['discount_type']==1) {
                                                $discount_of    = $value['discount_of'];
                                                $finalPrice += $value['twinkle_category_detail']['usd_price'] - $discount_of;
                                            }
                                        } else if($Input['price_type'] == 2) {
                                            if($value['discount_type']==0){
                                                $discount_of    = ($value['twinkle_category_detail']['sgd_price']*$value['discount_of'])/100;
                                                $finalPrice += $value['twinkle_category_detail']['sgd_price'] - $discount_of;
                                            } elseif ($value['discount_type']==1) {
                                                $discount_of    = $value['discount_of'];
                                                $finalPrice += $value['twinkle_category_detail']['sgd_price'] - $discount_of;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ## Get Taxrate Data
                                        $taxrateData = TaxRate::latest()->first();
                                        if($Input['price_type'] == 1) {
                                            foreach($Input['for_category'] as $index => $element){
                                                if($element['plan_duration'] == 3) {
                                                    // if($taxrateData->tax_rate > 0){
                                                    //     $tax_rate = ($value['category_details_with_price']['category_price_3month']*$taxrateData->tax_rate)/100;
                                                    //     $total = $value['category_details_with_price']['category_price_3month'] + $tax_rate;
                                                    // } else {
                                                        $total = $value['category_details_with_price']['category_price_3month'];
                                                    // }
                                                    if($total > 0){
                                                        if($value['discount_type']==0){
                                                            $discount_of    = ($total*$value['discount_of'])/100;
                                                            $price = $total - $discount_of;
                                                        } elseif ($value['discount_type']==1) {
                                                            $discount_of    = $value['discount_of'];
                                                            $price = $total - $discount_of;
                                                        }
                                                        if(!is_null($taxrateData)){
                                                            $taxrate_price = ($total * $taxrateData->tax_rate)/100;
                                                            $finalPrice += $price + $taxrate_price;
                                                        } else {
                                                            $finalPrice += $price;
                                                        }
                                                    }
                                                } elseif ($element['plan_duration'] == 6) {
                                                    // if($taxrateData->tax_rate > 0){
                                                    //     $tax_rate = ($value['category_details_with_price']['category_price_6month']*$taxrateData->tax_rate)/100;
                                                    //     $total = $value['category_details_with_price']['category_price_6month'] + $tax_rate;
                                                    // } else {
                                                        $total = $value['category_details_with_price']['category_price_6month'];
                                                    // }
                                                    if($total > 0){
                                                        if($value['discount_type']==0){
                                                            $discount_of    = ($total*$value['discount_of'])/100;
                                                            $price = $total - $discount_of;
                                                        } elseif ($value['discount_type']==1) {
                                                            $discount_of    = $value['discount_of'];
                                                            $price = $total - $discount_of;
                                                        }
                                                    }
                                                    if(!is_null($taxrateData)){
                                                        $taxrate_price = ($total * $taxrateData->tax_rate)/100;
                                                        $finalPrice += $price + $taxrate_price;
                                                    } else {
                                                        $finalPrice += $price;
                                                    }
                                                } else {
                                                    // if($taxrateData->tax_rate > 0){
                                                    //     $tax_rate = ($value['category_details_with_price']['category_price']*$taxrateData->tax_rate)/100;
                                                    //     $total = $value['category_details_with_price']['category_price'] + $tax_rate;
                                                    // } else {
                                                        $total = $value['category_details_with_price']['category_price'];
                                                    // }
                                                    if($total > 0){
                                                        if($value['discount_type']==0){
                                                            $discount_of    = ($total*$value['discount_of'])/100;
                                                            $price = $total - $discount_of;
                                                        } elseif ($value['discount_type']==1) {
                                                            $discount_of    = $value['discount_of'];
                                                            $price = $total - $discount_of;
                                                        }
                                                    }
                                                    if(!is_null($taxrateData)){
                                                        $taxrate_price = ($total * $taxrateData->tax_rate)/100;
                                                        $finalPrice += $price + $taxrate_price;
                                                    } else {
                                                        $finalPrice += $price;
                                                    }
                                                }   
                                            }
                                        } else if($Input['price_type'] == 2) {
                                            foreach($Input['for_category'] as $index => $element){
                                                if($element['plan_duration'] == 3) {
                                                    // if($taxrateData->tax_rate > 0){
                                                    //     $tax_rate = ($value['category_details_with_price']['category_price_sgd_3month']*$taxrateData->tax_rate)/100;
                                                    //     $total = $value['category_details_with_price']['category_price_sgd_3month'] + $tax_rate; 
                                                    // } else {
                                                        $total = $value['category_details_with_price']['category_price_sgd_3month'];
                                                    // }
                                                    if($total > 0){
                                                        if($value['discount_type']==0){
                                                            $discount_of    = ($total*$value['discount_of'])/100;
                                                            $price = $total - $discount_of;
                                                        } elseif ($value['discount_type']==1) {
                                                            $discount_of    = $value['discount_of'];
                                                            $price = $total - $discount_of;
                                                        }
                                                    }
                                                    if(!is_null($taxrateData)){
                                                        $taxrate_price = ($total * $taxrateData->tax_rate)/100;
                                                        $finalPrice += $price + $taxrate_price;
                                                    } else {
                                                        $finalPrice += $price;
                                                    }
                                                } elseif ($element['plan_duration'] == 6) {
                                                    // if($taxrateData->tax_rate > 0){
                                                    //     $tax_rate = ($value['category_details_with_price']['category_price_sgd_6month']*$taxrateData->tax_rate)/100;
                                                    //     $total = $value['category_details_with_price']['category_price_sgd_6month'] + $tax_rate;
                                                    // } else {
                                                        $total = $value['category_details_with_price']['category_price_sgd_6month'];
                                                    // }
                                                    if($total > 0){
                                                        if($value['discount_type']==0){
                                                            $discount_of    = ($total*$value['discount_of'])/100;
                                                            $price = $total - $discount_of;
                                                        } elseif ($value['discount_type']==1) {
                                                            $discount_of    = $value['discount_of'];
                                                            $price = $total - $discount_of;
                                                        }
                                                    }
                                                    if(!is_null($taxrateData)){
                                                        $taxrate_price = ($total * $taxrateData->tax_rate)/100;
                                                        $finalPrice += $price + $taxrate_price;
                                                    } else {
                                                        $finalPrice += $price;
                                                    }
                                                } else {
                                                    // if($taxrateData->tax_rate > 0){
                                                    //     $tax_rate = ($value['category_details_with_price']['category_price_sgd']*$taxrateData->tax_rate)/100;
                                                    //     $total = $value['category_details_with_price']['category_price_sgd'] + $tax_rate;
                                                    // } else {
                                                        $total = $value['category_details_with_price']['category_price_sgd'];
                                                    // }
                                                    if($total > 0){
                                                        if($value['discount_type']==0){
                                                            $discount_of    = ($total*$value['discount_of'])/100;
                                                            $price = $total - $discount_of;
                                                        } elseif ($value['discount_type']==1) {
                                                            $discount_of    = $value['discount_of'];
                                                            $price = $total - $discount_of;
                                                        }
                                                    }
                                                    if(!is_null($taxrateData)){
                                                        $taxrate_price = ($total * $taxrateData->tax_rate)/100;
                                                        $finalPrice += $price + $taxrate_price;
                                                    } else {
                                                        $finalPrice += $price;
                                                    }
                                                }   
                                            }
                                        }
                                    }
                                }
                                $promo['discount_of']   = $discount_of;
                                $promo['finalPrice']    = round($finalPrice,2);
                                if($finalPrice>0)
                                    $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1,$promo);
                                else{
                                    //$returnMessage = $Input['language']==1?'PROMO_NOT_APPLICABLE':'PROMO_NOT_APPLICABLE_CHINESE';
                                    $returnMessage = $Input['language']==1?'PROMO_NOT_APPLICABLE':($Input['language']==2?'PROMO_NOT_APPLICABLE_CHINESE':'PROMO_NOT_APPLICABLE_RU');
                                    $returnData = UtilityController::Generateresponse(false, $returnMessage, Response::HTTP_BAD_REQUEST);    
                                }
                            } else {
                                //$returnMessage = $Input['language']==1?'PROMO_NOT_APPLICABLE':'PROMO_NOT_APPLICABLE_CHINESE';
                                $returnMessage = $Input['language']==1?'PROMO_NOT_APPLICABLE':($Input['language']==2?'PROMO_NOT_APPLICABLE_CHINESE':'PROMO_NOT_APPLICABLE_RU');
                                $returnData = UtilityController::Generateresponse(false, $returnMessage, Response::HTTP_BAD_REQUEST);
                            }
                        }
                    } else {
                        //$returnMessage = $Input['language']==1?'PROMO_NOT_APPLICABLE':'PROMO_NOT_APPLICABLE_CHINESE';
                        $returnMessage = $Input['language']==1?'PROMO_NOT_APPLICABLE':($Input['language']==2?'PROMO_NOT_APPLICABLE_CHINESE':'PROMO_NOT_APPLICABLE_RU');
                        $returnData = UtilityController::Generateresponse(false, $returnMessage, Response::HTTP_BAD_REQUEST);
                    }
                }
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getMessage(), '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: Checkdistributorcode
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       if user enters distributor referal code while availing promo code, then navigate user accordingly
    //In Params:     Input parameter for CkeckPromo method
    //Return:        json
    //Date:          6th Dec, 2018
    //###############################################################
    public function Checkdistributorcode($Input){     
        try {
            \DB::beginTransaction();
                $user = Auth::user();
                $promoData = PromoCode::where('promo_code',$Input['promo'])->where('to_date','>=',Carbon::now())->where('status',1)->first()->toArray();
                $checkIfdistributorExists = DistributorsReferrals::where('user_id',$user->id)->first();
                if(!empty($checkIfdistributorExists)){
                    $distributorDetail = $promoData['code_for']==1?Distributors::where('referal_code',$Input['promo'])->first():DistributorsTeamMembers::where('referal_code',$Input['promo'])->first();
                    $return_promo_error = 0;
                    if(!($user->id==$checkIfdistributorExists['user_id'] && $promoData['distributor_id']==$checkIfdistributorExists['distributors_id'])||$distributorDetail['status']!=1){
                        $return_promo_error = 1;
                    }
                    if($checkIfdistributorExists['distributor_team_member_id']!='' && $promoData['distributor_team_member_id']!=$checkIfdistributorExists['distributor_team_member_id']){
                        $return_promo_error = 1;
                    }
                    if($return_promo_error == 1){
                        //$returnMessage = $Input['language']==1?'PROMO_NOT_APPLICABLE':'PROMO_NOT_APPLICABLE_CHINESE';
                        $returnMessage = $Input['language']==1?'PROMO_NOT_APPLICABLE':($Input['language']==2?'PROMO_NOT_APPLICABLE_CHINESE':'PROMO_NOT_APPLICABLE_RU');
                        $returnData = UtilityController::Generateresponse(false, $returnMessage, Response::HTTP_BAD_REQUEST);
                        return $returnData;
                    }
                }
                $Input['for_category'] = array_values($Input['for_category']);
                $price = array_sum(VideoCategory::whereIn('id',$Input['for_category'])->get()->pluck('category_price')->toArray());
                if($promoData['discount_type']==0){
                    $finalPrice = (($price*$promoData['discount_of'])/100);
                    $finalPrice = $price - $finalPrice;
                } else {
                    $finalPrice = $price - $promoData['discount_of'];
                }
                $finalPrice = round($finalPrice,2);
                if($finalPrice>0)
                    $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1,$finalPrice);
                else {
                    //$returnMessage = $Input['language']==1?'PROMO_NOT_APPLICABLE':'PROMO_NOT_APPLICABLE_CHINESE';
                    $returnMessage = $Input['language']==1?'PROMO_NOT_APPLICABLE':($Input['language']==2?'PROMO_NOT_APPLICABLE_CHINESE':'PROMO_NOT_APPLICABLE_RU');
                    $returnData = UtilityController::Generateresponse(false, $returnMessage, Response::HTTP_BAD_REQUEST);
                }
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return $returnData;
    }

    //###############################################################
    //Function Name: Getteamforcommissions
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to get team members for commission page search by team member
    //In Params:     void
    //Return:        json
    //Date:          3rd Dec, 2018
    //###############################################################
    public function Getteamforcommissions(Request $request){     
        try {
            $distributorId = Auth::guard('distributors')->user()->id;
            $teamData = DistributorsTeamMembers::select('id','first_name','last_name')->where('distributor_id',$distributorId)->get();
            if(!empty($teamData)){
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $teamData);    
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: Encryptdataforstripepayment
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To encrypt the amount and categories selected so that they cannot be easily manipulated by normal user
    //In Params:     categories selected for purchase, promocode (if applied)
    //Return:        json
    //Date:          18th Dec, 2018
    //###############################################################
    public function Encryptdataforstripepayment(Request $request){     
        try {
            $Input = Input::all();
            if($Input['promoStatus']==1){
                $paymentOf = self::Checkpromo($request);
                $paymentOf = $paymentOf->original['data']['finalPrice'];
            } else {
                ## Get taxrate Data
                $taxrateData = TaxRate::latest()->first();
                if($Input['price_type'] == 1) {
                    foreach($Input['for_category'] as $key => $value) {
                        $details = VideoCategory::where('id',$value['id'])->get();
                        if($value['plan_duration'] == 3){
                            $paymentOf[] = $details[0]['category_price_3month'];
                        } elseif ($value['plan_duration'] == 6) {
                            $paymentOf[] = $details[0]['category_price_6month'];
                        } else {
                            $paymentOf[] = $details[0]['category_price'];
                        }
                    }
                    $subTotal = array_sum($paymentOf);
                    if(!is_null($taxrateData)){
                        $taxrate_price = ($subTotal * $taxrateData->tax_rate)/100;
                        $paymentOf = $subTotal + $taxrate_price;
                    } else {
                        $paymentOf = $subTotal;
                    }
                } else if($Input['price_type'] == 2) {
                    foreach($Input['for_category'] as $key => $value) {
                        $details = VideoCategory::where('id',$value['id'])->get();
                        if($value['plan_duration'] == 3){
                            $paymentOf[] = $details[0]['category_price_sgd_3month'];
                        } elseif ($value['plan_duration'] == 6) {
                            $paymentOf[] = $details[0]['category_price_sgd_6month'];
                        } else {
                            $paymentOf[] = $details[0]['category_price_sgd'];
                        }
                    }
                    $subTotal = array_sum($paymentOf);
                    if(!is_null($taxrateData)){
                        $taxrate_price = ($subTotal * $taxrateData->tax_rate)/100;
                        $paymentOf = $subTotal + $taxrate_price;
                    } else {
                        $paymentOf = $subTotal;
                    }
                }
            }
            ## Can you have serialize... 
            // $data = serialize($Input['for_category']);
            // $en = base64_encode($data);
            // $de = base64_decode($en);
            // $data = unserialize($de);
            foreach($Input['for_category'] as $key=>$value)
            {
                $string[] = $value['id'].'_'.$value['plan_duration'];
            }
            $paymentFor = implode(',', $string);
            if($Input['promoStatus']==1){
                $encryptData = base64_encode(base64_encode($paymentOf).'_'.base64_encode($paymentFor).'_'.base64_encode(1).'_'.base64_encode($Input['price_type']).'_'.base64_encode($Input['promo']));
            } else {
                $encryptData = base64_encode(base64_encode($paymentOf).'_'.base64_encode($paymentFor).'_'.base64_encode(0).'_'.base64_encode($Input['price_type']));
            }
            $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $encryptData);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: Stripepayemnt
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To make payment through stripe payment gateway
    //In Params:     encoded string for amount and category for payment
    //Return:        json
    //Date:          18th Dec, 2018
    //###############################################################
    public function Stripepayemnt(Request $request){ 
        try {
            if(Auth::check()){    
                $Input = Input::all();
                $wholePath = substr(url('/'), 0, -3);
                $pathToStore = explode('/', $wholePath);
                #beta checking
                $fullPath = url()->full();
                $checkBeta = explode('/', $fullPath);

                $Input['token'] = base64_decode($Input['token']);
                $Input['token'] = explode('_', $Input['token']);

                $amount = base64_decode($Input['token'][0]);
                $category = explode(',', base64_decode($Input['token'][1]));
                foreach ($category as $key => $value) {
                    $purchase_category      = explode('_', $value);
                    $forCategory[$key]['id'] = $purchase_category[0];
                    $forCategory[$key]['plan_duration'] = $purchase_category[1]; 
                }
                $hasPromo = base64_decode($Input['token'][2]);
                $priceType = base64_decode($Input['token'][3]);

                if($hasPromo==1)
                    $Input['promo_code'] = base64_decode($Input['token'][4]);
                $user = Auth::user();
                if(in_array('localhost', $pathToStore) || in_array('beta', $checkBeta))
                    Stripe::setApiKey(env('STRIPE_SANDBOX'));
                else
                    Stripe::setApiKey(env('STRIPE_LIVE'));
                $token = Token::create([
                    'card' => [
                        'number'    => $Input['card_number'],
                        'exp_month' => $Input['exp_month'],
                        'exp_year'  => $Input['exp_year'],
                        'cvc'       => $Input['cvv'],
                    ],
                ]);
                if (!filter_var($user->email_address, FILTER_VALIDATE_EMAIL)) {
                    $customer = Customer::create(array(
                        'source' => $token->id
                    ));
                } else {
                    $customer = Customer::create(array(
                        'email'  => $user->email_address,
                        'source' => $token->id
                    ));
                }

                if($priceType == 1) {
                    $currency = 'usd';
                } else if($priceType == 2) {
                    $currency = 'sgd';
                }
                
                $charge = Charge::create(array(
                    'customer' => $customer->id,
                    // 'amount'   => $paymentFor['category_price'],
                    'amount'   => intval($amount)*100,
                    'currency' => $currency
                ));

                #User stripe card id update.. 
                User::where('id',$user->id)->update(['stripe_id'=>$customer->id]);

                if($charge->paid==1){
                    \DB::beginTransaction();
                        if(array_key_exists('promo_code', $Input) && isset($Input['promo_code'])){
                            foreach ($forCategory as $key => $value) {
                                $category_id[] = $value['id'];
                            }
                            $stringToIntArray = array_map('intval', $category_id);
                            $promoData = PromoCode::where('promo_code',$Input['promo_code'])->whereIn('video_category_id',$stringToIntArray)->where('status',1)->where('to_date','>=',Carbon::now())->first();
                            $ids[]  = $promoData['video_category_id'];    
                            $arraysAreEqual = (sort($stringToIntArray) == sort($ids));
                            if($arraysAreEqual){
                                $Input['discount_type'] = $promoData['discount_type'];
                                $Input['discount_of']   = $promoData['discount_of'];
                                if($promoData['code_for']==0){
                                    $Input['promo_code_id'] = PromoCode::select(DB::RAW('GROUP_CONCAT(id) AS ids'))->where('promo_code',$Input['promo_code'])->where('status',1)->whereIn('video_category_id',$stringToIntArray)->where('to_date','>=',Carbon::now())->first()->toArray();
                                    $Input['promo_code_id'] = $Input['promo_code_id']['ids'];
                                } else {
                                    $addToPayment['distributor_id'] = $promoData['distributor_id'];
                                    $addToPayment['distributer_team_member_id'] = $promoData['distributor_team_member_id'];
                                }
                            } else {
                                 $returnMessage = $Input['language']==1?'PROMO_NOT_APPLICABLE':($Input['language']==2?'PROMO_NOT_APPLICABLE_CHINESE':'PROMO_NOT_APPLICABLE_RU');
                                    $returnData = UtilityController::Generateresponse(false, $returnMessage, Response::HTTP_BAD_REQUEST);
                            }
                        }
                        $paymentResult       = self::Generatepaymentarray($user,$forCategory,$charge,$amount,$Input,!empty($promoData)?$promoData:'',!empty($addToPayment)?$addToPayment:'',$priceType);
                        // if(!empty($addToPayment)){
                        //     $payment['distributor_id'] = $promoData['distributor_id'];
                        //     $payment['distributer_team_member_id'] = $promoData['distributor_team_member_id'];
                        // }
                        // $payment['payment_amount_type'] = $priceType;
                        if(!empty($paymentResult)){
                            foreach($forCategory as $key => $value){
                                $category_id[] = $value['id'];
                                $plan_duration[] = $value['plan_duration'];
                            }
                            if($paymentResult['promo_code']!=''){
                                $promoData = PromoCode::where('promo_code',$paymentResult['promo_code'])->first();
                
                                if($promoData['code_for']==1||$promoData['code_for']==2){
                                    $checkIfdistributorExists = DistributorsReferrals::where('user_id',$paymentResult['paid_by'])->first();
                                    if(empty($checkIfdistributorExists)){
                                        if($promoData['code_for']==2)
                                            $addReferal['distributor_team_member_id'] = $promoData['distributor_team_member_id'];
                                        $addReferal['distributors_id'] = $promoData['distributor_id'];
                                        $addReferal['user_id'] = $paymentResult['paid_by'];
                                        $addReferal['referal_through'] = $promoData['code_for']==2?4:1;
                                        $addReferalResult = UtilityController::Makemodelobject($addReferal,'DistributorsReferrals');
                                    }
                                }
                                if($promoData['code_for']==0){
                                    $promoCount = PromoCode::whereIn('id',explode(',', $paymentResult['promo_code_id']))->where('status',1)->where('to_date','>=',Carbon::now())->increment('redeemed_count');
                                } else {
                                    $promoCount = PromoCode::where('promo_code',$Input['promo_code'])->where('status',1)->where('to_date','>=',Carbon::now())->increment('redeemed_count');
                                }
                            }
                            $hasDistributor = DistributorsReferrals::with('distributor','salesperson')->where('user_id',$user->id)->orderBy('id','DESC')->first();
                            if(!empty($hasDistributor)){
                                $addDistributortoPayment['distributor_id'] = $hasDistributor['distributors_id'];
                                if($paymentResult['distributer_team_member_id']=='')
                                    $addDistributortoPayment['distributer_team_member_id'] = $hasDistributor['distributor_team_member_id'];
                                $paymentResultDistributor = UtilityController::Makemodelobject($addDistributortoPayment,'MarketPlacePayment','',$paymentResult['id']);
                                $addCommissions = self::Generatecommissionsarray($category_id,$hasDistributor,$paymentResult);
                                IgniteCommissions::insert($addCommissions);
                            }
                            if($priceType == 1) {
                                foreach($forCategory as $key => $value){
                                    if($value['plan_duration'] == 3){
                                        $cate_name[] = VideoCategory::select('id','category_name','category_price_3month as category_price','subject_id')->where('id',$value['id'])->with('subject')->get()->toArray();
                                    } elseif ($value['plan_duration'] == 6) {
                                        $cate_name[] = VideoCategory::select('id','category_name','category_price_6month as category_price','subject_id')->where('id',$value['id'])->with('subject')->get()->toArray();
                                    } elseif ($value['plan_duration'] == 12) {
                                        $cate_name[] = VideoCategory::select('id','category_name','category_price','subject_id')->where('id',$value['id'])->with('subject')->get()->toArray();
                                    }
                                }
                            } else if($priceType == 2) {
                                foreach($forCategory as $key => $value){
                                    if($value['plan_duration'] == 3){
                                        $cate_name[] = VideoCategory::select('id','category_name','category_price_sgd_3month as category_price_sgd','subject_id')->where('id',$value['id'])->with('subject')->get()->toArray();
                                    } elseif ($value['plan_duration'] == 6) {
                                        $cate_name[] = VideoCategory::select('id','category_name','category_price_sgd_6month as category_price_sgd','subject_id')->where('id',$value['id'])->with('subject')->get()->toArray();
                                    } elseif ($value['plan_duration'] == 12) {
                                        $cate_name[] = VideoCategory::select('id','category_name','category_price_sgd','subject_id')->where('id',$value['id'])->with('subject')->get()->toArray();
                                    }
                                }
                            }
                            foreach ($cate_name as $ind => $ele) {
                                $cat_name[] = $ele[0];
                            }
                            if(!(array_key_exists('subscription_end_date', $paymentResult->toArray()))){
                                $todaysDate = Carbon::now()->timezone('Asia/Singapore');
                                $result     = MarketPlacePayment::select('*')->where('subscription_end_date','>=',$todaysDate)->where('paid_by', Auth::user()->id)->where('paid_for', 3)->where('payment_status', 1)->where('parent_id',$paymentResult->id)->get()->toArray();
                                // Add subscription end date and plan duration
                                $collection = collect($cat_name)->map(function ($name) use($result) {
                                    foreach ($result as $key => $value) {
                                        if($name['id'] == $value['video_category_id']){
                                            $name['plan_duration'] = $value['plan_duration'];
                                            $dateObject = Carbon::parse($value['subscription_end_date'])->format('d M Y');
                                            $name['subscription_end_date'] = $dateObject;    
                                        }    
                                    }
                                    return $name;
                                });
                            } else {
                                $collection = collect($cat_name)->map(function ($name) use($paymentResult) {
                                        if($name['id'] == $paymentResult->video_category_id){
                                            $name['plan_duration'] = $paymentResult->plan_duration;
                                            $dateObject = Carbon::parse($paymentResult->subscription_end_date)->format('d M Y');
                                            $name['subscription_end_date'] = $dateObject;        
                                        }
                                    return $name;
                                });
                            }

                            $category_name['categoies'] = $collection->toArray();
                            // $category_name['categoies'] = $cat_name;
                            if($paymentResult['promo_code']!=''){
                                $category_name['isDiscount'] = 1;
                                $category_name['promoCode'] = $paymentResult['promo_code'];
                                $category_name['discount_type'] = $paymentResult['discount_type'];
                                $category_name['discount_of'] = $paymentResult['discount_of'];
                            } else
                                $category_name['isDiscount'] = 0;
                            // $info['generateReceipt'] = self::Receiptattachement($paymentResult['payment_amount'],$user->email_address,$user->first_name,$user->last_name,$hasDistributor,$paymentResult['subscription_end_date'],$paymentResult['system_transaction_number'],$category_name);
                            $card_number = substr($Input['card_number'], -4);
                            $info['generateReceipt'] = self::IgniteReceiptattachement($forCategory,$priceType,$paymentResult['payment_amount'],$user->email_address,$user->first_name,$user->last_name,$hasDistributor,Carbon::now(),$paymentResult['system_transaction_number'],$category_name,$card_number);
                            $system_transaction_number = $paymentResult['system_transaction_number'];
                            $info['email_address'] = $user->email_address;
                            $info['current_language'] = 1;
                            $system_transaction_number = $paymentResult['system_transaction_number'];
                            $userFullName = $user->first_name." ".$user->last_name;

                            $info['subject']=$info['current_language']==1?'Thank you for your payment. Enclosed is your payment receipt.':($info['current_language']==2?'谢谢您的付款。 附件的是您的项付款收据。':'Спасибо за оплату. Ваша квитанция прилагается.');

                            $info['content'] = $info['current_language']==1?'Dear <strong>'.$user->first_name.',
                                </strong><br/><br/>
                                You have purchased the following Subscription.<br/><br/>':($info['current_language']==2?'你好 <strong>'.$paymentDetails['projectpaymentuserdetailinfo']['first_name'].' '.$paymentDetails['projectpaymentuserdetailinfo']['last_name'].'</strong> ,
                                <br/><br/>
                                谢谢您的付款。随函附上您的付款收据。<br/><br/><br/>':'Привет <strong>'.$userFullName.' ,
                                </strong><br/><br/>
                                Спасибо за оплату. Ваша квитанция прилагается.<br/><br/>');

                            foreach ($category_name['categoies'] as $key => $value) {
                                $path = URL::to('/public/uploads/subject_images/'.$value['subject']['subject_image']);
                                $info['content'] .= '<img src="'.$path.'" style="height: 120px;margin-right: 20px;">';
                            }

                            if($priceType == 1){
                                $priceSign = 'USD';
                                foreach ($category_name['categoies'] as $index => $detail) {
                                    $info['content'] .= '<br /><br /><span>'.$detail['subject']['subject_display_name'].' - '. $detail['category_name'] .' - '.$priceSign.$detail['category_price'].'/'.$detail['plan_duration'].' months starting '. Carbon::now()->timezone('Asia/Singapore')->format('d M Y') .', expiring on '. $detail['subscription_end_date'] .'</span>';
                                }
                            } elseif ($priceType == 2) {
                                $priceSign = 'SGD';
                                foreach ($category_name['categoies'] as $index => $detail) {
                                    $info['content'] .= '<br /><br /><span>'.$detail['subject']['subject_display_name'].' - '. $detail['category_name'] .' - '.$priceSign.$detail['category_price_sgd'].'/'.$detail['plan_duration'].' months starting '. Carbon::now()->timezone('Asia/Singapore')->format('d M Y') .', expiring on '. $detail['subscription_end_date'] .'</span>';
                                }
                            }

                            // foreach ($category_name['categoies'] as $index => $detail) {
                            //     $info['content'] .= '<br /><br /><span>'.$detail['subject']['subject_display_name'].' - '. $detail['category_name'] .' - '.$priceSign.$detail['category_price'].'/'.$detail['plan_duration'].' months starting '. Carbon::now()->timezone('Asia/Singapore')->format('d M Y') .', expiring on '. $detail['subscription_end_date'] .'</span>';
                            // }
                            $faqsPath = URL::to('/faq');
                            $info['content'] .= '<br /><br /><span>Subscriptions automatically renew for the same grade until you opt out of auto-renewal. To learn more about opting out of auto-renewal for your subscription, visit our <a href="'.$faqsPath.'" style="color: blue;">FAQs</a>.</span><br /><br />';

                            $info['footer_content']=$info['current_language']==1?'From,<br /> SixClouds':($info['current_language']==2?'六云 ':'SixClouds');
                            $info['footer']=$info['current_language']==1?'SixClouds':($info['current_language']==2?'六云':'SixClouds');
                            Mail::send('emails.email_template', $info, function ($message) use ($info,$system_transaction_number) {
                                $message->from('noreply@sixclouds.net', 'SixClouds');
                                $message->attach($info['generateReceipt'],array(
                                                    'as' => $system_transaction_number.'.pdf', 
                                                    'mime' => 'application/pdf'));
                                $message->to($info['email_address'])->subject($info['subject']);
                            });
                            $returnData = UtilityController::Generateresponse(true, 'PAYMENT_SUCCESS', 1, '1');
                        }
                    \DB::commit();
                } else {
                    $returnData = UtilityController::Generateresponse(false, 'PAYMENT_FAILED', 0, '0');
                }
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);    
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: Generatepaymentarray
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to generate marketplace payment table inserting array
    //In Params:     $user,$forCategory,$charge,$amount,$Input
    //Return:        json
    //Date:          18th Dec, 2018
    //###############################################################
    public function Generatepaymentarray($user,$forCategory,$charge,$amount,$Input,$promoData,$addToPayment,$priceType){     
        try {
                if(count($forCategory) == 1){
                    foreach ($forCategory as $key => $value) {
                        $history_id = VideoCategoryHistory::where('category_id',$value['id'])->latest()->first();
                        $catgoryNameArray = implode(', ', VideoCategory::where('id',$value['id'])->get()->pluck('category_name')->toArray());
                        $payment['payment_method']            = 5; //strip
                        $payment['paid_by']                   = $user->id;
                        $payment['video_category_id']         = $value['id'];
                        $payment['history_id']                = $history_id['id'];
                        $payment['video_category_name']       = $catgoryNameArray;
                        if($value['plan_duration'] == 3){
                            $payment['subscription_end_date']     = Carbon::now()->addMonths(3)->subDays(1);
                            $payment['plan_duration']             = 3;
                            $subscription_end_date1 = Carbon::now()->addMonths(3)->subDays(1);
                            $subscription_end_date2 = Carbon::now()->addMonths(3)->subDays(1);
                        } elseif ($value['plan_duration'] == 6) {
                            $payment['subscription_end_date']     = Carbon::now()->addMonths(6)->subDays(1);
                            $payment['plan_duration']             = 6;
                            $subscription_end_date1 = Carbon::now()->addMonths(3)->subDays(1);
                            $subscription_end_date2 = Carbon::now()->addMonths(3)->subDays(1);
                        } else {
                            $payment['subscription_end_date']     = Carbon::now()->addYears(1)->subDays(1);
                            $payment['plan_duration']             = 12;
                            $subscription_end_date1 = Carbon::now()->addMonths(3)->subDays(1);
                            $subscription_end_date2 = Carbon::now()->addMonths(3)->subDays(1);
                        }
                        $payment['remarks']                   = 'New subscription';
                        $payment['access_platform']           = 1;
                        $payment['transaction_reference']     = $charge->balance_transaction;
                        $payment['paid_for']                  = 3;
                        // $payment['payment_amount']         = $paymentFor['category_price'];
                        $payment['payment_amount']            = $amount;
                        if(array_key_exists('promo_code', $Input) && isset($Input['promo_code'])){   
                            $payment['promo_code']                = $Input['promo_code'];
                            $payment['discount_type']             = $Input['discount_type'];
                            $payment['discount_of']               = $Input['discount_of'];
                            if($promoData!=''&&$promoData['code_for']==0&&isset($Input['promo_code_id']))
                                $payment['promo_code_id']             = $Input['promo_code_id'];
                        }
                        $payment['payment_status']            = 1;
                        $payment['slug']                      = $user->slug;
                        if(!empty($addToPayment)){
                            $payment['distributor_id'] = $addToPayment['distributor_id'];
                            $payment['distributer_team_member_id'] = $addToPayment['distributor_team_member_id'];
                        }
                        $payment['payment_amount_type'] = $priceType;
                        $payment['plan_status'] = 1;
                        $payment['system_transaction_number'] = UtilityController::GenerateRunningNumber('MarketPlacePayment','system_transaction_number',"VL");
                    }
                    $paymentResult = UtilityController::Makemodelobject($payment,'MarketPlacePayment');
                    $paymentResult['url']          = URL::to('/');
                    ##Job Manage
                    $Ids = DB::table('job_manage')->where('ignite_category_id',$paymentResult['video_category_id'])->where('user_id',$user->id)->pluck('job_id')->toArray();
                    if(!empty($Ids)) {
                        DB::table('jobs')->whereIn('id',$Ids)->delete();
                    }

                    ## Live
                    if($paymentResult['plan_duration'] == 12){
                        $IgniteCategory = (new IgniteCategoryPayment($paymentResult->toArray()))->delay(Carbon::now()->addYears(1)->subDays(1));
                    } elseif ($paymentResult['plan_duration'] == 6) {
                        $IgniteCategory = (new IgniteCategoryPayment($paymentResult->toArray()))->delay(Carbon::now()->addMonths(6)->subDays(1));
                    } elseif ($paymentResult['plan_duration'] == 3) {
                        $IgniteCategory = (new IgniteCategoryPayment($paymentResult->toArray()))->delay(Carbon::now()->addMonths(3)->subDays(1));
                    }
                    // ## Test
                    // $IgniteCategory = (new IgniteCategoryPayment($paymentResult->toArray()))->delay(Carbon::now()->addMinutes(3));
                    $jobId = dispatch($IgniteCategory);

                    DB::table('job_manage')->where('ignite_category_id',$paymentResult['video_category_id'])->where('user_id',$user->id)->delete();
                    DB::table('job_manage')->insert(
                        ['ignite_category_id' => $paymentResult['video_category_id'], 'job_id' => $jobId,'user_id' => $user->id, 'status' => 1 ]
                    );

                    ## Get tax rate details 
                    $taxrateData = TaxRate::latest()->first();

                    ## User Notify recurring payment for email
                    $categoryData = VideoCategory::with('subject')->where('id',$value['id'])->where('status',1)->first();

                    if($paymentResult['payment_amount_type'] == 1){
                        if($paymentResult['plan_duration'] == 12){
                            $category_price = $categoryData->category_price;
                        } elseif ($paymentResult['plan_duration'] == 6) {
                            $category_price = $categoryData->category_price_6month;
                        } elseif ($paymentResult['plan_duration'] == 3) {
                            $category_price = $categoryData->category_price_3month;
                        }
                    } elseif ($paymentResult['payment_amount_type'] == 2) {
                        if($paymentResult['plan_duration'] == 12){
                            $category_price = $categoryData->category_price_sgd;
                        } elseif ($paymentResult['plan_duration'] == 6) {
                            $category_price = $categoryData->category_price_sgd_6month;
                        } elseif ($paymentResult['plan_duration'] == 3) {
                            $category_price = $categoryData->category_price_sgd_3month;
                        }
                    }
                    if(!is_null($taxrateData)){
                        $taxrate_price = ($category_price*$taxrateData->tax_rate)/100;
                        $emailAmount = $category_price + $taxrate_price;
                    } else {
                        $emailAmount = $category_price;
                    }

                    ## Delete Old category Cron Job Notify email and Notify email Ids
                    $id = StoreEmail::where('user_id',$user->id)->where('category_id',$paymentResult['video_category_id'])->pluck('job_id')->toArray();
                    if(!empty($id)){
                        foreach($id as $index => $emailId){
                            $email_job = explode(',', $emailId);
                            $email_job_delete = DB::table('jobs')->whereIn('id',$email_job)->delete();
                            $store_email_delete = StoreEmail::where('user_id',$user->id)->where('category_id',$paymentResult['video_category_id'])->where('job_id',$emailId)->delete();
                        }
                    }

                    $emailDetails['email_address']          = $user['email_address'];
                    $emailDetails['display_name']           = $user['display_name'];
                    $emailDetails['subscription_end_date']  = $paymentResult['subscription_end_date'];
                    $emailDetails['payment_amount_type']    = $paymentResult['payment_amount_type'];
                    $emailDetails['amount']                 = $emailAmount;
                    $emailDetails['video_category_id']      = $paymentResult['video_category_id'];
                    $emailDetails['user_id']                = $user->id;
                    $emailDetails['plan_duration']          = $paymentResult['plan_duration'];
                    $emailDetails['video_category_name']    = $paymentResult['video_category_name'];
                    $emailDetails['starting_date']          = Carbon::now()->timezone('Asia/Singapore')->format('d M Y');
                    $emailDetails['subject_name']           = $categoryData->subject['subject_display_name'];
                    $emailDetails['subject_image']           = URL::to('/public/uploads/subject_images/'.$categoryData->subject['subject_image']);
                    $emailDetails['review_url']             = URL::to('/my-subscription');
                    ## Live 
                    $EmailForNotifyFirst  = (new EmailForNotify($emailDetails))->delay($subscription_end_date1->subDays(7));
                    // ## Test 
                    // $EmailForNotifyFirst  = (new EmailForNotify($emailDetails))->delay(Carbon::now()->addMinutes(1));
                    $job['first'] = dispatch($EmailForNotifyFirst);
                    ## Live
                    $EmailForNotifySecond = (new EmailForNotify($emailDetails))->delay($subscription_end_date2->subMonths(1));
                    // ## Test 
                    // $EmailForNotifySecond  = (new EmailForNotify($emailDetails))->delay(Carbon::now()->addMinutes(2));
                    $job['second'] = dispatch($EmailForNotifySecond);
                    $job_email = implode(',', $job);
                    DB::table('store_email')->insert(
                        ['category_id' => $paymentResult['video_category_id'], 'job_id' => $job_email,'user_id' => $user->id, 'status' => 1 ]
                    );
                    
                    if(!empty($paymentResult)){
                        $returnData = $paymentResult;
                    }
                } elseif (count($forCategory) > 1) {
                    foreach ($forCategory as $index => $element) {
                        $category_id[] = $element['id'];
                    }
                    $catgoryNameArray = implode(', ', VideoCategory::whereIn('id',$category_id)->get()->pluck('category_name')->toArray());
                    $bundle['payment_method']            = 5; //strip
                    $bundle['paid_by']                   = $user->id;
                    $bundle['video_category_name']       = $catgoryNameArray;
                    $bundle['video_category_id']        = implode(',', $category_id);
                    $bundle['access_platform']           = 1;
                    $bundle['transaction_reference']     = $charge->balance_transaction;
                    $bundle['paid_for']                  = 3;
                    $bundle['remarks']                   = 'New subscription';
                    $bundle['payment_amount']            = $amount;
                    if(array_key_exists('promo_code', $Input) && isset($Input['promo_code'])){   
                        $bundle['promo_code']                = $Input['promo_code'];
                        $bundle['discount_type']             = $Input['discount_type'];
                        $bundle['discount_of']               = $Input['discount_of'];
                        if($promoData!=''&&$promoData['code_for']==0&&isset($Input['promo_code_id']))
                            $bundle['promo_code_id']             = $Input['promo_code_id'];
                    }
                    $bundle['payment_status']            = 1;
                    $bundle['slug']                      = $user->slug;
                    if(!empty($addToPayment)){
                        $bundle['distributor_id'] = $addToPayment['distributor_id'];
                        $bundle['distributer_team_member_id'] = $addToPayment['distributor_team_member_id'];
                    }
                    $bundle['payment_amount_type'] = $priceType;
                    $bundle['plan_status'] = 1;
                    $bundle['system_transaction_number'] = UtilityController::GenerateRunningNumber('MarketPlacePayment','system_transaction_number',"VL");
                    $bundleResult = UtilityController::Makemodelobject($bundle,'MarketPlacePayment');

                    if(!empty($bundleResult) && array_key_exists('id', $bundleResult->toArray())){
                        foreach ($forCategory as $key => $value) {
                            $history_id = VideoCategoryHistory::where('category_id',$value['id'])->latest()->first(); 
                            $catgoryNameArray = implode(', ', VideoCategory::where('id',$value['id'])->get()->pluck('category_name')->toArray());
                            $payment[$key]['payment_method']            = 5; //strip
                            $payment[$key]['paid_by']                   = $user->id;
                            $payment[$key]['video_category_id']         = $value['id'];
                            $payment[$key]['history_id']                = $history_id['id'];
                            $payment[$key]['video_category_name']       = $catgoryNameArray;
                            if($value['plan_duration'] == 3){
                                $payment[$key]['subscription_end_date']     = Carbon::now()->addMonths(3)->subDays(1);
                                $payment[$key]['plan_duration']             = 3;
                            } elseif ($value['plan_duration'] == 6) {
                                $payment[$key]['subscription_end_date']     = Carbon::now()->addMonths(6)->subDays(1);
                                $payment[$key]['plan_duration']             = 6;
                            } else {
                                $payment[$key]['subscription_end_date']     = Carbon::now()->addYears(1)->subDays(1);
                                $payment[$key]['plan_duration']             = 12;
                            }
                            $payment[$key]['remarks']                   = 'New subscription';
                            $payment[$key]['access_platform']           = 1;
                            $payment[$key]['transaction_reference']     = $charge->balance_transaction;
                            $payment[$key]['paid_for']                  = 3;
                            // $payment['payment_amount']         = $paymentFor['category_price'];
                            $payment[$key]['payment_amount']            = $amount;
                            if(array_key_exists('promo_code', $Input) && isset($Input['promo_code'])){   
                                $payment[$key]['promo_code']                = $Input['promo_code'];
                                $payment[$key]['discount_type']             = $Input['discount_type'];
                                $payment[$key]['discount_of']               = $Input['discount_of'];
                                if($promoData!=''&&$promoData['code_for']==0&&isset($Input['promo_code_id']))
                                    $payment[$key]['promo_code_id']             = $Input['promo_code_id'];
                            }
                            $payment[$key]['payment_status']            = 1;
                            $payment[$key]['slug']                      = $user->slug;
                            if(!empty($addToPayment)){
                                $payment[$key]['distributor_id'] = $addToPayment['distributor_id'];
                                $payment[$key]['distributer_team_member_id'] = $addToPayment['distributor_team_member_id'];
                            }
                            $payment[$key]['payment_amount_type'] = $priceType;
                            $payment[$key]['system_transaction_number'] = $bundleResult['system_transaction_number'];
                            $payment[$key]['parent_id'] = $bundleResult['id'];
                            $payment[$key]['plan_status'] = 1;
                            $paymentResult = UtilityController::Makemodelobject($payment[$key],'MarketPlacePayment');
                            $paymentResult['url']          = URL::to('/');
                            ##Job Manage
                            $Ids = DB::table('job_manage')->where('ignite_category_id',$paymentResult['video_category_id'])->where('user_id',$user->id)->pluck('job_id')->toArray();
                            if(!empty($Ids)) {
                                DB::table('jobs')->whereIn('id',$Ids)->delete();
                            }
                            ## Live
                            if($paymentResult['plan_duration'] == 12){
                                $IgniteCategory = (new IgniteCategoryPayment($paymentResult->toArray()))->delay(Carbon::now()->addYears(1)->subDays(1));
                            } elseif ($paymentResult['plan_duration'] == 6) {
                                $IgniteCategory = (new IgniteCategoryPayment($paymentResult->toArray()))->delay(Carbon::now()->addMonths(6)->subDays(1));
                            } elseif ($paymentResult['plan_duration'] == 3) {
                                $IgniteCategory = (new IgniteCategoryPayment($paymentResult->toArray()))->delay(Carbon::now()->addMonths(3)->subDays(1));
                            }
                            // ## Test
                            // $IgniteCategory = (new IgniteCategoryPayment($paymentResult->toArray()))->delay(Carbon::now()->addMinutes(3));
                            $jobId = dispatch($IgniteCategory);
                            DB::table('job_manage')->where('ignite_category_id',$paymentResult['video_category_id'])->where('user_id',$user->id)->delete();
                            DB::table('job_manage')->insert(
                                ['ignite_category_id' => $paymentResult['video_category_id'], 'job_id' => $jobId,'user_id' => $user->id, 'status' => 1]
                            );
                            
                            ## Get tax rate details 
                            $taxrateData = TaxRate::latest()->first();

                            ## User Notify recurring payment for email
                            $categoryData = VideoCategory::with('subject')->where('id',$value['id'])->where('status',1)->first();

                            ## User Notify recurring payment for email
                            if($paymentResult['payment_amount_type'] == 1){
                                if($paymentResult['plan_duration'] == 12){
                                    $category_price = $categoryData->category_price;
                                } elseif ($paymentResult['plan_duration'] == 6) {
                                    $category_price = $categoryData->category_price_6month;
                                } elseif ($paymentResult['plan_duration'] == 3) {
                                    $category_price = $categoryData->category_price_3month;
                                }
                            } elseif ($paymentResult['payment_amount_type'] == 2) {
                                if($paymentResult['plan_duration'] == 12){
                                    $category_price = $categoryData->category_price_sgd;
                                } elseif ($paymentResult['plan_duration'] == 6) {
                                    $category_price = $categoryData->category_price_sgd_6month;
                                } elseif ($paymentResult['plan_duration'] == 3) {
                                    $category_price = $categoryData->category_price_sgd_3month;
                                }
                            }

                            if(!is_null($taxrateData)){
                                $taxrate_price = ($category_price*$taxrateData->tax_rate)/100;
                                $emailAmount = $category_price + $taxrate_price;
                            } else {
                                $emailAmount = $category_price;
                            }
                            
                            ## Delete Old category Cron Job Notify email and Notify email Ids
                            $id = StoreEmail::where('user_id',$user->id)->where('category_id',$paymentResult['video_category_id'])->pluck('job_id')->toArray();
                            if(!empty($id)){
                                foreach($id as $index => $emailId){
                                    $email_job = explode(',', $emailId);
                                    $email_job_delete = DB::table('jobs')->whereIn('id',$email_job)->delete();
                                    $store_email_delete = StoreEmail::where('user_id',$user->id)->where('category_id',$paymentResult['video_category_id'])->where('job_id',$emailId)->delete();
                                }
                            }

                            $emailDetails['email_address']          = $user['email_address'];
                            $emailDetails['display_name']           = $user['display_name'];
                            $emailDetails['subscription_end_date']  = $paymentResult['subscription_end_date'];
                            $emailDetails['payment_amount_type']    = $paymentResult['payment_amount_type'];
                            $emailDetails['amount']                 = $emailAmount;
                            $emailDetails['video_category_id']      = $paymentResult['video_category_id'];
                            $emailDetails['user_id']                = $user->id;
                            $emailDetails['plan_duration']          = $paymentResult['plan_duration'];
                            $emailDetails['video_category_name']    = $paymentResult['video_category_name'];
                            $emailDetails['starting_date']          = Carbon::now()->timezone('Asia/Singapore')->format('d M Y');
                            $emailDetails['subject_name']           = $categoryData->subject['subject_display_name'];
                            $emailDetails['subject_image']          = URL::to('/public/uploads/subject_images/'.$categoryData->subject['subject_image']);
                            $emailDetails['review_url']             = URL::to('/my-subscription');
                            ## Live
                            $EmailForNotifyFirst  = (new EmailForNotify($emailDetails))->delay($paymentResult['subscription_end_date']->subDays(7));
                            // ## Test 
                            // $EmailForNotifyFirst  = (new EmailForNotify($emailDetails))->delay(Carbon::now()->addMinutes(1));
                            $job['first'] = dispatch($EmailForNotifyFirst);
                            ## Live
                            $EmailForNotifySecond = (new EmailForNotify($emailDetails))->delay($paymentResult['subscription_end_date']->subMonths(1));
                            // ## Test 
                            // $EmailForNotifySecond  = (new EmailForNotify($emailDetails))->delay(Carbon::now()->addMinutes(2));
                            $job['second'] = dispatch($EmailForNotifySecond);
                            $job_email = implode(',', $job);
                            DB::table('store_email')->insert(
                                ['category_id' => $paymentResult['video_category_id'], 'job_id' => $job_email,'user_id' => $user->id , 'status' => 1 ]
                            );
                        }
                    }
                    if(!empty($bundleResult)){
                        $returnData = $bundleResult;
                    }
                }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return $returnData;
    }


    //###############################################################
    //Function Name: Getsubjects
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       Get the subjects for the subscriber to select from and load its categories to proceed to the video listing page
    //In Params:     void
    //Return:        json
    //Date:          4th Feb, 2018
    //###############################################################
    public function Getsubjects(Request $request){     
        try {
            if(Auth::check()){
                $userZoneId = Auth::user()->zone_id;
                $subject = Subject::all();
                foreach($subject as $sub) {
                    $zones = explode(',', $sub['zones']);
                    if(in_array($userZoneId, $zones)) {
                        $subjectId[] = $sub['id'];
                    }
                }
                if(isset($subjectId)) {
                    $subjects = Subject::where('status',1)->whereIn('id', $subjectId)->get();
                    if($subjects->count()>0){
                        foreach ($subjects as $key => $value) {
                            $value['encoded_string'] = 'subject_'.base64_encode($value['id']);
                        }
                        $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $subjects);
                    } else{
                        $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 0);
                    }
                } else {
                    $returnData = UtilityController::Generateresponse(false, 'NO_SUBJECTS', 200);
                }
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: Getcategoriesforsubject
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       Get the categories for the subscriber to select from and load its sub-categories if exists or redirect to video listing page
    //In Params:     void
    //Return:        json
    //Date:          4th Feb, 2018
    //###############################################################
    public function Getcategoriesforsubject(Request $request){     
        try {
            if(Auth::check()){
                $Input = Input::all();
                $Input['id'] = base64_decode(explode('_', $Input['id'])[1]);
                $categories = VideoCategory::where('subject_id',$Input['id'])->where('status',1)->whereNull('parent_id')->get();
                if($categories->count()>0){
                    foreach ($categories as $key => $value) {
                        $value['encoded_string'] = 'category_'.base64_encode($value['id']);
                    }
                    $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $categories);
                } else {
                    $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 0);
                }
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    public function getCategories(){     
        try {
            if(Auth::check()){
                $userZone = Auth::user()->zone_id;
                $categories = VideoCategory::where('status',1)->whereNull('parent_id')->with('subjectZones')->get();
            }else{
                $categories = VideoCategory::where('status',1)->whereNull('parent_id')->get();
            }
            if($categories->count()>0){
                foreach ($categories as $key => $value) {
                    $value['encoded_string'] = 'category_'.base64_encode($value['id']);
                    $value['icon_path'] = Config('constants.path.ICON_URL').$value['category_icon'];
                }
                // $categories['loggedIn'] = Auth::check();
                $user = Auth::user();
                $returnData = UtilityController::Generateresponse(true, $user, Response::HTTP_OK, $categories);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 0);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return $returnData;
    }

    //###############################################################
    //Function Name: getCategoriesForSubjectFromSubjectId
    //Author:        Sumit Advani <sumit@creolestudios.com>
    //Purpose:       Get the categories for the subscriber to select from and load its sub-categories
    //In Params:     void
    //Return:        json
    //Date:          14th June, 2019
    //###############################################################
    public function getCategoriesForSubjectFromSubjectId(Request $request){     
        try {
            if(Auth::check()){
                $Input = Input::all();
                if(isset($Input['id'])) {
                    $Input['id'] = explode('_', base64_decode($Input['id']))[1];
                } else {
                    if( ! is_null(Auth::user()->video_preference) && ! empty(Auth::user()->video_preference)) {
                        $Input['id'] = explode(':', base64_decode(Auth::user()->video_preference));
                        $Input['id'] = explode('_', base64_decode($Input['id'][0]));
                        if(in_array(1, array_keys($Input['id']))) {
                            $Input['id'] = $Input['id'][1];
                        }
                    } else {
                        $returnData = UtilityController::Generateresponse(false, 'REDIRECT_TO_LIST', Response::HTTP_PERMANENTLY_REDIRECT);
                    }
                }
                $categories = VideoCategory::where('subject_id',$Input['id'])->where('status',1)->whereNull('parent_id')->get();
                if($categories->count()>0){
                    foreach ($categories as $key => $value) {
                        $value['encoded_string'] = 'category_'.base64_encode($value['id']);
                    }
                    $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', Response::HTTP_OK, $categories);
                } else {
                    $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 0);
                }
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return $returnData;
    }

    //###############################################################
    //Function Name: Getsubcategories
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       Get the sub-categories for the subscriber to select from and load its sub-categories if exists or redirect to video listing page
    //In Params:     void
    //Return:        json
    //Date:          4th Feb, 2018
    //###############################################################
    public function Getsubcategories(Request $request){     
        try {
            if(Auth::check()){
                $Input = Input::all();
                if(isset($Input['id']) && $Input['id'] != ''){
                    $Input['id'] = base64_decode(explode('_', $Input['id'])[1]);
                    $subCategories = VideoCategory::where('parent_id',$Input['id'])->where('on_detail_page',1)->where('status',1)->get();
                    if($subCategories->count()>0){
                        foreach ($subCategories as $key => $value) {
                            $value['encoded_string'] = 'subcategory_'.base64_encode($value['id']);
                        }
                        $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $subCategories);
                    } else {
                        $returnData = UtilityController::Generateresponse(false, $Input['language']=='en'?'NO_DATA_FOR_CATEGORY':'NO_DATA_FOR_CATEGORY_CHINESE', 0);
                    }
                }
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    public function Getsubcategoriesforlandingpage(){     
        try {
            $subCategories = VideoCategory::where('on_landing_page',1)->whereNotNull('parent_id')->where('status',1)->get();    
            if($subCategories->count()>0){
                foreach ($subCategories as $key => $value) {
                    $value['encoded_string'] = 'subcategory_'.base64_encode($value['id']);
                    $value['icon_path'] = Config('constants.path.ICON_URL').$value['sub_category_icon'];
                }
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $subCategories);
            } else {
                $returnData = UtilityController::Generateresponse(false, $Input['language']=='en'?'NO_DATA_FOR_CATEGORY':'NO_DATA_FOR_CATEGORY_CHINESE', 0);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: selectLanguageForSubject
    //Author:        Sumit Advani <sumit@creolestudios.com>
    //Purpose:       To get language for subject selected
    //In Params:     Subject ID
    //Return:        json
    //Date:          18th Feb, 2019
    //###############################################################
    public function selectLanguageForSubject(Request $request)
    {
        try {
            if(Auth::check()){
                $Input = Input::all();
                $languageIds = Subject::select('audio_language')->where('id', $Input['id'])->first()->toArray();
                $languageForZone = ZoneLanguage::whereIn('id',explode(',', $languageIds['audio_language']))->get()->toArray();
                $data['languageForZone'] = $languageForZone;
                $data['seeVideosFor'] = base64_encode('category_'.$Input['id']);
                $returnData = UtilityController::Generateresponse(false, 'REDIRECT_TO_LIST', Response::HTTP_PERMANENTLY_REDIRECT, $data);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);   
    }

    //###############################################################
    //Function Name: Savecategoryandlanguage
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To save prefered category ang language selected by user to watch videos
    //In Params:     selected language and encrypted category
    //Return:        json
    //Date:          18th Feb, 2019
    //###############################################################
    public function Savecategoryandlanguage(Request $request){
        try {
            $Input = Input::all();
            if(Auth::check()){
                $videoPreference['video_preference'] = base64_encode($Input['see_videos_for'].':'.$Input['see_videos_in']);
                \DB::beginTransaction();
                $updateResult = User::where('id',Auth::user()->id)->update($videoPreference);
                if($updateResult){
                    $returnData = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', Response::HTTP_OK, $videoPreference);
                }else{
                    $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', Response::HTTP_OK);
                }
                \DB::commit();
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return $returnData;
    }
    //###############################################################
    //Function Name : Activeamember
    //Author        : Jainam Shah
    //Purpose       : To reactive a team member
    //In Params     : Team member id
    //Return        : Json Encoded Data
    //Date          : 1st March, 2019
    //###############################################################
    public function Activeamember(Request $request)
    {
       try {
           if (Auth::guard('distributors')) {
               \DB::beginTransaction();
               $Input = Input::all();
               $activeTeamMember = DistributorsTeamMembers::where('id',$Input['id'])->update(['status'=>1]);
               $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1);
               \DB::commit();
           } else {
               $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
           }
       } catch (\Exception $e) {
           \DB::rollback();
           $sendExceptionMail = UtilityController::Sendexceptionmail($e);
           $responseArray     = UtilityController::Generateresponse(false, $e->getFile().':'.$e->getLine().':'.$e->getMessage(), 0);
       }
       return response()->json($responseArray);
    }

    //###############################################################
    //Function Name: Getzonelanguage
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To get user's zone available language and preference category if selected
    //In Params:     void
    //Return:        json
    //Date:          19th Feb, 2019
    //###############################################################
    public function Getzonelanguage(Request $request){     
        try {
            if(Auth::check()){
                $user = Auth::user();
                $subjectId = explode('_',base64_decode(explode(':', base64_decode($user->video_preference))[0]))[1];
                /*$getSubject = VideoCategory::select('subject_id')->where('id',$subjectId)->first()->toArray();*/
                $languageForZone = Subject::select('audio_language')->where('id', $subjectId)->first()->toArray();
                $languageForZone = ZoneLanguage::whereIn('id',explode(',', $languageForZone['audio_language']))->get()->toArray();
                $data['languageForZone'] = $languageForZone;
                $data['see_videos_for'] = explode(':', base64_decode($user->video_preference))[0];
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $data);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: Getuserzone
    //Author:        Jainam Shah <jainam@creolestudios.com>
    //Purpose:       To get user's zone
    //In Params:     void
    //Return:        json
    //Date:          28th May, 2019
    //###############################################################
    public function Getuserzone() {
        try {
            if(Auth::check()){
                $user = Auth::user();
                $data = Zone::find($user->zone_id);
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $data);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: GetUserTokenForTwinkle
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To get user's jwt token for passing it to smartJen
    //In Params:     void
    //Return:        json
    //Date:          7th May, 2020
    //###############################################################
    public function GetUserTokenForTwinkle() {
        try {
            if(Auth::check()){
                $token = session('user_mdstr');
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $token);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //##########################################################################
    //Function Name: WantToPurchaseData
    //Author:        Jainam Shah
    //Purpose:       To get data of video category id which user want's to buy
    //In Params:     slug
    //Return:        json
    //Date:          6th March, 2019
    //##########################################################################
    public function WantToPurchaseData(Request $request) {
        try {
            if(Auth::check()){
                $getId = base64_decode(explode(':', $request->slug)[1]);
                $slug=explode(':', $request->slug);
                if(count($slug)>2){
                    $content_type = explode(':', $request->slug)[2];
                }
                $checkSlug = explode(':', $request->slug)[0];
                $getName = explode('-', $checkSlug)[0];
                if(strtolower($getName) == 'quiz') {
                    $videoCategoryId = Quizzes::where('id', $getId)->first();
                    $videoCategoryId = $videoCategoryId->category_id;
                } else if(strtolower($getName) == 'worksheet') {
                    $videoCategoryId = Worksheets::where('id', $getId)->first();
                    $videoCategoryId = $videoCategoryId->worksheet_category_id;
                }else if(strtolower(base64_decode($getName)) == 'category') {
                    $videoCategoryId = base64_decode(explode(':', $request->slug)[1]);
                } else {
                    if(isset($content_type) && $content_type==1){
                        $videoCategoryId = Videos::where('id', $getId)->first()->toArray();
                        $videoCategoryId = $videoCategoryId['video_category_id'];  
                    }else if(isset($content_type) && $content_type==2){
                        $videoCategoryId = Quizzes::where('id', $getId)->first();
                        $videoCategoryId = $videoCategoryId->category_id;  
                    }else if(isset($content_type) && $content_type==3){
                        $videoCategoryId = Worksheets::where('id', $getId)->first();
                        $videoCategoryId = $videoCategoryId->worksheet_category_id;
                    }else{
                        $videoCategoryId = Videos::where('id', $getId)->first();
                        $videoCategoryId = $videoCategoryId->video_category_id;
                    }
                }
                // $data['purchase'] = VideoCategory::where('id', $videoCategoryId)->first();
                // $subjectId = $data['purchase']['subject_id'];
                // $data['purchase']['encoded_string'] = 'subject_'.base64_encode($subjectId);
                ## User has already purchased a plan for that subject, then that user cannot purchase another plan for that subject 
                $subject_id = VideoCategory::where('id',$videoCategoryId)->pluck('subject_id')->toArray();
                if(!empty($subject_id)){
                    $category_ids = VideoCategory::whereIn('subject_id',$subject_id)->whereNull('parent_id')->where('status',1)->pluck('id')->toArray();
                    if(!empty($category_ids)){
                        $checkPurCategory = MarketPlacePayment::select('*')->whereIn('video_category_id',$category_ids)->where('subscription_end_date','>=',Carbon::now()->timezone('Asia/Singapore'))->where('paid_by', Auth::user()->id)->where('paid_for', 3)->where('payment_status', 1)->exists();
                        if($checkPurCategory){
                            $data['purchase'] = null;
                        } else {
                            $data['purchase'] = VideoCategory::where('id', $videoCategoryId)->first();
                            $subjectId = $data['purchase']['subject_id'];
                            $data['purchase']['encoded_string'] = 'subject_'.base64_encode($subjectId);
                        }
                    }
                }
                // $data['allVideoCategory'] = VideoCategory::where('subject_id', $subjectId)->where('status',1)->whereNull('parent_id')->get();
                $data['allVideoCategory'] = VideoCategory::where('subject_id', $subjectId)->where('status',1)->get(); //this is according to the new changes
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $data);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //##########################################################################
    //Function Name: getCategoriesWithLanguages
    //Author:        Jainam Shah
    //Purpose:       To get data of categories and their languages
    //In Params:     void
    //Return:        json
    //Date:          2nd August, 2019
    //##########################################################################
    public function getCategoriesWithLanguages()
    {
        try {
            $userAuth = Auth::user();
            if($userAuth){ 
                $userZoneId = $userAuth->zone_id;
                $userId = $userAuth->id;
                $subject = Subject::all();
                foreach($subject as $sub) {
                    $zones = explode(',', $sub['zones']);
                    if(in_array($userZoneId, $zones)) {
                        $subjectId[] = $sub['id'];
                    }
                }
                if(isset($subjectId)) {
                    $subjects = Subject::where('status',1)->whereIn('id', $subjectId)->get()->toArray();
                    if(count($subjects) > 0) {
                        foreach ($subjects as $key => $subject) {
                            $languages[$key] = ZoneLanguage::select('id', DB::raw($subject['id']." as subject_id"),'language as language_name','language_translation as language_display_name', DB::raw("'1' as is_lower_level_cat"))->whereIn('id',explode(',', $subject['audio_language']))->get()->toArray();
                            $subjects[$key]['is_lower_level_cat'] = 1;
                            $subjects[$key]['categories'] = $languages[$key];
                            $subjects[$key]['seeVideosFor'] = base64_encode('category_'.$subject['id']);

                        }
                        $subjects['static'] = Subject::where('status',1)->whereIn('id', $subjectId)->where(function ($query) {
                                        $query->Where('subject_name', 'like', '%SMIL%')
                                                ->orWhere('subject_name', 'like', '%smil%')
                                                ->orWhere('subject_name', 'like', '%MAZE%')
                                                ->orWhere('subject_name', 'like', '%maze%');
                                            })->get();
                        $subjects['staticget'] = Subject::where('status',1)->whereIn('id', $subjectId)->where(function ($query) {
                                        $query->Where('subject_name', 'like', '%BUZZ%')
                                                ->orWhere('subject_name', 'like', '%buzz%');
                                            })->get(); 
                    }
                    $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', Response::HTTP_OK, $subjects);
                } else {
                    $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', Response::HTTP_OK);
                }
            }else{
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', Response::HTTP_OK);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', Response::HTTP_BAD_REQUEST, '');
        }           
        return response($returnData);
    }

    //###############################################################
    //Function Name: Getapkdownloadlink
    //Author:        Karan Kantesariya <karan@creolestudios.com>
    //Purpose:       To get APK download link
    //In Params:     void
    //Return:        json
    //Date:          11th June, 2020
    //###############################################################
    public function Getapkdownloadlink() {
        try {
                $link = VersionControl::select('link')->where('device_type', 'A')->orderBy("id","DESC")->first();
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $link);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name : GettaxrateDetails
    //Author        : Karan Kantesariya <karan@creolestudios.com>
    //Purpose       : Get the data taxrate for subscription module
    //In Params     : Void
    //Return        : 
    //Date          : 14th May 2021
    //###############################################################
    public function GettaxrateDetails(){

        try {
            if(Auth::check()){
                $result = TaxRate::latest()->first();
                if($result) {
                    $responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$result);
                } else {
                    $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);    
    }

    //###############################################################
    //Function Name : IgniteMysubscriptiondata
    //Author        : Karan Kantesariya <karan@creolestudios.com>
    //Purpose       : Get the data of old and new subscription purchased by the user
    //In Params     : Void
    //Return        : 
    //Date          : 08th Jan 2021
    //###############################################################
    public function IgniteMysubscriptiondata(){

        try {
            if(Auth::check()){
                $todaysDate = Carbon::now()->timezone('Asia/Singapore');
                $result     = MarketPlacePayment::select('*')->with(['video_categories' => function ($query) {
                                $query->with('subject')->get();
                            },
                ])->with('promocode')->where('subscription_end_date','>=',$todaysDate)->where('paid_by', Auth::user()->id)->where('paid_for', 3)->where('payment_status', 1)->get()->toArray();
                if($result){
                    foreach ($result as $key => $value) {
                        $video_category_id = VideoCategory::where('subject_id',$value['video_categories']['subject_id'])->whereNull('parent_id')->where('status',1)->pluck('id')->toArray();
                        $data     = MarketPlacePayment::select('*')->with(['video_categories' => function ($query) {
                                    $query->with('subject')->get();
                                },
                            ])->whereIn('video_category_id',$video_category_id)->where('paid_by', Auth::user()->id)->where('paid_for', 3)->where('payment_status', 1)->where('upcoming_plan',1)->where('plan_status',1)->get();
                        $result[$key]['upcoming_plan'] = $data;
                        $result[$key]['paymentstatus'] = ($value['payment_status'] == 1 ? 'Successful' :  'Failed');
                        $check = JobManage::where('user_id',$value['paid_by'])->where('ignite_category_id',$value['video_category_id'])->where('status',1)->exists();
                        if($check){
                            $result[$key]['opt_out_status'] = 1;
                        } else {
                            $result[$key]['opt_out_status'] = 0;
                        }
                        if($value['payment_amount_type'] == 1){
                            if($value['plan_duration'] == 12){
                                $result[$key]['category_price'] = $value['video_categories']['category_price']; 
                            } elseif ($value['plan_duration'] == 6) {
                                $result[$key]['category_price'] = $value['video_categories']['category_price_6month'];
                            } elseif ($value['plan_duration'] == 3) {
                                $result[$key]['category_price'] = $value['video_categories']['category_price_3month'];
                            }
                        } elseif ($value['payment_amount_type'] == 2) {
                            if($value['plan_duration'] == 12){
                                $result[$key]['category_price'] = $value['video_categories']['category_price_sgd']; 
                            } elseif ($value['plan_duration'] == 6) {
                                $result[$key]['category_price'] = $value['video_categories']['category_price_sgd_6month'];
                            } elseif ($value['plan_duration'] == 3) {
                                $result[$key]['category_price'] = $value['video_categories']['category_price_sgd_3month'];
                            }
                        }

                        if(is_null($value['system_transaction_number'])){
                            if(!is_null($value['switch_id'])){
                                $system_transaction_number = MarketPlacePayment::where('id',$value['switch_id'])->pluck('system_transaction_number')->first();
                                $result[$key]['system_transaction_number'] = $system_transaction_number;
                            }
                        }
                    }
                    $responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$result);
                }
                else{
                    $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);    
    }

    //###############################################################
    //Function Name : IgniteHistorysubscriptiondata
    //Author        : Karan Kantesariya <karan@creolestudios.com>
    //Purpose       : Get the data of old purchased by the user
    //In Params     : Void
    //Return        : 
    //Date          : 23th March 2021
    //###############################################################
    public function IgniteHistorysubscriptiondata(){

        try {
            if(Auth::check()){
                $user = Auth::user();
                $subject = Subject::get();
                $subject_id = array();
                foreach ($subject as $key => $subject) {
                    $ids = explode(',', $subject->zones);
                    if(in_array($user->zone_id, $ids)){
                        array_push($subject_id, $subject->id);
                    }
                }
                $result = array();
                foreach($subject_id as $index => $data){
                    $video_categories_ids = VideoCategory::where('subject_id',$data)->whereNull('parent_id')->where('status',1)->get()->pluck('id')->toArray();
                    if($video_categories_ids){
                        $subscriptionPlanHistory = MarketPlacePayment::with(['video_categories' => function ($query) {
                                $query->with('subject')->get();
                            },
                            ])->with('switching_category')->whereNull('switch_id')->where('subscription_end_date','<',Carbon::now()->timezone('Asia/Singapore'))->whereIn('video_category_id',$video_categories_ids)->where('paid_by',$user->id)->where('paid_for',3)->where('upcoming_plan',0)->where('payment_status',1)->where('plan_status',1)->orderBy("id","DESC")->limit(1)->get()->toArray();
                        
                        foreach ($subscriptionPlanHistory as $subscriptionPlanHistory) {
                            if(!is_null($subscriptionPlanHistory)){
                                ## Check switching plan history
                                if(!empty($subscriptionPlanHistory['switching_category'])){
                                    foreach ($subscriptionPlanHistory['switching_category'] as $key => $value) {
                                        if(Carbon::parse($value['subscription_end_date']) < Carbon::now()->timezone('Asia/Singapore')){
                                            array_push($result, $subscriptionPlanHistory);
                                        } else {
                                            array_push($result, $subscriptionPlanHistory);
                                        }
                                    }
                                } else {
                                    array_push($result, $subscriptionPlanHistory);
                                }
                            }
                        }

                    }
                }
                if($result){
                    foreach ($result as $key => $value) {
                        $result[$key]['paymentstatus'] = ($value['payment_status'] == 1 ? 'Successful' :  'Failed');
                    }
                    $responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$result);
                } else {
                    $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);    
    }

    //###############################################################
    //Function Name : IgniteTransactionsnData
    //Author        : Karan Kantesariya <karan@creolestudios.com>
    //Purpose       : Get the data all subscription by the user
    //In Params     : Void
    //Return        : 
    //Date          : 23th March 2021
    //###############################################################
    public function IgniteTransactionsnData(){

        try {
            if(Auth::check()){
                $result     = MarketPlacePayment::select('*')->with(['video_categories' => function ($query) {
                                $query->with('subject')->get();
                                // $query->with('subject')->groupby('subject_id')->distinct()->get();
                            },
                ])->where('paid_by', Auth::user()->id)->where('paid_for', 3)->where('payment_status', 1)->whereNull('parent_id')->whereNull('switch_id')->whereNotNull('access_platform')->get()->toArray();
                if($result){
                    foreach ($result as $key => $value) {
                        $result[$key]['paymentstatus'] = ($value['payment_status'] == 1 ? 'Successful' :  'Failed');
                    }
                    $responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$result);
                }
                else{
                    $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);    
    }

    //###############################################################
    //Function Name: SwitchPlan
    //Author:        Karan Kantesariya <karan@creolestudios.com>
    //Purpose:       Switch Plan category id
    //In Params:     subject id, purchase category id
    //Return:        json
    //Date:          05th Jan, 2021
    //###############################################################
    public function SwitchPlan(Request $request) {
        try {
            $Input = Input::all();
            $userAuth = Auth::user();
            if($userAuth){
                $count = MarketPlacePayment::where('paid_by',$userAuth->id)->whereNotNull('switch_id')->count();
                if($count >= 2){
                    $error['error'] = "Your Switching Plan are expired";
                    $returnData = UtilityController::Generateresponsewithmessage(false, $error, '', '');
                } else {
                    \DB::beginTransaction();
                    $date['subscription_end_date'] = Carbon::now();
                    $date['is_cancelsubscription'] = 1;

                    $checkForStatus = MarketPlacePayment::where('paid_by',$userAuth->id)->where('subscription_id',$Input['main_category_id'])->exists();

                    ## Old User purchase Category Details 
                    $planDetails = MarketPlacePayment::where('id',$Input['main_category_id'])->first();
                    $jobManageDetails = DB::table('job_manage')->where('ignite_category_id',$planDetails->video_category_id)->where('user_id',$userAuth->id)->first();
                    if(isset($jobManageDetails)){
                        if($jobManageDetails->status == 0){
                            $checkForSwitchStatus = true;
                        } else {
                            $checkForSwitchStatus = false;
                        }
                    }

                    ## Old User purchase Category Details 
                    $renewal_categoryDetails = MarketPlacePayment::where('renewal_id',$Input['main_category_id'])->orderBy('id','DESC')->get()->toArray();
                    if(!empty($renewal_categoryDetails) && $renewal_categoryDetails != ''){
                        $end_date = $renewal_categoryDetails[0]['subscription_end_date'];
                        $old_categorydetails = $renewal_categoryDetails;
                    } else {
                        $old_categorydetails = MarketPlacePayment::select('*')->where('id',$Input['purchase_category_id'])->get()->toArray();
                        $end_date   = $old_categorydetails[0]['subscription_end_date'];
                    }
                    $subscription_end_date = Carbon::parse($end_date)->timezone(Auth::user()->timezone);
                    if($subscription_end_date >= Carbon::now()){
                        ## History Category Details
                        $history_id = VideoCategoryHistory::select('id')->where('category_id',$Input['switch_categoryid'])->latest()->first();
                        ## Switch Category details
                        $catagoryDetails = VideoCategory::where('id',$Input['switch_categoryid'])->get()->toArray();
                        $payment['payment_method']            = 5;
                        $payment['paid_by']                   = $userAuth->id;
                        $payment['video_category_id']         = $Input['switch_categoryid'];
                        $payment['history_id']                = $history_id['id'];
                        $payment['video_category_name']       = $catagoryDetails[0]['category_name'];
                        if(!empty($old_categorydetails[0]['distributor_id'])){
                            $payment['distributor_id']            = $old_categorydetails[0]['distributor_id'];
                        }
                        if(!empty($old_categorydetails[0]['distributor_team_member_id'])){
                            $payment['distributer_team_member_id'] = $old_categorydetails[0]['distributor_team_member_id'];
                        }
                        $payment['subscription_end_date']     = $old_categorydetails[0]['subscription_end_date'];
                        $payment['switch_id']                 = $Input['main_category_id'];
                        $payment['remarks']                   = 'Switching';
                        $payment['access_platform']           = 1;
                        $payment['paid_for']                  = 3;
                        $payment['payment_amount_type']       = $old_categorydetails[0]['payment_amount_type'];
                        $payment['payment_status']            = 1;
                        $payment['plan_duration']             = 12;  
                        $payment['slug']                      = $userAuth->slug;
                        $payment['plan_status']               = 1;
                        $payment['is_cancelsubscription']     = $checkForStatus?1:0;
                        // $payment['renewal_id']                = !empty($old_categorydetails[0]['renewal_id'])?$old_categorydetails[0]['renewal_id']:$Input['main_category_id'];  
                        ## Create Switching Plan
                        $paymentResult = UtilityController::Makemodelobject($payment,'MarketPlacePayment','','');
                        $paymentResult['url'] = URL::to('/');
                        if(!empty($paymentResult->toArray()) && $paymentResult != ''){
                            $data = MarketPlacePayment::where('subscription_id',$Input['main_category_id'])->where('upcoming_plan',1)->pluck('id')->toArray();
                            $upt = MarketPlacePayment::whereIn('id',$data)->update(['subscription_id' => $paymentResult['id']]);
                        }
                        if(!empty($renewal_categoryDetails) && $renewal_categoryDetails != ''){
                            $update = UtilityController::Makemodelobject($date,'MarketPlacePayment','',$renewal_categoryDetails[0]['id']);
                        } else {
                            $update = UtilityController::Makemodelobject($date,'MarketPlacePayment','',$Input['purchase_category_id']);
                        }               
                        
                        $paymentResult['subscription_end_date'] = Carbon::parse($paymentResult['subscription_end_date'])->timezone(Auth::user()->timezone);
                        $data = MarketPlacePayment::where('id',$Input['main_category_id'])->update(['is_cancelsubscription' => 0 ]);
                        ##Job Manage
                        $Ids = DB::table('job_manage')->where('ignite_category_id',$old_categorydetails[0]['video_category_id'])->where('user_id',$userAuth->id)->pluck('job_id')->toArray();
                        if(!empty($Ids)) {
                            DB::table('jobs')->whereIn('id',$Ids)->delete();
                        }

                        ## Delete Old category Cron Job Notify email and Notify email Ids
                        $id = StoreEmail::where('user_id',$userAuth->id)->where('category_id',$old_categorydetails[0]['video_category_id'])->pluck('job_id')->toArray();
                        if(!empty($id)){
                            foreach($id as $index => $emailId){
                                $email_job = explode(',', $emailId);
                                $email_job_delete = DB::table('jobs')->whereIn('id',$email_job)->delete();
                                $store_email_delete = StoreEmail::where('user_id',$userAuth->id)->where('category_id',$old_categorydetails[0]['video_category_id'])->where('job_id',$emailId)->delete();
                            }
                        }

                        $hours = $paymentResult['subscription_end_date']->diffInHours(Carbon::now());
                        ##Live
                        $IgniteCategory = (new IgniteCategoryPayment($paymentResult->toArray()))->delay($hours);
                        // ##Test
                        // $IgniteCategory = (new IgniteCategoryPayment($paymentResult->toArray()))->delay(Carbon::now()->addMinutes(3));
                        $jobId = dispatch($IgniteCategory);
                        DB::table('job_manage')->where('ignite_category_id',$paymentResult['video_category_id'])->where('user_id',$userAuth->id)->delete();
                        if($checkForStatus){
                            DB::table('job_manage')->insert(
                                ['ignite_category_id' => $paymentResult['video_category_id'], 'job_id' => $jobId,'user_id' => $userAuth->id, 'status' => 0 ]
                            );
                        } elseif ($checkForSwitchStatus) {
                            DB::table('job_manage')->insert(
                                ['ignite_category_id' => $paymentResult['video_category_id'], 'job_id' => $jobId,'user_id' => $userAuth->id, 'status' => 0 ]
                            );
                        } else {
                            DB::table('job_manage')->insert(
                                ['ignite_category_id' => $paymentResult['video_category_id'], 'job_id' => $jobId,'user_id' => $userAuth->id, 'status' => 1 ]
                            );
                        }
                        ## Get taxrate data
                        $taxrateData = TaxRate::latest()->first();
                        ## User Notify recurring payment for email
                        if($paymentResult['payment_amount_type'] == 1){
                            if($paymentResult['plan_duration'] == 12){
                                $details = VideoCategory::with('subject')->where('id',$paymentResult['video_category_id'])->get();
                                if(!is_null($taxrateData)){
                                    $tax_rate = ($details[0]['category_price']*$taxrateData->tax_rate)/100;
                                    $emailAmount = $details[0]['category_price'] + $tax_rate;
                                } else {
                                    $emailAmount = $details[0]['category_price'];
                                }
                            }
                        } elseif ($paymentResult['payment_amount_type'] == 2) {
                            $details = VideoCategory::with('subject')->where('id',$paymentResult['video_category_id'])->get();
                            if(!is_null($taxrateData)){
                                $tax_rate = ($details[0]['category_price_sgd']*$taxrateData->tax_rate)/100;
                                $emailAmount = $details[0]['category_price_sgd'] + $tax_rate;
                            } else {
                                $emailAmount = $details[0]['category_price_sgd'];
                            }
                        }

                        $emailDetails['email_address']          = $userAuth->email_address;
                        $emailDetails['display_name']           = $userAuth->display_name;
                        $emailDetails['subscription_end_date']  = $paymentResult['subscription_end_date'];
                        $emailDetails['payment_amount_type']    = $paymentResult['payment_amount_type'];
                        $emailDetails['amount']                 = $emailAmount;
                        $emailDetails['video_category_id']      = $paymentResult['video_category_id'];
                        $emailDetails['user_id']                = $userAuth->id;
                        $emailDetails['plan_duration']          = $paymentResult['plan_duration'];
                        $emailDetails['video_category_name']    = $paymentResult['video_category_name'];
                        $emailDetails['starting_date']          = Carbon::now()->timezone('Asia/Singapore')->format('d M Y');
                        $emailDetails['subject_name']           = $details[0]->subject['subject_display_name'];
                        $emailDetails['subject_image']           = URL::to('/public/uploads/subject_images/'.$details[0]->subject['subject_image']);
                        $emailDetails['review_url']             = URL::to('/my-subscription');
                        ## Live 
                        $EmailForNotifyFirst  = (new EmailForNotify($emailDetails))->delay($paymentResult['subscription_end_date']->subDays(7));
                        // ## Test
                        // $EmailForNotifyFirst  = (new EmailForNotify($emailDetails))->delay(Carbon::now()->addMinutes(1));
                        $job['first'] = dispatch($EmailForNotifyFirst);
                        ## Live
                        $EmailForNotifySecond = (new EmailForNotify($emailDetails))->delay($paymentResult['subscription_end_date']->subMonths(1));
                        // ## Test
                        // $EmailForNotifySecond  = (new EmailForNotify($emailDetails))->delay(Carbon::now()->addMinutes(2));
                        $job['second'] = dispatch($EmailForNotifySecond);
                        $job_email = implode(',', $job);
                        DB::table('store_email')->insert(
                            ['category_id' => $paymentResult['video_category_id'], 'job_id' => $job_email,'user_id' => $userAuth->id, 'status' => 1 ]
                        );
                        \DB::commit();           
                        $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $paymentResult);
                    } else {
                        $error['error'] = "Plan are expire";
                        $returnData = UtilityController::Generateresponse(true, 'NO_DATA', 3,$error);
                    }
                }
            }else{
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: getSubjectCategory
    //Author:        Karan Kantesariya <karan@creolestudios.com>
    //Purpose:       Subject wise get category list
    //In Params:     subject id
    //Return:        json
    //Date:          05th Jan, 2021
    //###############################################################
    public function getSubjectCategory(Request $request) {
        try {
            $Input = Input::all();
            $userAuth = Auth::user();
            if($userAuth){
                \DB::beginTransaction();
                    $category_list = VideoCategory::where('subject_id',$Input['subject_id'])->whereNull('parent_id')->where('status',1)->get()->toArray();
                    $user_purchase_category = MarketPlacePayment::where('paid_by',$userAuth->id)->where('subscription_end_date','>',Carbon::now())->where('payment_status',1)->get()->pluck('video_category_id')->toArray();
                    foreach ($category_list as $key => $value) {
                        foreach ($user_purchase_category as $index => $element) {
                            if($value['id'] == $element){
                                unset($category_list[$key]);
                            }
                        }
                    } 
                \DB::commit();           
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $category_list);
            }else{
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: CancelSubscription
    //Author:        Karan Kantesariya <karan@creolestudios.com>
    //Purpose:       User Cancel Subscription Plan
    //In Params:     Subscriptoin Plan Id 
    //Return:        json
    //Date:          05th Jan, 2021
    //###############################################################
    public function CancelSubscription(Request $request) {
        try {
            $Input = Input::all();
            $userAuth = Auth::user();
            if($userAuth){
                \DB::beginTransaction();
                if(isset($Input['plan_purchase_id']) && array_key_exists('plan_purchase_id', $Input) && isset($Input['plan_purchase_id']) && array_key_exists('status', $Input)){
                    $details = MarketPlacePayment::where('id',$Input['plan_purchase_id'])->get()->toArray();
                    $subscription_end_date = Carbon::parse($details[0]['subscription_end_date'])->timezone(Auth::user()->timezone);
                    if($subscription_end_date >= Carbon::now()){
                        $Ids = JobManage::where('user_id',$userAuth->id)->where('ignite_category_id',$details[0]['video_category_id'])->update([ 'status' => $Input['status'] ]);
                        // $id = StoreEmail::where('user_id',$userAuth->id)->where('category_id',$details[0]['video_category_id'])->update([ 'status' => $Input['status'] ]);
                        // if($Input['status'] == 0){
                        //     $data = MarketPlacePayment::where('id',$Input['plan_purchase_id'])->update(['is_cancelsubscription' => 1 ]);
                        // } else {
                        //     $data = MarketPlacePayment::where('id',$Input['plan_purchase_id'])->update(['is_cancelsubscription' => 0 ]);
                        // }
                        if($Ids){
                            $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $Ids);
                        }           
                    } else {
                        $error['error'] = "Plan are expire";
                        $returnData = UtilityController::Generateresponse(true, 'NO_DATA', 3,$error);
                    }
                }
                \DB::commit();
            }else{
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name : Applypromocode
    //Author        : Karan Kantesariya <karan@creolestudios.com>
    //Purpose       : Get the data of promocode for recurring payment
    //In Params     : Promocode, Category Id
    //Return        : json
    //Date          : 12th Jan 2021
    //###############################################################
    public function Applypromocode(Request $request){
        try {
            $Input = Input::all();
            $userAuth = Auth::user();
            if($userAuth){
                $renewaldetails = MarketPlacePayment::where('renewal_id',$Input['main_category_id'])->orderBy('id','DESC')->get()->toArray();
                if(!empty($renewaldetails) && $renewaldetails != ''){
                    $subscriptiondetails = $renewaldetails;
                } else {
                    $subscriptiondetails = MarketPlacePayment::where('id',$Input['subscription_id'])->get()->toArray();    
                }
                #Categroy Details
                $CategoryDetails = VideoCategory::where('id',$subscriptiondetails[0]['video_category_id'])->get()->toArray();
                #Amount of purchase category
                if($subscriptiondetails[0]['payment_amount_type'] == 1) {
                    if($subscriptiondetails[0]['plan_duration'] == 3){
                        $amount     = $CategoryDetails[0]['category_price_3month'];
                    } elseif ($subscriptiondetails[0]['plan_duration'] == 6) {
                        $amount     = $CategoryDetails[0]['category_price_6month'];
                    } elseif ($subscriptiondetails[0]['plan_duration'] == 12) {
                        $amount     = $CategoryDetails[0]['category_price'];
                    }
                } else if($subscriptiondetails[0]['payment_amount_type'] == 2) {
                    if($subscriptiondetails[0]['plan_duration'] == 3){
                        $amount     = $CategoryDetails[0]['category_price_sgd_3month'];
                    } elseif ($subscriptiondetails[0]['plan_duration'] == 6) {
                        $amount     = $CategoryDetails[0]['category_price_sgd_6month'];
                    } elseif ($subscriptiondetails[0]['plan_duration'] == 12) {
                        $amount     = $CategoryDetails[0]['category_price_sgd'];
                    }
                }

                $promocode['cart_total']    = $amount;
                $promocode['price_type']    = $subscriptiondetails[0]['payment_amount_type'];
                $promocode['promo']         = $Input['promocode'];
                $promocode['module']        = 0;
                $promocode['user_id']       = $userAuth->id;
                $promocode['language']      = 1;
                $promocode['for_category']  = array('id'=>$subscriptiondetails[0]['video_category_id'],'plan_duration'=>$subscriptiondetails[0]['plan_duration']);
                $paymentOf  = IgniteController::Checkpromocode($promocode);
                $value      = $paymentOf->original['data'];
                $message    = $paymentOf->original['message'];
                if($Input){
                    $promo['promocode']     = $Input['promocode'];
                    $promo['category_id']   = $subscriptiondetails[0]['video_category_id'];
                    $promo['user_id']       = $userAuth->id;
                    $delete = StorePromocode::where('user_id', $promo['user_id'])->where('category_id', $promo['category_id'])->delete();
                    if(!is_null($value) && is_numeric($value)){
                        $result = UtilityController::Makemodelobject($promo,'StorePromocode','','');
                        $result['amount']               = $value;
                        $result['promocode']            = $Input['promocode'];
                        $result['payment_amount_type']  = $subscriptiondetails[0]['payment_amount_type'];
                        $responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$result);
                    } else {
                        $returnMessage = $promocode['language']==1?'PROMO_NOT_APPLICABLE':($Input['language']==2?'PROMO_NOT_APPLICABLE_CHINESE':'PROMO_NOT_APPLICABLE_RU');
                        $responseArray = UtilityController::Generateresponse(false, $returnMessage, Response::HTTP_BAD_REQUEST);   
                    }
                } else {
                    $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);    
    }

    //###############################################################
    //Function Name : Removepromocode
    //Author        : Karan Kantesariya <karan@creolestudios.com>
    //Purpose       : Remove Promocode details
    //In Params     : Promocode, Category Id
    //Return        : json
    //Date          : 17th Feb 2021
    //###############################################################
    public function Removepromocode(Request $request){
        try {
            $Input = Input::all();
            $userAuth = Auth::user();
            if($userAuth){
                // $subscriptiondetails = MarketPlacePayment::where('id',$Input['subscription_id'])->get()->toArray();
                $renewaldetails = MarketPlacePayment::where('renewal_id',$Input['main_category_id'])->orderBy('id','DESC')->get()->toArray();
                if(!empty($renewaldetails) && $renewaldetails != ''){
                    $subscriptiondetails = $renewaldetails;
                } else {
                    $subscriptiondetails = MarketPlacePayment::where('id',$Input['subscription_id'])->get()->toArray();    
                }
                $delete = StorePromocode::where('user_id', $userAuth->id)->where('category_id',$subscriptiondetails[0]['video_category_id'])->where('promocode', $Input['promocode'])->delete();
                if($delete){
                    $result = $delete;
                    $responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$result);
                } else {
                    $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 400, ''); 
                }  
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);    
    }

    //###############################################################
    //Function Name : getPromocodedetails
    //Author        : Karan Kantesariya <karan@creolestudios.com>
    //Purpose       : Get details of user of promocode
    //In Params     : 
    //Return        : json
    //Date          : 17th Feb 2021
    //###############################################################
    public function getPromocodedetails(Request $request){
        try {
            $Input      = Input::all();
            $userAuth   = Auth::user();
            if($userAuth){
                $promoDetails = StorePromocode::where('user_id',$userAuth->id)->where('category_id',$Input['main_category_id'])->get()->toArray();
                if(!empty($promoDetails) && !is_null($promoDetails) && $promoDetails != ''){
                    foreach ($promoDetails as $key => $value) {
                        #Categroy Details
                        $CategoryDetails = VideoCategory::where('id',$value['category_id'])->get()->toArray();
                        #Amount of purchase category
                        if($Input['payment_amount_type'] == 1) {
                            if($Input['plan_duration'] == 3){
                                $amount     = $CategoryDetails[0]['category_price_3month'];
                            } elseif ($Input['plan_duration'] == 6) {
                                $amount     = $CategoryDetails[0]['category_price_6month'];
                            } elseif ($Input['plan_duration'] == 12) {
                                $amount     = $CategoryDetails[0]['category_price'];
                            }
                        } else if($Input['payment_amount_type'] == 2) {
                            if($Input['plan_duration'] == 3){
                                $amount     = $CategoryDetails[0]['category_price_sgd_3month'];
                            } elseif ($Input['plan_duration'] == 6) {
                                $amount     = $CategoryDetails[0]['category_price_sgd_6month'];
                            } elseif ($Input['plan_duration'] == 12) {
                                $amount     = $CategoryDetails[0]['category_price_sgd'];
                            }
                        }

                        $promocode['cart_total']    = $amount;
                        $promocode['price_type']    = $Input['payment_amount_type'];
                        $promocode['promo']         = $value['promocode'];
                        $promocode['module']        = 0;
                        $promocode['user_id']       = $userAuth->id;
                        $promocode['language']      = 1;
                        $promocode['for_category']  = array('id'=>$value['category_id'],'plan_duration'=>$Input['plan_duration']);
                        $paymentOf  = IgniteController::Checkpromocode($promocode);
                        $price      = $paymentOf->original['data'];
                        $message    = $paymentOf->original['message'];
                        if(!is_null($price) && is_numeric($price)){
                            $result['discount_price']       = $price;
                            $result['promocode']            = $value['promocode'];
                            $result['payment_amount_type']  = $Input['payment_amount_type'];     
                            $responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$result);
                        } else {
                            $returnMessage = $promocode['language']==1?'PROMO_NOT_APPLICABLE':($Input['language']==2?'PROMO_NOT_APPLICABLE_CHINESE':'PROMO_NOT_APPLICABLE_RU');
                            $responseArray = UtilityController::Generateresponse(false, $returnMessage, Response::HTTP_BAD_REQUEST);   
                        }
                    }
                } else {
                    $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);    
    }

    //###############################################################
    //Function Name : IgniteReceiptattachement
    //Author : Karan Kantesariya <karan@creolestudios.com>
    //Purpose : generate invoice of the payment
    //In Params : Void
    //Return :
    //Date : 31th Dec 2020
    //###############################################################
    public function IgniteReceiptattachement($forCategory,$price_type, $totalAmount, $email_address, $first_name,$last_name,$hasDistributor, $date, $tradeNo,$projectTitle='',$card_number,$serviceExtra='',$current_language=1)
    {
        $current_domain = UtilityController::Getmessage('CURRENT_DOMAIN'); 
        if($current_domain=='cn'){
            $priceUnit='¥';
        }else{
            if($price_type == 1) {
                $priceUnit='$';
            } else if($price_type == 2) {
                $priceUnit='S$';
            }
        }
        if($current_language==1){
            $title      = 'Your Receipt From SixClouds';
            $invoice    = 'Invoice';
            $date_label = 'Date';
            $item       = 'Item';
            $price      = 'Price';
            $ignite     = 'IGNITE';
            $name       = $first_name.' '.$last_name;
        }else{
            $title      = '收据';
            $invoice    = '发票';
            $date_label = '日期';
            $item       = '项目';
            $price      = '价格';
            $ignite     = 'IGNITE';
            $name       = $first_name.' '.$last_name;
        }
        $date=UtilityController::Changedateformat($date, 'd/m/Y');
        $tax_rate = TaxRate::latest()->first();
        $html='<div class="container" style="font-size:13px; height:100%;">
                    <table style="width:100%">
                        <tr>
                            <td rowspan="6"><img src="' . url('/') . '/resources/assets/images/Corporate_Alternate_Color-1.png" height="60" /></td>
                            <td style="text-align:right"><strong>SixClouds Pte. Ltd.</strong></td>
                        </tr>
                        <tr>
                            <td style="text-align:right">UEN: 201720404G</td>
                        </tr>';
                        if(!is_null($tax_rate)){
                            if(!is_null($tax_rate->gst_number)){
                                $html.='<tr>
                                    <td style="text-align:right">GST Reg No: '.$tax_rate->gst_number.'</td>
                                </tr>';
                            }
                        }
                        $html.='<tr>
                            <td style="text-align:right">6001 Beach Road</td>
                        </tr>
                        <tr>
                            <td style="text-align:right">#09-09 Golden Mile Tower</td>
                        </tr>
                        <tr>
                            <td style="text-align:right">Singapore 199589</td>
                        </tr>
                    </table>
                    <br/>
                    <br/>
                <div class="row" style="font-size:16px; ">
                    <div class="col-md-12" align="center">
                        '.$title.'
                    </div>
                </div>
                <hr>
                <br/>
                <div class="row" style="font-size:16px;">
                    <table style="width:100%">
                        <tr>
                            <td><b> '. $name .' </b></td>
                            <td style="text-align:right"><b> '.$invoice.': '.$tradeNo.' </b></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="text-align:right">'.$date_label.': '. $date .'</td>
                        </tr>
                    </table>
                </div>
                <br>
                <div class="row">
                    <div class="table">
                        <table  width="100%" cellspacing="0" style="border-collapse: collapse;border-spacing: 0px 0px !important; ">
                        
                            <tbody class="row" style="margin-top:5px;">';
                            $subTotal = 0;
                            foreach ($projectTitle['categoies'] as $index => $element) {
                                $data[] = $element;
                            }
                            if(array_key_exists('starting_date', $projectTitle) && isset($projectTitle['starting_date'])){
                                $starting_date = $projectTitle['starting_date'];
                            } else {
                                $starting_date = Carbon::now()->format('d M Y');
                            }
                            if(!empty($data)){
                                foreach($data as $key => $value) {
                                    if($price_type == 1) {
                                        if(array_key_exists('subject', $value) && isset($value['subject']))
                                        {
                                            $html.='<tr  class="col-md-12">
                                                <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"><b> Subscription: </b></td>';
                                            $html.= '<td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td>
                                                <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> '.$value['subject']['subject_display_name'].' '.$value['category_name'].' ('.$value['plan_duration'].' months) </b></td></tr>';
                                            $html.='<tr  class="col-md-12">
                                                <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"><b> Subscription Price: </b></td>';
                                            $html.= '<td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td>
                                                <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> $'.$value['category_price'].' starting '. $starting_date .' </b></td></tr>';
                                            $html.='<tr  class="col-md-12">
                                                <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"><b> Subscription Expiry Date: </b></td>';
                                            $html.= '<td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td>
                                                <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b>'.$value['subscription_end_date'].' </b></td></tr>';
                                            $html.='<tr  class="col-md-12">
                                                <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"><b> Payment Method: </b></td>';
                                            $html.= '<td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td>
                                                <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> Card XXXX XXXX XXXX '.$card_number.' </b></td></tr>';
                                            $html.='<tr class="col-md-12"><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;border-bottom: 5px solid #fff;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;border-bottom: 5px solid #fff;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:13px; text-align:right;border-bottom: 5px solid #fff;"><b>$'. $value['category_price'] .'</b></td></tr>';
                                            $subTotal += $value['category_price'];    
                                        }
                                    } else if($price_type == 2) {
                                        if(array_key_exists('subject', $value) && isset($value['subject']))
                                        {
                                            $html.='<tr  class="col-md-12">
                                                <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"><b> Subscription </b></td>';
                                            $html.='<td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td>
                                                <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b>  '.$value['subject']['subject_display_name'].' '.$value['category_name'].' ('.$value['plan_duration'].' months) </b></td></tr>';
                                            $html.='<tr  class="col-md-12">
                                                <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"><b> Subscription Price: </b></td>';
                                            $html.= '<td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td>
                                                <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> S$'.$value['category_price_sgd'].' starting '. $starting_date .' </b></td></tr>';
                                            $html.='<tr  class="col-md-12">
                                                <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"><b> Subscription Expiry Date: </b></td>';
                                            $html.= '<td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td>
                                                <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b>'.$value['subscription_end_date'].' </b></td></tr>';
                                            $html.='<tr  class="col-md-12">
                                                <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"><b> Payment Method: </b></td>';
                                            $html.= '<td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td>
                                                <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> Card XXXX XXXX XXXX '.$card_number.' </b></td></tr>';
                                            $html.='<tr class="col-md-12"><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;border-bottom: 5px solid #fff;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;border-bottom: 5px solid #fff;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right; border-bottom: 5px solid #fff;"><b>S$'. $value['category_price_sgd'] .'</b></td></tr>';
                                            $subTotal += $value['category_price_sgd'];
                                        }
                                    }
                                }
                            }

                            $html.= '<tr><td class="col-md-4" style="background-color: #f5f5f5;text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> Sub-Total: '.$priceUnit.$subTotal.' </td></tr>';
                            if($projectTitle['isDiscount']==1){
                                // $discountOf = floatval($subTotal)-floatval($totalAmount);
                                if($projectTitle['discount_type']==0){
                                    $discountOf    = (($subTotal*$projectTitle['discount_of'])/100);
                                    $discountOf    = round($discountOf,2);
                                } else if($projectTitle['discount_type']==1) {
                                    $discountOf    = $projectTitle['discount_of'];
                                    $discountOf    = round($discountOf,2);
                                }
                                $html.= '<tr><td class="col-md-4" style="background-color: #f5f5f5;text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> Promo Code ('.$projectTitle['promoCode'].'): '.$priceUnit.$discountOf.'</b></td></tr>';
                                $html.= '<tr><td class="col-md-4" style="background-color: #f5f5f5;text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b>(Current Subscription period only)</b></td></tr>';
                                $html.= '<tr><td class="col-md-4" style="background-color: #f5f5f5;text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b>'.$projectTitle['discount_of'].'% off ';
                                if($projectTitle['discount_type']==0){
                                    if(!empty($data)) {
                                        foreach($data as $key => $value) {
                                            if(array_key_exists('subject', $value) && isset($value['subject'])) {
                                                if($key == 0){
                                                    $html.= $value['subject']['subject_display_name'].' '.$value['category_name'];
                                                } else {
                                                    $html.= ', '.$value['subject']['subject_display_name'].' '.$value['category_name'];
                                                }
                                            }
                                        }
                                    }
                                }
                                $html.= '</b></td></tr>';
                            }

                            ## Get Taxrate data
                            $taxrateData = TaxRate::latest()->first();
                            if(!is_null($taxrateData)){
                                $taxrateValue = round($taxrateData->tax_rate,2);
                                $taxrate_price = ($subTotal * $taxrateData->tax_rate)/100;
                                $taxrate_price = round($taxrate_price,2);
                            
                                if($taxrateData->tax_rate != 0){
                                    if($price_type == 1) {
                                        $html.='<tr class="col-md-12"><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td>
                                            <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> Tax Rate ('.$taxrateValue.'%): $'.$taxrate_price.' </b></td></tr>';
                                    } elseif ($price_type == 2) {
                                        $html.='<tr class="col-md-12"><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td>
                                            <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> Tax Rate ('.$taxrateValue.'%): S$'.$taxrate_price.' </b></td></tr>';
                                    }
                                } else {
                                    if($price_type == 1) {
                                        $html.='<tr class="col-md-12"><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td>
                                            <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> Tax Rate (0%): $0 </b></td></tr>';
                                    } elseif ($price_type == 2) {
                                        $html.='<tr class="col-md-12"><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td>
                                            <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> Tax Rate (0%): S$0 </b></td></tr>';
                                    }
                                }
                            } else {
                                if($price_type == 1) {
                                    $html.='<tr class="col-md-12"><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td>
                                        <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> Tax Rate (0%): $0 </b></td></tr>';
                                } elseif ($price_type == 2) {
                                    $html.='<tr class="col-md-12"><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td>
                                        <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> Tax Rate (0%): S$0 </b></td></tr>';
                                }
                            }

                            $totalAmount = round($totalAmount,2);
                            $html.='<tr><td class="col-md-4" style="background-color: #f5f5f5;text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> Total: '.$priceUnit.$totalAmount.' </td></tr>
                            </tbody> 
                        </table>   
                    </div>
                </div>';
                $url = url()->to('/');
                $html.= '<div class="row" style=" width: 100%;">
                            <table>
                                <tr>
                                    <td style="text-align:left;font-size:15px;">Your subscription will renew at ';
                        if(!empty($data)){
                            foreach($data as $key => $value) {
                                if($price_type == 1) {
                                    if(array_key_exists('subject', $value) && isset($value['subject'])) {
                                        if($key == 0){
                                            $html.= '$'.$value['category_price'];
                                        } else {
                                            $html.= ' + $'.$value['category_price'];
                                        }
                                    }
                                } else if($price_type == 2) {
                                    if(array_key_exists('subject', $value) && isset($value['subject'])) {
                                        if($key == 0){
                                            $html.= 'S$'.$value['category_price_sgd'];
                                        } else {
                                            $html.= ' + S$'.$value['category_price_sgd'];   
                                        }    
                                    }
                                }

                            }
                        }
                $html.= ' unless you opt-out before the subscription expiry date stated above.</td>
                                </tr>
                                <tr>
                                    <td style="text-align:left;font-size:15px;"> To learn more or opt out, <a href="'.$url.'/my-subscription/active">review your subscription</a>.</td>
                                </tr>
                            </table>
                        </div>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>';
                if(!empty($hasDistributor)){
                        $html.= '<div class="row" style="position: absolute; bottom: 0; width: 100%;">
                                    <table style="width:100%">
                                        <tr>
                                            <td style="text-align:left">'.$hasDistributor->distributor->company_name.' is an authorised distributor of </td>
                                            <td style="text-align:right">Fees paid are non-refundable.</td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:left">SixClouds Pte. Ltd.</td>
                                            <td style="text-align:right">Please refer to our Terms of Service.</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>';
                } else {
                    $html.= '<div class="row" style="position: absolute; bottom: 0; width: 100%;">
                                <table style="width:100%">
                                    <tr>
                                        <td></td>
                                        <td style="text-align:right">Fees paid are non-refundable.</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td style="text-align:right">Please refer to our Terms of Service.</td>
                                    </tr>
                                </table>
                            </div>
                        </div>';
                }
        $filename   = 'payment_receipt_'.$tradeNo . ".pdf";
        try{
            //$pdf = App::make('dompdf.wrapper');
            $pdf = PDF::loadHtml($html);
        }  catch (\Exception $e) {
            // $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            print_r($e->getMessage()); die;
        }  
        $finalPath = public_path() . UtilityController::Getpath('MP_RECEIPT_ATTACHMENT_UPLOAD_PATH') . $filename;
        $pdf->save($finalPath); 
        return $finalPath;
    }

    //###############################################################
    //Function Name: IgniteCategoryPayment
    //Author:        Karan Kantesariya <karan@creolestudios.com>
    //Purpose:       To Manage Jobs
    //In Params:     Payment Array 
    //Return:        json
    //Date:          08th Jan, 2021
    //###############################################################
    public static function IgniteCategoryPayment($request) {
        try {
            $Input = $request;
            if(!empty($Input) &&is_array($Input)) {
                $details = JobManage::where('user_id',$Input['paid_by'])->where('ignite_category_id',$Input['video_category_id'])->where('status',1)->exists();
                if($details){
                    // ## Check job are exists or not
                    // $jobId = JobManage::where('user_id',$Input['paid_by'])->where('ignite_category_id',$Input['video_category_id'])->pluck('job_id')->toArray();
                    // $jobExists = DB::table('jobs')->whereIn('id',$jobId)->exists();
                    // if($jobExists){
                        #Varible array method pass
                        $customer           = array();
                        $charge             = array();
                        $checkBeta          = array();
                        $token              = array();
                        $userData           = array();
                        $CategoryDetails    = array();
                        #User Details
                        $userData = User::where('id',$Input['paid_by'])->get()->toArray();
                        if(!is_null($userData) && !empty($userData)){
                            #Categroy Details
                            $CategoryDetails = VideoCategory::where('id',$Input['video_category_id'])->get()->toArray();
                            ## Get taxrate data
                            $taxrateData = TaxRate::latest()->first();
                            if(!is_null($CategoryDetails) && !empty($CategoryDetails)){
                                #Amount of purchase category
                                if($Input['payment_amount_type'] == 1) {
                                    $currency   = 'usd';
                                    if($Input['plan_duration'] == 3){
                                        $price     = $CategoryDetails[0]['category_price_3month'];
                                    } elseif ($Input['plan_duration'] == 6) {
                                        $price     = $CategoryDetails[0]['category_price_6month'];
                                    } elseif ($Input['plan_duration'] == 12) {
                                        $price     = $CategoryDetails[0]['category_price'];
                                    }
                                    if(!is_null($taxrateData)){
                                        $tax_rate = $price*$taxrateData->tax_rate/100;
                                        $amount = $price + $tax_rate;
                                    } else {
                                        $amount = $price;
                                    }
                                } else if($Input['payment_amount_type'] == 2) {
                                    $currency   = 'sgd';
                                    if($Input['plan_duration'] == 3){
                                        $price     = $CategoryDetails[0]['category_price_sgd_3month'];
                                    } elseif ($Input['plan_duration'] == 6) {
                                        $price     = $CategoryDetails[0]['category_price_sgd_6month'];
                                    } elseif ($Input['plan_duration'] == 12) {
                                        $price     = $CategoryDetails[0]['category_price_sgd'];
                                    }
                                    if(!is_null($taxrateData)){
                                        $tax_rate = $price*$taxrateData->tax_rate/100;
                                        $amount = $price + $tax_rate;
                                    } else {
                                        $amount = $price;
                                    }
                                }
                                #Promocode check
                                $promoDetails = array();
                                $promoDetails = StorePromocode::where('category_id',$Input['video_category_id'])->where('user_id',$userData[0]['id'])->get()->toArray();
                                if(!empty($promoDetails) && $promoDetails != ''){
                                    $promocode = array();
                                    $promocode['cart_total']    = $amount;
                                    $promocode['price_type']    = $Input['payment_amount_type'];
                                    $promocode['promo']         = $promoDetails[0]['promocode'];
                                    $promocode['module']        = 0;
                                    $promocode['user_id']       = $Input['paid_by'];
                                    $promocode['language']      = 1;
                                    $promocode['for_category']  = array('id'=>$Input['video_category_id'],'plan_duration'=>$Input['plan_duration']);

                                    $paymentOf  = array();
                                    $paymentOf  = IgniteController::Checkpromocode($promocode);
                                    $value      = $paymentOf->original['data'];
                                    if(!is_null($value) && is_numeric($value)){
                                        $promocodeDetails = PromoCode::where('promo_code',$promoDetails[0]['promocode'])->where('to_date','>=',Carbon::now())->where('status',1)->first();
                                        $amount = $value;
                                        StorePromocode::where('category_id',$Input['video_category_id'])->where('user_id',$userData[0]['id'])->delete();
                                    }
                                }

                                #beta/localhost checking
                                $fullPath = url()->full();
                                $checkBeta = explode('/', $Input['url']);
                                if(in_array('localhost', $checkBeta) || in_array('beta', $checkBeta)){
                                    Stripe::setApiKey(env('STRIPE_SANDBOX'));
                                } else {
                                    Stripe::setApiKey(env('STRIPE_LIVE'));
                                }

                                if(!is_null($userData[0]['stripe_id']) && $userData[0]['stripe_id'] != '') {
                                    try{    
                                        #Customer retrived details
                                        $customer = Customer::retrieve($userData[0]['stripe_id'], []);
                                        // #Create new card for payment  
                                        // $token = Token::create([
                                        //     'card' => [
                                        //         'number'    => '4000 0000 0000 9995',
                                        //         'exp_month' => '02',
                                        //         'exp_year'  => '2021',
                                        //         'cvc'       => '123',
                                        //     ],
                                        // ]);

                                        // #Create new customer 
                                        // $customer = Customer::create(array(
                                        //     'email'  => $userData[0]['email_address'],
                                        //     'source' => $token['id']
                                        // ));

                                        #Stripe Charge 
                                        $charge = Charge::create(array(
                                            'customer' => $customer->id,
                                            'amount'   => intval($amount)*100,
                                            'currency' => $currency
                                        ));
                                        if($charge->paid==1) {
                                            \DB::beginTransaction();
                                                $payment    = $Input;
                                                $Result     = array();
                                                unset($payment['system_transaction_number']);
                                                unset($payment['id']);
                                                unset($payment['switch_id']);
                                                unset($payment['promo_code']);
                                                unset($payment['discount_type']);
                                                unset($payment['discount_of']);
                                                unset($payment['promo_code_id']);
                                                $payment['renewal_id']  = !empty($payment['renewal_id'])?$payment['renewal_id']:$Input['id'];
                                                $payment['transaction_reference'] = $charge->balance_transaction;
                                                $payment['remarks']         = "Renewal subscription";
                                                $payment['payment_amount']  = $amount;
                                                $payment['system_transaction_number'] = UtilityController::GenerateRunningNumber('MarketPlacePayment','system_transaction_number',"VL");
                                                if(!empty($promocodeDetails) && $promocodeDetails != ''){
                                                    $payment['promo_code']      = $promocodeDetails['promo_code'];
                                                    $payment['discount_type']   = $promocodeDetails['discount_type'];
                                                    $payment['discount_of']     = $promocodeDetails['discount_of'];
                                                    $payment['promo_code_id']   = $promocodeDetails['id'];
                                                }

                                                if($Input['plan_duration'] == 12) {
                                                    $payment['subscription_end_date']     = Carbon::now()->addYears(1)->subDays(1);
                                                } elseif ($Input['plan_duration'] == 6) {
                                                    $payment['subscription_end_date']     = Carbon::now()->addMonths(6)->subDays(1);
                                                } elseif ($Input['plan_duration'] == 3) {
                                                    $payment['subscription_end_date']     = Carbon::now()->addMonths(3)->subDays(1);
                                                }

                                                $Result = UtilityController::Makemodelobject($payment,'MarketPlacePayment','','');
                                                $Result['url'] = $Input['url']; 
                                                $Result = $Result->toArray();
                                                
                                                $current_date['subscription_end_date'] = Carbon::now();
                                                $check = UtilityController::Makemodelobject($current_date,'MarketPlacePayment','',$Input['id']);

                                                if($Result['payment_amount_type'] == 1) {
                                                    if($Result['plan_duration'] == 3){
                                                        $cate_name = VideoCategory::select('id','category_name','category_price_3month as category_price','subject_id')->where('id',$Result['video_category_id'])->with('subject')->get()->toArray();
                                                    } elseif ($Result['plan_duration'] == 6) {
                                                        $cate_name = VideoCategory::select('id','category_name','category_price_6month as category_price','subject_id')->where('id',$Result['video_category_id'])->with('subject')->get()->toArray();
                                                    } elseif ($Result['plan_duration'] == 12) {
                                                        $cate_name = VideoCategory::select('id','category_name','category_price','subject_id')->where('id',$Result['video_category_id'])->with('subject')->get()->toArray();
                                                    }
                                                } else if($Result['payment_amount_type'] == 2) {
                                                    if($Result['plan_duration'] == 3){
                                                        $cate_name = VideoCategory::select('id','category_name','category_price_sgd_3month as category_price_sgd','subject_id')->where('id',$Result['video_category_id'])->with('subject')->get()->toArray();
                                                    } elseif ($Result['plan_duration'] == 6) {
                                                        $cate_name = VideoCategory::select('id','category_name','category_price_sgd_6month as category_price_sgd','subject_id')->where('id',$Result['video_category_id'])->with('subject')->get()->toArray();
                                                    } elseif ($Result['plan_duration'] == 12) {
                                                        $cate_name = VideoCategory::select('id','category_name','category_price_sgd','subject_id')->where('id',$Result['video_category_id'])->with('subject')->get()->toArray();
                                                    }
                                                }

                                                $collection = collect($cate_name)->map(function ($name) use($Result) {
                                                        if($name['id'] == $Result['video_category_id']){
                                                            $name['plan_duration'] = $Result['plan_duration'];
                                                            $dateObject = Carbon::parse($Result['subscription_end_date'])->format('d M Y');
                                                            $name['subscription_end_date'] = $dateObject;        
                                                        }
                                                    return $name;
                                                });

                                                $category_name['categoies'] = $collection->toArray();
                                                
                                                if(!empty($Result) && is_array($Result)){
                                                    $Ids = DB::table('job_manage')->where('ignite_category_id',$Input['video_category_id'])->where('user_id',$userData[0]['id'])->pluck('job_id')->toArray();
                                                    if(!empty($Ids)) {
                                                        DB::table('jobs')->whereIn('id',$Ids)->delete();
                                                    }
                                                    
                                                    ## Delete Old category Cron Job Notify email and Notify email Ids
                                                    $id = StoreEmail::where('user_id',$userData[0]['id'])->where('category_id',$Input['video_category_id'])->pluck('job_id')->toArray();
                                                    if(!empty($id)){
                                                        foreach($id as $index => $emailId){
                                                            $email_job = explode(',', $emailId);
                                                            $email_job_delete = DB::table('jobs')->whereIn('id',$email_job)->delete();
                                                            $store_email_delete = StoreEmail::where('user_id',$userData[0]['id'])->where('category_id',$Input['video_category_id'])->where('job_id',$emailId)->delete();
                                                        }
                                                    }

                                                    ## Live
                                                    if($Input['plan_duration'] == 12) {
                                                        $IgniteCategory = (new IgniteCategoryPayment($Result))->delay(Carbon::now()->addYears(1)->subDays(1));
                                                    } elseif ($Input['plan_duration'] == 6) {
                                                        $IgniteCategory = (new IgniteCategoryPayment($Result))->delay(Carbon::now()->addMonths(6)->subDays(1));
                                                    } elseif ($Input['plan_duration'] == 3) {
                                                        $IgniteCategory = (new IgniteCategoryPayment($Result))->delay(Carbon::now()->addMonths(3)->subDays(1));
                                                    }
                                                    // ## Test
                                                    // $IgniteCategory = (new IgniteCategoryPayment($Input))->delay(Carbon::now()->addMinutes(3));
                                                    $jobId = dispatch($IgniteCategory);
                                                    DB::table('job_manage')->where('ignite_category_id',$Input['video_category_id'])->where('user_id',$userData[0]['id'])->delete();
                                                    DB::table('job_manage')->insert(
                                                        ['ignite_category_id' => $Input['video_category_id'], 'job_id' => $jobId, 'user_id'=>$userData[0]['id'], 'status' => 1 ]
                                                    );

                                                    ## User Notify recurring payment for email
                                                    if($Result['payment_amount_type'] == 1){
                                                        $details = VideoCategory::with('subject')->where('id',$Result['video_category_id'])->get();
                                                        if($Result['plan_duration'] == 12){
                                                            $category_price = $details[0]['category_price'];
                                                        } elseif ($Result['plan_duration'] == 6) {
                                                            $category_price = $details[0]['category_price_6month'];
                                                        } elseif ($Result['plan_duration'] == 3) {
                                                            $category_price = $details[0]['category_price_3month'];
                                                        }
                                                    } elseif ($Result['payment_amount_type'] == 2) {
                                                        $details = VideoCategory::with('subject')->where('id',$Result['video_category_id'])->get();
                                                        if($Result['plan_duration'] == 12){
                                                            $category_price = $details[0]['category_price_sgd'];
                                                        } elseif ($Result['plan_duration'] == 6) {
                                                            $category_price = $details[0]['category_price_sgd_6month'];
                                                        } elseif ($Result['plan_duration'] == 3) {
                                                            $category_price = $details[0]['category_price_sgd_3month'];
                                                        }
                                                    }
                                                    if(!is_null($taxrateData)){
                                                        $taxrate_price = ($category_price*$taxrateData->tax_rate)/100;
                                                        $emailAmount = $category_price + $taxrate_price;
                                                    } else {
                                                        $emailAmount = $category_price;
                                                    }

                                                    $emailDetails['email_address']          = $userData[0]['email_address'];
                                                    $emailDetails['display_name']           = $userData[0]['display_name'];
                                                    $emailDetails['subscription_end_date']  = $Result['subscription_end_date'];
                                                    $emailDetails['payment_amount_type']    = $Result['payment_amount_type'];
                                                    $emailDetails['amount']                 = $emailAmount;
                                                    $emailDetails['video_category_id']      = $Result['video_category_id'];
                                                    $emailDetails['user_id']                = $userData[0]['id'];
                                                    $emailDetails['plan_duration']          = $Result['plan_duration'];
                                                    $emailDetails['video_category_name']    = $Result['video_category_name'];
                                                    $emailDetails['starting_date']          = Carbon::now()->timezone('Asia/Singapore')->format('d M Y');
                                                    $emailDetails['subject_name']           = $details[0]->subject['subject_display_name'];
                                                    $emailDetails['subject_image']           = $Input['url'].'/public/uploads/subject_images/'.$details[0]->subject['subject_image'];
                                                    $emailDetails['review_url']             = $Input['url'].'/my-subscription';
                                                    ## Live 
                                                    $EmailForNotifyFirst  = (new EmailForNotify($emailDetails))->delay($Result['subscription_end_date']->subDays(7));
                                                    // ## Test
                                                    // $EmailForNotifyFirst  = (new EmailForNotify($emailDetails))->delay(Carbon::now()->addMinutes(1));
                                                    $job['first'] = dispatch($EmailForNotifyFirst);
                                                    ## Live
                                                    $EmailForNotifySecond = (new EmailForNotify($emailDetails))->delay($Result['subscription_end_date']->subMonths(1));
                                                    // ## Test
                                                    // $EmailForNotifySecond  = (new EmailForNotify($emailDetails))->delay(Carbon::now()->addMinutes(2));
                                                    $job['second'] = dispatch($EmailForNotifySecond);
                                                    $job_email = implode(',', $job);
                                                    DB::table('store_email')->insert(
                                                        ['category_id' => $Result['video_category_id'], 'job_id' => $job_email,'user_id' => $userData[0]['id'], 'status' => 1 ]
                                                    );
                                                    ## Get Card Number
                                                    $card_number = 'XXXX XXXX XXXX '.$customer->sources['data'][0]['last4'];
                                                    ## Creta Array forCategory
                                                    $forCategory['id'] = $Result['video_category_id'];
                                                    $forCategory['plan_duration'] = $Result['plan_duration'];
                                                    ## Add Promocode detials
                                                    if(array_key_exists('promo_code', $Result)){
                                                        if($Result['promo_code']!=''){
                                                            $category_name['isDiscount'] = 1;
                                                            $category_name['promoCode'] = $Result['promo_code'];
                                                            $category_name['discount_type'] = $Result['discount_type'];
                                                            $category_name['discount_of'] = $Result['discount_of'];
                                                        } else
                                                            $category_name['isDiscount'] = 0;
                                                    } else {
                                                        $category_name['isDiscount'] = 0;
                                                    }

                                                    $info['generateReceipt'] = IgniteController::IgniteSubscriptionReceiptattachement($forCategory,$Result['payment_amount_type'],$Result['payment_amount'],$userData[0]['email_address'],$userData[0]['first_name'],$userData[0]['last_name'],Carbon::now(),$Result['system_transaction_number'],$Input['url'],$category_name,$card_number);
                                                    $system_transaction_number = $Result['system_transaction_number'];
                                                    $info['email_address'] = $userData[0]['email_address'];
                                                    $info['current_language'] = 1;

                                                    $userFullName = $userData[0]['first_name']." ".$userData[0]['last_name'];

                                                    $info['subject']=$info['current_language']==1?'Thank you for your renewal payment. Enclosed is your payment receipt.':($info['current_language']==2?'谢谢您的付款。 附件的是您的项付款收据。':'Спасибо за оплату. Ваша квитанция прилагается.');

                                                    $info['content'] = $info['current_language']==1?'Dear <strong>'.$userData[0]['first_name'].',
                                                        </strong><br/><br/>
                                                        You have purchased the following Subscription.<br/><br/>':($info['current_language']==2?'你好 <strong>'.$userData[0]['first_name'].' '.$userData[0]['last_name'].'</strong> ,
                                                        <br/><br/>
                                                        谢谢您的付款。随函附上您的付款收据。<br/><br/><br/>':'Привет <strong>'.$userFullName.' ,
                                                        </strong><br/><br/>
                                                        Спасибо за оплату. Ваша квитанция прилагается.<br/><br/>');

                                                    foreach ($category_name['categoies'] as $key => $value) {
                                                        $path = $Input['url'].'/public/uploads/subject_images/'.$value['subject']['subject_image'];
                                                        $info['content'] .= '<img src="'.$path.'" style="height: 120px;margin-right: 20px;">';
                                                    }

                                                    if($Result['payment_amount_type'] == 1){
                                                        $paymentSign = "USD";
                                                        foreach ($category_name['categoies'] as $index => $detail) {
                                                            $info['content'] .= '<br /><br /><span>'.$detail['subject']['subject_display_name'].' - '. $detail['category_name'] .' - '.$paymentSign.$detail['category_price'].'/'.$detail['plan_duration'].' months starting '. Carbon::now()->timezone('Asia/Singapore')->format('d M Y') .', expiring on '. $detail['subscription_end_date'] .'</span>';
                                                        }
                                                    } elseif ($Result['payment_amount_type'] == 1) {
                                                        $paymentSign = "SGD";
                                                        foreach ($category_name['categoies'] as $index => $detail) {
                                                            $info['content'] .= '<br /><br /><span>'.$detail['subject']['subject_display_name'].' - '. $detail['category_name'] .' - '.$paymentSign.$detail['category_price_sgd'].'/'.$detail['plan_duration'].' months starting '. Carbon::now()->timezone('Asia/Singapore')->format('d M Y') .', expiring on '. $detail['subscription_end_date'] .'</span>';
                                                        }
                                                    }

                                                    // foreach ($category_name['categoies'] as $index => $detail) {
                                                    //     $info['content'] .= '<br /><br /><span>'.$detail['subject']['subject_display_name'].' - '. $detail['category_name'] .' - '.$paymentSign.$detail['category_price'].'/'.$detail['plan_duration'].' months starting '. Carbon::now()->timezone('Asia/Singapore')->format('d M Y') .', expiring on '. $detail['subscription_end_date'] .'</span>';
                                                    // }
                                                    $faqsPath = $Input['url'].'/faq';
                                                    $info['content'] .= '<br /><br /><span>Subscriptions automatically renew for the same grade until you opt out of auto-renewal. To learn more about opting out of auto-renewal for your subscription, visit our <a href="'.$faqsPath.'" style="color: blue;">FAQs</a>.</span><br /><br />';

                                                    $info['footer_content']=$info['current_language']==1?'From,<br /> SixClouds':($info['current_language']==2?'六云 ':'SixClouds');
                                                    $info['footer']=$info['current_language']==1?'SixClouds':($info['current_language']==2?'六云':'SixClouds');
                                                    Mail::send('emails.email_template', $info, function ($message) use ($info,$system_transaction_number) {
                                                        $message->from('noreply@sixclouds.net', 'SixClouds');
                                                        $message->attach($info['generateReceipt'],array(
                                                                            'as' => $system_transaction_number.'.pdf', 
                                                                            'mime' => 'application/pdf'));
                                                        $message->to($info['email_address'])->subject($info['subject']);
                                                    });
                                                }
                                            \DB::commit();    
                                        }
                                    } catch (\Exception $m) {
                                        JobManage::where('user_id',$userData[0]['id'])->where('ignite_category_id',$Input['video_category_id'])->delete();
                                        $data = StoreEmail::where('user_id',$userData[0]['id'])->where('category_id',$Input['video_category_id'])->get()->pluck('job_id')->toArray();
                                        if(!empty($data)){
                                            $Ids = explode(',', $data[0]);
                                            DB::table('jobs')->whereIn('id',$Ids)->delete();
                                        }
                                        $user_email = $userData[0]['email_address'];
                                        #User mail are sent!
                                        $mail['content']='Hello <strong>'.$userData[0]['display_name'].' ,
                                        </strong><br/><br/>Your recurring payment for the next grade has been failed due to '.$m->getMessage().'<br/><br/>You will no longer be able to access the grade/services.<br/><br/>If you want to continue accessing the content, please purchase a new grade from the sixclouds panel.';
                                        $mail['footer_content'] ='From,<br /> SixClouds ';
                                        $mail['footer']         ='SixClouds';
                                        Mail::send('emails.email_template', $mail, function ($message) use ($user_email) {
                                            $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                                            $message->to($user_email);
                                            $message->subject('Thank you for your payment.');
                                        });
                                    }
                                }
                            }
                        }
                    // }
                }
            }           
            // $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $result);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        // return response($returnData);
    }

    //###############################################################
    //Function Name: EmailForNotify
    //Author:        Karan Kantesariya <karan@creolestudios.com>
    //Purpose:       Notify User recurring payment before
    //In Params:     Email Address 
    //Return:        json
    //Date:          11th Jan, 2021
    //###############################################################
    public static function EmailForNotify($request) {
        try {
            $emailDetails = $request;
            $details = JobManage::where('user_id',$emailDetails['user_id'])->where('ignite_category_id',$emailDetails['video_category_id'])->where('status',1)->exists();
            if($details){
                $amount = $emailDetails['amount'];
                #Promocode check
                $promoDetails = array();
                $userDetails  = array();  
                $promoDetails = StorePromocode::where('category_id',$emailDetails['video_category_id'])->where('user_id',$emailDetails['user_id'])->get()->toArray();
                if(!empty($promoDetails) && $promoDetails != ''){
                    $promocode = array();
                    $promocode['cart_total']    = $emailDetails['amount'];
                    $promocode['price_type']    = $emailDetails['payment_amount_type'];
                    $promocode['promo']         = $promoDetails[0]['promocode'];
                    $promocode['module']        = 0;
                    $promocode['user_id']       = $emailDetails['user_id'];
                    $promocode['language']      = 1;
                    $promocode['for_category']  = array('id'=>$emailDetails['video_category_id'],'plan_duration'=>$emailDetails['plan_duration']);

                    $paymentOf  = array();
                    $paymentOf  = IgniteController::Checkpromocode($promocode);
                    $value      = $paymentOf->original['data'];
                    if(!is_null($value) && is_numeric($value)){
                        $amount = $value;
                    } else {
                        $amount = $emailDetails['amount'];
                    }
                }

                $userDetails = User::where('id',$emailDetails['user_id'])->get()->toArray();

                if($emailDetails['payment_amount_type'] == 1){
                    $amount = 'USD'.$amount;
                } elseif ($emailDetails['payment_amount_type'] == 2) {
                    $amount = 'SGD'.$amount;
                }

                $info['current_language']   = 1;
                $info['email_address']      = $emailDetails['email_address'];
                $userFullName               = $emailDetails['display_name'];

                $info['subject'] = 'Your Subscription is Expiring';

                $info['content'] = $info['current_language']==1?'Dear <strong>'.$userDetails[0]['first_name'].',
                    </strong><br/><br/>
                    The following subscription will expire soon.<br/><br/>':($info['current_language']==2?'你好 <strong>'.$userDetails[0]['first_name'].'</strong> ,
                    <br/><br/>
                    谢谢您的付款。随函附上您的付款收据。<br/><br/><br/>':'Привет <strong>'.$userDetails[0]['first_name'].' ,
                    </strong><br/><br/>
                    Спасибо за оплату. Ваша квитанция прилагается.<br/><br/>');

                $info['content'] .= '<img src="'.$emailDetails['subject_image'].'" style="height: 120px;margin-right: 20px;">';

                $info['content'] .= '<br /><br /><span>'.$emailDetails['subject_name'].' - '. $emailDetails['video_category_name'] .' - '.$amount.'/'.$emailDetails['plan_duration'].' months starting '. $emailDetails['starting_date'] .', expiring on '. $emailDetails['subscription_end_date']->format('d M Y') .'</span>';

                $info['content'] .= '<br /><br /><span>To review or learn more, <a href="'.$emailDetails['review_url'].'" style="color: blue;">review your subscription</a>. If you do not wish to automatically renew your subscription, you need to opt out 48 hours before '.$emailDetails['subscription_end_date']->format('d M Y').'.</span><br /><br />';

                $info['footer_content']=$info['current_language']==1?'From,<br /> SixClouds':($info['current_language']==2?'六云 ':'SixClouds');
                $info['footer']=$info['current_language']==1?'SixClouds':($info['current_language']==2?'六云':'SixClouds');
                Mail::send('emails.email_template', $info, function ($message) use ($info) {
                    $message->from('noreply@sixclouds.net', 'SixClouds');
                    $message->to($info['email_address'])->subject($info['subject']);
                });
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
    }

    //###############################################################
    //Function Name: Checkpromocode
    //Author:        Karan Kantesariya <karan@creolestudios.com>
    //Purpose:       to check the applied promo code is valid or not
    //In Params:     promocode
    //Return:        json
    //Date:          13nd Jan, 2021
    //###############################################################
    public static function Checkpromocode($request){
        try {
                $Input = $request;
                if(!array_key_exists('promo', $Input))
                    throw new \Exception($Input['language']==1?'PROMO_CODE_REQUIRED':'PROMO_CODE_REQUIRED_CHINESE');
                if(!array_key_exists('for_category', $Input))
                    throw new \Exception($Input['language']==1?'SELECT_CATEGORY':'SELECT_CATEGORY_CHINESE');
                $whatPromo = PromoCode::where('promo_code',$Input['promo'])->where('status',1)->where('to_date','>=',Carbon::now())->where('status',1)->first();
                if(empty($whatPromo)){
                    $returnMessage = $Input['language']==1?'PROMO_NOT_APPLICABLE':($Input['language']==2?'PROMO_NOT_APPLICABLE_CHINESE':'PROMO_NOT_APPLICABLE_RU');
                    $returnData = UtilityController::Generateresponse(false, $returnMessage, Response::HTTP_BAD_REQUEST);
                } elseif ($whatPromo['code_for']==1||$whatPromo['code_for']==2) {
                    // $returnData = self::Checkdistributorcode($Input);
                    $promoData = PromoCode::where('promo_code',$Input['promo'])->where('to_date','>=',Carbon::now())->where('status',1)->first()->toArray();
                    $checkIfdistributorExists = DistributorsReferrals::where('user_id',$user->id)->first();
                    if(!empty($checkIfdistributorExists)){
                        $distributorDetail = $promoData['code_for']==1?Distributors::where('referal_code',$Input['promo'])->first():DistributorsTeamMembers::where('referal_code',$Input['promo'])->first();
                        $return_promo_error = 0;
                        if(!($user->id==$checkIfdistributorExists['user_id'] && $promoData['distributor_id']==$checkIfdistributorExists['distributors_id'])||$distributorDetail['status']!=1){
                            $return_promo_error = 1;
                        }
                        if($checkIfdistributorExists['distributor_team_member_id']!='' && $promoData['distributor_team_member_id']!=$checkIfdistributorExists['distributor_team_member_id']){
                            $return_promo_error = 1;
                        }
                        if($return_promo_error == 1){
                            //$returnMessage = $Input['language']==1?'PROMO_NOT_APPLICABLE':'PROMO_NOT_APPLICABLE_CHINESE';
                            $returnMessage = $Input['language']==1?'PROMO_NOT_APPLICABLE':($Input['language']==2?'PROMO_NOT_APPLICABLE_CHINESE':'PROMO_NOT_APPLICABLE_RU');
                            $returnData = UtilityController::Generateresponse(false, $returnMessage, Response::HTTP_BAD_REQUEST);
                            return $returnData;
                        }
                    }
                    $Input['for_category'] = array_values($Input['for_category']);
                    ## Get taxrate Data
                    $taxrateData = TaxRate::latest()->first();
                    if($Input['price_type'] == 1) {
                        foreach ($Input['for_category'] as $key => $value) {
                            $details = VideoCategory::where('id',$value['id'])->get();
                            if($value['plan_duration'] == 3){
                                $all_price[] = $details[0]['category_price_3month'];
                            } elseif ($value['plan_duration'] == 6) {
                                $all_price[] = $details[0]['category_price_6month'];
                            } else {
                                $all_price[] = $details[0]['category_price'];
                            }  
                        }
                        $subtotal = array_sum($all_price);
                        // if($taxrateData->tax_rate > 0){
                        //     $taxrate_price = ($subtotal * $taxrateData->tax_rate)/100;
                        //     $price = $subtotal + $taxrate_price;
                        // } else {
                        //     $price = array_sum($total);
                        // }
                    } elseif ($Input['price_type'] == 2) {
                        foreach ($Input['for_category'] as $key => $value) {
                            $details = VideoCategory::where('id',$value['id'])->get();
                            if($value['plan_duration'] == 3){
                                $all_price[] = $details[0]['category_price_sgd_3month'];
                            } elseif ($value['plan_duration'] == 6) {
                                $all_price[] = $details[0]['category_price_sgd_6month'];
                            } else {
                                $all_price[] = $details[0]['category_price_sgd'];
                            }  
                        }
                        $subtotal = array_sum($all_price);
                        // if($taxrateData->tax_rate > 0){
                        //     $taxrate_price = ($subtotal * $taxrateData->tax_rate)/100;
                        //     $price = $subtotal + $taxrate_price;
                        // } else {
                        //     $price = array_sum($total);
                        // }
                    }
                    if($promoData['discount_type']==0){
                        $price = (($subtotal*$promoData['discount_of'])/100);
                        $price = $subtotal - $price;
                        if(!is_null($taxrateData)){
                            $taxrate_price = ($subtotal * $taxrateData->tax_rate)/100;
                            $finalPrice = $price + $taxrate_price;
                        } else {
                            $finalPrice = $price;
                        }
                    } else {
                        $price = $subtotal - $promoData['discount_of'];
                        if(!is_null($taxrateData)){
                            $taxrate_price = ($subtotal * $taxrateData->tax_rate)/100;
                            $finalPrice = $price + $taxrate_price;
                        } else {
                            $finalPrice = $price;
                        }
                    }
                    $finalPrice = round($finalPrice,2);
                    if($finalPrice>0)
                        $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1,$finalPrice);
                    else {
                        //$returnMessage = $Input['language']==1?'PROMO_NOT_APPLICABLE':'PROMO_NOT_APPLICABLE_CHINESE';
                        $returnMessage = $Input['language']==1?'PROMO_NOT_APPLICABLE':($Input['language']==2?'PROMO_NOT_APPLICABLE_CHINESE':'PROMO_NOT_APPLICABLE_RU');
                        $returnData = UtilityController::Generateresponse(false, $returnMessage, Response::HTTP_BAD_REQUEST);
                    }
                } else {
                    $checkIfdistributorExists = DistributorsReferrals::select('distributors_id')->where('user_id',$Input['user_id'])->first();

                    $checkExists = (!empty($checkIfdistributorExists)?PromoCode::where(function ($query) use($checkIfdistributorExists){
                            $query->where('distributor_id',$checkIfdistributorExists['distributors_id'])->orWhereNull('distributor_id');
                        })->where('promo_code',$Input['promo'])->where('to_date','>=',Carbon::now())->where('status',1)->exists():PromoCode::whereNull('distributor_id')->where('promo_code',$Input['promo'])->where('to_date','>=',Carbon::now())->where('status',1)->exists());

                    if($checkExists){
                        $finalPrice = 0;
                        $category_id =array();  
                        $category_id[] = $Input['for_category']['id'];
                        $promoData = PromoCode::with('category_details_with_price')->where('promo_code',$Input['promo'])->whereIn('video_category_id',$category_id)->where('to_date','>=',Carbon::now())->where('status',1)->get()->toArray();
                        ## Get taxrate Data
                        $taxrateData = TaxRate::latest()->first();
                        if(!empty($promoData) && count($category_id)==count($promoData)){
                            foreach ($promoData as $key => $value){
                                if($Input['price_type'] == 1) {
                                    if($Input['for_category']['plan_duration'] == 3) {
                                        if($value['discount_type']==0){
                                            // if($taxrateData->tax_rate > 0){
                                            //     $tax_rate = ($value['category_details_with_price']['category_price_3month']*$taxrateData->tax_rate)/100;
                                            //     $total = $value['category_details_with_price']['category_price_3month'] + $tax_rate;
                                            // } else {
                                                $total = $value['category_details_with_price']['category_price_3month'];
                                            // }
                                            if($total > 0){
                                                $discount_of    = ($total*$value['discount_of'])/100;
                                                $price = $total - $discount_of;
                                                if(!is_null($taxrateData)){
                                                    $taxrate_price = ($total * $taxrateData->tax_rate)/100;
                                                    $finalPrice += $price + $taxrate_price;
                                                } else {
                                                    $finalPrice += $price;
                                                }
                                            }
                                            // $discount_of    = ($total*$value['discount_of'])/100;
                                            // $finalPrice += $total - $discount_of;
                                        } elseif ($value['discount_type']==1) {
                                            // if($taxrateData->tax_rate > 0){
                                            //     $tax_rate = ($value['category_details_with_price']['category_price_3month']*$taxrateData->tax_rate)/100;
                                            //     $total = $value['category_details_with_price']['category_price_3month'] + $total;
                                            // } else {
                                                $total = $value['category_details_with_price']['category_price_3month'];
                                            // }
                                            // $discount_of    = $value['discount_of'];
                                            // $finalPrice += $total - $discount_of;
                                            if($total > 0){
                                                $discount_of    = $value['discount_of'];
                                                $price = $total - $discount_of;
                                                if(!is_null($taxrateData)){
                                                    $taxrate_price = ($total * $taxrateData->tax_rate)/100;
                                                    $finalPrice += $price + $taxrate_price;
                                                } else {
                                                    $finalPrice += $price;
                                                }
                                            }
                                        }
                                    } elseif ($Input['for_category']['plan_duration'] == 6) {
                                        if($value['discount_type']==0){
                                            // if($taxrateData->tax_rate > 0){
                                            //     $tax_rate = ($value['category_details_with_price']['category_price_6month']*$taxrateData->tax_rate)/100;
                                            //     $total = $value['category_details_with_price']['category_price_6month'] + $tax_rate;
                                            // } else {
                                                $total = $value['category_details_with_price']['category_price_6month'];
                                            // }
                                            // $discount_of    = ($total*$value['discount_of'])/100;
                                            // $finalPrice += $total - $discount_of;
                                            if($total > 0){
                                                $discount_of    = ($total*$value['discount_of'])/100;
                                                $price = $total - $discount_of;
                                                if(!is_null($taxrateData)){
                                                    $taxrate_price = ($total * $taxrateData->tax_rate)/100;
                                                    $finalPrice += $price + $taxrate_price;
                                                } else {
                                                    $finalPrice += $price;
                                                }
                                            }
                                        } elseif ($value['discount_type']==1) {
                                            // if($taxrateData->tax_rate > 0){
                                            //     $tax_rate = ($value['category_details_with_price']['category_price_6month']*$taxrateData->tax_rate)/100;
                                            //     $total = $value['category_details_with_price']['category_price_6month'] + $tax_rate;
                                            // } else {
                                                $total = $value['category_details_with_price']['category_price_6month'];
                                            // }
                                            // $discount_of    = $value['discount_of'];
                                            // $finalPrice += $total - $discount_of;
                                            if($total > 0){
                                                $discount_of    = $value['discount_of'];
                                                $price = $total - $discount_of;
                                                if(!is_null($taxrateData)){
                                                    $taxrate_price = ($total * $taxrateData->tax_rate)/100;
                                                    $finalPrice += $price + $taxrate_price;
                                                } else {
                                                    $finalPrice += $price;
                                                }
                                            }
                                        }
                                    } else {
                                        if($value['discount_type']==0){
                                            // if($taxrateData->tax_rate > 0){
                                            //     $tax_rate = ($value['category_details_with_price']['category_price']*$taxrateData->tax_rate)/100;
                                            //     $total = $value['category_details_with_price']['category_price'] + $tax_rate;
                                            // } else {
                                                $total = $value['category_details_with_price']['category_price'];
                                            // }
                                            // $discount_of    = ($total*$value['discount_of'])/100;
                                            // $finalPrice += $total - $discount_of;
                                            if($total > 0){
                                                $discount_of    = ($total*$value['discount_of'])/100;
                                                $price = $total - $discount_of;
                                                if(!is_null($taxrateData)){
                                                    $taxrate_price = ($total * $taxrateData->tax_rate)/100;
                                                    $finalPrice += $price + $taxrate_price;
                                                } else {
                                                    $finalPrice += $price;
                                                }
                                            }
                                        } elseif ($value['discount_type']==1) {
                                            // if($taxrateData->tax_rate > 0){
                                            //     $tax_rate = ($value['category_details_with_price']['category_price']*$taxrateData->tax_rate)/100;
                                            //     $total = $value['category_details_with_price']['category_price'] + $tax_rate;
                                            // } else {
                                                $total = $value['category_details_with_price']['category_price'];
                                            // }
                                            // $discount_of    = $value['discount_of'];
                                            // $finalPrice += $total - $discount_of;
                                            if($total > 0){
                                                $discount_of    = $value['discount_of'];
                                                $price = $total - $discount_of;
                                                if(!is_null($taxrateData)){
                                                    $taxrate_price = ($total * $taxrateData->tax_rate)/100;
                                                    $finalPrice += $price + $taxrate_price;
                                                } else {
                                                    $finalPrice += $price;
                                                }
                                            }
                                        }
                                    }   
                                } else if($Input['price_type'] == 2) {
                                    if($Input['for_category']['plan_duration'] == 3) {
                                        // if($taxrateData->tax_rate > 0){
                                        //     $tax_rate = ($value['category_details_with_price']['category_price_sgd_3month']*$taxrateData->tax_rate)/100;
                                        //     $total = $value['category_details_with_price']['category_price_sgd_3month'] + $tax_rate; 
                                        // } else {
                                            $total = $value['category_details_with_price']['category_price_sgd_3month'];
                                        // }
                                        if($value['discount_type']==0){
                                            // $discount_of    = ($total*$value['discount_of'])/100;
                                            // $finalPrice += $total - $discount_of;
                                            if($total > 0){
                                                $discount_of    = ($total*$value['discount_of'])/100;
                                                $price = $total - $discount_of;
                                                if(!is_null($taxrateData)){
                                                    $taxrate_price = ($total * $taxrateData->tax_rate)/100;
                                                    $finalPrice += $price + $taxrate_price;
                                                } else {
                                                    $finalPrice += $price;
                                                }
                                            }
                                        } elseif ($value['discount_type']==1) {
                                            // $discount_of    = $value['discount_of'];
                                            // $finalPrice += $total - $discount_of;
                                            if($total > 0){
                                                $discount_of    = $value['discount_of'];
                                                $price = $total - $discount_of;
                                                if(!is_null($taxrateData)){
                                                    $taxrate_price = ($total * $taxrateData->tax_rate)/100;
                                                    $finalPrice += $price + $taxrate_price;
                                                } else {
                                                    $finalPrice += $price;
                                                }
                                            }
                                        }
                                    } elseif ($Input['for_category']['plan_duration'] == 6) {
                                        // if($taxrateData->tax_rate > 0){
                                        //     $tax_rate = ($value['category_details_with_price']['category_price_sgd_6month']*$taxrateData->tax_rate)/100;
                                        //     $total = $value['category_details_with_price']['category_price_sgd_6month'] + $tax_rate;
                                        // } else {
                                            $total = $value['category_details_with_price']['category_price_sgd_6month'];
                                        // }
                                        if($value['discount_type']==0){
                                            // $discount_of    = ($total*$value['discount_of'])/100;
                                            // $finalPrice += $total - $discount_of;
                                            if($total > 0){
                                                $discount_of    = ($total*$value['discount_of'])/100;
                                                $price = $total - $discount_of;
                                                if(!is_null($taxrateData)){
                                                    $taxrate_price = ($total * $taxrateData->tax_rate)/100;
                                                    $finalPrice += $price + $taxrate_price;
                                                } else {
                                                    $finalPrice += $price;
                                                }
                                            }
                                        } elseif ($value['discount_type']==1) {
                                            // $discount_of    = $value['discount_of'];
                                            // $finalPrice += $total - $discount_of;
                                            if($total > 0){
                                                $discount_of    = $value['discount_of'];
                                                $price = $total - $discount_of;
                                                if(!is_null($taxrateData)){
                                                    $taxrate_price = ($total * $taxrateData->tax_rate)/100;
                                                    $finalPrice += $price + $taxrate_price;
                                                } else {
                                                    $finalPrice += $price;
                                                }
                                            }
                                        }
                                    } else {
                                        // if($taxrateData->tax_rate > 0){
                                        //     $tax_rate = ($value['category_details_with_price']['category_price_sgd']*$taxrateData->tax_rate)/100;
                                        //     $total = $value['category_details_with_price']['category_price_sgd'] + $tax_rate;
                                        // } else {
                                            $total = $value['category_details_with_price']['category_price_sgd'];
                                        // }
                                        if($value['discount_type']==0){
                                            // $discount_of    = ($total*$value['discount_of'])/100;
                                            // $finalPrice += $total - $discount_of;
                                            if($total > 0){
                                                $discount_of    = ($total*$value['discount_of'])/100;
                                                $price = $total - $discount_of;
                                                if(!is_null($taxrateData)){
                                                    $taxrate_price = ($total * $taxrateData->tax_rate)/100;
                                                    $finalPrice += $price + $taxrate_price;
                                                } else {
                                                    $finalPrice += $price;
                                                }
                                            }
                                        } elseif ($value['discount_type']==1) {
                                            // $discount_of    = $value['discount_of'];
                                            // $finalPrice += $total - $discount_of;
                                            if($total > 0){
                                                $discount_of    = $value['discount_of'];
                                                $price = $total - $discount_of;
                                                if(!is_null($taxrateData)){
                                                    $taxrate_price = ($total * $taxrateData->tax_rate)/100;
                                                    $finalPrice += $price + $taxrate_price;
                                                } else {
                                                    $finalPrice += $price;
                                                }
                                            }
                                        }
                                    }   
                                }
                            }
                            $finalPrice = round($finalPrice,2);
                            if($finalPrice>0)
                                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1,$finalPrice);
                            else{
                                $returnMessage = $Input['language']==1?'PROMO_NOT_APPLICABLE':($Input['language']==2?'PROMO_NOT_APPLICABLE_CHINESE':'PROMO_NOT_APPLICABLE_RU');
                                $returnData = UtilityController::Generateresponse(false, $returnMessage, Response::HTTP_BAD_REQUEST);    
                            }
                        } else {
                            $returnMessage = $Input['language']==1?'PROMO_NOT_APPLICABLE':($Input['language']==2?'PROMO_NOT_APPLICABLE_CHINESE':'PROMO_NOT_APPLICABLE_RU');
                            $returnData = UtilityController::Generateresponse(false, $returnMessage, Response::HTTP_BAD_REQUEST);
                        }
                    } else {
                        $returnMessage = $Input['language']==1?'PROMO_NOT_APPLICABLE':($Input['language']==2?'PROMO_NOT_APPLICABLE_CHINESE':'PROMO_NOT_APPLICABLE_RU');
                        $returnData = UtilityController::Generateresponse(false, $returnMessage, Response::HTTP_BAD_REQUEST);
                    }
                }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getMessage(), '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name : Getcategorydetails
    //Author        : Karan Kantesariya <karan@creolestudios.com>
    //Purpose       : Get details of user of user purchase category
    //In Params     : 
    //Return        : json
    //Date          : 17th March 2021
    //###############################################################
    public function Getcategorydetails(Request $request){
        try {
            $Input      = Input::all();
            $userAuth   = Auth::user();
            if($userAuth){
                $categoryId = VideoCategory::where('subject_id',$Input['subject_id'])->whereNull('parent_id')->where('status',1)->pluck('id')->toArray();
                if(!empty($categoryId) && $categoryId != ''){
                    $data['purchaseCategory'] = MarketPlacePayment::whereIn('video_category_id',$categoryId)->where('paid_by',$userAuth->id)->where('paid_for',3)->where('subscription_end_date','>=',Carbon::now()->timezone('Asia/Singapore'))->where('payment_status',1)->where('plan_duration',12)->where('plan_status',1)->get();
                    $switchCount = MarketPlacePayment::where('paid_by',$userAuth->id)->whereNotNull('switch_id')->count();
                    $data['switchCount'] = 2 - $switchCount;
                    if(!empty($data['purchaseCategory']->toArray()) && $data['purchaseCategory']->toArray() != ''){
                        $responseArray = UtilityController::Generateresponse(true, 'GOT_DATA', 1,$data);
                    } else {
                        $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');    
                    }
                } else {
                    $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);    
    }

    //###############################################################
    //Function Name : SwitchhistoryDetails
    //Author        : Karan Kantesariya <karan@creolestudios.com>
    //Purpose       : Get User switch history details
    //In Params     : 
    //Return        : json
    //Date          : 17th March 2021
    //###############################################################
    public function SwitchhistoryDetails(Request $request){
        try {
            $Input = Input::all();
            $userAuth   = Auth::user();
            if($userAuth){
                $categoryId = VideoCategory::where('subject_id',$Input['subject_id'])->whereNull('parent_id')->where('status',1)->pluck('id')->toArray();
                if(!empty($categoryId) && $categoryId != ''){
                    $switchCategoryHistory = MarketPlacePayment::with('switch_category')->whereIn('video_category_id',$categoryId)->where('paid_by',$userAuth->id)->where('paid_for',3)->where('payment_status',1)->where('plan_duration',12)->whereNotNull('switch_id')->where('plan_status',1)->get();
                    foreach ($switchCategoryHistory as $key => $value) {
                        $data[$key]['to']           = $value['video_category_name'];
                        $data[$key]['from']         = $value['switch_category']['video_category_name'];
                        $data[$key]['created_at']   = Carbon::parse($value['created_at'])->format('M d Y H:s:i');
                    }
                    if(!empty($data) && $data != ''){
                        $responseArray = UtilityController::Generateresponse(true, 'GOT_DATA', 1,$data);
                    } else {
                        $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');    
                    }
                } else {
                    $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);    
    }

    //###############################################################
    //Function Name : Getpurchasecategorydetails
    //Author        : Karan Kantesariya <karan@creolestudios.com>
    //Purpose       : Get details of user of user purchase category
    //In Params     : 
    //Return        : json
    //Date          : 17th March 2021
    //###############################################################
    public function Getpurchasecategorydetails(Request $request){
        try {
            $Input      = Input::all();
            $userAuth   = Auth::user();

            if($userAuth){
                ## Get Category Id  
                $categoryId = VideoCategory::where('subject_id',$Input['subject_id'])->whereNull('parent_id')->where('status',1)->pluck('id')->toArray();

                if(!empty($categoryId) && $categoryId != ''){
                    $data['purchaseCategory'] = MarketPlacePayment::with(['video_categories' => function ($query) {
                                $query->with('subject')->get();
                            },
                    ])->whereIn('video_category_id',$categoryId)->where('paid_by',$userAuth->id)->where('paid_for',3)->where('subscription_end_date','>=',Carbon::now()->timezone('Asia/Singapore'))->where('payment_status',1)->where('plan_status',1)->get();

                    ## Upcoming Plan Details
                    $upcoming_plan_count = MarketPlacePayment::whereIn('video_category_id',$categoryId)->where('paid_by',$userAuth->id)->where('paid_for',3)->where('payment_status',1)->where('plan_status',1)->where('upcoming_plan',1)->count();
                    if($upcoming_plan_count >= 1){
                        $data['upcoming_plan_count'] = $upcoming_plan_count;
                    } else {
                        $data['upcoming_plan_count'] = 0;
                    }

                    if(!empty($data['purchaseCategory']->toArray()) && $data['purchaseCategory']->toArray() != ''){
                        $responseArray = UtilityController::Generateresponse(true, 'GOT_DATA', 1,$data);
                    } else {
                        $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
                    }

                } else {
                    $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);    
    }

    //###############################################################
    //Function Name : GetsubscriptionplanHistory
    //Author        : Karan Kantesariya <karan@creolestudios.com>
    //Purpose       : Get User subscription plan history details
    //In Params     : 
    //Return        : json
    //Date          : 19th March 2021
    //###############################################################
    public function GetsubscriptionplanHistory(Request $request){
        try {
            $Input = Input::all();
            $userAuth   = Auth::user();
            if($userAuth){
                $categoryId = VideoCategory::where('subject_id',$Input['subject_id'])->whereNull('parent_id')->where('status',1)->pluck('id')->toArray();
                if(!empty($categoryId) && $categoryId != ''){
                    $subscriptionPlanHistory = MarketPlacePayment::with(['video_categories' => function ($query) {
                                $query->with('subject')->get();
                            },
                ])->whereIn('video_category_id',$categoryId)->where('paid_by',$userAuth->id)->where('paid_for',3)->where('payment_status',1)->where('plan_status',1)->whereNotNull('subscription_id')->where('upcoming_plan',0)->get();
                    if(!empty($subscriptionPlanHistory->toArray()) && $subscriptionPlanHistory != ''){
                        $responseArray = UtilityController::Generateresponse(true, 'GOT_DATA', 1,$subscriptionPlanHistory);
                    } else {
                        $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
                    }
                } else {
                    $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);    
    }

    //###############################################################
    //Function Name: Subscriptionpayemnt
    //Author:        Karan Kantesariya <karan@creolestudios.com>
    //Purpose:       To make payment through subscription payment gateway
    //In Params:     encoded string for amount and category for payment
    //Return:        json
    //Date:          19th March, 2021
    //###############################################################
    public function Subscriptionpayemnt(Request $request){ 
        try {
            if(Auth::check()){    
                $Input = Input::all();
                $wholePath = substr(url('/'), 0, -3);
                $pathToStore = explode('/', $wholePath);
                #beta checking
                $fullPath = url()->full();
                $checkBeta = explode('/', $fullPath);

                $Input['token'] = base64_decode($Input['token']);
                $Input['token'] = explode('_', $Input['token']);

                $amount = base64_decode($Input['token'][0]);
                $category = explode(',', base64_decode($Input['token'][1]));
                foreach ($category as $key => $value) {
                    $purchase_category      = explode('_', $value);
                    $forCategory[$key]['id'] = $purchase_category[0];
                    $forCategory[$key]['plan_duration'] = $purchase_category[1]; 
                }
                $hasPromo = base64_decode($Input['token'][2]);
                $priceType = base64_decode($Input['token'][3]);

                if($hasPromo==1)
                    $Input['promo_code'] = base64_decode($Input['token'][4]);
                $user = Auth::user();
                if(in_array('localhost', $pathToStore) || in_array('beta', $checkBeta))
                    Stripe::setApiKey(env('STRIPE_SANDBOX'));
                else
                    Stripe::setApiKey(env('STRIPE_LIVE'));
                $token = Token::create([
                    'card' => [
                        'number'    => $Input['card_number'],
                        'exp_month' => $Input['exp_month'],
                        'exp_year'  => $Input['exp_year'],
                        'cvc'       => $Input['cvv'],
                    ],
                ]);
                if (!filter_var($user->email_address, FILTER_VALIDATE_EMAIL)) {
                    $customer = Customer::create(array(
                        'source' => $token->id
                    ));
                } else {
                    $customer = Customer::create(array(
                        'email'  => $user->email_address,
                        'source' => $token->id
                    ));
                }

                if($priceType == 1) {
                    $currency = 'usd';
                } else if($priceType == 2) {
                    $currency = 'sgd';
                }
                
                $charge = Charge::create(array(
                    'customer' => $customer->id,
                    // 'amount'   => $paymentFor['category_price'],
                    'amount'   => intval($amount)*100,
                    'currency' => $currency
                ));

                #User stripe card id update.. 
                User::where('id',$user->id)->update(['stripe_id'=>$customer->id]);

                if($charge->paid==1){
                    \DB::beginTransaction();
                        if(array_key_exists('promo_code', $Input) && isset($Input['promo_code'])){
                            $promoData = PromoCode::where('promo_code',$Input['promo_code'])->first();
                            $Input['discount_type'] = $promoData['discount_type'];
                            $Input['discount_of']   = $promoData['discount_of'];
                            if($promoData['code_for']==0){
                                foreach ($forCategory as $key => $value) {
                                    $category_id[] = $value['id'];
                                }
                                $stringToIntArray = array_map('intval', $category_id);
                                $Input['promo_code_id'] = PromoCode::select(DB::RAW('GROUP_CONCAT(id) AS ids'))->where('promo_code',$Input['promo_code'])->whereIn('video_category_id',$stringToIntArray)->first()->toArray();
                                $Input['promo_code_id'] = $Input['promo_code_id']['ids'];
                            } else {
                                $addToPayment['distributor_id'] = $promoData['distributor_id'];
                                $addToPayment['distributer_team_member_id'] = $promoData['distributor_team_member_id'];
                            }
                        }
                        
                        foreach ($forCategory as $key => $value) {

                            $subject_id = VideoCategory::where('id',$value['id'])->get()->pluck('subject_id')->toArray();
                            $category_id = VideoCategory::whereIn('subject_id',$subject_id)->whereNull('parent_id')->pluck('id')->toArray();

                            ## Old Category Details
                            $oldCategory = MarketPlacePayment::whereIn('video_category_id',$category_id)->where('paid_by',$user->id)->where('paid_for',3)->where('subscription_end_date','>=',Carbon::now()->timezone('Asia/Singapore'))->where('payment_status',1)->where('plan_status',1)->latest()->get();

                            ## Delete Old category Cron Job and Job manage Ids 
                            $Ids = DB::table('job_manage')->where('ignite_category_id',$oldCategory[0]['video_category_id'])->where('user_id',$user->id)->pluck('job_id')->toArray();
                            if(!empty($Ids)) {
                                DB::table('jobs')->whereIn('id',$Ids)->delete();
                                DB::table('job_manage')->whereIn('job_id',$Ids)->delete();
                            }

                            ## Delete Old category Cron Job Notify email and Notify email Ids
                            $id = StoreEmail::where('user_id',$user->id)->where('category_id',$oldCategory[0]['video_category_id'])->pluck('job_id')->toArray();
                            if(!empty($id)){
                                foreach($id as $index => $emailId){
                                    $email_job = explode(',', $emailId);
                                    $email_job_delete = DB::table('jobs')->whereIn('id',$email_job)->delete();
                                    $store_email_delete = StoreEmail::where('user_id',$user->id)->where('category_id',$oldCategory[0]['id'])->where('job_id',$emailId)->delete();
                                }
                            }

                            ## Update Old category cancel subscription status
                            $data['is_cancelsubscription'] = 1;
                            $paymentResult = UtilityController::Makemodelobject($data,'MarketPlacePayment','', $oldCategory[0]['id']); 

                            ## Get category history Id
                            $history_id = VideoCategoryHistory::where('category_id',$value['id'])->latest()->first();

                            $catgoryNameArray = implode(', ', VideoCategory::where('id',$value['id'])->get()->pluck('category_name')->toArray());

                            $payment['subscription_id']           = is_null($oldCategory[0]['id'])?'':$oldCategory[0]['id'];
                            $payment['payment_method']            = 5; //strip
                            $payment['paid_by']                   = $user->id;
                            $payment['video_category_id']         = $value['id'];
                            $payment['history_id']                = $history_id['id'];
                            $payment['video_category_name']       = $catgoryNameArray;
                            $payment['subscription_end_date']     = Carbon::now();
                            if($value['plan_duration'] == 3){
                                $subscription_end_date      = Carbon::parse($oldCategory[0]['subscription_end_date'])->addMonths(3)->subDays(1);
                                $payment['plan_duration']   = 3;
                            } elseif ($value['plan_duration'] == 6) {
                                $subscription_end_date      = Carbon::parse($oldCategory[0]['subscription_end_date'])->addMonths(6)->subDays(1);
                                $payment['plan_duration']   = 6;
                            } else {
                                $subscription_end_date      = Carbon::parse($oldCategory[0]['subscription_end_date'])->addYears(1)->subDays(1);
                                $payment['plan_duration']   = 12;
                            }
                            $payment['remarks']                   = 'New subscription';
                            $payment['access_platform']           = 1;
                            $payment['transaction_reference']     = $charge->balance_transaction;
                            $payment['paid_for']                  = 3;
                            $payment['payment_amount']            = $amount;
                            if(array_key_exists('promo_code', $Input) && isset($Input['promo_code'])){   
                                $payment['promo_code']                = $Input['promo_code'];
                                $payment['discount_type']             = $Input['discount_type'];
                                $payment['discount_of']               = $Input['discount_of'];
                                if($promoData!=''&&$promoData['code_for']==0&&isset($Input['promo_code_id']))
                                    $payment['promo_code_id']             = $Input['promo_code_id'];
                            }
                            $payment['payment_status']            = 1;
                            $payment['slug']                      = $user->slug;
                            if(!empty($addToPayment)){
                                $payment['distributor_id']              = $addToPayment['distributor_id'];
                                $payment['distributer_team_member_id']  = $addToPayment['distributor_team_member_id'];
                            }
                            $payment['payment_amount_type']         = $priceType;
                            $payment['plan_status']                 = 1;
                            $payment['system_transaction_number']   = UtilityController::GenerateRunningNumber('MarketPlacePayment','system_transaction_number',"VL");
                            $payment['upcoming_plan']               = 1;
                            $payment['upcoming_plan_date']          = $subscription_end_date;
                            
                            $hours = Carbon::parse($oldCategory[0]['subscription_end_date'])->diffInHours(Carbon::now());
                            $date = Carbon::parse($oldCategory[0]['subscription_end_date'])->format('d M Y');
                            $Ids = $oldCategory[0]['id'];
                        }
                        $paymentResult = UtilityController::Makemodelobject($payment,'MarketPlacePayment');
                        $paymentResult['Ids']   = $Ids;
                        $paymentResult['url']   = URL::to('/');
                        ##Job Manage
                        $paymentResult['forCategory']   = $forCategory;
                        ## Live
                        $IgniteCategory = (new SubscriptionPlanPayment($paymentResult->toArray()))->delay($hours);
                        // ## Test
                        // $IgniteCategory = (new SubscriptionPlanPayment($paymentResult->toArray()))->delay(Carbon::now()->addMinutes(3));
                        $jobId = dispatch($IgniteCategory);
                        
                        if(!empty($paymentResult)){
                            foreach($forCategory as $key => $value){
                                $category_id[] = $value['id'];
                                $plan_duration[] = $value['plan_duration'];
                            }
                            if($paymentResult['promo_code']!=''){
                                $promoData = PromoCode::where('promo_code',$paymentResult['promo_code'])->first();
                
                                if($promoData['code_for']==1||$promoData['code_for']==2){
                                    $checkIfdistributorExists = DistributorsReferrals::where('user_id',$paymentResult['paid_by'])->first();
                                    if(empty($checkIfdistributorExists)){
                                        if($promoData['code_for']==2)
                                            $addReferal['distributor_team_member_id'] = $promoData['distributor_team_member_id'];
                                        $addReferal['distributors_id'] = $promoData['distributor_id'];
                                        $addReferal['user_id'] = $paymentResult['paid_by'];
                                        $addReferal['referal_through'] = $promoData['code_for']==2?4:1;
                                        $addReferalResult = UtilityController::Makemodelobject($addReferal,'DistributorsReferrals');
                                    }
                                }
                                if($promoData['code_for']==0){
                                    $promoCount = PromoCode::whereIn('id',explode(',', $paymentResult['promo_code_id']))->where('status',1)->where('to_date','>=',Carbon::now())->increment('redeemed_count');
                                } else {
                                    $promoCount = PromoCode::where('promo_code',$Input['promo_code'])->where('status',1)->where('to_date','>=',Carbon::now())->increment('redeemed_count');
                                }
                            }
                            $hasDistributor = DistributorsReferrals::with('distributor','salesperson')->where('user_id',$user->id)->orderBy('id','DESC')->first();
                            if(!empty($hasDistributor)){
                                $addDistributortoPayment['distributor_id'] = $hasDistributor['distributors_id'];
                                if($paymentResult['distributer_team_member_id']=='')
                                    $addDistributortoPayment['distributer_team_member_id'] = $hasDistributor['distributor_team_member_id'];
                                $paymentResultDistributor = UtilityController::Makemodelobject($addDistributortoPayment,'MarketPlacePayment','',$paymentResult['id']);
                                $addCommissions = self::Generatecommissionsarray($category_id,$hasDistributor,$paymentResult);
                                IgniteCommissions::insert($addCommissions);
                            }
                            if($priceType == 1) {
                                foreach($forCategory as $key => $value){
                                    if($value['plan_duration'] == 3){
                                        $cate_name[] = VideoCategory::select('id','category_name','category_price_3month as category_price','subject_id','tax_rate')->where('id',$value['id'])->with('subject')->get()->toArray();
                                    } elseif ($value['plan_duration'] == 6) {
                                        $cate_name[] = VideoCategory::select('id','category_name','category_price_6month as category_price','subject_id','tax_rate')->where('id',$value['id'])->with('subject')->get()->toArray();
                                    } elseif ($value['plan_duration'] == 12) {
                                        $cate_name[] = VideoCategory::select('id','category_name','category_price','subject_id','tax_rate')->where('id',$value['id'])->with('subject')->get()->toArray();
                                    }
                                }
                            } else if($priceType == 2) {
                                foreach($forCategory as $key => $value){
                                    if($value['plan_duration'] == 3){
                                        $cate_name[] = VideoCategory::select('id','category_name','category_price_sgd_3month as category_price_sgd','subject_id','tax_rate')->where('id',$value['id'])->with('subject')->get()->toArray();
                                    } elseif ($value['plan_duration'] == 6) {
                                        $cate_name[] = VideoCategory::select('id','category_name','category_price_sgd_6month as category_price_sgd','subject_id','tax_rate')->where('id',$value['id'])->with('subject')->get()->toArray();
                                    } elseif ($value['plan_duration'] == 12) {
                                        $cate_name[] = VideoCategory::select('id','category_name','category_price_sgd','subject_id','tax_rate')->where('id',$value['id'])->with('subject')->get()->toArray();
                                    }
                                }
                            }
                            foreach ($cate_name as $ind => $ele) {
                                $cat_name[] = $ele[0];
                            }

                            $collection = collect($cat_name)->map(function ($name) use($paymentResult) {
                                    if($name['id'] == $paymentResult->video_category_id){
                                        $name['plan_duration'] = $paymentResult->plan_duration;
                                        $dateObject = Carbon::parse($paymentResult->upcoming_plan_date)->format('d M Y');
                                        $name['subscription_end_date'] = $dateObject;        
                                    }
                                return $name;
                            });

                            $category_name['categoies'] = $collection->toArray();
                            $category_name['starting_date'] = $date;
                            if($paymentResult['promo_code']!=''){
                                $category_name['isDiscount'] = 1;
                                $category_name['promoCode'] = $paymentResult['promo_code'];
                                $category_name['discount_type'] = $paymentResult['discount_type'];
                                $category_name['discount_of'] = $paymentResult['discount_of'];
                            } else
                                $category_name['isDiscount'] = 0;
                            
                            $card_number = substr($Input['card_number'], -4);
                            $info['generateReceipt'] = self::IgniteReceiptattachement($forCategory,$priceType,$paymentResult['payment_amount'],$user->email_address,$user->first_name,$user->last_name,$hasDistributor,Carbon::now(),$paymentResult['system_transaction_number'],$category_name,$card_number);
                            $system_transaction_number = $paymentResult['system_transaction_number'];
                            
                            #User mail are sent!
                            $mail['content']='Hello <strong>'.$user->display_name.' , </strong><br/><br/>Thank you for your payment, Your plan '.$paymentResult['video_category_name'] .'- for '. $paymentResult['plan_duration'] .' months will start on '.$date;
                            $mail['footer_content'] ='From,<br /> SixClouds ';
                            $mail['footer']         ='SixClouds';
                            Mail::send('emails.email_template', $mail, function ($message) use ($user,$system_transaction_number,$info) {
                                $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                                $message->to($user->email_address);
                                $message->subject('Thank you for your payment.');
                                $message->attach($info['generateReceipt'],array(
                                                    'as' => $system_transaction_number.'.pdf', 
                                                    'mime' => 'application/pdf'));
                            });
                            $returnData = UtilityController::Generateresponse(true, 'PAYMENT_SUCCESS', 1, '1');
                        }
                    \DB::commit();
                } else {
                    $returnData = UtilityController::Generateresponse(false, 'PAYMENT_FAILED', 0, '0');
                }
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);    
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: SubscriptionPlanPayment
    //Author:        Karan Kantesariya <karan@creolestudios.com>
    //Purpose:       To Manage Jobs
    //In Params:     User purchase plan are active 
    //Return:        json
    //Date:          31th March, 2021
    //###############################################################
    public static function SubscriptionPlanPayment($request) {
        try {
            $Input = $request;
            if(!empty($Input) &&is_array($Input)) {
                ## Varible array method pass
                $userData           = array();
                $CategoryDetails    = array();
                $forCategory        = array();

                $forCategory = $Input['forCategory'];
                ## User Details
                $userData = User::where('id',$Input['paid_by'])->get()->toArray();

                if(!is_null($userData) && !empty($userData)){
                    ## Categroy Details
                    $CategoryDetails = VideoCategory::where('id',$Input['video_category_id'])->get()->toArray();

                    if(!is_null($CategoryDetails) && !empty($CategoryDetails)){
                    \DB::beginTransaction();
                        $payment    = $Input;
                        $Result     = array();

                        unset($payment['subscription_end_date']);
                        unset($payment['forCategory']);
                        unset($payment['renewal_id']);
                        unset($payment['upcoming_plan']);
                        unset($payment['upcoming_plan_date']);
                        unset($payment['subscription_id']);

                        if($Input['plan_duration'] == 12) {
                            $payment['subscription_end_date']     = Carbon::now()->addYears(1)->subDays(1);
                        } elseif ($Input['plan_duration'] == 6) {
                            $payment['subscription_end_date']     = Carbon::now()->addMonths(6)->subDays(1);
                        } elseif ($Input['plan_duration'] == 3) {
                            $payment['subscription_end_date']     = Carbon::now()->addMonths(3)->subDays(1);
                        }

                        $payment['upcoming_plan']       = 0;
                        ## Edit to active upcoming plan
                        $Result = UtilityController::Makemodelobject($payment,'MarketPlacePayment','',$payment['id']);
                        $Result['url']  = $Input['url'];
                        $Result = $Result->toArray();

                        ## There is problem of model in also define variable
                        $update = MarketPlacePayment::where('id',$payment['id'])->update(['subscription_id' => NULL, 'upcoming_plan_date' => NULL ]);

                        ## Old category subscription end date update
                        $data['subscription_end_date']  = Carbon::now();
                        $check_details = MarketPlacePayment::where('id',$payment['Ids'])->get()->toArray();
                        if(Carbon::parse($check_details[0]['subscription_end_date'])->format('d/m/Y') == Carbon::now()->format('d/m/Y')){
                            $check = UtilityController::Makemodelobject($data,'MarketPlacePayment','',$payment['Ids']);
                        } else {
                            $check = UtilityController::Makemodelobject($data,'MarketPlacePayment','',$payment['Ids']);
                        }

                        if(!empty($Result) && is_array($Result)){

                            ## Delete Job and Job manage Id
                            $Ids = DB::table('job_manage')->where('ignite_category_id',$Input['video_category_id'])->where('user_id',$userData[0]['id'])->pluck('job_id')->toArray();
                            if(!empty($Ids)) {
                                DB::table('jobs')->whereIn('id',$Ids)->delete();
                                DB::table('job_manage')->whereIn('id',$Ids)->delete();
                            }

                            ## Delete notify Email Job and notify Email Id
                            $id = StoreEmail::where('user_id',$userData[0]['id'])->where('category_id',$Input['video_category_id'])->pluck('job_id')->toArray();
                            if(!empty($id)){
                                foreach($id as $index => $emailId){
                                    $email_job = explode(',', $emailId);
                                    $email_job_delete = DB::table('jobs')->whereIn('id',$email_job)->delete();
                                    $store_email_delete = StoreEmail::where('user_id',$userData[0]['id'])->where('category_id',$Input['video_category_id'])->where('job_id',$emailId)->delete();
                                }
                            }

                            ## Live
                            if($Input['plan_duration'] == 12) {
                                $IgniteCategory = (new IgniteCategoryPayment($Result))->delay(Carbon::now()->addYears(1)->subDays(1));
                            } elseif ($Input['plan_duration'] == 6) {
                                $IgniteCategory = (new IgniteCategoryPayment($Result))->delay(Carbon::now()->addMonths(6)->subDays(1));
                            } elseif ($Input['plan_duration'] == 3) {
                                $IgniteCategory = (new IgniteCategoryPayment($Result))->delay(Carbon::now()->addMonths(3)->subDays(1));
                            }
                            // ## Test
                            // $IgniteCategory = (new IgniteCategoryPayment($Result))->delay(Carbon::now()->addMinutes(3));
                            $jobId = dispatch($IgniteCategory);
                            DB::table('job_manage')->where('ignite_category_id',$Input['video_category_id'])->where('user_id',$userData[0]['id'])->delete();
                            DB::table('job_manage')->insert(
                                ['ignite_category_id' => $Input['video_category_id'], 'job_id' => $jobId, 'user_id'=>$userData[0]['id'], 'status'=> 1 ]
                            );

                            ## Get taxrate value
                            $taxrateData = TaxRate::latest()->first(); 
                            ## User Notify recurring payment for email
                            // $emailAmount = array();
                            if($Result['payment_amount_type'] == 1){
                                $details = VideoCategory::with('subject')->where('id',$Result['video_category_id'])->get();
                                if($Result['plan_duration'] == 12){                                  
                                    $subTotal = $details[0]['category_price'];
                                } elseif ($Result['plan_duration'] == 6) {
                                    $subTotal = $details[0]['category_price_6month'];
                                } elseif ($Result['plan_duration'] == 3) {
                                    $subTotal = $details[0]['category_price_3month'];
                                }
                            } elseif ($Result['payment_amount_type'] == 2) {
                                $details = VideoCategory::with('subject')->where('id',$Result['video_category_id'])->get();
                                if($Result['plan_duration'] == 12){
                                    $subTotal = $details[0]['category_price_sgd'];
                                } elseif ($Result['plan_duration'] == 6) {
                                    $subTotal = $details[0]['category_price_sgd_6month'];
                                } elseif ($Result['plan_duration'] == 3) {
                                    $subTotal = $details[0]['category_price_sgd_3month'];
                                }
                            }
                            if(!is_null($taxrateData)){
                                $taxrate_price = $subTotal * $taxrateData->tax_rate/100;
                                $emailAmount = $subTotal + $taxrate_price;
                            } else {
                                $emailAmount = $subTotal;
                            }

                            $emailDetails['email_address']          = $userData[0]['email_address'];
                            $emailDetails['display_name']           = $userData[0]['display_name'];
                            $emailDetails['subscription_end_date']  = $Result['subscription_end_date'];
                            $emailDetails['payment_amount_type']    = $Result['payment_amount_type'];
                            $emailDetails['amount']                 = $emailAmount;
                            $emailDetails['video_category_id']      = $Result['video_category_id'];
                            $emailDetails['user_id']                = $userData[0]['id'];
                            $emailDetails['plan_duration']          = $Result['plan_duration'];
                            $emailDetails['video_category_name']    = $Result['video_category_name'];
                            $emailDetails['starting_date']          = Carbon::now()->timezone('Asia/Singapore')->format('d M Y');
                            $emailDetails['subject_name']           = $details[0]->subject['subject_display_name'];
                            $emailDetails['subject_image']           = $Input['url'].'/public/uploads/subject_images/'.$details[0]->subject['subject_image'];
                            $emailDetails['review_url']             = $Input['url'].'/my-subscription';
                            ## Live 
                            $EmailForNotifyFirst  = (new EmailForNotify($emailDetails))->delay($Result['subscription_end_date']->subDays(7));
                            // ## Test
                            // $EmailForNotifyFirst  = (new EmailForNotify($emailDetails))->delay(Carbon::now()->addMinutes(1));
                            $job['first'] = dispatch($EmailForNotifyFirst);
                            ## Live
                            $EmailForNotifySecond = (new EmailForNotify($emailDetails))->delay($Result['subscription_end_date']->subMonths(1));
                            // ## Test
                            // $EmailForNotifySecond  = (new EmailForNotify($emailDetails))->delay(Carbon::now()->addMinutes(1));
                            $job['second'] = dispatch($EmailForNotifySecond);
                            $job_email = implode(',', $job);
                            DB::table('store_email')->insert(
                                ['category_id' => $Result['video_category_id'], 'job_id' => $job_email,'user_id' => $userData[0]['id'], 'status' => 1 ]
                            );

                            ## User send mail active plan Receipt
                            $info = array();
                            $info['email_address'] = $userData[0]['email_address'];
                            $info['current_language'] = 1;
                            $info['system_transaction_number'] = $Result['system_transaction_number'];
                            $path = public_path('/uploads/MP_Attachments/receiptAttachments/payment_receipt_'.$info['system_transaction_number']);    
                            $userFullName = $userData[0]['first_name']." ".$userData[0]['last_name'];

                            $info['subject']=$info['current_language']==1?'Enclosed is your payment receipt.':($info['current_language']==2?'谢谢您的付款。 附件的是您的项付款收据。':'Спасибо за оплату. Ваша квитанция прилагается.');

                            $info['content']='Hello <strong>'.$userFullName.' ,</strong><br/><br/>Your Plan '.$Result['video_category_name'].'- for '.$Result['plan_duration'].' months is active now. <br/><br/>';
                            $info['footer_content']=$info['current_language']==1?'From,<br /> SixClouds':($info['current_language']==2?'六云 ':'SixClouds');
                            $info['footer']=$info['current_language']==1?'SixClouds':($info['current_language']==2?'六云':'SixClouds');
                            Mail::send('emails.email_template', $info, function ($message) use ($info) {
                                $message->from('noreply@sixclouds.net', 'SixClouds');
                                $message->to($info['email_address'])->subject($info['subject']);
                            });
                            // $message->attach(public_path() . UtilityController::Getpath('MP_RECEIPT_ATTACHMENT_UPLOAD_PATH') .'payment_receipt_'.$info['system_transaction_number'].'.pdf',array(
                            //                 'as' => $info['system_transaction_number'].'.pdf', 
                            //                 'mime' => 'application/pdf'));
                        }
                    \DB::commit();
                    }
                }
            }           
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
    }

    //###############################################################
    //Function Name : IgniteSubscriptionReceiptattachement
    //Author : Karan Kantesariya <karan@creolestudios.com>
    //Purpose : generate invoice of the payment
    //In Params : Void
    //Return :
    //Date : 31th Dec 2020
    //###############################################################
    public static function IgniteSubscriptionReceiptattachement($forCategory,$price_type, $totalAmount, $email_address, $first_name,$last_name, $date, $tradeNo,$url,$projectTitle='', $card_number, $serviceExtra='',$current_language=1)
    {
        $current_domain = UtilityController::Getmessage('CURRENT_DOMAIN'); 
        if($current_domain=='cn'){
            $priceUnit='¥';
        }else{
            if($price_type == 1) {
                $priceUnit='$';
            } else if($price_type == 2) {
                $priceUnit='S$';
            }
        }
        if($current_language==1){
            $title      = 'Your Receipt From SixClouds';
            $invoice    = 'Invoice';
            $date_label = 'Date';
            $item       = 'Item';
            $price      = 'Price';
            $ignite     = 'IGNITE';
            $name       = $first_name.' '.$last_name;
        }else{
            $title      = '收据';
            $invoice    = '发票';
            $date_label = '日期';
            $item       = '项目';
            $price      = '价格';
            $ignite     = 'IGNITE';
            $name       = $first_name.' '.$last_name;
        }
        $date=UtilityController::Changedateformat($date, 'd/m/Y');
        $tax_rate = TaxRate::latest()->first();
        $html='<div class="container" style="font-size:13px; height:100%;">
                    <table style="width:100%">
                        <tr>
                            <td rowspan="6"><img src="'.$url.'/resources/assets/images/Corporate_Alternate_Color-1.png" height="60" /></td>
                            <td style="text-align:right"><strong>SixClouds Pte. Ltd.</strong></td>
                        </tr>
                        <tr>
                            <td style="text-align:right">UEN: 201720404G</td>
                        </tr>';
                        if(!is_null($tax_rate)){
                            if(!is_null($tax_rate->gst_number)){
                                $html.='<tr>
                                    <td style="text-align:right">GST Reg No: '.$tax_rate->gst_number.'</td>
                                </tr>';
                            }
                        }
                        $html.='<tr>
                            <td style="text-align:right">6001 Beach Road</td>
                        </tr>
                        <tr>
                            <td style="text-align:right">#09-09 Golden Mile Tower</td>
                        </tr>
                        <tr>
                            <td style="text-align:right">Singapore 199589</td>
                        </tr>
                    </table>
                    <br/>
                    <br/>
                <div class="row" style="font-size:16px; ">
                    <div class="col-md-12" align="center">
                        '.$title.'
                    </div>
                </div>
                <hr>
                <br/>
                <div class="row" style="font-size:16px;">
                    <table style="width:100%">
                        <tr>
                            <td><b> '. $name .' </b></td>
                            <td style="text-align:right"><b> '.$invoice.': '.$tradeNo.' </b></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="text-align:right">'.$date_label.': '. $date .'</td>
                        </tr>
                    </table>
                </div>
                <br>
                <div class="row">
                    <div class="table">
                        <table  width="100%" cellspacing="0" style="border-collapse: collapse;border-spacing: 0px 0px !important; ">
                        
                            <tbody class="row" style="margin-top:5px;">';
                            $subTotal = 0;
                            foreach ($projectTitle['categoies'] as $index => $element) {
                                $data[] = $element;
                            }
                            if(!empty($data)){
                                foreach($data as $key => $value) {
                                    if($price_type == 1) {
                                        if(array_key_exists('subject', $value) && isset($value['subject']))
                                        {
                                            $html.='<tr  class="col-md-12">
                                                <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"><b> Subscription: </b></td>';
                                            $html.= '<td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td>
                                                <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> '.$value['subject']['subject_display_name'].' '.$value['category_name'].' ('.$value['plan_duration'].' months) </b></td></tr>';
                                            $html.='<tr  class="col-md-12">
                                                <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"><b> Subscription Price: </b></td>';
                                            $html.= '<td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td>
                                                <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> $'.$value['category_price'].' starting '. Carbon::now()->format('d M Y') .' </b></td></tr>';
                                            $html.='<tr  class="col-md-12">
                                                <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"><b> Subscription Expiry Date: </b></td>';
                                            $html.= '<td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td>
                                                <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b>'.$value['subscription_end_date'].' </b></td></tr>';
                                            $html.='<tr  class="col-md-12">
                                                <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"><b> Payment Method: </b></td>';
                                            $html.= '<td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td>
                                                <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> Card '.$card_number.' </b></td></tr>';
                                            $html.='<tr class="col-md-12"><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left; border-bottom: 5px solid #fff;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left; border-bottom: 5px solid #fff;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:13px; text-align:right; border-bottom: 5px solid #fff;"><b>$'. $value['category_price'] .'</b></td></tr>';
                                            $subTotal += $value['category_price'];    
                                        }
                                    } else if($price_type == 2) {
                                        if(array_key_exists('subject', $value) && isset($value['subject']))
                                        {
                                            $html.='<tr  class="col-md-12">
                                                <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"><b> Subscription </b></td>';
                                            $html.='<td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td>
                                                <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b>  '.$value['subject']['subject_display_name'].' '.$value['category_name'].' ('.$value['plan_duration'].' months) </b></td></tr>';
                                            $html.='<tr  class="col-md-12">
                                                <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"><b> Subscription Price: </b></td>';
                                            $html.= '<td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td>
                                                <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> S$'.$value['category_price_sgd'].' starting '. Carbon::now() .' </b></td></tr>';
                                            $html.='<tr  class="col-md-12">
                                                <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"><b> Subscription Expiry Date: </b></td>';
                                            $html.= '<td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td>
                                                <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b>'.$value['subscription_end_date'].' </b></td></tr>';
                                            $html.='<tr  class="col-md-12">
                                                <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"><b> Payment Method: </b></td>';
                                            $html.= '<td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td>
                                                <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> Card '.$card_number.' </b></td></tr>';
                                            $html.='<tr class="col-md-12"><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left; border-bottom: 5px solid #fff;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left; border-bottom: 5px solid #fff;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right; border-bottom: 5px solid #fff;"><b>S$'. $value['category_price_sgd'] .'</b></td></tr>';
                                            $subTotal += $value['category_price_sgd'];
                                        }
                                    }
                                }
                            }

                            $html.= '<tr><td class="col-md-4" style="background-color: #f5f5f5;text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> Sub-Total: '.$priceUnit.$subTotal.' </td></tr>';

                            if($projectTitle['isDiscount']==1){
                                // $discountOf = floatval($subTotal)-floatval($totalAmount);
                                if($projectTitle['discount_type']==0){
                                    $discountOf    = (($subTotal*$projectTitle['discount_of'])/100);
                                    $discountOf    = round($discountOf,2);
                                } else if($projectTitle['discount_type']==1) {
                                    $discountOf    = $projectTitle['discount_of'];
                                    $discountOf    = round($discountOf,2);
                                }
                                $html.= '<tr><td class="col-md-4" style="background-color: #f5f5f5;text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> Promo Code ('.$projectTitle['promoCode'].'): '.$priceUnit.$discountOf.' </td></tr>';
                                $html.= '<tr><td class="col-md-4" style="background-color: #f5f5f5;text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b>(Current Subscription period only)</b></td></tr>';
                                $html.= '<tr><td class="col-md-4" style="background-color: #f5f5f5;text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b>'.$projectTitle['discount_of'].'% off ';
                                if($projectTitle['discount_type']==0){
                                    if(!empty($data)) {
                                        foreach($data as $key => $value) {
                                            if(array_key_exists('subject', $value) && isset($value['subject'])) {
                                                if($key == 0){
                                                    $html.= $value['subject']['subject_display_name'].' '.$value['category_name'];
                                                } else {
                                                    $html.= ', '.$value['subject']['subject_display_name'].' '.$value['category_name'];
                                                }
                                            }
                                        }
                                    }
                                }
                                $html.= '</b></td></tr>';
                            }

                            ## Get Taxrate data
                            $taxrateData = TaxRate::latest()->first();
                            if(!is_null($taxrateData)){
                                $taxrateValue = round($taxrateData->tax_rate,2);
                                $taxrate_price = ($subTotal * $taxrateData->tax_rate)/100;
                                $taxrate_price = round($taxrate_price,2);
                                if($taxrateData->tax_rate != 0){
                                    if($price_type == 1) {
                                        $html.='<tr class="col-md-12"><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td>
                                            <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> Tax Rate ('.$taxrateValue.'%): $'.$taxrate_price.'</b></td></tr>';
                                    } elseif ($price_type == 2) {
                                        $html.='<tr class="col-md-12"><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td>
                                            <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> Tax Rate ('.$taxrateValue.'%): S$'.$taxrate_price.'</b></td></tr>';
                                    }
                                } else {
                                    if($price_type == 1) {
                                        $html.='<tr class="col-md-12"><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td>
                                            <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> Tax Rate (0%): $0</b></td></tr>';
                                    } elseif ($price_type == 2) {
                                        $html.='<tr class="col-md-12"><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td>
                                            <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> Tax Rate (0%): S$0</b></td></tr>';
                                    }
                                }
                            } else {
                                if($price_type == 1) {
                                    $html.='<tr class="col-md-12"><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td>
                                        <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> Tax Rate (0%): $0</b></td></tr>';
                                } elseif ($price_type == 2) {
                                    $html.='<tr class="col-md-12"><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:left;"></td>
                                        <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> Tax Rate (0%): S$0</b></td></tr>';
                                }
                            }
                            
                            $totalAmount = round($totalAmount,2);
                            $html.='<tr><td class="col-md-4" style="background-color: #f5f5f5;text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> Total: '.$priceUnit.$totalAmount.' </td></tr>
                            </tbody> 
                        </table>   
                    </div>
                </div>';
                $html.= '<div class="row" style=" width: 100%;">
                            <table>
                                <tr>
                                    <td style="text-align:left;font-size:15px;">Your subscription will renew at ';
                if(!empty($data)){
                    foreach($data as $key => $value) {
                        if($price_type == 1) {
                            if(array_key_exists('subject', $value) && isset($value['subject'])) {
                                if($key == 0){
                                    $html.= '$'.$value['category_price'];
                                } else {
                                    $html.= ' + $'.$value['category_price'];
                                }
                            }
                        } else if($price_type == 2) {
                            if(array_key_exists('subject', $value) && isset($value['subject'])) {
                                if($key == 0){
                                    $html.= 'S$'.$value['category_price'];
                                } else {
                                    $html.= ' + S$'.$value['category_price'];   
                                }    
                            }
                        }

                    }
                } 
                $html.= ' unless you opt-out before the subscription expiry date stated above.</td>
                                </tr>
                                <tr>
                                    <td style="text-align:left;font-size:15px;"> To learn more or opt out, <a href="'.$url.'/my-subscription/active">review your subscription</a>.</td>
                                </tr>
                            </table>
                        </div>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>';
                // if(!empty($hasDistributor)){
                //         $html.= '<div class="row" style="position: absolute; bottom: 0; width: 100%;">
                //                     <table style="width:100%">
                //                         <tr>
                //                             <td style="text-align:left">'.$hasDistributor->distributor->company_name.' is an authorised distributor of </td>
                //                             <td style="text-align:right">Fees paid are non-refundable.</td>
                //                         </tr>
                //                         <tr>
                //                             <td style="text-align:left">SixClouds Pte. Ltd.</td>
                //                             <td style="text-align:right">Please refer to our Terms of Service.</td>
                //                         </tr>
                //                     </table>
                //                 </div>
                //             </div>';
                // } else {
                    $html.= '<div class="row" style="position: absolute; bottom: 0; width: 100%;">
                                <table style="width:100%">
                                    <tr>
                                        <td></td>
                                        <td style="text-align:right">Fees paid are non-refundable.</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td style="text-align:right">Please refer to our Terms of Service.</td>
                                    </tr>
                                </table>
                            </div>
                        </div>';
                // }
        $filename   = 'payment_receipt_'.$tradeNo . ".pdf";
        try{
            //$pdf = App::make('dompdf.wrapper');
            $pdf = PDF::loadHtml($html);
        }  catch (\Exception $e) {
            // $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            print_r($e->getMessage()); die;
        }  
        $finalPath = public_path() . UtilityController::Getpath('MP_RECEIPT_ATTACHMENT_UPLOAD_PATH') . $filename;
        $pdf->save($finalPath); 
        return $finalPath;
    }
}

<?php

/**
 * Short description for file
 *
 * PHP version 5 and 7
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 */
/*
 * Place includes controller for login & forgot password.
 */

/**
  Pre-Load all necessary library & name space
 */

namespace App\Http\Controllers;

//load required library by use
use App\Userprcase;
use App\Userprcaselog;
use App\Proofreadingpackage;
use App\Jobs\CaseAssignment;
//load session & other useful library
use Auth;
use Validator;
use Image;
//define model
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use OSS\OssClient;
use OSS\Core\OssException;
use OSS\Core\OssUtil;
use Carbon\Carbon;

/**
 * Photos
 * @package    PRController
 * @subpackage Controller
 * @author     Senil Shah <senil@creolestudios.com>
 */
class PRController extends BaseController {

    public function __construct() {
        //Artisan::call('cache:clear');
    }

    public function index() {
        
    }

    //###############################################################
    //Function Name : getcases
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To get cases
    //In Params : Void
    //Return : json
    //Date : 6th December 2017
    //###############################################################
    public function getcases(Request $request) {
        try {
            if(Auth::check()){
                $getCase = Userprcase::select('*')->with([
                    'accessment_time' => function($query){
                        $query->select('*')->where('case_status',3)->with(
                            [
                                'accessor_information' => function($query){
                                    $query->select('*');       
                                }
                            ]);
                    },
                    'proof_reading_package' => function($query){
                        $query->select('*');
                    }
                ])->where('cases_type','!=',4)->where('user_id',Auth::user()->id)->orderBy('id','DESC')->get();
                if(!empty($getCase)){
                    foreach ($getCase as $key => $value) {
                        if($getCase[$key]['case_status'] == 1 )
                            $getCase[$key] = self::Getcasestatus('1',$getCase[$key],'');
                        elseif ($getCase[$key]['case_status'] == 2 )
                            $getCase[$key] = self::Getcasestatus('2',$getCase[$key],'');
                        elseif ($getCase[$key]['case_status'] == 3 )
                            $getCase[$key] = self::Getcasestatus('3',$getCase[$key],'');
                        else
                            $getCase[$key] = self::Getcasestatus('4',$getCase[$key],'');
                        $getCase[$key]['document_submitted_at'] = UtilityController::Changedateformat($value['created_at']);
                        $caseAt = Carbon::parse($value['created_at'])->addHours($value['proof_reading_package']['service_package']);
                        $getCase[$key]['deadline'] = $caseAt->toDateTimeString();
                        foreach ($value['accessment_time'] as $keyInner => $valueInner) {
                            if($valueInner['accessor_information']['level_type'] == 1)
                                $getCase[$key] = self::Getcasestatus('5',$getCase[$key],$valueInner);
                            if($valueInner['accessor_information']['level_type'] == 2) 
                                $getCase[$key] = self::Getcasestatus('6',$getCase[$key],$valueInner);
                        }
                    }
                    $responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$getCase);
                    $responseArray['total_cases'] = $getCase->count();
                } else
                    $responseArray = UtilityController::Generateresponse(false,'GENERAL_ERROR',0);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Getcasestatus
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : switch case to get case status accordingly
    //In Params : case status
    //Return : variables to set class and numeric status to user readable format
    //Date : 14th December 2017
    //###############################################################
    public function Getcasestatus($case, $getCase, $valueInner) {
        try {
            switch ($case) {
                case 1:
                    $getCase['case_status_to_string'] = 'Document Submitted';
                    // $getCase['case_class'] = 'back-lr';
                    $getCase['case_class'] = 'red';
                    break;
                case 2:
                    $getCase['case_status_to_string'] = '1st Level Accessment Completed';
                    // $getCase['case_class'] = 'back-lb';
                    $getCase['case_class'] = 'blue';
                    break;
                case 3:
                    $getCase['case_status_to_string'] = '2nd Level Accessment Completed';
                    // $getCase['case_class'] = 'back-ly';
                    $getCase['case_class'] = 'yellow';
                    break;
                case 4:
                    $getCase['case_status_to_string'] = 'Completed';
                    // $getCase['case_class'] = 'back-lg';
                    $getCase['case_class'] = 'green';
                    $getCase['file_download_bubble'] = 'back-green';
                    $getCase['file_downloaded'] = "File downloaded";
                    break;
                case 5:
                    $getCase['accessor_one_case_bubble'] = 'blue';
                    $getCase['accessor_one_time'] = UtilityController::Changedateformat($valueInner['case_resolved_time']);
                    break;
                case 6:
                    $getCase['accessor_two_case_bubble'] = 'yellow';
                    $getCase['accessor_two_time'] = UtilityController::Changedateformat($valueInner['case_resolved_time']);
                    $getCase['accessed_file'] = $valueInner['case_file_name'];
                    break;
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            // $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return $getCase;
    }

    //###############################################################
    //Function Name : Fileupload
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To get uploaded files
    //In Params : Void
    //Return : json
    //Date : 11th December 2017
    //###############################################################
    public function Fileupload(Request $request) {
        try {
            $Input = Input::all();
            if (Input::hasFile('file_uploaded')) {
                $fileSize = $request->file('file_uploaded')->getClientSize();
                if ($fileSize <= UtilityController::Getmessage('FILE_SIZE_5')) {
                    $originalFileName = Input::file('file_uploaded')->getClientOriginalName();
                    $fileName = rand() . time() . str_replace(' ', '_', Input::file('file_uploaded')->getClientOriginalName());
                    $extension = \File::extension($fileName);
                    if (in_array($extension, array('docx','doc','txt','DOCX','DOC','TXT'))) {
                        $request->file('file_uploaded')->move(public_path('').UtilityController::Getpath('PR_DOCUMENT_UPLOAD_PATH'), $fileName);
                        $documentFile = UtilityController::Fileexist($fileName,public_path('').UtilityController::Getpath('PR_DOCUMENT_UPLOAD_PATH'),url('').UtilityController::Getpath('PR_DOCUMENT_UPLOAD_URL'));
                        /*Doc2Txt is function used for converting docx or doc file to txt format*/
                        $fileContent = new Doc2Txt(public_path('').UtilityController::Getpath('PR_DOCUMENT_UPLOAD_PATH').$fileName);
                        $fileContent = $fileContent->convertToText();
                        $wordCount = str_word_count($fileContent);
                        if($wordCount >= UtilityController::Getmessage('PR_MINIMUM_WORD_COUNT')){
                            $getPackages = Proofreadingpackage::select('*')->get();
                            $data = array('total_words' => $wordCount, 'case_file_name' => $fileName, 'case_file_size' => $fileSize, 'packages' => $getPackages, 'case_original_file_name' => $originalFileName);
                            $responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$data); 
                        }
                        else
                            $responseArray = UtilityController::Generateresponse(false,'FILE_MINIMUM_WORD_COUNT',0);
                    } else {
                        $responseArray = UtilityController::Generateresponse(false,'FILE_TYPE_RESTRICTION',0);
                    }
                } else {
                    $responseArray = UtilityController::Generateresponse(false,'FILE_SIZE_LARGE',0);
                }
            } else {
                $fileName = '';
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Addcase
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To add newly uploaded case
    //In Params : Void
    //Return : json
    //Date : 12th December 2017
    //###############################################################
    public function Addcase(Request $request) {
        try {
            $Input                                                = Input::all();
            $Input['uploadedFileDetails']['user_id']              = Auth::user()->id;
            $Input['uploadedFileDetails']['is_higher_word_count'] = ($Input['uploadedFileDetails']['total_words'] >= UtilityController::Getmessage('PR_HIGHER_WORD_COUNT') ? 1 : 0);
            try{
                $bucket    = UtilityController::Getmessage('BUCKET');
                $object    = UtilityController::Getpath('BUCKET_PR_FOLDER').$Input['uploadedFileDetails']['case_file_name'];
                $filePath  = public_path('').UtilityController::Getpath('PR_DOCUMENT_UPLOAD_PATH').$Input['uploadedFileDetails']['case_file_name'];
                $ossClient = new OssClient(UtilityController::Getmessage('ACCESS_KEY_ID'), UtilityController::Getmessage('ACCESS_KEY_SECRET'), UtilityController::Getmessage('END_POINT'));
                $ossClient->uploadFile($bucket, $object, $filePath);
                if($Input['uploadedFileDetails']['is_higher_word_count'] == 0 ){
                    \DB::beginTransaction();
                    $result = UtilityController::Makemodelobject($Input['uploadedFileDetails'],'Userprcase');
                    if($result)
                        $job = dispatch(new CaseAssignment($result['id'],''));
                } else {
                    $Input['uploadedFileDetails']['cases_type'] = 2;
                    $result                                     = UtilityController::Makemodelobject($Input['uploadedFileDetails'],'Userprcase');
                }
                if ($result) {
                    $responseArray = UtilityController::Generateresponse(true,'INSERT_SUCCESS',1,$result);
                } else{
                    $responseArray = UtilityController::Generateresponse(false,'GENERAL_ERROR',0);
                }
                \DB::commit();
            } catch(OssException $e) {
                $sendExceptionMail = UtilityController::Sendexceptionmail($e);
                $responseArray = UtilityController::Generateresponse(false,'TRY_AGAIN',0);
            }
            unlink(public_path('').UtilityController::Getpath('PR_DOCUMENT_UPLOAD_PATH').$Input['uploadedFileDetails']['case_file_name']);
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Rejectcase
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To unlink the file from our file system in case of rejection of case
    //In Params : Void
    //Return : json
    //Date : 11th January 2018
    //###############################################################
    public function Rejectcase(Request $request) {
        try {
            $Input = Input::all();
            $fileUnlink =  unlink(public_path('').UtilityController::Getpath('PR_DOCUMENT_UPLOAD_PATH').$Input['file']);
            $responseArray = UtilityController::Generateresponse(true,'FILE_REJECTED',1,$fileUnlink);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Download
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To download the file after accessment completed
    //In Params : Void
    //Return : json
    //Date : 12th January 2018
    //###############################################################
    public function Download(Request $request) {
        try {
            $Input = Input::all();
            $fileName = ($Input['whichFile']==1 ? $Input['accessed_file'] : $Input['case_file_name']);
            $bucket    = UtilityController::Getmessage('BUCKET');
            $object    = UtilityController::Getpath('BUCKET_PR_FOLDER').$fileName;
            $localfile = public_path('').UtilityController::Getpath('PR_DOCUMENT_DOWNLOAD_PATH').$fileName;
            $options   = array(
                OssClient::OSS_FILE_DOWNLOAD => $localfile,
            );
            try{
                $result['url']       = UtilityController::Getpath('DOWNLOAD_ON_USER_SYSTEM').$fileName;
                $result['file_name'] = $fileName;
                $ossClient           = new OssClient(UtilityController::Getmessage('ACCESS_KEY_ID'), UtilityController::Getmessage('ACCESS_KEY_SECRET'), UtilityController::Getmessage('END_POINT'));
                $ossClient->getObject($bucket, $object, $options);
                if($Input['whichFile'] == 1){
                    $data['case_status'] = 4;
                    $caseStatusChange = UtilityController::Makemodelobject($data,'Userprcase','',$Input['id']);
                }
                $responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$result);
            } catch(OssException $e) {
                $sendExceptionMail = UtilityController::Sendexceptionmail($e);
                $responseArray = UtilityController::Generateresponse(false,'TRY_AGAIN',0);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Unlinkdownloadedfile
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To unlink the downloaded file from our file system
    //In Params : Void
    //Return : json
    //Date : 13th January 2018
    //###############################################################
    public function Unlinkdownloadedfile(Request $request) {
        try {
            $Input = Input::all();
            $fileUnlink =  unlink(public_path('').UtilityController::Getpath('PR_DOCUMENT_DOWNLOAD_PATH').$Input['file_name']);
            $responseArray = UtilityController::Generateresponse(true,'FILE_UNLINKED',1,$fileUnlink);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }
}

<?php

/**
 * Short description for file
 *
 * PHP version 5 and 7
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 */
/*
 * Place includes controller for login & forgot password.
 */

/**
Pre-Load all necessary library & name space
 */

namespace App\Http\Controllers;

//load required library by use
use App\DesignerSkill;
use App\MarketPlaceProject;
use App\ProjectSkill;
use App\SkillSet;
use App\User;
use App\MarketPlaceDesignerSuggestionProject;
//load session & other useful library
use Auth;
use Carbon\carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
//define model
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

/**
 * Photos
 * @package    SuggestionAlgoController
 * @subpackage Controller
 * @author     Nivedita Mitra <nivedita@creolestudios.com>
 */
class SuggestionalgoController extends BaseController
{

    public function __construct()
    {
        //Artisan::call('cache:clear');
    }

    public function index()
    {

    }

    //###############################################################
    //Function Name : Suggestionfunction
    //Author : Nivedita <nivedita@creolestudios.com>
    //Purpose : To get the designers suitable for project common function for methods Designersuggestions
    //In Params : Project ID
    //Return : Array
    //Date : 5th April 2018
    //###############################################################
    public static function Suggestionfunction($mpProjectId, $assigned_designer_id = ""){
        try {
            
            ## getting skills of that project
            $projectSkillInfo = ProjectSkill::where('mp_project_id', $mpProjectId)->get()->toArray();

            $skillIds         = array();

            if($projectSkillInfo){
                foreach ($projectSkillInfo as $key => $value) {
                    array_push($skillIds, $value['skill_set_id']);
                }
                $skillCount = count($projectSkillInfo);
            }
            //echo("<pre>");print_r($skillIds);echo("</pre>");
            if(!empty($skillIds)){
                ## get sellers as per that skill
                $getFinalDesigners = self::Desigerperskill($skillIds,$mpProjectId,$skillCount, $assigned_designer_id);
               
                return $getFinalDesigners;
            }

            /*end*/
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        //return $data;
    }


    //###############################################################
    //Function Name : Desigerperskill
    //Author : Nivedita <nivedita@creolestudios.com>
    //Purpose : To get the designers as per the skills in which the project is created
    //In Params : Skill ids
    //Return : Array of designers if count is <= 5
    //Date : 16th April 2018
    //###############################################################
    public static function Desigerperskill($skillIds,$mpProjectId,$skillCount,$assigned_designer_id){ 
        ## as per skills
        $designers = User::wherehas('services')->where('status', 1)->where('is_designer', 1)->where('availability_unavailability_satus', 1)->wherehas('skills', function($query) use ($skillIds){
            $query->whereIn('skill_set_id', $skillIds);
        })->with('skills','ratings', 'reviews','services');

        if (isset($assigned_designer_id) && $assigned_designer_id != 0) {
            $designers   = $designers->where('id', '!=', $assigned_designer_id);
        }
        
        //$designers = $designers->get();
        
        if(!empty($designers)){

            $getTotalDesigners = self::Counttotaldesigner($designers);
            //echo("<pre>");print_r($getTotalDesigners);echo("</pre>");

            ## if total sellers count is less or equal to 5 then return those sellers in suggestion
            if($getTotalDesigners <= 5){
                $designers = $designers->get();
                self::Logsuggesteddesigners($designers,$mpProjectId); ##insert the suggested designers
                return $designers;
            }
            else{
                ##look as per the BUDGET of the category.
                $designers = self::Filterasperbudget($designers,$mpProjectId,$skillCount,$designers);
                return $designers;
            }

        }
    }

    //###############################################################
    //Function Name : Counttotaldesigner
    //Author : Nivedita <nivedita@creolestudios.com>
    //Purpose : To get the final count of designers array
    //In Params : Designer's array
    //Return : count
    //Date : 16th April 2018
    //###############################################################
    public static function Counttotaldesigner($designers){

        return $designers->count();
    }

    //###############################################################
    //Function Name : Logsuggesteddesigners
    //Author : Nivedita <nivedita@creolestudios.com>
    //Purpose : To insert in the database the log for the designers who has appeared in the suggestion
    //In Params : Designer's array
    //Return : 
    //Date : 16th April 2018
    //###############################################################
    public static function Logsuggesteddesigners($designers,$mpProjectId){

        $arrayToObj = array();
        foreach ($designers as $key => $value) {
            $arrayToObj[$key]['mp_project_id'] = $mpProjectId;
            $arrayToObj[$key]['user_id']       = $value['id'];
        }
        if(!empty($arrayToObj))
            $inserData = UtilityController::Savearrayobject($arrayToObj, 'MarketPlaceDesignerSuggestionProject');
    }

    //###############################################################
    //Function Name : Filterasperbudget
    //Author : Nivedita <nivedita@creolestudios.com>
    //Purpose : To filter the designer as per the budget per category
    //In Params : Designer's array
    //Return : 
    //Date : 17th April 2018
    //###############################################################
    public static function Filterasperbudget($designers,$mpProjectId,$skillCount,$designersQuery){
        
        $projectDetails = MarketPlaceProject::where('id', $mpProjectId)->get()->toArray();
       
        if($skillCount){
          
            $avgCostPerCategory = round($projectDetails[0]['project_budget']/$skillCount);
            
            $designersQuery = $designersQuery->where('mp_designer_min_price', '<=', $avgCostPerCategory)->where('mp_designer_max_price', '>=', $avgCostPerCategory)->get();
           

            $getTotalDesigners = self::Counttotaldesigner($designersQuery);
            

            ## if total sellers count is less or equal to 5 then return those sellers in suggestion
            if($getTotalDesigners <= 5){
                
                self::Logsuggesteddesigners($designersQuery,$mpProjectId); ##insert the suggested designers
                return $designersQuery;
            }
            else{
                ##look as per the TIMELINE FILETER.
                
            }

        }
    }
    
}

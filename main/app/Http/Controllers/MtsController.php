<?php

/**
 * Short description for file
 *
 * PHP version 5 and 7
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 */
/*
 * Place includes controller for login & forgot password.
 */

/**
Pre-Load all necessary library & name space
 */

namespace App\Http\Controllers;

//include_once 'aliyun-php-sdk-core/Config.php';
//include(app_path() . '/aliyun-php-sdk-core/Config.php');
//load required library by use
use Auth;
//load session & other useful library
use Carbon\carbon;
use DateTime;
use File;
use Request;
use Illuminate\Http\Response;
//define model
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use App\VideoMtsDemo;
use App\Videos;
/*use OSS\OssClient;
use Mts\Request\V20140618 as Mts;*/


/**
 * Photos
 * @package    MtsController
 * @subpackage Controller
 * @author     Nivedita Mitra <nivedita@creolestudios.com>
 */
class MtsController extends BaseController
{

    public function __construct()
    {
        //Artisan::call('cache:clear');
    }

    public function index()
    {   

    }

    public function Uploadvideodemo(Request $request){

        try{

            
            $input = Input::all();
            $filepath = '/var/www/html/mts/videos';
            $fileName  = $input['video_upload3']->getClientOriginalName();
            $extension = \File::extension($fileName);
            if (!in_array($extension, array('zip', 'ZIP', 'tar', 'TAR', 'rar', 'RAR', 'js', 'JS', 'tar.gz', 'TAR.GZ'))) {
                $input['video_upload3']->move(($filepath), $fileName);
                chmod($filepath.'/'.$fileName, 0777);
            } else {
                throw new \Exception(UtilityController::Getmessage('NO_COMPRESSED_ALLOWED'));
            }
            $data1 = array(
                'video_name' => $input['video_title'],
                'video_file' => $fileName
            );

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL            => "http://".Request::server('SERVER_NAME')."/mts/main.php",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "",
                CURLOPT_MAXREDIRS      => 10,
                CURLOPT_TIMEOUT        => 30000,
                CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST  => "POST",
                CURLOPT_POSTFIELDS     => $data1,
            ));

            $response = curl_exec($curl);
            $videoUrl = array();
            
            if(isset($response)){

                $videoUrl['video_title'] = $input['video_title'];
                $videoUrl['url']         = $response;
                $videoUrl['created_at']  = Carbon::now();
                $videoUrl['updated_at']  = Carbon::now();
                $insertedData = VideoMtsDemo::insert($videoUrl);
            }
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                print_r(json_decode($response));
            }
        }catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    public function Getvideos(Request $request)
    {
        try {
            
            \DB::beginTransaction();
            //$videoDetail=VideoMtsDemo::all()->toArray();
            $videoDetail = Videos::with('video_urls')->get()->toArray();
            //echo("<pre>");print_r($videoDetail);echo("</pre>");
            if(!empty($videoDetail))
            {
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $videoDetail);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }

            \DB::commit();
            return $returnData;

            } catch (\Exception $e) {
         
                \DB::rollback();
                $sendExceptionMail = UtilityController::Sendexceptionmail($e);
                $returnData        = UtilityController::Generateresponse(false, $e->getMessage(), '', '');
                return $returnData;
            }
    }

}

<?php

/**
 * Short description for file
 *
 * PHP version 5 and 7
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
/*
 * Place includes controller for login & forgot password.
 */
/**
Pre-Load all necessary library & name space
 */

namespace App\Http\Controllers;

//load required library by use
use App\City;
use App\Country;
use App\Language;
use App\Province;
use App\School;
use App\State;
use App\User;
use App\Jobs\SellerUnavailibilitySettingFlag;
use App\Jobs\SignupVerificationLinkNotAccessed;
use App\MarketplaceSellerPayments;
use App\MarketPlaceSellerUnavailability;
use App\MarketPlacePayment;
use App\UserEmailLog;
use App\TicketType;
use App\ProductType;
use App\TicketCategory;
use App\CustomerSupport;
use App\MarketPlaceSellerBankDetails;
use App\Jobs\MpAdminMarkTicketComplete;
use App\Distributors;
use App\DistributorsTeamMembers;
use App\DistributorsReferrals;
use App\Zone;
//load session & other useful library
use Carbon\carbon;
use Auth;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
//define model
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Image;
use ZipArchive;
use OSS\OssClient;

##for sms gatway start
use Qcloud\Sms\SmsSingleSender;
use Qcloud\Sms\SmsMultiSender;
use Qcloud\Sms\SmsVoiceVerifyCodeSender;
use Qcloud\Sms\SmsVoicePromptSender;
use Qcloud\Sms\SmsStatusPuller;
use Qcloud\Sms\SmsMobileStatusPuller;

/**
 * @subpackage Controller
 * @author     Komal Kapadi <komal@creolestudios.com>
 */
class UserController extends BaseController
{
    public function __construct()
    {
        //Artisan::call('cache:clear');
    }

    public function index()
    {

    }

    //###############################################################
    //Function Name : Dosignup
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To register new user
    //In Params : Void
    //Return : json
    //Date : 5th December 2017
    //###############################################################
    public function Dosignup(Request $request)
    {
        try {
            \DB::beginTransaction();
            $input      = Input::all();
            if(array_key_exists('signup_for', $input)) {
                if(array_key_exists('dob', $input)){
                    if(!isset($input['dob']))
                        unset($input['dob']);
                    else
                        $input['dob'] = Carbon::createFromFormat('d/m/Y',$input['dob']);
                }
            }
            $yesImage = 0;          
            if($input['country_id']=='other'){
                $existingCountry = Country::where('name',$input['country_text'])->first();
                if(empty($existingCountry)){
                    $country['name'] = $input['country_text'];
                    $country['phonecode'] = $input['phonecode'];
                    $country['others']=1;
                    $country['status']=2;
                    $countryResult = UtilityController::Makemodelobject($country,'Country');
                    $input['country_id'] = $countryResult['id'];
                } else {
                    $input['country_id'] = $existingCountry['id'];
                }
                $state['name'] = $input['state_text'];
                $state['country_id'] = $input['country_id'];
                $stateResult = UtilityController::Makemodelobject($state,'State');
                $input['provience_id'] = $stateResult['id'];

                $city['name'] = $input['city_text'];
                $city['state_id'] = $stateResult['id'];
                $cityResult = UtilityController::Makemodelobject($city,'City');
                $input['city_id'] = $cityResult['id'];
            }
            $validator  = UtilityController::ValidationRules($input, 'User');
            $returnData = $validator;
            $input['dob'] = strtotime($input['dob']);
            $input['dob'] = date('Y-m-d',$input['dob']);
            $input['current_language'] = $input['language']=='en'?1:($input['language']=='chi'?2:3);

            if (!$validator['status']) {
                $errorMessage = explode('.', $validator['message']);
                $returnData   = UtilityController::Generateresponse(false, $errorMessage['0'], 0);
            } else {
                $UserExists = User::where('email_address', $input['email_address'])->where('status', 1)->first();
                if (!empty($UserExists)) {
                    $returnMessage = $input['language']=='en'?'EMAIL_ALREADY_EXISTS':($input['language']=='chi'?'EMAIL_ALREADY_EXISTS_CHINESE':'EMAIL_ALREADY_EXISTS_RU');
                    //$returnMessage=$input['language']=='en'?'EMAIL_ALREADY_EXISTS':'EMAIL_ALREADY_EXISTS_CHINESE';
                    $returnData = UtilityController::Generateresponse(false, $returnMessage, 400, '');
                    return $returnData;
                }
                /*$UserExists = User::where('contact', $input['contact'])->where('status', 1)->first();
                if (!empty($UserExists)) {
                    $returnMessage=$input['language']=='en'?'CONTACT_ALREADY_EXISTS':($input['language']=='chi'?'CONTACT_ALREADY_EXISTS_CHINESE':'CONTACT_ALREADY_EXISTS_RU');
                    $returnData = UtilityController::Generateresponse(false, $returnMessage, 400, '');
                    return $returnData;
                }*/
                if(array_key_exists('signup_for', $input)){
                    $input['platform'] = 'web';
                    $input['display_name'] = $input['first_name'].' '.$input['last_name'];
                }
                $userRunningNumber = UtilityController::GenerateRunningNumber('User','user_number',"U");                                
                $input['user_number'] = $userRunningNumber;
                $input['status'] = 4;
                $zones = Zone::get()->toArray();
                foreach ($zones as $key => $value)
                    if(in_array($input['country_id'], explode(',', $value['country_id'])))
                        $input['zone_id'] = $value['id'];
                $user = UtilityController::Makemodelobject($input, 'User');
                if (!empty($input['image']) && !is_string($input['image'])) {
                    $yesImage = 1;
                    if ($input['image']->getClientSize() <= UtilityController::Getmessage('FILE_SIZE_2')) {
                        $fileName  = 
                        $extension = \File::extension($input['image']->getClientOriginalName());
                        if (in_array($extension, array('jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG'))) {
                            $file            = $input['image']->getMimeType();
                            $imageName       = date("dmYHis").rand();
                            $DestinationPath = public_path().config('constants.path.USER_PROFILE_PATH');
                            // $data            = url('/').config('constants.path.USER_PROFILE_URL') . $input['image'];

                            if (request('x1') && request('y1') && request('w') && request('h') == 0) {
                            $w = $h = 300;
                            $x = $y = 300;
                            } else {
                            $x = (int) request('x1');
                            $y = (int) request('y1');
                            $w = (int) request('w');
                            $h = (int) request('h');
                            }
                            Image::make(Input::file('image'))->crop($w, $h, $x, $y)->resize(300, 300, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                            })->save($DestinationPath . $imageName);
                        } else {
                            throw new \Exception(UtilityController::Getmessage('ONLY_JPG_PNG_JPEG'));
                        }
                    } else {
                        throw new \Exception($input['image']->getClientOriginalName() . UtilityController::Getmessage('PROFILE_PHOTO_SIZE_LARGE'));
                    }
                }

                $name = $input['first_name'] .' '.$input['last_name'];
                $slug['slug'] = str_slug($name, '-');
                $slug['slug'] = $slug['slug'].':'.base64_encode($user['id']);
                $slug['password'] = Hash::make(Input::get('password'));
                if($yesImage) {
                    if(is_string($input['image']))
                        $slug['image'] = 'NULL';
                    else
                        $slug['image'] = $imageName;
                } else {
                    $slug['image'] = 'NULL';
                }
                $result = UtilityController::Makemodelobject($slug, 'User','',$user['id']);
                if(array_key_exists('signup_through_url', $input)&&isset($input['signup_through_url'])&&array_key_exists('signup_mode', $input)&&isset($input['signup_mode'])) {
                    $checkUrl = url('').'/ignite-signup/'.$input['signup_through_url'].'/'.$input['signup_mode'];
                    if(base64_decode($input['signup_mode'])==3){
                        if(!Distributors::where('unique_link',$checkUrl)->exists())
                            if(!DistributorsTeamMembers::where('unique_link',$checkUrl)->exists())
                                throw new \Exception(UtilityController::getMessage('INVALID_SIGNUP_URL'));
                    } else {
                        if(!Distributors::where('unique_link_qr',$checkUrl)->exists())
                            if(!DistributorsTeamMembers::where('unique_link_qr',$checkUrl)->exists())
                                throw new \Exception(UtilityController::getMessage('INVALID_SIGNUP_URL'));
                    }

                    $distributor = explode('_', base64_decode($input['signup_through_url']));
                    $isWho = base64_decode($distributor[2]);
                    if($isWho==2)
                        $distributorId = base64_decode($distributor[3]);
                    else
                        $distributorId = base64_decode(explode(':', $distributor[1])[1]);
                    if($isWho==1)
                        $distributorExist = Distributors::where('slug',$distributor[1])->where('status',1)->exists();
                    else
                        $distributorExist = DistributorsTeamMembers::where('slug',$distributor[1])->where('status',1)->exists();
                    if(!$distributorExist)
                        throw new \Exception(UtilityController::getMessage('INVALID_SIGNUP_URL'));
                    else {
                        $saveReferral['user_id'] = $user['id'];
                        $saveReferral['referal_through'] = base64_decode($input['signup_mode']);
                        if($isWho==2) {
                            $saveReferral['distributors_id'] = $distributorId;
                            $saveReferral['distributor_team_member_id'] = base64_decode(explode(':', $distributor[1])[1]);
                        } else {
                            $saveReferral['distributors_id'] = $distributorId;
                        }
                        $saveReferralResult = UtilityController::Makemodelobject($saveReferral,'DistributorsReferrals');
                    }
                }
                if ($result) {
                    $registerEmailToken['account_verification_token'] = rand().base64_encode($input['email_address']).date('dmYHis').rand();
                    $registerEmailToken['email'] = base64_encode($input['email_address']);
                    $registerEmailToken['language'] = $input['current_language'];
                    $resultEmailToken = UtilityController::Makemodelobject($registerEmailToken,'User','',$result['id']);
                    $verificationURL = action('MarketPlaceController@Registrationaccountverification', $registerEmailToken);
                    if(UtilityController::getMessage('CURRENT_DOMAIN')=='net') {
                        // User::where('id',$result['id'])->update(['status'=>4]);
                        $registerEmail['user_name'] = $result['first_name'].' '.$result['last_name'];
                        $registerEmail['registerLink'] = $verificationURL;
                        $registerEmail['email_address'] = $input['email_address'];
                        $registerEmail['current_language'] = $registerEmailToken['language'];
                        $registerEmail['showImage'] = 1; //for passing in email template file
                        $registerEmail['is_ignite'] = $result['is_ignite']; //for logo of ignite
                        $registerEmail['subject']=$registerEmail['current_language']==1?'Let\'s Your Child\'s Learning!':($registerEmail['current_language']==2?'点燃你孩子学习的热情吧！':'Позвольте IGNITE учить вашего ребенка!');
                        $supportEmail = (UtilityController::getMessage('CURRENT_DOMAIN')=='cn'?'support@sixclouds.cn':'support@sixclouds.net');

                        // $registerEmail['content']=$registerEmail['current_language']==1?'
                        //     Your account has been created. Please click on the button below to complete your registration process. 
                        //     <br><br><a href="'.$registerEmail['registerLink'].'" class="" style="color: #fff;
                        //                            background-color: #009bdd;
                        //                            border-color: #bd383e;
                        //                            text-decoration: none;display: inline-block;
                        //                            padding: 5px 10px;
                        //                            margin-bottom: 0;
                        //                            margin-top: 3px;
                        //                            font-size: 14px;
                        //                            font-weight: 400;
                        //                            line-height: 1.42857143;
                        //                            text-align: center;
                        //                            white-space: nowrap;
                        //                            vertical-align: middle;
                        //                            -ms-touch-action: manipulation;
                        //                            touch-action: manipulation;
                        //                            cursor: pointer;
                        //                            -webkit-user-select: none;
                        //                            -moz-user-select: none;
                        //                            -ms-user-select: none;
                        //                            user-select: none;
                        //                            background-image: none;
                        //                            border: 1px solid transparent;
                        //                            border-radius: 4px;">Click Here</a><br><br>
                        //     If the link does not work, copy the below link to the browser address bar.
                        //     <br><br>'.$registerEmail['registerLink'].'<br><br>
                        //     If you have any queries, please contact '.$supportEmail:'

                        //     您的帐号已经建立。 请点击以下链接完成注册。
                        //     <br><br><a href="'.$registerEmail['registerLink'].'" class="" style="color: #fff;
                        //                            background-color: #009bdd;
                        //                            border-color: #bd383e;
                        //                            text-decoration: none;display: inline-block;
                        //                            padding: 5px 10px;
                        //                            margin-bottom: 0;
                        //                            margin-top: 3px;
                        //                            font-size: 14px;
                        //                            font-weight: 400;
                        //                            line-height: 1.42857143;
                        //                            text-align: center;
                        //                            white-space: nowrap;
                        //                            vertical-align: middle;
                        //                            -ms-touch-action: manipulation;
                        //                            touch-action: manipulation;
                        //                            cursor: pointer;
                        //                            -webkit-user-select: none;
                        //                            -moz-user-select: none;
                        //                            -ms-user-select: none;
                        //                            user-select: none;
                        //                            background-image: none;
                        //                            border: 1px solid transparent;
                        //                            border-radius: 4px;">请点击这里</a><br><br>
                        //     如果链接不起作用，请将链接复制到浏览器地址栏。
                        //     <br><br>'.$registerEmail['registerLink'].'<br><br>
                        //     如果您有任何疑问，请联系 '.$supportEmail;

                        $coverImageName = url('resources/assets/images/new_email_image.png');
                        $imageName = url('resources/assets/images/sc108x108.png');

                        $registerEmail['content']=$registerEmail['current_language']==1?'
                            <html style="height: 100%; margin: 0;">
                            <div style="background-image: url('.$coverImageName.');
                                background-position: center;
                                background-repeat: no-repeat;
                                background-size: cover;
                                height: 50%;"></div>
                            <div style="background-image: url('.$imageName.');
                                background-position: center;
                                background-repeat: no-repeat;
                                height: 50%;"></div>
                            <span style="font-size:16px;"><b>Welcome to SixClouds!</b></span>
                            <br><br>
                            Dear '.$name.',<br><br>
                            So glad you\'re one step closer to getting your child started on a fun learning journey!<br>
                            <hr>
                            Now, let\'s find out what best suits your child’s needs - click on the button to verify your email and get more info on our Learning Programmes. 
                            <br><br><center><a href="'.$registerEmail['registerLink'].'" class="" style="color: #fff;
                                                   background-color: #88dc6f;
                                                   border-color: #bd383e;
                                                   text-decoration: none;display: inline-block;
                                                   padding: 5px 10px;
                                                   margin-bottom: 0;
                                                   margin-top: 3px;
                                                   font-size: 14px;
                                                   font-weight: 400;
                                                   line-height: 1.42857143;
                                                   text-align: center;
                                                   white-space: nowrap;
                                                   vertical-align: middle;
                                                   -ms-touch-action: manipulation;
                                                   touch-action: manipulation;
                                                   cursor: pointer;
                                                   -webkit-user-select: none;
                                                   -moz-user-select: none;
                                                   -ms-user-select: none;
                                                   user-select: none;
                                                   background-image: none;
                                                   border: 1px solid transparent;
                                                   border-radius: 4px;">Click Here</a></center><br><br>
                            If the link does not work, copy the below link to the browser\'s address bar.
                            <br><br>'.$registerEmail['registerLink'].'<br><br>
                            If you have any queries, please contact support@sixclouds.net<br><br></html>':($registerEmail['current_language']==2?'<html style="height: 100%; margin: 0;">
                            <div style="background-image: url('.$coverImageName.');
                                background-position: center;
                                background-repeat: no-repeat;
                                background-size: cover;
                                height: 50%;"></div>
                            <div style="background-image: url('.$imageName.');
                                background-position: center;
                                background-repeat: no-repeat;
                                height: 50%;"></div>
                            <span style="font-size:16px;"><b>欢迎来到六云！ </b></span>
                            <br><br>
                            您好， '.$name.',<br><br>
                            很高兴您的孩子离开始有趣的学习之旅又近了一步！<br>
                            <hr>
                            让我们了解适合您孩子需要的课程 - 单击以下按钮来验证您的电邮，并获得关于我们智宝的学习计划的更多信息。 
                            <br><br><center><a href="'.$registerEmail['registerLink'].'" class="" style="color: #fff;
                                                   background-color: #88dc6f;
                                                   border-color: #bd383e;
                                                   text-decoration: none;display: inline-block;
                                                   padding: 5px 10px;
                                                   margin-bottom: 0;
                                                   margin-top: 3px;
                                                   font-size: 14px;
                                                   font-weight: 400;
                                                   line-height: 1.42857143;
                                                   text-align: center;
                                                   white-space: nowrap;
                                                   vertical-align: middle;
                                                   -ms-touch-action: manipulation;
                                                   touch-action: manipulation;
                                                   cursor: pointer;
                                                   -webkit-user-select: none;
                                                   -moz-user-select: none;
                                                   -ms-user-select: none;
                                                   user-select: none;
                                                   background-image: none;
                                                   border: 1px solid transparent;
                                                   border-radius: 4px;"> 点击这里 </a></center><br><br>
                            如果按钮不起作用，请将下面的链接复制到浏览器的地址栏上。
                            <br><br>'.$registerEmail['registerLink'].'<br><br>
                            如果您有任何疑问，请联系 
                            support@sixclouds.cn<br><br><html>':'
                            <html style="height: 100%; margin: 0;">
                            <div style="background-image: url('.$coverImageName.');
                                background-position: center;
                                background-repeat: no-repeat;
                                background-size: cover;
                                height: 50%;"></div>
                            <div style="background-image: url('.$imageName.');
                                background-position: center;
                                background-repeat: no-repeat;
                                height: 50%;"></div>
                            <span style="font-size:16px;"><b>Добро пожаловать в SixClouds!</b></span>
                            <br><br>
                             Уважаемый '.$name.',<br><br>
                            Рад, что вы на шаг ближе к тому, чтобы ваш ребенок начал увлекательное путешествие!<br>
                            <hr>
                             Теперь давайте выясним, что лучше всего соответствует потребностям вашего ребенка - нажмите кнопку, чтобы подтвердить свою электронную почту и получить дополнительную информацию о нашей программе обучения IGNITE.. 
                            <br><br><center><a href="'.$registerEmail['registerLink'].'" class="" style="color: #fff;
                                                   background-color: #88dc6f;
                                                   border-color: #bd383e;
                                                   text-decoration: none;display: inline-block;
                                                   padding: 5px 10px;
                                                   margin-bottom: 0;
                                                   margin-top: 3px;
                                                   font-size: 14px;
                                                   font-weight: 400;
                                                   line-height: 1.42857143;
                                                   text-align: center;
                                                   white-space: nowrap;
                                                   vertical-align: middle;
                                                   -ms-touch-action: manipulation;
                                                   touch-action: manipulation;
                                                   cursor: pointer;
                                                   -webkit-user-select: none;
                                                   -moz-user-select: none;
                                                   -ms-user-select: none;
                                                   user-select: none;
                                                   background-image: none;
                                                   border: 1px solid transparent;
                                                   border-radius: 4px;">Кликните сюда</a></center><br><br>
                            Если кнопка не работает, скопируйте приведенную ниже ссылку в адресную строку браузера.
                            <br><br>'.$registerEmail['registerLink'].'<br><br>
                            Если у вас есть какие-либо вопросы, пожалуйста, свяжитесь с support@sixclouds.net<br><br></html>');

                        // print_r($registerEmail['content']);die;

                        $registerEmail['footer_content']=$registerEmail['current_language']==1?'From,<br /> SixClouds':($registerEmail['current_language']==2?'六云 ':'SixClouds');

                        $registerEmail['footer']=$registerEmail['current_language']==1?'SixClouds':($registerEmail['current_language']==2?'六云 ':'SixClouds');
                        Mail::send('emails.email_template', $registerEmail, function ($message) use ($registerEmail) {
                            $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                            $message->to($registerEmail['email_address'])->subject($registerEmail['subject']);

                        });

                        // print_r('here');die;
                        ##expire the verification link if account is not verified till 24 hours
                        $verificationLinkNotAccessed = (new SignupVerificationLinkNotAccessed($result['id']))->delay(Carbon::now()->addHours(48));
                        dispatch($verificationLinkNotAccessed);
                        // $returnData = UtilityController::Generateresponse(true, 'SIGNUP_SUCCESS', 200, '');
                        //$returnMessage = ($result['current_language']==1?'USER_CREATED':'USER_CREATED_CHINESE');
                        $returnMessage = ($result['current_language']==1?'USER_CREATED':($result['current_language']==2?'USER_CREATED_CHINESE':'USER_CREATED_RU'));
                        $data['status'] = 4;
                        $returnData = UtilityController::Generateresponse(true, $returnMessage, 1, $data);
                    } else {
                        /* Send OTP for phone number verification : start */
                            $otpInput['country_code']  = $result['phonecode'];
                            $otpInput['phone_number']  = $result['contact'];
                            $otpInput['email_address'] = $result['email_address'];
                            $otpInput['language'] = $result['current_language'];
                            $otpInput['user_id'] = $result['id'];
                            $otpReturn = self::Sendotp($otpInput);
                            // print_r($otpReturn); die;
                            if($otpReturn['status']==0){
                                throw new \Exception($otpReturn['message']);
                            } elseif($otpReturn['status']==1){
                                // $otp['otp'] = $otpReturn['otp'];
                                // $storeOtp = UtilityController::Makemodelobject($otp, 'User', '', $result['id']);
                                $returnMessage = ($result['current_language']==1?'USER_CREATED_CN':'USER_CREATED_CN_CHINESE');
                                $data['phonecode'] = $result['phonecode'];
                                $data['contact'] = $result['contact'];
                                $data['email_address'] = base64_encode($result['email_address']);
                                $data['status'] = 5;
                                $returnData = UtilityController::Generateresponse(true, $returnMessage, 1, $data);
                            }
                        /* Send OTP for phone number verification : end */
                    }
                } else {
                    $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 400, '');
                }
                \DB::commit();
            }
            return $returnData;
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, $e->getMessage(), '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name: Senduserotp
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to send the otp for phone verification
    //In Params:     country_code, phone_number
    //Return:        json
    //Date:          10th Oct 2018
    //###############################################################
    public function Senduserotp(Request $reques){
        try {
            $Input = Input::all();
            // $user = User::where('email_address',base64_decode($Input['email_address']))->where('phonecode',$Input['country_code'])->where('contact',$Input['phone_number'])->first();
            $user = User::where('email_address',base64_decode($Input['email_address']))->first();
            if(!empty($user)){
                $otpInput['country_code']  = $Input['country_code'];
                $otpInput['phone_number']  = $Input['phone_number'];
                $otpInput['email_address'] = base64_decode($Input['email_address']);
                $otpInput['language'] = $Input['language'];
                $otpInput['user_id'] = $user['id'];
                $otpReturn = self::Sendotp($otpInput);
                // print_r($otpReturn); die;
                if($otpReturn['status']==1){
                    $returnMessage = ($Input['language']==1?'OTP_SENT':($Input['language']==2?'OTP_SENT_CHINESE':'OTP_SENT_RU'));
                    $data['country_code'] = $Input['country_code'];
                    $data['phone_number'] = $Input['phone_number'];
                    $data['email_address'] = $Input['email_address'];
                    $data['status'] = 5;
                    $returnData = UtilityController::Generateresponse(true, $returnMessage, 1, $data);
                } else {
                    $returnData = $otpReturn;
                }
            } else {
                throw new \Exception($Input['language']=='en'?UtilityController::getMessage('NO_SUCH_USER'):UtilityController::getMessage('NO_SUCH_USER_CHINESE'));
            }
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getMessage(), '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: Sendotp
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to send the otp for phone verification
    //In Params:     country_code, phone_number
    //Return:        json
    //Date:          10th Oct 2018
    //###############################################################
    public static function Sendotp($Input){
        try {
            // if(Auth::guard('distributor_team_members')->check()){
                // $Input = Input::all();
                // $userId = Auth::guard('distributor_team_members')->user()->id;
                if(!array_key_exists('country_code', $Input)){
                    throw new \Exception(UtilityController::getMessage('COUNTRY_CODE_REQUIRED'));
                }
                if(!array_key_exists('phone_number', $Input)){
                    throw new \Exception(UtilityController::getMessage('PHONE_NUMBER_REQUIRED'));
                }
                if(!array_key_exists('email_address', $Input)){
                    throw new \Exception(UtilityController::getMessage('PHONE_NUMBER_REQUIRED'));
                }
                if(!array_key_exists('user_id', $Input)){
                    throw new \Exception('User ID required');
                }
                if(!User::where('id',$Input['user_id'])->where('status',1)->exists()){
                    if(isset($Input['phone_number']) && isset($Input['country_code'])) {
                    // print_r($Input); die;
                        \DB::beginTransaction();
                            ## SMS application SDK AppID
                            $appid = UtilityController::Getmessage('APP_ID_SMS'); // Starting with 1400
                            ## SMS application SDK AppKey
                            $appkey = UtilityController::Getmessage('APP_KEY_SMS');
                            ## SMS template ID, you need to apply in the SMS application
                            $templateId = UtilityController::Getmessage('APP_TEMPLATE_ID_SMS');
                            ## signature
                            $smsSign = UtilityController::Getmessage('SMS_SIGN');
                            ## Mobile number that needs to send a text message
                            // $phoneNumbers = ["18883708501"];
                            $phoneNumber = $Input['phone_number'];
                            $countryCode  = $Input['country_code'];
                            $ssender   = new SmsSingleSender($appid, $appkey);
                            $randomOtp = rand(1000,9999);
                            $params    = [$randomOtp, "5"];
                            $returnData = $ssender->sendWithParam($countryCode, $phoneNumber, $templateId, $params, $smsSign, "", "");
                            $returnData = json_decode($returnData,true);
                            if($returnData['errmsg']=='OK'||$returnData['errmsg']=='ok') {
                                $storeOtp = User::where('id',$Input['user_id'])->update(['otp'=>$randomOtp,'contact'=>$phoneNumber,'phonecode'=>$countryCode,'status'=>5]);
                                // print_r($storeOtp); die;
                                if($storeOtp){
                                    $returnData['status'] = 1;
                                    $returnData['message'] = UtilityController::getMessage('OTP_SENT');
                                    $returnData['otp'] = $randomOtp;
                                    $returnData = UtilityController::Generateresponse(true, 'OTP_SENT', 1);
                                    \DB::commit();
                                }
                            } else {
                                $returnData['status'] = 0;
                                $returnMessage = $Input['language']==1?'INVALID_PHONE_NUMBER':'INVALID_PHONE_NUMBER_CHINESE';
                                $returnData = UtilityController::Generateresponse(false, $returnMessage, 401);
                            }
                    } else {
                        $returnData = UtilityController::Generateresponse(false, $Input['language']==1?'GENERAL_ERROR':'GENERAL_ERROR_CHINESE', 400, '');
                    }
                } else {
                    $returnMessage = $Input['language']==1?'VERIFIED':'VERIFIED_CHINESE';
                    $returnData = UtilityController::Generateresponse(false, $returnMessage, 300, '');
                }
            // } else {
            //     $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            // }
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }
        return $returnData;
    }

    //###############################################################
    //Function Name: Verifyotp
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to verify the otp sent to user
    //In Params:     country code, phone number, otp
    //Return:        json
    //Date:          10th Oct, 2018
    //###############################################################
    public function Verifyotp(Request $request){     
        try {
            // if(Auth::guard('distributor_team_members')->check()){
                $Input = Input::all();
                if(array_key_exists('otp', $Input)&&!isset($Input['otp'])){
                    throw new \Exception($Input['language']=='en'?'ENTER_OTP':'ENTER_OTP_CHINESE');
                }
                $correctOtp = User::where('email_address',base64_decode($Input['email_address']))->where('contact',$Input['phone_number'])->where('phonecode',$Input['country_code'])->where('otp',$Input['otp'])->exists();
                if($correctOtp){
                    \DB::beginTransaction();
                        $memberVerified = User::where('email_address',base64_decode($Input['email_address']))->where('contact',$Input['phone_number'])->where('otp',$Input['otp'])->update(['status'=>1,'otp'=>'']);
                        if($memberVerified){
                            $returnData = UtilityController::Generateresponse(true, $Input['language']=='en'?'VERIFIED':'VERIFIED_CHINESE', 1);
                        } else {
                            $returnData = UtilityController::Generateresponse(false, $Input['language']=='en'?'GENERAL_ERROR':'GENERAL_ERROR_CHINESE', 0);
                        }
                    \DB::commit();
                } else {
                    $returnMessage = $Input['language']=='en'?'INVALID_OTP':'INVALID_OTP_CHINESE';
                    $returnData = UtilityController::Generateresponse(false, $returnMessage, 401);
                }
            // } else {
            //     $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            // }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getMessage(), 401, '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name : signup
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To register new user
    //In Params : Void
    //Return : json
    //Date : 5th December 2017
    //###############################################################
    public function Signup(Request $request)
    {
        $data = array();
        try {
            $data['country']  = Country::get();
            $data['city']     = City::get();
            $data['province'] = Province::get();
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
        ## return json response
        return view('front.user_signup', $data);
    }

    //###############################################################
    //Function Name : Checkuseremail
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To check user email
    //In Params : Void
    //Return : json
    //Date : 5th December 2017
    //###############################################################
    public function Checkuseremail(Request $request)
    {
        $email       = $request->email;
        $emailExists = User::where('email_address', $email)->count();
        return ($emailExists > 0 ? 'false' : 'true');
    }

    //###############################################################
    //Function Name : checkuseremailexists
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To check email exists or not for existing user
    //In Params : Void
    //Return : json
    //Date : 5th December 2017
    //###############################################################
    public function checkuseremailexists(Request $request)
    {
        $email       = (!empty($request->email_fp) ? $request->email_fp : $request->email_address);
        $emailExists = User::where('email_address', $email)->count();
        return ($emailExists > 0 ? 'true' : 'false');
    }

    //###############################################################
    //Function Name : Getcities
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get cities of particular country
    //In Params : Void
    //Return : json
    //Date : 5th December 2017
    //###############################################################
    public function Getcities(Request $request)
    {
        try {
            $input     = Input::all();
            $countryId = $request->country_id;
            $cities    = City::select('id', 'name')->whereHas('state', function ($query) use ($input) {
                $query->where('country_id', $input['id']);
            })->get();
            $returnData = UtilityController::Generateresponse(true, '', '', $cities);
            return $returnData;
        } catch (Exception $ex) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Getloginuserdata
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get login user data
    //In Params : Void
    //Return : json
    //Date : 7th December 2017
    //###############################################################
    public function Getloginuserdata(Request $request)
    {
        try {
            if (Auth::check()) {

                //$loginData          = Auth::user();
                $loginData = User::with( 'seller_unavailability','country')->with([
                    'ratings'=> function ($query) {
                        $query->select('rating_to_id','matrix_rating');
                    }])->where('id', Auth::user()->id)->get()->toArray();

                $loginData = $loginData[0];
               
                if($loginData['is_designer']){
                    if(!empty($loginData['ratings'])){
                        foreach ($loginData['ratings'] as $keyOuter => $valueOuter) {
                            $valueOuter = json_decode($valueOuter['matrix_rating'], true);
                            foreach ($valueOuter as $key => $value) {
                                if ($key == 'quality_rating') {
                                    $rating_array[0]['key']   = 'Quality';
                                    $rating_array[0]['value'] = $value;
                                    $qualityArray[] = $value;
                                }
                                if ($key == 'creativity_rating') {
                                    $rating_array[1]['key']   = 'Creativity';
                                    $rating_array[1]['value'] = $value;
                                    $creativityArray[] = $value;
                                }
                                if ($key == 'responsiveness_rating') {
                                    $rating_array[2]['key']   = 'Responsiveness';
                                    $rating_array[2]['value'] = $value;
                                    $responsiveArray[] = $value;
                                }
                                if ($key == 'timeliness_rating') {
                                    $rating_array[3]['key']   = 'Timeliness';
                                    $rating_array[3]['value'] = $value;
                                    $timeArray[] = $value;
                                }
                                if ($key == 'buyers_requirement_rating') {
                                    $rating_array[4]['key']   = 'Comprehension Skills';
                                    $rating_array[4]['value'] = $value;
                                    $buyerRequirementArray[] = $value;
                                }
                            }
                        }
                        $qualityArray = array_sum($qualityArray)/count($qualityArray);
                        
                        $buyerRequirementArray = array_sum($buyerRequirementArray)/count($buyerRequirementArray);
                        
                        $responsiveArray = array_sum($responsiveArray)/count($responsiveArray);
                        
                        $creativityArray = array_sum($creativityArray)/count($creativityArray);
                        
                        $timeArray = array_sum($timeArray)/count($timeArray);
                        
                        foreach ($rating_array as $key => $value) {
                            if ($key == 'Quality') {
                                    $rating_array[0]['name']   = 'Quality';
                                    $rating_array[0]['y'] = $qualityArray;
                                    $rating_value[] = $qualityArray;
                                }
                                if ($key == 'Creativity') {
                                    $rating_array[1]['name']   = 'Creativity';
                                    $rating_array[1]['y'] = $creativityArray;
                                    $rating_value[] = $creativityArray;
                                }
                                if ($key == 'Responsiveness') {
                                    $rating_array[2]['name']   = 'Responsiveness';
                                    $rating_array[2]['y'] = $responsiveArray;
                                    $rating_value[] = $responsiveArray;
                                }
                                if ($key == 'Timeliness') {
                                    $rating_array[3]['name']   = 'Timeliness';
                                    $rating_array[3]['y'] = $timeArray;
                                    $rating_value[] = $timeArray;
                                }
                                if ($key == 'Comprehension Skills') {
                                    $rating_array[4]['name']   = 'Comprehension Skills';
                                    $rating_array[4]['y'] = $buyerRequirementArray;
                                    $rating_value[] = $buyerRequirementArray;
                                } 

                        }
                        ksort($rating_array);
                       
                         $loginData['rating_value'] = $rating_value;

                         $loginData['rating_array'] = $rating_array;

                    }
                    if($loginData['languages_known']!=''){

                        $languages_known=json_decode($loginData['languages_known']);
                        $languages_known_chinese=json_decode($loginData['languages_known_chinese']);
                        if(!empty($languages_known)){
                            foreach ($languages_known as $key => $value) {
                                $loginData['languages'][$key]['languages']=$value->language." - ".$value->proficiency;
                                $loginData['languages'][$key]['key']=$key;
                                $loginData['languages'][$key]['language']=$value->language;
                            }
                        }
                        if(!empty($languages_known_chinese)){
                            foreach ($languages_known_chinese as $key => $value) {
                                $loginData['languages_chinese'][$key]['languages']=$value->language." - ".$value->proficiency;
                                $loginData['languages_chinese'][$key]['key']=$key;
                                $loginData['languages_chinese'][$key]['language']=$value->language;
                            }
                        }
                    }
                }

               $loginData['dob']     = UtilityController::Changedateformat($loginData['dob'], 'd-m-Y'); 
               $loginData['profile_image'] = $loginData['image'];
               
            
               $loginData['image'] = UtilityController::Imageexist($loginData['image'],public_path('/'). UtilityController::Getmessage('USER_PROFILE_PATH'), url('/') .  UtilityController::Getmessage('USER_PROFILE_URL'), url('/') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));

               $loginData['id_proof'] ="http://sixclouds88-bucket.oss-cn-beijing.aliyuncs.com/".UtilityController::Getpath('BUCKET_idproof_FOLDER') .    $loginData['designer_id_proof_document'];
               
                $returnData            = UtilityController::Generateresponse(true, '', '', $loginData);
                return $returnData;
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
        
    }

    //###############################################################
    //Function Name : Getuserpaymenthistory
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : To get the payment history of seller and buyer
    //In Params : Void
    //Return : json
    //Date : 31th May 2018
    //###############################################################
    public function Getuserpaymenthistory()
    {
        try {
            if (Auth::check()) {

                $user = User::where('id', Auth::user()->id)->get();
                
                if($user[0]['is_designer']==1){
                    
                    $paymentData = MarketplaceSellerPayments::where('user_id', Auth::user()->id)->paginate(Config('constants.other.MP_PAYMENT_HISTORY_PAGINATE'))->toArray();

                    $paymentData['user_type']['is_designer']=$user[0]['is_designer'];
                    if(!empty($paymentData['data'])){
                        foreach ($paymentData['data'] as $key => $value) {
                            # code...
                            if($user[0]['is_designer']==1){
                                if($paymentData['data'][$key]['payment_status']==1){
                                    $paymentData['data'][$key]['payment_status']="Pending";
                                }
                                else{
                                    $paymentData['data'][$key]['payment_status']="Successfull";
                                }    
                            }
                        }
                    }
                }
                else{
                    $paymentData = MarketPlacePayment::where('paid_by', Auth::user()->id)->paginate(Config('constants.other.MP_PAYMENT_HISTORY_PAGINATE'))->toArray();
                    $paymentData['user_type']['is_buyer']=$user[0]['is_buyer'];
                    if(!empty($paymentData['data'])){
                        foreach ($paymentData['data'] as $key => $value) {
                            # code...
                            if($user[0]['is_buyer']==1){
                                if($paymentData['data'][$key]['alipay_trade_no']!=''){
                                    $paymentData['data'][$key]['payment_status']="Successfull";
                                }
                                else{
                                    $paymentData['data'][$key]['payment_status']="Pending";
                                }    
                            }
                        }
                    }
                }

                    
                $returnData            = UtilityController::Generateresponse(true, '', '', $paymentData);
                return $returnData;
            }
            else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }

    }

    //###############################################################
    //Function Name : Checkoldpassword
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : To check old password
    //In Params : Void
    //Return : json
    //Date : 22th May 2018
    //###############################################################
    public function Checkoldpassword(Request $request){

        $user = User::find(auth()->user()->id);
        if(!Hash::check($request->old_password, $user->password)){
            return 0;
        }else{
           return 1;
        }

    }

    //###############################################################
    //Function Name : Updatepassword
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : To update password
    //In Params : Void
    //Return : json
    //Date : 22th May 2018
    //###############################################################
    public function Updateuserpassword(Request $request){
        try {
            if (Auth::check()) {
                \DB::beginTransaction();
                $input = Input::all();
                $result = User::where('id',Auth::user()->id)->update(array('password' => Hash::make($input['new_password'])));
                
                if($result){
                    \DB::commit();
                    $returnMessage = $input['language']=='en'?'PASSWORD_UPADTED':'PASSWORD_UPADTED_CHINESE';
                    $responseArray = UtilityController::Generateresponse(true, $returnMessage, 1);
                }else {
                    $responseArray = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
                }
            }
            else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }
        return $responseArray;
    }
    //###############################################################
    //Function Name : Updateuserdata
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : To update user general informations
    //In Params : Void
    //Return : json
    //Date : 22th May 2018
    //###############################################################
    public function Updateuserdata(Request $request)
    {
        try {
            if (Auth::check()) {
                \DB::beginTransaction();
                $input = Input::all();

                if($input['country_id']=='other'){
                    $existingCountry = Country::where('name',$input['country_text'])->first();
                    if(empty($existingCountry)){   
                        $country['name'] = $input['country_text'];
                        $country['phonecode'] = $input['phonecode'];
                        $country['others']=1;
                        $country['status']=2;
                        $countryResult = UtilityController::Makemodelobject($country,'Country');
                        $input['country_id'] = $countryResult['id'];
                    } else {
                        $input['country_id'] = $existingCountry['id'];
                    }
                    
                    $state['name'] = $input['state_text'];
                    $state['country_id'] = $input['country_id'];
                    $stateResult = UtilityController::Makemodelobject($state,'State');
                    $input['provience_id'] = $stateResult['id'];

                    $city['name'] = $input['city_text'];
                    $city['state_id'] = $stateResult['id'];
                    $cityResult = UtilityController::Makemodelobject($city,'City');
                    $input['city_id'] = $cityResult['id'];
                }          
                $UserExists             = User::where('email_address', $input['email_address'])->where('status', 1)->where('id','!=', Auth::user()->id)->first();

                if (!empty($UserExists)) {
                    $returnData        = UtilityController::Generateresponse(false, 'EMAIL_ALREADY_EXISTS', '', '');    
                }
                else{
                    if (!empty($input['image']) && !is_string($input['image'])) {
                        if ($input['image']->getClientSize() <= UtilityController::Getmessage('FILE_SIZE_2')) {
                            $fileName  = 
                            $extension = \File::extension($input['image']->getClientOriginalName());
                            if (in_array($extension, array('jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG'))) {
                                $file            = $input['image']->getMimeType();
                                $imageName       = date("dmYHis").rand();
                                $DestinationPath = public_path().config('constants.path.USER_PROFILE_PATH');
                                $data            = url('/').config('constants.path.USER_PROFILE_URL') . $input['image'];

                                if (request('x1') && request('y1') && request('w') && request('h') == 0) {
                                $w = $h = 290;
                                $x = $y = 294;
                                } else {
                                $x = (int) request('x1');
                                $y = (int) request('y1');
                                $w = (int) request('w');
                                $h = (int) request('h');
                                }
                                Image::make(Input::file('image'))->crop($w, $h, $x, $y)->resize(300, 300, function ($constraint) {
                                $constraint->aspectRatio();
                                $constraint->upsize();
                                })->save($DestinationPath . $imageName);


                            } else {
                                throw new \Exception(UtilityController::Getmessage('ONLY_JPG_PNG_JPEG'));
                            }
                        } else {
                            throw new \Exception($input['image']->getClientOriginalName() . UtilityController::Getmessage('PROFILE_PHOTO_SIZE_LARGE'));
                        }

                        if(is_string($input['image']))
                            $input['image'] = 'NULL';
                        else
                            $input['image'] = $imageName;
                        }
                    else{
                        if(!empty($input['profile_image'])){
                            $input['image'] = $input['profile_image'];
                        }
                    }
                    
                    $result = UtilityController::Makemodelobject($input, 'User', '', Auth::user()->id); 
                    $dataUpdate['user_id']           = Auth::user()->id;
                    $dataUpdate['old_email_address'] = $input['old_email'];
                    $dataUpdate['new_email_address'] = $input['email_address'];
                    
                    if($input['old_email']!=$input['email_address']){
                        $result = UtilityController::Makemodelobject($dataUpdate, 'UserEmailLog');    
                    }
                    
                    $loginData = User::with('country','payment', 'seller_unavailability')->where('id', Auth::user()->id)->get()->toArray();

                    $loginData = $loginData[0];
                   
                    if($loginData['is_designer']){
                        if($loginData['languages_known']!=''){
                            $languages_known=json_decode($loginData['languages_known']);
                            if(!empty($languages_known)){
                                foreach ($languages_known as $key => $value) {
                             
                                    $loginData['languages'][$key]['languages']=$value->language." - ".$value->proficiency;
                                    $loginData['languages'][$key]['key']=$key;
                                    $loginData['languages'][$key]['language']=$value->language;
                                }
                            }
                        }
                    }
                    $loginData['profile_image'] = $loginData['image'];
                    $loginData['image'] = UtilityController::Imageexist($loginData['image'],public_path('/'). UtilityController::Getmessage('USER_PROFILE_PATH'), url('/') .  UtilityController::Getmessage('USER_PROFILE_URL'), url('/') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));
                    $returnMessage = ($input['language']=='en'?'PROFILE_UPDATED_SUCCESSFULLY':'PROFILE_UPDATED_SUCCESSFULLY_CHINESE');
                    $returnData = UtilityController::Generateresponse(true, $returnMessage, '', $loginData);
                    \DB::commit();
                }
                return $returnData;
                
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }
        return $returnData;
    }

    //###############################################################
    //Function Name : Addsellerunavailability
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : add seller unavailability
    //In Params : Void
    //Return : json
    //Date : 27th March 2018
    //###############################################################
    public function Addsellerunavailability(Request $request){
        try {
            
            if (Auth::check()) {
                \DB::beginTransaction();
                $Input = Input::all();
                $data['user_id']=Auth::user()->id;
                
                $startDate = $Input['start_date'];                
                $Input['start_date']               = strtotime($Input['start_date']);
                $data['unavailability_start_date'] = date('Y-m-d',$Input['start_date']);

                $endDate = $Input['end_date'];
                $Input['end_date'] = strtotime($Input['end_date']);

                ##this is to set availability_unavailability_satus to 2 once start date is the current date*/
                $notifySellerForUnAvailibilityStatus = (new SellerUnavailibilitySettingFlag($data['user_id'], 2))->delay(Carbon::now()->diffInMinutes(Carbon::parse($startDate)));
                dispatch($notifySellerForUnAvailibilityStatus);
                

                ##this is to set availability_unavailability_satus to 1 once end date is the current date*/
                $notifySellerForAvailibilityStatus = (new SellerUnavailibilitySettingFlag($data['user_id'], 1))->delay(Carbon::now()->diffInMinutes(Carbon::parse($endDate)));
                dispatch($notifySellerForAvailibilityStatus);
                

                $data['unavailability_end_date'] = date('Y-m-d',$Input['end_date']);

                $result = UtilityController::Makemodelobject($data, 'MarketPlaceSellerUnavailability');

                $unavailabilityData['seller_unavailability']=MarketPlaceSellerUnavailability::where('user_id',Auth::user()->id)->get();
                $returnMessage = ($Input['language']=='en'?'ADDED':'ADDED_CHINESE');
                $returnData            = UtilityController::Generateresponse(true, $returnMessage, '', $unavailabilityData);
                \DB::commit();
                return $returnData;
            }
            else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }
        return $returnData;

    }

    //###############################################################
    //Function Name : Deleteunavailablity
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : delete unavailability of user
    //In Params : Void
    //Return : json
    //Date : 27th March 2018
    //###############################################################
    public function Deleteunavailablity(){
         try {
            if (Auth::check()) {
                \DB::beginTransaction();
                $Input           = Input::all();

                $removeAttention = MarketPlaceSellerUnavailability::where('id', $Input['id'])->forceDelete();

                $unavailabilityData['seller_unavailability'] = MarketPlaceSellerUnavailability::where('user_id',Auth::user()->id)->get();
                $returnMessage                               = ($Input['language'] == 'en'?'DELETED':'DELETED_CHINESE');
                $returnData                                  = UtilityController::Generateresponse(true, $returnMessage, '', $unavailabilityData);
                \DB::commit();

                return $returnData;
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData     = UtilityController::Generateresponse(false, $e->getMessage(), 0);
        }
        return $returnData;
    }

    //###############################################################
    //Function Name : Updatesellerdetail
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : Update designer details
    //In Params : Void
    //Return : json
    //Date : 27th March 2018
    //###############################################################
    public function Updatesellerdetails(Request $request){
        
        try {
            
            if (Auth::check()) {
                \DB::beginTransaction();
                $Input=Input::all();
                
                $data['school_id']=$Input['school_id'];
                $data['mp_designer_proj_availability']=$Input['radio-group'];
                $data['about_us']=$Input['about_seller'];
                
                if(!empty($Input['points'])){
                    $points=explode(',', $Input['points']);
                    $data['mp_designer_min_price']=$points[0];
                    $data['mp_designer_max_price']=$points[1];
                }
                $result = UtilityController::Makemodelobject($data, 'User', '', Auth::user()->id);
                
                $loginData = User::with('payment', 'seller_unavailability')->where('id', Auth::user()->id)->get()->toArray();
               
                $loginData = $loginData[0];
               
                if($loginData['is_designer']){
                    if($loginData['languages_known']!=''){
                        $languages_known=json_decode($loginData['languages_known']);
                        if(!empty($languages_known)){
                            foreach ($languages_known as $key => $value) {
                         
                                $loginData['languages'][$key]['languages']=$value->language." - ".$value->proficiency;
                                $loginData['languages'][$key]['key']=$key;
                                $loginData['languages'][$key]['language']=$value->language;
                            }
                        }
                    }
                }
                
               $loginData['profile_image'] = $loginData['image']; 
               $loginData['image'] = UtilityController::Imageexist($loginData['image'],public_path('/'). UtilityController::Getmessage('USER_PROFILE_PATH'), url('/') .  UtilityController::Getmessage('USER_PROFILE_URL'), url('/') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));
                $returnMessage = ($Input['language'] == 'en'?'PROFILE_UPDATED_SUCCESSFULLY':'PROFILE_UPDATED_SUCCESSFULLY_CHINESE');
                $returnData    = UtilityController::Generateresponse(true, $returnMessage, '', $loginData);
                \DB::commit();
                return $returnData;
            }
            else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }
        return $returnData;
        
    }

    //###############################################################
    //Function Name : Addlanguage
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : add language for seller
    //In Params : Void
    //Return : json
    //Date : 27th March 2018
    //###############################################################
    public function Addlanguage(Request $request){

        $input=Input::all();
        $system_language = $input['system_language'];
        unset($input['system_language']);
        try {
            if (Auth::check()) {
                \DB::beginTransaction();
                $languageQuery=User::select('languages_known')->where('id',Auth::user()->id)->get();
                $languageQueryChinese=User::select('languages_known_chinese')->where('id',Auth::user()->id)->get();
                
                $languageQuery=$languageQuery[0]->languages_known;
                $languageQueryChinese=$languageQueryChinese[0]->languages_known_chinese;
                
                $input['language'] = $input['language'];
                $input['proficiency'] = $input['proficiency'];
                
                $inputchinese['language'] = $input['languageChinese'];
                $inputchinese['proficiency'] = $input['proficiencyChinese'];
                
                unset($input['languageChinese']);
                unset($input['proficiencyChinese']);
                
                if($languageQuery==''){
                    $input['languages_known']=json_encode(array($input));
                }
                else{

                    $language=json_decode($languageQuery);
                  
                    $input=array($input);
                    $languageArray=array_merge($language,$input);
                    $input['languages_known']=json_encode($languageArray);
                }

                if($languageQueryChinese==''){
                    $input['languages_known_chinese']=json_encode(array($inputchinese));
                }
                else{

                    $languagechinese=json_decode($languageQueryChinese);
                  
                    $inputchinese=array($inputchinese);
                    $languageArrayChinese=array_merge($languagechinese,$inputchinese);
                    $input['languages_known_chinese']=json_encode($languageArrayChinese);
                }
                $result = User::where('id',Auth::user()->id)->update(array('languages_known' => $input['languages_known']));
                $resultChinese = User::where('id',Auth::user()->id)->update(array('languages_known_chinese' => $input['languages_known_chinese']));

                $languageQuery=User::select('languages_known','languages_known_chinese','is_designer')->where('id',Auth::user()->id)->first();

                if($languageQuery['is_designer']){
                    if($languageQuery['languages_known']!=''){
                        $languages_known=json_decode($languageQuery['languages_known']);
                        if(!empty($languages_known)){
                            foreach ($languages_known as $key => $value) {

                                $loginData['languages'][$key]['languages']=$value->language." - ".$value->proficiency;
                                $loginData['languages'][$key]['key']=$key;
                                $loginData['languages'][$key]['language']=$value->language; 
                            }
                        }
                    }

                    if($languageQuery['languages_known_chinese']!=''){
                        $languages_known_chinese=json_decode($languageQuery['languages_known_chinese']);
                        if(!empty($languages_known_chinese)){
                            foreach ($languages_known_chinese as $key => $value) {

                                $loginData['languages_chinese'][$key]['languages']=$value->language." - ".$value->proficiency;
                                $loginData['languages_chinese'][$key]['key']=$key;
                                $loginData['languages_chinese'][$key]['language']=$value->language; 
                            }
                        }
                    }
                }
                $returnMessage = ($system_language=='en'?'LANGUAGE_ADDED':'LANGUAGE_ADDED_CHINESE');
                $returnData = UtilityController::Generateresponse(true, $returnMessage, '', $loginData);
                \DB::commit();
                return $returnData;
            }
        }catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, $e->getMessage(), '', '');
        }
        return $returnData;

    }

    //###############################################################
    //Function Name : Deletelanguage
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : delete language for seller
    //In Params : Void
    //Return : json
    //Date : 27th March 2018
    //###############################################################
    public function Deletelanguage(Request $request){
        try {
            if (Auth::check()) {
                \DB::beginTransaction();
                $Input           = Input::all();
                $key=$Input['key'];
                
                $languageQuery=User::select('languages_known')->where('id',Auth::user()->id)->get();
                $languageQueryChinese=User::select('languages_known_chinese')->where('id',Auth::user()->id)->get();

                $languageQuery=$languageQuery[0]->languages_known;
                $language=array(json_decode($languageQuery));

                $languageQueryChinese=$languageQueryChinese[0]->languages_known_chinese;
                $languageChinese=array(json_decode($languageQueryChinese));
                
                unset($language[0][$key]);
                unset($languageChinese[0][$key]);

                $language=array_values($language[0]);
                $languageChinese=array_values($languageChinese[0]);


                $input['languages_known']=json_encode($language);
                $input['languages_known_chinese']=json_encode($languageChinese);
                
                $result = User::where('id',Auth::user()->id)->update(array('languages_known' => $input['languages_known']));
                $result = User::where('id',Auth::user()->id)->update(array('languages_known_chinese' => $input['languages_known_chinese']));

                $languageQuery=User::select('languages_known','languages_known_chinese','is_designer')->where('id',Auth::user()->id)->first();

                $loginData=[];

                if($languageQuery['is_designer']){
                    if($languageQuery['languages_known']!=''){
                        $languages_known=json_decode($languageQuery['languages_known']);
                        if(!empty($languages_known)){
                            foreach ($languages_known as $key => $value) {
                                $loginData['languages'][$key]['language']=$value->language;
                                $loginData['languages'][$key]['languages']=$value->language." - ".$value->proficiency;
                                $loginData['languages'][$key]['key']=$key;
                               
                            }
                        }
                    }
                    if($languageQuery['languages_known_chinese']!=''){
                        $languages_known_chinese=json_decode($languageQuery['languages_known_chinese']);
                        if(!empty($languages_known_chinese)){
                            foreach ($languages_known_chinese as $key => $value) {
                                $loginData['languages_chinese'][$key]['language']=$value->language;
                                $loginData['languages_chinese'][$key]['languages']=$value->language." - ".$value->proficiency;
                                $loginData['languages_chinese'][$key]['key']=$key;
                               
                            }
                        }
                    }
                }
                $returnMessage = ($Input['language']=='en'?'LANGUAGE_DELETED':'LANGUAGE_DELETED_CHINESE');
                $returnData            = UtilityController::Generateresponse(true, $returnMessage, '', $loginData);
                \DB::commit();
                return $returnData;

            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData     = UtilityController::Generateresponse(false, $e->getMessage(), 0);
        }
        return $returnData;
    }

    //###############################################################
    //Function Name : Editbankdetails
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : edit bank details for seller
    //In Params : Void
    //Return : json
    //Date : 31th May 2018
    //###############################################################
    public function Editbankdetails(Request $request){
        try {
            
            if (Auth::check()) {
                \DB::beginTransaction();
                $Input               = Input::all();
                if($Input['country']!='undefined'||$Input['fields']!='undefined'){
                    $Input['country_id'] = $Input['country'];
                    $Input['user_id']    = Auth::user()->id;
                    $submittedFields     = explode(',', $Input['fields']);
                    $fields              = ['id_number','bank_name','account_name_chinese','account_name_eng','account_number','sub_bank_name','sub_bank_province','sub_bank_city','store_front_url','branch_code','swift_bic','bank_province','bank_code','account_type','japan_field','iban','pan','ifsc_code','suffix','sort_code','routing_number','branch_name'];
                    
                    foreach ($fields as $key => $value) {
                        foreach ($submittedFields as $keyInner => $valueInner) {
                            if($value==$valueInner)
                                unset($fields[$key]);
                        }
                    }

                    $fields    = array_values($fields);
                    $validator = UtilityController::ValidationRules($Input, 'MarketPlaceSellerBankDetails',$fields);
                    if (!$validator['status']) {
                        $errorMessage  = explode('.', $validator['message']);
                       
                        if( in_array( "The bank name field is required" ,$errorMessage ) ){
                            $returnMessage = ($Input['language']=='en'?'BANK_NAME_REQUIRED':'BANK_NAME_REQUIRED_CHINESE');
                            throw new \Exception(UtilityController::Getmessage($returnMessage));
                        }
                        else if(in_array("The account name eng field is required",$errorMessage)){
                            $returnMessage = ($Input['language']=='en'?'ACCOUNT_NAME_REQUIRED':'ACCOUNT_NAME_REQUIRED_CHINESE');
                            throw new \Exception(UtilityController::Getmessage($returnMessage));
                        }
                        else if(in_array("The account number field is required",$errorMessage)){
                            $returnMessage = ($Input['language']=='en'?'ACCOUNT_NUMBER_REQUIRED':'ACCOUNT_NUMBER_REQUIRED_CHINESE');
                            throw new \Exception(UtilityController::Getmessage($returnMessage));
                        }
                        else if(in_array("The branch name field is required",$errorMessage)){
                            $returnMessage = ($Input['language']=='en'?'BRANCH_NAME_REQUIRED':'BRANCH_NAME_REQUIRED_CHINESE');
                            throw new \Exception(UtilityController::Getmessage($returnMessage));
                        }
                        $returnData = UtilityController::Generateresponse(false, $errorMessage['0'], 0);
                    } else {
                        $checkIfDetailsExist = MarketPlaceSellerBankDetails::where('user_id',Auth::user()->id)->orderBy('id','DESC')->first();
                        
                        if(!empty($checkIfDetailsExist))
                            MarketPlaceSellerBankDetails::where('user_id',Auth::user()->id)->forceDelete();
                        $result = UtilityController::Makemodelobject($Input,'MarketPlaceSellerBankDetails');
                        if($result){
                            $returnMessage = ($Input['language']=='en'?'BANK_DETAILS_UPADTED':'BANK_DETAILS_UPADTED_CHINESE');
                            $returnData = UtilityController::Generateresponse(true, $returnMessage, '', $result);   
                            \DB::commit();
                        }
                    }    
                } else {
                    $returnMessage = ($Input['language']=='en'?'SELECT_COUNTRY':'SELECT_COUNTRY_CHINESE');
                    throw new \Exception(UtilityController::Getmessage($returnMessage));
                }
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
                $Input=Input::all();
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData     = UtilityController::Generateresponse(false, $e->getMessage(), 0);
        }
        return $returnData;

    }

    //###############################################################
    //Function Name : Getcountrycitypro
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get country  city and provience
    //In Params : Void
    //Return : json
    //Date : 27th March 2018
    //###############################################################
    public function Getcountrycitypro(Request $request)
    {
        $data = array();
        try {
            $data['country'] = Country::get();
            // $data['city']     = City::get();
            // $data['province'] = Province::get();
            $returnData = UtilityController::Generateresponse(true, '', '', $data);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }
        return $returnData;
    }

    //###############################################################
    //Function Name : Getcountry
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To get country
    //In Params : Void
    //Return : json
    //Date : 29th March 2018
    //###############################################################
    public function Getcountry(Request $request)
    {
        $data = array();
        try {
            $data['country'] = Country::whereIn('others',array(2,3))->where('status',1)->orderBy('name')->get();
            $returnData      = UtilityController::Generateresponse(true, '', '', $data);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }
        return $returnData;
    }

    //###############################################################
    //Function Name : Getstatesforcountry
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get states of particular country
    //In Params : Void
    //Return : json
    //Date : 5th December 2017
    //###############################################################
    public function Getstatesforcountry(Request $request)
    {
        try {
            $input      = Input::all();
            $cities     = State::select('id', 'name')->where('country_id', $input['id'])->get();
            $returnData = UtilityController::Generateresponse(true, '', '', $cities);
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Getcitiesforcountry
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get cities of particular states
    //In Params : Void
    //Return : json
    //Date : 5th December 2017
    //###############################################################
    public function Getcitiesforstate(Request $request)
    {
        try {
            $input      = Input::all();
            $cities     = City::select('id', 'name')->where('state_id', $input['id'])->get();
            $returnData = UtilityController::Generateresponse(true, '', '', $cities);
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Getschools
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get schools
    //In Params : Void
    //Return : json
    //Date : 4th April 2018
    //###############################################################
    public function Getschools(Request $request)
    {
        try {
            $input      = Input::all();
            $schools    = School::select('id', 'name')->where('status', 1)->get();
            $returnData = UtilityController::Generateresponse(true, '', '', $schools);
            return $returnData;
        } catch (Exception $ex) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Getlanguages
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get languages
    //In Params : Void
    //Return : json
    //Date : 4th April 2018
    //###############################################################
    public function Getlanguages(Request $request)
    {
        try {
            $languages  = Language::get();
            $returnData = UtilityController::Generateresponse(true, '', '', $languages);
            return $returnData;
        } catch (Exception $ex) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Switchrole
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To switch user role if both buyer and seller
    //In Params : Void
    //Return : json
    //Date : 14th June 2018
    //###############################################################
    public function Switchrole(Request $request)
    {
        $Input = Input::all();
        try {
            if (Auth::check()) {
                \DB::beginTransaction();
                    if(Auth::user()->is_buyer==1 && Auth::user()->is_designer==1){    
                        $result = UtilityController::Makemodelobject($Input,'User','',Auth::user()->id);
                        $responseArray = UtilityController::Generateresponse(true, 'ROLE_SWITCHED', 1,$result);
                    } else {
                        if($Input['marketplace_current_role']==1)
                            $userMessage = 'ROLE_CANNOT_BE_SWITCHED_SELLER';
                        else
                            $userMessage = 'ROLE_CANNOT_BE_SWITCHED_BUYER';
                        $responseArray = UtilityController::Generateresponse(false, $userMessage, 0);
                    }
                \DB::commit();
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, $e->getMessage(), 0);
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Gettickettype
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To get the ticket type for customer support page
    //In Params : Void
    //Return : json
    //Date : 15th June 2018
    //###############################################################
    public function Gettickettype(Request $request)
    {
        try {
            $Input = Input::all();
            $productId = 0;
            if(isset($Input['productId']) && is_numeric($Input['productId']) && $Input['productId'] > 0){
                $productId = $Input['productId'];
            }            
            \DB::beginTransaction();
                $ticketTypes = TicketType::where('status',1)->where('custome_support_product_id',$productId)->get();
                $ticketCategory = TicketCategory::get();
                $tickets['ticketTypes'] = $ticketTypes;
                $tickets['ticketCategory'] = $ticketCategory;
                $tickets['userdata'] = Auth::user();
                $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $tickets);
            \DB::commit();
            
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, $e->getMessage(), 0);
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Createnewticket
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To create a new issue ticket by user
    //In Params : Void
    //Return : json
    //Date : 15th June 2018
    //###############################################################
    public function Createnewticket(Request $request)
    {
        try {            
        $Input = Input::all();
                \DB::beginTransaction();

                    if (isset($Input['ticket_attachments'])) {
                        $removeFiles = explode(',', $Input['filesPortfolioToRemove']);
                        if ((count($Input['ticket_attachments']) != count($removeFiles)) && $removeFiles['0']!='') {
                            foreach ($removeFiles as $key => $value) {
                                foreach ($Input['ticket_attachments'] as $keyInner => $valueInner) {
                                    if ($value == $valueInner->getClientOriginalName()) {
                                        unset($Input['ticket_attachments'][$keyInner]);
                                    }
                                }
                            }
                        }
                        if (count($Input['ticket_attachments']) > 5) {
                            throw new \Exception(UtilityController::Getmessage('FILE_LIMIT_EXCEED_NEW_PROJECT'));
                        } else {
                            $zip         = new ZipArchive;
                            $zipFileName = rand() . time();
                            $zip->open(public_path('').UtilityController::Getpath('CUSTOMER_SUPPORT_UPLOAD_PATH').$zipFileName, ZipArchive::CREATE);
                            foreach ($Input['ticket_attachments'] as $key => $value) {
                                $fileName  = rand() . time() . str_replace(' ', '_', $value->getClientOriginalName());
                                $extension = \File::extension($fileName);
                                if (!in_array($extension, array('zip', 'ZIP', 'tar', 'TAR', 'rar', 'RAR', 'js', 'JS', 'tar.gz', 'TAR.GZ'))) {
                                    $value->move(public_path('') . UtilityController::Getpath('CUSTOMER_SUPPORT_UPLOAD_PATH'), $fileName);
                                    chmod(public_path('') . UtilityController::Getpath('CUSTOMER_SUPPORT_UPLOAD_PATH') . $fileName, 0777);
                                    $zip->addFile(public_path('') . UtilityController::Getpath('CUSTOMER_SUPPORT_UPLOAD_PATH') . $fileName, $fileName);
                                    $fileList[] = $fileName;
                                } else {
                                    throw new \Exception(UtilityController::Getmessage('NO_COMPRESSED_ALLOWED'));
                                }
                            }
                            $zip->close();
                            chmod(public_path('') . UtilityController::Getpath('CUSTOMER_SUPPORT_UPLOAD_PATH') . $zipFileName, 0777);
                            $bucket   = UtilityController::Getmessage('BUCKET');
                            $object   = UtilityController::Getpath('BUCKET_CUSTOMER_SUPPORT').$zipFileName;
                            $filePath = public_path('').UtilityController::Getpath('CUSTOMER_SUPPORT_UPLOAD_PATH').$zipFileName;
                            if (filesize($filePath) <= UtilityController::Getmessage('FILE_SIZE_5')) {
                                $ossClient = new OssClient(UtilityController::Getmessage('ACCESS_KEY_ID'), UtilityController::Getmessage('ACCESS_KEY_SECRET'), UtilityController::Getmessage('END_POINT'));
                                foreach ($fileList as $key => $value){
                                    unlink(public_path('').UtilityController::Getpath('CUSTOMER_SUPPORT_UPLOAD_PATH').$value);
                                }
                                $ossClient->uploadFile($bucket, $object, $filePath);
                                unlink(public_path('').UtilityController::Getpath('CUSTOMER_SUPPORT_UPLOAD_PATH').$zipFileName);
                                $data['ticket_attachments'] = $zipFileName;
                            } else {
                                throw new \Exception(UtilityController::Getmessage('FILE_SIZE_LARGE_5'));
                            }
                        }
                    }
                    if (Auth::check()) {
                        $data['user_id']            = Auth::user()->id;
                    }

                    $lastRecord=CustomerSupport::orderBy('created_at','desc')->first();
                    $str=$lastRecord['ticket_number']; 
                    if($str==''){
                        $num=sprintf("%04d", 1);
                        $char='A';
                        $domainPrefix = UtilityController::getMessage('CURRENT_DOMAIN')=='net'?'CSN':'CSC';
                        $str= $domainPrefix.date('y').date('m').$num.$char;
                    }else{
                        $num=substr( $str, -5,-1 );
                        $num=$num+1;
                        $num=sprintf("%04d", $num);
                        $char=substr( $str, -1 );
                        if($num>9999){
                            $num=sprintf("%04d", 1);
                            $char++;
                        }
                        $domainPrefix = UtilityController::getMessage('CURRENT_DOMAIN')=='net'?'CSN':'CSC';
                        $str= $domainPrefix.date('y').date('m').$num.$char;
                    }
                    
                    $data['ticket_number']=$str;
                    $data['user_name'] = $Input['user_name'];
                    $data['email_address'] = $Input['email_address'];
                    if(isset($Input['contact']) && $Input['contact'] != ''){
                        $data['phone'] = $Input['contact'];    
                    }
                    if(isset($Input['country_code']) && $Input['country_code'] != ''){
                        $data['phone_code'] = $Input['country_code'];
                    }

                    $data['ticket_type_id']     = $Input['ticket_type'];
                    if(array_key_exists('ticket_category', $Input))
                        $data['ticket_category_id'] = $Input['ticket_category'];
                    $data['ticket_subject']     = $Input['ticket_subject'];
                    $data['ticket_description'] = $Input['ticket_description'];
                    $data['status']= 2;
                    $data['ticket_status']= 1;
                    $data['product_id']= $Input['product_type'];
                    $data['time_zone']=$Input['timezone'];
                    $data['ticket_for']=$Input['ticket_for'];
                    //$data['ticket_number']='CS'.date('y').date('m').$num.$char;

                    $validator = UtilityController::ValidationRules($data, 'CustomerSupport');
                    if (!$validator['status']) {
                        $errorMessage  = explode('.', $validator['message']);
                        $responseArray = UtilityController::Generateresponse(false, $errorMessage['0'], 0);
                    } else {
                        $result = UtilityController::Makemodelobject($data,'CustomerSupport');
                        $result = CustomerSupport::where('id', $result['id'])->with('customer_ticket_type','customer_ticket_category')->first();

                        $data['slug'] = str_slug($data['ticket_subject'], '-');
                        $data['slug']      = $data['slug'] . ":" . base64_encode($result['id']);
                        
                        CustomerSupport::where('id', $result['id'])->update(array('slug' => $data['slug']));
                        $returnMessage=$Input['language']=='en'?'TICKET_SUCCESS':($Input['language']=='chi'?'TICKET_SUCCESS_CHINESE':'TICKET_SUCCESS_RU');
                        $responseArray = UtilityController::Generateresponse(true, $returnMessage, 1, $result);

                        /*$ticketStatusPending = (new MpAdminMarkTicketComplete($result['id']))->delay(Carbon::now()->addHours(24));
                        dispatch($ticketStatusPending);*/
                        if($result['ticket_for']==1)
                            $product = $Input['language']=='en'?'Ignite':($Input['language']=='chi'?'智宝':'IGNITE');
                        elseif ($result['ticket_for']==1)
                            $product = $Input['language']=='en'?'Sixteen':($Input['language']=='chi'?'十六':'Sixteen');
                        else
                            $product = $Input['language']=='en'?'Proof Reading':($Input['language']=='chi'?'Proof Reading':'Proof Reading');
                        //$data['subject'] = $Input['language']=='en'?'Thanks for Contacting Us (REF#'.$result['ticket_number'].')':'#'.$result['ticket_number'];

                         $data['subject'] = $Input['language']=='en'?'Thanks for Contacting Us (REF#'.$result['ticket_number'].')':($Input['language']=='chi'?'#'.$result['ticket_number']:'Спасибо, что связались с нами  (REF#'.$result['ticket_number'].')');
                        $data['content']=$Input['language']=='en'?'Hi <strong>'. $data['user_name'].',
                                </strong><br/>
                                <br/>
                                Thank you for contacting us. We\'ll be reaching out to assist you soon. Please take note of your Customer Support Ticket Number <strong>#'.$result['ticket_number'].'</strong> for future reference.<br><br>
                                <div style="font-family:Arial,Helvetica,sans-serif; font-size: 11px; color: #6C6C6C;">
                                <b>Email Address:</b> '.$result['email_address'].'
                                <br> 
                                <b>Phone Number:</b> +'.$result['phone_code']." ".$result['phone'].'
                                <br>
                                <b>Product:</b> '.$product.'
                                <br> 
                                <b>Ticket Type:</b> '.$result['customer_ticket_type']['ticket_type_name'].' 
                                <br> 
                                <b>Ticket Category:</b> '.$result['customer_ticket_category']['ticket_category_name'].'
                                <br> 
                                <b>Subject: </b>'.$result['ticket_subject'].'
                                <br> 
                                <b>Description: </b>'.$result['ticket_description'].'
                                </div>':($Input['language']=='chi'?'您好 <strong>'. $data['user_name'].'</strong> ,
                                <br/>
                                <br/>
                                感谢您与我们联系。 我们的客服员将尽快为您提供帮助<strong>#'.$result['ticket_number'].'</strong>共以后参考。<br><br>
                                <div style="font-family:Arial,Helvetica,sans-serif; font-size: 11px; color: #6C6C6C;">
                                <b>电邮:</b> '.$result['email_address'].'
                                <br> 
                                <b>手机号:</b> +'.$result['phone_code']." ".$result['phone'].'
                                <br>
                                <b>订阅系列:</b> '.$product.'
                                <br>
                                <b>客服种类:</b> '.$result['customer_ticket_type']['ticket_type_name_chi'].' 
                                <br> 
                                <b>客服类别:</b> '.$result['customer_ticket_category']['ticket_category_name_chi'].'
                                <br> 
                                <b>主题: </b>'.$result['ticket_subject'].'
                                <br> 
                                <b>描述问题: </b>'.$result['ticket_description'].'     
                                </div>':'Привет <strong>'. $data['user_name'].',
                                </strong><br/>
                                <br/>
                                Благодарим Вас за обращение к нам. Мы скоро поможем вам. Пожалуйста, обратите внимание на номер своей карточки поддержки клиентов #'.$result['ticket_number'].'</strong> для дальнейшего использования.<br><br>
                                <div style="font-family:Arial,Helvetica,sans-serif; font-size: 11px; color: #6C6C6C;">
                                <b>Адрес электронной почты:</b> '.$result['email_address'].'
                                <br>
                                <b>Номер телефона:</b> +'.$result['phone_code']." ".$result['phone'].'
                                <br>
                                <b>Продукт:</b> '.$product.'
                                <br> 
                                <b>Тип билета:</b> '.$result['customer_ticket_type']['ticket_type_name_ru'].' 
                                <br> 
                                <b>Категория билета:</b> '.$result['customer_ticket_category']['ticket_category_name_ru'].'
                                <br> 
                                <b>Тема: </b>'.$result['ticket_subject'].'
                                <br> 
                                <b>Описание: </b>'.$result['ticket_description'].'
                                </div>');

                        $data['footer_content']=$Input['language']=='en'?'From,<br /> SixClouds Customer Support':($Input['language']=='chi'?'六云客服中心':'SixClouds Поддержка клиентов');

                        $data['footer']=$Input['language']=='en'?'SixClouds':($Input['language']=='chi'?'六云':'SixClouds');

                        Mail::send('emails.email_template', $data, function ($message) use ($data) {
                            $message->from('support@sixclouds.net', 'SixClouds');
                            $message->to([$data['email_address'],'support@sixclouds.net'])->subject($data['subject']);
                            // $message->to([$data['email_address'],'senil@creolestudios.com'])->subject($data['subject']);

                        });

                    }
                \DB::commit();
            
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, $e->getMessage(), 0);
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name: GetProducttype
    //Author:        Ketan Solanki <ketan@creolestudios.com>
    //Purpose:       To get the Product type for customer support page
    //In Params:     Void
    //Return:        json
    //Date:          8th August 2018
    //###############################################################
    public function GetProducttype(Request $request)
    {
        $Input = Input::all();
        try {            
            \DB::beginTransaction();
                $productTypes = ProductType::where('status',1)->get()->toArray();                
                $products['productTypes'] = $productTypes;                
                $products['userdata'] = Auth::user();
                $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $products);
            \DB::commit();            
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, $e->getMessage(), 0);
        }
        return response()->json($responseArray);
    }
    //###############################################################
    //Function Name: Getticketcategoriesforcustomersupport
    //Author:        Ketan Solanki <ketan@creolestudios.com>
    //Purpose:       To get the Product type for customer support page
    //In Params:     Void
    //Return:        json
    //Date:          8th August 2018
    //###############################################################
    public function Getticketcategoriesforcustomersupport(Request $request)
    {        
        try {            
            $Input = Input::all();        
            $ticketId = 0;
            if(isset($Input['ticketId']) && is_numeric($Input['ticketId']) && $Input['ticketId'] > 0){
                $ticketId = $Input['ticketId'];
            }
            \DB::beginTransaction();
                $ticketCategories = TicketCategory::where('ticket_type_id',$ticketId)->get()->toArray();
                dd($ticketCategories);                
                $categories['ticketCategories'] = $ticketCategories;                
                $categories['userdata'] = Auth::user();
                $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $categories);
            \DB::commit();            
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, $e->getMessage(), 0);
        }
        return response()->json($responseArray);
    }    
}


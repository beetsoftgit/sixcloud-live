<?php

/**
 * Short description for file
 *
 * PHP version 5 and 7
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @package    ELTController
 * @author     Komal Kapadi (komal@creolestudios.com)
 * @copyright  2017 Sixcloud Group
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    SVN Branch: backend_api_controller
 * @since      File available since Release 1.0.0
 * @deprecated N/A
 */
/*
 * Clients section related controller
 */

/**
Pre-Load all necessary library & name space
 */

namespace Trending\Http\Controllers\File;

//===================================

namespace App\Http\Controllers;

use App\Http\Controllers\UtilityController;
use App\VideoCategory;
use App\VideoNotes;
use App\Videos;
use Auth;
use Illuminate\Http\Request;
use OSS\OssClient;

class ELTController extends Controller
{

    //###############################################################
    //Function Name : Geteltsections
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get ELT sections with videos
    //In Params : Void
    //Return : json
    //Date : 11th December 2017
    //###############################################################
    public function Geteltsections(Request $request)
    {
        try {
            $result = VideoCategory::has('videos')->with([
                'videos' => function ($query) {
                    $query->select('*')->where('status', 1)->orderby('video_ordering', 'asc');
                },
            ])->where('status', 1)->get();
            if ($result) {
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $result);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 400, '');
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : GetVideodata
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get ELT sections with videos
    //In Params : Void
    //Return : json
    //Date : 11th December 2017
    //###############################################################
    public function GetVideodata(Request $request)
    {
        try {
            $result = Videos::with([
                'notes'    => function ($query) {
                    if (Auth::check()) {
                        $query->where('user_id', Auth::user()->id)->where('status', 1)->get();
                    }

                },
                'category' => function ($query) {
                    $query->get();
                },
            ])->where('slug', request('video_id'))->first();
            // && $result['video_status'] == 2

            if ($result) {
                $ossClient           = new OssClient(UtilityController::Getmessage('ACCESS_KEY_ID'), UtilityController::Getmessage('ACCESS_KEY_SECRET'), UtilityController::Getmessage('END_POINT'));
                $result['file_name'] = UtilityController::getSignedUrlForGettingObject($ossClient, UtilityController::Getmessage('BUCKET'), UtilityController::Getpath('BUCKET_ELT_FOLDER') . $result['file_name']);
                // $result['file_name'] = UtilityController::ELTVideoExists($result['file_name'], Config('constants.path.ELT_VIDEO_PATH'), Config('constants.path.ELT_VIDEO_URL'));
                $nextVideos          = Videos::where('video_category_id', $result['video_category_id'])->where('video_ordering', $result['video_ordering'] + 1)->orderby('video_ordering', 'asc')->take(3);
                $result['nextVideo'] = $nextVideos->get();
                if (!empty($result['nextVideo'])) {
                    foreach ($result['nextVideo'] as $key => $value) {
                        $result['nextVideo'][$key]['file_name'] = UtilityController::getSignedUrlForGettingObject($ossClient, UtilityController::Getmessage('BUCKET'), UtilityController::Getpath('BUCKET_ELT_FOLDER') . $value['file_name']);
                    }
                }
                $result['preVideo'] = Videos::where('video_category_id', $result['video_category_id'])->where('video_ordering', $result['video_ordering'] - 1)->take(5 - $nextVideos->count())->orderby('video_ordering', 'asc')->get();
                if (!empty($result['preVideo'])) {
                    foreach ($result['preVideo'] as $key => $value) {
                        $result['preVideo'][$key]['file_name'] = UtilityController::getSignedUrlForGettingObject($ossClient, UtilityController::Getmessage('BUCKET'), UtilityController::Getpath('BUCKET_ELT_FOLDER') . $value['file_name']);
                    }
                }
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $result);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 400, '');
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Addnote
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To add new note
    //In Params : Void
    //Return : json
    //Date : 13th December 2017
    //###############################################################
    public function Addnote(Request $request)
    {
        try {
            $validator            = UtilityController::ValidationRules($request->all(), 'VideoNotes');
            $returnData           = $validator;
            $notesData            = UtilityController::MakeObjectFromArray($request->all(), 'VideoNotes')->toArray();
            $notesData['user_id'] = Auth::user()->id;
            $videoNotesResult     = UtilityController::Makemodelobject($notesData, 'VideoNotes', 'id');
            if ($videoNotesResult) {
                $returnData = UtilityController::Generateresponse(true, 'NOTE_ADDED', 200, $videoNotesResult);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 400, '');
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Deletenote
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To delete note
    //In Params : Void
    //Return : json
    //Date : 14th December 2017
    //###############################################################
    public function Deletenote(Request $request)
    {
        try {
            $User_id          = Auth::user()->id;
            $videoNotesResult = VideoNotes::where('id', $request->note_id)->update(array('status' => 2));
            if ($videoNotesResult) {
                $returnData = UtilityController::Generateresponse(true, 'NOTE_DELETED', 200, $videoNotesResult);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 400, '');
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\AdminNotifications;
use App\AdminTimeline;
use App\Jobs\MpMessageBoardMail;
use App\MarketPlaceProject;
use App\MarketPlaceProjectsDetailInfo;
use App\MarketplaceSellerPayments;
use App\MarketPlaceSellerServicePackage;
use App\MarketPlaceCart;
use App\SkillSet;
use App\User;
use App\MarketPlaceMessageBoard;
use App\MarketPlaceDesignerSuggestionProject;
use App\MarketPlaceInsBankCategory;
use App\ProjectSkill;
use App\MarketPlaceUpload;
use App\MarketPlaceSellerServiceSkill;
use App\CustomerSupport;
use App\TicketType;
use App\TicketCategory;
use App\Country;
use App\MarketPlaceWallet;
use App\MarketplaceSellerOutsatndingPaymentStatus;
use Image;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use OSS\OssClient;

class AdminMarketPlaceController extends Controller
{

    //###############################################################
    //Function Name : Getallcategories
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : To get all categories
    //In Params : Void
    //Return : json
    //Date : 24th April 2018
    //###############################################################
    public function Getallcategories()
    {
        try
        {
            \DB::beginTransaction();

            $allCategories['categories'] = SkillSet::all();
            $allCategories['dispute_count'] = MarketPlaceProject::where('project_status', 9)->count();

            if (!empty($allCategories)) {

                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $allCategories);
            } else {

                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }

            \DB::commit();

            return $returnData;
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Getalljobsdata
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : To get all jobs data filter by status
    //In Params : Void
    //Return : json
    //Date : 24th April 2018
    //###############################################################
    public function Getalljobsdata(Request $request)
    {
        $input = Input::all();

        $searchString = (!empty($input['search']) ? $input['search'] : '');
        $category = (!empty($input['category']) ? $input['category'] : 'all');
        $status = (!empty($input['status']) ? $input['status'] : array(2));
        $status = (array) $status;

        try
        {
            \DB::beginTransaction();
            if (in_array(1, $status)) {
                $totalCompleteProjects = MarketPlaceProject::where('project_status', 3)->count();

                $totalCancelledProjects = MarketPlaceProject::whereIn('project_status', array(5, 6, 8, 10))->count();

                $totalCompleteProjectsThisMonth = MarketPlaceProject::where('project_status', 3)->whereMonth('created_at', Carbon::now()->month)->count();
            }
            /*  Get total data for all tabs(open,active,dispute etc) */
            $allOpenJobs = MarketPlaceProject::where('project_status', 2)
                ->count();

            $allActiveJobs = MarketPlaceProject::where('project_status', 1)
                ->with([
                    'adminProjectDesigner' => function ($query) {
                        $query->where('status', 1)->get();
                    },
                ])
                ->count();

            $allCompletedJobs = MarketPlaceProject::where('project_status', 3)
                ->count();

            $allCancelledJobs = MarketPlaceProject::whereIn('project_status', array(5, 6, 8, 10))
                ->count();

            $allEscrowJobs = MarketPlaceProject::where('project_status', 7)
                ->count();

            /*  Get all data  */
            $allJobsDataQuery = MarketPlaceProject::select('*', DB::raw("project_timeline - datediff(UTC_TIMESTAMP(),created_at) as days"))

                ->whereIn('project_status', $status)

                ->whereHas('adminprojectcreator', function ($query) use ($searchString) {
                    if (!empty($searchString)):
                        $query->where('users.first_name', 'like', '%' . $searchString . '%')
                            ->orWhere('last_name', 'like', '%' . $searchString . '%')
                            ->orWhere('project_title', 'like', '%' . $searchString . '%');
                    endif;
                })

                ->whereHas('adminskills', function ($query) use ($category) {
                    if (!empty($category) && $category != 'all'):
                        $query->where('project_skills.skill_set_id', $category);

                    endif;
                })
                ->with([
                    'projectCreator' => function ($query) {
                        $query->get();
                    },
                    'adminProjectDesigner' => function ($query) {
                        $query->get();
                    },
                    'skills' => function ($query) {
                        $query->with('skillDetail')->get();
                    },
                    'designer_suggestions' => function ($query) {
                        $query->groupby('user_id')->get();
                    },
                ]);

            if (in_array(1, $status)) {
                $allJobsDataQuery->with([
                    'adminProjectDesigner' => function ($query) {
                        $query->where('status', 1)->get();
                    },
                ]);
            }if (in_array(9, $status)) {
                $allJobsDataQuery->with([
                    'reportdispute' => function ($query) {
                        $query->get();
                    },
                ]);
            }

            if (isset($input['sort'])) {

                $allJobsDataQuery->orderby('created_at', $input['sort']);
            }
            if (isset($input['price'])) {
                $allJobsDataQuery->orderby('project_budget', $input['price']);
            }
            if (isset($input['timeleft'])) {
                $allJobsDataQuery->orderby('days', $input['timeleft']);
            }

            $allJobsData = $allJobsDataQuery
                ->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))
                ->toArray();
            if (!empty($allJobsData)) {

                foreach ($allJobsData['data'] as $key => $value) {
                    $allJobsData['data'][$key]['project_creator_name'] = $allJobsData['data'][$key]['project_creator']['first_name'] . " " . $allJobsData['data'][$key]['project_creator']['last_name'];

                    $allJobsData['data'][$key]['project_created_at'] = UtilityController::Changedateformat($value
                        ['created_at'], 'd F, Y H:i A');

                    $projectAt = Carbon::parse($value['created_at']);
                    $diffDays = $projectAt->diffInDays(Carbon::now());
                    $allJobsData['data'][$key]['days_remaining'] = $value['project_timeline'] - $diffDays;

                    if ($allJobsData['data'][$key]['days_remaining'] < 0) {
                        $allJobsData['data'][$key]['days_remaining'] = 0;
                    }
                    if($value['project_status']==9){
                        $projectAt                                     = Carbon::parse($value['project_cancel_request_at']);
                        $diffDays                                      = $projectAt->diffInDays(Carbon::now());
                        $allJobsData['data'][$key]['days_ago']   = $diffDays .' day(s) ago';  
                        if($diffDays<1){
                            $cancelled = \Carbon\Carbon::createFromTimeStamp(strtotime($value['project_cancel_request_at']));
                            $allJobsData['data'][$key]['days_ago']=$cancelled ->diff(\Carbon\Carbon::now())->format('%h hours %i minutes ago');
                        }
                    }

                    if (in_array(1, $status)) {
                        $allJobsData['data'][$key]['assigned_designer_name'] = $allJobsData['data'][$key]['admin_project_designer']['designer_details']['first_name'] . " " . $allJobsData['data'][$key]['admin_project_designer']['designer_details']['last_name'];
                    }
                    if (!empty($value['designer_suggestions'])) {
                        $allJobsData['data'][$key]['suggestions'] = count($value['designer_suggestions']);
                    } else {
                        $allJobsData['data'][$key]['suggestions'] = 0;
                    }

                    $skillTemp = $value;

                    if (!empty($skillTemp['skills'])) {
                        foreach ($skillTemp['skills'] as $keyInner => $valueInner) {
                            $tempSkills = $valueInner['skill_detail'];
                            $allJobsData['data'][$key]['project_category'] = $tempSkills['skill_name'];
                        }
                        $skill_count = count($allJobsData['data'][$key]['skills']);
                        $allJobsData['data'][$key]['skill_count'] = $skill_count - 1;
                    } else {
                        $allJobsData['data'][$key]['skill_count'] = 0;
                    }
                }

                if (in_array(1, $status)) {
                    $allJobsData['total_completed_project'] = $totalCompleteProjects;
                    $allJobsData['total_cancelled_project'] = $totalCancelledProjects;
                    $allJobsData['total_completed_project_month'] = $totalCompleteProjectsThisMonth;
                }

                $allJobsData['total_open_jobs'] = $allOpenJobs;
                $allJobsData['total_active_jobs'] = $allActiveJobs;
                $allJobsData['total_completed_jobs'] = $allCompletedJobs;
                $allJobsData['total_cancelled_jobs'] = $allCancelledJobs;
                $allJobsData['total_escrow_jobs'] = $allEscrowJobs;

                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $allJobsData);
            } else {

                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }

            \DB::commit();

            return $returnData;
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }

    }

    //###############################################################
    //Function Name : Getalldesignerdata
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : To get all designer data
    //In Params : Void
    //Return : json
    //Date : 27th April 2018
    //###############################################################

    public function Getalldesignerdata(Request $request)
    {
        $input = Input::all();
        
        $sum          = 0;
        $searchString = (isset($input['search']) ? $input['search'] : '');
        $category = (isset($input['category']) && $input['category'] != '' ? $input['category'] : 'all');
        //$rate         = (isset($input['rate'][0]) && $input['rate'][0] != 0 ? $input['rate'][0] : 0);

        $availabilityLongTerm = (isset($input['availability_long_term']) && $input['availability_long_term'] != '' ? $input['availability_long_term'] : 0);

        try
        {
            \DB::beginTransaction();

            $allDesignerDataQuery = User::

                where('is_designer', 1)
                ->with([
                    'city' => function ($query) {
                        $query->get();
                    },
                    'country' => function ($query) {
                        $query->get();
                    },
                    'skills' => function ($query) {
                        $query->groupby('skill_set_id')->get();
                    },
                    'ratings' => function ($query) {
                        $query->get();
                    },
                    'rating' => function ($query) {
                        $query->get();
                    },
                ]);

            $allDesignerDataQuery->withCount([
                'payment AS total_sum' => function ($query) {
                    $query->select(DB::raw("SUM(total_amount) as totalsum"))->whereIn('payment_status', array(2, 4, 5));
                },
            ]);
            if (isset($searchString) && $searchString != ''):
                $allDesignerDataQuery->whereHas('adminmarketplaceprojectdetailinfo', function ($query) use ($searchString) {
                    $query->where('first_name', 'like', '%' . $searchString . '%')
                        ->orWhere('last_name', 'like', '%' . $searchString . '%')
                        ->orWhere('email_address', 'like', '%' . $searchString . '%');
                });
            endif;

            if (!empty($category) && $category != 'all'):
                $allDesignerDataQuery->whereHas('adminskills', function ($query) use ($category) {
                    $query->where('designer_skills.skill_set_id', $category);
                });
            endif;

            if (!empty($input['proj_availability'])):
                $allDesignerDataQuery->whereIn('mp_designer_proj_availability', $input['proj_availability']);
            endif;

            if (!empty($availabilityLongTerm) && $availabilityLongTerm == 1):
                $allDesignerDataQuery->where('longterm_availability', 1);
            endif;

            if (!empty($input['budget'])):

                $minBudget = $input['budget'][0];
                $maxBudget = $input['budget'][1];

                $allDesignerDataQuery->where('mp_designer_min_price', '>=', $minBudget)
                    ->where('mp_designer_max_price', '<=', $maxBudget);
            endif;
            
            $allDesignerDataQuery->withCount(['reviews', 'adminmarketplaceprojectdetailinfo', 'adminmarketplaceprojectdetailinfowithstatus','admin_completed_projects']);

            if (isset($input['rate']) && $input['rate'] != 0) {
                $from = $input['rate'] - 0.9;
                $to = $input['rate'];
                $allDesignerDataQuery->where('total_avg_rate', '>=', $from)
                    ->where('total_avg_rate', '<=', $to);
            }

            if (isset($input['sort'])) {
                $allDesignerDataQuery->orderby('created_at', $input['sort']);
            }

            if (isset($input['name'])):
                $allDesignerDataQuery->orderby('first_name', $input['name']);
            endif;

            if (isset($input['review'])):
                $allDesignerDataQuery->orderby('reviews_count', $input['review']);
            endif;

            if (isset($input['complete_project'])):
                $allDesignerDataQuery->orderby('admin_completed_projects_count', $input['complete_project']);
            endif;

            if (isset($input['earnings'])):
                $allDesignerDataQuery->orderby('total_sum_count', $input['earnings']);
            endif;

            $allDesignerData = $allDesignerDataQuery
                ->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))
                ->toArray();

            if (!empty($allDesignerData['data'])) {
                foreach ($allDesignerData['data'] as $key => $value) {

                    if ($value['total_sum_count'] == null) {
                        $allDesignerData['data'][$key]['total_sum_count'] = 0;
                    }

                    $allDesignerData['data'][$key]['designer_created_at'] = UtilityController::Changedateformat($value
                        ['created_at'], 'd F, Y H:i A');

                    $skillTemp = $value;

                    if (!empty($skillTemp['skills'])) {
                        foreach ($skillTemp['skills'] as $keyInner => $valueInner) {

                            $tempSkills = $valueInner['skill_details'];
                            $allDesignerData['data'][$key]['designer_skill'] = $tempSkills['skill_name'];
                        }
                        $skill_count = count($allDesignerData['data'][$key]['skills']);
                        $allDesignerData['data'][$key]['skill_count'] = $skill_count - 1;
                    } else {
                        $allDesignerData['data'][$key]['skill_count'] = 0;
                    }
                }

                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $allDesignerData);
            } else {

                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }

            \DB::commit();

            return $returnData;
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Getallbuyerdata
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : To get all buyer data
    //In Params : Void
    //Return : json
    //Date : 2nd May 2018
    //###############################################################
    public function Getallbuyerdata(Request $request)
    {
        $input = Input::all();

        $searchString = ($input['search'] != '' ? $input['search'] : '');
        $sort = ($input['sort'] != '' ? $input['sort'] : 'asc');

        try
        {
            \DB::beginTransaction();
            $allBuyerDataQuery = User::where('is_buyer', 1)
                ->with('city', 'country', 'userpayments')
                ->withCount([
                    'marketplaceproject' => function ($query) {
                        $query->where('project_status', 3);
                    },
                ]);

            $allBuyerDataQuery->withCount([
                'project_creator_payment AS total_sum' => function ($query) {
                    // $query->select(DB::raw("SUM(alipay_total_amount_paid) as totalsum"))->where('paid_for',1);
                    $query->select(DB::raw("SUM(payment_amount) as totalsum"))->where('paid_for',1);                    
                },
            ]);

            $allBuyerDataQuery->withCount('adminprojects');
            // ->with([
            //     'adminprojects' => function ($query) {
            //         return $query->get();
            //     },
            // ]);
            if (isset($searchString) && $searchString != ''):
                $allBuyerDataQuery->whereHas('adminmarketplaceprojectdetailinfo', function ($query) use ($searchString) {
                    $query->where('first_name', 'like', '%' . $searchString . '%')
                        ->orWhere('last_name', 'like', '%' . $searchString . '%')
                        ->orWhere('email_address', 'like', '%' . $searchString . '%');
                });
            endif;
            if (isset($input['spendings'])):
                $allBuyerDataQuery->orderby('total_sum_count', $input['spendings']);
            endif;
            //Code added by Ketan Solanki for adding Sorting Filters DATE: 27th June 2018
            if (isset($sort)) {
                if ($sort == 'asc' || $sort == 'desc') {
                    $allBuyerDataQuery->orderby('created_at', $sort);
                } elseif ($sort == 'nameasc') {
                    $allBuyerDataQuery->orderby('first_name', 'ASC');
                } elseif ($sort == 'namedesc') {
                    $allBuyerDataQuery->orderby('first_name', 'DESC');
                } elseif ($sort == 'projectasc') {
                    $allBuyerDataQuery->orderby('adminprojects_count', 'ASC');
                } elseif ($sort == 'projectdesc') {
                    $allBuyerDataQuery->orderby('adminprojects_count', 'DESC');
                }
            }


            $allBuyerData = $allBuyerDataQuery
                ->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))
                ->toArray();

            if (!empty($allBuyerData['data'])) {
                /*foreach ($allBuyerData['data'] as $key => $value) {
                    $allBuyerData['data'][$key]['totalSpending'] = 0;
                    if (!empty($value['userpayments'])) {
                        $allBuyerData['data'][$key]['totalSpending'] = $value['userpayments'][0]['total_amount_spend'];
                    }
                }*/
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $allBuyerData);
            } else {

                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }

            \DB::commit();

            return $returnData;
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }

    }

    //###############################################################
    //Function Name : Getadmincategories
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get all categories
    //In Params : Void
    //Return : json
    //Date : 24th May 2018
    //###############################################################
    public function Getadmincategories()
    {
        try
        {
            \DB::beginTransaction();
            $input=Input::all();
            $allCategories = SkillSet::withCount('adminskills')->where('status', $input['status'])
                ->with([
                    'skillsbudget'=>function($query){
                        $query->get();
                    }
                ])
                ->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))->toArray();
            if(!empty($allCategories['data'])){
                foreach ($allCategories['data'] as $key => $value) {

                    $allCategories['data'][$key]['average_budget']=0.00;
                    if(!empty($value['skillsbudget'])){

                        $averageSkillBudget=[];

                        foreach ($value['skillsbudget'] as $keyInner => $valueInner) {
                           
                            $averageSkillBudget[]= $valueInner['projectskills']['project_budget'];
                        }
                        $allCategories['data'][$key]['average_budget']=array_sum($averageSkillBudget)/count($allCategories['data'][$key]['skillsbudget']);

                    }
                 
                }
            }

            if (!empty($allCategories)) {
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $allCategories);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            \DB::commit();
            return $returnData;
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Addeditcategorydata
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : To add new skill and edit existing skill in skill sets table
    //In Params : Void
    //Return : json
    //Date : 27th june 2018
    //###############################################################
    public function Addeditcategorydata(Request $request){
        try
        {
            \DB::beginTransaction();

            $input =Input::all();

            if($input['category_name_en']=='' || $input['category_name_chi']==''){
                if($input['category_name_en']==''){
                    $returnData        = UtilityController::Generateresponse(false, 'ENTER_SKILL_NAME', '', '');      
                }
                if($input['category_name_chi']==''){
                    $returnData        = UtilityController::Generateresponse(false, 'ENTER_SKILL_NAME', '', '');
                }
            }else{
                $data['skill_name']=$input['category_name_en'];
                $data['skill_name_chinese']=$input['category_name_chi'];
                if($input['status']==1){
                    $result = UtilityController::Makemodelobject($data, 'SkillSet');
                    if($result){
                        $returnData        = UtilityController::Generateresponse(true, 'CATEGORY_ADDED', '', '');  
                    }
                    else{
                        $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
                    }        
                }
                if($input['status']==2){

                    $result=SkillSet::where('id', $input['skill_id'])->update(array('skill_name' => $input['category_name_en'],'skill_name_chinese' => $input['category_name_chi']));
                    if($result){
                        $returnData        = UtilityController::Generateresponse(true, 'CATEGORY_UPDATED', '', '');  
                    }
                    else{
                        $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
                    }  
                } 
            }
            
            \DB::commit();
            return $returnData;
            
            
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
    //###############################################################
    //Function Name : Getactiveprojectsbyskill
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : To get total active projects in skill
    //In Params : Void
    //Return : json
    //Date : 27th june 2018
    //###############################################################
    public function Getactiveprojectsbyskill(Request $request){
        try
        {
            $input=Input::all();
            $totalProjects=ProjectSkill::where('skill_set_id',$input['skill_id'])
            ->with([
                'projectskills'=>function($query){
                    $query->get();
                }
            ])->get();

            $totalServices=MarketPlaceSellerServiceSkill::where('skill_id',$input['skill_id'])->where('status',1)->count();
            
            if(!empty($totalProjects)){
                $countActiveProjects=0;
                $countCompletedProjects=0;
                foreach ($totalProjects as $key => $value) {
                    if($value['projectskills']['project_status']==3 || $value['projectskills']['project_status']==5 ||  $value['projectskills']['project_status']==6 || $value['projectskills']['project_status']==10){
                        $countCompletedProjects++;
                    }
                    else{
                        $countActiveProjects++;
                    }
                }
                $totalProjects['completeProjects']=$countCompletedProjects;
                $totalProjects['activeProjects']=$countActiveProjects;
                $totalProjects['totalServices']=$totalServices;
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $totalProjects);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }

            return $returnData;
        } catch (Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Enabledeleteskill
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : To delete skill in skill sets table
    //In Params : Void
    //Return : json
    //Date : 27th june 2018
    //###############################################################
    public function Enabledeleteskill(Request $request){
        try
        {
            \DB::beginTransaction();

            $input =Input::all();
            $result=SkillSet::where('id', $input['id'])->update(array('status' => $input['new_status']));
                if($result){
                    if($input['new_status']==2){
                        $message='CATEGORY_DELETED';    
                    }
                    if($input['new_status']==1){
                        $message='CATEGORY_ENABLED';    
                    }
                    $returnData        = UtilityController::Generateresponse(true, $message, '', '');  
                }
                else{
                    $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
                }  
            \DB::commit();
            return $returnData;
            
            
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Getinspirationcategorydata
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : To get inspiration bank categories
    //In Params : Void
    //Return : json
    //Date : 27th june 2018
    //###############################################################
    public function Getinspirationcategorydata(Request $request){
        try
        {
            $input=Input::all();
            $inspirationCategories=MarketPlaceInsBankCategory::where('status',$input['status'])
            ->withCount('uploads')
            ->withCount([
                'uploads AS total_download' => function ($query) {
                    $query->select(DB::raw("SUM(inspiration_download_count) as totaldownload"));
                }
                ])
            ->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))->toArray();

            if (!empty($inspirationCategories['data'])) {

                foreach ($inspirationCategories['data'] as $key => $value) {
                    if($value['total_download_count']>0){
                        $inspirationCategories['data'][$key]['total_download_count']=$value['total_download_count'];    
                    }
                    else{
                        $inspirationCategories['data'][$key]['total_download_count']=0;
                    }
                    
                }
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $inspirationCategories);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }

            
            return $returnData;
        } catch (Exception $e) {
            
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    
    //###############################################################
    //Function Name : Addeditinscategorydata
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : To add and edit category for inspiration
    //In Params : Void
    //Return : json
    //Date : 27th june 2018
    //###############################################################
    public function Addeditinscategorydata(Request $request){
        try
        {
            \DB::beginTransaction();
            $input=Input::all();
            if($input['ins_category_name_en']==''){
                if($input['ins_category_name_en']==''){
                    $returnData        = UtilityController::Generateresponse(false, 'ENTER_CATEGORY_NAME', '', '');      
                }
            }else{
                $data['category_name_en']=$input['ins_category_name_en'];
                
                if($input['status']==1){
                    $result = UtilityController::Makemodelobject($data, 'MarketPlaceInsBankCategory');
                    if($result){
                        $returnData        = UtilityController::Generateresponse(true, 'INS_CATEGORY_ADDED', '', '');  
                    }
                    else{
                        $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
                    }        
                }
                if($input['status']==2){

                    $result=MarketPlaceInsBankCategory::where('id', $input['ins_cat_id'])->update(array('category_name_en' => $input['ins_category_name_en']));
                    if($result){
                        $returnData        = UtilityController::Generateresponse(true, 'INS_CATEGORY_UPDATED', '', '');  
                    }
                    else{
                        $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
                    }  
                } 
            }
            \DB::commit();
            return $returnData;
            } catch (Exception $e) {
                \DB::rollback();
                $sendExceptionMail = UtilityController::Sendexceptionmail($e);
                $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
                return $returnData;
        }
    }
    //###############################################################
    //Function Name : Getinspirationuploadsbycategory
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : To get no of uploads by category id
    //In Params : Void
    //Return : json
    //Date : 27th june 2018
    //###############################################################
    public function Getinspirationuploadsbycategory(Request $request){
        try
        {
            $input=Input::all();
            $totalInsUploads=MarketPlaceUpload::where('upload_of',1)->where('category_id',$input['cat_id'])->count();
            $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $totalInsUploads);

            return $returnData;
        } catch (Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Enabledeleteinscategory
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : To delete category from inspiration bank
    //In Params : Void
    //Return : json
    //Date : 2nd july 2018
    //###############################################################
    public function Enabledeleteinscategory(Request $request){
        try
        {
            \DB::beginTransaction();

            $input =Input::all();
         
            $result=MarketPlaceInsBankCategory::where('id', $input['id'])->update(array('status' => $input['new_status']));
                if($result){
                    if($input['new_status']==2){
                        $message='INS_CATEGORY_DELETED';    
                    }
                    if($input['new_status']==1){
                        $message='INS_CATEGORY_ENABLED';    
                    }
                    $returnData        = UtilityController::Generateresponse(true, $message, '', '');  
                }
                else{
                    $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
                }  
            \DB::commit();
            return $returnData;
            
            
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
    //###############################################################
    //Function Name : Getprojectdetails
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : To get project details
    //In Params : Void
    //Return : json
    //Date : 13th june 2018
    //###############################################################
    public function Getprojectdetails()
    {
        try
        {
            \DB::beginTransaction();
            $Input = Input::all();
            $mp_project_id = substr($Input['id'], strpos($Input['id'], ":") + 1);
            $mp_project_id = base64_decode($mp_project_id);

            $projectDetail = MarketPlaceProject::where('id', $mp_project_id)
                ->with([
                    'projectCreator' => function ($query) {
                        $query->get();
                    },
                    'skill_detail' => function ($query) {
                        $query->get();
                    },
                    'projectattachment' => function ($query) {
                        $query->get();
                    },
                    'project_detail_info_for_admin_detail_page' => function ($query) {
                        $query->orderBy('created_at', 'DESC')->get();
                    },
                    'rating_for_project' => function ($query) {
                        $query->orderBy('created_at', 'DESC')->get();
                    },
                    'review_for_project' => function ($query) {
                        $query->orderBy('created_at', 'DESC')->get();
                    },
                    'admin_seller_payment' => function ($query) {
                        $query->get();
                    },
                ])->first();

            $adminNotifications = AdminNotifications::where('mp_project_id', $mp_project_id)->where('mail_sent_status', 1)->count();

            if (!empty($projectDetail)) {

                $projectDetail['project_created_at'] = UtilityController::Changedateformat($projectDetail['created_at'], 'd F, Y  H:i A');

                if (!empty($projectDetail['admin_seller_payment'])) {
                    $projectDetail['seller_payment'] = 1;
                } else {
                    $projectDetail['seller_payment'] = 0;
                }

                if ($projectDetail['project_status'] == 3 && $projectDetail['project_detail_info_for_admin_detail_page'][0]['status'] == 7) {

                    if (!empty($projectDetail['rating_for_project']) && !empty($projectDetail['review_for_project'])) {

                        $projectDetail['no_rate_review'] = 0;

                        $projectDetail['review_on'] = UtilityController::Changedateformat($projectDetail['review_for_project']['created_at'], 'd F, Y  H:i A');

                        $projectDetail['review'] = $projectDetail['review_for_project']['review'];

                        $projectDetail['rating_matrix'] = $projectDetail['rating_for_project']['matrix_rating'];
                    } else {
                        if ($adminNotifications == 0) {
                            $projectDetail['no_rate_review'] = 1;
                        }
                    }
                }
               
                if (!empty($projectDetail['project_detail_info_for_admin_detail_page'])) {

                    if(isset($projectDetail['project_detail_info_for_admin_detail_page'][0])){
                        $projectDetail['designer'] = $projectDetail['project_detail_info_for_admin_detail_page'][0]['designer_details_admin']['first_name'] . ' ' . $projectDetail['project_detail_info_for_admin_detail_page'][0]['designer_details_admin']['last_name'];

                        $projectDetail['invitation_sent_on'] = UtilityController::Changedateformat($projectDetail['project_detail_info_for_admin_detail_page'][0]['created_at'], 'd F, Y  H:i A');
                    }
                }
                if (!empty($projectDetail['project_detail_info_for_admin_detail_page'])) {
                    foreach ($projectDetail['project_detail_info_for_admin_detail_page'] as $key => $value) {

                        $projectDetail['project_detail_info_for_admin_detail_page'][$key]['designer_details_admin']['image'] = UtilityController::Imageexist($value['designer_details_admin']['image'], public_path('') . UtilityController::Getmessage('USER_PROFILE_PATH'), url('') . UtilityController::Getmessage('USER_PROFILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));

                        if (count($projectDetail['project_detail_info_for_admin_detail_page'][$key]['designer_details_admin']['ratings']) > 0) {
                            $array_rating = [];

                            foreach ($projectDetail['project_detail_info_for_admin_detail_page'][$key]['designer_details_admin']['ratings'] as $keyInner => $valueInner) {
                                $array_rating[] = $valueInner['rating'];
                            }

                            $projectDetail['project_detail_info_for_admin_detail_page'][$key]['designer_details_admin']['avg_rate'] = array_sum($array_rating) / count($array_rating);
                        } else {
                            $projectDetail['project_detail_info_for_admin_detail_page'][$key]['designer_details_admin']['avg_rate'] = 0;
                        }

                        if (!empty($value['designer_details_admin']['services'])) {
                           
                            $service_images_array[0]['image_name'] =
                            url('') . UtilityController::Getmessage('NO_IMAGE_URL');
                            $j = 0;
                            foreach ($value['designer_details_admin']['services'] as $keyServices => $valueServices) {

                                if (!empty($valueServices['service_details'])) {
                                    foreach ($valueServices['service_details'] as $keyServicesDetail => $valueServiceDetail) {
                                      
                                        if(!empty($valueServices['service_details'][$keyServicesDetail]['service_images'])){
                                            foreach ($valueServices['service_details'][$keyServicesDetail]['service_images'] as $keyServicesImage => $valueServiceImage){

                                                $service_images_array[$j]['image_name'] = UtilityController::Imageexist($valueServiceDetail['service_images'][$keyServicesImage]['image_name'], public_path('') . UtilityController::Getpath('MP_SERVICES_UPLOAD_PATH'), url('') . UtilityController::Getpath('MP_SERVICES_UPLOAD_URL'), url('') . UtilityController::Getmessage('NO_IMAGE_URL'));
                                                 $j++;
                                            }
                                        }

                                    }
                                }

                            }
                            $projectDetail['project_detail_info_for_admin_detail_page'][$key]['designer_details_admin']['service_images_array'] = $service_images_array;
                        }

                    }
                }

                if (!empty($projectDetail['projectattachment'])) {
                    $projectDetail['project_attachment'] = 'http://sixclouds88-bucket.oss-cn-beijing.aliyuncs.com/MarketPlace_attachments/' . $projectDetail['projectattachment']['attachment_name'];
                } else {
                    $projectDetail['project_attachment'] = '';
                }
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $projectDetail);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            \DB::commit();
            return $returnData;
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Getprojectpaymentlogs
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : To get project payment logs
    //In Params : Void
    //Return : json
    //Date : 14th june 2018
    //###############################################################
    public function Getprojectpaymentlogs()
    {

        try
        {
            \DB::beginTransaction();
            $Input = Input::all();
            $mp_project_id = substr($Input['id'], strpos($Input['id'], ":") + 1);
            $mp_project_id = base64_decode($mp_project_id);

            $projectPaymentLogs = AdminTimeline::where('mp_project_id', $mp_project_id)
                ->with([
                    'user_detail' => function ($query) {
                        $query->get();
                    },
                    'project_detail_info' => function ($query) {
                        $query->get();
                    },
                ])->get();

            if (!empty($projectPaymentLogs)) {

                foreach ($projectPaymentLogs as $key => $value) {

                    $projectPaymentLogs[$key]['date_created_at'] = UtilityController::Changedateformat($value['created_at'], 'd F, Y  H:i A');
                }

                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $projectPaymentLogs);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            \DB::commit();
            return $returnData;
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Sendmailforratereview
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : To send mail to buyer to notify about rate and review for designer
    //In Params : Void
    //Return : json
    //Date : 15th june 2018
    //###############################################################
    public function Sendmailforratereview()
    {
        try
        {
            $Input = Input::all();

            $mp_project_id = substr($Input['mp_project_id'], strpos($Input['mp_project_id'], ":") + 1);
            $mp_project_id = base64_decode($mp_project_id);

            $userData = User::where('id', $Input['id'])->first();

            if (!empty($userData)) {

                $buyer_info = array(
                    "first_name" => $userData['first_name'],
                    "last_name" => $userData['last_name'],
                    "email_address"=>$userData['email_address'],
                );
                
                $current_language=$userData['current_language'];

                $buyer_info['subject']=$current_language==1?'SixClouds : Rate and review for seller':'六云 : 卖家的价格和评论';

                $buyer_info['content']=$current_language==1?'Hello <strong>'.$userData['first_name'].' '.$userData['last_name'].' ,
                    </strong><br/>
                    <br/>
                    Please give rating and review for seller based on his experience.<br><br>':'你好 <strong>'.$userData['first_name'].' '.$userData['last_name'].'</strong> ,
                    <br/>
                    <br/>
                    请根据他的经验给卖家评分和评论。<br> <br>';

                $buyer_info['footer_content']=$current_language==1?'From,<br /> SixClouds Sixteen':'六云十六 ';

                $buyer_info['footer']=$current_language==1?'SixClouds':'六云';

                Mail::send('emails.email_template', $buyer_info, function ($message) use ($buyer_info) {
                    $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                    $message->to($buyer_info['email_address'])->subject($buyer_info['subject']);

                });
                $data['mail_sent_status'] = 1;
                $data['mp_project_id'] = $mp_project_id;

                $result = UtilityController::Makemodelobject($data, 'AdminNotifications');

                $returnData = UtilityController::Generateresponse(true, 'MAIL_SENT_TO_BUYER', 1);
                return $returnData;
            }

        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }

    }

    //###############################################################
    //Function Name : Getmessagedata
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : To get message data
    //In Params : Void
    //Return : json
    //Date : 15th june 2018
    //###############################################################
    public function Getmessagedata()
    {
        try
        {
            $Input = Input::all();

            $mp_project_id = substr($Input['mp_project_id'], strpos($Input['mp_project_id'], ":") + 1);
            $mp_project_id = base64_decode($mp_project_id);

            $messageData = MarketPlaceProject::where('slug', $Input['mp_project_id'])
                ->with([
                    'message_board' => function ($qry) {
                        $qry->with(['message_board' => function ($q) {
                            $q->where('type', '!=', 1)->get();
                        }])->whereIn('type', array(1))->get();
                    },
                ])->first();

            foreach ($messageData['message_board'] as $key => $value) {
                // IF designer role is there then designer's image came else buyer's

                $value['date_created_at'] = UtilityController::Changedateformat($value['created_at'], 'd F, Y h:i A');
                if ($value['user_role'] == 3) {
                    $value['name'] = 'ADMIN';
                    $value['side_class']='center';
                } else {
                    $value['name'] = $value['user_details']['first_name'] . ' ' . $value['user_details']['last_name'];
                    if($value['user_role'] == 2){
                        $value['side_class']='left';
                    }
                    else{
                        $value['side_class']='right';
                    }
                }

                // If type 2 is there then mity image will be assigned
                if ($messageData['message_board'][$key]['message_board']) {
                    $innerMessageBoard = $messageData['message_board'][$key]['message_board'];
                    foreach ($innerMessageBoard as $keyInner => $valueInner) {
                        if ($valueInner['type'] == 2) {
                            $innerMessageBoard[$keyInner]['placeholder'] = UtilityController::Imageexist('tiny_' . $valueInner['message'], public_path('') . UtilityController::Getmessage('MSG_FILE_PATH'), url('') . UtilityController::Getmessage('MSG_FILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));
                            $innerMessageBoard[$keyInner]['download_placeholder'] = UtilityController::Imageexist($valueInner['message'], public_path('') . UtilityController::Getmessage('MSG_FILE_PATH'), url('') . UtilityController::Getmessage('MSG_FILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));

                            $innerMessageBoard[$keyInner]['message'] = public_path('') . UtilityController::Getmessage('MSG_FILE_PATH') . $valueInner['message'];
                        }

                        // If type 3 means non image file is there then desfault file pic will be assigned to return.
                        if ($valueInner['type'] == 3) {
                            $innerMessageBoard[$keyInner]['placeholder'] = url('/') . UtilityController::Getmessage('DEFAULT_FILE_URL');
                            $innerMessageBoard[$keyInner]['download_placeholder'] = UtilityController::Fileexist($valueInner['message'], public_path('') . UtilityController::Getmessage('MSG_FILE_PATH'), url('') . UtilityController::Getmessage('MSG_FILE_URL'));
                            $innerMessageBoard[$keyInner]['message'] = public_path('') . UtilityController::Getmessage('MSG_FILE_PATH') . $valueInner['message'];
                        }
                    }
                }
            }

            $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $messageData);
            return $returnData;
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }

    }
    //###############################################################
    //Function Name : Sendmessage
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : To get message data
    //In Params : Void
    //Return : json
    //Date : 15th june 2018
    //###############################################################
    public function Sendmessage()
    {
        try
        {
            $Input = Input::all();
            if(isset($Input['send_buyer']) && $Input['send_buyer']==1){
                $Input['send_buyer']=1;
            }else{
                $Input['send_buyer']=0;
            }
            if(isset($Input['send_seller']) && $Input['send_seller']==1){
                $Input['send_seller']=1;
            }else{
                $Input['send_seller']=0;
            }

            if($Input['send_buyer']==1 && $Input['send_seller']==1){
                $user_id= 0;
            }
            else if($Input['send_seller']==1 && $Input['send_buyer']==0){
                $user_id = $Input['seller_id'];       
            }
            else if($Input['send_seller']==0 && $Input['send_buyer']==1){
                $user_id = $Input['buyer_id'];       
            }
            else{
               
            }
            $Input['user_id']=$user_id;
            if ((array_key_exists('message', $Input) && $Input['message'] != '') || !empty($Input['files'])) {
                $result = UtilityController::Makemodelobject($Input, 'MarketPlaceMessageBoard');
                $Input['project_detail'] = MarketPlaceProject::where('id', $Input['mp_project_id'])->first();
                $Input['project_detail'] = json_decode(json_encode($Input['project_detail']), true);
                if (array_key_exists('message', $Input)) {
                    $Input['message_text'] = $Input['message'];
                }

                $Input['file_count'] = count($Input['files']);
                $firstMsgId = $result->id;
                $arrayUpdate['group_with'] = $firstMsgId;
                UtilityController::Makemodelobject($arrayUpdate, 'MarketPlaceMessageBoard', 'id', $firstMsgId);
                if (!empty($Input['files'])) {
                    foreach ($Input['files'] as $key => $value) {
                        $temp = array();
                        $temp['user_id'] = $user_id;
                        $temp['mp_project_id'] = $Input['mp_project_id'];
                        $temp['type'] = $value['type'];
                        $temp['user_role'] = $Input['user_role'];
                        $temp['message'] = $value['message'];
                        $temp['group_with'] = $firstMsgId;
                        $result = UtilityController::Makemodelobject($temp, 'MarketPlaceMessageBoard');
                    }
                }
                if ($result) {
                    for ($i = 0; $i < 2; $i++) {
                        $project_title = MarketPlaceProject::where('id', $Input['mp_project_id'])->first();
                        $notification['sender_id'] = 0;
                        if ($i == 0) {
                            $notification['receiver_id'] = $Input['seller_id'];
                        } else {
                            $notification['receiver_id'] = $Input['buyer_id'];
                        }

                        $notification['notification_text'] = 'New message in "' . $project_title['project_title'] . '"';
                        $notification['chi_notification_text'] = '新消息"' . $project_title['project_title'] . '"';
                        $notification['module'] = 1;
                        $sendNotification = UtilityController::Makemodelobject($notification, 'SystemNotification');

                        $user = User::findOrFail($Input['seller_id']);
                        $job = (new MpMessageBoardMail($user, $notification, $Input['mp_project_id']))->delay(1);
                        dispatch($job);

                        $data['mp_project_id'] = $Input['mp_project_id'];
                        $data['action_type'] = 4;
                        $data['action_text'] = UtilityController::Getmessage('NEW_MESSAGE_ARRIVED');
                        $data['show_first'] = 1;
                        $actionResult = UtilityController::Makemodelobject($data, 'MarketplaceAction');
                        $actionResult = UtilityController::Makemodelobject($data, 'MarketPlaceProject', '', $data['mp_project_id']);
                    }

                    \DB::commit();
                    $responseArray = UtilityController::Generateresponse(true, 'MESSAGE_SENT', 1);
                } else {
                    $responseArray = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
                }
            } else {
                throw new \Exception(UtilityController::Getmessage('SELECT_IMAGE_OR_TYPE_MESSAGE'));
            }

            $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1);
            return $returnData;
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
    //###############################################################
    //Function Name : Uploadmessagefileadmin
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : To upload message file
    //In Params : Void
    //Return : json
    //Date : 18th june 2018
    //###############################################################
    public function Uploadmessagefileadmin(Request $request)
    {
        try {
            \DB::beginTransaction();
            $Input = Input::all();
            if (!empty($Input['file'][0])) {
                if ($Input['file'][0]->getClientSize() > UtilityController::Getmessage('MAX_UPLOAD_FILE_SIZE')) {
                    throw new \Exception(UtilityController::Getmessage('FILE_UPTO_MB'));
                }
                $file = $Input['file'][0]->getMimeType();
                // create file unique name
                $orgfileName = rand() . time() . str_replace(' ', '_', $Input['file'][0]->getClientOriginalName());
                $extension = \File::extension($Input['file'][0]->getClientOriginalName());
                if (in_array($extension, array('zip', 'ZIP', 'tar', 'TAR', 'rar', 'RAR', 'js', 'JS', 'tar.gz', 'TAR.GZ', 'gz', 'GZ'))) {
                    throw new \Exception(UtilityController::Getmessage('NO_COMPRESSED_ALLOWED'));
                } else {
                    $tinyfileName = 'tiny_' . $orgfileName;
                    if (!empty($Input['path'])) {
                        $DestinationPath = config('constants.path.MP_ATTACHMENT_UPLOAD_PATH');
                    } else {
                        $DestinationPath = config('constants.messages.MSG_FILE_PATH');
                    }
                    $DestinationPath = public_path() . $DestinationPath;
                    if (strpos($file, 'image/') !== false) {
                        Image::make($Input['file'][0])->resize(300, 300, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        })->save($DestinationPath . $tinyfileName);
                        chmod($DestinationPath . $tinyfileName, 0777);
                        $Input['file'][0]->move($DestinationPath, $orgfileName);
                        chmod($DestinationPath . $orgfileName, 0777);
                        $result['type'] = 2;
                    } else {
                        $Input['file'][0]->move($DestinationPath, $orgfileName);
                        chmod($DestinationPath . $orgfileName, 0777);
                        $result['type'] = 3;
                    }
                    $result['message'] = $orgfileName;
                    $result['size'] = $Input['file'][0]->getClientSize();
                    $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $result);
                }
            } else {
                $returnData = UtilityController::Generateresponse(false, '', '', '');
            }

            return $returnData;
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Unlinkmsgbrdremovedfile
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : To unlink file which is cancelled by user in message board
    //In Params : Void
    //Return : json
    //Date : 18th June 2018
    //###############################################################
    public function Unlinkmsgbrdremovedfile(Request $request)
    {
        try {
            \DB::beginTransaction();
            $Input = Input::all();
            if (file_exists(public_path() . config('constants.messages.MSG_FILE_PATH') . $Input['file_name'])) {
                unlink(public_path() . config('constants.messages.MSG_FILE_PATH') . $Input['file_name']);
            }
            if (file_exists(public_path() . config('constants.messages.MSG_FILE_PATH') . 'tiny_' . $Input['file_name'])) {
                unlink(public_path() . config('constants.messages.MSG_FILE_PATH') . 'tiny_' . $Input['file_name']);
            }
            $returnData = UtilityController::Generateresponse(true, '', '', '');
            return $returnData;
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Infavorofbuyer
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : To refund buyer
    //In Params : Void
    //Return : json
    //Date : 18th June 2018
    //###############################################################
    public function Infavorofbuyer()
    {
        try {
            \DB::beginTransaction();
            $Input = Input::all();

            $mp_project_id = substr($Input['mp_project_id'], strpos($Input['mp_project_id'], ":") + 1);
            $mp_project_id = base64_decode($mp_project_id);

            $projectBudget=MarketPlaceProject::where('id',$mp_project_id)
            ->withCount([
                'infovor_buyer_seller_payment AS total_sum' => function ($query) {
                    $query->select(DB::raw("SUM(total_amount) as totalsum"))->where('payment_status', 2);
                },
            ])
            ->first();

            if(!empty($projectBudget)){

                
                $data['user_id']=$Input['buyer_id'];
                $data['wallet_amount']=$projectBudget['project_budget']-$projectBudget['total_sum_count'];

                $userExists=MarketPlaceWallet::where('user_id',$Input['buyer_id'])->count();
                if($userExists>0){
                    $result =MarketPlaceWallet::where('user_id', $Input['buyer_id'])->update(array('wallet_amount' => $data['wallet_amount']));
                }else{
                    $result = UtilityController::Makemodelobject($data, 'MarketPlaceWallet');    
                }
            }

            if($result){
                MarketplaceProject::where('id', $mp_project_id)->update(array('project_status' => 3));

                MarketPlaceProjectsDetailInfo::where('mp_project_id', $mp_project_id)->where('assigned_designer_id', $Input['seller_id'])->update(['status' => 7]);

                $returnData = UtilityController::Generateresponse(true, 'BUYER_WALLET', '', '');
            }else{
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');    
            }
            \DB::commit();
            return $returnData;
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Infavorofseller
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : To refund seller
    //In Params : Void
    //Return : json
    //Date : 18th June 2018
    //###############################################################
    public function Infavorofseller(Request $request)
    {
        try {
            \DB::beginTransaction();
            $Input = Input::all();
            $Input['user_id'] = Auth::guard('admins')->user()->id;
            $Input['total_amount'] = $Input['amount'];
            $Input['payment_status'] = 5;
            $Input['is_admin'] = 1;

            $projectData = MarketPlaceProject::where('id', $Input['mp_project_id'])->first();
            $alreadyRequested = MarketplaceSellerPayments::where('mp_project_id', $Input['mp_project_id'])->where('user_id', $Input['seller_id'])->where('payment_status', 2)->orWhere('payment_status', 4)->first();

            $array_total_withdraw = [];
            $result = 0;

            if (!empty($alreadyRequested) ) {

                $total_amount = $alreadyRequested['total_amount'];
                $total_amount = $total_amount + $Input['amount'];

                if ($total_amount > $projectData['project_budget']) {
                    //throw new \Exception(UtilityController::Getmessage('WITHDRAW_UPTO_BUDGET'));
                    return $returnData = UtilityController::Generateresponse(false, 'WITHDRAW_UPTO_BUDGET', 400, '');
                } else {
                    $result = UtilityController::Makemodelobject($Input, 'MarketplaceSellerPayments', '');
                }

            } else {

                if ($Input['amount'] < $projectData['project_budget']) {
                    $result = UtilityController::Makemodelobject($Input, 'MarketplaceSellerPayments', '');
                } else {
                    return $returnData = UtilityController::Generateresponse(false, 'WITHDRAW_UPTO_BUDGET', 400, '');
                    //throw new \Exception(UtilityController::Getmessage('WITHDRAW_UPTO_BUDGET'));
                }
            }
            if ($result) {
                $returnData = UtilityController::Generateresponse(true, 'WITHDRAW_SUBMITED', 200, '');

                $calculate_percentage = (100 * $Input['amount']) / $projectData['project_budget'];
                $calculate_percentage = sprintf('%0.2f', $calculate_percentage);

                $adminNotificationData['user_id'] = Auth::guard('admins')->user()->id;
                $adminNotificationData['mp_project_id'] = $projectData['id'];
                $adminNotificationData['message_text_en'] = 'Admin paid ' . $calculate_percentage . '% of amount to seller';
                $adminNotificationData['message_text_chi'] = '管理员付款 ' . $calculate_percentage . '% 的金额给卖家';
                $adminNotificationData['amount'] = $Input['amount'];
                $adminNotificationData['status'] = 1;
                $adminNotificationData['is_admin'] = 1;
                $adminNotificationData['created_at'] = Carbon::now();
                $adminNotificationData['updated_at'] = Carbon::now();

                $resultDetailInfo = UtilityController::Makemodelobject($adminNotificationData, 'AdminTimeline');

            } else {
                $returnData = UtilityController::Generateresponse(false, '', 400, '');
            }
            \DB::commit();
            return $returnData;

        } catch (Exception $e) {
            \DB::rollback();

            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '','');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name :   Cancelprojectbyadmin
    //Author :          Zalak Kapadia <zalak@creolestudios.com>
    //Purpose :         To cancel project by admin
    //In Params :       Void
    //Return :          json
    //Date :            15th june 2018
    //###############################################################
    public function Cancelprojectbyadmin()
    {
        try {
            \DB::beginTransaction();

            $Input = Input::all();
            MarketplaceProject::where('id', $Input['mp_project_id'])->update(array('project_status' => 10, 'project_cancelled_at' => Carbon::now()));

            $result = MarketPlaceProjectsDetailInfo::where('mp_project_id', $Input['mp_project_id'])->where('assigned_designer_id', $Input['designer_id'])->update(array('status' => 14));

            for ($i = 0; $i < 2; $i++) {
                $project_title = MarketPlaceProject::where('id', $Input['mp_project_id'])->first();
                $notification['sender_id'] = 0;
                if ($i == 0) {
                    $notification['receiver_id'] = $Input['designer_id'];
                } else {
                    $notification['receiver_id'] = $Input['buyer_id'];
                }

                $notification['notification_text'] = 'New message in "' . $project_title['project_title'] . '"';
                $notification['chi_notification_text'] = '新消息"' . $project_title['project_title'] . '"';
                $notification['module'] = 1;
                $sendNotification = UtilityController::Makemodelobject($notification, 'SystemNotification');

                $user = User::findOrFail($Input['designer_id']);
                $job = (new MpMessageBoardMail($user, $notification, $Input['mp_project_id']))->delay(1);
                dispatch($job);

                $data['mp_project_id'] = $Input['mp_project_id'];
                $data['action_type'] = 4;
                $data['action_text'] = UtilityController::Getmessage('NEW_MESSAGE_ARRIVED');
                $data['show_first'] = 1;
                $actionResult = UtilityController::Makemodelobject($data, 'MarketplaceAction');
                $actionResult = UtilityController::Makemodelobject($data, 'MarketPlaceProject', '', $data['mp_project_id']);
            }

            $adminNotificationData['user_id'] = Auth::guard('admins')->user()->id;
            $adminNotificationData['mp_project_id'] = $Input['mp_project_id'];
            $adminNotificationData['message_text_en'] = 'Project cancelled by Admin';
            $adminNotificationData['message_text_chi'] = '项目被管理员取消';
            $adminNotificationData['status'] = 7;
            $adminNotificationData['is_admin'] = 1;
            $adminNotificationData['created_at'] = Carbon::now();
            $adminNotificationData['updated_at'] = Carbon::now();

            $resultDetailInfo = UtilityController::Makemodelobject($adminNotificationData, 'AdminTimeline');

            $returnData = UtilityController::Generateresponse(true, 'PROJECT_CANCELLED', '', '');

            \DB::commit();
            return $returnData;
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
    //###############################################################
    //Function Name : Exportdata
    //Author        : Ketan Solanki <ketan@creolestudios.com>
    //Purpose       : To export the data in Excel or CSV Format
    //In Params     : Export Type( 1 => Excel, 2=> CSV)
    //Return        : Download the file
    //Date          : 27th June 2018
    //###############################################################
    public function Exportdata(Request $request)
    {
        try {
            $searchString = (!empty($request->search) ? $request->search : '');
            $sort = (!empty($request->sort) ? $request->sort : '');
            $price = (!empty($request->price) ? $request->price : '');
            $timeleft = (!empty($request->timeleft) ? $request->timeleft : '');
            $category = (!empty($request->category) ? $request->category : 'all');
            $exportValue = (!empty($request->Exportvalue) ? $request->Exportvalue : '');
            $status = (!empty($request->status) ? explode(",", $request->status) : '');
            if ($exportValue != '' && !empty($status)) {
                DB::enableQueryLog();
                DB::statement(DB::raw('set @row:=0'));
                $data = MarketPlaceProject::
                    select("*", DB::RAW("project_timeline - datediff(UTC_TIMESTAMP(),created_at) as days"))
                    ->selectRaw('@row:=@row+1 as sr_no')
                    ->whereIn("project_status", $status)
                    ->with('buyer', 'skills') //Add suggestions here
                    ->whereHas('adminprojectcreator', function ($query) use ($searchString) {
                        if (!empty($searchString) && $searchString != "no-search"):
                            $query->where('users.first_name', 'like', '%' . $searchString . '%')
                                ->orWhere('last_name', 'like', '%' . $searchString . '%')
                                ->orWhere('project_title', 'like', '%' . $searchString . '%');
                        endif;
                    })
                    ->whereHas('adminskills', function ($query) use ($category) {
                        if (!empty($category) && $category != 'all'):
                            $query->where('project_skills.skill_set_id', $category);
                        endif;
                    });
                if (isset($sort) && $sort != '' && $sort != "no-sort") {
                    $data = $data->orderby('created_at', $sort);
                }
                if (isset($price) && $price != '' && $price != 'no-price') {
                    $data = $data->orderby('project_budget', $price);
                }
                if (isset($timeleft) && $timeleft != '' && $timeleft != "no-timeleft") {
                    $data = $data->orderby('days', $timeleft);
                }
                $data = $data->get()->toArray();
                $dataArray[] = ['Sr.No', 'Project ID', 'Title', 'Client', 'Capabilities', 'Timeline(Days)', 'Price(RMB)', 'Created Date', 'Suggestion'];
                if (!empty($data)) {
                    foreach ($data as $key => $value) {
                        $tempArray = array();
                        array_push($tempArray, $value['sr_no']);
                        array_push($tempArray, $value['id']);
                        $projectNumber = $value['project_number'] ? "(" .$value['project_number'] .")" : ""; 
                        $projectTitle = $value['project_title'].$projectNumber;
                        array_push($tempArray, $projectTitle);
                        array_push($tempArray, $value['buyer']['first_name'] . " " . $value['buyer']['last_name']);
                        if (!empty($value['skills'])) {
                            $text = "";
                            foreach ($value['skills'] as $keySkills => $valueSkills) {
                                $text .= $valueSkills['skill_detail']['skill_name'] . ",";
                            }
                            $text = rtrim($text, ",");
                            array_push($tempArray, $text);
                        } else {
                            array_push($tempArray, "");
                        }
                        array_push($tempArray, $value['project_timeline']);
                        array_push($tempArray, $value['project_budget']);
                        array_push($tempArray, $value['created_at']);
                        //Code to set the dynamic suggestion count comes here
                        // if (!empty($value['suggestions'])) {
                        //  loop comes here to get the count of suggestions
                        // } else {
                        //     array_push($tempArray, "0");
                        // }
                        array_push($tempArray, "2");
                        array_push($dataArray, $tempArray);
                    }
                    if ($exportValue == 1) {
                        UtilityController::ExportData($dataArray, 'Six-Clouds-Jobs', 'Six-Clouds-Jobs', 'All Jobs Data', "xlsx");
                    } elseif ($exportValue == 2) {
                        UtilityController::ExportData($dataArray, 'Six-Clouds-Jobs', 'Six-Clouds-Jobs', 'All Jobs Data', "csv");
                    }
                }
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
    //###############################################################
    //Function Name : ExportBuyerData
    //Author        : Ketan Solanki <ketan@creolestudios.com>
    //Purpose       : To export the data in Excel or CSV Format
    //In Params     : Export Type( 1 => Excel, 2=> CSV)
    //Return        : Download the file
    //Date          : 28th June 2018
    //###############################################################
    public function ExportBuyerData(Request $request)
    {
        try {
            $exportValue = (!empty($request->Exportvalue) ? $request->Exportvalue : '');
            $searchString = ($request->search != '' ? $request->search : '');
            $sort = ($request->sort != '' ? $request->sort : 'asc');
            DB::statement(DB::raw('set @row:=0'));
            $data = User::Select("id", "first_name", "last_name", "email_address", "city_id", "country_id")
                ->selectRaw('@row:=@row+1 as sr_no')
                ->where('is_buyer', 1)
                ->with('city', 'country', 'userpayments')
                ->withCount([
                    'marketplaceproject' => function ($query) {
                        $query->where('project_status', 3);
                    },
                ]);
            if (isset($searchString) && $searchString != '' && $searchString != "no-search"):
                $data = $data->whereHas('adminmarketplaceprojectdetailinfo', function ($query) use ($searchString) {
                    $query->where('first_name', 'like', '%' . $searchString . '%')
                        ->orWhere('last_name', 'like', '%' . $searchString . '%')
                        ->orWhere('email_address', 'like', '%' . $searchString . '%');
                });
            endif;
            if (isset($sort) && $sort != "no-sort") {
                if ($sort == 'asc' || $sort == 'desc') {
                    $data = $data->orderby('created_at', $sort);
                } elseif ($sort == 'nameasc') {
                    $data = $data->orderby('first_name', 'ASC');
                } elseif ($sort == 'namedesc') {
                    $data = $data->orderby('first_name', 'DESC');
                } elseif ($sort == 'projectasc') {
                    $data = $data->orderby('adminprojects_count', 'ASC');
                } elseif ($sort == 'projectdesc') {
                    $data = $data->orderby('adminprojects_count', 'DESC');
                }
            }
            $data = $data->withCount('adminprojects')
                ->get()
                ->toArray();
            $dataArray[] = ['Sr.No', 'Buyer Name', 'Email', 'Project Posted', 'Project Completed', 'Location', 'Total Spending(RMB)'];
            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    $tempArray = array();
                    array_push($tempArray, $value['sr_no']);
                    array_push($tempArray, $value['first_name'] . " " . $value['last_name']);
                    array_push($tempArray, $value['email_address']);
                    array_push($tempArray, (string) $value['adminprojects_count']);
                    array_push($tempArray, (string) $value['marketplaceproject_count']);
                    array_push($tempArray, $value['city']['state']['name'] . ", " . $value['country']['name']);
                    $value['totalSpending'] = "0";
                    if (!empty($value['userpayments'])) {
                        $value['totalSpending'] = $value['userpayments'][0]['total_amount_spend'];
                    }
                    array_push($tempArray, $value['totalSpending']);
                    array_push($dataArray, $tempArray);
                }
                if ($exportValue == 1) {
                    UtilityController::ExportData($dataArray, 'Six-Clouds-Buyers', 'Six-Clouds-Buyers', 'Buyers Data', "xlsx");
                } elseif ($exportValue == 2) {
                    UtilityController::ExportData($dataArray, 'Six-Clouds-Buyers', 'Six-Clouds-Buyers', 'Buyers Data', "csv");
                }
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
    //###############################################################
    //Function Name : ExportSellerData
    //Author        : Ketan Solanki <ketan@creolestudios.com>
    //Purpose       : To export the data in Excel or CSV Format
    //In Params     : Export Type( 1 => Excel, 2=> CSV)
    //Return        : Download the file
    //Date          : 28th June 2018
    //###############################################################
    public function ExportSellerData(Request $request)
    {
        try {
            $exportValue = (!empty($request->Exportvalue) ? $request->Exportvalue : '');
            $category = (isset($request->category) && $request->category != '' ? $request->category : 'all');
            $sort = (isset($input['sort']) ? $input['sort'] : '');
            $searchString = ($request->search != '' ? $request->search : '');
            $proj_availability = ($request->proj_availability != '' ? $request->proj_availability : '');
            $rate = ($request->rate != '' ? $request->rate : '');
            $availability_long_term = ($request->availability_long_term != '' ? $request->availability_long_term : '');
            $budget = ($request->budget != '' ? explode(",", $request->budget) : '');
            $name = ($request->name != '' ? $request->name : '');
            $review = ($request->review != '' ? $request->review : '');
            $complete_project = ($request->complete_project != '' ? $request->complete_project : '');
            $earnings = ($request->earnings != '' ? $request->earnings : '');

            DB::statement(DB::raw('set @row:=0'));
            $data = User::select("id", "first_name", "last_name", "email_address", "city_id", "country_id", "mp_designer_min_price", "mp_designer_max_price")
                ->selectRaw('@row:=@row+1 as sr_no')
                ->where('is_designer', 1)
                ->with([
                    'city' => function ($query) {
                        $query->get();
                    },
                    'country' => function ($query) {
                        $query->get();
                    },
                    'skills' => function ($query) {
                        $query->groupby('skill_set_id')->get();
                    },
                    'sumofratings' => function ($query) {
                        $query->get();
                    },
                ])
                ->withCount(['reviews', 'adminmarketplaceprojectdetailinfo', 'adminmarketplaceprojectdetailinfowithstatus', 'rating', 'admin_completed_projects']);
            if (isset($searchString) && $searchString != '' && $searchString != 'no-search'):
                $data = $data->whereHas('adminmarketplaceprojectdetailinfo', function ($query) use ($searchString) {
                    $query->where('first_name', 'like', '%' . $searchString . '%')
                        ->orWhere('last_name', 'like', '%' . $searchString . '%')
                        ->orWhere('email_address', 'like', '%' . $searchString . '%');
                });
            endif;
            $data = $data->withCount([
                'payment AS total_sum' => function ($query) {
                    $query->select(DB::raw("SUM(total_amount) as totalsum"))->whereIn('payment_status', array(2, 4, 5));
                },
            ]);
            if (!empty($category) && $category != 'all' && $category != 'no-category'):
                $data = $data->whereHas('adminskills', function ($query) use ($category) {
                    $query->where('designer_skills.skill_set_id', $category);
                });
            endif;

            if (!empty($proj_availability) && $proj_availability != 'no-proj_availability'):
                $data = $data->whereIn('mp_designer_proj_availability', $proj_availability);
            endif;

            if (!empty($availability_long_term) && $availability_long_term == 1 && $availability_long_term != 'no-availability_long_term'):
                $data = $data->where('longterm_availability', 1);
            endif;

            if (!empty($budget)):
                $minBudget = $budget[0];
                $maxBudget = $budget[1];
                $data = $data->where('mp_designer_min_price', '>=', $minBudget)
                    ->where('mp_designer_max_price', '<=', $maxBudget);
            endif;
            if (isset($rate) && $rate != 0 && $rate != 'no-rate') {
                $from = $rate - 0.9;
                $to = $rate;
                $data = $data->where('total_avg_rate', '>=', $from)
                    ->where('total_avg_rate', '<=', $to);
            }
            if (isset($sort) && $sort != 'no-sort') {
                $data = $data->orderby('created_at', $sort);
            }

            if (isset($name) && $name != 'no-name'):
                $data = $data->orderby('first_name', $name);
            endif;

            if (isset($review) && $review != 'no-review'):
                $data = $data->orderby('reviews_count', $review);
            endif;

            if (isset($complete_project) && $complete_project != 'no-complete_project'):
                $data = $data->orderby('admin_completed_projects_count', $complete_project);
            endif;

            if (isset($earnings) && $earnings != 'no-earnings'):
                $data = $data->orderby('total_sum_count', $earnings);
            endif;
            $data = $data->get()
                ->toArray();
            $dataArray[] = ['Sr.No', 'Seller Name', 'Email', 'Ratings', 'Number of Reviews', 'Number of Invitation Sent', 'Number of Invitation Accepted', 'Location', 'Price Range (RMB)', 'Capabilities'];
            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    $tempArray = array();
                    $ratingArray = collect($value['sumofratings']);
                    $totalRating = $ratingArray->sum('rating');
                    if (isset($value['rating_count']) && is_numeric($value['rating_count']) && $value['rating_count'] > 0) {
                        $value['average_ratings'] = $totalRating / $value['rating_count'];
                    } else {
                        $value['average_ratings'] = 0;
                    }
                    $skillTemp = $value;
                    if (!empty($skillTemp['skills'])) {
                        $text = "";
                        foreach ($value['skills'] as $keySkills => $valueSkills) {
                            $text .= $valueSkills['skill_details']['skill_name'] . ",";
                        }
                        $text = rtrim($text, ",");
                        $value['designer_skill'] = $text;
                        $skill_count = count($value['skills']);
                        $value['skill_count'] = $skill_count - 1;
                    } else {
                        $value['designer_skill'] = "";
                        $value['skill_count'] = 0;
                    }
                    array_push($tempArray, $value['sr_no']);
                    array_push($tempArray, $value['first_name'] . " " . $value['last_name']);
                    array_push($tempArray, $value['email_address']);
                    array_push($tempArray, (string) $value['average_ratings']);
                    array_push($tempArray, (string) $value['reviews_count']);
                    array_push($tempArray, (string) $value['adminmarketplaceprojectdetailinfo_count']);
                    array_push($tempArray, (string) $value['adminmarketplaceprojectdetailinfowithstatus_count']);
                    array_push($tempArray, $value['city']['state']['name'] . ", " . $value['country']['name']);
                    array_push($tempArray, $value['mp_designer_min_price'] . " - " . $value['mp_designer_max_price']);
                    array_push($tempArray, $value['designer_skill']);
                    array_push($dataArray, $tempArray);
                }
                if ($exportValue == 1) {
                    UtilityController::ExportData($dataArray, 'Six-Clouds-Sellers', 'Six-Clouds-Sellers', 'Sellers Data', "xlsx");
                } elseif ($exportValue == 2) {
                    UtilityController::ExportData($dataArray, 'Six-Clouds-Sellers', 'Six-Clouds-Sellers', 'Sellers Data', "csv");
                }
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    /********************************************   Ticket section start  **********************************************/
    
    //###############################################################
    //Function Name :   Gettickettypes
    //Author :          Zalak Kapadia <zalak@creolestudios.com>
    //Purpose :         To get types for ticket section
    //In Params :       Void
    //Return :          json
    //Date :            3rd july 2018
    //###############################################################
    public function Gettickettypes(){

        try {

            $types=TicketType::where('status',1)->get();

            $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', '', $types);
            return $returnData;

        } catch (Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
    //###############################################################
    //Function Name :   Getticketcategories
    //Author :          Zalak Kapadia <zalak@creolestudios.com>
    //Purpose :         To get categories for ticket section based on selected ticket type
    //In Params :       Void
    //Return :          json
    //Date :            3rd july 2018
    //###############################################################
    public function Getticketcategories(){

        try {

            $input =Input::all();
            $categories=TicketCategory::where('ticket_type_id',$input['tickettype'])->get();

            $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', '', $categories);
            return $returnData;

        } catch (Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
    //###############################################################
    //Function Name :   Getticketlisting
    //Author :          Zalak Kapadia <zalak@creolestudios.com>
    //Purpose :         To get ticket listing
    //In Params :       Void
    //Return :          json
    //Date :            3rd july 2018
    //###############################################################
    public function Getticketlisting(){
        try {
            $input =Input::all();
           
            $searchString = (isset($input['search']) ? $input['search'] : '');
            $start_date=(isset($input['start_date']))?$input['start_date']:'';
            $end_date=(isset($input['end_date']))?$input['end_date']:Carbon::now();
            
            $ticketQuery=CustomerSupport::where('ticket_for',$input['ticket_for'])->orderBy('created_at','desc')
            ->with([
                'customer_ticket_type'=>function($query){
                    $query->get();
                },
                'customer_ticket_category'=>function($query){
                    $query->get();
                }
            ]);
            if(isset($input['type'])){
                $ticketQuery->where('ticket_type_id',$input['type']);
            }
            if(isset($input['category'])){
                $ticketQuery->where('ticket_category_id',$input['category']);
            }
            if (isset($searchString) && $searchString != ''){
                $ticketQuery->where('ticket_subject', 'like', '%' . $searchString . '%')
                            ->orWhere('user_name', 'like', '%' . $searchString . '%');
            }
            
            if($start_date!='' && $end_date!=''){
                $ticketQuery->where('created_at','>=',date("Y-m-d", strtotime($start_date)));
                $ticketQuery->where('created_at','<=',date("Y-m-d 23:59:59", strtotime($end_date)));
            }

            $tickets=$ticketQuery->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))->toArray();
           
            if(!empty($tickets['data'])){
                foreach ($tickets['data'] as $key => $value) {
                    $date = Carbon::parse($value['created_at'])->timezone($value['time_zone']);
                    $tickets['data'][$key]['created_at'] = UtilityController::Changedateformat($date, 'd F, Y h:i A');
                }
            }

            $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', '', $tickets);
            return $returnData;

        } catch (Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name :   Exportticketdata
    //Author :          Zalak Kapadia <zalak@creolestudios.com>
    //Purpose :         To export ticket data
    //In Params :       Void
    //Return :          json
    //Date :            16th july 2018
    //###############################################################
    public function Exportticketdata(Request $request){
        try {

            $input =Input::all();
            $exportValue = (!empty($request->Exportvalue) ? $request->Exportvalue : '');
            $searchString = ($request->search != '' ? $request->search : '');
            $start_date=(isset($request->startdate))?$request->startdate:'';
            $end_date=(isset($request->enddate))?$request->enddate:Carbon::now();

            $ticketQuery=CustomerSupport::orderBy('created_at','desc')
            ->with([
                'customer_ticket_type'=>function($query){
                    $query->get();
                },
                'customer_ticket_category'=>function($query){
                    $query->get();
                }
            ]);
            if(isset($request->tickettype) &&  $request->tickettype!='no-type'){
                $ticketQuery->where('ticket_type_id',$request->tickettype);
            }
            if(isset($request->ticketcategory) &&  $request->ticketcategory!='no-category'){
                $ticketQuery->where('ticket_category_id',$request->ticketcategory);
            }
            if (isset($searchString) && $searchString != 'no-search'){
                $ticketQuery->where('ticket_subject', 'like', '%' . $searchString . '%')
                            ->orWhere('user_name', 'like', '%' . $searchString . '%');
            }
            if($start_date!='no-startdate' && $end_date!='no-enddate'){
                $ticketQuery->where('created_at','>=',date("Y-m-d", strtotime($start_date)));
                $ticketQuery->where('created_at','<=',date("Y-m-d 23:59:59", strtotime($end_date)));
            }

            $tickets=$ticketQuery->get()->toArray();

            $dataArray[] = ['Sr.No', 'Ticket Subject', 'Ticket Type', 'Ticket Category', 'Ticket description','Created By', 'Email Address','Created At'];
            if (!empty($tickets)) {
                foreach ($tickets as $key => $value) {
                    $tempArray = array();
                    
                    array_push($tempArray, $key+1);
                    array_push($tempArray, $value['ticket_subject']);
                    array_push($tempArray, $value['customer_ticket_type']['ticket_type_name']);
                    array_push($tempArray, $value['customer_ticket_category']['ticket_category_name']);
                    array_push($tempArray, $value['ticket_description']);
                    array_push($tempArray, $value['user_name']);
                    array_push($tempArray, $value['email_address']);
                    array_push($tempArray, UtilityController::Changedateformat($value['created_at'], 'd F, Y'));
                    
                    array_push($dataArray, $tempArray);
                }
            }

            if ($exportValue == 1) {
                UtilityController::ExportData($dataArray, 'Six-Clouds-Tickets', 'Six-Clouds-Tickets', 'Ticket Data', "xlsx");
            } elseif ($exportValue == 2) {
                UtilityController::ExportData($dataArray, 'Six-Clouds-Tickets', 'Six-Clouds-Tickets', 'Ticket Data', "csv");
            }
        } catch (Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
    //###############################################################
    //Function Name :   Getticketdetails
    //Author :          Zalak Kapadia <zalak@creolestudios.com>
    //Purpose :         To get ticket detail
    //In Params :       Void
    //Return :          json
    //Date :            4th july 2018
    //###############################################################
    public function Getticketdetails(){
        try {
            $input =Input::all();
            $ticket_id = substr($input['id'], strpos($input['id'], ":") + 1);
            $ticket_id = base64_decode($ticket_id);

            $ticketDetail=CustomerSupport::where('id',$ticket_id)
            ->with([
                'customer_ticket_type'=>function($query){
                    $query->get();
                },
                'customer_ticket_category'=>function($query){
                    $query->get();
                }
            ])->first();
            
            if(!empty($ticketDetail)){

                $date = Carbon::parse($ticketDetail['created_at'])->timezone($ticketDetail['time_zone']);
                $ticketDetail['date_created_at'] = UtilityController::Changedateformat($date, 'd F, Y h:i A');
                if($ticketDetail['ticket_for']==1){
                    $ticketDetail['button_color'] = 'btn-pink';
                } elseif ($ticketDetail['ticket_for']==2) {
                    $ticketDetail['button_color'] = 'btn-purple';
                } else {
                    $ticketDetail['button_color'] = 'btn-dark-gray';
                }
                if($ticketDetail['ticket_closed_at']!=''){   
                    $date = Carbon::parse($ticketDetail['ticket_closed_at'])->timezone($ticketDetail['time_zone']);
                    $ticketDetail['ticket_closed_at'] = UtilityController::Changedateformat($date, 'd F, Y h:i A');
                }
                $ticketDetail['ticket_attachment_path'] = 'http://'.Config('constants.messages.BUCKET').'.'.Config('constants.messages.OSS_REGION').'.aliyuncs.com/'.Config('constants.path.BUCKET_CUSTOMER_SUPPORT') . $ticketDetail['ticket_attachments'];
            }
            $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', '', $ticketDetail);
            return $returnData;

        } catch (Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name :   Sendmessageuser
    //Author :          Zalak Kapadia <zalak@creolestudios.com>
    //Purpose :         To send message from admin to user for ticket
    //In Params :       Void
    //Return :          json
    //Date :            4th july 2018
    //###############################################################
    public function Sendmessageuser(){
        try {

            $input =Input::all();
            \DB::beginTransaction();
            if(!isset($input['admin_message'])){
                $returnData = UtilityController::Generateresponse(true, 'ENTER_MESSAGE', '', '');
            }
            else{

                $result = CustomerSupport::where('id', $input['id'])->update(array('status' => 1));
                if($input['ticket_attachments']!=''){
                    $fileName=$input['ticket_attachments'];
                    $bucket    = UtilityController::Getmessage('BUCKET');
                    $object    = UtilityController::Getpath('BUCKET_CUSTOMER_SUPPORT').$fileName;
                    $localfile = public_path('').UtilityController::Getpath('CUSTOMER_SUPPORT_UPLOAD_PATH').$fileName;
                    $options   = array(
                        OssClient::OSS_FILE_DOWNLOAD => $localfile,
                    );  
                    $ossClient           = new OssClient(UtilityController::Getmessage('ACCESS_KEY_ID'), UtilityController::Getmessage('ACCESS_KEY_SECRET'), UtilityController::Getmessage('END_POINT'));
                    $ossClient->getObject($bucket, $object, $options);
                }
                $userData=User::where('email_address',$input['email_address'])->first();
                if($input['ticket_for']==1)
                    $product = 'Ignite';
                elseif ($input['ticket_for']==1)
                    $product = 'Sixteen';
                else
                    $product = 'Proof Reading';
                if(empty($userData)){
                    $input['content']='Hello <strong>'.$input['user_name'].',
                        </strong><br> 
                        <br>
                        '.$input['admin_message'].'
                        <br>
                        <br>
                        <div style="font-family:Arial,Helvetica,sans-serif; font-size: 11px; color: #6C6C6C;">
                            <b>Email Address:</b> '.$input['email_address'].'
                            <br> 
                            <b>Product:</b> '.$product.'
                            <br> 
                            <b>Ticket Type:</b> '.$input['customer_ticket_type']['ticket_type_name'].' 
                            <br> 
                            <b>Ticket Category:</b> '.$input['customer_ticket_category']['ticket_category_name'].'
                            <br> 
                            <b>Subject: </b>'.$input['ticket_subject'].'
                            <br> 
                            <b>Description: </b>'.$input['ticket_description'].'     
                        </div>';

                    $input['footer_content']='From,<br /> SixClouds Sixteen';

                    $input['footer']='SixClouds';   
                    
                }else{
                    $current_language=$userData['current_language'];
                    if($current_language==1){
                        $input['content']='Hello <strong>'.$input['user_name'].',
                            </strong><br> 
                            <br>
                            '.$input['admin_message'].'
                            <br>
                            <br>
                            <div style="font-family:Arial,Helvetica,sans-serif; font-size: 11px; color: #6C6C6C;">
                                <b>Email Address:</b> '.$input['email_address'].'
                                <br> 
                                <b>Product:</b> '.$product.'
                                <br> 
                                <b>Ticket Type:</b> '.$input['customer_ticket_type']['ticket_type_name'].' 
                                <br> 
                                <b>Ticket Category:</b> '.$input['customer_ticket_category']['ticket_category_name'].'
                                <br> 
                                <b>Subject: </b>'.$input['ticket_subject'].'
                                <br> 
                                <b>Description: </b>'.$input['ticket_description'].'     
                            </div>';

                        $input['footer_content']='From,<br /> SixClouds Sixteen';

                        $input['footer']='SixClouds';      
                    }else{
                        $input['content']='你好 <strong>'.$input['user_name'].',
                            </strong><br> 
                            <br>
                            '.$input['admin_message'].'
                            <br>
                            <br>
                            <div style="font-family:Arial,Helvetica,sans-serif; font-size: 11px; color: #6C6C6C;">
                                <b>电子邮件地址:</b> '.$input['email_address'].'
                                <br> 
                                <b>订阅系列:</b> '.$product.'
                                <br> 
                                <b>门票类型:</b> '.$input['customer_ticket_type']['ticket_type_name'].' 
                                <br> 
                                <b>门票类别:</b> '.$input['customer_ticket_category']['ticket_category_name'].'
                                <br> 
                                <b>学科: </b>'.$input['ticket_subject'].'
                                <br> 
                                <b>描述: </b>'.$input['ticket_description'].'     
                            </div>';

                        $input['footer_content']='六云十六';

                        $input['footer']='SixClouds';
                    }
                }

                Mail::send('emails.email_template', $input, function ($message) use ($input) {
                $message->from('support@sixclouds.net', 'SixClouds');
                if($input['ticket_attachments']!=''){
                    $message->attach(public_path('').UtilityController::Getpath('CUSTOMER_SUPPORT_UPLOAD_PATH').$input['ticket_attachments']);    
                }
                
                $message->to($input['email_address'])->subject($input['ticket_subject'].'#'.$input['ticket_number']);
                });
                if($input['ticket_attachments']!=''){
                    unlink(public_path('').UtilityController::Getpath('CUSTOMER_SUPPORT_UPLOAD_PATH').$fileName);
                }
                $returnData = UtilityController::Generateresponse(true, 'MESSAGE_SENT', '', ''); 
            }
            \DB::commit();
            return $returnData;

        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
    //###############################################################
    //Function Name :   Markascompleted
    //Author :          Zalak Kapadia <zalak@creolestudios.com>
    //Purpose :         To change ticket status as completed
    //In Params :       Void
    //Return :          json
    //Date :            16th july 2018
    //###############################################################
    public function Markascompleted(){
        try {

            $input =Input::all();
            \DB::beginTransaction();
            $data['ticket_status'] = 3;
            $data['ticket_closed_at'] = Carbon::now()->timezone($input['ticket']['time_zone']);
            $result = UtilityController::Makemodelobject($data,'CustomerSupport','',$input['ticket']['id']);
            if($result){
                $date = Carbon::parse($result['created_at'])->timezone($result['time_zone']);
                $result['date_created_at'] = UtilityController::Changedateformat($date, 'd F, Y h:i A');
                $result['ticket_closed_at'] = UtilityController::Changedateformat($result['ticket_closed_at'], 'd F, Y h:i A');
                $returnData = UtilityController::Generateresponse(true, 'TICKET_RESOLVED', '', $result); 
                \DB::commit();
                return $returnData;
            }else{
                throw new \Exception(UtilityController::Getmessage('GENERAL_ERROR'));
            }
            
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, $e->getMessage(), '', '');
            return $returnData;
        }
    }
     //###############################################################
    //Function Name :   GetUserCountriesListing
    //Author :          Ketan Solanki <ketan@creolestudios.com>
    //Purpose :         To get the list of countries with state and city
    //In Params :       Void
    //Return :          json
    //Date :            5th july 2018
    //###############################################################
    public function GetUserCountriesListing(){
        try {

            $input =Input::all();                        
            $searchString = (isset($input['search']) ? $input['search'] : '');
            $countryType  = (isset($input['others']) ? $input['others'] : '');            
            $countryQuery=Country::where("others",$countryType)->where("status","2")
            ->with([
                'states'=>function($query){
                    $query->with('hascities')->get();
                },                 
            ]);
            if (isset($searchString) && $searchString != ''){
                $countryQuery->where('name', 'like', '%' . $searchString . '%')
                            ->orWhere('sortname', 'like', '%' . $searchString . '%');
            }

            $countries=$countryQuery->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))->toArray();
            $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', '', $countries);
            return $returnData;

        } catch (Exception $e) {             
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
    //###############################################################
    //Function Name :   ExportCountryData
    //Author :          Ketan Solanki <ketan@creolestudios.com>
    //Purpose :         To get the list of countries with state and city
    //In Params :       Void
    //Return :          json
    //Date :            5th july 2018
    //###############################################################
    public function ExportCountryData(Request $request){
        try {            
            $searchString = (!empty($request->search) ? $request->search : '');            
            $exportValue = (!empty($request->Exportvalue) ? $request->Exportvalue : '');             
            $data = Country::select("id","name")->where("others","1")->where("status","2")                
                ->with([
                    'states'=>function($query){
                        $query->with('hascities')->get();
                    },                 
                ]);               
                if (isset($searchString) && $searchString != '' && $searchString != 'no-search'):
                    $data = $data->where('name', 'like', '%' . $searchString . '%')
                                ->orWhere('sortname', 'like', '%' . $searchString . '%');
                endif;
            $data = $data->get()
                ->toArray();                
            $dataArray[] = ['Country', 'State', 'City'];
            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    foreach($value['states'] as $keyState => $valueState){                        
                        foreach($valueState['hascities'] as $keyCities => $valueCities){
                            $tempArray = array();                            
                            array_push($tempArray, $value['name']);
                            array_push($tempArray, $valueState['name']);
                            array_push($tempArray, $valueCities['name']);
                            array_push($dataArray, $tempArray);
                        }
                    }                    
                }                
                if ($exportValue == 1) {
                    UtilityController::ExportData($dataArray, 'Six-Clouds-User-Added-Countries', 'Six-Clouds-User-Added-Countries', 'Countries Data', "xlsx");
                } elseif ($exportValue == 2) {
                    UtilityController::ExportData($dataArray, 'Six-Clouds-User-Added-Countries', 'Six-Clouds-User-Added-Countries', 'Countries Data', "csv");
                }
            }
        } catch (Exception $e) {             
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
    //###############################################################
    //Function Name :   uploadCSVFileData
    //Author :          Ketan Solanki <ketan@creolestudios.com>
    //Purpose :         To get the list of countries with state and city
    //In Params :       Void
    //Return :          json
    //Date :            5th july 2018
    //###############################################################
    public function uploadCSVFileData(Request $request){
        try{
            $Input = Input::all();
            if (Input::hasFile('UploadData')) {
                $extension = \File::extension($Input['UploadData']->getClientOriginalName());
                if (in_array($extension, array('xlsx', 'XLSX', 'csv', 'CSV'))) {                                     
                    $csvFileName        = rand() . time() . str_replace(' ', '_', Input::file('UploadData')->getClientOriginalName());
                    $csvFileDestinationPath = public_path('') . UtilityController::Getpath('COUNTRY_CSV_FILE_PATH');                 
                    Input::file('UploadData')->move($csvFileDestinationPath, $csvFileName);
                    if(isset($csvFileName) && $csvFileName != ""){    
                        $path = public_path('') . UtilityController::Getpath('COUNTRY_CSV_FILE_PATH').$csvFileName;
                        $data = \Excel::load($path)->get()->toArray();         
                        if(!empty($data)){
                            foreach($data as $key => $value){                        
                                $countryData = Country::where("name",$value['country'])->update(['status' => 1, 'others' => 3]);  
                            }
                        }
                        //Delete the file                
                        \File::delete($path);
                        $returnData = UtilityController::Generateresponse(true, 'COUNTRY_LIST_UPDATED', '', '');
                    }else{
                        $returnData = UtilityController::Generateresponse(true, 'ERROR_UPLOADING_FILE', '', '');
                    }
                }else{                    
                    $returnData        = UtilityController::Generateresponse(false, 'PROVIDE_VALID_FILE', '', '');
                }
            } else {
                $returnData        = UtilityController::Generateresponse(false, 'NO_FILE_UPLOADED', '', '');
            }
            return $returnData;
    } catch (\Exception $e) {             
        $sendExceptionMail = UtilityController::Sendexceptionmail($e);
        $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        return $returnData;
    }
    } 

    //###############################################################
    //Function Name :   Getsellerdetail
    //Author :          Zalak Kapadia <zalak@creolestudios.com>
    //Purpose :         To get detail of seller
    //In Params :       Void
    //Return :          json
    //Date :            9th july 2018
    //###############################################################
    public function Getsellerdetail(Request $request){

        try{
            $Input = Input::all();

            $sellerInfo=User::where('slug',$Input['id'])->with('skills','school','country','completed_projects')
            ->first()->toArray();

            if(!empty($sellerInfo)){
                $sellerInfo['user_image']=UtilityController::Imageexist($sellerInfo['image'], public_path('') . UtilityController::Getmessage('USER_PROFILE_PATH'), url('') . UtilityController::Getmessage('USER_PROFILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));

                if ($sellerInfo['mp_designer_proj_availability'] == 1) {
                    $sellerInfo['mp_designer_proj_availability'] = 'Full-time (40+ hours per week)';
                }
                if ($sellerInfo['mp_designer_proj_availability'] == 2) {
                    $sellerInfo['mp_designer_proj_availability'] = 'Part-time (25-36 hours per week)';
                }
                if ($sellerInfo['mp_designer_proj_availability'] == 3) {
                    $sellerInfo['mp_designer_proj_availability'] = 'Hobbist (12-24 hours per week)';
                }
                if ($sellerInfo['mp_designer_proj_availability'] == 4) {
                    $sellerInfo['mp_designer_proj_availability'] = 'Casual (less than 12 hours per week)';
                }

                if($sellerInfo['languages_known']!=''){

                    $languages_known=json_decode($sellerInfo['languages_known']);

                    if(!empty($languages_known)){

                        foreach ($languages_known as $key => $value) {
                          
                           $sellerInfo['languages'][$key]['language']=$value->language;
                           $sellerInfo['languages'][$key]['proficiency']=$value->proficiency;
                        }
                    }
                }
                $tempCategory = [];
                if (!empty($sellerInfo['skills'])) {
                    foreach ($sellerInfo['skills'] as $key => $value) {

                        $tempSkills     = $value['skill_details'];
                        $tempCategory[] = $tempSkills['skill_name'];
                    }
                }
                $sellerInfo['skills']=$tempCategory;

                $completedProjects = 0;
                foreach ($sellerInfo['completed_projects'] as $keyInner => $valueInner) {
                    if ($valueInner['project_details']['project_status'] == 3) {
                        $completedProjects++;
                    }

                }
                $sellerInfo['projects_completed'] = $completedProjects;

                $onGoingProjects = 0;
                foreach ($sellerInfo['completed_projects'] as $keyInner => $valueInner) {
                    if ($valueInner['project_details']['project_status'] == 1) {
                        $onGoingProjects++;
                    }

                }
                $sellerInfo['projects_ongoing'] = $onGoingProjects;
               
            }
            $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', '', $sellerInfo);
            return $returnData;

        }catch (\Exception $e) {             
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Getadminservices
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : To get the services for seller profile page
    //In Params : Void
    //Return : json
    //Date : 10th July 2018
    //###############################################################
    public function Getadminservices(Request $request) {
        try {
            $Input    = Input::all();
           
            $sellerId = substr($Input['id'], strpos($Input['id'], ":") + 1);
            $sellerId = base64_decode($sellerId);

            $services = MarketPlaceSellerServiceSkill::with('service_skill_detail','service_details')->where('seller_id',$sellerId)->where('status',1);
            if(!empty($skillId))
                $services = $services->whereIn('skill_id',$skillId);    
            $services = $services->get();
            foreach ($services as $key => $value) {
                foreach ($value['service_details'] as $keyInner => $valueInner) {
                    $value['service_details'][$keyInner]['parent_service_name_english'] = $value['service_skill_detail']['skill_name'];
                    $value['service_details'][$keyInner]['parent_service_name_chinese'] = $value['service_skill_detail']['skill_name_chinese'];
                    $value['service_details'][$keyInner]['parent_service_skill_id']     = $value['service_skill_detail']['id'];
                    
                    if(!empty($valueInner['service_images'])){
                        foreach ($valueInner['service_images'] as $keyIn => $valueIn) {
                            $valueIn['image'] = UtilityController::Imageexist($valueIn['image_name'], public_path('') . UtilityController::Getpath('MP_SERVICES_UPLOAD_PATH'), url('') . UtilityController::Getpath('MP_SERVICES_UPLOAD_URL'), url('') . UtilityController::Getmessage('NO_IMAGE_URL'));        
                        }
                    }
                   
                }
            }
            $responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$services);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, 'GENERAL_ERROR', '', '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Designeradminworkhistoryandreviews
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : to display designer work history and reviews
    //In Params : Void
    //Return : json
    //Date : 3rd May 2018
    //###############################################################
    public function Designeradminworkhistoryandreviews(Request $request)
    {
        try {
            \DB::beginTransaction();
            $input = Input::all();

            //$getId=User::select('id')->where('slug',$input['id'])->get();
            $id                   = substr($input['id'], strpos($input['id'], ":") + 1);
            $assigned_designer_id = base64_decode($id);

            $designerWorkReviewDataQuery = MarketPlaceProjectsDetailInfo::with('project_details_designer_admin')
                ->with('payment_details')
                ->whereIn('status', array(7,14))
                ->where('assigned_designer_id', $assigned_designer_id);

            $designerWorkReviewData = $designerWorkReviewDataQuery
                ->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))
                ->toArray();

            if ($designerWorkReviewData) {
                foreach ($designerWorkReviewData['data'] as $key => $value) {

                    $completed_project_data = $value;

                    $date = Carbon::parse($value['project_completed_at'])->timezone(Auth::user()->timezone);
                    $date_project_completed_on = UtilityController::Changedateformat($date, 'd M, Y');

                    $designerWorkReviewData['data'][$key]['project_completed_on'] = $date_project_completed_on;

                    $date = Carbon::parse($value['payment_details']['created_at'])->timezone(Auth::user()->timezone);
                    $designerWorkReviewData['data'][$key]['project_started_at']   = UtilityController::Changedateformat($date, 'd M, Y');

                    $designerWorkReviewData['data'][$key]['project_details_designer_admin']['project_timeline'] = $value['project_details_designer_admin']['project_timeline'] + $value['project_details_designer_admin']['extended_days'];

                    $designerWorkReviewData['data'][$key]['project_details_designer_admin']['project_creator']['image'] = UtilityController::Imageexist($value['project_details_designer_admin']['project_creator']['image'], public_path('') . UtilityController::Getmessage('USER_PROFILE_PATH'), url('') . UtilityController::Getmessage('USER_PROFILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));

                    $message_count = 0;

                    $designerWorkReviewData['data'][$key]['message_count'] = $message_count;

                    if (!empty($designerWorkReviewData['data'][$key]['project_details_designer_admin']['message_board'])) {
                        foreach ($designerWorkReviewData['data'][$key]['project_details_designer_admin']['message_board'] as $keyInner => $valueInner) {
                            if ($designerWorkReviewData['data'][$key]['project_details_designer_admin']['message_board'][$keyInner]['user_id'] == $assigned_designer_id) {

                                $message_count++;
                                $designerWorkReviewData['data'][$key]['message_count'] = $message_count;
                            }

                        }
                    }

                }

                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $designerWorkReviewData);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }

            return $returnData;
        } catch (\Exception $e) {
            \DB::rollback();
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Getadminportfolio
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : To get all the uploaded portfolio(s) by designer
    //In Params : Void
    //Return : json
    //Date : 10th July 2018
    //###############################################################
    public function Getadminportfolio(Request $request)
    {
        try {
            if (Auth::check()) {
                $Input = Input::all();

                $id         = substr($Input['id'], strpos($Input['id'], ":") + 1);
                $designerId = base64_decode($id);
                
                $activePortfolio = MarketPlaceUpload::with('attachments', 'uploadedby')->where('upload_of', 3)->where('uploaded_user_id', $designerId)->where('status', 1)->orderby('id', 'DESC');

                $activePortfolio = $activePortfolio->paginate(Config('constants.other.MP_DESIGNER_PROFILE_PAGINATE'))->toArray();
                
                if (!empty($activePortfolio) && !empty($activePortfolio['data'])) {

                    foreach ($activePortfolio['data'] as $key => $value) {
                        $activePortfolio['data'][$key]['cover_image'] = UtilityController::Imageexist($value['cover_img'], public_path('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_COVER_IMAGE_PATH'), url('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_COVER_IMAGE_URL'), url('') . UtilityController::Getmessage('NO_IMAGE_URL'));

                        foreach ($value['attachments'] as $keyInner => $valueInner) {

                            $activePortfolio['data'][$key]['attachments'][$keyInner]['cover_image'] = UtilityController::Imageexist($value['cover_img'], public_path('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_COVER_IMAGE_PATH'), url('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_COVER_IMAGE_URL'), url('') . UtilityController::Getmessage('NO_IMAGE_URL'));

                            $tinyImages[] = UtilityController::Imageexist($valueInner['tiny_thumbnail'], public_path('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_TINY_PATH'), url('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_TINY_URL'));
                        }
                        if (!empty($tinyImages)) {
                            $activePortfolio['data'][$key]['tiny_images']             = $tinyImages;
                            $activePortfolio['data'][$key]['total_tiny_images']       = count($tinyImages);
                            $activePortfolio['data'][$key]['total_tiny_images_count'] = $activePortfolio['data'][$key]['total_tiny_images'] - 3;
                            $tinyImages                                               = [];
                        }

                    }

                    $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $activePortfolio);

                } else {
                    $responseArray = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0, $activePortfolio);
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '','');
        }
        return $responseArray;
    }

     //###############################################################
    //Function Name :   Getbuyerdetail
    //Author :          Zalak Kapadia <zalak@creolestudios.com>
    //Purpose :         To get detail of seller
    //In Params :       Void
    //Return :          json
    //Date :            9th july 2018
    //###############################################################
    public function Getbuyerdetail(Request $request){

        try{
            $Input = Input::all();
            $buyerInfo=User::where('slug',$Input['id'])->with('skills','school','country','completed_projects')
            ->first()->toArray();            
            if(!empty($buyerInfo)){
                $buyerInfo['user_image']=UtilityController::Imageexist($buyerInfo['image'], public_path('') . UtilityController::Getmessage('USER_PROFILE_PATH'), url('') . UtilityController::Getmessage('USER_PROFILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));
                if($buyerInfo['languages_known']!=''){
                    $languages_known=json_decode($buyerInfo['languages_known']);
                    if(!empty($languages_known)){
                        foreach ($languages_known as $key => $value) {                          
                           $buyerInfo['languages'][$key]['language']=$value->language;
                           $buyerInfo['languages'][$key]['proficiency']=$value->proficiency;
                        }
                    }
                }
                $tempCategory = [];
                if (!empty($buyerInfo['skills'])) {
                    foreach ($buyerInfo['skills'] as $key => $value) {
                        $tempSkills     = $value['skill_details'];
                        $tempCategory[] = $tempSkills['skill_name'];
                    }
                }
                $buyerInfo['skills']=$tempCategory;
                $completedProjects = 0;
                foreach ($buyerInfo['completed_projects'] as $keyInner => $valueInner) {
                    if ($valueInner['project_details']['project_status'] == 3) {
                        $completedProjects++;
                    }
                }
                $buyerInfo['projects_completed'] = $completedProjects;
                $onGoingProjects = 0;
                foreach ($buyerInfo['completed_projects'] as $keyInner => $valueInner) {
                    if ($valueInner['project_details']['project_status'] == 1) {
                        $onGoingProjects++;
                    }
                }
                $buyerInfo['projects_ongoing'] = $onGoingProjects;               
            }
            $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', '', $buyerInfo);
            return $returnData;
        }catch (\Exception $e) {             
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
   
     //###############################################################
    //Function Name : GetBuyerProjects
    //Author : Ketan Solanki <ketan@creolestudios.com>
    //Purpose : to display projects of the buyer
    //In Params : Void
    //Return : json
    //Date : 23rd July 2018
    //###############################################################
    public function GetBuyerProjects(Request $request)
    {
        try {                  
                $Input = Input::all();
                $buyerId         = substr($Input['id'], strpos($Input['id'], ":") + 1);
                $buyerId = base64_decode($buyerId);
                $activeProjects = MarketPlaceProject::select("*",DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as project_created_at"))
                ->with('selected_designer', 'rating_for_project', 'review_for_project', 'projectCreator', 'extension_details', 'seller_payment', 'priority_actioins', 'skill_detail','cart')
                ->with([
                    'projectCreator'    => function ($query) {
                        $query->get();
                    },                     
                    'skills'            => function ($query) {
                        $query->with('skillDetail')->get()->groupby('skill_set_id');
                    },
                    'message_board'     => function ($qry) {
                        $qry->with(['message_board' => function ($q) {
                            $q->where('type', '!=', 1)->get();
                        }])->whereIn('type', array(1, 4))->get();
                    },
                    'seller_payment'    => function ($query) {
                        $query->get();
                    },
                    'projectdetail' => function ($query) {
                        $query->first();
                    },                                     
                ])->with('extension_details')
                ->where(function ($query) {$query->where('project_status', 1)->orWhere('project_status', 2)
                    ->orWhere('project_status', 7)
                    ->orWhere('project_status', 8);})
                    ->withCount('message_board')
                    ->where('project_created_by_id', $buyerId)
                    ->orderby('show_first', 'DESC')
                    ->orderby('id', 'DESC');
                $activeProjects = $activeProjects->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))->toArray();
                if ($activeProjects) {
                    foreach ($activeProjects['data'] as $key => $value) {                      
                        ($activeProjects['data'][$key]['extension_details'] != '' ? (($value['extension_details']['extension_status'] == 3 || $value['extension_details']['extension_status'] == 2) ? $value['show_buyer_extension'] = 0 : $value['show_buyer_extension'] = 1) : $value['show_buyer_extension'] = 0);                                                                        
                        $date = Carbon::parse($value['selected_designer']['created_at'])->timezone("UTC");
                        $activeProjects['data'][$key]['invitation_sent_on'] = UtilityController::Changedateformat($date, 'd F, Y h:i A');
                        if ($value['selected_designer']['invitation_accepted_at'] != '') {
                            $date = Carbon::parse($value['selected_designer']['invitation_accepted_at'])->timezone("UTC");
                            $activeProjects['data'][$key]['invitation_accepted_on'] = UtilityController::Changedateformat($date, 'd F, Y h:i A');
                        } else {
                            $activeProjects['data'][$key]['invitation_accepted_on'] = "Pending";
                        }                         
                        if ($value['selected_designer']['project_completed_at'] != '') {
                            $date = Carbon::parse($value['selected_designer']['project_completed_at'])->timezone("UTC");
                            $activeProjects['data'][$key]['project_completed_on'] = UtilityController::Changedateformat($date, 'd F, Y h:i A');
                        } else {
                            $activeProjects['data'][$key]['project_completed_on'] = "Pending";
                        }
                        $value['selected_designer']['designer_details']['image'] = UtilityController::Imageexist($value['selected_designer']['designer_details']['image'], public_path('') . UtilityController::Getmessage('USER_PROFILE_PATH'), url('') . UtilityController::Getmessage('USER_PROFILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));
                        if(!empty($value['projectdetail'])){
                            $activeProjects['data'][$key]['projectdetail']['status']=$value['projectdetail'][0]['status'];
                        }                        
                        $projectAt = Carbon::parse($value['created_at'])->addDays($value['project_timeline']);
                        $activeProjects['data'][$key]['deadline'] = $projectAt->toDateTimeString();

                        //Code to get the services listing starts here
                        $PurchasedSkillsIds = array();
                        if(!empty($value['cart'])){
                            foreach($value['cart'] as $keyCart => $valueCart){
                                array_push($PurchasedSkillsIds,$valueCart['mp_seller_service_package_id']);
                            }
                        }                        
                        $activeProjects['data'][$key]['purchsed_services'] = MarketPlaceSellerServicePackage::with('skill_parent')
                        ->whereIn('id',$PurchasedSkillsIds)
                        ->where('status',1)->get();
                        //Code to get the services listing ends here
                    }
                    $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $activeProjects);
                } else {
                    $responseArray = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
                }
             
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, 'GENERAL_ERROR', Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }
    //###############################################################
    //Function Name :   Gettransactionlist
    //Author :          Zalak Kapadia <zalak@creolestudios.com>
    //Purpose :         To get transaction listing
    //In Params :       Void
    //Return :          json
    //Date :            30th july 2018
    //###############################################################
    public function Gettransactionlist(Request $request){
        try {

            $input=Input::all();

            $transactionDataQuery=MarketplaceSellerOutsatndingPaymentStatus::with('user');
            
            if(isset($input['status'])){
                $transactionDataQuery->where('payment_status',$input['status']);
            }
            if(isset($input['amount'])){
                $transactionDataQuery->orderBy('total_amount',$input['amount']);
            }else{
                $transactionDataQuery->orderBy('payment_status','asc')->orderBy('created_at','desc');
            }
            $transactionData=$transactionDataQuery->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))->toArray();

            if(!empty($transactionData['data'])){

                foreach ($transactionData['data'] as $key => $value) {
                    $transactionData['data'][$key]['transaction_created_at'] = UtilityController::Changedateformat($transactionData['data'][$key]['payment_requested_on'], 'd F, Y h:i A');
                    
                    if($transactionData['data'][$key]['payment_status']==1){
                        $transactionData['data'][$key]['transaction_payment_status'] = 'Pending';    
                    }else{
                        $transactionData['data'][$key]['transaction_payment_status'] = 'Paid';    
                        $transactionData['data'][$key]['payment_paid_on'] = UtilityController::Changedateformat($transactionData['data'][$key]['paid_on'], 'd F, Y h:i A');
                    }
                }
                
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 0, $transactionData);
            }else{
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 0);
            }
            return $returnData;
        } catch (Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, $e->getMessage(), '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name :   Withdrawamountpaidbyadmin
    //Author :          Zalak Kapadia <zalak@creolestudios.com>
    //Purpose :         To change payment status after paid by admin
    //In Params :       Void
    //Return :          json
    //Date :            30th july 2018
    //###############################################################
    public function Withdrawamountpaidbyadmin(){
        try {
            $input =Input::all();
            \DB::beginTransaction();
            $paidOn=Carbon::now();
            $resultOutstandingPayment = MarketplaceSellerOutsatndingPaymentStatus::where('id', $input['transaction_id'])->update(array('payment_status' => 2,'paid_on'=>$paidOn));
            $resultPayment = MarketplaceSellerPayments::where('user_id', $input['user_id'])->update(array('payment_status' => 7));

            $paidOn=UtilityController::Changedateformat($paidOn, 'd F, Y h:i A');

            if($resultOutstandingPayment && $resultPayment){
                $returnData = UtilityController::Generateresponse(true, 'Done', '', $paidOn); 
                \DB::commit();
                return $returnData;
            }else{
                throw new \Exception(UtilityController::Getmessage('GENERAL_ERROR'));
            }
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, $e->getMessage(), '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name :   ExportTransactionData
    //Author :          Zalak Kapadia <zalak@creolestudios.com>
    //Purpose :         To export transaction data
    //In Params :       Void
    //Return :          json
    //Date :            30th july 2018
    //###############################################################
    public function ExportTransactionData(Request $request){
        try {

            $exportValue = (!empty($request->Exportvalue) ? $request->Exportvalue : '');
            $stattus = ($request->status != '' ? $request->status : '');
            $stattus = ($request->amount != '' ? $request->amount : '');
            
            $transactionDataQuery=MarketplaceSellerOutsatndingPaymentStatus::with('user');
            
            if(isset($request->status) && $request->status!='no-status'){
                $transactionDataQuery->where('payment_status',$request->status);
            }
            if(isset($request->amount) && $request->status!='no-amount'){
                $transactionDataQuery->orderBy('total_amount',$request->amount);
            }else{
                $transactionDataQuery->orderBy('payment_status','asc')->orderBy('created_at','desc');
            }
            $transactionData=$transactionDataQuery->get()->toArray();

            $dataArray[] = ['Sr.No', 'User Name', 'Withdraw Request Date', 'Amount', 'Payment Status'];
            if (!empty($transactionData)) {
                foreach ($transactionData as $key => $value) {
                    $tempArray = array();
                    
                    if($value['payment_status']==1){
                        $value['payment_status']='Pending';
                    }else{
                        $value['payment_status']='Paid';
                    }
                    array_push($tempArray, $key+1);
                    array_push($tempArray, $value['user']['first_name']." ".$value['user']['last_name']);
                    array_push($tempArray, UtilityController::Changedateformat($value['payment_requested_on'], 'd F, Y'));
                    array_push($tempArray, $value['total_amount']);
                    array_push($tempArray, $value['payment_status']);
                    array_push($dataArray, $tempArray);
                }
            }

            if ($exportValue == 1) {
                UtilityController::ExportData($dataArray, 'Six-Clouds-Transactions', 'Six-Clouds-Transactions', 'Transaction Data', "xlsx");
            } elseif ($exportValue == 2) {
                UtilityController::ExportData($dataArray, 'Six-Clouds-Transactions', 'Six-Clouds-Transactions', 'Transaction Data', "csv");
            }
        } catch (Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name: Assignticket
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To assign customer support ticket by admin
    //In Params:     id, assignee name
    //Return:        json
    //Date:          11th Jan. 2019
    //###############################################################
    public function Assignticket(Request $request){     
        try {
            $Input = Input::all();
            \DB::beginTransaction();
                $Input['ticket_status'] = 2;
                $result = UtilityController::Makemodelobject($Input,'CustomerSupport','',$Input['ticket_id']);
                if(!empty($result)){
                        $returnData = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $result);
                        \DB::commit();
                }
                else
                    $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: Closeticket
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To close customer support ticket by someone who has rights to do so
    //In Params:     id, closed by name
    //Return:        json
    //Date:          11th Jan. 2019
    //###############################################################
    public function Closeticket(Request $request){     
        try {
            $Input = Input::all();
            \DB::beginTransaction();
                $Input['ticket_closed_at'] = Carbon::now()->timezone($Input['time_zone']);
                $Input['ticket_status'] = 3;
                $result = UtilityController::Makemodelobject($Input,'CustomerSupport','',$Input['ticket_id']);
                if(!empty($result)){
                        $returnData = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $result);
                        \DB::commit();
                }
                else
                    $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }
}

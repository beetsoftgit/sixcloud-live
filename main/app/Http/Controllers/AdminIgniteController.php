<?php

/**
 * Short description for file
 *
 * PHP version 5 and 7
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 */
/*
 * Place includes controller for login & forgot password.
 */

/**
Pre-Load all necessary library & name space
 */

namespace App\Http\Controllers;

//load required library by use
use App\Jobs\DoExpirePromoCode;
use App\Jobs\DoLivePromoCode;
use App\Jobs\TaxRateJob;
use App\Distributors;
use App\DistributorsReferrals;
use App\DistributorsTeamMembers;
use App\IgnitePrerequisite;
use App\Quizzes;
use App\VideoCategory;
use App\VideoCategoryHistory;
use App\VideoLanguageUrl;
use App\Videos;
use App\VideoViews;
use App\EventLog;
use App\QuizQuestionOptions;
use App\QuizQuestions;
use App\Worksheets;
use App\User;
use App\MarketPlacePayment;
use App\Country;
use App\PromoCode;
use App\IgniteCommissions;
use App\Zone;
use App\ZoneLanguage;
use App\Subject;
use App\ZoneVideo;
use App\ZoneQuiz;
use App\ZoneWorksheet;
use App\WorksheetLanguage;
use App\QuizLanguage;
use App\Admin;
use App\TaxRate;
use Auth;
use Carbon\carbon;
use DB;
use File;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Request;
use Hash;
use Image;
use QrCode;
use Excel;
use OSS\OssClient;
use OSS\Core\OssException;
use OSS\Core\OssUtil;

use Illuminate\Http\Response;
// use Illuminate\Http\Request;

/**
 * Photos
 * @package    AdminIgniteController
 * @subpackage Controller
 * @author     Senil Shah <senil@creolestudios.com>
 */
class AdminIgniteController extends BaseController
{

    public function __construct()
    {
        //Artisan::call('cache:clear');
    }

    public function index()
    {

    }

    //###############################################################
    //Function Name: Getallcategories
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       Get data of all the buzz categories
    //In Params:     void
    //Return:        json
    //Date:          30th Oct, 2018
    //###############################################################
    public function Getallbuzzcategories(Request $request){     
        try {
            $categories = VideoCategory::where('status',1)->whereNull('parent_id')->get();
            $returnData = UtilityController::Generateresponse(true,'GOT_DATA',1,$categories);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name : GetAllContentData
    //Author        : Ketan Solanki <ketan@creolestudios.com>
    //Purpose       : TO get the data of videos and Quizzes based on their order of listing
    //In Params     : Null
    //Return        : Json Encoded Data
    //Date          : 11th July 2018
    //###############################################################
    public function GetAllContentData(Request $request)
    {
        try {
            $Input = Input::all();    

            // $videoData = Videos::select("*", "video_ordering as Order", "slug AS VIDEO", DB::Raw('IF(video_status = 1, "PAID", IF(video_status = 2, "FREE", "")) AS video_status_name'), DB::Raw('IF(video_status = 1, "LOCKED", IF(video_status = 2, "OPEN", "")) AS video_status_locked'), DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as new_created_at"))->with('video_urls');

            $videoData = Videos::select("*", "video_ordering as Order", "slug AS VIDEO", DB::Raw('IF(video_status = 1, "PAID", IF(video_status = 2, "FREE", "")) AS video_status_name'), DB::Raw('IF(video_status = 1, "LOCKED", IF(video_status = 2, "OPEN", "")) AS video_status_locked'), DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as new_created_at"))
                        ->where("status",1)
                        ->with('video_urls')
                ->whereHas('zones', function ($query) use ($Input) {
                            $query->where('zone_id', $Input['zone']);
                });

            if(array_key_exists('subject', $Input)&&isset($Input['subject'])){
                $videoData = $videoData->where('subject_id', $Input['subject']);
            }
            if(array_key_exists('category', $Input)&&isset($Input['category'])){
                $videoData = $videoData->where('video_category_id', $Input['category']);
            }
            if(array_key_exists('subcategory', $Input)&&isset($Input['subcategory'])){
                $videoData = $videoData->where('subcategory_id', $Input['subcategory']);
            }
            $videoData = $videoData->get();

            $quizData = Quizzes::select("*", "quiz_ordering as Order", "slug AS QUIZ", DB::Raw('IF(quiz_status = 1, "PAID", IF(quiz_status = 2, "FREE", "")) AS quiz_status_name'), DB::Raw('IF(quiz_status = 1, "LOCKED", IF(quiz_status = 2, "OPEN", "")) AS quiz_status_locked'), DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as new_created_at"))->where("status", 1)
                        ->where("quiz_published_status",2)
                        // ->with('quizlanguage')
                        ->withCount('quizquestions')
                ->whereHas('zones', function ($query) use ($Input) {
                            $query->where('zone_id', $Input['zone']);
                });
            if(array_key_exists('subject', $Input)&&isset($Input['subject'])){
                $quizData = $quizData->where('subject_id', $Input['subject']);
            }
            if(array_key_exists('category', $Input)&&isset($Input['category'])){
                $quizData = $quizData->where('category_id', $Input['category']);
            }
            if(array_key_exists('subcategory', $Input)&&isset($Input['subcategory'])){
                $quizData = $quizData->where('subcategory_id', $Input['subcategory']);
            }
            $quizData = $quizData->get();

            $worksheetData = Worksheets::select("*", "worksheet_ordering as Order", "slug AS WORKSHEET",  DB::Raw('IF(status = 1, "Active", IF(status = 2, "Deleted", "inactive")) AS status'), DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as new_created_at"), DB::Raw('IF(worksheet_status = 1, "PAID", IF(worksheet_status = 2, "FREE", "")) AS worksheet_status_name'), DB::Raw('IF(worksheet_status = 1, "LOCKED", IF(worksheet_status = 2, "OPEN", "")) AS worksheet_status_locked'))
                        ->where("status",1)
                        // ->with('worksheetlanguage')
                ->whereHas('zones', function ($query) use ($Input) {
                            $query->where('zone_id', $Input['zone']);
                });
            if(array_key_exists('subject', $Input)&&isset($Input['subject'])){
                $worksheetData = $worksheetData->where('subject_id', $Input['subject']);
            }
            if(array_key_exists('category', $Input)&&isset($Input['category'])){
                $worksheetData = $worksheetData->where('worksheet_category_id', $Input['category']);
            }
            if(array_key_exists('subcategory', $Input)&&isset($Input['subcategory'])){
                $worksheetData = $worksheetData->where('subcategory_id', $Input['subcategory']);
            }
            $worksheetData = $worksheetData->get();

            $allContentData['videos_count']     = $videoData->count();
            $allContentData['quizzes_count']    = $quizData->count();
            $allContentData['worksheets_count'] = $worksheetData->count();

            $mergedArray = array_merge($videoData->toArray(), $quizData->toArray(), $worksheetData->toArray());
            $sortedArray = collect($mergedArray)->sortBy('Order')->values()->toArray();
            if (!empty($sortedArray)) {
                $allContentData['allData'] = $sortedArray;
                /*foreach ($allContentData as $key => $value) {
                    $allContentData[$key]['quizzes_count'] = count($value['quizzes']);
                    $allContentData[$key]['videos_count'] = count($value['videos']);
                    $allContentData[$key]['worksheets_count'] = count($value['worksheets']);
                    $mergedArray = array_merge($value->videos->toArray(), $value->quizzes->toArray(), $value->worksheets->toArray()); //To merge the data of Videos and Quizzes
                    $sortedArray = collect($mergedArray)->sortBy('Order')->values()->toArray(); //To sort all the array based on the Order specified
                    // $paginate = Config('constants.other.MP_DASHOARD_PAGINATE');
                    // $page = Input::get('page', 1);                    
                    // $offSet = ($page * $paginate) - $paginate;  
                    // $itemsForCurrentPage = array_slice($sortedArray, $offSet, $paginate, true);  
                    // $result = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($sortedArray), $paginate, $page);
                    // $allContentData[$key]['allData'] = $result;       
                    $allContentData[$key]['allData'] = $sortedArray;                    
                    unset($allContentData[$key]['videos']);
                    unset($allContentData[$key]['quizzes']);
                    unset($allContentData[$key]['worksheets']);
                    
                }    */            
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $allContentData);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
    //###############################################################
    //Function Name : GetAllQuizData
    //Author        : Ketan Solanki <ketan@creolestudios.com>
    //Purpose       : TO get the data of Quizzes
    //In Params     : Null
    //Return        : Json Encoded Data
    //Date          : 11th July 2018
    //###############################################################
    public function GetAllQuizData(Request $request)
    {
        try {
            $Input = Input::all();
            if (isset($Input['type']) && $Input['type'] != '') {
                $type = $Input['type'];
            }
            // if (isset($Input['categoty_id']) && $Input['categoty_id'] != '') {
            //     $categotyId = $Input['categoty_id'];
            // }
            ## New : start
            $allQuizData = ZoneQuiz::
                with([
                    'quizzes'=> function($query) use($Input){
                        $query->select("*", 
                        "quiz_ordering as Order", "slug AS QUIZ",
                        DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as new_created_at"), 
                        DB::Raw('IF(quiz_status = 1, "PAID", IF(quiz_status = 2, "FREE", "")) AS quiz_status_name'), 
                        DB::Raw('IF(quiz_status = 1, "LOCKED", IF(quiz_status = 2, "OPEN", "")) AS quiz_status_locked'))
                        ->orderBy("created_at","DESC")
                        ->where("status",'!=', 2)
                        ->withCount('quizquestions', 'quiz_attempt')
                        ->with([
                            'category' => function ($query) {
                                $query->select("id", "category_name")->get();
                            },
                            // 'quizlanguage' => function($query) {
                            //     $query->select('*');
                            // }
                        ]);
                    }
                ])
                ->whereHas('quizzes', function ($query) use ($Input) {
                            $query->where('quiz_published_status', $Input['type'])->where("status",'!=', 2)->orderBy("quiz_ordering");
                })
                ->where('zone_id',$Input['zone']);
            if(array_key_exists('subject', $Input)&&isset($Input['subject'])){
                $allQuizData = $allQuizData->whereHas('quizzes', function ($query) use ($Input) {
                        $query->where('subject_id', $Input['subject']);
                    });
            }
            if(array_key_exists('category', $Input)&&isset($Input['category'])){
                $allQuizData = $allQuizData->whereHas('quizzes', function ($query) use ($Input) {
                        $query->where('category_id', $Input['category']);
                    });
            }
            if(array_key_exists('subcategory', $Input)&&isset($Input['subcategory'])){
                $allQuizData = $allQuizData->whereHas('quizzes', function ($query) use ($Input) {
                        $query->where('subcategory_id', $Input['subcategory']);
                    });
            }
            $allQuizData = $allQuizData->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))->toArray();
            ## New : end
            /*$allQuizData = Quizzes::select("*", 
                DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as new_created_at"), 
                DB::Raw('IF(quiz_status = 1, "PAID", IF(quiz_status = 2, "FREE", "")) AS quiz_status_name'), 
                DB::Raw('IF(quiz_status = 1, "LOCKED", IF(quiz_status = 2, "OPEN", "")) AS quiz_status_locked'))
                ->with([
                    'category' => function ($query) {
                        $query->select("id", "category_name")->get();
                    },
                ])
                ->where("quiz_published_status", $type)
                ->where("category_id", $categotyId)
                ->where("status",'!=', 2)
                ->withCount('quizquestions')
                ->orderBy("created_at","DESC")
                ->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))
                ->toArray();*/
            if (!empty($allQuizData)) {
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $allQuizData);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
    //###############################################################
    //Function Name : UpdateContentOrder
    //Author        : Ketan Solanki <ketan@creolestudios.com>
    //Purpose       : To update the order of the content as per the order given from jquery ui sortable
    //In Params     : ID array and Value Array to map the order to be updated
    //Return        : Json Encoded Data
    //Date          : 13th July 2018
    //###############################################################
    public function UpdateContentOrder(Request $request)
    {
        try {
            $Input = Input::all();
            //Create an combined data array
            if (!empty($Input['idArray'])) {
                $combinedDataArray = array();
                $totalElements = count($Input['idArray']);
                for ($i = 0; $i < $totalElements; $i++) {
                    $Input['idArray'][$i] = json_decode($Input['idArray'][$i], true);
                    $combinedDataArray[$i]['order'] = $i + 1;
                    $combinedDataArray[$i]['id'] = $Input['idArray'][$i]['id'];
                    $combinedDataArray[$i]['type'] = $Input['idArray'][$i]['type'];
                }

                if (!empty($combinedDataArray)) {
                    //Update the values as received from the Jquery Ui Sortable
                    foreach ($combinedDataArray as $key => $value) {
                        if ($value['type'] == 'video') {
                            $updated = Videos::where("id", $value['id'])->update(['video_ordering' => $value['order']]);
                        } elseif ($value['type'] == 'quiz') {
                            $updated = Quizzes::where("id", $value['id'])->update(['quiz_ordering' => $value['order']]);
                        } else {
                            $updated = Worksheets::where("id", $value['id'])->update(['worksheet_ordering' => $value['order']]);
                        }
                    }
                } else {
                    $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
                }
                $returnData = UtilityController::Generateresponse(true, 'ORDER_UPDATE', 200, '');
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Getvideocategories
    //Author        : Senil Shah <senil@creolestudios.com>
    //Purpose       : TO get the categories for uploading new video
    //In Params     :
    //Return        : Json Encoded Data
    //Date          : 11th July 2018
    //###############################################################
    public function Getvideocategories(Request $request)
    {
        try {
            if (Auth::guard('admins')->user()->id) {
                $Input = Input::all();
                $categories = VideoCategory::select("id", "category_name")->with('subcategories')->whereNull('parent_id')->get();
                if (!empty($categories)) {
                    $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $categories);
                } else {
                    $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 0);
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray = UtilityController::Generateresponse(0, 'GENERAL_ERROR', '', '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Uploadnewvideo
    //Author        : Senil Shah <senil@creolestudios.com>
    //Purpose       : TO upload a new video
    //In Params     :
    //Return        : Json Encoded Data
    //Date          : 11th July 2018
    //###############################################################
    public function Uploadnewvideo(Request $request)
    {
        try {
            if (Auth::guard('admins')->user()->id) {
                $Input = Input::all();
                $Input['video_ordering'] = 1;
                if (!array_key_exists('video_for_zones', $Input) || empty($Input['video_for_zones'])) {
                    throw new \Exception('Select zone(s)');
                }

                if (!array_key_exists('subject_id', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('SUBJECT_ID_REQUIRED'));
                }

                if (!array_key_exists('video_category_id', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('VIDEO_CATEGORY_REQUIRED'));
                }
                if ($Input['subCategoriesPresent']==true && !array_key_exists('video_category_id', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('VIDEO_SUB_CATEGORY_REQUIRED'));
                }

                if (!array_key_exists('file', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('VIDEO_FILE_REQUIRED'));
                }

                /*if (!array_key_exists('video_ordering', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('VIDEO_ORDERING_REQUIRED'));
                }*/
                if (!array_key_exists('video_thumbnail', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('VIDEO_THUMBNAIL_REQUIRED'));
                }          
                $validator = UtilityController::ValidationRules($Input, 'Videos',['title','description']);             
                if (!$validator['status']) {
                    $errorMessage = explode('.', $validator['message']);
                    $responseArray = UtilityController::Generateresponse(false, $errorMessage['0'], 0);
                } else {                    
                    \DB::beginTransaction();
                    if(array_key_exists('video_thumbnail', $Input)){
                        if (!empty($Input['video_thumbnail']) && !is_string($Input['video_thumbnail'])) {
                            if ($Input['video_thumbnail']->getClientSize() <= UtilityController::Getmessage('FILE_SIZE_2')) {
                                $extension = \File::extension($Input['video_thumbnail']->getClientOriginalName());
                                if (in_array($extension, array('jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG'))) {
                                    $imageName       = date("dmYHis").rand();
                                    $Input['video_thumbnail']->move(public_path('') . UtilityController::Getpath('VIDEO_THUMBNAIL_PATH'), $imageName);
                                    $path = public_path('').UtilityController::Getpath('VIDEO_THUMBNAIL_PATH').$imageName;

                                    $DestinationPathWeb = public_path().UtilityController::Getpath('VIDEO_THUMBNAIL_WEB_PATH');
                                    $imageNameWeb       = 'web_'.$imageName;
                                    $DestinationPath16090 = public_path().UtilityController::Getpath('VIDEO_THUMBNAIL_MOBILE_160_90_PATH');
                                    $imageName16090       = 'mobile16090_'.$imageName;
                                    $DestinationPath12872 = public_path().UtilityController::Getpath('VIDEO_THUMBNAIL_MOBILE_128_72_PATH');
                                    $imageName12872       = 'mobile12872_'.$imageName;

                                    $bucket    = UtilityController::Getpath('BUCKET_VIDEO_THUMBNAIL');
                                    $ossClient = new OssClient(UtilityController::Getmessage('ACCESS_KEY_ID'), UtilityController::Getmessage('ACCESS_KEY_SECRET'), UtilityController::Getmessage('END_POINT'));

                                    /* for web : 640pxX360px */
                                    // Image::make($path)->resize(640, 360, function ($constraint) {
                                    // // $constraint->aspectRatio();
                                    // $constraint->upsize();
                                    // })->save($DestinationPathWeb . $imageNameWeb);
                                    Copy($path, $DestinationPathWeb . $imageNameWeb);
                                    chmod($DestinationPathWeb.$imageNameWeb, 0777);
                                    $object[]    = UtilityController::Getpath('BUCKET_VIDEO_THUMBNAIL_FOLDER').'thumbnail_web/'.$imageNameWeb;
                                    $filePath[]  = $DestinationPathWeb.$imageNameWeb;

                                    /* for mobile : 160pxX90px */
                                    // Image::make($path)->resize(320, 180, function ($constraint) {
                                    // // $constraint->aspectRatio();
                                    // $constraint->upsize();
                                    // })->save($DestinationPath16090 . $imageName16090);
                                    Copy($path, $DestinationPath16090 . $imageName16090);
                                    chmod($DestinationPath16090.$imageName16090, 0777);
                                    $object[]    = UtilityController::Getpath('BUCKET_VIDEO_THUMBNAIL_FOLDER').'thumbnail_mobile_160_90/'.$imageName16090;
                                    $filePath[]  = $DestinationPath16090.$imageName16090;

                                    /* for mobile : 128pxX72px */
                                    // Image::make($path)->resize(256, 144, function ($constraint) {
                                    // // $constraint->aspectRatio();
                                    // $constraint->upsize();
                                    // })->save($DestinationPath12872 . $imageName12872);
                                    Copy($path, $DestinationPath12872 . $imageName12872);
                                    chmod($DestinationPath12872.$imageName12872, 0777);
                                    $object[]    = UtilityController::Getpath('BUCKET_VIDEO_THUMBNAIL_FOLDER').'thumbnail_mobile_128_72/'.$imageName12872;
                                    $filePath[]  = $DestinationPath12872.$imageName12872;

                                    if(UtilityController::Getmessage('CURRENT_DOMAIN')=='net'){
                                        foreach ($object as $key => $value) {
                                            $ossClient->uploadFile($bucket, $object[$key], $filePath[$key]);
                                        }
                                        unlink($DestinationPathWeb.$imageNameWeb);
                                        unlink($DestinationPath16090.$imageName16090);
                                        unlink($DestinationPath12872.$imageName12872);
                                    }

                                    $Input['thumbnail_160_90'] = $imageName16090;
                                    $Input['thumbnail_128_72'] = $imageName12872;
                                    $Input['thumbnail_web']    = $imageNameWeb;
                                    unlink($path);
                                } else {
                                    throw new \Exception(UtilityController::Getmessage('ONLY_JPG_PNG_JPEG'));
                                }
                            } else {
                                throw new \Exception($Input['video_thumbnail']->getClientOriginalName() . UtilityController::Getmessage('THUMBNAIL_SIZE_LARGE'));
                            }
                        }
                    }
                    $result = UtilityController::Makemodelobject($Input, 'Videos');
                    if(array_key_exists('file', $Input)){
                        $Input['video_id'] = $result['id'];
                        $videoDetails = self::UploadvideotoOss($Input);
                        if(!empty($videoDetails) && !array_key_exists('status', $videoDetails))
                            $VideoDetailsResult = VideoLanguageUrl::insert($videoDetails);
                        else{
                            throw new \Exception($videoDetails['message']);
                        }
                    }
                    /*foreach ($Input['file'] as $key => $value) {                          
                        if (!empty($value['video'])) {
                            $fileName = date("dmYHis") . rand();
                            $file_original_name = $value['video']->getClientOriginalName();
                            $extension = \File::extension($value['video']->getClientOriginalName());
                            if (in_array($extension, array('mp4', 'MP4', 'avi', 'AVI', 'mov', 'MOV'))) {

                                $video_size = $value['video']->getClientSize();

                                $filepath = '/var/www/html/mts/videos';
                                $value['video']->move(($filepath), $fileName);
                                chmod($filepath . '/' . $fileName, 0777);
                                $data1['video_file'] = $fileName;
                                if (Request::server('SERVER_NAME') == 'sixclouds.net' || Request::server('SERVER_NAME') == 'sixclouds.cn') {
                                    $httpServer = "https://";
                                } else {
                                    $httpServer = "http://";
                                }
                                $whatfolder = explode('/', url(''));
                                $data1['yesBeta'] = in_array('beta', $whatfolder)?1:0;

                                $curl = curl_init();
                                curl_setopt_array($curl, array(
                                    CURLOPT_URL => $httpServer . Request::server('SERVER_NAME') . "/mts/main.php",
                                    CURLOPT_RETURNTRANSFER => true,
                                    CURLOPT_ENCODING => "",
                                    CURLOPT_MAXREDIRS => 10,
                                    CURLOPT_TIMEOUT => 30000,
                                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                    CURLOPT_CUSTOMREQUEST => "POST",
                                    CURLOPT_POSTFIELDS => $data1,
                                ));
                                $response = curl_exec($curl);
                                $response = json_decode(trim($response), true);
                                $duration = $response[2];
                                $url      = $response[1];
                                $urlmp4   = $response[0];
                                $duration = date("H:i:s", (int) $duration);
                                $err      = curl_error($curl);
                                curl_close($curl);
                                if ($err) {
                                    throw new \Exception($err);
                                } else {
                                    $videoDetails[$key]['video_id']           = $result['id'];
                                    $videoDetails[$key]['title']              = $value['video_title'];
                                    $videoDetails[$key]['description']        = $value['video_description'];
                                    $videoDetails[$key]['video_oss_url']      = $url;
                                    $videoDetails[$key]['video_oss_url_mp4']  = $urlmp4;
                                    $videoDetails[$key]['video_language']     = $value['language'];
                                    $videoDetails[$key]['file_original_name'] = $file_original_name;
                                    $videoDetails[$key]['video_size']         = $video_size;
                                    $videoDetails[$key]['video_duration']     = $duration;
                                    $videoDetails[$key]['created_at']         = Carbon::now();
                                    $videoDetails[$key]['updated_at']         = Carbon::now();

                                    $validator = UtilityController::ValidationRules($videoDetails[$key], 'VideoLanguageUrl');
                                    if (!$validator['status']) {
                                        $errorMessage = explode('.', $validator['message']);
                                        throw new \Exception($errorMessage['0']);
                                    } else {
                                        continue;
                                    }
                                }
                            } else {
                                throw new \Exception(UtilityController::Getmessage('VIDEO_FORMAT_VALIDATION'));
                            }
                        } else {
                            continue;
                        }
                    }*/
                    if (!empty($result)) {
                        // $VideoDetailsResult = VideoLanguageUrl::insert($videoDetails);
                        $slug['slug'] = str_slug($result['title'], '-');
                        $slug['slug'] = $slug['slug'] . ":" . base64_encode($result['id']);
                        $resultslug = UtilityController::Makemodelobject($slug, 'Videos', '', $result['id']);
                        foreach ($Input['video_for_zones'] as $key => $value) {
                            $pivotEntry[$key]['zone_id'] = $value;
                            $pivotEntry[$key]['video_id'] = $result['id'];
                            $pivotEntry[$key]['created_at'] = Carbon::now();
                            $pivotEntry[$key]['updated_at'] = Carbon::now();
                        }
                        ZoneVideo::insert($pivotEntry);
                        /*if (array_key_exists('prerequisites', $Input)) {
                            $Input['prerequisites'] = array_unique($Input['prerequisites']);
                            $Input['prerequisites'] = array_filter($Input['prerequisites']);
                            if (!empty($Input['prerequisites'])) {
                                foreach ($Input['prerequisites'] as $key => $value) {
                                    $value = json_decode($value, true);
                                    if(!empty($value)){
                                        $prerequisitesData[$key]['video_id'] = $result['id'];
                                        if(array_key_exists('VIDEO', $value)){
                                            $prerequisitesData[$key]['pre_requist_video_id'] = $value['id'];
                                        } else{
                                            $prerequisitesData[$key]['pre_requist_quiz_id'] = $value['id'];
                                        }
                                    }
                                }
                                $vidoesPreRequisites = Videos::find($result['id']);
                                $vidoesPreRequisites->pre_requisites()->createMany($prerequisitesData);
                            }
                        }*/
                        $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $result);
                        \DB::commit();
                    } else {
                        $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 0);
                    }
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            // $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), '', '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Loadprerequisites
    //Author        : Senil Shah <senil@creolestudios.com>
    //Purpose       : TO get the video list that can be pre-requisites before new video
    //In Params     :
    //Return        : Json Encoded Data
    //Date          : 12th July 2018
    //###############################################################
    public function Loadprerequisites(Request $request)
    {
        try {
            if (Auth::guard('admins')->user()->id) {
                $Input = Input::all();
                if(VideoCategory::where('parent_id',$Input['video_category_id'])->exists()){
                    $data['yesSubcategories']['subCategories'] = VideoCategory::where('parent_id',$Input['video_category_id'])->get();
                    $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $data);
                } else {
                    //Get the list of all Videos
                    // $preRequisites = Videos::select("id", "title", "video_ordering as Order", "slug AS VIDEO")->where('video_category_id', $Input['video_category_id'])->where('status', 1)->get()->toArray();
                    $preRequisites = Videos::select("id", "title", "video_ordering as Order", "slug AS VIDEO")->where(function($q) use($Input) {
                        $q->where('video_category_id', $Input['video_category_id'])->orWhere('subcategory_id', $Input['video_category_id']);
                    })->where('status', 1)->get()->toArray();
                    $videosDataCount = count($preRequisites);
                    //Get the list of all Quizzes
                    // $preRequisitesQuiz = Quizzes::select("id", "title", "quiz_ordering as Order", "slug AS QUIZ")->where('category_id', $Input['video_category_id'])->where('status', 1)->get()->toArray();
                    $preRequisitesQuiz = Quizzes::select("id", "title", "quiz_ordering as Order", "slug AS QUIZ")->where(function($q) use($Input) {
                        $q->where('category_id', $Input['video_category_id'])->orWhere('subcategory_id', $Input['video_category_id']);
                    })->where('status', 1)->get()->toArray();
                    $quizzesDataCount = count($preRequisitesQuiz);
                    $data['noSubcategories']['lastSequence'] = (int) ($videosDataCount + $quizzesDataCount + 1);
                    $mergedArray = array_merge($preRequisites, $preRequisitesQuiz);
                    $sortedArray = collect($mergedArray)->sortBy('Order')->values()->toArray();
                    $data['noSubcategories']['preRequisites'] = $sortedArray;
                    if (!empty($data)) {
                        $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $data);
                    } else {
                        $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 0);
                    }
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray = UtilityController::Generateresponse(0, 'GENERAL_ERROR', '', '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : GetQuizListing
    //Author        : Ketan Solanki <ketan@creolestudios.com>
    //Purpose       : To get the Published Quiz list
    //In Params     :
    //Return        : Json Encoded Data
    //Date          : 13th July 2018
    //###############################################################
    public function GetQuizListing(Request $request)
    {
        try {
            // $allQuizData = Quizzes::select("id", "title")->whereIn("quiz_published_status", array(1,3))->get()->toArray();
            $allQuizData = Quizzes::select("id", "title",'category_id')->where("quiz_published_status", 3)->where("status",'!=', 2)->get()->toArray();
            if (!empty($allQuizData)) {
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $allQuizData);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
    //###############################################################
    //Function Name : Uploadnewquiz
    //Author        : Ketan Solanki <ketan@creolestudios.com>
    //Purpose       : To get the Published Quiz list
    //In Params     :
    //Return        : Json Encoded Data
    //Date          : 13th July 2018
    //###############################################################
    public function Uploadnewquiz(Request $request)
    {
        try {
            if (Auth::guard('admins')->user()->id) {
                $Input = Input::all();
                $Input['quiz_ordering'] = 1;
                
                if (!array_key_exists('hidQuizID', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('QUIZ_ID_REQUIRED'));
                }
                if (!array_key_exists('video_for_zones', $Input)||empty($Input['video_for_zones'])) {
                    throw new \Exception('Select zone(s) for quiz');
                }
                if (!array_key_exists('subject_id', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('SUBJECT_ID_REQUIRED'));
                }
                if (!array_key_exists('quiz_category_id', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('QUIZ_CATEGORY_REQUIRED'));
                }
                if ($Input['subCategoriesPresent']===true) {
                    if(!array_key_exists('subcategory_id', $Input)) {
                        throw new \Exception(UtilityController::Getmessage('SELECT_SUBCATEGORY'));
                    }
                }
                /*if (!array_key_exists('quiz_ordering', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('QUIZ_ORDERING_REQUIRED'));
                }*/
                /*if (!array_key_exists('quiz_description', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('QUIZ_DESCRIPTION_REQUIRED'));
                }
                else{
                    if ($Input['quiz_description'] == '') {
                        throw new \Exception(UtilityController::Getmessage('QUIZ_DESCRIPTION_REQUIRED'));
                    }   
                }*/
                $quizData = Quizzes::find($Input['hidQuizID']);
                if (!empty($quizData)) {
                    \DB::beginTransaction();
                    $quizData->subject_id = $Input['subject_id'];
                    $quizData->category_id = $Input['quiz_category_id'];
                    if(array_key_exists('subcategory_id', $Input) && $Input['subcategory_id']!='' && isset($Input['subcategory_id']))
                        $quizData->subcategory_id = $Input['subcategory_id'];
                    // $quizData->description = $Input['quiz_description'];
                    $quizData->quiz_status = $Input['quiz_status'];
                    $quizData->attempts_per_user = $Input['attempts_per_user'];
                    $quizData->quiz_ordering = $Input['quiz_ordering'];
                    $quizData->quiz_published_status = 2;
                    $quizData->title = $Input['title'];
                    $quizData->description = $Input['description'];
                    $quizData->title_chi = $Input['title_chi'];
                    $quizData->description_chi = $Input['description_chi'];
                    $quizData->title_ru = $Input['title_ru'];
                    $quizData->description_ru = $Input['description_ru'];
                    $quizData->updated_at = Carbon::now();
                    $quizData->save();
                    /*if (array_key_exists('preRequisitesQuizes', $Input)) {
                        $Input['preRequisitesQuizes'] = array_unique($Input['preRequisitesQuizes']);
                        $Input['preRequisitesQuizes'] = array_filter($Input['preRequisitesQuizes']);
                        if (!empty($Input['preRequisitesQuizes'])) {
                            foreach ($Input['preRequisitesQuizes'] as $key => $value) {
                                $value = explode("|", $value);
                                $ignitePrerequisiteObject = new IgnitePrerequisite;
                                $ignitePrerequisiteObject->quiz_id = $Input['hidQuizID'];
                                if ($value[1] == 'Video') {
                                    $ignitePrerequisiteObject->pre_requist_video_id = $value[0];
                                } elseif ($value[1] == 'Quiz') {
                                    $ignitePrerequisiteObject->pre_requist_quiz_id = $value[0];
                                }
                                $ignitePrerequisiteObject->save();
                            }
                        }
                        $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, "");
                    }*/

                    // foreach ($Input['details'] as $key => $value) {
                    //     $languageEntry[$key]['quiz_id'] = $quizData['id'];
                    //     $languageEntry[$key]['language'] = $value['language'];
                    //     $languageEntry[$key]['title'] = $value['title'];
                    //     $languageEntry[$key]['description'] = $value['description'];
                    //     $languageEntry[$key]['slug'] = UtilityController::Makeslug($value['title']).":".base64_encode($quizData['id']);
                    //     $languageEntry[$key]['created_at'] = Carbon::now();
                    //     $languageEntry[$key]['updated_at'] = Carbon::now();
                    // }
                    // QuizLanguage::insert($languageEntry);
                    foreach ($Input['video_for_zones'] as $key => $value) {
                        $pivotEntry[$key]['zone_id'] = $value;
                        $pivotEntry[$key]['quiz_id'] = $Input['hidQuizID'];
                        $pivotEntry[$key]['created_at'] = Carbon::now();
                        $pivotEntry[$key]['updated_at'] = Carbon::now();
                    }
                    ZoneQuiz::insert($pivotEntry);
                    $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1);
                    \DB::commit();
                } else {
                    throw new \Exception(UtilityController::Getmessage('QUIZ_ID_REQUIRED'));
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), '', '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : GetallDistributorData
    //Author : Ketan Solanki <ketan@creolestudios.com>
    //Purpose : To get all distributor data
    //In Params : Void
    //Return : json
    //Date : 18th July 2018
    //###############################################################
    public function GetallDistributorData(Request $request)
    {
        try
        {
            $input = Input::all();
            $searchString = ($input['search'] != '' ? $input['search'] : '');
            $sort = ($input['sort'] != '' ? $input['sort'] : 'asc');
            $allDistributorData = Distributors::select("*", DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as joining_date"))
                ->whereIn("status", [1,0])
                ->with('country', 'state', 'city')
                ->with([
                    'distributor_referrals' => function ($query) {
                        $query->select("*", 'distributor_referrals.created_at')->orderBy("distributor_referrals.id", "DESC")->first();
                    },
                ])
                ->withCount('distributor_team_members', 'distributor_referrals');
            if (isset($searchString) && $searchString != ''):
                $allDistributorData = $allDistributorData
                    ->where('company_name', 'like', '%' . $searchString . '%')
                    // ->orWhere('last_name', 'like', '%' . $searchString . '%')
                    ->orWhere('email_address', 'like', '%' . $searchString . '%');
            endif;
            if (isset($sort) && $sort != "") {
                if ($sort == 'referralAsc') {
                    $allDistributorData = $allDistributorData->orderby('distributor_referrals_count', "ASC");
                } elseif ($sort == 'referralDesc') {
                    $allDistributorData = $allDistributorData->orderby('distributor_referrals_count', "DESC");
                } elseif ($sort == 'teamAsc') {
                    $allDistributorData = $allDistributorData->orderby('distributor_team_members_count', "ASC");
                } elseif ($sort == 'teamDsc') {
                    $allDistributorData = $allDistributorData->orderby('distributor_team_members_count', "DESC");
                }
            }
            $allDistributorData = $allDistributorData->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))
                ->toArray();
            if (!empty($allDistributorData['data'])) {
                foreach ($allDistributorData['data'] as $key => $value) {
                    if (!empty($value['distributor_referrals'])) {
                        $allDistributorData['data'][$key]['last_referal_time'] = Carbon::createFromFormat('Y-m-d H:i:s', $value['distributor_referrals'][0]['created_at'])->diffForHumans();
                    } else {
                        $allDistributorData['data'][$key]['last_referal_time'] = "---";
                    }
                }
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $allDistributorData);
            } else {

                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : GetDistributorDetailsData
    //Author : Ketan Solanki <ketan@creolestudios.com>
    //Purpose : To get all distributor details data
    //In Params : Slug
    //Return : json
    //Date : 19th July 2018
    //###############################################################
    public function GetDistributorDetailsData(Request $request)
    {
        try
        {
            $input = Input::all();
            $slug = ($input['slug'] != '' ? $input['slug'] : '');
            if (isset($slug) && $slug != "") {
                $distributorData = Distributors::select("*", DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as joining_date"))
                    ->with('country', 'state', 'city', 'distributor_referrals', 'distributor_team_members')
                    ->withCount('distributor_team_members', 'distributor_referrals')
                    ->where("slug", $slug)
                    ->first()
                    ->toArray();
                if (!empty($distributorData)) {
                    if(isset($distributorData['qrcode_file_name']) && $distributorData['qrcode_file_name'] != ""){
                        $distributorData['qrcode_file_path'] = url('')."/public/uploads/distributor_qr_codes/".$distributorData['qrcode_file_name'];
                    }else{
                        $distributorData['qrcode_file_path'] = "";
                    }                     
                    $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $distributorData);
                } else {
                    $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
                }
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }

    }
    //###############################################################
    //Function Name : GetDistributorMembers
    //Author : Ketan Solanki <ketan@creolestudios.com>
    //Purpose : To get all distributor Members data
    //In Params : Void
    //Return : json
    //Date : 18th July 2018
    //###############################################################
    public function GetDistributorMembers(Request $request)
    {
        try
        {
            $input = Input::all();
            $distributorId = (isset($input['distributorId']) && is_numeric($input['distributorId']) && $input['distributorId'] > 0 ? $input['distributorId'] : '');
            if (isset($distributorId) && $distributorId != "") {
                $allMembersData = DistributorsTeamMembers::
                    select("*", DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as joining_date"))
                    ->where("distributor_id", $distributorId)->whereIn('status',[1,3,4,5])
                    ->withCount('distributor_referrals', 'distributor_referrals_text', 'distributor_referrals_barcode')
                    ->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))
                    ->toArray();
                if (!empty($allMembersData['data'])) {
                    $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $allMembersData);
                } else {
                    $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
                }
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
    //###############################################################
    //Function Name : GetDistributorReferalls
    //Author : Ketan Solanki <ketan@creolestudios.com>
    //Purpose : To get all distributor Referalls data
    //In Params : Void
    //Return : json
    //Date : 18th July 2018
    //###############################################################
    public function GetDistributorReferalls(Request $request)
    {
        try
        {
            $input = Input::all();
            $distributorId = (isset($input['distributorId']) && is_numeric($input['distributorId']) && $input['distributorId'] > 0 ? $input['distributorId'] : '');
            if (isset($distributorId) && $distributorId != "") {
                $allMembersData = DistributorsReferrals::
                    select("*", DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as joining_date"))
                    ->with('distributor_member','referal_user')
                    ->where('distributors_id',$distributorId)
                    ->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))
                    ->toArray();
                if (!empty($allMembersData['data'])) {
                    $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $allMembersData);
                } else {
                    $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
                }
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : AddNewDistributor
    //Author        : Ketan Solank <ketan@creolestudios.com>
    //Purpose       : TO add a new Distributor
    //In Params     :
    //Return        : Json Encoded Data
    //Date          : 23rd July 2018
    //###############################################################
    public function AddNewDistributor(Request $request)
    {
        try {                    
            if (Auth::guard('admins')->user()->id) {
                $Input = Input::all();
                /*echo("<pre>");print_r($Input);echo("</pre>");                                           
                exit();*/
                if (!array_key_exists('company_name', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('COMPANY_NAME_REQUIRED'));
                }
                /*if (!array_key_exists('last_name', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('LAST_NAME_REQUIRED'));
                }*/
                if (!array_key_exists('email_address', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('EMAIL_ADDRESS_REQUIRED'));
                }
                if (!array_key_exists('contact', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('CONTACT_REQUIRED'));
                }
                if (!array_key_exists('commission_percentage', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('COMMISION_PERCENTAGE_REQUIRED'));
                }
                if($Input['commission_percentage'] && $Input['commission_percentage'] > 100){
                    throw new \Exception(UtilityController::Getmessage('COMMISION_PERCENTAGE_LESS_100'));   
                }
                if($Input['commission_percentage']){
                    $decimalValues = strlen(substr(strrchr($Input['commission_percentage'], "."), 1));
                    if($decimalValues > 2){
                        throw new \Exception(UtilityController::Getmessage('COMMISION_PERCENTAGE_DECIMAL_2'));   
                    }                    
                }
                if (!array_key_exists('renewal_commission_percentage', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('RENEWAL_COMMISION_PERCENTAGE_REQUIRED'));
                }
                if($Input['renewal_commission_percentage'] && $Input['renewal_commission_percentage'] > 100){
                    throw new \Exception(UtilityController::Getmessage('RENEWAL_COMMISION_PERCENTAGE_LESS_100'));   
                }
                if($Input['renewal_commission_percentage']){
                    $decimalValues = strlen(substr(strrchr($Input['renewal_commission_percentage'], "."), 1));
                    if($decimalValues > 2){
                        throw new \Exception(UtilityController::Getmessage('RENEWAL_COMMISION_PERCENTAGE_DECIMAL_2'));   
                    }                    
                }
                if (!array_key_exists('country_id', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('COUNTRY_REQUIRED'));
                }

                if (!array_key_exists('state_id', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('STATE_REQUIRED'));
                }

                if (!array_key_exists('city_id', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('CITY_REQUIRED'));
                }

                $validator = UtilityController::ValidationRules($Input, 'Distributors');
                if (!$validator['status']) {
                    $errorMessage = explode('.', $validator['message']);
                    $responseArray = UtilityController::Generateresponse(false, $errorMessage['0'], 0);
                } else {
                    \DB::beginTransaction();
                    $DistributorExists = Distributors::where('email_address', $Input['email_address'])->first();
                    if (!empty($DistributorExists)) {
                        $responseArray = UtilityController::Generateresponse(false, 'EMAIL_ALREADY_EXISTS', 0);
                    }else{
                        $Input['total_earnings']          = 0;
                    $Input['outstanding_commissions'] = 0;
                    $Input['status']                  = 1;
                    $phoneCode                        = Country::select('phonecode')->where('id',$Input['country_id'])->first();
                    $Input['phonecode']               = $phoneCode['phonecode'];
                    $password                         = str_random(10);
                    $Input['password']                = Hash::make($password);
                    $result                           = UtilityController::Makemodelobject($Input, 'Distributors');                    
                    if (!empty($result)) {
                        $slug['slug']         = UtilityController::Makeslug($result['company_name'], '-') . ':' . base64_encode($result['id']);
                        $slug['qrcode_token'] = date("YmdHis").str_random(10);
                        // $referalCode = $this->GenerateReferalCode("DS",UtilityController::Makeslug($Input['company_name']));
                        repeat:
                        $referalCode = $this->GenerateReferalCode("DS",self::Generaterandominitial());
                        if(Distributors::where('referal_code',$referalCode)->exists())
                            goto repeat;
                        else
                            $slug['referal_code'] = $referalCode;
                        //Generation of Referal Code ends here     
                        //Code to generate the Unique Link starts here
                        $backUrl                = base64_encode(date("dmYHis").rand().'_'.$slug['slug'].'_'.base64_encode(1));
                        $backUrl                = str_replace('/', '@', $backUrl);
                        $backUrlLink            = url('')."/ignite-signup/".$backUrl.'/'.base64_encode(3);
                        $backUrlQr              = url('')."/ignite-signup/".$backUrl.'/'.base64_encode(2);
                        $slug['unique_link']    = $backUrlLink;
                        $slug['unique_link_qr'] = $backUrlQr;
                        //Code to generate the Unique Link ends here 
                        $Input['User']['user_name']     = $result['company_name'];
                        $Input['User']['email_address'] = $result['email_address'];
                        $Input['User']['password']      = $password;
                        $toLogin = url('/').'/distributor';
                        if(UtilityController::Getmessage('CURRENT_DOMAIN')=='net') {
                            $Input['User']['message']       = "Here are your Login Credentials:";
                            $Input['footer_content']        = "SixClouds";
                            $Input['footer']                = "SixClouds";
                            $Input['subject']         = 'You have been added as a SixClouds Distributor';
                            $Input['content']               = "Hi <strong>".$Input['User']['user_name']."</strong>,<br/><br/>Congratulations on becoming a SixClouds Distributor! Here are the login details to your account:<br/><br/><span style='font-size: 15px;''><strong>Email Address : </strong>".$Input['User']['email_address']."</span><br/><span style='font-size: 15px;'><strong>Password : </strong>".$password."</span><br><br><a href='".$toLogin."' class='' style='color: #fff;
                                                   background-color: #009bdd;
                                                   border-color: #bd383e;
                                                   text-decoration: none;display: inline-block;
                                                   padding: 5px 10px;
                                                   margin-bottom: 0;
                                                   margin-top: 3px;
                                                   font-size: 14px;
                                                   font-weight: 400;
                                                   line-height: 1.42857143;
                                                   text-align: center;
                                                   white-space: nowrap;
                                                   vertical-align: middle;
                                                   -ms-touch-action: manipulation;
                                                   touch-action: manipulation;
                                                   cursor: pointer;
                                                   -webkit-user-select: none;
                                                   -moz-user-select: none;
                                                   -ms-user-select: none;
                                                   user-select: none;
                                                   background-image: none;
                                                   border: 1px solid transparent;
                                                   border-radius: 4px;'>Login</a><br/><br/>If you are not able to click the link successfully, try the link below or copy the link to the browser address bar.<br/>".$toLogin."<br/><br/>Happy Selling!";
                        } else {
                            $Input['User']['message'] = "请用以下的登录凭据，";
                            $Input['footer_content']  = "六云";
                            $Input['footer']          = "六云";
                            $Input['subject']         = '欢迎！您已被添加为分销商';
                            $Input['content']         = "您好 <strong>".$Input['User']['user_name']."</strong>,<br/><br/>欢迎成为六云分销商！请用以下的登录凭据，<br/><br/><span style='font-size: 15px;''><strong>电邮：</strong>".$Input['User']['email_address']."</span><br/><span style='font-size: 15px;'><strong>密码: </strong>".$password."</span><br><br><br><br><a href='".$toLogin."' class='' style='color: #fff;
                                                   background-color: #009bdd;
                                                   border-color: #bd383e;
                                                   text-decoration: none;display: inline-block;
                                                   padding: 5px 10px;
                                                   margin-bottom: 0;
                                                   margin-top: 3px;
                                                   font-size: 14px;
                                                   font-weight: 400;
                                                   line-height: 1.42857143;
                                                   text-align: center;
                                                   white-space: nowrap;
                                                   vertical-align: middle;
                                                   -ms-touch-action: manipulation;
                                                   touch-action: manipulation;
                                                   cursor: pointer;
                                                   -webkit-user-select: none;
                                                   -moz-user-select: none;
                                                   -ms-user-select: none;
                                                   user-select: none;
                                                   background-image: none;
                                                   border: 1px solid transparent;
                                                   border-radius: 4px;'>登录</a><br/><br/>如果链接不起作用，请将链接网址复制到浏览器地址栏。<br/>".$toLogin."";
                        }
                        //Code to generate the QRcode starts here                                 
                        //$qrCodeString = str_slug($Input['User']['user_name'])."*".$slug['slug']."*".$slug['qrcode_token'];
                        $fileName = date("YmdHis").str_random(10).".png";
                        QrCode::format('png')->size(500)->generate($backUrlQr, public_path().'/uploads/distributor_qr_codes/'.$fileName);
                        $filePath = public_path().'/uploads/distributor_qr_codes/'.$fileName;
                        //Code to generate the QRcode ends here                            
                        //Update Qr Code Filename and qrcode token 
                        $slug['qrcode_file_name'] = $fileName;                         
                        $resultslug           = UtilityController::Makemodelobject($slug, 'Distributors', '', $result['id']);
                        if($Input['discount_type']==0||$Input['discount_type']==1){
                            if(!array_key_exists('discount_of', $Input)&&!isset($Input['discount_of'])) {
                                throw new \Exception("Discount of is required"); 
                            }
                            $addReferaltoPromo['promo_code']      = $resultslug['referal_code'];
                            $addReferaltoPromo['distributor_id']  = $resultslug['id'];
                            $addReferaltoPromo['discount_type']   = $Input['discount_type'];
                            $addReferaltoPromo['promo_type']      = 0;
                            $addReferaltoPromo['discount_of']     = $Input['discount_of'];
                            $addReferaltoPromo['redemption_type'] = 1;
                            $addReferaltoPromo['status']          = 1;
                            $addReferaltoPromo['code_for']        = 1;
                            $addReferaltoPromoResult = UtilityController::Makemodelobject($addReferaltoPromo,'PromoCode');
                        }
                        if (isset($Input['User']['email_address']) && $Input['User']['email_address'] != "") {
                            Mail::send('emails.email_template', $Input, function ($message) use ($Input, $filePath) {
                                $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                                $message->to($Input['User']['email_address']);
                                $message->subject($Input['subject']);
                                // $message->attach($filePath);
                            });
                        }                                                        
                        //Code to send the Login credentials to the Distributor ends here
                        $responseArray    = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $result);
                        \DB::commit();
                    } else {
                        $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 0);
                    }           
                    }
                    
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), '', '');
        }
        return response()->json($responseArray);
    }


    //###############################################################
    //Function Name : CheckReferalCode
    //Author        : Ketan Solank <ketan@creolestudios.com>
    //Purpose       : TO check the Referal Code Already Present or not.
    //In Params     :
    //Return        : Json Encoded Data
    //Date          : 20th August 2018
    //###############################################################
    public function CheckReferalCode($referalCode)
    {
        try {                          
            $data = Distributors::select("*")->where('referal_code',$referalCode)->get()->toArray();
            if(!empty($data)){
                return false;
            }else{
                return true;
            }            
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), '', '');
        }
        return response()->json($responseArray); 
    }
    //###############################################################
    //Function Name : GenerateReferalCode
    //Author        : Ketan Solank <ketan@creolestudios.com>
    //Purpose       : TO Generate teh Referal Code 
    //In Params     :
    //Return        : Json Encoded Data
    //Date          : 20th August 2018
    //###############################################################
    public function GenerateReferalCode($prefix,$firstName, $lastName = '')
    {
        try {           
            $dateString = date("YmdHis");
            $randomChar = substr(str_shuffle($dateString), 0, 4);
            if($lastName != '')
                $referalCode = $prefix.strtoupper(mb_substr($firstName,0,1,'utf-8')).strtoupper(mb_substr($lastName,0,1,'utf-8')).$randomChar; 
            else
                $referalCode = $prefix.strtoupper(mb_substr($firstName,0,1,'utf-8')).$randomChar;

            $checkedReferalCode = $this->CheckReferalCode($referalCode);
            if($checkedReferalCode === true){
               return $referalCode;             
            }else{
                if($lastName != '')
                    $this->GenerateReferalCode($prefix,$firstName,$lastName);
                else
                    $this->GenerateReferalCode($prefix,$firstName);
            }            
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), '', '');
        }
        return response()->json($responseArray); 
    }

    //###############################################################
    //Function Name : CheckOutstandingCommisions
    //Author        : Ketan Solank <ketan@creolestudios.com>
    //Purpose       : TO check the outstanding commsion of the Distributor
    //In Params     :
    //Return        : Json Encoded Data
    //Date          : 23rd July 2018
    //###############################################################
    public function CheckOutstandingCommisions(Request $request)
    {
        try {
            if (Auth::guard('admins')->user()->id) {
                $Input = Input::all();
                if (!array_key_exists('distributorId', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('DISTRIBUTOR_ID_REQUIRED'));
                }
                if (isset($Input['distributorId']) && is_numeric($Input['distributorId']) && $Input['distributorId'] > 0) {
                    $distributorsData = Distributors::find($Input['distributorId']);
                    if (!empty($distributorsData)) {
                        $outstanding_commissions = $distributorsData['outstanding_commissions'];
                        if ($outstanding_commissions == 0) {
                            $responseArray = UtilityController::Generateresponse(true, 'DISTRIBUTOR_CAN_BE_SUSPENDED', 1);
                        } else {
                            $responseArray = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
                        }
                    }
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), '', '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : suspendAccount
    //Author        : Ketan Solank <ketan@creolestudios.com>
    //Purpose       : TO suspend the  Distributor Account
    //In Params     :
    //Return        : Json Encoded Data
    //Date          : 23rd July 2018
    //###############################################################
    public function suspendAccount(Request $request)
    {
        try {
            if (Auth::guard('admins')->user()->id) {
                $Input = Input::all();
                if (!array_key_exists('distributorId', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('DISTRIBUTOR_ID_REQUIRED'));
                }
                if (isset($Input['distributorId']) && is_numeric($Input['distributorId']) && $Input['distributorId'] > 0) {
                    if(!array_key_exists('suspendFor', $Input)){
                        $distributorsUpdated = Distributors::where("id", $Input['distributorId'])->update(['status' => 2]);
                        if ($distributorsUpdated) {
                            $responseArray = UtilityController::Generateresponse(true, 'DISTRIBUTOR_SUSPENDED', 1);
                        } else {
                            $responseArray = UtilityController::Generateresponse(false, 'DISTRIBUTOR_CANNOT_BE_SUSPENDED', 0);
                        }
                    } else {
                        if(Hash::check($Input['admin_password'],Auth::guard('admins')->user()->password)){
                            $distributorsUpdated = User::where("id", $Input['distributorId'])->update(['status' => $Input['status']]);
                            if ($distributorsUpdated) {
                                $responseArray = UtilityController::Generateresponse(true, 'USER_SUSPENDED', 1);
                            } else {
                                $responseArray = UtilityController::Generateresponse(false, 'USER_CANNOT_BE_SUSPENDED', 0);
                            }
                        } else {
                            $responseArray = UtilityController::Generateresponse(false, 'ADMIN_PASSWORD_INCORRECT', 0);
                        }
                    }
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), '', '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Teamaccountaction
    //Author        : Nivedita <nivedita@creolestudios.com>
    //Purpose       : TO suspend the  Distributor's team member's Account
    //In Params     :
    //Return        : Json Encoded Data
    //Date          : 20th Sept 2018
    //###############################################################
    public function Teamaccountaction(Request $request)
    {
        try {
            if (Auth::guard('admins')->user()->id) {
                $Input = Input::all();
                if (isset($Input['id']) && is_numeric($Input['id']) && $Input['id'] > 0) {
                    $action      = $Input['action_type'] == 'suspend'?3:1;
                    $message = $Input['action_type'] == 'suspend'?'TEAM_MEMBER_SUSPENDED':'TEAM_MEMBER_ACTIVATED';
                    $teamUpdated = DistributorsTeamMembers::where("id", $Input['id'])->update(['status' => $action]);
                    if ($teamUpdated) {
                        $responseArray = UtilityController::Generateresponse(true, $message, 1);
                    } else {
                        $responseArray = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
                    }
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), '', '');
        }
        return response()->json($responseArray);
    }
    //###############################################################
    //Function Name : sendEmailToDistributor
    //Author        : Ketan Solank <ketan@creolestudios.com>
    //Purpose       : TO send Email to the Distributor
    //In Params     :
    //Return        : Json Encoded Data
    //Date          : 23rd July 2018
    //###############################################################
    public function sendEmailToDistributor(Request $request)
    {
        try {
            if (Auth::guard('admins')->user()->id) {
                $Input = Input::all();
                if (!array_key_exists('hidDistributorId', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('DISTRIBUTOR_ID_REQUIRED'));
                }
                if (!array_key_exists('email_subject', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('SUBJECT_IS_REQUIRED'));
                }
                if (!array_key_exists('message', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('MESSAGE_IS_REQUIRED'));
                }
                if (isset($Input['hidDistributorId']) && is_numeric($Input['hidDistributorId']) && $Input['hidDistributorId'] > 0) {

                    $distributorsData = (array_key_exists('emailTo', $Input)?User::select('id','email_address','first_name','last_name')->where('id',$Input['hidDistributorId'])->first()->toArray():Distributors::find($Input['hidDistributorId'])->toArray());
                    if (!empty($distributorsData)) {
                        $email_address              = $distributorsData['email_address'];
                        if(UtilityController::Getmessage('CURRENT_DOMAIN')=='net') {
                            $Input['email_subject'] = 'You have a new message';
                            $Input['User']['user_name'] = array_key_exists('emailTo', $Input) ? $distributorsData['first_name'] . " " . $distributorsData['last_name'] : $distributorsData['company_name'];                                
                            $Input['User']['message']   = $Input['message'];
                            $Input['footer_content']    = "SixClouds";
                            $Input['footer']            = "SixClouds";                                                                        
                            $Input['content']           = "Hi <strong>".$Input['User']['user_name']."</strong>,<br/><br/>".$Input['User']['message'];
                        } else {
                            $Input['email_subject'] = '您有新信息';
                            $Input['User']['user_name'] = array_key_exists('emailTo', $Input)? $distributorsData['first_name'] . " " . $distributorsData['last_name'] : $distributorsData['company_name'];
                            $Input['User']['message']   = $Input['message'];
                            $Input['footer_content']    = "六云";
                            $Input['footer']            = "六云";                                                                        
                            $Input['content']           = "您好 <strong>".$Input['User']['user_name']."</strong>,<br/><br/>".$Input['User']['message'];
                        }
                                                
                        if (isset($email_address) && $email_address != "") {
                            Mail::send('emails.email_template', $Input, function ($message) use ($Input, $email_address) {
                                $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                                $message->to($email_address)->subject($Input['email_subject']);
                            });
                        }
                        $responseArray = UtilityController::Generateresponse(true, 'MESSAGE_SENT', 1);
                    }
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), '', '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Addnewquestion
    //Author        : Senil Shah <senil@creolestudios.com>
    //Purpose       : TO add question(s) for quiz
    //In Params     :
    //Return        : Json Encoded Data
    //Date          : 18th July 2018
    //###############################################################
    public function Addnewquestion(Request $request) {
        try {
            if(Auth::guard('admins')->user()->id){
                $Input = Input::all();
                if(!isset($Input['question_title'])) {
                    throw new \Exception(UtilityController::getMessage('ENTER_QUESTION'));
                } else {
                    if(!array_key_exists('correctAnswers', $Input)||empty($Input['correctAnswers'])){
                        throw new \Exception(UtilityController::getMessage('CHOOSE_CORRECT_ANSWER'));
                    } else {
                        \DB::beginTransaction();
                        if(array_key_exists('may_be_new_question', $Input)&&$Input['may_be_new_question']){
                            foreach ($Input['correctAnswers'] as $key => $value)
                                if(array_key_exists('value', $value))
                                    $correctAnswers[] = $value['value'];
                            $Input['correctAnswers'] = $correctAnswers;                            
                        }
                        if(isset($Input['question_audio'])){
                            $fileName  = date("dmYHis") . rand();
                            $extension = \File::extension($Input['question_audio']->getClientOriginalName());
                            if (in_array($extension, array('mp3', 'MP3'))) {
                                $Input['question_audio']->move(public_path('') . UtilityController::Getpath('QUIZ_OPTION_UPLOAD_PATH'), $fileName);
                                $path = public_path('') . UtilityController::Getpath('QUIZ_OPTION_UPLOAD_PATH') . $fileName;
                                chmod($path, 0777);
                                $Input['question_audio'] = $fileName;
                            } else {
                                throw new \Exception('Only mp3 file formats is allowed for question audio');
                            }
                        }
                        if(isset($Input['question_image'])){
                            $fileName  = date("dmYHis") . rand();
                            $extension = \File::extension($Input['question_image']->getClientOriginalName());
                            if (in_array($extension, array('jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG'))) {
                                $Input['question_image']->move(public_path('') . UtilityController::Getpath('QUIZ_OPTION_UPLOAD_PATH'), $fileName);
                                $path = public_path('') . UtilityController::Getpath('QUIZ_OPTION_UPLOAD_PATH') . $fileName;
                                chmod($path, 0777);
                                $Input['question_image'] = $fileName;
                            } else {
                                throw new \Exception('Only jpg, png and jpeg file formats are allowed for question image');
                            }
                        }
                        $quizQuestion = UtilityController::Makemodelobject($Input,'QuizQuestions');
                        $slug['slug'] = str_slug($quizQuestion['question_title'], '-');
                        $slug['slug'] = $slug['slug'].':'.base64_encode($quizQuestion['id']);
                        $quizQuestion = UtilityController::Makemodelobject($slug,'QuizQuestions','',$quizQuestion['id']);
                        foreach ($Input['option'] as $key => $value) {
                            if($value['type']=='text'){
                                if(isset($value['text'])){
                                    $options[$key]['options_type'] = 1;
                                    $options[$key]['options_value'] = $value['text'];
                                    if(in_array($value['order'], $Input['correctAnswers']))
                                        $options[$key]['correct_answer'] = 1;
                                    else
                                        $options[$key]['correct_answer'] = 2;
                                } else {
                                    throw new \Exception("Enter value for option ".$value['order']);
                                }
                            } else if ($value['type']=='image') {
                                if(isset($value['file'])){
                                    $options[$key]['options_type'] = 2;
                                    $fileName  = date("dmYHis") . rand();
                                    $extension = \File::extension($value['file']->getClientOriginalName());
                                    if (in_array($extension, array('jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG'))) {
                                        $value['file']->move(public_path('') . UtilityController::Getpath('QUIZ_OPTION_UPLOAD_PATH'), $fileName);
                                        $path = public_path('') . UtilityController::Getpath('QUIZ_OPTION_UPLOAD_PATH') . $fileName;
                                        chmod($path, 0777);
                                    } else {
                                        throw new \Exception('Only jpg, png and jpeg file formats are allowed for '.$value['order']);
                                    }
                                    $options[$key]['options_value'] = $fileName;
                                    if(in_array($value['order'], $Input['correctAnswers']))
                                        $options[$key]['correct_answer'] = 1;
                                    else
                                        $options[$key]['correct_answer'] = 2;
                                } else {
                                    throw new \Exception("Select image for option ".$value['order']);
                                }
                            } else {
                                if(isset($value['file'])){
                                    $options[$key]['options_type'] = 3;
                                    $fileName  = date("dmYHis") . rand();
                                    $extension = \File::extension($value['file']->getClientOriginalName());
                                    if (in_array($extension, array('mp3', 'MP3'))) {
                                        $value['file']->move(public_path('') . UtilityController::Getpath('QUIZ_OPTION_UPLOAD_PATH'), $fileName);
                                        $path = public_path('') . UtilityController::Getpath('QUIZ_OPTION_UPLOAD_PATH') . $fileName;
                                        chmod($path, 0777);
                                    } else {
                                        throw new \Exception('Only mp3 file formats is allowed for '.$value['order']);
                                    }
                                    $options[$key]['options_value'] = $fileName;
                                    if(in_array($value['order'], $Input['correctAnswers']))
                                        $options[$key]['correct_answer'] = 1;
                                    else
                                        $options[$key]['correct_answer'] = 2;
                                } else {
                                    throw new \Exception("Select audio for option ".$value['order']);
                                }
                            }
                            if(isset($value['text']) || isset($value['file'])){
                                $options[$key]['quiz_question_id'] = $quizQuestion['id'];
                                $options[$key]['created_at'] = Carbon::now();
                                $options[$key]['updated_at'] = Carbon::now();
                            }
                            $validator = UtilityController::ValidationRules($options[$key], 'QuizQuestionOptions');

                            if (!$validator['status']) {
                                $errorMessage = explode('.', $validator['message']);
                                throw new \Exception($errorMessage['0']);
                            } else {
                                continue;
                            }
                        }
                        if(!empty($options)){
                            QuizQuestionOptions::insert($options);
                            $quizQuestion['options'] = $options;
                            $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1,$quizQuestion);
                            \DB::commit();
                        } else {
                            throw new \Exception(UtilityController::getMessage('SELECT_OPTIONS'));
                            
                        }
                    }
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(),'', '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Addquiztitle
    //Author        : Senil Shah <senil@creolestudios.com>
    //Purpose       : TO add quiz title
    //In Params     :
    //Return        : Json Encoded Data
    //Date          : 23rd July 2018
    //###############################################################
    public function Addquiztitle(Request $request) {
        try {
            if(Auth::guard('admins')->user()->id){
                $Input = Input::all();
                \DB::beginTransaction();
                if(array_key_exists('what', $Input)) {
                    $result = UtilityController::Makemodelobject($Input,'Videos');
                } else {
                    if(!array_key_exists('id', $Input))
                        $result = UtilityController::Makemodelobject($Input,'Quizzes');
                    else
                        $result = UtilityController::Makemodelobject($Input,'Quizzes','',$Input['id']);
                }
                $slug['slug'] = date("dmYHis").rand().':'.base64_encode($result['id']);
                if(array_key_exists('what', $Input)){
                    $result = UtilityController::Makemodelobject($slug,'Videos','',$result['id']);
                } else {
                    // QuizLanguage::where('quiz_id',$result['id'])->forceDelete();
                    // if(array_key_exists('details', $Input)){
                    //     foreach ($Input['details'] as $key => $value) {
                    //         if($value['title']=='')
                    //             throw new \Exception('Quiz title required for '.$value['language']);
                    //         if($value['description']=='')
                    //             throw new \Exception('Quiz description required for '.$value['language']);
                    //         $languageEntry[$key]['quiz_id'] = $result['id'];
                    //         $languageEntry[$key]['language'] = $value['language'];
                    //         $languageEntry[$key]['title'] = $value['title'];
                    //         $languageEntry[$key]['description'] = $value['description'];
                    //         $languageEntry[$key]['slug'] = UtilityController::Makeslug($value['title']).":".base64_encode($result['id']);
                    //         $languageEntry[$key]['created_at'] = Carbon::now();
                    //         $languageEntry[$key]['updated_at'] = Carbon::now();
                    //     }
                    //     QuizLanguage::insert($languageEntry);
                    // }
                    if(array_key_exists('video_for_zones', $Input)){
                        ZoneQuiz::where('quiz_id',$result['id'])->forceDelete();
                        foreach ($Input['video_for_zones'] as $key => $value) {
                            $pivotEntry[$key]['zone_id'] = $value;
                            $pivotEntry[$key]['quiz_id'] = $Input['id'];
                            $pivotEntry[$key]['created_at'] = Carbon::now();
                            $pivotEntry[$key]['updated_at'] = Carbon::now();
                        }
                        ZoneQuiz::insert($pivotEntry);
                    }
                    $result = UtilityController::Makemodelobject($slug,'Quizzes','',$result['id']);
                }
                $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1,$result);
                \DB::commit();
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(),'', '');
        }
        return response()->json($responseArray);
    }
    //###############################################################
    //Function Name : Quizpublishdraft
    //Author        : Senil Shah <senil@creolestudios.com>
    //Purpose       : TO publish or draft the quiz
    //In Params     :
    //Return        : Json Encoded Data
    //Date          : 24th July 2018
    //###############################################################
    public function Quizpublishdraft(Request $request) {
        try {
            if(Auth::guard('admins')->user()->id){
                $Input = Input::all();
                \DB::beginTransaction();
                $result = UtilityController::Makemodelobject($Input,'Quizzes','',$Input['quizId']);
                // $quizStatus = ($Input['quiz_published_status']==1?'QUIZ_DRAFTED':'QUIZ_UNPUBLISHED');
                $quizStatus = ($Input['quiz_published_status']==1?'QUIZ_DRAFTED':'QUIZ_READY_TO_PUBLISH');
                $responseArray = UtilityController::Generateresponse(true, $quizStatus, 1,$result);
                \DB::commit();
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(),'', '');
        }
        return response()->json($responseArray);    
    }

    //###############################################################
    //Function Name : GetQuizDetails
    //Author        : Ketan Solank <ketan@creolestudios.com>
    //Purpose       : To get the quiz data
    //In Params     : Slug
    //Return        : Json Encoded Data
    //Date          : 24th July 2018
    //###############################################################
    public function GetQuizDetails(Request $request)
    {
        try {
            if (Auth::guard('admins')->user()->id) {
                $Input = Input::all();
                if (!array_key_exists('id', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('QUIZ_ID_REQUIRED'));
                }
                $id      = substr($Input['id'], strpos($Input['id'], ":") + 1);
                $quiz_id = base64_decode($id);                     
                if (isset($quiz_id) && is_numeric($quiz_id) && $quiz_id > 0) {                                        
                    $quizData = Quizzes::select("*", 
                    DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as new_created_at"),
                    DB::Raw("DATE_FORMAT(updated_at, '%d %M, %Y, %h:%i %p') as last_updated_at"),
                    DB::Raw('IF(quiz_status = 1, "PAID", IF(quiz_status = 2, "FREE", "")) AS quiz_status_name'), 
                    DB::Raw('IF(quiz_status = 1, "LOCKED", IF(quiz_status = 2, "OPEN", "")) AS quiz_status_locked'),
                    DB::Raw('IF(quiz_published_status = 1, "Draft", IF(quiz_published_status = 2, "Published", "Un Published")) AS quiz_publish_status_name'))
                    ->with([
                        'category' => function ($query) {
                            $query->select("id", "category_name")->get();
                        },
                        'quizquestions' => function ($query) {
                            $query->select("*")->with('quizquestionsoptions')->where('question_status',1)->orderBy('question_order','ASC')->get();
                        },
                        // 'quizlanguages' => function ($query) {
                        //     $query->select("*");
                        // },                                                
                    ])
                    ->where("id", $quiz_id)
                    ->withCount('quizquestions')
                    ->first()
                    ->toArray();                                        
                    if(!empty($quizData)){
                        // if(array_key_exists('isEdit', $Input) && $Input['isEdit']==1){
                            $zones = Zone::get()->pluck('id')->toArray();
                            $selectedZones = array_unique(ZoneQuiz::where('quiz_id',$quiz_id)->get()->pluck('zone_id')->toArray());
                            $array_intersect = array_intersect($zones, $selectedZones);
                            $quizData['zones'] = Zone::get();
                            $quizData['selectedZonesCount'] = $selectedZones;
                            $quizData['zones'] = $quizData['zones']->map(function($value, $key) use($array_intersect){
                                $value['selected'] = 0;
                                if(in_array($value['id'], $array_intersect))
                                    $value['selected'] = 1;
                                return $value;
                            });
                            $quizData['zones'] = $quizData['zones']->toArray();
                            $quizData['zones_string'] = implode(', ', Zone::whereIn('id',$quizData['selectedZonesCount'])->get()->pluck('zone_name')->toArray());
                        // }
                        $responseArray = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $quizData);
                    }else{
                        $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');                           
                    }
                } 
            }else{
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
            } catch (\Exception $e) {
                $sendExceptionMail = UtilityController::Sendexceptionmail($e);
                $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), '', '');
            }
            return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Uploadnewworksheet
    //Author        : Ketan Solanki <ketan@creolestudios.com>
    //Purpose       : TO upload a new worksheet
    //In Params     :
    //Return        : Json Encoded Data
    //Date          : 31st July 2018
    //###############################################################
    public function Uploadnewworksheet(Request $request)
    {
        try {
            if (Auth::guard('admins')->user()->id) {                                
                $Input = Input::all();
                $Input['worksheet_ordering'] = 1;
                // if (!array_key_exists('description', $Input)) {
                //     throw new \Exception(UtilityController::Getmessage('WORKSHEET_DESCRIPTION_REQUIRED'));
                // }
                if (!array_key_exists('video_for_zones', $Input)||empty($Input['video_for_zones'])) {
                    throw new \Exception('Select zone(s) for Worksheet');
                }
                // if (!array_key_exists('details', $Input)) {
                //     throw new \Exception('Enter title and description for all languages');
                // }
                if (!array_key_exists('title', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('ENGLISH_TITLE_REQUIRED'));
                }
                if (!array_key_exists('description', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('ENGLISH_DESCRIPTION_REQUIRED'));
                }
                if (!array_key_exists('title_chi', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('MANDRIN_TITLE_REQUIRED'));
                }
                if (!array_key_exists('description_chi', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('MANDRIN_DESCRIPTION_REQUIRED'));
                }
                if (!array_key_exists('subject_id', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('SUBJECT_ID_REQUIRED'));
                }
                if (!array_key_exists('worksheet_category_id', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('WORKSHEET_CATEGORY_REQUIRED'));
                }
                if ($Input['subCategoriesPresent'] == 1 && !array_key_exists('subcategory_id', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('SELECT_SUBCATEGORY'));
                }
                /*if (!array_key_exists('worksheet_ordering', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('WORKSHEET_ORDERING_REQUIRED'));
                }*/                                                
                if (!array_key_exists('worksheet_file', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('WORKSHEET_FILE_REQUIRED'));
                }
                $validator = UtilityController::ValidationRules($Input, 'Worksheets');                                
                if (!$validator['status']) {
                    $errorMessage = explode('.', $validator['message']);
                    $responseArray = UtilityController::Generateresponse(false, $errorMessage['0'], 0);
                } else {                    
                    \DB::beginTransaction();
                    if(array_key_exists('worksheet_thumbnail', $Input)){
                        if (!empty($Input['worksheet_thumbnail']) && !is_string($Input['worksheet_thumbnail'])) {
                            if ($Input['worksheet_thumbnail']->getClientSize() <= UtilityController::Getmessage('FILE_SIZE_2')) {
                                $extension = \File::extension($Input['worksheet_thumbnail']->getClientOriginalName());
                                if (in_array($extension, array('jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG'))) {
                                    $imageName       = date("dmYHis").rand();
                                    $Input['worksheet_thumbnail']->move(public_path('') . UtilityController::Getpath('WORKSHEET_THUMBNAIL_PATH'), $imageName);
                                    $path = public_path('').UtilityController::Getpath('WORKSHEET_THUMBNAIL_PATH').$imageName;
                                    $DestinationPathWeb = public_path().UtilityController::Getpath('WORKSHEET_THUMBNAIL_WEB_PATH');
                                    $imageNameWeb       = 'web_'.$imageName;
                                    $DestinationPath16090 = public_path().UtilityController::Getpath('WORKSHEET_THUMBNAIL_MOBILE_160_90_PATH');
                                    $imageName16090       = 'mobile16090_'.$imageName;
                                    $DestinationPath12872 = public_path().UtilityController::Getpath('WORKSHEET_THUMBNAIL_MOBILE_128_72_PATH');
                                    $imageName12872       = 'mobile12872_'.$imageName;

                                    $bucket    = UtilityController::Getpath('BUCKET_VIDEO_THUMBNAIL');
                                    $ossClient = new OssClient(UtilityController::Getmessage('ACCESS_KEY_ID'), UtilityController::Getmessage('ACCESS_KEY_SECRET'), UtilityController::Getmessage('END_POINT'));

                                    /* for web : 640pxX360px */
                                    // Image::make($path)->resize(640, 360, function ($constraint) {
                                    // // $constraint->aspectRatio();
                                    // $constraint->upsize();
                                    // })->save($DestinationPathWeb . $imageNameWeb);
                                    Copy($path, $DestinationPathWeb . $imageNameWeb);
                                    chmod($DestinationPathWeb.$imageNameWeb, 0777);
                                    $object[]    = UtilityController::Getpath('BUCKET_WORKSHEET_THUMBNAIL_FOLDER').'thumbnail_web/'.$imageNameWeb;
                                    $filePath[]  = $DestinationPathWeb.$imageNameWeb;

                                    /* for mobile : 160pxX90px */
                                    // Image::make($path)->resize(320, 180, function ($constraint) {
                                    // // $constraint->aspectRatio();
                                    // $constraint->upsize();
                                    // })->save($DestinationPath16090 . $imageName16090);
                                    Copy($path, $DestinationPath16090 . $imageName16090);
                                    chmod($DestinationPath16090.$imageName16090, 0777);
                                    $object[]    = UtilityController::Getpath('BUCKET_WORKSHEET_THUMBNAIL_FOLDER').'thumbnail_mobile_160_90/'.$imageName16090;
                                    $filePath[]  = $DestinationPath16090.$imageName16090;

                                    /* for mobile : 128pxX72px */
                                    // Image::make($path)->resize(256, 144, function ($constraint) {
                                    // // $constraint->aspectRatio();
                                    // $constraint->upsize();
                                    // })->save($DestinationPath12872 . $imageName12872);
                                    Copy($path, $DestinationPath12872 . $imageName12872);
                                    chmod($DestinationPath12872.$imageName12872, 0777);
                                    $object[]    = UtilityController::Getpath('BUCKET_WORKSHEET_THUMBNAIL_FOLDER').'thumbnail_mobile_128_72/'.$imageName12872;
                                    $filePath[]  = $DestinationPath12872.$imageName12872;

                                    if(UtilityController::Getmessage('CURRENT_DOMAIN')=='net'){
                                        foreach ($object as $key => $value) {
                                            $ossClient->uploadFile($bucket, $object[$key], $filePath[$key]);
                                        }
                                        unlink($DestinationPathWeb.$imageNameWeb);
                                        unlink($DestinationPath16090.$imageName16090);
                                        unlink($DestinationPath12872.$imageName12872);
                                    }

                                    $Input['thumbnail_160_90'] = $imageName16090;
                                    $Input['thumbnail_128_72'] = $imageName12872;
                                    $Input['thumbnail_web']    = $imageNameWeb;
                                    unlink($path);
                                } else {
                                    throw new \Exception(UtilityController::Getmessage('ONLY_JPG_PNG_JPEG'));
                                }
                            } else {
                                throw new \Exception($Input['video_thumbnail']->getClientOriginalName() . UtilityController::Getmessage('THUMBNAIL_SIZE_LARGE'));
                            }
                        }
                    }
                    $Input['original_file_name'] = $Input['worksheet_file']->getClientOriginalName();

                    // foreach ($Input['details'] as $key => $value) {
                    //     if($value['language'] == 'English') {
                    //         $Input['title'] = $value['title'];
                    //         $Input['description'] = $value['description'];
                    //     }else if($value['language'] == 'Mandarin') {
                    //         $Input['title_chi'] = $value['title'];
                    //         $Input['description_chi'] = $value['description'];
                    //     }
                    // }
                    $result = UtilityController::Makemodelobject($Input, 'Worksheets');
                                                                                    
                     if (!empty($result)) {
                        if (!empty($Input['worksheet_file'])) {                            
                            $extension = \File::extension($Input['worksheet_file']->getClientOriginalName());
                            if (in_array($extension, array('pdf', 'PDF'))) {
                                $fileName  = date("dmYHis") . rand().".".$extension; 
                                $Input['worksheet_file']->move(public_path('') . UtilityController::Getpath('WORKSHEET_FILE_UPLOAD_PATH'), $fileName);
                                $path = public_path('') . UtilityController::Getpath('WORKSHEET_FILE_UPLOAD_PATH') . $fileName;
                                chmod($path, 0777);                                
                            } else {
                                $fileName  = ""; 
                                throw new \Exception(UtilityController::Getmessage('WORKSHEET_FORMAT_VALIDATION'));
                            }
                        }  
                        //Update fileName and Slug 
                        // $slug                    = str_slug($Input['title'], '-');                                                
                        $updateData['slug']      = date("dmYHis") . rand() . ":" . base64_encode($result['id']);
                        $updateData['file_name'] = $fileName;                        
                        $resultslug              = UtilityController::Makemodelobject($updateData, 'Worksheets', '', $result['id']);
                        $responseArray           = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $result);

                        // foreach ($Input['details'] as $key => $value) {
                        //     if($value['title']=='')
                        //         throw new \Exception('Worksheet title required for '.$value['language']);
                        //     if($value['description']=='')
                        //         throw new \Exception('Worksheet description required for '.$value['language']);
                        //     $languageEntry[$key]['worksheet_id'] = $result['id'];
                        //     $languageEntry[$key]['language'] = $value['language'];
                        //     $languageEntry[$key]['title'] = $value['title'];
                        //     $languageEntry[$key]['description'] = $value['description'];
                        //     $languageEntry[$key]['slug'] = UtilityController::Makeslug($value['title']).":".base64_encode($result['id']);
                        //     $languageEntry[$key]['created_at'] = Carbon::now();
                        //     $languageEntry[$key]['updated_at'] = Carbon::now();
                        // }
                        // WorksheetLanguage::insert($languageEntry);

                        foreach ($Input['video_for_zones'] as $key => $value) {
                            $pivotEntry[$key]['zone_id'] = $value;
                            $pivotEntry[$key]['worksheet_id'] = $result['id'];
                            $pivotEntry[$key]['created_at'] = Carbon::now();
                            $pivotEntry[$key]['updated_at'] = Carbon::now();
                        }
                        ZoneWorksheet::insert($pivotEntry);
                      \DB::commit();
                     }else {
                        $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 0);
                    }                    
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            // $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), '', '');
        }
        return response()->json($responseArray);
    }


    //###############################################################
    //Function Name : GetVideoDetails
    //Author        : Ketan Solank <ketan@creolestudios.com>
    //Purpose       : To get the Video data
    //In Params     : Slug
    //Return        : Json Encoded Data
    //Date          : 24th July 2018
    //###############################################################
    public function GetVideoDetails(Request $request)
    {
        try {
            if (Auth::guard('admins')->user()->id) {
                $Input = Input::all();
                if (!array_key_exists('slug', $Input)&&!array_key_exists('isEdit', $Input)) {
                    throw new \Exception('Slug required');
                }
                if (!array_key_exists('id', $Input)&&array_key_exists('isEdit', $Input)) {
                    throw new \Exception('id required');
                }
                if(array_key_exists('isEdit', $Input) && $Input['isEdit']==1){
                    $id      = substr($Input['id'], strpos($Input['id'], ":") + 1);
                    $video_id = base64_decode($id);
                    if (isset($video_id) && is_numeric($video_id) && $video_id > 0) {
                        $videoData = Videos::select("*",
                            DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as new_created_at"),
                            DB::Raw('IF(video_status = 1, "PAID", IF(video_status = 2, "FREE", "")) AS video_status_name'),
                            DB::Raw('IF(video_status = 1, "LOCKED", IF(video_status = 2, "OPEN", "")) AS video_status_locked'))
                        ->with([
                            'category' => function ($query) {
                                $query->select("id", "category_name")->get();
                            },
                        ])
                        ->where("id", $video_id);
                        if(array_key_exists('isEdit', $Input) && $Input['isEdit']==1)
                            $videoData = $videoData->with('video_urls','pre_requisites');
                        else
                            $videoData = $videoData->with('video_url','pre_requisites');
                        $videoData = $videoData->first()->toArray();
                    }
                }else {
                    $videoData = VideoLanguageUrl::with([
                            'video_detail'=>function($query){
                                $query->select("*",
                            DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as new_created_at"),
                            DB::Raw('IF(video_status = 1, "PAID", IF(video_status = 2, "FREE", "")) AS video_status_name'),
                            DB::Raw('IF(video_status = 1, "LOCKED", IF(video_status = 2, "OPEN", "")) AS video_status_locked'))->with('category');
                            }
                        ])->where('slug',$Input['slug'])->first();
                }
                    if(!empty($videoData)){
                        if(array_key_exists('isEdit', $Input) && $Input['isEdit']==1){
                            $zones = Zone::get()->pluck('id')->toArray();
                            $selectedZones = array_unique(ZoneVideo::where('video_id',array_key_exists('isEdit', $Input) && $Input['isEdit']==1?$video_id:$videoData['video_id'])->get()->pluck('zone_id')->toArray());
                            $array_intersect = array_intersect($zones, $selectedZones);
                            $videoData['zones'] = Zone::get();
                            $videoData['selectedZonesCount'] = $selectedZones;
                            $videoData['zones'] = $videoData['zones']->map(function($value, $key) use($array_intersect){
                                $value['selected'] = 0;
                                if(in_array($value['id'], $array_intersect))
                                    $value['selected'] = 1;
                                return $value;
                            });
                            $videoData['zones'] = $videoData['zones']->toArray();
                        }
                        $responseArray = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $videoData);
                    }else{
                        $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
                    }
            }else{
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
            } catch (\Exception $e) {
                $sendExceptionMail = UtilityController::Sendexceptionmail($e);
                $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), '', '');
            }
            return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : PublishVideo
    //Author        : Ketan Solank <ketan@creolestudios.com>
    //Purpose       : To Publish The Video
    //In Params     : Slug
    //Return        : Json Encoded Data
    //Date          : 24th July 2018
    //###############################################################
    public function PublishVideo(Request $request)
    {
        try {
            if (Auth::guard('admins')->user()->id) {
                $Input = Input::all();                                
                if (!array_key_exists('id', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('QUIZ_ID_REQUIRED'));
                }                
                $video_id = $Input['id'];                                     
                if (isset($video_id) && is_numeric($video_id) && $video_id > 0) {                                        
                $videoData = Videos::where("id", $video_id)->update(array('status' => 1));                                    
                    if($videoData){                        
                        $responseArray = UtilityController::Generateresponse(true, 'VIDEO_PUBLISHED', 200, $videoData);
                    }else{
                        $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');                           
                    }
                } 
            }else{
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
            } catch (\Exception $e) {
                $sendExceptionMail = UtilityController::Sendexceptionmail($e);
                $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), '', '');
            }
            return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : DeleteVideo
    //Author        : Ketan Solank <ketan@creolestudios.com>
    //Purpose       : To Publish The Video
    //In Params     : Slug
    //Return        : Json Encoded Data
    //Date          : 24th July 2018
    //###############################################################
    public function DeleteVideo(Request $request)
    {
        try {
            if (Auth::guard('admins')->user()->id) {
                $Input = Input::all();                                
                if (!array_key_exists('id', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('QUIZ_ID_REQUIRED'));
                }                
                $video_id = $Input['id'];                                     
                if (isset($video_id) && is_numeric($video_id) && $video_id > 0) {                                        
                $videoData = Videos::where("id", $video_id)->update(array('status' => 2));                                    
                    if($videoData){                        
                        $responseArray = UtilityController::Generateresponse(true, 'VIDEO_DELETED', 200, $videoData);
                    }else{
                        $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');                           
                    }
                } 
            }else{
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
            } catch (\Exception $e) {
                $sendExceptionMail = UtilityController::Sendexceptionmail($e);
                $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), '', '');
            }
            return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Deletequiz
    //Author        : Senil Shah <senil@creolestudios.com>
    //Purpose       : To delete the un-published quiz
    //In Params     : id
    //Return        : Json Encoded Data
    //Date          : 19th Sept 2018
    //###############################################################
    public function Deletequiz(Request $request)
    {
        try {
            if (Auth::guard('admins')->user()->id) {
                $Input = Input::all();
                if (!array_key_exists('id', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('QUIZ_ID_REQUIRED'));
                }                
                $quizId = $Input['id'];                                     
                if (isset($quizId) && is_numeric($quizId) && $quizId > 0) {                                        
                $videoData = Quizzes::where("id", $quizId)->update(array('status' => 2));                                    
                    if($videoData){                        
                        $responseArray = UtilityController::Generateresponse(true, 'QUIZ_DELETED', 200, $videoData);
                    }else{
                        $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');                           
                    }
                } 
            }else{
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
            } catch (\Exception $e) {
                $sendExceptionMail = UtilityController::Sendexceptionmail($e);
                $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), '', '');
            }
            return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Deleteworksheet
    //Author        : Senil Shah <senil@creolestudios.com>
    //Purpose       : To delete the un-published worksheet
    //In Params     : id
    //Return        : Json Encoded Data
    //Date          : 19th Sept 2018
    //###############################################################
    public function Deleteworksheet(Request $request)
    {
        try {
            if (Auth::guard('admins')->user()->id) {
                $Input = Input::all();
                if (!array_key_exists('id', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('QUIZ_ID_REQUIRED'));
                }
                if (!array_key_exists('status', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('STATUS_REQUIRED'));
                }
                $worksheetId = $Input['id'];
                $status = $Input['status'];
                if (isset($worksheetId) && is_numeric($worksheetId) && $worksheetId > 0) {
                $videoData = Worksheets::where("id", $worksheetId)->update(array('status' => $status));                                    
                    if($videoData){
                        $returnMessage = ($status==4?'WORKSHEET_DELETED':'WORKSHEET_UNPUBLISHED');
                        $responseArray = UtilityController::Generateresponse(true, $returnMessage, 200, $videoData);
                    }else{
                        $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');                           
                    }
                } 
            }else{
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
            } catch (\Exception $e) {
                $sendExceptionMail = UtilityController::Sendexceptionmail($e);
                $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), '', '');
            }
            return response()->json($responseArray);
    }


    //###############################################################
    //Function Name : UnPublishVideo
    //Author        : Ketan Solank <ketan@creolestudios.com>
    //Purpose       : To Publish The Video
    //In Params     : Slug
    //Return        : Json Encoded Data
    //Date          : 24th July 2018
    //###############################################################
    public function UnPublishVideo(Request $request)
    {
        try {
            if (Auth::guard('admins')->user()->id) {
                $Input = Input::all();                                
                if (!array_key_exists('id', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('QUIZ_ID_REQUIRED'));
                }                
                $video_id = $Input['id'];                                     
                if (isset($video_id) && is_numeric($video_id) && $video_id > 0) {                                        
                $videoData = Videos::where("id", $video_id)->update(array('status' => 0));                                    
                    if($videoData){                        
                        $responseArray = UtilityController::Generateresponse(true, 'VIDEO_UNPUBLISHED', 200, $videoData);
                    }else{
                        $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');                           
                    }
                } 
            }else{
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
            } catch (\Exception $e) {
                $sendExceptionMail = UtilityController::Sendexceptionmail($e);
                $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), '', '');
            }
            return response()->json($responseArray);
    }
    //###############################################################
    //Function Name : UnPublishQuiz
    //Author        : Ketan Solank <ketan@creolestudios.com>
    //Purpose       : To Un Publish The Quiz
    //In Params     : Slug
    //Return        : Json Encoded Data
    //Date          : 24th July 2018
    //###############################################################
    public function UnPublishQuiz(Request $request)
    {
        try {
            if (Auth::guard('admins')->user()->id) {
                $Input = Input::all();                                                
                if (!array_key_exists('id', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('QUIZ_ID_REQUIRED'));
                }                                
                $id      = substr($Input['id'], strpos($Input['id'], ":") + 1);
                $quiz_id = base64_decode($id);                                     
                if (isset($quiz_id) && is_numeric($quiz_id) && $quiz_id > 0) {                                        
                $quizData = Quizzes::where("id", $quiz_id)->update(array('quiz_published_status' => 3));                                    
                    if($quizData){                        
                        $responseArray = UtilityController::Generateresponse(true, 'QUIZ_UNPUBLISHED', 200, $quizData);
                    }else{
                        $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');                           
                    }
                } 
            }else{
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
            } catch (\Exception $e) {
                $sendExceptionMail = UtilityController::Sendexceptionmail($e);
                $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), '', '');
            }
            return response()->json($responseArray);
    } 
    //###############################################################
    //Function Name : PublishQuiz
    //Author        : Ketan Solank <ketan@creolestudios.com>
    //Purpose       : To Un Publish The Quiz
    //In Params     : Slug
    //Return        : Json Encoded Data
    //Date          : 24th July 2018
    //###############################################################
    public function PublishQuiz(Request $request)
    {
        try {
            if (Auth::guard('admins')->user()->id) {
                $Input = Input::all();                                                
                if (!array_key_exists('id', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('QUIZ_ID_REQUIRED'));
                }                                
                $id      = substr($Input['id'], strpos($Input['id'], ":") + 1);
                $quiz_id = base64_decode($id);                                     
                if (isset($quiz_id) && is_numeric($quiz_id) && $quiz_id > 0) {                                        
                $quizData = Quizzes::where("id", $quiz_id)->update(array('quiz_published_status' => 2));                                    
                    if($quizData){                        
                        $responseArray = UtilityController::Generateresponse(true, 'QUIZ_PUBLISHED', 200, $quizData);
                    }else{
                        $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');                           
                    }
                } 
            }else{
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
            } catch (\Exception $e) {
                $sendExceptionMail = UtilityController::Sendexceptionmail($e);
                $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), '', '');
            }
            return response()->json($responseArray);
    }   

    //###############################################################
    //Function Name : GetVideoListing
    //Author        : Ketan Solanki <ketan@creolestudios.com>
    //Purpose       : To get the Un Published Video list
    //In Params     :
    //Return        : Json Encoded Data
    //Date          : 13th July 2018
    //###############################################################
    public function GetVideoListing(Request $request)
    {
        try {            
            $allVideoData = Videos::select("id", "title")->where("status", 0)->get()->toArray();
            if (!empty($allVideoData)) {
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $allVideoData);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : GetAllVideoData
    //Author        : Ketan Solanki <ketan@creolestudios.com>
    //Purpose       : TO get the data of Videos
    //In Params     : Null
    //Return        : Json Encoded Data
    //Date          : 11th July 2018
    //###############################################################
    public function GetAllVideoData(Request $request)
    {
        try {
            $Input = Input::all();
            if (isset($Input['type'])) {
                $type = $Input['type'];
            }
            $allVideoData = ZoneVideo::with([
                'videos'=> function($query) use($Input){
                    $query->select("*", 
                    "video_ordering as Order", "slug AS VIDEO",
                    DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as new_created_at"), 
                    DB::Raw('IF(video_status = 1, "PAID", IF(video_status = 2, "FREE", "")) AS video_status_name'), 
                    DB::Raw('IF(video_status = 1, "LOCKED", IF(video_status = 2, "OPEN", "")) AS video_status_locked'),
                    DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as new_created_at"))->with([
                    'category' => function ($query) {
                            $query->select("id", "category_name")->get();
                        },
                    ])->with('video_urls');
                }
            ])
            ->whereHas('videos', function ($query) use ($Input) {
                        $query->where('status', $Input['type']);
            })
            ->where('zone_id',$Input['zone']);
            if(array_key_exists('subject', $Input)&&isset($Input['subject'])){
                $allVideoData = $allVideoData->whereHas('videos', function ($query) use ($Input) {
                        $query->where('subject_id', $Input['subject']);
                    });
            }
            if(array_key_exists('category', $Input)&&isset($Input['category'])){
                $allVideoData = $allVideoData->whereHas('videos', function ($query) use ($Input) {
                        $query->where('video_category_id', $Input['category']);
                        // ->where("status", $Input['type']);
                    });
            }
            if(array_key_exists('subcategory', $Input)&&isset($Input['subcategory'])){
                $allVideoData = $allVideoData->whereHas('videos', function ($query) use ($Input) {
                        $query->where('subcategory_id', $Input['subcategory']);
                        // ->where("status", $Input['type']);
                    });
            }
            $allVideoData = $allVideoData->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))->toArray();
            // $allQuizData = Videos::select("*", 
            //     "video_ordering as Order", "slug AS VIDEO",
            //     DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as new_created_at"), 
            //     DB::Raw('IF(video_status = 1, "PAID", IF(video_status = 2, "FREE", "")) AS video_status_name'), 
            //     DB::Raw('IF(video_status = 1, "LOCKED", IF(video_status = 2, "OPEN", "")) AS video_status_locked'),
            //     DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as new_created_at"))
            //     ->with([
            //         'category' => function ($query) {
            //             $query->select("id", "category_name")->get();
            //         },
            //     ])
            //     ->where("status", $type)
            //     // ->where("video_category_id",$videoCategotyId)
            //     ->where(function($q) use($videoCategotyId){
            //         $q->where('video_category_id',$videoCategotyId)->orWhere('subcategory_id',$videoCategotyId);
            //     })
            //     // ->with('video_urls')
            //     ->with('video_url')
            //     ->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))
            //     ->toArray();
            if (!empty($allVideoData)) {
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $allVideoData);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Editquestion
    //Author        : Senil Shah <senil@creolestudios.com>
    //Purpose       : TO edit existing question(s) for quiz
    //In Params     :
    //Return        : Json Encoded Data
    //Date          : 8th August 2018
    //###############################################################
    public function Editquestion(Request $request) {
        try {
            if(Auth::guard('admins')->user()->id){
                $Input = Input::all();
                if(isset($Input['quiestionId'])){
                    if(!isset($Input['question_title'])) {
                        throw new \Exception(UtilityController::getMessage('ENTER_QUESTION'));
                    } else {
                            \DB::beginTransaction();
                            $correctAnswerExist = 0;
                            foreach ($Input['correctAnswers'] as $key => $value){
                                if(array_key_exists('value', $value)){
                                    $correctAnswerExist = 1;
                                    break;
                                }
                            }
                            if(!$correctAnswerExist){
                                throw new \Exception(UtilityController::getMessage('CHOOSE_CORRECT_ANSWER'));
                            }
                            $oldQuestionData = QuizQuestions::with('quizquestionsoptions')->where('id',$Input['quiestionId'])->first()->toArray();
                            
                            if(array_key_exists('removequestionAudio', $Input)){
                                if(file_exists(public_path('').UtilityController::Getpath('QUIZ_OPTION_UPLOAD_PATH').$oldQuestionData['question_audio']))
                                    unlink(public_path('').UtilityController::Getpath('QUIZ_OPTION_UPLOAD_PATH').$oldQuestionData['question_audio']);
                                $removequestionAudioResult = QuizQuestions::where('id',$Input['quiestionId'])->update(['question_audio'=>'']);
                            }
                            
                            if(array_key_exists('removequestionImage', $Input)){
                                if(file_exists(public_path('').UtilityController::Getpath('QUIZ_OPTION_UPLOAD_PATH').$oldQuestionData['question_image']))
                                    unlink(public_path('').UtilityController::Getpath('QUIZ_OPTION_UPLOAD_PATH').$oldQuestionData['question_image']);
                                $removequestionImageResult = QuizQuestions::where('id',$Input['quiestionId'])->update(['question_image'=>'']);
                            }

                            if(isset($Input['question_audio'])){
                                if(isset($oldQuestionData['question_audio'])&&$oldQuestionData['question_audio']!=''){
                                    if(file_exists(public_path('').UtilityController::Getpath('QUIZ_OPTION_UPLOAD_PATH').$oldQuestionData['question_audio']))
                                        unlink(public_path('').UtilityController::Getpath('QUIZ_OPTION_UPLOAD_PATH').$oldQuestionData['question_audio']);
                                }
                                $fileName  = date("dmYHis") . rand();
                                $extension = \File::extension($Input['question_audio']->getClientOriginalName());
                                if (in_array($extension, array('mp3', 'MP3'))) {
                                    $Input['question_audio']->move(public_path('') . UtilityController::Getpath('QUIZ_OPTION_UPLOAD_PATH'), $fileName);
                                    $path = public_path('') . UtilityController::Getpath('QUIZ_OPTION_UPLOAD_PATH') . $fileName;
                                    chmod($path, 0777);
                                    $Input['question_audio'] = $fileName;
                                } else {
                                    throw new \Exception('Only mp3 file formats is allowed for question audio');
                                }
                            }
                            if(isset($Input['question_image'])){
                                if(isset($oldQuestionData['question_image'])&&$oldQuestionData['question_image']!=''){
                                    if(file_exists(public_path('').UtilityController::Getpath('QUIZ_OPTION_UPLOAD_PATH').$oldQuestionData['question_image']))
                                        unlink(public_path('').UtilityController::Getpath('QUIZ_OPTION_UPLOAD_PATH').$oldQuestionData['question_image']);
                                }
                                $fileName  = date("dmYHis") . rand();
                                $extension = \File::extension($Input['question_image']->getClientOriginalName());
                                if (in_array($extension, array('jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG'))) {
                                    $Input['question_image']->move(public_path('') . UtilityController::Getpath('QUIZ_OPTION_UPLOAD_PATH'), $fileName);
                                    $path = public_path('') . UtilityController::Getpath('QUIZ_OPTION_UPLOAD_PATH') . $fileName;
                                    chmod($path, 0777);
                                    $Input['question_image'] = $fileName;
                                } else {
                                    throw new \Exception('Only jpg, png and jpeg file formats are allowed for question image');
                                }
                            }
                            $quizQuestion = UtilityController::Makemodelobject($Input,'QuizQuestions','',$Input['quiestionId']);
                            $slug['slug'] = str_slug($quizQuestion['question_title'], '-');
                            $slug['slug'] = $slug['slug'].':'.base64_encode($quizQuestion['id']);
                            $quizQuestion = UtilityController::Makemodelobject($slug,'QuizQuestions','',$quizQuestion['id']);
                            if(array_key_exists('removeOption', $Input)){
                                if(!empty($Input['removeOption'])){
                                    $unlinkoptions = QuizQuestionOptions::select('options_value')->whereIn('id',$Input['removeOption'])->whereIn('options_type',[2,3])->get();
                                    $unlinkoptions = json_decode(json_encode($unlinkoptions),true);
                                    foreach ($unlinkoptions as $key => $value)
                                        if(file_exists(public_path('').UtilityController::Getpath('QUIZ_OPTION_UPLOAD_PATH').$value['options_value']))
                                            unlink(public_path('').UtilityController::Getpath('QUIZ_OPTION_UPLOAD_PATH').$value['options_value']);
                                QuizQuestionOptions::whereIn('id',$Input['removeOption'])->forceDelete();
                                }
                            }
                            if(array_key_exists('option', $Input)){
                                foreach ($Input['option'] as $key => $value) {
                                    if($value['type']=='text'){
                                        if(isset($value['text'])){
                                            if($value['which']=="old")
                                                $remove[] = $value['id'];
                                            $options[$key]['options_type'] = 1;
                                            $options[$key]['options_value'] = $value['text'];
                                            foreach ($Input['correctAnswers'] as $keytext => $valuetext) {
                                                if($key==$keytext){
                                                    if(array_key_exists('value', $valuetext))
                                                        $options[$key]['correct_answer'] = 1;
                                                    else
                                                        $options[$key]['correct_answer'] = 2;
                                                }
                                            }
                                        } else {
                                            throw new \Exception("Enter value for option ".$value['order']);
                                        }
                                    } else if ($value['type']=='image') {
                                        if(isset($value['file'])) {
                                            $options[$key]['options_type'] = 2;
                                            $fileName  = date("dmYHis") . rand();
                                            $extension = \File::extension($value['file']->getClientOriginalName());
                                            if (in_array($extension, array('jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG'))) {
                                                $value['file']->move(public_path('') . UtilityController::Getpath('QUIZ_OPTION_UPLOAD_PATH'), $fileName);
                                                $path = public_path('') . UtilityController::Getpath('QUIZ_OPTION_UPLOAD_PATH') . $fileName;
                                                chmod($path, 0777);
                                            } else {
                                                throw new \Exception('Only jpg, png and jpeg file formats are allowed for '.$value['order']);
                                            }
                                            $options[$key]['options_value'] = $fileName;
                                            $filenamearray[] = $fileName;
                                            foreach ($Input['correctAnswers'] as $keyimage => $valueimage) {
                                                if($key==$keyimage){
                                                    if(array_key_exists('value', $valueimage))
                                                        $options[$key]['correct_answer'] = 1;
                                                    else
                                                        $options[$key]['correct_answer'] = 2;
                                                }
                                            }
                                        } else {
                                            foreach ($Input['correctAnswers'] as $keyimage => $valueimage) {
                                                if($key==$keyimage){
                                                    if(array_key_exists('value', $valueimage))
                                                        $onlyAnswerChange[] = $valueimage['value'];
                                                }
                                            }
                                            if($value['which']=='new')
                                                throw new \Exception("Select image for option ".$value['order']);
                                        }
                                    } else {
                                        if(isset($value['file'])){
                                            $options[$key]['options_type'] = 3;
                                            $fileName  = date("dmYHis") . rand();
                                            $extension = \File::extension($value['file']->getClientOriginalName());
                                            if (in_array($extension, array('mp3', 'MP3'))) {
                                                $value['file']->move(public_path('') . UtilityController::Getpath('QUIZ_OPTION_UPLOAD_PATH'), $fileName);
                                                $path = public_path('') . UtilityController::Getpath('QUIZ_OPTION_UPLOAD_PATH') . $fileName;
                                                chmod($path, 0777);
                                            } else {
                                                throw new \Exception('Only mp3 file formats is allowed for '.$value['order']);
                                            }
                                            $options[$key]['options_value'] = $fileName;
                                            $filenamearray[] = $fileName;
                                            foreach ($Input['correctAnswers'] as $keyaudio => $valueaudio) {
                                                if($key==$keyaudio){
                                                    if(array_key_exists('value', $valueaudio))
                                                        $options[$key]['correct_answer'] = 1;
                                                    else
                                                        $options[$key]['correct_answer'] = 2;
                                                }
                                            }
                                        } else {
                                            foreach ($Input['correctAnswers'] as $keyaudio => $valueaudio) {
                                                if($key==$keyaudio){
                                                    if(array_key_exists('value', $valueaudio))
                                                        $onlyAnswerChange[] = $valueaudio['value'];
                                                }
                                            }
                                            if($value['which']=='new')
                                                throw new \Exception("Select audio for option ".$value['order']);
                                        }
                                    }
                                    if( isset($value['text']) || isset($value['file'])) {
                                        $options[$key]['quiz_question_id'] = $Input['quiestionId'];
                                        $options[$key]['created_at'] = Carbon::now();
                                        $options[$key]['updated_at'] = Carbon::now();
                                    }
                                    if(!empty($options[$key])){
                                        $validator = UtilityController::ValidationRules($options[$key], 'QuizQuestionOptions',array('correct_answer'));

                                        if (!$validator['status']) {
                                            $errorMessage = explode('.', $validator['message']);
                                            throw new \Exception($errorMessage['0']);
                                        } else {
                                            continue;
                                        }
                                    } else {
                                        continue;
                                    }
                                }
                                if(!empty($options)){
                                    if(!empty($remove))
                                        QuizQuestionOptions::whereIn('id',$remove)->forceDelete();
                                    $insertResult = QuizQuestionOptions::insert($options);
                                    $quizQuestion['options'] = $options;
                                } else {
                                    throw new \Exception("SELECT_OPTIONS"); 
                                }
                            }
                            if(array_key_exists('correctAnswers', $Input)){
                                foreach ($Input['correctAnswers'] as $key => $value) {
                                    if(array_key_exists('value', $value))
                                        $onlyAnswerChange[] = $value['value'];
                                }
                                $onlyAnswerChange = array_unique($onlyAnswerChange);
                                if(!empty($filenamearray))
                                    QuizQuestionOptions::where('quiz_question_id',$oldQuestionData['id'])->where('options_type','!=',1)->whereNotIn('options_value',$filenamearray)->update(['correct_answer'=>2]);
                                else
                                    QuizQuestionOptions::where('quiz_question_id',$oldQuestionData['id'])->where('options_type','!=',1)->update(['correct_answer'=>2]);
                                QuizQuestionOptions::whereIn('id',$onlyAnswerChange)->update(['correct_answer'=>1]);
                            }
                            $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1,$quizQuestion);
                            \DB::commit();
                    }
                } else {
                    if(array_key_exists('option', $Input)){
                        if(array_key_exists('correctAnswers', $Input)){
                            foreach ($Input['correctAnswers'] as $key => $value){
                                if(array_key_exists('value', $value)){
                                    $correctAnswerExist = 1;
                                    break;
                                }
                                else
                                    $correctAnswerExist = 0;
                            }
                            if(!$correctAnswerExist){
                                throw new \Exception(UtilityController::getMessage('CHOOSE_CORRECT_ANSWER'));
                            } else {
                                $result = self::Addnewquestion($request);
                                $responseArray = $result->original;
                            }
                        } else {
                            throw new \Exception(UtilityController::getMessage('CHOOSE_CORRECT_ANSWER'));
                        }
                    } else {
                        throw new \Exception(UtilityController::getMessage('SELECT_OPTIONS'));
                    }
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(),'', '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : GetAllWorkSheetData
    //Author        : Ketan Solanki <ketan@creolestudios.com>
    //Purpose       : TO get the data of WorkSheets
    //In Params     : Null
    //Return        : Json Encoded Data
    //Date          : 13th August 2018
    //###############################################################
    public function GetAllWorkSheetData(Request $request)
    {
        try {
            $Input = Input::all();  
            if (isset($Input['type']) && $Input['type'] != '') {
                $type = $Input['type'];
            }
            // if (isset($Input['worksheet_category_id']) && $Input['worksheet_category_id'] != '') {
            //     $worksheetCategotyId = $Input['worksheet_category_id'];
            // }
            ## New : start
            $allWorkSheeetData = ZoneWorksheet::
                with([
                    'worksheet'=> function($query) use($Input){
                        $query->select("*", 
                        "worksheet_ordering as Order", "slug AS WORKSHEET",
                        DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as new_created_at"), 
                        DB::Raw('IF(worksheet_status = 1, "PAID", IF(worksheet_status = 2, "FREE", "")) AS worksheet_status_name'), 
                        DB::Raw('IF(worksheet_status = 1, "LOCKED", IF(worksheet_status = 2, "OPEN", "")) AS worksheet_status_locked'))
                        ->with([
                            'category' => function ($query) {
                                $query->select("id", "category_name")->get();
                            },
                            // 'worksheetlanguage' => function($query) {
                            //     $query->select('*');
                            // }
                        ])
                        ->where("status",'!=',4);
                    }
                ])
                ->whereHas('worksheet', function ($query) use ($Input) {
                            $query->where('status', $Input['type']);
                })
                
                ->where('zone_id',$Input['zone']);
            if(array_key_exists('subject', $Input)&&isset($Input['subject'])){
                $allWorkSheeetData = $allWorkSheeetData->whereHas('worksheet', function ($query) use ($Input) {
                        $query->where('subject_id', $Input['subject']);
                    });
            }
            if(array_key_exists('category', $Input)&&isset($Input['category'])){
                $allWorkSheeetData = $allWorkSheeetData->whereHas('worksheet', function ($query) use ($Input) {
                        $query->where('worksheet_category_id', $Input['category']);
                    });
            }
            if(array_key_exists('subcategory', $Input)&&isset($Input['subcategory'])){
                $allWorkSheeetData = $allWorkSheeetData->whereHas('worksheet', function ($query) use ($Input) {
                        $query->where('subcategory_id', $Input['subcategory']);
                    });
            }
            $allWorkSheeetData = $allWorkSheeetData->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))->toArray();
            ## New : end
            // $allWorkSheeetData = Worksheets::select("*", 
            //     "worksheet_ordering as Order",
            //     DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as new_created_at"))
            //     ->with([
            //         'category' => function ($query) {
            //             $query->select("id", "category_name")->get();
            //         },
            //     ])
            //     ->where("status", $type) 
            //     ->where("worksheet_category_id",$worksheetCategotyId)               
            //     ->where("status",'!=',4)                
            //     ->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))
            //     ->toArray();                
            if (!empty($allWorkSheeetData)) {
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $allWorkSheeetData);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
    //###############################################################
    //Function Name: DownloadWorkSheetFile
    //Author:        Ketan Solanki <ketan@creolestudios.com>
    //Purpose:       TO download the work sheet file
    //In Params:     Null
    //Return:        Json Encoded Data
    //Date:          13th August 2018
    //###############################################################
    public function DownloadWorkSheetFile(Request $request)
    {
        try {
            $Input = Input::all();
            if(isset($Input['filename']) && $Input['filename'] != ""){
               $fileName = $Input['filename'];
            }             
            $fileNameRelativePath = public_path('') . Config('constants.path.WORKSHEET_FILE_UPLOAD_PATH') . $fileName;
            if (File::exists($fileNameRelativePath)) {
                return response()->download($fileNameRelativePath,$Input['originalfilename']); //Download the file
            } 
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;           
        }
    }    
     //###############################################################
    //Function Name: GetworkSheetDetails
    //Author:        Ketan Solank <ketan@creolestudios.com>
    //Purpose:       To get the worksheet data
    //In Params:     Slug
    //Return:        Json Encoded Data
    //Date:          13th August 2018
    //###############################################################
    public function GetworkSheetDetails(Request $request)
    {
        try {
            if (Auth::guard('admins')->user()->id) {
                $Input = Input::all();         
                if (!array_key_exists('id', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('WORKSHEET_ID_REQUIRED'));
                }
                $id      = substr($Input['id'], strpos($Input['id'], ":") + 1);
                $worksheet_id = base64_decode($id);                                     
                if (isset($worksheet_id) && is_numeric($worksheet_id) && $worksheet_id > 0) {                                        
                    $workSheetData = Worksheets::select("*", 
                    DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as new_created_at"),
                    DB::Raw("DATE_FORMAT(updated_at, '%d %M, %Y, %h:%i %p') as last_updated_at"))
                    ->with([
                        'category' => function ($query) {
                            $query->select("id", "category_name")->get();
                        },
                        // 'worksheet_language' => function ($query) {
                        //     $query->select('*')->get();
                        // },
                    ])
                    ->where("id", $worksheet_id)                    
                    ->first()                    
                    ->toArray();                                                            
                    if(!empty($workSheetData)){
                        // if(array_key_exists('isEdit', $Input) && $Input['isEdit']==1){
                            $zones = Zone::get()->pluck('id')->toArray();
                            $selectedZones = array_unique(ZoneWorksheet::where('worksheet_id',$worksheet_id)->get()->pluck('zone_id')->toArray());
                            $array_intersect = array_intersect($zones, $selectedZones);
                            $workSheetData['zones'] = Zone::get();
                            $workSheetData['selectedZonesCount'] = $selectedZones;
                            $workSheetData['zones'] = $workSheetData['zones']->map(function($value, $key) use($array_intersect){
                                $value['selected'] = 0;
                                if(in_array($value['id'], $array_intersect))
                                    $value['selected'] = 1;
                                return $value;
                            });
                            $workSheetData['zones'] = $workSheetData['zones']->toArray();
                            $workSheetData['zones_string'] = implode(', ', Zone::whereIn('id',$workSheetData['selectedZonesCount'])->get()->pluck('zone_name')->toArray());
                        // }
                        $workSheetData['file_path'] = url('public/').Config('constants.path.WORKSHEET_FILE_UPLOAD_PATH').$workSheetData['file_name'];
                        $responseArray = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $workSheetData);
                    }else{
                        $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');                           
                    }
                } 
            }else{
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
            } catch (\Exception $e) {
                $sendExceptionMail = UtilityController::Sendexceptionmail($e);
                $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), '', '');
            }
            return response()->json($responseArray);
    } 

    //###############################################################
    //Function Name : EditworksheetGetworkSheetDetails
    //Author        : Ketan Solank <ketan@creolestudios.com>
    //Purpose       : To edit the worksheet data
    //In Params     : Slug
    //Return        : Json Encoded Data
    //Date          : 24th July 2018
    //###############################################################
    public function Editworksheet(Request $request)
    {
        try {
            if (Auth::guard('admins')->user()->id) {
                $Input = Input::all();
                $workSheetId = 0;
                if(isset($Input['hidWorkSheetId']) && is_numeric($Input['hidWorkSheetId']) && $Input['hidWorkSheetId'] > 0){
                     $workSheetId = $Input['hidWorkSheetId'];
                }
                // if (!array_key_exists('workSheetTitleText', $Input)) {
                //     throw new \Exception(UtilityController::Getmessage('WORKSHEET_TITLE_REQUIRED'));
                // }
                // if (!array_key_exists('description', $Input)) {
                //     throw new \Exception(UtilityController::Getmessage('WORKSHEET_DESCRIPTION_REQUIRED'));
                // }
                if (!array_key_exists('video_for_zones', $Input)||empty($Input['video_for_zones'])) {
                    throw new \Exception('Select zone(s) for Worksheet');
                }
                if (!array_key_exists('subject_id', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('SUBJECT_ID_REQUIRED'));
                }
                if ($Input['subCategoriesPresent'] == 1 && !array_key_exists('subcategory_id', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('SELECT_SUBCATEGORY'));
                }
                if (!array_key_exists('worksheet_category_id', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('WORKSHEET_CATEGORY_REQUIRED'));
                }
                /*if (!array_key_exists('worksheet_ordering', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('WORKSHEET_ORDERING_REQUIRED'));
                }*/
                // $Input['title'] = $Input['workSheetTitleText'];
                $validator = UtilityController::ValidationRules($Input, 'Worksheets','file_name');
                if (!$validator['status']) {
                    $errorMessage = explode('.', $validator['message']);
                    $responseArray = UtilityController::Generateresponse(false, $errorMessage['0'], 0);
                } else {                    
                    \DB::beginTransaction();                    
                    $result = UtilityController::Makemodelobject($Input, 'Worksheets', '', $workSheetId);                                                                
                     if (!empty($result)) {
                        if (!empty($Input['worksheet_file'])) {                            
                            $extension = \File::extension($Input['worksheet_file']->getClientOriginalName());                                    
                            if (in_array($extension, array('pdf', 'PDF'))) {
                                $fileName  = date("dmYHis") . rand().".".$extension; 
                                $updateData['original_file_name'] = $Input['worksheet_file']->getClientOriginalName();
                                $Input['worksheet_file']->move(public_path('') . UtilityController::Getpath('WORKSHEET_FILE_UPLOAD_PATH'), $fileName);
                                $path = public_path('') . UtilityController::Getpath('WORKSHEET_FILE_UPLOAD_PATH') . $fileName;
                                chmod($path, 0777);  
                                //Delete the old file
                                $fileNameOld = Worksheets::find($workSheetId);                              
                                $fileNameOld = $fileNameOld->file_name;                              
                                unlink(public_path('') . UtilityController::Getpath('WORKSHEET_FILE_UPLOAD_PATH') . $fileNameOld); 
                                //Update the File Name if the file was uploaded                               
                                $updateData['file_name'] = $fileName;
                                $resultFile              = UtilityController::Makemodelobject($updateData, 'Worksheets', '', $workSheetId);
                            } else {
                                $fileName  = ""; 
                                throw new \Exception(UtilityController::Getmessage('WORKSHEET_FORMAT_VALIDATION'));
                            }
                        }  
                        //Update Slug 
                        // $slug                    = str_slug($Input['workSheetTitleText'], '-');
                        $updateData['slug']      = date('dmYHis').rand().":".base64_encode($workSheetId);                        
                        $responseArray           = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $result);
                        // WorksheetLanguage::where('worksheet_id',$workSheetId)->forceDelete();
                        // foreach ($Input['details'] as $key => $value) {
                        //     if($value['title']=='')
                        //         throw new \Exception('Worksheet title required for '.$value['language']);
                        //     if($value['description']=='')
                        //         throw new \Exception('Worksheet description required for '.$value['language']);
                        //     $languageEntry[$key]['worksheet_id'] = $workSheetId;
                        //     $languageEntry[$key]['language'] = $value['language'];
                        //     $languageEntry[$key]['title'] = $value['title'];
                        //     $languageEntry[$key]['description'] = $value['description'];
                        //     if($value['language'] == 'English') {
                        //         $updateData['title'] = $value['title'];
                        //         $updateData['description'] = $value['description'];
                        //     }else if($value['language'] == 'Mandarin') {
                        //         $updateData['title_chi'] = $value['title'];
                        //         $updateData['description_chi'] = $value['description'];
                        //     }
                        //     $languageEntry[$key]['slug'] = UtilityController::Makeslug($value['title']).":".base64_encode($workSheetId);
                        //     $languageEntry[$key]['created_at'] = Carbon::now();
                        //     $languageEntry[$key]['updated_at'] = Carbon::now();
                        // }
                        // $resultslug              = UtilityController::Makemodelobject($updateData, 'Worksheets', '', $workSheetId);
                        // WorksheetLanguage::insert($languageEntry);

                        ZoneWorksheet::where('worksheet_id',$workSheetId)->forceDelete();
                        foreach ($Input['video_for_zones'] as $key => $value) {
                            $pivotEntry[$key]['zone_id'] = $value;
                            $pivotEntry[$key]['worksheet_id'] = $workSheetId;
                            $pivotEntry[$key]['created_at'] = Carbon::now();
                            $pivotEntry[$key]['updated_at'] = Carbon::now();
                        }
                        ZoneWorksheet::insert($pivotEntry);
                      \DB::commit();
                     }else {
                        $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 0);
                    }                    
                }                                                                               
            }else{
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            // $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), '', '');
        }
        return response()->json($responseArray);
    }  

    //###############################################################
    //Function Name: UnpublishWorkSheet
    //Author:        Ketan Solank <ketan@creolestudios.com>
    //Purpose:       To un publish the worksheet
    //In Params:     Slug
    //Return:        Json Encoded Data
    //Date:          14th August 2018
    //###############################################################
    public function UnpublishWorkSheet(Request $request)
    {
        try {
             if (Auth::guard('admins')->user()->id) {
                $Input = Input::all();                                
                $workSheetId = 0;
                if(isset($Input['WorkSheetId']) && is_numeric($Input['WorkSheetId']) && $Input['WorkSheetId'] > 0){
                    $workSheetId          = $Input['WorkSheetId'];
                    $updateData['status'] = $Input['status'];                        
                    $resultUpdate         = UtilityController::Makemodelobject($updateData, 'Worksheets', '', $workSheetId);
                    $responseArray        = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $resultUpdate);
                }else{                    
                    $responseArray = UtilityController::Generateresponse(false, 'WORKSHEET_ID_REQUIRED', 2);
                }
            }else{
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
             }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), '', '');
        }
        return response()->json($responseArray);
    }     
    //###############################################################
    //Function Name : Editvideo
    //Author        : Senil Shah <senil@creolestudios.com>
    //Purpose       : TO edit an existing video
    //In Params     :
    //Return        : Json Encoded Data
    //Date          : 13th Aug 2018
    //###############################################################
    public function Editvideo(Request $request)
    {
        try {
            if (Auth::guard('admins')->user()->id) {
                $Input = Input::all();
                $oldData = json_decode($Input['old_data'],true);
                foreach($Input['file'] as $key => $video) {
                    if( ! isset($video['video'])) {
                        unset($Input['file'][$key]);
                    }
                }
                if (!array_key_exists('video_category_id', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('VIDEO_CATEGORY_REQUIRED'));
                }

                if (!array_key_exists('video_ordering', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('VIDEO_ORDERING_REQUIRED'));
                }                
                $validator = UtilityController::ValidationRules($Input, 'Videos');
                if (!$validator['status']) {
                    $errorMessage = explode('.', $validator['message']);
                    $responseArray = UtilityController::Generateresponse(false, $errorMessage['0'], 0);
                } else {                    
                    \DB::beginTransaction();
                    // $selectedZones['selected_zones'] = $Input['video_for_zones'];
                    // $selectedZones['isEdit'] = 1;
                    // $languages = self::Getzonesforlanguage($Input);
                    // if(array_key_exists('removeVideo', $Input)){
                    //     $deleteVideos = VideoLanguageUrl::whereIn('id',$Input['removeVideo'])->where('video_id',$Input['video_id'])->forceDelete();
                    //     $videoCount = VideoLanguageUrl::where('video_id',$Input['video_id'])->count();
                    //     if($videoCount<=0 || ($videoCount<$languages->count() && !array_key_exists('file', $Input)) ){
                    //         throw new \Exception(UtilityController::Getmessage('VIDEO_FILE_REQUIRED'));
                    //     } else {
                    //         if(array_key_exists('file', $Input)){
                    //             foreach ($Input['file'] as $key => $value) {
                    //                 if($value['video_title']=='')
                    //                     throw new \Exception('Enter title of '.$value['language'].' video');
                    //                 if($value['video_description']=='')
                    //                     throw new \Exception('Enter  of description'.$value['language'].' video');
                    //                 if(!array_key_exists('video', $value))
                    //                     throw new \Exception(UtilityController::Getmessage('VIDEO_FILE_REQUIRED'). ' of '.$value['language']);
                    //             }
                    //         }
                    //     }
                    // } else {
                    //     if(count($Input['existing_videos']) < $languages->count() && !array_key_exists('file', $Input)){
                    //         throw new \Exception('Select Video files for all languages according to selected zones');
                    //     }
                    // }
                    if(array_key_exists('video_thumbnail', $Input)){
                        if (!empty($Input['video_thumbnail']) && !is_string($Input['video_thumbnail'])) {
                            if ($Input['video_thumbnail']->getClientSize() <= UtilityController::Getmessage('FILE_SIZE_2')) {
                                $extension = \File::extension($Input['video_thumbnail']->getClientOriginalName());
                                if (in_array($extension, array('jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG'))) {
                                    $imageName       = date("dmYHis").rand();
                                    $Input['video_thumbnail']->move(public_path('') . UtilityController::Getpath('VIDEO_THUMBNAIL_PATH'), $imageName);
                                    $path = public_path('').UtilityController::Getpath('VIDEO_THUMBNAIL_PATH').$imageName;

                                    $DestinationPathWeb = public_path().UtilityController::Getpath('VIDEO_THUMBNAIL_WEB_PATH');
                                    $imageNameWeb       = 'web_'.$imageName;
                                    $DestinationPath16090 = public_path().UtilityController::Getpath('VIDEO_THUMBNAIL_MOBILE_160_90_PATH');
                                    $imageName16090       = 'mobile16090_'.$imageName;
                                    $DestinationPath12872 = public_path().UtilityController::Getpath('VIDEO_THUMBNAIL_MOBILE_128_72_PATH');
                                    $imageName12872       = 'mobile12872_'.$imageName;

                                    $bucket    = UtilityController::Getpath('BUCKET_VIDEO_THUMBNAIL');
                                    $ossClient = new OssClient(UtilityController::Getmessage('ACCESS_KEY_ID'), UtilityController::Getmessage('ACCESS_KEY_SECRET'), UtilityController::Getmessage('END_POINT'));

                                    /* for web : 640pxX360px */
                                    // Image::make($path)->resize(640, 360, function ($constraint) {
                                    // // $constraint->aspectRatio();
                                    // $constraint->upsize();
                                    // })->save($DestinationPathWeb . $imageNameWeb);
                                    Copy($path, $DestinationPathWeb . $imageNameWeb);
                                    chmod($DestinationPathWeb.$imageNameWeb, 0777);
                                    $object[]    = UtilityController::Getpath('BUCKET_VIDEO_THUMBNAIL_FOLDER').'thumbnail_web/'.$imageNameWeb;
                                    $filePath[]  = $DestinationPathWeb.$imageNameWeb;

                                    /* for mobile : 160pxX90px */
                                    // Image::make($path)->resize(160, 90, function ($constraint) {
                                    // // $constraint->aspectRatio();
                                    // $constraint->upsize();
                                    // })->save($DestinationPath16090 . $imageName16090);
                                    Copy($path, $DestinationPath16090 . $imageName16090);
                                    chmod($DestinationPath16090.$imageName16090, 0777);
                                    $object[]    = UtilityController::Getpath('BUCKET_VIDEO_THUMBNAIL_FOLDER').'thumbnail_mobile_160_90/'.$imageName16090;
                                    $filePath[]  = $DestinationPath16090.$imageName16090;

                                    /* for mobile : 128pxX72px */
                                    // Image::make($path)->resize(128, 72, function ($constraint) {
                                    // // $constraint->aspectRatio();
                                    // $constraint->upsize();
                                    // })->save($DestinationPath12872 . $imageName12872);
                                    Copy($path, $DestinationPath12872 . $imageName12872);
                                    chmod($DestinationPath12872.$imageName12872, 0777);
                                    $object[]    = UtilityController::Getpath('BUCKET_VIDEO_THUMBNAIL_FOLDER').'thumbnail_mobile_128_72/'.$imageName12872;
                                    $filePath[]  = $DestinationPath12872.$imageName12872;
                                    
                                    if(UtilityController::getMessage('CURRENT_DOMAIN')=='net'){
                                        foreach ($object as $key => $value) {
                                            $ossClient->uploadFile($bucket, $object[$key], $filePath[$key]);
                                        }
                                        unlink($DestinationPathWeb.$imageNameWeb);
                                        unlink($DestinationPath16090.$imageName16090);
                                        unlink($DestinationPath12872.$imageName12872);
                                    } else {
                                        unlink($DestinationPath16090.$oldData['thumbnail_160_90']);
                                        unlink($DestinationPath12872.$oldData['thumbnail_128_72']);
                                        unlink($DestinationPathWeb.$oldData['thumbnail_web']);
                                    }

                                    $Input['thumbnail_160_90'] = $imageName16090;
                                    $Input['thumbnail_128_72'] = $imageName12872;
                                    $Input['thumbnail_web']    = $imageNameWeb;
                                    unlink($path);
                                } else {
                                    throw new \Exception(UtilityController::Getmessage('ONLY_JPG_PNG_JPEG'));
                                }
                            } else {
                                throw new \Exception($Input['video_thumbnail']->getClientOriginalName() . UtilityController::Getmessage('THUMBNAIL_SIZE_LARGE'));
                            }
                        }
                    }
                    if(VideoCategory::where('parent_id', $Input['video_category_id'])->exists()) {
                        $subcategoryId = VideoCategory::select('id')->where('parent_id', $Input['video_category_id'])->first()->toArray();

                        $Input['subcategory_id'] = (isset($Input['subcategory_id']) && $Input['subcategory_id'] != '') ? $Input['subcategory_id'] : $subcategoryId['id'];    
                    } else {
                        $Input['subcategory_id'] = '';
                    }
                    $result = UtilityController::Makemodelobject($Input, 'Videos','',$Input['video_id']);
                    if(array_key_exists('file', $Input) && ! empty($Input['file']) && ! is_null($Input['file'])){
                        $videoDetails = self::UploadvideotoOss($Input);
                        if(!empty($videoDetails) && !array_key_exists('status', $videoDetails))
                            $VideoDetailsResult = VideoLanguageUrl::insert($videoDetails);
                        else{
                            throw new \Exception($videoDetails['message']);
                        }
                    }
                    if(array_key_exists('existing_videos', $Input)){
                        foreach ($Input['existing_videos'] as $key => $value) {
                            if(array_key_exists('video', $value)){
                                $ossCall = self::Ossfunction($value);
                                $Input['existing_videos'][$key]['video_oss_url']      = $ossCall['url'];
                                $Input['existing_videos'][$key]['video_oss_url_mp4']  = $ossCall['urlmp4'];
                                $Input['existing_videos'][$key]['video_duration']     = $ossCall['duration'];
                                $Input['existing_videos'][$key]['video_language']     = $value['language'];
                                $Input['existing_videos'][$key]['file_original_name'] = $value['video']->getClientOriginalName();
                                $Input['existing_videos'][$key]['video_size']         = $value['video']->getClientSize();
                            }
                        }
                        $existingUpdate = UtilityController::Savearrayobject($Input['existing_videos'],'VideoLanguageUrl');
                    }
                    if (!empty($result)) {
                        $slug['slug'] = str_slug($result['title'], '-');
                        $slug['slug'] = $slug['slug'] . ":" . base64_encode($result['id']);
                        $resultslug = UtilityController::Makemodelobject($slug, 'Videos', '', $result['id']);
                        
                        /*if (array_key_exists('prerequisites', $Input)) {
                            $Input['prerequisites'] = array_unique($Input['prerequisites']);
                            $Input['prerequisites'] = array_filter($Input['prerequisites']);
                            if (!empty($Input['prerequisites'])) {
                                foreach ($Input['prerequisites'] as $key => $value) {
                                    $value = json_decode($value, true);
                                    if(!empty($value)){
                                        $prerequisitesData[$key]['video_id'] = $result['id'];
                                        if(array_key_exists('VIDEO', $value)){
                                            $prerequisitesData[$key]['pre_requist_video_id'] = $value['id'];
                                        } else{
                                            $prerequisitesData[$key]['pre_requist_quiz_id'] = $value['id'];
                                        }
                                    }
                                }
                                $deleteExistingPreRequisites = IgnitePrerequisite::where('video_id',$result['id'])->forceDelete();
                                $vidoesPreRequisites = Videos::find($result['id']);
                                $vidoesPreRequisites->pre_requisites()->createMany($prerequisitesData);
                            } else {
                                $deleteExistingPreRequisites = IgnitePrerequisite::where('video_id',$result['id'])->forceDelete();
                            }
                        }*/
                        if(array_key_exists('removeVideo', $Input)){
                            $deleteVideos = VideoLanguageUrl::whereIn('id',$Input['removeVideo'])->where('video_id',$result['id'])->forceDelete();
                            $videoCount = VideoLanguageUrl::where('video_id',$result['id'])->count();
                            if($videoCount<=0)
                                if(isset($VideoDetailsResult)||empty($videoDetails))
                                    throw new \Exception(UtilityController::Getmessage('VIDEO_FILE_REQUIRED'));
                        }
                        if(array_key_exists('video_for_zones', $Input)){
                            // ZoneVideo::whereNotIn('zone_id',$Input['video_for_zones'])->where('video_id',$result['id'])->forceDelete();
                            ZoneVideo::where('video_id',$result['id'])->forceDelete();
                            foreach ($Input['video_for_zones'] as $key => $value) {
                                $pivotEntry[$key]['zone_id'] = $value;
                                $pivotEntry[$key]['video_id'] = $result['id'];
                                $pivotEntry[$key]['created_at'] = Carbon::now();
                                $pivotEntry[$key]['updated_at'] = Carbon::now();
                            }
                            ZoneVideo::insert($pivotEntry);
                        }
                        $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $result);
                        \DB::commit();
                    } else {
                        $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 0);
                    }
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray = UtilityController::Generateresponse(false, $e->getMessage(), 0);
        }
        return response()->json($responseArray);
    }    

    //###############################################################
    //Function Name: UploadvideotoOss
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       eliminating repeatative code for uploading the video to Oss
    //In Params:     array list of file videos
    //Return:        json
    //Date:          5th Feb, 2019
    //###############################################################
    public function UploadvideotoOss($Input){     
        try {
            foreach ($Input['file'] as $key => $value) {
                if (!empty($value['video'])) {
                    $file_original_name = $value['video']->getClientOriginalName();
                    $extension = \File::extension($value['video']->getClientOriginalName());
                    if (in_array($extension, array('mp4', 'MP4', 'avi', 'AVI', 'mov', 'MOV'))) {
                        $video_size = $value['video']->getClientSize();
                        $ossCall = self::Ossfunction($value);
                        $videoExist = VideoLanguageUrl::where('video_id',$Input['video_id'])->where('video_language',$value['language'])->first();
                        if(!empty($videoExist))
                            VideoLanguageUrl::where('video_id',$Input['video_id'])->where('video_language',$value['language'])->forceDelete();
                        $videoDetails[$key]['video_id']           = $Input['video_id'];
                        $videoDetails[$key]['title']              = $Input['title'];
                        $videoDetails[$key]['description']        = $Input['description'];
                        $videoDetails[$key]['slug']               = rand().date("dmYHis").rand();
                        $videoDetails[$key]['video_oss_url']      = $ossCall['url'];
                        $videoDetails[$key]['video_oss_url_mp4']  = $ossCall['urlmp4'];
                        $videoDetails[$key]['video_language']     = $value['language'];

                        $languages = ZoneLanguage::select('id','language')->get()->toArray();
                        foreach ($languages as $key1 => $language) {
                            if(strcasecmp($language['language'], $value['language']) == 0) {
                                $videoDetails[$key]['video_language_id'] = $language['id'];
                                break;
                            }
                        }

                        $videoDetails[$key]['file_original_name'] = $file_original_name;
                        $videoDetails[$key]['video_size']         = $video_size;
                        $videoDetails[$key]['video_duration']     = $ossCall['duration'];
                        $videoDetails[$key]['created_at']         = Carbon::now();
                        $videoDetails[$key]['updated_at']         = Carbon::now();

                        $validator = UtilityController::ValidationRules($videoDetails[$key], 'VideoLanguageUrl');
                        if (!$validator['status']) {
                            $errorMessage = explode('.', $validator['message']);
                            throw new \Exception($errorMessage['0']);
                        } else {
                            continue;
                        }
                    } else {
                        throw new \Exception(UtilityController::Getmessage('VIDEO_FORMAT_VALIDATION'));
                    }
                } else {
                    $videoDetails['status'] = 1;
                    $videoDetails['message'] = 'Video for '.$value['language'].' is required';
                    return $videoDetails;
                    // continue;
                }
            }
            return $videoDetails;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getMessage(), 0);
            return $returnData;
        }            
    }

    //###############################################################
    //Function Name: Ossfunction
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to upload a file to oss and respond accordingly
    //In Params:     required details
    //Return:        json
    //Date:          5th Feb, 2019
    //###############################################################
    public function Ossfunction($value){     
        try {
            $fileName = date("dmYHis") . rand();
            $filepath = '/var/www/html/mts/videos';
            $value['video']->move(($filepath), $fileName);
            chmod($filepath . '/' . $fileName, 0777);
            $data1['video_file'] = $fileName;
            if (Request::server('SERVER_NAME') == 'www.sixclouds.net' || Request::server('SERVER_NAME') == 'www.sixclouds.cn') {
                $httpServer = "https://";
            } else {
                $httpServer = "http://";
            }
            $whatfolder = explode('/', url(''));
            $data1['yesBeta'] = in_array('beta', $whatfolder)?1:0;

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $httpServer . Request::server('SERVER_NAME') . "/mts/main.php",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $data1,
            ));
            $response = curl_exec($curl);
            $response = json_decode(trim($response), true);
            $returnData['duration'] = $response[2];
            $returnData['url']      = $response[1];
            $returnData['urlmp4']   = $response[0];
            $returnData['duration'] = gmdate("H:i:s", (int) $returnData['duration']);
            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
                throw new \Exception($err);
            } else {
                ##unlinking file
                $destinationPath = '/var/www/html/mts/videos';
                File::delete($filepath.'/'.$fileName);
                return $returnData;
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getMessage(), 0);
            return $returnData;
        }            
    }

    //###############################################################
    //Function Name: ChangeQuizStatus
    //Author:        Ketan Solank <ketan@creolestudios.com>
    //Purpose:       To change Quiz Status
    //In Params:     Slug
    //Return:        Json Encoded Data
    //Date:          14th August 2018
    //###############################################################
    public function ChangeQuizStatus(Request $request)
    {
        try {
             if (Auth::guard('admins')->user()->id) {
                $Input = Input::all();
                $quizId = 0;
                if(isset($Input['quizId']) && is_numeric($Input['quizId']) && $Input['quizId'] > 0){
                    $quizId          = $Input['quizId'];
                    // $updateData['quiz_published_status'] = $Input['quiz_published_status'];
                    $resultUpdate         = UtilityController::Makemodelobject($Input, 'Quizzes', '', $quizId);
                    ZoneQuiz::where('quiz_id',$Input['quizId'])->forceDelete();
                    foreach ($Input['video_for_zones'] as $key => $value) {
                        $pivotEntry[$key]['zone_id']    = $value;
                        $pivotEntry[$key]['quiz_id']    = $Input['quizId'];
                        $pivotEntry[$key]['created_at'] = Carbon::now();
                        $pivotEntry[$key]['updated_at'] = Carbon::now();
                    }
                    ZoneQuiz::insert($pivotEntry);
                    $responseArray        = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $resultUpdate);
                }else{
                    $responseArray = UtilityController::Generateresponse(false, 'QUIZ_ID_REQUIRED', 2);
                }
            }else{
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
             }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), '', '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name: GetDistributorDetails
    //Author:        Ketan Solank <ketan@creolestudios.com>
    //Purpose:       To get distributor data for edit page
    //In Params:     Slug
    //Return:        Json Encoded Data
    //Date:          16th August 2018
    //###############################################################
    public function GetDistributorDetails(Request $request)
    {
        try {
             if (Auth::guard('admins')->user()->id) {
                $Input = Input::all();                                                                
                if (!array_key_exists('id', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('DISTRIBUTOR_ID_REQUIRED'));
                }
                $id      = substr($Input['id'], strpos($Input['id'], ":") + 1);
                $distributor_id = base64_decode($id);                                                     
                if(isset($distributor_id) && is_numeric($distributor_id) && $distributor_id > 0){                    
                     $distributorData = Distributors::select("*")->with('referal_promo_code')->where("id", $distributor_id)->first()->toArray();                                        
                    if(!empty($distributorData)){
                        $responseArray = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $distributorData);
                    }else{
                        $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');                           
                    }
                }else{                    
                    $responseArray = UtilityController::Generateresponse(false, 'DISTRIBUTOR_ID_REQUIRED', 2);
                }
            }else{
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), '', '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name: editDistributor
    //Author:        Ketan Solank <ketan@creolestudios.com>
    //Purpose:       TO edit the Distributor details 
    //In Params:     
    //Return:        Json Encoded Data
    //Date:          16th August 2018
    //###############################################################
    public function editDistributor(Request $request)
    {
        try {
            if (Auth::guard('admins')->user()->id) {
                $Input = Input::all();
                if (!array_key_exists('hidDistributorId', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('DISTRIBUTOR_ID_REQUIRED'));
                }
                $DistributorId = 0;
                if(isset($Input['hidDistributorId']) && is_numeric($Input['hidDistributorId']) && $Input['hidDistributorId'] > 0){
                    $DistributorId = $Input['hidDistributorId'];
                }
                if (!array_key_exists('company_name', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('COMPANY_NAME_REQUIRED'));
                }
                /*if (!array_key_exists('last_name', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('LAST_NAME_REQUIRED'));
                }*/
                if (!array_key_exists('email_address', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('EMAIL_ADDRESS_REQUIRED'));
                }
                if (!array_key_exists('phonecode', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('COUNTRY_CODE_REQUIRED'));
                }
                if (!array_key_exists('contact', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('CONTACT_REQUIRED'));
                }
                if (!array_key_exists('commission_percentage', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('COMMISION_PERCENTAGE_REQUIRED'));
                }
                if($Input['commission_percentage'] && $Input['commission_percentage'] > 100){
                    throw new \Exception(UtilityController::Getmessage('COMMISION_PERCENTAGE_LESS_100'));   
                }
                if($Input['commission_percentage']){
                    $decimalValues = strlen(substr(strrchr($Input['commission_percentage'], "."), 1));
                    if($decimalValues > 2){
                        throw new \Exception(UtilityController::Getmessage('COMMISION_PERCENTAGE_DECIMAL_2'));   
                    }                    
                }
                if (!array_key_exists('renewal_commission_percentage', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('RENEWAL_COMMISION_PERCENTAGE_REQUIRED'));
                }
                if($Input['renewal_commission_percentage'] && $Input['renewal_commission_percentage'] > 100){
                    throw new \Exception(UtilityController::Getmessage('RENEWAL_COMMISION_PERCENTAGE_LESS_100'));   
                }
                if($Input['renewal_commission_percentage']){
                    $decimalValues = strlen(substr(strrchr($Input['renewal_commission_percentage'], "."), 1));
                    if($decimalValues > 2){
                        throw new \Exception(UtilityController::Getmessage('RENEWAL_COMMISION_PERCENTAGE_DECIMAL_2'));   
                    }                    
                }
                if (!array_key_exists('country_id', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('COUNTRY_REQUIRED'));
                }

                if (!array_key_exists('state_id', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('STATE_REQUIRED'));
                }

                if (!array_key_exists('city_id', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('CITY_REQUIRED'));
                }

                /*$validator = UtilityController::ValidationRules($Input, 'Distributors');
                if (!$validator['status']) {
                    $errorMessage = explode('.', $validator['message']);
                    $responseArray = UtilityController::Generateresponse(false, $errorMessage['0'], 0);
                } else { */                 
                    if(isset($DistributorId) && is_numeric($DistributorId) && $DistributorId > 0){
                        \DB::beginTransaction();
                        $isExsists = Distributors::where('email_address', $Input['email_address'])->pluck('id')->toArray();
                        if(count($isExsists) == 1){
                            if($DistributorId != $isExsists[0])
                                throw new \Exception(UtilityController::Getmessage('EMAIL_ALREADY_EXISTS'));
                        }else {
                            if(in_array($DistributorId,$isExsists))
                                throw new \Exception(UtilityController::Getmessage('EMAIL_ALREADY_EXISTS'));
                        }
                        //Delete old Qr Code File Starts HereCreole
                        $distriButorData = Distributors::find($DistributorId)->toArray();
                        if(!empty($distriButorData)){
                            $oldFileName = $distriButorData['qrcode_file_name'];
                            // unlink(public_path()."/uploads/distributor_qr_codes/".$oldFileName);
                        }                        
                        //Delete old Qr Code File Ends Here
                        $result = UtilityController::Makemodelobject($Input, 'Distributors', '', $DistributorId);
                        if (!empty($result)) {        
                            $password             = str_random(10);
                            $hashPassword         = Hash::make($password);
                            $slug['password']     = $hashPassword;           

                            $slug['slug']  = UtilityController::Makeslug($Input['company_name'], '-') . ':' . base64_encode($Input['hidDistributorId']);
                            $slug['qrcode_token'] = $distriButorData['qrcode_token']; //date("YmdHis").str_random(10);
                            //Code to generate the QRcode starts here                                 
                            //$qrCodeString = str_slug($result['company_name'])."*".$slug['slug']."*".$slug['qrcode_token'];
                            $fileName =  $distriButorData['qrcode_file_name']; //date("YmdHis").str_random(10).".png";
                            $filePath = public_path().'/uploads/distributor_qr_codes/'.$fileName;
                            //Code to generate the QRcode ends here                                                 
                            //$slug['qrcode_file_name'] = $fileName;
                             //Generation of Referal Code starts here                                 
                                // $referalCode = $this->GenerateReferalCode("DS",UtilityController::Makeslug($Input['company_name']));
                            repeat:
                            $referalCode = $this->GenerateReferalCode("DS",self::Generaterandominitial());
                            if(Distributors::where('referal_code',$referalCode)->exists())
                                goto repeat;
                            else
                                $slug['referal_code'] = $referalCode;
                            //Generation of Referal Code ends here  
                            //Code to generate the Unique Link starts here
                            /*$backUrl             = base64_encode(date("dmYHis").rand().'_'.$slug['slug'].'_'.base64_encode(1));
                            $backUrl             = str_replace('/', '_', $backUrl);
                            $uniqueLink          = url('')."/ignite-signup/".$backUrl;
                            $slug['unique_link'] = $uniqueLink;*/

                            $backUrl                = base64_encode(date("dmYHis").rand().'_'.$slug['slug'].'_'.base64_encode(1));
                            $backUrl                = str_replace('/', '@', $backUrl);
                            $backUrlLink            = url('')."/ignite-signup/".$backUrl.'/'.base64_encode(3);
                            $backUrlQr              = url('')."/ignite-signup/".$backUrl.'/'.base64_encode(2);
                            $slug['unique_link']    = $distriButorData['unique_link'];//$backUrlLink;
                            $slug['unique_link_qr'] = $distriButorData['unique_link_qr'];//$backUrlQr;
                            $toLogin                = url('/').'/distributor';

                            //QrCode::format('png')->size(500)->generate($backUrlQr, public_path().'/uploads/distributor_qr_codes/'.$fileName);
                            //Code to generate the Unique Link ends here
                            //Code to send the Login credentials to the Distributor starts here                        
                            $Input['User']['user_name']     = $result['company_name'];
                            $Input['User']['email_address'] = $result['email_address'];
                            $Input['User']['password']      = $password;                                                                             
                            $Input['footer_content']        = "From,<br /> SixClouds";
                            $Input['footer']                = "SixClouds";                                                                            
                            $Input['content']               = "Hello <strong>".$Input['User']['user_name']."</strong>,<br/><br/>Here are your Login Credentials:<br/><br/><span style='font-size: 15px;''><strong>Email Address : </strong>".$Input['User']['email_address']."</span><br/><span style='font-size: 15px;'><strong>Password : </strong>".$password."</span><br><br><a href='".$toLogin."' class='' style='color: #fff;
                                               background-color: #009bdd;
                                               border-color: #bd383e;
                                               text-decoration: none;display: inline-block;
                                               padding: 5px 10px;
                                               margin-bottom: 0;
                                               margin-top: 3px;
                                               font-size: 14px;
                                               font-weight: 400;
                                               line-height: 1.42857143;
                                               text-align: center;
                                               white-space: nowrap;
                                               vertical-align: middle;
                                               -ms-touch-action: manipulation;
                                               touch-action: manipulation;
                                               cursor: pointer;
                                               -webkit-user-select: none;
                                               -moz-user-select: none;
                                               -ms-user-select: none;
                                               user-select: none;
                                               background-image: none;
                                               border: 1px solid transparent;
                                               border-radius: 4px;'>Login</a>";                        
                            //Code to generate the QRcode starts here 
                            $resultslug    = UtilityController::Makemodelobject($slug, 'Distributors', '', $result['id']);
                            /* Referal to promo code part */
                            $promoExists = PromoCode::where('promo_code',$Input['old_referal_code'])->where('distributor_id',$Input['hidDistributorId'])->where('code_for',1)->first();
                            $getPromoofTeam = PromoCode::where('distributor_id',$Input['hidDistributorId'])->whereNotNull('distributor_team_member_id')->where('code_for',2)->get()->pluck('id');
                            if($Input['discount_type']==2){
                                if(!empty($promoExists)){
                                    PromoCode::where('promo_code',$Input['old_referal_code'])->where('distributor_id',$Input['hidDistributorId'])->where('code_for',1)->update(['status'=>0]);
                                    if(!empty($getPromoofTeam)) {
                                        PromoCode::where('distributor_id',$Input['hidDistributorId'])->whereNotNull('distributor_team_member_id')->where('code_for',2)->update(['status'=>0]);
                                    }
                                }
                            }
                            if($Input['discount_type']==0||$Input['discount_type']==1){
                                if(!array_key_exists('discount_of', $Input)&&!isset($Input['discount_of'])) {
                                    throw new \Exception("Discount of is required"); 
                                }
                                if(!empty($promoExists)){
                                    $addReferaltoPromo['promo_code']    = $resultslug['referal_code'];
                                    $addReferaltoPromo['discount_type'] = $Input['discount_type'];
                                    $addReferaltoPromo['discount_of']   = $Input['discount_of'];
                                    $addReferaltoPromoResult            = UtilityController::Makemodelobject($addReferaltoPromo,'PromoCode','',$promoExists['id']);
                                    
                                    if(!empty($getPromoofTeam)) {
                                        unset($addReferaltoPromo['promo_code']);
                                        $addReferaltoPromo['status']   = 1;
                                        $updatePromoofTeam = PromoCode::whereIn('id',$getPromoofTeam->toArray())->where('code_for',2)->update($addReferaltoPromo);
                                    }
                                } else {
                                    $addReferaltoPromo['promo_code']      = $resultslug['referal_code'];
                                    $addReferaltoPromo['distributor_id']  = $resultslug['id'];
                                    $addReferaltoPromo['discount_type']   = $Input['discount_type'];
                                    $addReferaltoPromo['promo_type']      = 0;
                                    $addReferaltoPromo['discount_of']     = $Input['discount_of'];
                                    $addReferaltoPromo['redemption_type'] = 1;
                                    $addReferaltoPromo['status']          = 1;
                                    $addReferaltoPromo['code_for']        = 1;
                                    $addReferaltoPromoResult = UtilityController::Makemodelobject($addReferaltoPromo,'PromoCode');
                                    $getTeamMates = DistributorsTeamMembers::select('id','referal_code')->where('distributor_id',$resultslug['id'])->get();
                                    if(!empty($getTeamMates)){
                                        $addReferaltoPromoTeam = $getTeamMates->map(function($value, $key) use($Input,$resultslug){
                                            $addReferaltoPromoTeam['promo_code']                 = $value->referal_code;
                                            $addReferaltoPromoTeam['distributor_team_member_id'] = $value->id;
                                            $addReferaltoPromoTeam['distributor_id']             = $resultslug['id'];
                                            $addReferaltoPromoTeam['discount_type']              = $Input['discount_type'];
                                            $addReferaltoPromoTeam['promo_type']                 = 0;
                                            $addReferaltoPromoTeam['discount_of']                = $Input['discount_of'];
                                            $addReferaltoPromoTeam['redemption_type']            = 1;
                                            $addReferaltoPromoTeam['status']                     = 1;
                                            $addReferaltoPromoTeam['code_for']                   = 2;
                                            return $addReferaltoPromoTeam;
                                        });
                                        if(!empty($getPromoofTeam))
                                            PromoCode::whereIn('id',$getPromoofTeam->toArray())->where('code_for',2)->forceDelete();
                                        PromoCode::insert($addReferaltoPromoTeam->toArray());
                                    }
                                }
                            }
                            if (isset($Input['User']['email_address']) && $Input['User']['email_address'] != "") {
                            Mail::send('emails.email_template', $Input, function ($message) use ($Input, $filePath) {
                                $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                                $message->to($Input['User']['email_address']);
                                $message->subject("You have been added as a distributor");
                                $message->attach($filePath);
                            });
                        }     
                            $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $resultslug);
                            \DB::commit();
                        } else {
                        $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 0);
                        }
                    }else{
                        $responseArray = UtilityController::Generateresponse(false, 'DISTRIBUTOR_ID_REQUIRED', 0);        
                    }                                        
                // }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), '', '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name: changeDistributorPassword
    //Author:        Ketan Solank <ketan@creolestudios.com>
    //Purpose:       TO change the Password of Distributor
    //In Params:     
    //Return:        Json Encoded Data
    //Date:          17th August 2018
    //###############################################################
    public function changeDistributorPassword(Request $request)
    {
        try {
            if (Auth::guard('admins')->user()->id) {
                $Input = Input::all();
                
                if (!array_key_exists('hidDistributorIdChangePassword', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('DISTRIBUTOR_ID_REQUIRED'));
                }
                if (!array_key_exists('change_password', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('PASSWORD_REQUIRED'));
                }
                if (!array_key_exists('change_confirm_password', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('CONFIRM_PASSWORD_REQUIRED'));
                }
                if($Input['change_password'] != $Input['change_confirm_password']){
                    throw new \Exception(UtilityController::Getmessage('PASSWORD_AND_CONFIRM_PASSWORD_NOT_SAME'));
                }
                if(array_key_exists('updateFor', $Input)&&$Input['updateFor']=='User'&&!array_key_exists('admin_password', $Input)){
                    throw new \Exception(UtilityController::Getmessage('ADMIN_PASSWORD_REQUIRED'));
                }
                if(array_key_exists('updateFor', $Input)&&$Input['updateFor']=='User'&&array_key_exists('admin_password', $Input)){
                    if(!Hash::check($Input['admin_password'],Auth::guard('admins')->user()->password))
                        throw new \Exception(UtilityController::Getmessage('ADMIN_PASSWORD_INCORRECT'));       
                }

                if (isset($Input['hidDistributorIdChangePassword']) && is_numeric($Input['hidDistributorIdChangePassword']) && $Input['hidDistributorIdChangePassword'] > 0) {
                    $distributorsData = (array_key_exists('updateFor', $Input)&&$Input['updateFor']=='User'?User::select('id','email_address','first_name','last_name','is_email')->where('id',$Input['hidDistributorIdChangePassword'])->first()->toArray():Distributors::find($Input['hidDistributorIdChangePassword'])->toArray());
                    if (!empty($distributorsData)) {
                        $data                                        = array();
                        $password                                    = $Input['change_password'];
                        $hashPassword                                = Hash::make($password);
                        $data['password']                            = $hashPassword;     
                        $modelName                                   = (array_key_exists('updateFor', $Input)&&$Input['updateFor']=='User'?'User':'Distributors');
                        $resultPassword                              = UtilityController::Makemodelobject($data, $modelName, '', $Input['hidDistributorIdChangePassword']); 
                        $email_address                               = $distributorsData['email_address'];
                        if($distributorsData['is_email'] == 1) {
                            if(array_key_exists('updateFor', $Input))
                                $Input['User']['user_name']                  = $distributorsData['first_name'] . " " . $distributorsData['last_name'];                        
                            else
                                $Input['User']['user_name']                  = $distributorsData['company_name'];                        
                            $Input['User']['email_address']              = $distributorsData['email_address'];                        
                            $Input['User']['password']                   = $password;                        
                            $Input['footer_content']                     = "From,<br /> SixClouds";
                            $Input['footer']                             = "SixClouds";                                                
                            $Input['User']['message']                    = "Admin has changed your Passord.<br/>Here are your Login Credentials:";
                            $Input['content']                            = "Hello <strong>".$Input['User']['user_name']."</strong>,<br/><br/>Admin has changed your Passord.<br/>Here are your Login Credentials:<br/><br/><span style='font-size: 15px;''><strong>Email Address : </strong>".$Input['User']['email_address']."</span><br/><span style='font-size: 15px;'><strong>Password : </strong>".$password."</span><br><br>";
                            if (isset($email_address) && $email_address != "") {
                                Mail::send('emails.email_template', $Input, function ($message) use ($Input, $email_address) {
                                    $message->from('noreply@sixclouds.com', 'SixClouds');
                                    $message->to($email_address)->subject("Admin Changed your Password");
                                });
                            }
                        }
                        $responseArray = UtilityController::Generateresponse(true, 'PASSWORD_SET', 1);
                    }else{
                        $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 1);        
                    }
                }else{
                    $responseArray = UtilityController::Generateresponse(false, 'DISTRIBUTOR_ID_REQUIRED', 1);    
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), '', '');
        }
        return response()->json($responseArray);
    }
    //###############################################################
    //Function Name: RegenerateQRCode
    //Author:        Ketan Solank <ketan@creolestudios.com>
    //Purpose:       TO regerate the Qr Code file for Distributor
    //In Params:     
    //Return:        Json Encoded Data
    //Date:          21st August 2018
    //###############################################################
    public function RegenerateQRCode(Request $request)
    {
        try {
            if (Auth::guard('admins')->user()->id) {
                $Input = Input::all();                               
                if (!array_key_exists('slug', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('DISTRIBUTOR_ID_REQUIRED'));
                }
                $slug = ($Input['slug'] != '' ? $Input['slug'] : '');                
                if (isset($slug) && $slug != "") {
                    $distributorsData = $Input['whose']=='Distributors'?Distributors::select("*")->where("slug",$slug)->first()->toArray():DistributorsTeamMembers::select("*")->where("slug",$slug)->first()->toArray();
                    if (!empty($distributorsData)) {  
                        if($Input['whose']=='Distributors') {
                            $slug = array();
                            $slug['slug']  = UtilityController::Makeslug($distributorsData['company_name'], '-') . ':' . base64_encode($distributorsData['id']);
                            $slug['qrcode_token'] = date("YmdHis").str_random(10);
                            //Code to generate the QRcode starts here
                            $backUrl                 = base64_encode(date("dmYHis").rand().'_'.$distributorsData['slug'].'_'.base64_encode(1));
                            $backUrl                 = str_replace('/', '@', $backUrl);
                            $backUrlQr               = url('')."/ignite-signup/".$backUrl.'/'.base64_encode(2);
                            $slug['unique_link_qr'] = $backUrlQr;
                            $fileName = date("YmdHis").str_random(10).".png";
                            QrCode::format('png')->size(800)->generate($backUrlQr, public_path().'/uploads/distributor_qr_codes/'.$fileName);
                            //Delete the old file
                            $oldFileName = $distributorsData['qrcode_file_name'];
                            if(file_exists(public_path()."/uploads/distributor_qr_codes/".$oldFileName))
                                unlink(public_path()."/uploads/distributor_qr_codes/".$oldFileName);
                        } else {
                            $slug = array();
                            $display_name = $distributorsData['first_name'].' '.$distributorsData['last_name'];
                            $slug['slug']  = UtilityController::Makeslug($display_name, '-') . ':' . base64_encode($distributorsData['id']);
                            $slug['qrcode_token'] = date("YmdHis").str_random(10);
                            $backUrl   = base64_encode(date("dmYHis").rand().'_'.$slug['slug'].'_'.base64_encode(2).'_'.base64_encode($distributorsData['distributor_id']));
                            $backUrl   = str_replace('/', '@', $backUrl);
                            $backUrlQr = url('')."/ignite-signup/".$backUrl.'/'.base64_encode(2);
                            $slug['unique_link_qr'] = $backUrlQr;
                            $fileName = date("YmdHis").str_random(10).".png";
                            QrCode::format('png')->size(500)->generate($backUrlQr, public_path().'/uploads/distributor_qr_codes/team/'.$fileName);
                            //Delete the old file
                            $oldFileName = $distributorsData['qrcode_file_name'];
                            if(file_exists(public_path()."/uploads/distributor_qr_codes/team/".$oldFileName))
                                unlink(public_path()."/uploads/distributor_qr_codes/team/".$oldFileName);
                        }
                        // $filePath = public_path().'/uploads/distributor_qr_codes/'.$fileName;
                        //Code to generate the QRcode ends here                     
                        $slug['qrcode_file_name'] = $fileName;
                        $resultslug = UtilityController::Makemodelobject($slug, $Input['whose'], '', $distributorsData['id']);
                        $path = $Input['whose']=='Distributors'?"/public/uploads/distributor_qr_codes/":"/public/uploads/distributor_qr_codes/team/";
                        $fileData['file_path'] = url('').$path.$fileName;                     
                        $fileData['file_name'] = $fileName;
                        $responseArray = UtilityController::Generateresponse(true, 'QR_CODE_REGENERATED', 1, $fileData);
                    }else{
                        $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 1);        
                    }
                }else{
                    $responseArray = UtilityController::Generateresponse(false, 'DISTRIBUTOR_ID_REQUIRED', 1);    
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), '', '');
        }
        return response()->json($responseArray);
    }    
    //###############################################################
    //Function Name: DownloadQrCodeFile
    //Author:        Ketan Solanki <ketan@creolestudios.com>
    //Purpose:       TO download the Qr Code file
    //In Params:     Null
    //Return:        download the file
    //Date:          21st August 2018
    //###############################################################
    public function DownloadQrCodeFile(Request $request)
    {
        try {
            $Input = Input::all();                        
            if(isset($Input['filename']) && $Input['filename'] != ""){
               $fileName = $Input['filename'];
            }             
            $fileNameRelativePath = public_path('') . Config('constants.path.QRCODE_FILE_UPLOAD_PATH') . $fileName;            
            if (File::exists($fileNameRelativePath)) {
                return response()->download($fileNameRelativePath); //Download the file
            } 
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;           
        }
    }

    //###############################################################
    //Function Name: RegenerateReferalCode
    //Author:        Ketan Solanki <ketan@creolestudios.com>
    //Purpose:       TO regenrate the Referal Code of Distributor
    //In Params:     
    //Return:        Json Encoded Data
    //Date:          21st August 2018
    //###############################################################
    public function RegenerateReferalCode(Request $request)
    {
        try {
            if (Auth::guard('admins')->user()->id) {
                $Input = Input::all();                               
                if (!array_key_exists('slug', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('DISTRIBUTOR_ID_REQUIRED'));
                }
                $slug = ($Input['slug'] != '' ? $Input['slug'] : '');                
                if (isset($slug) && $slug != "") {
                    if($Input['whose']=='Distributors'){
                        $distributorsData = Distributors::select("*")->where("slug",$slug)->first()->toArray();
                        $referalInitial = "DS";
                    } else {
                        $distributorsData = DistributorsTeamMembers::select("*")->where("slug",$slug)->first()->toArray();
                        $referalInitial = "TM";
                    }
                    if (!empty($distributorsData)) {                        
                        $slug = array();
                        // $referalCode = $this->GenerateReferalCode("DS",$distributorsData['first_name'],$distributorsData['last_name']);
                        repeat:
                        $referalCode = $this->GenerateReferalCode($referalInitial,self::Generaterandominitial(),self::Generaterandominitial());
                        if($Input['whose']=='Distributors'?Distributors::where('referal_code',$referalCode)->exists():DistributorsTeamMembers::where('referal_code',$referalCode)->exists())
                            goto repeat;
                        else
                            $slug['referal_code'] = $referalCode;
                        $resultslug    = UtilityController::Makemodelobject($slug, $Input['whose'], '', $distributorsData['id']);
                        $returnData['referal_code'] = $referalCode;
                        if($Input['whose']=='Distributors'){
                            if(PromoCode::where('distributor_id',$distributorsData['id'])->exists())
                                PromoCode::where('distributor_id',$distributorsData['id'])->where('code_for',1)->update(['promo_code'=>$referalCode]);
                        } else {
                            if(PromoCode::where('distributor_team_member_id',$distributorsData['id'])->exists())
                                PromoCode::where('distributor_team_member_id',$distributorsData['id'])->where('code_for',2)->update(['promo_code'=>$referalCode]);
                        }
                        $responseArray = UtilityController::Generateresponse(true, 'REFERAL_CODE_REGENERATED', 1, $returnData);
                    }else{
                        $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 1);        
                    }
                }else{
                    $responseArray = UtilityController::Generateresponse(false, 'DISTRIBUTOR_ID_REQUIRED', 1);    
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), '', '');
        }
        return response()->json($responseArray);
    } 

    //###############################################################
    //Function Name: Generaterandominitial
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to generate a random alphabet that can be used for creating referal codes initial
    //In Params:     lenght to generate
    //Return:        json
    //Date:          26th Nov,
    //###############################################################
    public static function Generaterandominitial($length = 1){     
        try {
            repeat:
            $randomChar = strtoupper(str_random($length));
            if(is_numeric($randomChar))
                goto repeat;
            else
            return $randomChar;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: RegenerateUniqueLink
    //Author:        Ketan Solanki <ketan@creolestudios.com>
    //Purpose:       TO regenerate the Unique Link of distributor
    //In Params:     
    //Return:        Json Encoded Data
    //Date:          21st August 2018
    //###############################################################
    public function RegenerateUniqueLink(Request $request)
    {
        try {
            if (Auth::guard('admins')->user()->id) {
                $Input = Input::all();
                if (!array_key_exists('slug', $Input)) {
                    throw new \Exception(UtilityController::Getmessage('DISTRIBUTOR_ID_REQUIRED'));
                }
                $slug = ($Input['slug'] != '' ? $Input['slug'] : '');
                if (isset($slug) && $slug != "") {
                    if($Input['whose']=='Distributors'){
                        $distributorsData = Distributors::select("*")->where("slug",$slug)->first()->toArray();
                        $backUrl             = base64_encode(date("dmYHis").rand().'_'.$Input['slug'].'_'.base64_encode(1));
                        $backUrl             = str_replace('/', '@', $backUrl);
                        $backUrlLink         = url('')."/ignite-signup/".$backUrl.'/'.base64_encode(3);
                    } else {
                        $distributorsData    = DistributorsTeamMembers::select("*")->where("slug",$slug)->first()->toArray();
                        $backUrl          = base64_encode(date("dmYHis").rand().'_'.$Input['slug'].'_'.base64_encode(2).'_'.base64_encode($distributorsData['distributor_id']));
                        $backUrl             = str_replace('/', '@', $backUrl);
                        $backUrlLink         = url('')."/ignite-signup/".$backUrl.'/'.base64_encode(3);
                    }
                    if (!empty($distributorsData)) {
                        $slug                = array();
                        $slug['unique_link'] = $backUrlLink;
                        $resultslug    = UtilityController::Makemodelobject($slug, $Input['whose'], '', $distributorsData['id']); 
                        $returnData['unique_link'] = $backUrlLink;                                             
                        $responseArray = UtilityController::Generateresponse(true, 'LINK_REGENERATED', 1, $returnData);
                    }else{
                        $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 1);        
                    }
                }else{
                    $responseArray = UtilityController::Generateresponse(false, 'DISTRIBUTOR_ID_REQUIRED', 1);    
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray = UtilityController::Generateresponse(0, $e->getMessage(), '', '');
        }
        return response()->json($responseArray);
    }  


    public function ExportToExcel(Request $request) {
        try
        {
            $input = Input::all();
            $phoneNumber = (isset($input['phone_number']) ? $input['phone_number'] : '');
            $emailAddress = (isset($input['email_address']) ? $input['email_address'] : '');
            $created_from = (isset($input['created_from']) ? $input['created_from'] : '');
            $created_to = (isset($input['created_to']) ? $input['created_to'] : '');

            $allSubscriberData = User::select("*", DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as joining_date"))
                ->where("status",'!=',2)
                ->where('is_ignite',1)
                ->with(['subscriber_country', 'subscriber_state', 'subscriber_city', 'distributorRefer','transactions' => function($query){
                        $query->select('id','payment_method','promo_code','paid_by','system_transaction_number','payment_amount','payment_amount_type','access_platform','remarks','distributor_id','distributer_team_member_id','video_category_id','video_category_name','subscription_end_date', DB::Raw("DATE_FORMAT(created_at, '%d-%b-%y') as transaction_date"))->with([
                        'video_categories' => function($query){
                            $query->select('id','category_name');
                        },
                        'distributor' => function($query){
                            $query->select('id','company_name');
                        },
                        'distributor_team_member' => function($query){
                            $query->select('id','first_name','last_name');
                        }
                    ])->where('paid_for',3)->where('payment_status',1);
                }
            ]);

            if (isset($phoneNumber) && $phoneNumber != ''):
                $allSubscriberData = $allSubscriberData
                    ->where(function ($query) use ($phoneNumber) {
                        $query->where('contact', 'like', '%' . $phoneNumber . '%');
                    });
            endif;
            if (isset($emailAddress) && $emailAddress != ''):
                $allSubscriberData = $allSubscriberData
                    ->where(function ($query) use ($emailAddress) {
                        $query->where('email_address', 'like', '%' . $emailAddress . '%');
                    });
            endif;

            if((isset($created_from) && $created_from != '') && (isset($created_to) && $created_to != '')):
                $allSubscriberData = $allSubscriberData
                    ->where(function ($query) use ($created_from,$created_to) {
                        $query->whereBetween('created_at', [Carbon::parse($created_from)->startOfDay(), Carbon::parse($created_to)->endOfDay()]);
                    });
            elseif(isset($created_from) && $created_from != ''):
                $allSubscriberData = $allSubscriberData
                    ->where(function ($query) use ($created_from) {
                        $query->whereDate('created_at', '<=', $created_from);
                    });
            elseif(isset($created_to) && $created_to != ''):
                $allSubscriberData = $allSubscriberData
                    ->where(function ($query) use ($created_to) {
                        $query->whereDate('created_at', '>=', $created_to);
                    });
            endif;
            $subscriberData = $allSubscriberData->get()->toArray();

            return Excel::create('Subscriber_user_list', function($excel) use ($subscriberData) {
                $excel->sheet('Sheet1', function($sheet) use ($subscriberData)
                {
                    $sheet->mergeCells('A1:A2');
                    $sheet->mergeCells('B1:B2');
                    $sheet->mergeCells('C1:C2');
                    $sheet->mergeCells('D1:D2');
                    $sheet->mergeCells('E1:E2');
                    $sheet->mergeCells('F1:F2');
                    $sheet->mergeCells('G1:G2');
                    $sheet->mergeCells('H1:H2');
                    $sheet->mergeCells('I1:I2'); 
                    $sheet->mergeCells('J1:L1');
                    $sheet->mergeCells('M1:O1'); 
                    $sheet->mergeCells('P1:X1');

                    $j = 2;
                    foreach ($subscriberData as $key => $data) {
                        $j++;
                        $sum = count($data['transactions'])-1;
                        if(count($data['transactions']) > 0)
                        {
                            $sum = count($data['transactions'])-1;
                        }
                        else
                        {
                            $sum = 0;   
                        }
                        $count = $j + $sum;
                        $sheet->mergeCells('A'.$j.':A'.$count);
                        $sheet->mergeCells('B'.$j.':B'.$count);
                        $sheet->mergeCells('C'.$j.':C'.$count);
                        $sheet->mergeCells('D'.$j.':D'.$count);
                        $sheet->mergeCells('E'.$j.':E'.$count);
                        $sheet->mergeCells('F'.$j.':F'.$count);
                        $sheet->mergeCells('G'.$j.':G'.$count);
                        $sheet->mergeCells('H'.$j.':H'.$count);
                        $sheet->mergeCells('I'.$j.':I'.$count);
                        $j = $count;
                    }
                    $sheet->setSize(array(
                        'A1' => array(
                            'width'     => 15,
                            'height'    => 20
                        ),
                        'B1' => array(
                            'width'     => 15,
                            'height'    => 20
                        ),
                        'C1' => array(
                            'width'     => 25,
                            'height'    => 20
                        ),
                        'D1' => array(
                            'width'     => 15,
                            'height'    => 20
                        ),
                        'E1' => array(
                            'width'     => 15,
                            'height'    => 20
                        ),
                        'F1' => array(
                            'width'     => 15,
                            'height'    => 20
                        ),
                        'G1' => array(
                            'width'     => 25,
                            'height'    => 20
                        ),
                        'H1' => array(
                            'width'     => 15,
                            'height'    => 20
                        ),
                        'I1' => array(
                            'width'     => 15,
                            'height'    => 20
                        ),
                        'J1' => array(
                            'width'     => 35,
                            'height'    => 20
                        ),
                        'M1' => array(
                            'width'     => 35,
                            'height'    => 20
                        ),
                        'P1' => array(
                            'width'     => 95,
                            'height'    => 20
                        ),
                        'J2' => array(
                            'width'     => 15,
                            'height'    => 15
                        ),
                        'K2' => array(
                            'width'     => 15,
                            'height'    => 15
                        ),
                        'L2' => array(
                            'width'     => 15,
                            'height'    => 15
                        ),
                        'M2' => array(
                            'width'     => 15,
                            'height'    => 15
                        ),
                        'N2' => array(
                            'width'     => 15,
                            'height'    => 15
                        ),
                        'O2' => array(
                            'width'     => 15,
                            'height'    => 15
                        ),
                        'P2' => array(
                            'width'     => 15,
                            'height'    => 15
                        ),
                        'Q2' => array(
                            'width'     => 20,
                            'height'    => 15
                        ),
                        'R2' => array(
                            'width'     => 15,
                            'height'    => 15
                        ),
                        'S2' => array(
                            'width'     => 15,
                            'height'    => 15
                        ),
                        'T2' => array(
                            'width'     => 15,
                            'height'    => 15
                        ),
                        'U2' => array(
                            'width'     => 25,
                            'height'    => 15
                        ),
                        'V2' => array(
                            'width'     => 25,
                            'height'    => 15
                        ),
                        'W2' => array(
                            'width'     => 25,
                            'height'    => 15
                        ),
                        'X2' => array(
                            'width'     => 35,
                            'height'    => 15
                        ),
                    ));

                    $sheet->setColumnFormat(array(
                        'G' => '0'
                    ));
                    
                    $sheet->cell('J',function($cell) {
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('K',function($cell) {
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });    
                    $sheet->cell('L',function($cell) {
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('M',function($cell) {
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('N',function($cell) {
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('O',function($cell) {
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('P',function($cell) {
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('Q',function($cell) {
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('R',function($cell) {
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('S',function($cell) {
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('T',function($cell) {
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('U',function($cell) {
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('V',function($cell) {
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('W',function($cell) {
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('X',function($cell) {
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });

                    $sheet->cell('A1', function($cell) {
                        $cell->setValue('First Name');   
                        $cell->setFontWeight('bold');
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('B1', function($cell) {
                        $cell->setValue('Last Name');  
                        $cell->setFontWeight('bold'); 
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('C1', function($cell) {
                        $cell->setValue('Email/Username'); 
                        $cell->setFontWeight('bold'); 
                        $cell->setAlignment('center');
                        $cell->setValignment('center'); 
                    });
                    $sheet->cell('D1', function($cell) {
                        $cell->setValue('Country');   
                        $cell->setFontWeight('bold');
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('E1', function($cell) {
                        $cell->setValue('Provience');   
                        $cell->setFontWeight('bold');
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('F1', function($cell) {
                        $cell->setValue('City');   
                        $cell->setFontWeight('bold');
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('G1', function($cell) {
                        $cell->setValue('Phone Number');   
                        $cell->setFontWeight('bold');
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('H1', function($cell) {
                        $cell->setValue('Status');   
                        $cell->setFontWeight('bold');
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('I1', function($cell) {
                        $cell->setValue('DOB');   
                        $cell->setFontWeight('bold');
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('J1', function($cell) {
                        $cell->setValue('Current Subscription');   
                        $cell->setFontWeight('bold');
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('J2', function($cell) {
                        $cell->setValue('Product');  
                        $cell->setFontWeight('bold'); 
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('K2', function($cell) {
                        $cell->setValue('From');   
                        $cell->setFontWeight('bold');
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('L2', function($cell) {
                        $cell->setValue('Till'); 
                        $cell->setFontWeight('bold');  
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('M1', function($cell) {
                        $cell->setValue('Historical Subscription');  
                        $cell->setFontWeight('bold'); 
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('M2', function($cell) {
                        $cell->setValue('Product'); 
                        $cell->setFontWeight('bold');  
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('N2', function($cell) {
                        $cell->setValue('From');  
                        $cell->setFontWeight('bold'); 
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('O2', function($cell) {
                        $cell->setValue('Till'); 
                        $cell->setFontWeight('bold');  
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('P1', function($cell) {
                        $cell->setValue('Transaction'); 
                        $cell->setFontWeight('bold');  
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('P2', function($cell) {
                        $cell->setValue('Date');  
                        $cell->setFontWeight('bold'); 
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('Q2', function($cell) {
                        $cell->setValue('Invoice ID');  
                        $cell->setFontWeight('bold'); 
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('R2', function($cell) {
                        $cell->setValue('Product');   
                        $cell->setFontWeight('bold');
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('S2', function($cell) {
                        $cell->setValue('Amount'); 
                        $cell->setFontWeight('bold');  
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('T2', function($cell) {
                        $cell->setValue('Access Platform'); 
                        $cell->setFontWeight('bold');  
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('U2', function($cell) {
                        $cell->setValue('Distributor');  
                        $cell->setFontWeight('bold'); 
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('V2', function($cell) {
                        $cell->setValue('Salesperson');   
                        $cell->setFontWeight('bold'); 
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('W2', function($cell) {
                        $cell->setValue('Promo Code');   
                        $cell->setFontWeight('bold'); 
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                    $sheet->cell('X2', function($cell) {
                        $cell->setValue('Remarks');   
                        $cell->setFontWeight('bold'); 
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });           
                    $i=2;
                    $sr_no = 0;
                    foreach ($subscriberData as $key => $finalData) {
                        if(!empty($finalData)){
                            $i++;
                            $sr_no++;

                            if($finalData['status'] == 1)
                            {
                                $status = 'Active';
                            }
                            elseif ($finalData['status'] == 0) {
                                $status = 'Inactive';
                            }
                            elseif ($finalData['status'] == 2) {
                                $status = 'Deleted';
                            }
                            elseif ($finalData['status'] == 3) {
                                $status = 'Suspend';
                            }
                            elseif ($finalData['status'] == '')
                            {
                                $status = '';   
                            }

                            $sheet->cell('A'.$i, function($cell) use ($finalData) {
                                $cell->setValue($finalData['first_name'] != '' ? $finalData['first_name'] : '');    
                                $cell->setAlignment('center');
                                $cell->setValignment('center');
                            });
                            $sheet->cell('B'.$i, function($cell) use ($finalData) {
                                $cell->setValue($finalData['last_name'] != '' ? $finalData['last_name'] : '');    
                                $cell->setAlignment('center');
                                $cell->setValignment('center');
                            });
                            $sheet->cell('C'.$i, function($cell) use ($finalData) {
                                $cell->setValue($finalData['email_address'] != '' ? $finalData['email_address'] : '');    
                                $cell->setAlignment('center');
                                $cell->setValignment('center');
                            });
                            $sheet->cell('D'.$i, function($cell) use ($finalData) {
                                $cell->setValue($finalData['subscriber_country']['name'] != '' ? $finalData['subscriber_country']['name'] : '');    
                                $cell->setAlignment('center');
                                $cell->setValignment('center');
                            });
                            $sheet->cell('E'.$i, function($cell) use ($finalData) {
                                $cell->setValue($finalData['subscriber_state']['name'] != '' ? $finalData['subscriber_state']['name'] : '');    
                                $cell->setAlignment('center');
                                $cell->setValignment('center');
                            });
                            $sheet->cell('F'.$i, function($cell) use ($finalData) {
                                $cell->setValue($finalData['subscriber_city']['name'] != '' ? $finalData['subscriber_city']['name'] : '');    
                                $cell->setAlignment('center');
                                $cell->setValignment('center');
                            });
                            $sheet->cell('G'.$i, function($cell) use ($finalData) {
                                $cell->setValue($finalData['contact'] != '' ? '+'.$finalData['phonecode'].' '.$finalData['contact'] : '');    
                                $cell->setAlignment('center');
                                $cell->setValignment('center');
                            });
                            $sheet->cell('H'.$i, function($cell) use ($finalData,$status) {
                                $cell->setValue($status != '' ? $status : '');    
                                $cell->setAlignment('center');
                                $cell->setValignment('center');
                            });
                            $sheet->cell('I'.$i, function($cell) use ($finalData) {
                                $cell->setValue(isset($finalData['dob']) ? date('d-m-Y', strtotime($finalData['dob'])) : '');    
                                $cell->setAlignment('center');
                                $cell->setValignment('center');
                            });

                            if(empty($finalData['transactions']))
                            {
                                $sheet->mergeCells('J'.$i.':X'.$i);
                                $sheet->cell('J'.$i, function($cell) use ($finalData) {
                                        $cell->setValue('Nil');    
                                        $cell->setAlignment('center');
                                        $cell->setValignment('center');
                                });
                            }
                            else
                            {
                                $j = $i;
                                foreach ($finalData['transactions'] as $key => $value) {
                                    if($value['subscription_end_date'] >= Carbon::now()->toDateString())
                                    {
                                        $sheet->cell('J'.$j, $value['video_category_name'] != '' ? $value['video_category_name'] : '');
                                        $sheet->cell('K'.$j, isset($value['transaction_date']) ? date('d-m-Y', strtotime($value['transaction_date'])) : '');
                                        $sheet->cell('L'.$j, isset($value['subscription_end_date']) ? date('d-m-Y', strtotime($value['subscription_end_date'])) : '');
                                            $j++;
                                    }
                                    else
                                    {
                                        $sheet->mergeCells('J'.$j.':L'.$j);
                                        $sheet->cell('J'.$j, function($cell) use ($finalData) {
                                            $cell->setValue('Nil');    
                                            $cell->setAlignment('center');
                                            $cell->setValignment('center');
                                        });
                                        $j++;
                                    }
                                }

                                $k = $i;
                                foreach ($finalData['transactions'] as $key => $value) {
                                    if($value['subscription_end_date'] < Carbon::now()->toDateString())
                                    {
                                        $sheet->cell('M'.$k, $value['video_category_name'] != '' ? $value['video_category_name'] : '');
                                        $sheet->cell('N'.$k, isset($value['transaction_date']) ? date('d-m-Y', strtotime($value['transaction_date'])) : '');
                                        $sheet->cell('O'.$k, isset($value['subscription_end_date']) ? date('d-m-Y', strtotime($value['subscription_end_date'])) : '');
                                            $k++;
                                    }
                                    else
                                    {
                                        $sheet->mergeCells('M'.$k.':O'.$k);
                                        $sheet->cell('M'.$k, function($cell) use ($finalData) {
                                            $cell->setValue('Nil');    
                                            $cell->setAlignment('center');
                                            $cell->setValignment('center');
                                        });
                                        $k++;
                                    }
                                }

                                foreach ($finalData['transactions'] as $key => $value) {
                                    if($value['access_platform'] == 1)
                                    {
                                        $access_platform = 'Web';
                                    }
                                    elseif ($value['access_platform'] == 2) {
                                        $access_platform = 'ios';
                                    }
                                    elseif ($value['access_platform'] == 3) {
                                        $access_platform = 'Android';
                                    }
                                    elseif ($value['access_platform'] == 4) {
                                        $access_platform = 'Wechat';
                                    }
                                    elseif ($value['access_platform'] == '')
                                    {
                                        $access_platform = 'N.A.';   
                                    }

                                    $sheet->cell('P'.$i, isset($value['transaction_date']) ? date('d-m-Y', strtotime($value['transaction_date'])) : '');
                                    $sheet->cell('Q'.$i, $value['system_transaction_number'] != '' ? $value['system_transaction_number'] : 'N.A.');
                                    $sheet->cell('R'.$i, $value['video_category_name'] != '' ? $value['video_category_name'] : '');

                                    if($value['payment_amount_type'] == 1)
                                    {
                                        $sheet->cell('S'.$i, $value['payment_amount'] != '' ? '$'.$value['payment_amount'] : '$0');    
                                    }
                                    elseif($value['payment_amount_type'] == 2)
                                    {
                                        $sheet->cell('S'.$i, $value['payment_amount'] != '' ? 'S$'.$value['payment_amount'] : 'S$0');   
                                    }
                                    $sheet->cell('T'.$i, $access_platform != '' ? $access_platform : '');
                                    $sheet->cell('U'.$i, $value['distributor']['company_name'] != '' ? $value['distributor']['company_name'] : 'SixClouds Web');
                                    $sheet->cell('V'.$i, $value['distributor_team_member']['first_name'] != '' ? $value['distributor_team_member']['first_name'] : 'N.A.');
                                    $sheet->cell('W'.$i, $value['promo_code'] != '' ? $value['promo_code'] : 'N.A.');
                                    $sheet->cell('X'.$i, $value['remarks'] != '' ? $value['remarks'] : '');
                                    if(count($finalData['transactions']) != ($key+1)){
                                        $i++;
                                    }
                                }
                            }
                        }
                    }
                });
            })->store($input['type'],'public/reports/',true);

        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }


    //###############################################################
    //Function Name : GetallSubscriberData
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To get all subscriber data
    //In Params : Void
    //Return : json
    //Date : 5th September 2018
    //###############################################################
    public function GetallSubscriberData(Request $request)
    {
        try
        {
            $input = Input::all();
            $phoneNumber = (isset($input['phone_number']) ? $input['phone_number'] : '');
            $emailAddress = (isset($input['email_address']) ? $input['email_address'] : '');
            $created_from = (isset($input['created_from']) ? $input['created_from'] : '');
            $created_to = (isset($input['created_to']) ? $input['created_to'] : '');
            
            $allSubscriberData = User::select("*", DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as joining_date"))
                ->where("status",'!=',2)
                ->where('is_ignite',1)
                ->with('subscriber_country', 'subscriber_state', 'subscriber_city', 'distributorRefer');
            if (isset($phoneNumber) && $phoneNumber != ''):
                $allSubscriberData = $allSubscriberData
                    ->where(function ($query) use ($phoneNumber) {
                        $query->where('contact', 'like', '%' . $phoneNumber . '%');
                    });
            endif;
            if (isset($emailAddress) && $emailAddress != ''):
                $allSubscriberData = $allSubscriberData
                    ->where(function ($query) use ($emailAddress) {
                        $query->where('email_address', 'like', '%' . $emailAddress . '%');
                    });
            endif;

            if((isset($created_from) && $created_from != '') && (isset($created_to) && $created_to != '')):
                $allSubscriberData = $allSubscriberData
                    ->where(function ($query) use ($created_from,$created_to) {
                        $query->whereBetween('created_at', [Carbon::parse($created_from)->startOfDay(), Carbon::parse($created_to)->endOfDay()]);
                    });
            elseif(isset($created_from) && $created_from != ''):
                $allSubscriberData = $allSubscriberData
                    ->where(function ($query) use ($created_from) {
                        $query->whereDate('created_at', '<=', $created_from);
                    });
            elseif(isset($created_to) && $created_to != ''):
                $allSubscriberData = $allSubscriberData
                    ->where(function ($query) use ($created_to) {
                        $query->whereDate('created_at', '>=', $created_to);
                    });
            endif;

            $allSubscriberData = $allSubscriberData->paginate((Config('constants.other.MP_ADMIN_CATEGORY_PAGINATE') + Config('constants.other.MP_ADMIN_CATEGORY_PAGINATE')))->toArray();
            if (!empty($allSubscriberData['data'])) {
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $allSubscriberData);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            return $returnData;
        } catch (\Exception $e) {
            //print_r($e->getLine()." - ".$e->getMessage());;die;
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getFile().' -:- '.$e->getLine().' -:- '.$e->getMessage(), '', '');
            return $returnData;
        }
    }  

    //###############################################################
    //Function Name : GetSubscriberDetailsData
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To get all subscriber details data
    //In Params : Slug
    //Return : json
    //Date : 5th September 2018
    //###############################################################
    public function GetSubscriberDetailsData(Request $request)
    {
        try
        {
            $input = Input::all();
            $slug = ($input['slug'] != '' ? $input['slug'] : '');
            if (isset($slug) && $slug != "") {
                $distributorData = User::select("*", DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as joining_date"), DB::Raw("DATE_FORMAT(dob, '%d %M, %Y') as dob"))
                    ->with('subscriber_country', 'subscriber_state', 'subscriber_city', 'distributorRefer')
                    ->where("slug", $slug)
                    ->first()
                    ->toArray();

                if($distributorData['dob'] && !is_null($distributorData['dob']) && !empty($distributorData['dob'])) {
                    $distributorData['date_format'] = Carbon::createFromFormat('d M, Y', $distributorData['dob']);
                    $distributorData['date_format'] = Carbon::parse($distributorData['date_format'])->format('d/m/Y');
                } else {
                    unset($distributorData['date_format']);
                }
                if(!$distributorData['contact'] || is_null($distributorData['contact']) || empty($distributorData['contact'])) {
                    unset($distributorData['contact']);
                    unset($distributorData['phonecode']);
                }
                if($distributorData['country_id'] == 0)
                {
                    $distributorData['country_id'] = "";   
                }
                if($distributorData['provience_id'] == 0)
                {
                    $distributorData['provience_id'] = "";   
                }
                if($distributorData['city_id'] == 0)
                {
                    $distributorData['city_id'] = "";   
                }
                $id = base64_decode(explode(":", $slug)[1]);
                $transactions = MarketPlacePayment::select('id','payment_method','promo_code','paid_by','system_transaction_number','payment_amount','access_platform','remarks','distributor_id','distributer_team_member_id','video_category_id','video_category_name', DB::Raw("DATE_FORMAT(created_at, '%d-%b-%y') as transaction_date"))->with([
                    'video_categories' => function($query){
                        $query->select('id','category_name');
                    },
                    'distributor' => function($query){
                        $query->select('id','company_name');
                    },
                    'distributor_team_member' => function($query){
                        $query->select('id','first_name','last_name');
                    }
                ])->where('paid_by',$id)->where('paid_for',3)->where('payment_status',1)->get();
                if (!empty($distributorData)) {
                    $data['transactions'] = $transactions;
                    $data['distributorData'] = $distributorData;
                    $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $data);
                } else {
                    $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
                }
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }

    }        

    //###############################################################
    //Function Name: GetCurrentSubscription
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To get the current subscriptions
    //In Params:     subscriber's id
    //Return:        json
    //Date:          5th Sept. 2018
    //###############################################################
    public function GetCurrentSubscription(Request $request){
        try {
            $Input = Input::all();
            $currentSubscription = MarketPlacePayment::select('id','paid_by','video_category_id','video_category_name', DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as from_date"), DB::Raw("DATE_FORMAT(subscription_end_date, '%d %M, %Y') as to_date"))
                ->with([
                    'video_categories' => function($query){
                        $query->select('id','category_name');
                    }
                ])
            ->where('paid_by',$Input['paid_by'])
            ->where('paid_for',3)
            ->where('subscription_end_date', '>=', Carbon::now()->toDateString())
            ->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))->toArray();
            $returnData = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $currentSubscription);
        } catch (Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }
        return response($returnData);
    }

    //###############################################################
    //Function Name: getAllSubscription
    //Author:        Sumit Advani <sumit@creolestudios.com>
    //Purpose:       To get the all subscriptions
    //In Params:     subscriber's id
    //Return:        json
    //Date:          13th June, 2019
    //###############################################################
    public function getAllSubscription(Request $request){
        try {
            $Input = Input::all();
            $currentSubscription = MarketPlacePayment::select('id','paid_by','video_category_id', DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as from_date"), DB::Raw("DATE_FORMAT(subscription_end_date, '%d %M, %Y') as to_date"))->with([
                    'video_categories' => function($query){
                        $query->select('id','category_name');
                    }
                ])->where('paid_by',$Input['paid_by'])->where('paid_for',3)->where('subscription_end_date', '>=', Carbon::now()->toDateString())->get()->toArray();
            $returnData = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $currentSubscription);
        } catch (Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }
        return response($returnData);
    }

    //###############################################################
    //Function Name: removeSubscription
    //Author:        Sumit Advani <sumit@creolestudios.com>
    //Purpose:       To remove subscription plan
    //In Params:     id, effective_date, paid_by
    //Return:        json
    //Date:          13th June, 2019
    //###############################################################
    public function removeSubscription(Request $request)
    {
        try {
            $error = false;
            if (Auth::guard('admins')->check()) {
                $Input = Input::all();
                if(!empty($Input)) {
                    foreach ($Input as $key => $value) {
                        if($value['subscription_end_date']!='' && isset($value['subscription_end_date']) && !empty($value['subscription_end_date'])){
                            $effectiveDate = Carbon::createFromFormat('d/m/Y',$value['subscription_end_date']);
                            $effectiveDate->hour = 23;
                            $effectiveDate->minute = 59;
                            $effectiveDate->second = 59;
                            $storeAccess['subscription_end_date'] = $effectiveDate;
                            $storeAccess['updated_at'] = Carbon::now();
                            if(!empty($storeAccess)){
                                \DB::beginTransaction();
                                MarketPlacePayment::where('paid_by',$value['subscriberId'])->where('id',$value['id'])->update($storeAccess);
                                $returnData = UtilityController::Generateresponse(true, 'ACCESS_GRANTED', 1);
                                \DB::commit();
                            } else {
                                $returnData = UtilityController::Generateresponse(false, 'Unable to remove subscription plans', 0);
                                break;
                            }
                        } else {
                            $returnData = UtilityController::Generateresponse(false, "Select 'Effective Date' for '".$value['category_name']."'", 0);
                            break;
                        }
                    }
                } else {
                    $returnData = UtilityController::Generateresponse(false, "Select Atleast One Category", 0);
                }
            } else {
                    $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getMessage(), '', '');
        }
        return response($returnData);
    }

    //###############################################################
    //Function Name: GetHistorySubscription
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To get the past subscriptions
    //In Params:     subscriber's id
    //Return:        json
    //Date:          5th Sept. 2018
    //###############################################################
    public function GetHistorySubscription(Request $request){
        try {
            $Input = Input::all();
            $historySubscription = MarketPlacePayment::select('id','paid_by','video_category_id', DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as from_date"), DB::Raw("DATE_FORMAT(subscription_end_date, '%d %M, %Y') as to_date"))->with([
                    'video_categories' => function($query){
                        $query->select('id','category_name');
                    }
                ])->where('paid_by',$Input['paid_by'])->where('paid_for',3)->where('subscription_end_date', '<', Carbon::now()->toDateString())->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))->toArray();
            $returnData = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $historySubscription);
        } catch (Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }
        return response($returnData);
    }

    //###############################################################
    //Function Name: addNewSubscriber
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To add a new subscriber from admin side
    //In Params:     subscriber's data
    //Return:        json
    //Date:          20th Sept 2018
    //###############################################################
    public function addNewSubscriber(Request $request){     
        try {
            $Input = Input::all();
            $generated_password = $Input['generated_password'];
            unset($Input['generated_password']);
            $validator = UtilityController::ValidationRules($Input, 'User', array('password', 'country_id', 'city_id'));
            if (!$validator['status']) {
                $errorMessage = explode('.', $validator['message']);
                $returnData   = UtilityController::Generateresponse(false, $errorMessage['0'], 0);
            } else {
                \DB::beginTransaction();
                    if(isset($generated_password) && !is_null($generated_password) && !empty($generated_password)) {
                        $Input['is_registered_by_admin'] = 1;
                        $Input['password'] = Hash::make($generated_password);    
                    } else {
                        $password = str_random(10);
                        $Input['is_registered_by_admin'] = 1;
                        $Input['password'] = Hash::make($password);
                    }
                    $Input['current_language'] = 1;
                    /*if(array_key_exists('dob', $Input)){
                        if(!isset($Input['dob']))
                            unset($Input['dob']);
                        else
                            $Input['dob'] = Carbon::createFromFormat('d/m/Y',$Input['dob']);
                    }*/
                    if(array_key_exists('country_id', $Input)) {
                        if(!isset($Input['country_id'])) {
                            $Input['country_id'] = 0;
                        } else {
                            $zones = Zone::get()->toArray();
                            foreach ($zones as $key => $value) {
                                if(in_array($Input['country_id'], explode(',', $value['country_id']))) {
                                    $Input['zone_id'] = $value['id'];
                                }
                            }
                            // unset($Input['country_id']);
                        }
                    }

                    if(array_key_exists('provience_id', $Input))
                        if(!isset($Input['provience_id']))
                            $Input['provience_id'] = 0;
                            // unset($Input['provience_id']);

                    if(array_key_exists('city_id', $Input))
                        if(!isset($Input['city_id']))
                            $Input['city_id'] = 0;
                            // unset($Input['city_id']);

                    $UserExists = User::where('email_address', $Input['email_address'])->where('status', 1)->first();
                    if (!empty($UserExists)) {
                        $returnMessage = 'EMAIL_ALREADY_EXISTS';
                        $returnData    = UtilityController::Generateresponse(false, $returnMessage, 400, '');
                        return $returnData;
                    }
                    $Input['user_number'] = UtilityController::GenerateRunningNumber('User','user_number',"U");
                    $Input['status'] = 1;
                    $Input['is_ignite'] =1;
                    $user = UtilityController::Makemodelobject($Input, 'User');
                    $name = preg_replace('/\s+/', '', $Input['first_name']) .' '.preg_replace('/\s+/', '', $Input['last_name']);
                    $slug['slug'] = str_slug($name, '-');
                    $slug['slug'] = $slug['slug'].':'.base64_encode($user['id']);
                    $user = UtilityController::Makemodelobject($slug, 'User','',$user['id']);
                    if(!isset($generated_password) || is_null($generated_password) || empty($generated_password)) {
                        $newSubscriber['subject'] = 'SixClouds Account Created';
                        $newSubscriber['email_address'] = $Input['email_address'];
                        $newSubscriber['content'] = 'Hello '.$name.' <br><br> Congratulation, SixClouds Team has created your account. <br><br> Find below your credentials to login <br><br> Email Address : '.$Input['email_address'].' <br> Password : '.$password.'<br><br>';
                        if(/*!array_key_exists('dob', $Input)||*/!array_key_exists('country_id', $Input)||!array_key_exists('provience_id', $Input)||!array_key_exists('city_id', $Input))
                            $newSubscriber['content'] = $newSubscriber['content'].' Note : You need to first fill all your necessary details without which you will not be able to access anything.';

                        $newSubscriber['footer_content']='From,<br /> SixClouds Customer Support';
                        $newSubscriber['footer']='SixClouds';
                        Mail::send('emails.email_template', $newSubscriber, function ($message) use ($newSubscriber) {
                            $message->from('noreply@sixclouds.com', 'SixClouds');
                            $message->to($newSubscriber['email_address'])->subject($newSubscriber['subject']);
                        });
                    }
                    $returnData = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $user);
                \DB::commit();
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getMessage().':-:'.$e->getLine(), '', '');
        }            
        return response($returnData);
    }
    //###############################################################
    //Function Name: Getcategoriesforsubscription
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To get the categories and its paymet details by subscriber if done any
    //In Params:     Subscriber's is
    //Return:        json
    //Date:          22nd Oct, 2018
    //###############################################################
    public function Getcategoriesforsubscription(Request $request){
       try {
           $Input = Input::all();
           $categories = VideoCategory::whereNull('parent_id')->with('subject')->get();
           $paymentExists = implode(',', MarketPlacePayment::where('paid_by',$Input['subscriberId'])->where('subscription_end_date','>',Carbon::now())->pluck('video_category_id')->toArray());
           $paymentExists = array_unique(array_filter(explode(',', $paymentExists)));
           $paymentExistsData = MarketPlacePayment::select('video_category_id','subscription_end_date')->where('paid_by',$Input['subscriberId'])->where('subscription_end_date','>',Carbon::now())->get()->toArray();
           foreach ($categories as $key => $value){
               $value['canGrant'] = in_array($value['id'], $paymentExists)?0:1;
               if($value['canGrant']==0){
                   foreach ($paymentExistsData as $keyInner => $valueInner)
                       if($value['id']==$valueInner['video_category_id'])
                           $value['subscriptionTill'] = Carbon::parse($valueInner['subscription_end_date'])->format('d/m/Y');
               }
           }
           $alreadyHave = 0;
           foreach ($categories as $key => $value){
               if($value['canGrant']==0)
                   $alreadyHave++;
           }
           if($alreadyHave==$categories->count()) {
               $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 3, $categories);
           } else {
               $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $categories);
           }
       } catch (\Exception $e) {
           $sendExceptionMail = UtilityController::Sendexceptionmail($e);
           $returnData = UtilityController::Generateresponse(false, $e->getMessage(), '', '');
       }
       return response($returnData);
   }

    //###############################################################
    //Function Name: AddSubscription
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to grant subscriber access to category contents
    //In Params:     array of category ids
    //Return:        json
    //Date:          22nd Oct, 2018
    //###############################################################
    public function AddSubscription(Request $request){
        try {
            if (Auth::guard('admins')->check()) {
                $Input = Input::all();
                foreach ($Input['category'] as $key => $value) {
                    if(array_key_exists('category', $value)) {
                        if($value['end_date']!=''&&isset($value['end_date'])){
                            $endDate                                    = Carbon::createFromFormat('d/m/Y',$value['end_date']);
                            $storeAccess[$key]['payment_method']        = 3;
                            $storeAccess[$key]['paid_by']               = $Input['subscriberId'];
                            $storeAccess[$key]['video_category_id']     = $value['category'];
                            $storeAccess[$key]['video_category_name']   = $value['category_name'];
                            $storeAccess[$key]['subscription_end_date'] = $endDate;
                            $storeAccess[$key]['remarks']               = 'Added by admin';
                            $storeAccess[$key]['paid_for']              = 3;
                            $storeAccess[$key]['payment_status']        = 1;
                            $storeAccess[$key]['created_at']            = Carbon::now();
                            $storeAccess[$key]['updated_at']            = Carbon::now();
                        } else {
                            throw new \Exception("Select 'End Date' for '".$value['category_name']."'");
                        }
                    } else {
                        unset($Input['category'][$key]);
                    }
                }
                if(!empty($storeAccess)){
                    \DB::beginTransaction();
                    MarketPlacePayment::insert($storeAccess);
                    $returnData = UtilityController::Generateresponse(true, 'ACCESS_GRANTED', 1);
                    \DB::commit();
                } else {
                    $returnData = UtilityController::Generateresponse(false, 'SELECT_CATEGORY', 0);
                }
            } else {
                    $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getMessage(), '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name : UpdateQuizQuestionOrder
    //Author        : Senil Shah <senil@creolestudios.com>
    //Purpose       : To update the order of the quiz question as per the order given from jquery ui sortable
    //In Params     : ID array to map the order to be updated
    //Return        : Json Encoded Data
    //Date          : 23rd Oct, 2018
    //###############################################################
    public function UpdateQuizQuestionOrder(Request $request)
    {
        try {
            $Input = Input::all();
            if (!empty($Input['idArray'])) {
                \DB::beginTransaction();
                foreach ($Input['idArray'] as $key => $value)
                    $returnData = QuizQuestions::where("id", $value)->update(['question_order' => $key+1]);
                if($returnData){
                    $returnData = UtilityController::Generateresponse(true, 'ORDER_UPDATE', 200, '');
                    \DB::commit();
                }
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            return $returnData;
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name: Deletequestion
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To delete a quiz question
    //In Params:     question id
    //Return:        json
    //Date:          23rd Oct, 2018
    //###############################################################
    public function Deletequestion(Request $request){     
        try {
            $Input = Input::all();
            \DB::beginTransaction();
            if($Input['id']!=''&& is_numeric($Input['id'])&&$Input['id']!=0){
                $deleteQuestionResult = QuizQuestions::where('id',$Input['id'])->update(['question_status'=>2]);
                if($deleteQuestionResult) {
                    $returnData = UtilityController::Generateresponse(true, 'QUESTION_DELETED', 1);        
                    \DB::commit();
                }
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: Editsubscriberdetails
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To let admin edit details of subscriber
    //In Params:     Details to be changed
    //Return:        json
    //Date:          26th Oct, 2018
    //###############################################################
    public function Editsubscriberdetails(Request $request){     
        try {
            $Input = Input::all();
            $zones = Zone::get()->toArray();
            foreach ($zones as $key => $value)
                if(in_array($Input['country_id'], explode(',', $value['country_id'])))
                    $Input['zone_id'] = $value['id'];
            if(isset($Input['dob']) && !is_null($Input['dob']) && !empty($Input['dob'])) {
                $Input['dob'] = Carbon::createFromFormat('d/m/Y',$Input['dob']);
            }
            \DB::beginTransaction();
            $storeResult = UtilityController::Makemodelobject($Input,'User','',$Input['subscriber_id']);
            $returnData = UtilityController::Generateresponse(true, 'DETAILS_UPDATED', 1,$storeResult);
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getMessage(), '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: Savecategory
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to save/update the buzz category details
    //In Params:     details to be updated
    //Return:        json
    //Date:          30th Oct, 2018
    //###############################################################
    public function Savecategory(Request $request){     
        try {
            $Input = Input::all();
            if(!array_key_exists('description', $Input))
                throw new \Exception('"English Description" is required.');
            if(!array_key_exists('description_chi', $Input))
                throw new \Exception('"Chinese Description" is required.');
            if(!array_key_exists('category_price', $Input))
                throw new \Exception('"Category Price" is required.');
            if(!array_key_exists('category_id', $Input))
                throw new \Exception('Select Category.');
            if(!array_key_exists('promotions', $Input))
                throw new \Exception('Select promotion.');
            if(!array_key_exists('promotional_price', $Input)&&$Input['promotions']==1)
                throw new \Exception('"Promotional Price" is required.');
            \DB::beginTransaction();
                if($Input['promotions']==0)
                    $Input['promotional_price'] = 0.00;
                $saveCategory = UtilityController::Makemodelobject($Input,'VideoCategory','',$Input['category_id']);
                if(!empty($saveCategory)){
                    $returnData = UtilityController::Generateresponse(true,'INS_CATEGORY_UPDATED',1,$saveCategory);
                    \DB::commit();
                }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: Savepromo
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to save/update the promo for categories
    //In Params:     details to be updated
    //Return:        json
    //Date:          2nd Nov, 2018
    //###############################################################
    public function Savepromo(Request $request){     
        try {
            $Input = Input::all();
            if(!array_key_exists('promo', $Input))
                throw new \Exception('Promo code is required.');
            \DB::beginTransaction();
                $addPromoTo = VideoCategory::get()->pluck('id');
                $savePromo = VideoCategory::whereIn('id',$addPromoTo)->update(['promo_code'=>$Input['promo']]);
                if(!empty($savePromo)){
                    $returnData = UtilityController::Generateresponse(true,'PROMO_ADDED_TO_ALL',1,$savePromo);
                    \DB::commit();
                }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }
    //###############################################################
    //Function Name: Showteammemberinfo
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to fetch distributor's particular team member details
    //In Params:     team member id
    //Return:        json
    //Date:          14th Nov, 2018
    //###############################################################
    public function Showteammemberinfo(Request $request){     
        try {
            $Input = Input::all();
            $memberDetail = DistributorsTeamMembers::with('country', 'state', 'city')->where('id',$Input['id'])->first();
            if(!empty($memberDetail)){
                $memberDetail['subscriberCount'] = DistributorsReferrals::where('distributor_team_member_id',$Input['id'])->count();
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $memberDetail);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_SUCH_USER', 0);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: Getalldistributors
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to fetch distributors details
    //In Params:     team member id
    //Return:        json
    //Date:          14th Nov, 2018
    //###############################################################
    public function Getalldistributors(Request $request){     
        try {
            $distributorDetail = Distributors::select('id','company_name')->where('status',1)->get()->toArray();
            if(!empty($distributorDetail)){
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $distributorDetail);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_SUCH_USER', 0);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: Generatepromo
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to generate a random automatic promo code for category
    //In Params:     void
    //Return:        json
    //Date:          20th Nov, 2018
    //###############################################################
    public function Generatepromo(Request $request){     
        try {
            repeat:
            $promoCode = str_random(8);
            $promoCode = strtoupper($promoCode);
            if(!PromoCode::where('promo_code',$promoCode)->exists())
                $returnData = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $promoCode);
            else
                goto repeat;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: generatePassword
    //Author:        Sumit<sumit@creolestudios.com>
    //Purpose:       to generate a random automatic password for category
    //In Params:     void
    //Return:        json
    //Date:          20th Nov, 2018
    //###############################################################
    public function generatePassword(){     
        try {
            repeat:
            $password = str_random(8);
            $password = strtoupper($password);
                $returnData = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $password);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }
        return response($returnData);
    }

    //###############################################################
    //Function Name: Savepromocode
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to store new promo code
    //In Params:     void
    //Return:        json
    //Date:          20th Nov, 2018
    //###############################################################
    public function Savepromocode(Request $request){     
        try {
            \DB::beginTransaction();
            $Input = Input::all();
            $Input['discount_of'] = ($Input['discount_type']==0?$Input['discount_percent']:$Input['discount_price']);
            $Input['video_category_id'] = $Input['category'];
            $Input['category_name'] = VideoCategory::whereIn('id',$Input['video_category_id'])->get()->pluck('category_name')->toArray();
            $Input['category_name'] = implode(', ', $Input['category_name']);
            $Input['redemption_type'] = array_key_exists('redemption_count', $Input)&&isset($Input['redemption_count'])?0:1;

            $validator = UtilityController::ValidationRules($Input, 'PromoCode');
            if (!$validator['status']) {
                $errorMessage = explode('.', $validator['message']);
                $returnData   = UtilityController::Generateresponse(false, $errorMessage['0'], 0);
            } else {
                $fromDate = Carbon::createFromFormat('m/d/Y h:i A',$Input['from_date'], 'Asia/Singapore');
                // $Input['from_date'] = $fromDate->setTimezone('UTC');
                $Input['from_date'] = $fromDate;

                $toDate = Carbon::createFromFormat('m/d/Y h:i A',$Input['to_date'], 'Asia/Singapore');
                // $Input['to_date'] = $toDate->setTimezone('UTC');
                $Input['to_date'] = $toDate;
                if($Input['from_date']>$Input['to_date'])
                    throw new \Exception('From date cannot be greater than To date.');
                if($Input['from_date']==$Input['to_date'])
                    throw new \Exception('From date and To date cannot be equal.');
                $discountOf = ($Input['discount_type']==0?$Input['discount_percent']:$Input['discount_price']);
                $status = ($Input['from_date']>Carbon::now()?2:1);
                if(array_key_exists('promo_id', $Input)&& isset($Input['promo_id'])){
                    $Input['video_category_id'] = implode(',', $Input['video_category_id']);
                    $number_of_redemptions      = $Input['redemption_type'] != 1?$Input['redemption_count']:NULL;
                    $Input['status']            = $status;
                    $insertPromo                = UtilityController::Makemodelobject($Input,'PromoCode','',$Input['promo_id']);
                    $promo                      = $insertPromo;
                    PromoCode::where('id',$Input['promo_id'])->update(['number_of_redemptions'=>$number_of_redemptions]);
                } else {
                    if($Input['promo_type']==1){
                        $Input['video_category_id']     = implode(',', $Input['video_category_id']);
                        if($Input['redemption_type']!=1)
                            $Input['number_of_redemptions'] = $Input['redemption_count'];
                        $Input['status']                = $status;
                        $Input['redeemed_count']        = 0;
                        $insertPromo                    = UtilityController::Makemodelobject($Input,'PromoCode');
                        $promo                          = $insertPromo;
                    } else {
                        $individualCategoryName = explode(', ', $Input['category_name']);
                        foreach ($Input['video_category_id'] as $key => $value) {
                            $promo[$key]['promo_code']        = $Input['promo_code'];
                            $promo[$key]['remarks']           = $Input['remarks'];
                            $promo[$key]['video_category_id'] = $value;
                            $promo[$key]['discount_type']     = $Input['discount_type'];
                            $promo[$key]['promo_type']        = $Input['promo_type'];
                            $promo[$key]['category_name']     = $individualCategoryName[$key];
                            $promo[$key]['discount_of']       = $discountOf;
                            $promo[$key]['redemption_type']   = $Input['redemption_type'];
                            if($Input['redemption_type']!=1)
                                $promo[$key]['number_of_redemptions'] = $Input['redemption_count'];
                            $promo[$key]['from_date']             = $Input['from_date'];
                            $promo[$key]['to_date']               = $Input['to_date'];
                            $promo[$key]['status']                = $status;
                            $promo[$key]['redeemed_count']        = 0;
                            $promo[$key]['created_at']            = Carbon::now();
                            $promo[$key]['updated_at']            = Carbon::now();
                            if(array_key_exists('distributor', $Input))
                                $promo[$key]['distributor_id'] = $Input['distributor'];
                        }
                        $insertPromo = PromoCode::insert($promo);
                    }
                }
                if($insertPromo){
                    $returnData = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $promo);
                    $lastlyInserted = PromoCode::where('promo_code',$Input['promo_code'])->get()->pluck('id')->toArray();
                    if($status==2){
                        $diffInMinutes = Carbon::now()->diffInMinutes($Input['from_date']);
                        $livePromo = (new DoLivePromoCode($lastlyInserted,$Input['from_date']))->delay(Carbon::now()->addMinutes($diffInMinutes));
                        dispatch($livePromo);
                    }
                    $diffInMinutes = Carbon::now()->diffInMinutes($Input['to_date']);
                    $expirePromo = (new DoExpirePromoCode($lastlyInserted,$Input['to_date']))->delay(Carbon::now()->addMinutes($diffInMinutes));
                    dispatch($expirePromo);
                    \DB::commit();
                }
                else
                    $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getMessage(), '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: GetAllPromoData
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to get all promos for promo codes page
    //In Params:     status type
    //Return:        json
    //Date:          21st Nov, 2018
    //###############################################################
    public function GetAllPromoData(Request $request){     
        try {
            $Input = Input::all();
            $promoCodes = PromoCode::select('*',DB::RAW("DATE_FORMAT(created_at, '%d %b, %Y') as date"),DB::RAW("DATE_FORMAT(from_date, '%d %b %Y, %h:%i %p') as from_date"),DB::RAW("DATE_FORMAT(to_date, '%d %b %Y, %h:%i %p') as to_date"),DB::RAW('CONCAT(promo_code, ":", id) AS slug'))->with('category_detail','distributor_detail')->where('status',$Input['status'])->where('code_for',0);
            if(array_key_exists('search_by', $Input)&&$Input['search_by']!='') {
                $promoDistributors = Distributors::where('company_name','like','%'.$Input['search_by'].'%')->get()->pluck('id')->toArray();
                $promoCategories = VideoCategory::where('category_name','like','%'.$Input['search_by'].'%')->whereNull('parent_id')->get()->pluck('id')->toArray();
                $promoCodes = $promoCodes->where(function($q) use($Input,$promoDistributors,$promoCategories){
                    $q->where('promo_code','like','%'.$Input['search_by'].'%')->orWhereIn('distributor_id',$promoDistributors)->orWhereIn('video_category_id',$promoCategories);
                });
            }
            $promoCodes = $promoCodes->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))->toArray();
            $collection = collect($promoCodes['data']);
            $promoData      = $collection->map(function ($data) {
                $fromDate          = Carbon::parse($data['from_date'])->timezone('Asia/Singapore')->toDateTimestring(); 
                $data['from_date'] = UtilityController::Changedateformat($fromDate, 'd F, Y');

                $toDate          = Carbon::parse($data['to_date'])->timezone('Asia/Singapore')->toDateTimestring(); 
                $data['to_date'] = UtilityController::Changedateformat($toDate, 'd F, Y');
                return $data;
            })->values()->toArray();
            //echo("<pre>");print_r($promoData);echo("</pre>");
            if(!empty($promoCodes)){
                //$data['data'] = $promoData;
                $data['content'] = $promoCodes;
                $data['status']  = $Input['status'];
                $returnData      = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $data);
            } else
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getMessage().'-'.$e->getLine(), '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name :   ExportPromos
    //Author :          Senil Shah <senil@creolestudios.com>
    //Purpose :         To export Promo data
    //In Params :       status type, search value (if any), export type
    //Return :          json
    //Date :            21st Nov, 2018
    //###############################################################
    public function ExportPromos(Request $request){
        try {
            $Input = Request::all();
            if(Auth::guard('admins')->check()){
                if($Input['search_by']=='undefined'||$Input['search_by']=='')
                    unset($Input['search_by']);
                $allPromoData = PromoCode::select('*',DB::RAW("DATE_FORMAT(created_at, '%d %b, %Y') as date"),DB::RAW("DATE_FORMAT(from_date, '%d %b %Y, %h:%i %p') as from_date"),DB::RAW("DATE_FORMAT(to_date, '%d %b %Y, %h:%i %p') as to_date"))->with('category_detail','distributor_detail')->where('status',$Input['status'])->where('code_for',0);
                if(array_key_exists('search_by', $Input)){
                    $promoDistributors = Distributors::where('company_name','like','%'.$Input['search_by'].'%')->get()->pluck('id')->toArray();
                    $promoCategories = VideoCategory::where('category_name','like','%'.$Input['search_by'].'%')->whereNull('parent_id')->get()->pluck('id')->toArray();
                    $allPromoData = $allPromoData->where('promo_code','like','%'.$Input['search_by'].'%')->orWhereIn('distributor_id',$promoDistributors)->orWhereIn('video_category_id',$promoCategories);
                }
                $allPromoData = $allPromoData->get()->toArray();
                $dataArray[] = ['S/N', 'Date', 'Category', 'Promo Code', 'Discount', 'Number of Redemptions', 'Redeemed Count', 'From', 'Till', 'Owner', 'Remarks'];
                $dataArray = self::createExportData($allPromoData,$dataArray,1,'Promo Codes','Promo Codes','Promo Codes',$Input['export_type']);
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $allPromoData);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name: createExportData
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To create an array from data into export sheet array
    //In Params:     original data, data to export, which foreach to user 
    //Return:        json
    //Date:          9th Oct 2018
    //###############################################################
    public function createExportData($data, $dataArray, $which, $fileName, $title, $description, $type){     
        try {
            switch ($which) {
                case 1:
                    foreach ($data as $key => $value) {
                        $tempArray = array();
                        array_push($tempArray, $key+1);
                        array_push($tempArray, $value['date']);
                        array_push($tempArray, $value['category_name']);
                        array_push($tempArray, $value['promo_code']);
                        
                        if($value['discount_type']==0)
                            array_push($tempArray,$value['discount_of'].' %');
                        else
                            array_push($tempArray, UtilityController::getMessage('CURRENT_DOMAIN')=='net'?'S$ ':'¥ '.$value['discount_of']);

                        array_push($tempArray, $value['number_of_redemptions']);
                        array_push($tempArray, $value['redeemed_count']);
                        array_push($tempArray, $value['from_date']);
                        array_push($tempArray, $value['to_date']);

                        array_push($tempArray, $value['distributor_id']?$value['distributor_detail']['company_name']:'SixClouds');
                        array_push($tempArray, $value['remarks']);
            
                        array_push($dataArray, $tempArray);
                    }
                    if ($type == 1) 
                        UtilityController::ExportData($dataArray, $fileName, $title, $description, "xlsx");
                    elseif ($type == 2)
                        UtilityController::ExportData($dataArray, $fileName, $title, $description, "csv");
                    break;
                case 2:
                    foreach ($data as $key => $value) {
                        $tempArray = array();
                        array_push($tempArray, $key+1);
                        array_push($tempArray, $value['system_transaction_number']);
                        array_push($tempArray, $value['video_category_name']);
                        array_push($tempArray, $value['payment_amount']);
                        array_push($tempArray, $value['date']);
                        
                        if($value['payment_status']==1)
                            array_push($tempArray,'Success');
                        else if($value['payment_status']==2)
                            array_push($tempArray,'Pending');
                        else
                            array_push($tempArray,'Fail');
            
                        array_push($dataArray, $tempArray);
                    }
                    if ($type == 1) 
                        UtilityController::ExportData($dataArray, $fileName, $title, $description, "xlsx");
                    elseif ($type == 2)
                        UtilityController::ExportData($dataArray, $fileName, $title, $description, "csv");
                    break;
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return $dataArray;
    }

    //###############################################################
    //Function Name: Getpromodetaildata
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       get promo data for edit page of promo page
    //In Params:     slug
    //Return:        json
    //Date:          23rd Nov, 2018
    //###############################################################
    public function Getpromodetaildata(Request $request){
        try {
            $Input = Input::all();
            $Input['slug'] = explode(':', $Input['slug']);
            $promo['promo_code'] = $Input['slug'][0];
            $promo['id'] = $Input['slug'][1];
            $getPromoDetail = PromoCode::select('*',DB::RAW("DATE_FORMAT(created_at, '%d %b, %Y') as date"),DB::RAW("DATE_FORMAT(from_date, '%m/%d/%Y %h:%i %p') as from_date"),DB::RAW("DATE_FORMAT(to_date, '%m/%d/%Y %h:%i %p') as to_date"))->where('id',$promo['id'])->where('promo_code',$promo['promo_code'])->first()->toArray();
            if(!empty($getPromoDetail)){
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $getPromoDetail);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 0);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: Suspendpromo
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to suspend a promo code from live to expired
    //In Params:     promo code id
    //Return:        json
    //Date:          27th Nov, 2018
    //###############################################################
    public function Suspendpromo(Request $request){     
        try {
            $Input = Input::all();
            \DB::beginTransaction();
                $suspendPromo = PromoCode::where('id',$Input['id'])->update(['status'=>2]);
                $returnData = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1);
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: GetPromoDetail
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to get promo code details page to list down the list of subscribers who had used this promo code
    //In Params:     slug
    //Return:        json
    //Date:          30th Nov, 2018
    //###############################################################
    public function GetPromoDetail(Request $request){     
        try {
            $Input = Input::all();
            $Input['slug'] = explode(':', $Input['slug']);
            $promoCode = $Input['slug'][0];
            $promoCodeId = $Input['slug'][1];
            $promoUsedDetails = MarketPlacePayment::select('*',DB::RAW("DATE_FORMAT(created_at, '%m/%d/%Y %h:%i %p') as date"))->where('promo_code',$promoCode)->where('payment_status',1)->where('paid_for',3)->with('user_detail')->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))->toArray();
            if(!empty($promoUsedDetails)){
                $data['content'] = $promoUsedDetails;
                $data['promo_code'] = $promoCode;
                $data['promo_code_status'] = PromoCode::select('status')->where('id',$promoCodeId)->first();
                $data['promo_code_status'] = $data['promo_code_status']['status'];
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $data);
            } else
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 0);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: Getteamforcommissions
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to get team members for commission page search by team member
    //In Params:     void
    //Return:        json
    //Date:          3rd Dec, 2018
    //###############################################################
    public function Getteamforcommissions(Request $request){     
        try {
            $Input = Input::all();
            $distributorId = base64_decode(explode(':', $Input['slug'])[1]);
            $teamData = DistributorsTeamMembers::select('id','first_name','last_name')->where('distributor_id',$distributorId)->get();
            if(!empty($teamData)){
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $teamData);    
            }
        } catch (\Exception $e) {
            // $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getMessage(), '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: Gettypeandyear
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to get the type and year for commissions page
    //In Params:     void
    //Return:        json
    //Date:          15th Nov, 2018
    //###############################################################
    public function Gettypeandyear(Request $request){     
        try {
            $data['years'] = array_combine(range(date("Y"), 2015), range(date("Y"), 2015));
            $data['type'] = array('0' => 'Summary','1' => 'New Sales','2' =>'Renewal Sales');
            $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $data);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: Getcommissions
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to get distributors and team commissions
    //In Params:     void
    //Return:        json
    //Date:          31st Oct, 2018
    //###############################################################
    public function Getcommissions(Request $request){     
        try {
            $Input = Input::all();
            $distributorSlugtoId = base64_decode(explode(':', $Input['slug'])[1]);
            // if(Auth::guard('distributors')->check()||Auth::guard('distributor_team_members')->check()){
                if(array_key_exists('team_member', $Input) && isset($Input['team_member'])){
                    $distributors = DistributorsTeamMembers::where('id',$Input['team_member'])->first();
                } else {
                    $distributors = Distributors::where('id',$distributorSlugtoId)->first();
                }
                $distributorCommission        = $distributors['commission_percentage'];
                $distributorRenewalCommission = $distributors['renewal_commission_percentage'];

                $videoCategory = VideoCategory::select('id','category_name','category_price','promotional_price')->whereNull('parent_id')->get()->toArray();
                $monthTill = $Input['year']==Carbon::now()->format('Y')?Carbon::now()->format('m'):12;
                $static100 = 100;
                for ($i=1; $i <= $monthTill ; $i++) {
                    if($Input['type']==0)
                        $commissions[$i] = IgniteCommissions::where(!(array_key_exists('team_member', $Input) && isset($Input['team_member']))?'distributor_id':'distributer_team_member_id',$distributors['id'])->where('subscription_year',$Input['year']);
                    else{
                        $commissions[$i] = IgniteCommissions::where(!(array_key_exists('team_member', $Input) && isset($Input['team_member']))?'distributor_id':'distributer_team_member_id',$distributors['id'])->where('subscription_year',$Input['year'])->where('subscription_type',$Input['type']);
                    }
                    if(array_key_exists('team_member', $Input)&&isset($Input['team_member'])){
                        $commissions[$i] = $commissions[$i]->where('distributer_team_member_id',$Input['team_member']);
                    }
                    $commissions[$i] = $commissions[$i]->select('id','video_category_id','subscribers_count','subscription_type','subscription_month','distributor_commission_amount','team_member_commission_amount')
                        /*->with([
                        'video_details' => function($query) use($distributorCommission,$distributorRenewalCommission,$static100){
                            $query->select('id','category_name',DB::Raw("category_price*".$distributorCommission."/".$static100." as commissionPriceNew"),DB::Raw("category_price*".$distributorRenewalCommission."/".$static100." as commissionPriceRenew"));
                        }])*/
                    ->where('subscription_month',$i)->get()->toArray();
                }
                $totalYearCommission = 0;
                if($Input['type']==0){
                    foreach ($commissions as $key => $value){
                        $count = 0;
                        $commissions[$key]['totalNewCount'] = 0;
                        $commissions[$key]['totalRenewCount'] = 0;
                        $commissions[$key]['totalNewCountPrice'] = 0;
                        $commissions[$key]['totalRenewCountPrice'] = 0;
                        $commissions[$key]['current_month'] = date("M", mktime(0, 0, 0, $key, 10));
                        foreach ($value as $keyInner => $valueInner){
                            // $valueInner['subscription_type']==1?$commissionsNew[$key][] = $valueInner:$commissionsRenewal[$key][] = $valueInner;
                            $valueInner['subscription_type']==1?$commissions[$key]['totalNewCount'] += $valueInner['subscribers_count']:$commissions[$key]['totalRenewCount'] += $valueInner['subscribers_count'];
                            // $valueInner['subscription_type']==1?$commissions[$key]['totalNewCountPrice'] += $valueInner['video_details']['commissionPriceNew']:$commissions[$key]['totalRenewCountPrice'] += $valueInner['video_details']['commissionPriceRenew'];
                            if(array_key_exists('team_member', $Input)&&isset($Input['team_member'])){
                                $valueInner['subscription_type']==1?$commissions[$key]['totalNewCountPrice'] += $valueInner['team_member_commission_amount']:$commissions[$key]['totalRenewCountPrice'] += $valueInner['team_member_commission_amount'];
                            } else {
                                $valueInner['subscription_type']==1?$commissions[$key]['totalNewCountPrice'] += $valueInner['distributor_commission_amount']:$commissions[$key]['totalRenewCountPrice'] += $valueInner['distributor_commission_amount'];
                            }

                            $valueInner['subscription_type']==1?$commissions[$key]['totalNewCountPrice'] = round($commissions[$key]['totalNewCountPrice'],2):$commissions[$key]['totalRenewCountPrice'] = round($commissions[$key]['totalRenewCountPrice'],2);

                            // $totalYearCommission += $valueInner['subscription_type']==1?$valueInner['video_details']['commissionPriceNew']:$valueInner['video_details']['commissionPriceRenew'];
                            // $totalYearCommission = $valueInner['subscription_type']==1?$commissions[$key]['totalNewCountPrice']:$commissions[$key]['totalRenewCountPrice'];
                            if($valueInner['subscription_type']==1) {
                                $count += 1;
                            } else {
                                $count = 0;
                            }
                        }
                        if(count($value) == $count) {
                            $totalYearCommission += $commissions[$key]['totalNewCountPrice'];
                        }
                    }
                } else {
                    foreach ($commissions as $key => $value){
                        $count = 0;
                        if($Input['type']==1){
                            $commissions[$key]['totalNewCount'] = 0;
                            $commissions[$key]['totalNewCountPrice'] = 0;
                        } else {
                            $commissions[$key]['totalRenewCount'] = 0;
                            $commissions[$key]['totalRenewCountPrice'] = 0;
                        }
                        $commissions[$key]['current_month'] = date("M", mktime(0, 0, 0, $key, 10));
                        foreach ($value as $keyInner => $valueInner){
                            $Input['type']==1?$commissions[$key]['totalNewCount'] += $valueInner['subscribers_count']:$commissions[$key]['totalRenewCount'] += $valueInner['subscribers_count'];
                            // $Input['type']==1?$commissions[$key]['totalNewCountPrice'] += $valueInner['video_details']['commissionPriceNew']:$commissions[$key]['totalRenewCountPrice'] += $valueInner['video_details']['commissionPriceRenew'];
                            if(array_key_exists('team_member', $Input)&&isset($Input['team_member'])){
                                $Input['type']==1?$commissions[$key]['totalNewCountPrice'] += $valueInner['team_member_commission_amount']:$commissions[$key]['totalRenewCountPrice'] += $valueInner['team_member_commission_amount'];
                            } else {
                                $Input['type']==1?$commissions[$key]['totalNewCountPrice'] += $valueInner['distributor_commission_amount']:$commissions[$key]['totalRenewCountPrice'] += $valueInner['distributor_commission_amount'];
                            }

                            $Input['type']==1?$commissions[$key]['totalNewCountPrice'] = round($commissions[$key]['totalNewCountPrice'],2):$commissions[$key]['totalRenewCountPrice'] = round($commissions[$key]['totalRenewCountPrice'],2);

                            // $totalYearCommission += $Input['type']==1?$valueInner['video_details']['commissionPriceNew']:$valueInner['video_details']['commissionPriceRenew'];
                            // $totalYearCommission = $Input['type']==1?$commissions[$key]['totalNewCountPrice']:$commissions[$key]['totalRenewCountPrice'];
                            if($Input['type']==1) {
                                $count += 1;
                            } else {
                                $count = 0;
                            }

                            $commissions[$key]['counts'][$valueInner['video_category_id']][] = $valueInner['subscribers_count'];

                            // $commissions[$key]['counts'][$valueInner['video_category_id']]   = array_sum($commissions[$key]['counts'][$valueInner['video_category_id']]);
                        
                        }
                        if(count($value) == $count) {
                            if($Input['type']==1) {
                                $totalYearCommission += $commissions[$key]['totalNewCountPrice'];
                            } else {
                                $totalYearCommission += $commissions[$key]['totalRenewCountPrice'];
                            }
                        }
                        foreach ($videoCategory as $keyCategory => $valueCategory) {
                            if(array_key_exists('counts', $commissions[$key])&&!array_key_exists($valueCategory['id'], $commissions[$key]['counts']))
                                $commissions[$key]['counts'][$valueCategory['id']] = 0;
                        }
                    }
                }
                $data['commissions'] = $commissions;
                $totalYearCommission = round($totalYearCommission, 2);
                $data['totalYearCommission'] = $totalYearCommission;
                $data['current_domain'] = UtilityController::getMessage('CURRENT_DOMAIN');
                $data['video_categories'] = $videoCategory;
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $data);
            // }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getMessage().' - '.$e->getLine(), '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: Getignitetransactions
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To get listing of transactions done for ignite module
    //In Params:     void
    //Return:        json
    //Date:          24th Dec, 2018
    //###############################################################
    public function Getignitetransactions(Request $request){     
        try {
            $Input = Request::all();
            if(\Request::has('propertyName') && $Input['propertyName'] != '') {
                if($Input['propertyName'] == 'date') {
                    $transactions = MarketPlacePayment::select("*",DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as date"))->orderBy('created_at', $Input['sort'] == 'yes' ? 'asc' : 'desc')->where('paid_for',3)->paginate(Config('constants.other.MP_ADMIN_CATEGORY_PAGINATE'))->toArray();
                } else {
                    $transactions = MarketPlacePayment::select("*",DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as date"))->orderBy($Input['propertyName'], $Input['sort'] == 'yes' ? 'asc' : 'desc')->where('paid_for',3)->paginate(Config('constants.other.MP_ADMIN_CATEGORY_PAGINATE'))->toArray();
                }
            } else {
                $transactions = MarketPlacePayment::select("*",DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as date"))->orderBy('created_at', $Input['sort'] == 'yes' ? 'asc' : 'desc')->where('paid_for',3)->paginate(Config('constants.other.MP_ADMIN_CATEGORY_PAGINATE'))->toArray();
            }
            $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $transactions);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name :   ExportTransactions
    //Author :          Senil Shah <senil@creolestudios.com>
    //Purpose :         To export Transaction data
    //In Params :       status type, search value (if any), export type
    //Return :          json
    //Date :            21st Nov, 2018
    //###############################################################
    public function ExportTransactions(Request $request){
        try {
            $Input = Request::all();
            if(Auth::guard('admins')->check()){
                /*if($Input['search_by']=='undefined'||$Input['search_by']=='')
                    unset($Input['search_by']);*/
                $allTransactionData = MarketPlacePayment::select("*",DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y') as date"))->where('paid_for',3);
                /*if(array_key_exists('search_by', $Input)){
                    $promoDistributors = Distributors::where('company_name','like','%'.$Input['search_by'].'%')->get()->pluck('id')->toArray();
                    $promoCategories = VideoCategory::where('category_name','like','%'.$Input['search_by'].'%')->get()->pluck('id')->toArray();
                    $allPromoData = $allPromoData->where('promo_code','like','%'.$Input['search_by'].'%')->orWhereIn('distributor_id',$promoDistributors)->orWhereIn('video_category_id',$promoCategories);
                }*/
                $allTransactionData = $allTransactionData->get()->toArray();
                $dataArray[] = ['S/N', 'Invoice', 'Subscription Plan', 'Amount', 'Date', 'Status'];
                $dataArray = self::createExportData($allTransactionData,$dataArray,2,'Ignite Transactions','Ignite Transactions','Ignite Transactions',$Input['export_type']);
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $allTransactionData);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name: Getcountryforzones
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To get countries for zones according to matter that 1 country will appear in only 1 zone
    //In Params:     void
    //Return:        json
    //Date:          28th Jan, 2018
    //###############################################################
    public function Getcountryforzones(Request $request){
       try {
           if(Auth::guard('admins')->check()){
               $getUsedCountries = Zone::get()->pluck('country_id')->toArray();
               $getUsedCountries = explode(',', implode(',', $getUsedCountries));
               if(!empty($getUsedCountries))
                   $countryList = Country::whereNotIn('id',$getUsedCountries)->orderBy('name')->get();
               else
                   $countryList = Country::orderBy('name')->get();
               $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', Response::HTTP_OK, $countryList);
           } else {
               $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
           }
       } catch (\Exception $e) {
           $sendExceptionMail = UtilityController::Sendexceptionmail($e);
           $returnData        = UtilityController::Generateresponse(false, $e->getMessage(), Response::HTTP_BAD_REQUEST);
       }
       return response($returnData);
   }

    //###############################################################
    //Function Name: Getlanguagesforzones
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To get languages for zones
    //In Params:     void
    //Return:        json
    //Date:          28th Jan, 2018
    //###############################################################
    public function Getlanguagesforzones(Request $request){
        try {
            if(Auth::guard('admins')->check()){
                $languageList = ZoneLanguage::get();
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', Response::HTTP_OK, $languageList);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, $e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
        return response($returnData);
    }

    //###############################################################
    //Function Name: Getzones
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To get available zones (id any)
    //In Params:     void
    //Return:        json
    //Date:          28th Jan, 2018
    //###############################################################
    public function Getzones(Request $request){
        try {
            if(Auth::guard('admins')->check()){
                $data['zones'] = Zone::get();
                $data['zones'] = $data['zones']->map(function($value, $key) {
                    $data['country_id']        = $value['country_id'];
                    $data['audio_language']    = $value['audio_language'];
                    $data['subtitle_language'] = $value['subtitle_language'];
                    $data['countries']         = 'countries_'.$value['id'];
                    $data['id']                = $value['id'];
                    $data['zone_name']         = $value['zone_name'];
                    $data['exceptCountries']   = Country::whereNotIn('id',explode(',', implode(',', Zone::where('id','!=',$value['id'])->get()->pluck('country_id')->toArray())))->orderBy('name')->get() ;
                    $data['allow_subscription'] = $value['is_subscription_allowed'];
                    return $data;
                });
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', Response::HTTP_OK, $data);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, $e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
        return response($returnData);
    }

    //###############################################################
    //Function Name: Savenewzone
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To add a new zone based on country, audio language and subtitle selection
    //In Params:     selected country, selected audio languages and selected subtitle languages
    //Return:        json
    //Date:          28th Jan, 2018
    //###############################################################
    public function Savenewzone(Request $request){
        try {
            if(Auth::guard('admins')->check()){
                $Input = Input::all();
                if(!array_key_exists('zone_name', $Input) || $Input['zone_name']=='')
                    throw new \Exception("'Zone Name' required");
                if(!array_key_exists('country', $Input))
                    throw new \Exception("'Country' required");
                if(!array_key_exists('allow_subscription', $Input))
                    throw new \Exception("'Subscription Status' required");
                
                $zone['country_id']        = implode(',', $Input['country']);
                $zone['zone_name']         = $Input['zone_name'];
                $zone['is_subscription_allowed'] = $Input['allow_subscription'];
                DB::beginTransaction();
                if($Input['whatType']==0){
                    $newZoneResult = UtilityController::Makemodelobject($zone,'Zone');
                } else {
                    $newZoneResult = UtilityController::Makemodelobject($zone,'Zone','',$Input['zoneNumber']);
                }
                if(!empty($newZoneResult)){
                    $returnData = UtilityController::Generateresponse(true, 'ZONE_CREATED', Response::HTTP_OK);
                    DB::commit();
                } else {
                    $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', Response::HTTP_PRECONDITION_FAILED);
                }
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            DB::rollback();
            // $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, $e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
        return response($returnData);
    }

    //###############################################################
    //Function Name: Deletezone
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To delete zone based on id
    //In Params:     zone id
    //Return:        json
    //Date:          29th Jan, 2018
    //###############################################################
    public function Deletezone(Request $request){
        try {
            if(Auth::guard('admins')->check()){
                $Input = Input::all();
                Zone::where('id',$Input['zone_id'])->forceDelete();
                $returnData = UtilityController::Generateresponse(true, 'ZONE_DELETED', Response::HTTP_OK);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, $e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
        return response($returnData);
    }

    //###############################################################
    //Function Name: Savenewcategory
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To add a new category
    //In Params:     category details
    //Return:        json
    //Date:          30th Jan, 2018
    //###############################################################
    public function Savenewcategory(Request $request){
        try {
            if(Auth::guard('admins')->check()){
                $Input = Input::all();
                if(!array_key_exists('subject_name', $Input) || $Input['subject_name']=='')
                    throw new \Exception("'Subject Name' required");
                if(!array_key_exists('subject_display_name', $Input) || $Input['subject_display_name']=='')
                    throw new \Exception("'Subject Display Name' required");
                if(!array_key_exists('category', $Input))
                    throw new \Exception("'Categories' required");
                if(!array_key_exists('zon', $Input))
                    throw new \Exception("'Zone' required");
                DB::beginTransaction();
                if($Input['whatType']==1){
                    // videoCategory::where('subject_id',$Input['zoneNumber'])->whereNotNull('parent_id')->forceDelete();
                }
                $subject['audio_language']    = implode(',', $Input['audio']);
                $subject['zones']             = implode(',', $Input['zon']);
                // $subject['subtitle_language'] = implode(',', $Input['subtitle']);
                $subject['subject_name'] = $Input['subject_name'];
                $subject['subject_display_name'] = $Input['subject_display_name'];
                if(array_key_exists('subject_image', $Input)){
                    if (!empty($Input['subject_image']) && !is_string($Input['subject_image'])) {
                        /*if ($Input['subject_image']->getClientSize() <= UtilityController::Getmessage('FILE_SIZE_2')) {*/
                        $extension = \File::extension($Input['subject_image']->getClientOriginalName());
                        if (in_array($extension, array('gif', 'GIF', 'jpeg', 'JPEG', 'jpg', 'JPG', 'png', 'PNG'))) {
                            $imageName       = date("dmYHis").rand().".$extension";
                            $Input['subject_image']->move(public_path('') . UtilityController::Getpath('SUBJECT_IMAGE_PATH'), $imageName);
                            $permitPath = public_path().UtilityController::Getpath('SUBJECT_IMAGE_PATH');
                            $path = public_path().UtilityController::Getpath('SUBJECT_IMAGE_PATH').$imageName;
                            Image::make($path)->save($path);
                            chmod($path, 0777);
                            $Input['subject_image']    = $imageName;
                        } else {
                            throw new \Exception(UtilityController::Getmessage('ONLY_GIF_JPEG_JPG_PNG'));
                        }
                        /*} else {
                            throw new \Exception($Input['subject_image']->getClientOriginalName() . UtilityController::Getmessage('THUMBNAIL_SIZE_LARGE'));
                        }*/
                    }
                }
                if(isset($Input['subject_image']) && !is_null($Input['subject_image']) && !empty($Input['subject_image'])) {
                    $subject['subject_image'] = $Input['subject_image'];
                }/* else {
                    $subject['subject_image'] = 'mazz.png';
                }*/
                $subject['status'] = 1;

                $subjectResult = $Input['whatType']==0?UtilityController::Makemodelobject($subject,'Subject'):UtilityController::Makemodelobject($subject,'Subject','',$Input['zoneNumber']);
                if(!empty($subjectResult)){
                    // print_r($Input['category']);die;
                    foreach ($Input['category'] as $key => $value) {
                        if(!array_key_exists('bundle_category_id', $value) || $value['bundle_category_id']=='')
                            throw new \Exception("'Category Bundle Identifier' required");
                        if(!array_key_exists('category', $value)||$value['category']=='')
                            throw new \Exception("'Category Name' required in category #".preg_match_all('!\d+!', $key, $key));

                        if(array_key_exists('category_icon', $value)){
                            if (!empty($value['category_icon']) && !is_string($value['category_icon'])) {
                                /*if ($value['category_icon']->getClientSize() <= UtilityController::Getmessage('FILE_SIZE_2')) {*/
                                $extension = \File::extension($value['category_icon']->getClientOriginalName());
                                if (in_array($extension, array('svg','SVG','jpeg', 'JPEG', 'jpg', 'JPG', 'png', 'PNG'))) {
                                    $imageName       = date("dmYHis").rand().".$extension";
                                    $value['category_icon']->move(public_path('') . UtilityController::Getpath('ICON_PATH'), $imageName);
                                    $permitPath = public_path().UtilityController::Getpath('ICON_PATH');
                                    $path = public_path().UtilityController::Getpath('ICON_PATH').$imageName;
                                    // Image::make($path)->save($path);
                                    chmod($path, 0777);
                                    $value['category_icon']    = $imageName;
                                } else {
                                    throw new \Exception(UtilityController::Getmessage('ONLY_JPG_PNG_JPEG'));
                                }
                                /*} else {
                                    throw new \Exception($value['category_icon']->getClientOriginalName() . UtilityController::Getmessage('THUMBNAIL_SIZE_LARGE'));
                                }*/
                            }
                        }

                        if(array_key_exists('category_gif_url', $value)){
                            if (!empty($value['category_gif_url']) && !is_string($value['category_gif_url'])) {
                                /*if ($value['category_gif_url']->getClientSize() <= UtilityController::Getmessage('FILE_SIZE_2')) {*/
                                $extension = \File::extension($value['category_gif_url']->getClientOriginalName());
                                if (in_array($extension, array('gif', 'GIF', 'jpeg', 'JPEG', 'jpg', 'JPG', 'png', 'PNG'))) {
                                    $imageName       = date("dmYHis").rand().".$extension";
                                    $value['category_gif_url']->move(public_path('') . UtilityController::Getpath('GIF_PATH'), $imageName);
                                    $permitPath = public_path().UtilityController::Getpath('GIF_PATH');
                                    $path = public_path().UtilityController::Getpath('GIF_PATH').$imageName;
                                    // Image::make($path)->save($path);
                                    chmod($path, 0777);
                                    $value['category_gif_url']    = $imageName;
                                } else {
                                    throw new \Exception(UtilityController::Getmessage('ONLY_GIF_JPEG_JPG_PNG'));
                                }
                                /*} else {
                                    throw new \Exception($value['category_gif_url']->getClientOriginalName() . UtilityController::Getmessage('THUMBNAIL_SIZE_LARGE'));
                                }*/
                            }
                        }
                        $category['subject_id']      = $subjectResult['id'];
                        $category['category_name']   = $value['category'];
                        $category['category_name_chi']   = isset($value['category_chi'])?$value['category_chi']:'';
                        $category['category_name_ru']   = isset($value['category_ru'])?$value['category_ru']:'';
                        $category['bundle_category_id'] = $value['bundle_category_id'];
                        $category['status']          = 1;
                        $category['description']     = $value['description'];
                        $category['description_chi'] = $value['description_chi'];
                        $category['description_ru'] = $value['description_ru'];
                        $category['category_price']  = $value['category_price'];
                        $category['category_price_sgd']  = $value['category_price_sgd'];
                        $category['category_price_6month']      = $value['category_price_6month'];
                        $category['category_price_sgd_6month']  = $value['category_price_sgd_6month'];
                        $category['category_price_3month']      = $value['category_price_3month'];
                        $category['category_price_sgd_3month']  = $value['category_price_sgd_3month'];
                         if(isset($value['category_icon']) && !is_null($value['category_icon']) && !empty($value['category_icon'])) {
                            $category['category_icon']   = $value['category_icon'];
                        }
                        if(isset($value['category_gif_url']) && !is_null($value['category_gif_url']) && !empty($value['category_gif_url'])) {
                            $category['category_gif_url']   = $value['category_gif_url'];
                        }
                        /* else {
                            $category['category_gif_url']   = 'Maze 1.gif';
                        }*/
                        $category['created_at']      = Carbon::now();
                        $category['updated_at']      = Carbon::now();
                        // $categoryResult              = UtilityController::Makemodelobject($category,'VideoCategory');
                        if(array_key_exists('id', $value)){
                            $categoryWhatType = explode('_', $value['id']);
                            $categoryWhatType = (count($categoryWhatType)>1?0:1);
                        } else {
                            $categoryWhatType = 0;
                        }
                        if($categoryWhatType==1 && count(explode('_', $value['id']))==1 ){
                            $categoriesToKeep[] = $value['id'];
                        }
                        /* New */
                        $videoCategoryDetails = $categoryWhatType==0?'':VideoCategory::where('id',$value['id'])->get()->toArray();
                        /* New End */
                        $categoryResult = $categoryWhatType==0?UtilityController::Makemodelobject($category,'VideoCategory'):UtilityController::Makemodelobject($category,'VideoCategory','',$value['id']);
                        unset($category['category_gif_url']);
                        unset($category['category_icon']);

                        /* New */
                        ## Admin id
                        $AdminId = Auth::guard('admins')->user()->id;
                        ## Video Categoies Price History are manage
                        $categoryHistory['category_id'] = $categoryResult['id'];
                        $categoryHistory['category_name'] = $categoryResult['category_name']; 
                        $categoryHistory['category_price'] = $categoryResult['category_price'];
                        $categoryHistory['category_sgd_price'] = $categoryResult['category_price_sgd'];
                        $categoryHistory['category_price_3month'] = $categoryResult['category_price_3month'];
                        $categoryHistory['category_sgd_price_3month'] = $categoryResult['category_price_sgd_3month'];
                        $categoryHistory['category_price_6month'] = $categoryResult['category_price_6month'];
                        $categoryHistory['category_sgd_price_6month'] = $categoryResult['category_price_sgd_6month'];
                        $categoryHistory['admin_id'] = $AdminId;

                        if($categoryResult['id']){
                            if(!(VideoCategoryHistory::where('category_id',$categoryResult['id'])->exists())){
                                $categoryHistoryResult = UtilityController::Makemodelobject($categoryHistory,'VideoCategoryHistory');
                            } elseif (!($videoCategoryDetails[0]['category_price'] == $categoryHistory['category_price'] && $videoCategoryDetails[0]['category_price_sgd'] == $categoryHistory['category_sgd_price'] && $videoCategoryDetails[0]['category_price_6month'] == $categoryHistory['category_price_6month'] && $videoCategoryDetails[0]['category_price_sgd_6month'] == $categoryHistory['category_sgd_price_6month'] && $videoCategoryDetails[0]['category_price_3month'] == $categoryHistory['category_price_3month'] && $videoCategoryDetails[0]['category_price_sgd_3month'] == $categoryHistory['category_sgd_price_3month'])) {
                                $categoryHistoryResult = UtilityController::Makemodelobject($categoryHistory,'VideoCategoryHistory');
                            }
                        }
                        /* New End */

                        $categoryResult['parent_id'] = $categoryResult['id'];
                        if($categoryWhatType==0)
                            $categoriesToKeep[] = $categoryResult['id'];
                        $parentId[] = $categoryResult['id'];
                        unset($categoryResult['id']);
                        
                        //print_r($value['subcategory'][ 0 ]);
                        // echo("<pre>");print_r($value);echo("</pre>"); 
                        $tempArray = [];
                        $i = 0;
                        foreach ($value['subcategory'] as $tempkeyInner => $tempvalueInner) {
                            if($tempvalueInner['name'] != '' || $tempvalueInner['subcategory_description'] != '' || $tempvalueInner['name_chi'] != '' || $tempvalueInner['subcategory_description_chi'] != '' || $tempvalueInner['name_ru'] != '' || $tempvalueInner['subcategory_description_ru'] != ''){
                                $tempArray[$i] = $tempvalueInner;
                                $i++;
                            }
                        }
                        // echo("<pre>");print_r($tempArray);echo("</pre>"); 
                        // foreach ($value['subcategory'] as $keyInner => $valueInner) {
                        foreach ($tempArray as $keyInner => $valueInner) {
                            if(array_key_exists('sub_category_icon', $valueInner)){
                                if (!empty($valueInner['sub_category_icon']) && !is_string($valueInner['sub_category_icon'])) {
                                    $extension = \File::extension($valueInner['sub_category_icon']->getClientOriginalName());
                                    if (in_array($extension, array('svg','SVG','gif', 'GIF', 'jpeg', 'JPEG', 'jpg', 'JPG', 'png', 'PNG'))) {
                                        $imageName       = date("dmYHis").rand().".$extension";
                                        $valueInner['sub_category_icon']->move(public_path('') . UtilityController::Getpath('ICON_PATH'), $imageName);
                                        $permitPath = public_path().UtilityController::Getpath('ICON_PATH');
                                        $path = public_path().UtilityController::Getpath('ICON_PATH').$imageName;
                                        Image::make($path)->save($path);
                                        chmod($path, 0777);
                                        $valueInner['sub_category_icon'] = $imageName;
                                    } else {
                                        throw new \Exception(UtilityController::Getmessage('ONLY_GIF_JPEG_JPG_PNG'));
                                    }
                                }
                            }
                            $subcategory[$keyInner] = $categoryResult->toArray();
                            if(array_key_exists('id', $valueInner)){
                                // print_r('yesss');die;
                                // print_r($subcategory[$keyInner]);
                                $subcategory[$keyInner]['id'] = $valueInner['id'];

                                if(isset($valueInner['name']))
                                    $subcategory[$keyInner]['subcategory_name'] = $valueInner['name'];
                                if(isset($valueInner['name_chi']))
                                    $subcategory[$keyInner]['subcategory_name_chi'] = $valueInner['name_chi'];
                                if(isset($valueInner['name_ru']))
                                    $subcategory[$keyInner]['subcategory_name_ru'] = $valueInner['name_ru'];
                                if(isset($valueInner['subcategory_description']) && $valueInner['subcategory_description']!=''){
                                    $subcategory[$keyInner]['subcategory_description'] = $valueInner['subcategory_description'];
                                }else{
                                    $subcategory[$keyInner]['subcategory_description'] = NULL;
                                }
                                if(isset($valueInner['subcategory_description_chi']))
                                    $subcategory[$keyInner]['subcategory_description_chi'] = $valueInner['subcategory_description_chi'];
                                if(isset($valueInner['subcategory_description_ru']))
                                    $subcategory[$keyInner]['subcategory_description_ru'] = $valueInner['subcategory_description_ru'];  
                                if(isset($valueInner['sub_category_icon']) && !is_null($valueInner['sub_category_icon']) && !empty($valueInner['sub_category_icon'])) {
                                    $subcategory[$keyInner]['sub_category_icon'] = $valueInner['sub_category_icon']; 
                                }
                                if(isset($valueInner['on_landing_page']) && $valueInner['on_landing_page']!=''){
                                    $subcategory[$keyInner]['on_landing_page'] = 1; 
                                }else{
                                    $subcategory[$keyInner]['on_landing_page'] = 0; 
                                }
                                if(isset($valueInner['on_detail_page']) && $valueInner['on_detail_page']!=''){
                                    $subcategory[$keyInner]['on_detail_page'] = 1; 
                                }else{
                                    $subcategory[$keyInner]['on_detail_page'] = 0; 
                                }
                            }else{
                                
                                if(isset($valueInner['name']))
                                    $subcategory[$keyInner]['subcategory_name'] = $valueInner['name'];
                                if(isset($valueInner['name_chi']))
                                    $subcategory[$keyInner]['subcategory_name_chi'] = $valueInner['name_chi'];
                                if(isset($valueInner['name_ru']))
                                    $subcategory[$keyInner]['subcategory_name_ru'] = $valueInner['name_ru'];
                                if(isset($valueInner['subcategory_description']))
                                    $subcategory[$keyInner]['subcategory_description'] = $valueInner['subcategory_description'];
                                if(isset($valueInner['subcategory_description_chi']))
                                    $subcategory[$keyInner]['subcategory_description_chi'] = $valueInner['subcategory_description_chi'];
                                if(isset($valueInner['subcategory_description_ru']))
                                    $subcategory[$keyInner]['subcategory_description_ru'] = $valueInner['subcategory_description_ru'];  
                                if(isset($valueInner['sub_category_icon']) && !is_null($valueInner['sub_category_icon']) && !empty($valueInner['sub_category_icon'])) {
                                    $subcategory[$keyInner]['sub_category_icon'] = $valueInner['sub_category_icon']; 
                                }
                                if(isset($valueInner['on_landing_page']) && $valueInner['on_landing_page']!=''){
                                    $subcategory[$keyInner]['on_landing_page'] = 1; 
                                }else{
                                    $subcategory[$keyInner]['on_landing_page'] = 0; 
                                }
                                if(isset($valueInner['on_detail_page']) && $valueInner['on_detail_page']!=''){
                                    $subcategory[$keyInner]['on_detail_page'] = 1; 
                                }else{
                                    $subcategory[$keyInner]['on_detail_page'] = 0; 
                                }
                                // if(isset($valueInner['on_landing_page'])){
                                //     $subcategory[$keyInner]['on_landing_page'] = ($valueInner['on_landing_page']==1?1:0); 
                                // }
                                // if(isset($valueInner['on_detail_page'])){
                                //     $subcategory[$keyInner]['on_detail_page'] = ($valueInner['on_detail_page']==1?1:0);  
                                // }
                            }
                        }
                        // die;
                        if(!empty($subcategory))
                            // print_r($subcategory);
                            $subcategoryAll[] = $subcategory;
                            $subcategory=[];
                    }
                    $videoCategoryId = VideoCategory::select('id')->whereIn('parent_id', $parentId)->get()->toArray();
                    if(!empty($subcategoryAll)){
                        foreach ($subcategoryAll as $key => $value) {
                            foreach ($value as $keyInner => $valueInner) {
                                // print_r($valueInner);
                                // if($valueInner['subcategory_name'] != '') {
                                //     $allSubcategory[] = $valueInner;
                                // }
                                if(isset($valueInner['sub_category_icon']) && $valueInner['sub_category_icon']==''){
                                    unset($valueInner['sub_category_icon']);
                                }
                                $allSubcategory[] = $valueInner;
                            }

                        }
                        // die;
                        if(isset($allSubcategory)) {
                            // print_r($allSubcategory);die;
                            $allSubCatId = collect(videoCategory::select('id')->where('subject_id',$Input['zoneNumber'])->whereNotNull('parent_id')->get())->toArray();
                            foreach($allSubcategory as $key => $value) {
                                // if($Input['whatType']==0) {
                                // print_r($value);
                                if(!array_key_exists('id', $value)) {
                                    $allUpdateData[] = UtilityController::Makemodelobject($value,'VideoCategory');
                                } else {
                                    if(VideoCategory::where('id', $value['id'])->exists()) {
                                        $value['subcategory_name']=strlen($value['subcategory_name'])==0?'':$value['subcategory_name'];
                                        $value['subcategory_name_chi']=strlen($value['subcategory_name_chi'])==0?'':$value['subcategory_name_chi'];
                                        $value['subcategory_name_ru']=strlen($value['subcategory_name_ru'])==0?'':$value['subcategory_name_ru'];

                                        $value['subcategory_description']=strlen($value['subcategory_description'])==0?'':$value['subcategory_description'];
                                        $value['subcategory_description_chi']=strlen($value['subcategory_description_chi'])==0?'':$value['subcategory_description_chi'];
                                        $value['subcategory_description_ru']=strlen($value['subcategory_description_ru'])==0?'':$value['subcategory_description_ru'];
                                        // print_r($value);
                                        // $allUpdateData[] = VideoCategory::where('id',$value['id'])->update($value);
                                        $allUpdateData[] = UtilityController::Makemodelobject($value,'VideoCategory', '', $value['id']);
                                        // print_r($allUpdateData);
                                    } else {
                                        $allUpdateData[] = UtilityController::Makemodelobject($value,'VideoCategory');
                                    }
                                }
                            }
                            
                            // die;
                            $collection = collect($allUpdateData);
                            // print_r($collection->toArray());die;
                            $updatedId = $collection->map(function ($item) {
                                return ['id' => $item['id']];
                            })->toArray();
                            $allSubCatId = array_column($allSubCatId, 'id');
                            $updatedId = array_column($updatedId, 'id');
                            $deleteId = array_diff($allSubCatId, $updatedId);
                            VideoCategory::whereIn('id', $deleteId)->forceDelete();
                        }
                    } else {
                        $videoCategoryId = array_column($videoCategoryId, 'id');
                        VideoCategory::whereIn('id', $videoCategoryId)->forceDelete();
                    }
                    if(!empty($categoriesToKeep)&&$Input['whatType']==1){
                        $deleteIDs = VideoCategory::where('subject_id',$Input['zoneNumber'])->whereNotIn('id', $categoriesToKeep)->whereNull('parent_id')->get()->pluck('id')->toArray();
                        if(!empty($deleteIDs)){
                            VideoCategory::where('subject_id',$Input['zoneNumber'])->whereIn('id', $deleteIDs)->forceDelete();
                            VideoCategory::where('subject_id',$Input['zoneNumber'])->whereIn('parent_id', $deleteIDs)->forceDelete();
                        }
                    }
                    $returnData = UtilityController::Generateresponse(true, 'ZONE_CREATED', Response::HTTP_OK);
                    DB::commit();
                } else {
                    $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', Response::HTTP_PRECONDITION_FAILED);
                }
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, $e->getMessage().' - '.$e->getLine(), Response::HTTP_BAD_REQUEST);
        }
        return response($returnData);
    }

    //###############################################################
    //Function Name: Getcategories
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To get available zones (id any)
    //In Params:     void
    //Return:        json
    //Date:          28th Jan, 2018
    //###############################################################
    public function Getcategories(Request $request){
        try {
            $Input = Input::all();
            if(Auth::guard('admins')->check()){
                if(array_key_exists('zone_id', $Input)) {
                    $userZoneId = $Input['zone_id'];
                    $subject = Subject::all();
                    $subjectIdCount = 0;
                    foreach($subject as $sub) {
                        $zones = explode(',', $sub['zones']);
                        if(in_array($userZoneId, $zones)) {
                            $subjectId[] = $sub['id'];
                            $subjectIdCount = 1;
                        }   
                    }
                    if($subjectIdCount == 1) {
                        $data['categories'] = Subject::with([
                            'categories' => function($query){
                                $query->select('*')->with('subcategories')->whereNull('parent_id');
                            }
                        ])->where('status',1)->whereIn('id', $subjectId)->get();
                    } else {
                        $data['categories'] = null;
                    }
                } else {
                    $data['categories'] = Subject::with([
                        'categories' => function($query){
                            $query->select('*')->with('subcategories')->whereNull('parent_id');
                        }
                    ])->where('status',1)->get();
                }
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', Response::HTTP_OK, $data);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, $e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
        return response($returnData);
    }

    //###############################################################
    //Function Name: addOrEditBundleCategoryId
    //Author:        Sumit <sumit@creolestudios.com>
    //Purpose:       To get random string for bundle category ID
    //Return:        json
    //Date:          4th July, 2019
    //###############################################################    
    public function addOrEditBundleCategoryId() {
        //$data = "net.sixclouds.ignite.ios.".strtotime(date('Y-m-dH:i:s')).".";
        $data = "net.sixclouds.ignite.ios.".strtotime(date('Y-m-dH:i:s'));
        $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', Response::HTTP_OK, $data);
        return $returnData;
    }

    //###############################################################
    //Function Name: Deletesubject
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To delete subject and its related category based on id
    //In Params:     subject id
    //Return:        json
    //Date:          29th Jan, 2018
    //###############################################################
    public function Deletesubject(Request $request){
        try {
            if(Auth::guard('admins')->check()){
                $Input = Input::all();
                Subject::where('id',$Input['subject_id'])->forceDelete();
                videoCategory::where('subject_id',$Input['subject_id'])->forceDelete();
                $returnData = UtilityController::Generateresponse(true, 'ZONE_DELETED', Response::HTTP_OK);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, $e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
        return response($returnData);
    }
    
    //###############################################################
    //Function Name: Loadcategoriesforsubject
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To get the categories for subject
    //In Params:     subject id
    //Return:        json
    //Date:          1st Feb, 2019
    //###############################################################
    public function Loadcategoriesforsubject(Request $request){
        try {
            if(Auth::guard('admins')->check()){
                $Input = Input::all();
                $subjects = Subject::where('id',$Input['subject_id'])->get();
                $categories = VideoCategory::where('subject_id',$Input['subject_id'])->whereNull('parent_id')->where('status',1)->get();

                if (array_key_exists('for', $Input)&&$Input['for']=='quiz') {
                    $languages = self::Getzonesforlanguageinquiz($Input);
                } elseif (array_key_exists('for', $Input)&&$Input['for']=='worksheet') {
                    $languages = self::Getzonesforlanguageinworksheet($Input);
                } else {
                    $languages = self::Getzonesforlanguage($Input);
                    if(array_key_exists('isEdit', $Input) && $Input['isEdit']==1 && !empty($Input['removeVideo'])){
                        $languages = $languages->toArray();
                        $addforLanguage = [];
                        foreach ($languages as $key => $value) {
                            foreach ($Input['removeVideo'] as $keyInner => $valueInner) {
                                if($valueInner==$value['language'])
                                    $addforLanguage[] = $value;
                            }
                        }
                        $languages = $addforLanguage;
                    }
                    if(array_key_exists('isEdit', $Input) && $Input['isEdit']==1 && !empty($Input['existingVideos'])){
                        $languages = $languages->toArray();
                        $addforLanguage = [];
                        foreach ($languages as $key => $value) {
                            foreach ($Input['existingVideos'] as $keyInner => $valueInner) {
                                if($valueInner['video_language']==$value['language'])
                                    unset($languages[$key]);
                            }
                        }
                    }
                }

                $categoryAndLanguages['categories'] = $categories;
                $categoryAndLanguages['languages'] = $languages;
                $categoryAndLanguages['subjects'] = $subjects;
                $returnData = UtilityController::Generateresponse(true, 'ZONE_DELETED', Response::HTTP_OK,$categoryAndLanguages);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, $e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
        return response($returnData);
    }

    //###############################################################
    //Function Name: Loadlanguagesforzones
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To get the languages for selected zones for subject
    //In Params:     selected zones array
    //Return:        json
    //Date:          1st Feb, 2019
    //###############################################################
    public function Loadlanguagesforzones(Request $request){
        try {
            if(Auth::guard('admins')->check()){
                $Input = Input::all();
                if (array_key_exists('for', $Input)&&$Input['for']=='quiz') {
                    $languages = self::Getzonesforlanguageinquiz($Input);
                } elseif (array_key_exists('for', $Input)&&$Input['for']=='worksheet') {
                    $languages = self::Getzonesforlanguageinworksheet($Input);
                } else {
                    $languages = self::Getzonesforlanguage($Input);
                    if(array_key_exists('isEdit', $Input) && $Input['isEdit']==1 && !empty($Input['removeVideo'])){
                        $languages = $languages->toArray();
                        $addforLanguage = [];
                        foreach ($languages as $key => $value) {
                            foreach ($Input['removeVideo'] as $keyInner => $valueInner) {
                                if($valueInner==$value['language'])
                                    $addforLanguage[] = $value;
                            }
                        }
                        $languages = $addforLanguage;
                    }
                    if(array_key_exists('isEdit', $Input) && $Input['isEdit']==1 && !empty($Input['existingVideos'])){
                        $languages = $languages->toArray();
                        $addforLanguage = [];
                        foreach ($languages as $key => $value) {
                            foreach ($Input['existingVideos'] as $keyInner => $valueInner) {
                                if($valueInner['video_language']==$value['language'])
                                    unset($languages[$key]);
                            }
                        }
                    }
                }
                $returnData = UtilityController::Generateresponse(true, 'ZONE_DELETED', Response::HTTP_OK,$languages);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, $e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
        return response($returnData);
    }

    //###############################################################
    //Function Name: Getzonesforlanguage
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       common function for Editvideo and Loadlanguagesforzones
    //In Params:     
    //Return:        json
    //Date:          6th Feb, 2019
    //###############################################################
    public function Getzonesforlanguage($Input){     
        try {
            $languages = Subject::where('id',$Input['subject_id'])->get()->pluck('audio_language');
            $data = '';
            $languageMergeString = $languages->map(function($value, $key) use($data) {
                $data .= $value;
                return $data;
            });
            $languageMergeArray =  array_unique(explode(',', implode(',', $languageMergeString->toArray())));
            $languages = ZoneLanguage::whereIn('id',$languageMergeArray)->get();
            return $languages;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }            
    }
    //###############################################################
    //Function Name: Getzonesforlanguageinquiz
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       common function for quiz zone language
    //In Params:     
    //Return:        json
    //Date:          25th Feb, 2019
    //###############################################################
    public function Getzonesforlanguageinquiz($Input){     
        try {
            $languages = Subject::where('id',$Input['subject_id'])->get()->pluck('audio_language')->toArray();
            $languageMergeArray =  array_unique(explode(',', implode(',', $languages)));
            $languages = ZoneLanguage::whereIn('id',$languageMergeArray)->get()->toArray();
            // $alreadyAdded = QuizLanguage::where('quiz_id',$Input['quiz_id'])->get()->toArray();
            foreach ($languages as $key => $value) {
                foreach ($alreadyAdded as $keyInner => $valueInner) {
                    $value['title']       = '';
                    $value['description'] = '';
                    if($value['language'] == $valueInner['language']){
                        $languages[$key]['title'] = $valueInner['title'];
                        $languages[$key]['description'] = $valueInner['description'];
                    }
                }
            }
            return $languages;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }            
    }
    //###############################################################
    //Function Name: Getzonesforlanguageinworksheet
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       common function for worksheet zone language
    //In Params:     
    //Return:        json
    //Date:          25th Feb, 2019
    //###############################################################
    public function Getzonesforlanguageinworksheet($Input){     
        try {
            $languages = Subject::where('id',$Input['subject_id'])->get()->pluck('audio_language')->toArray();
            $languageMergeArray =  array_unique(explode(',', implode(',', $languages)));
            $languages = ZoneLanguage::whereIn('id',$languageMergeArray)->get()->toArray();
            // $alreadyAdded = WorksheetLanguage::where('worksheet_id',$Input['worksheet_id'])->get()->toArray();
            foreach ($languages as $key => $value) {
                foreach ($alreadyAdded as $keyInner => $valueInner) {
                    $value['title']       = '';
                    $value['description'] = '';
                    if($value['language'] == $valueInner['language']){
                        $languages[$key]['title'] = $valueInner['title'];
                        $languages[$key]['description'] = $valueInner['description'];
                    }
                }
            }
            return $languages;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }            
    }

    //###############################################################
    //Function Name: Getsubcategories
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       Get the sub-categories for the subscriber to select from and load its sub-categories if exists or redirect to video listing page
    //In Params:     void
    //Return:        json
    //Date:          4th Feb, 2018
    //###############################################################
    public function Getsubcategories(Request $request){     
        try {
            if(Auth::guard('admins')->check()){
                $Input = Input::all();
                $subCategories = VideoCategory::where('parent_id',$Input['id'])->where('status',1)->whereNotNull('parent_id')->get();
                if($subCategories->count()>0)
                    $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $subCategories);
                else{
                    if($Input['for'] == 'Video') {
                        $videoExist = Videos::where('video_category_id',$Input['id'])->orWhere('subcategory_id',$Input['id'])->where('status',1)->exists();
                        if($videoExist)
                            $returnData = UtilityController::Generateresponse(false, 'REDIRECT_TO_LIST', Response::HTTP_PERMANENTLY_REDIRECT,$Input['id']);
                        else
                            $returnData = UtilityController::Generateresponse(false, 'NO_DATA_FOR_CATEGORY', 0);
                    } else if($Input['for'] == 'Quiz') {
                        $quizExist = Quizzes::where('category_id',$Input['id'])->orWhere('subcategory_id',$Input['id'])->where('status',1)->exists();
                        if($quizExist)
                            $returnData = UtilityController::Generateresponse(false, 'REDIRECT_TO_LIST', Response::HTTP_PERMANENTLY_REDIRECT,$Input['id']);
                        else
                            $returnData = UtilityController::Generateresponse(false, 'NO_DATA_FOR_CATEGORY', 0);
                    } else if($Input['for'] == 'Worksheet') {
                        $worksheetExist = Worksheets::where('worksheet_category_id',$Input['id'])->orWhere('subcategory_id',$Input['id'])->where('status',1)->exists();
                        if($worksheetExist)
                            $returnData = UtilityController::Generateresponse(false, 'REDIRECT_TO_LIST', Response::HTTP_PERMANENTLY_REDIRECT,$Input['id']);
                        else
                            $returnData = UtilityController::Generateresponse(false, 'NO_DATA_FOR_CATEGORY', 0);
                    } else if($Input['for'] == 'all') {
                        $videoExist = Videos::where('video_category_id',$Input['id'])->orWhere('subcategory_id',$Input['id'])->where('status',1)->exists();
                        $quizExist = Quizzes::where('category_id',$Input['id'])->orWhere('subcategory_id',$Input['id'])->where('status',1)->exists();
                        $worksheetExist = Worksheets::where('worksheet_category_id',$Input['id'])->orWhere('subcategory_id',$Input['id'])->where('status',1)->exists();
                        if($videoExist || $quizExist || $worksheetExist)
                            $returnData = UtilityController::Generateresponse(false, 'REDIRECT_TO_LIST', Response::HTTP_PERMANENTLY_REDIRECT,$Input['id']);
                        else
                            $returnData = UtilityController::Generateresponse(false, 'NO_DATA_FOR_CATEGORY', 0);
                    }
                }
            } else {
                $returnData = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }
    //Function Name :   removeSubscriptionOfSubscriber
    //Author :          Jainam Shah
    //Purpose :         to remove subscription of subscriber by admin
    //In Params :       paid_by and video_category_id
    //Return :          json
    //Date :            7th March, 2019
    //###############################################################

    public function removeSubscriptionOfSubscriber(Request $request){
        try {
            \DB::beginTransaction();
            $Input = Request::all();
            $checkMoreData = MarketPlacePayment::where('paid_by', $Input['paid_by'])->get();
            foreach($checkMoreData as $key => $value){
                $getIds[] = explode(',', $value->video_category_id);
                $moreKeys = explode(',', $value->video_category_id);
                $catgoryNames[] = explode(',', $value->video_category_name);
                if(count($moreKeys) > 0) {
                    foreach($moreKeys as $keyInner => $valueInner){
                        if($valueInner == $Input['video_category_id']){
                            $deleteKey = $key;
                            $matched = $valueInner;
                        }    
                    }
                } else {
                    if($value == $Input['video_category_id']){
                        $deleteKey = $key;
                        $matched = $value;
                    }
                }
            }
            
            if(array_key_exists('date', $Input)) {
                $updateEndDate = Carbon::createFromFormat('d/m/Y', $Input['date'])->setTime(23, 59, 59);
            } else {
                $updateEndDate = $checkMoreData[$deleteKey]['subscription_end_date'];
            }
            
            for($i=0; $i<count($getIds[$deleteKey]); $i++) {
                $makeSingleRecords[$i] = $checkMoreData[$deleteKey]->toArray();
                if($getIds[$deleteKey][$i] == $matched) {
                    $makeSingleRecords[$i]['video_category_id'] = $getIds[$deleteKey][$i];
                    $makeSingleRecords[$i]['video_category_name'] = $catgoryNames[$deleteKey][$i];
                    $makeSingleRecords[$i]['subscription_end_date'] = $updateEndDate;
                } else {
                    $makeSingleRecords[$i]['video_category_id'] = $getIds[$deleteKey][$i];
                    $makeSingleRecords[$i]['video_category_name'] = $catgoryNames[$deleteKey][$i];
                }
                array_splice($makeSingleRecords[$i], 0, 1);
            }

            $removeOldData = MarketPlacePayment::where('id', $checkMoreData[$deleteKey]['id'])->first()->delete();
            $data = MarketPlacePayment::insert($makeSingleRecords);
            // $endSubscription = MarketPlacePayment::where('paid_by', $Input['paid_by'])
                // ->where('video_category_id', $Input['video_category_id'])->update($updateEndDate);
            if($data) {
                $returnData = UtilityController::Generateresponse(true, 'SUBSCRIPTION_REMOVED', 200, '');    
            } else {
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 200, '');
            }
            \DB::commit();
            return $returnData;
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }   
    }

    //###############################################################
    //Function Name : Updateadminpassword
    //Author        : Karan Kantesariya <karan@creolestudios.com>
    //Purpose       : To update admin's password
    //In Params     :
    //Return        : Json Encoded Data
    //Date          : 23rd August 2018
    //###############################################################
    public function Updateadminpassword(Request $request)
    {
        try {        
            if (Auth::guard('admins')) {
                \DB::beginTransaction();
                $AdminId = Auth::guard('admins')->user()->id;
                $Input = Input::all();
                $returnMessage = $Input['language']=='en'?'PASSWORD_CHANGES_SUCCESSFULLY':'PASSWORD_CHANGES_SUCCESSFULLY_CHINESE';
                // if($Input['updateFor']=='Distributors'){
                //     if(Hash::check($Input['old_password'],Auth::guard('distributors')->user()->password)) {
                //         if($Input['new_password']==$Input['confirm_password']){
                //             $result = Distributors::where('id',Auth::guard('distributors')->user()->id)->update(['password'=>Hash::make($Input['new_password'])]);
                //             $responseArray = UtilityController::Generateresponse(true, $returnMessage, 1,$result);
                //             \DB::commit();
                //         } else {
                //             throw new \Exception(UtilityController::getMessage($Input['language']=='en'?'PASSWORD_AND_CONFIRM_PASSWORD_NOT_SAME':'PASSWORD_AND_CONFIRM_PASSWORD_NOT_SAME_CHINESE'));
                //         }
                //     } else {
                //         throw new \Exception(UtilityController::getMessage($Input['language']=='en'?'PASSWORD_OLD_INCORRECT':($Input['language']=='chi'?'PASSWORD_OLD_INCORRECT_CHINESE':'PASSWORD_OLD_INCORRECT_RU')));
                //     }
                // }
                // if($Input['updateFor']=='DistributorsTeamMembers'){
                //     if(Hash::check($Input['old_password'],Auth::guard('distributor_team_members')->user()->password)) {
                //         if($Input['new_password']==$Input['confirm_password']){
                //             $result = DistributorsTeamMembers::where('id',Auth::guard('distributor_team_members')->user()->id)->update(['password'=>Hash::make($Input['new_password'])]);
                //             $responseArray = UtilityController::Generateresponse(true, $returnMessage, 1,$result);
                //             \DB::commit();
                //         } else {
                //             throw new \Exception(UtilityController::getMessage($Input['language']=='en'?'PASSWORD_AND_CONFIRM_PASSWORD_NOT_SAME':'PASSWORD_AND_CONFIRM_PASSWORD_NOT_SAME_CHINESE'));
                //         }
                //     } else {
                //         throw new \Exception(UtilityController::getMessage($Input['language']=='en'?'PASSWORD_OLD_INCORRECT':($Input['language']=='chi'?'PASSWORD_OLD_INCORRECT_CHINESE':'PASSWORD_OLD_INCORRECT_RU')));
                //     }
                // }
                if(Auth::guard('admins')){
                    if(Hash::check($Input['old_password'],Auth::guard('admins')->user()->password)) {
                        if($Input['new_password']==$Input['confirm_password']){
                            $result = Admin::where('id',Auth::guard('admins')->user()->id)->update(['password'=>Hash::make($Input['new_password'])]);
                            $responseArray = UtilityController::Generateresponse(true, $returnMessage, 1,$result);
                            \DB::commit();
                        } else {
                            throw new \Exception(UtilityController::getMessage($Input['language']=='en'?'PASSWORD_AND_CONFIRM_PASSWORD_NOT_SAME':'PASSWORD_AND_CONFIRM_PASSWORD_NOT_SAME_CHINESE'));
                        }
                    } else {
                        throw new \Exception(UtilityController::getMessage($Input['language']=='en'?'PASSWORD_OLD_INCORRECT':($Input['language']=='chi'?'PASSWORD_OLD_INCORRECT_CHINESE':'PASSWORD_OLD_INCORRECT_RU')));
                    }
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2,$Input['updateFor']);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, $e->getMessage(), 0);
        }
        return response()->json($responseArray); 
    }

    //###############################################################
    //Function Name: GetIgnitehistory
    //Author:        Karan Kantesariya <karan@creolestudios.com>
    //Purpose:       Get data of Ignite history categories
    //In Params:     void
    //Return:        json
    //Date:          04th Jan 2021
    //###############################################################
    public function GetIgnitehistory(Request $request){     
        try {
            $Input = Input::all(); 
            $category = $Input[0];
            $ids = VideoCategoryHistory::where('category_id',$category)->pluck('id')->toArray();
            $usd = VideoCategoryHistory::withCount('payment_usd')->whereIn('id',$ids)->orderby('id','DESC')->get()->toArray();
            $usdCollection = collect($usd);
            $usdData       = $usdCollection->map(function ($data) {
                $createdAt = Carbon::parse($data['created_at']);
                $data['created_at'] = $createdAt->format('M d Y');
                $admin = Admin::where('id',$data['admin_id'])->pluck('email_address')->first();
                $data['admin_email'] = $admin; 
                return $data;
            })->values()->toArray();
            $categories['usd'] = $usdData;
            $sgd = VideoCategoryHistory::withCount('payment_sgd')->whereIn('id',$ids)->orderby('id','DESC')->get()->toArray();
            $sgdCollection = collect($sgd);
            $sgdData       = $sgdCollection->map(function ($data) {
                $createdAt = Carbon::parse($data['created_at']);
                $data['created_at'] = $createdAt->format('M d Y');
                $admin = Admin::where('id',$data['admin_id'])->pluck('email_address')->first();
                $data['admin_email'] = $admin;
                return $data;
            })->values()->toArray();
            $categories['sgd'] = $sgdData;
            $categories['total_active_user'] = MarketPlacePayment::where('video_category_id',$category)->where('subscription_end_date','>',carbon::now())->where('payment_status',1)->count();
           $returnData = UtilityController::Generateresponse(true,'GOT_DATA',1,$categories);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: GetTaxrate
    //Author:        Karan Kantesariya <karan@creolestudios.com>
    //Purpose:       To get listing of taxrate done for ignite module
    //In Params:     void
    //Return:        json
    //Date:          13th May, 2021
    //###############################################################
    public function GetTaxrate(Request $request){     
        try {
            $Input = Request::all();
            if(\Request::has('propertyName') && $Input['propertyName'] != '') {
                if($Input['propertyName'] == 'date') {
                    $transactions = TaxRate::select("*",DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y %h:%i %p') as date"),DB::Raw("DATE_FORMAT(applicable_from, '%d %M, %Y %h:%i %p') as applicable_from"))->with('admin')->orderBy('created_at', $Input['sort'] == 'yes' ? 'asc' : 'desc')->paginate(Config('constants.other.MP_ADMIN_CATEGORY_PAGINATE'))->toArray();
                } else {
                    $transactions = TaxRate::select("*",DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y %h:%i %p') as date"),DB::Raw("DATE_FORMAT(applicable_from, '%d %M, %Y %h:%i %p') as applicable_from"))->with('admin')->orderBy($Input['propertyName'], $Input['sort'] == 'yes' ? 'asc' : 'desc')->paginate(Config('constants.other.MP_ADMIN_CATEGORY_PAGINATE'))->toArray();
                }
            } else {
                $transactions = TaxRate::select("*",DB::Raw("DATE_FORMAT(created_at, '%d %M, %Y %h:%i %p') as date"),DB::Raw("DATE_FORMAT(applicable_from, '%d %M, %Y %h:%i %p') as applicable_from"))->with('admin')->orderBy('created_at', $Input['sort'] == 'yes' ? 'asc' : 'desc')->paginate(Config('constants.other.MP_ADMIN_CATEGORY_PAGINATE'))->toArray();
            }
            $taxrateData = TaxRate::latest()->first();
            if($taxrateData){
                $transactions['latest_taxrate'] = $taxrateData->tax_rate;
            }
            $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $transactions);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name : Getignitetaxrate
    //Author        : Karan Kantesariya <karan@creolestudios.com>
    //Purpose       : Add queue for taxrate 
    //In Params     : taxrate
    //Return        : json
    //Date          : 13th May 2021
    //###############################################################
    public function Getignitetaxrate(Request $request){
        try {
            $Input      = Input::all();
            $validator = UtilityController::ValidationRules($Input, 'TaxRate');
            if (!$validator['status']) {
                $errorMessage = explode('.', $validator['message']);
                $responseArray = UtilityController::Generateresponse(false, $errorMessage['0'], 0);
            } else {
                \DB::beginTransaction();
                // $result = UtilityController::Makemodelobject($Input, 'TaxRate');
                $date       = Carbon::parse($Input['date'])->timezone('Asia/Singapore');
                $today_date = Carbon::now()->timezone('Asia/Singapore');
                $data['tax_rate']   = $Input['tax_rate'];
                $data['admin_id']   = Auth::guard('admins')->user()->id;
                $data['gst_number'] = $Input['gst_number'];
                $data['applicable_from'] = $date;
                $minute = $today_date->diffInMinutes($date);
                // ## Live 
                // $taxRate = (new TaxRateJob($data))->delay($today_date->addMinutes($minute));
                ## Test
                $taxRate = (new TaxRateJob($data))->delay(Carbon::now()->addSeconds(10));
                $jobId = dispatch($taxRate);
                if(!empty($jobId) && $jobId != ''){
                    $responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$jobId);
                } else {
                    $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
                }
                \DB::commit();
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);    
    }

    //###############################################################
    //Function Name : IgniteTextRateUpdate
    //Author        : Karan Kantesariya <karan@creolestudios.com>
    //Purpose       : Add taxrate 
    //In Params     : taxrate
    //Return        : json
    //Date          : 22th July 2021
    //###############################################################
    public static function IgniteTextRateUpdate($request){
        try {
            $Input      = $request;
            \DB::beginTransaction();
            $result = UtilityController::Makemodelobject($Input, 'TaxRate');
            if(!empty($result) && $result != ''){
                $responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$result);
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            \DB::commit();
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);    
    }
}
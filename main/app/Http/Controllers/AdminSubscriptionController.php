<?php

/**
 * Short description for file
 *
 * PHP version 5 and 7
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @package    AdminSubscriptionController
 * @author     Komal Kapadi (komal@creolestudios.com)
 * @copyright  2017 Sixcloud Group
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @since      File available since Release 1.0.0
 * @deprecated N/A
 */
/*
 */

/**
Pre-Load all necessary library & name space
 */

namespace App\Http\Controllers;

use App\Http\Controllers\UtilityController;
use App\Payment;
use App\SubscriptionPlan;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mail;

class AdminSubscriptionController extends Controller
{

    //###############################################################
    //Function Name : Getsubscriptions
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get subscription plans
    //In Params : Void
    //Return : json
    //Date : 21st December 2017
    //###############################################################
    public function Getsubscriptions(Request $request)
    {
        try {
            $result = SubscriptionPlan::where('status', 1)->get();
            if (!empty($result)) {
                foreach ($result as $key => $value) {
                    $chartResult = array();
                    for ($i = 0; $i < 6; $i++) {
                        $chartResult['months'][$i] = date("F Y", strtotime(date('Y-m-01') . " -$i months"));
                        $month                     = date("m", strtotime(date('Y-m-01') . " -$i months"));
                        $year                      = date("Y", strtotime(date('Y-m-01') . " -$i months"));
                        $finalResult               = Payment::where('status', 1)
                        // ->where('created_at', )
                            ->whereRaw('year(`created_at`) = ?', $year)
                            ->whereRaw('month(`created_at`) = ?', $month)
                            ->where('subscription_plan_id', $value['id'])->count();
                        $chartResult['value'][$i] = $finalResult;
                    }
                    $value['chart'] = $chartResult;
                }

                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $result);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 400, '');
            }
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Getpayments
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get payments
    //In Params : Void
    //Return : json
    //Date : 21st December 2017
    //###############################################################
    public function Getpayments(Request $request)
    {
        try {
            $Offset       = $request->offset * 2;
            $searchString = '';
            if (!empty(request('search'))) {
                $searchString = request('search');
            }
            $subscriptionId = '';
            if (!empty(request('subscription_id'))) {
                $subscriptionId = request('subscription_id');
            }
            ## Search string for saerch data.
            $Page         = 2;
            $queryPayment = Payment::select('*')
                ->whereHas('users', function ($queryPayment) use ($searchString) {
                    if (!empty($searchString)):
                        $queryPayment->where('users.first_name', 'like', '%' . $searchString . '%')
                            ->orWhere('last_name', 'like', '%' . $searchString . '%');
                    endif;
                })
                ->whereHas('subscription', function ($queryPayment) use ($subscriptionId) {
                    if (!empty($subscriptionId)):
                        $queryPayment->where('id', $subscriptionId);
                    endif;
                })
                ->with([
                    'users'        => function ($query) use ($searchString) {
                        $query->select('id', 'first_name', 'last_name');
                        if (!empty($searchString)) {
                            $query->where('first_name', 'like', '%' . $searchString . '%')
                                ->orWhere('last_name', 'like', '%' . $searchString . '%');
                        }
                    },
                    'subscription' => function ($query) use ($subscriptionId) {
                        $query->select('id', 'subscription_name', 'amount');
                    },
                ])
                ->where('status', 1);

            $result = $queryPayment->offset($Offset)->take($Page)->get();
            if ($result) {
                foreach ($result as $key => $value) {
                    $result[$key]['paymentStatus'] = UtilityController::PaymentStatus($value['payment_status']);
                }
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $result);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 400, '');
            }
            $returnData['count_total'] = $queryPayment->count();
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : GetSubscriptionusers
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get users ratio of subscription
    //In Params : Void
    //Return : json
    //Date : 27th December 2017
    //###############################################################
    public function GetSubscriptionusers(Request $request)
    {
        try {
            $returnData['data'] = Payment::select('*', DB::raw('count(*) as total'), DB::raw('month(created_at) as month'))
                ->orderBy('id', 'desc')
                ->groupBy(DB::raw('month(created_at)'), DB::raw('year(created_at)'))
                ->having('status', 1)
                ->get()->toArray();

            $returnData['months'][] = date('F, Y');
            for ($i = 1; $i < 6; $i++) {
                $returnData['months'][] = date('F, Y', strtotime("-$i month"));
            }
            $searchString = '';
            if (!empty(request('search'))) {
                $searchString = request('search');
            }
            $subscriptionId = '';
            if (!empty(request('subscription_id'))) {
                $subscriptionId = request('subscription_id');
            }
            ## Search string for search data.
            $Page         = 2;
            $Offset       = request('offset');
            $queryPayment = Payment::select('*')
                ->whereHas('users', function ($queryPayment) use ($searchString) {
                    if (!empty($searchString)):
                        $queryPayment->where('users.first_name', 'like', '%' . $searchString . '%')
                            ->orWhere('last_name', 'like', '%' . $searchString . '%');
                    endif;
                })
                ->whereHas('subscription', function ($queryPayment) use ($subscriptionId) {
                    if (!empty($subscriptionId)):
                        $queryPayment->where('id', $subscriptionId);
                    endif;
                })
                ->with([
                    'users'        => function ($query) use ($searchString) {
                        $query->select('id', 'first_name', 'last_name');
                        if (!empty($searchString)) {
                            $query->where('first_name', 'like', '%' . $searchString . '%')
                                ->orWhere('last_name', 'like', '%' . $searchString . '%');
                        }
                    },
                    'subscription' => function ($query) use ($subscriptionId) {
                        $query->select('id', 'subscription_name', 'amount');
                    },
                ])
                ->where('status', 1);

            $result = $queryPayment->offset($Offset)->take($Page)->get();
            //================================================================================
            $querySubscription = SubscriptionPlan::where('status', 1)->get();
            $dateS             = Carbon::now()->startOfMonth()->subMonth(6);
            $dateE             = Carbon::now()->startOfMonth();
            $result            = Payment::where('status', 1)
                ->whereBetween('created_at', [$dateS, $dateE])->distinct()->get()->groupBy(function ($date) {
                return Carbon::parse($date->created_at)->format('m,Y'); // grouping by years
                //return Carbon::parse($date->created_at)->format('m'); // grouping by months
            });
            for ($i = 0; $i < 6; $i++) {
                $finalResult['months'][] = date("F Y", strtotime(date('Y-m-01') . " -$i months"));
                $month                   = date("m", strtotime(date('Y-m-01') . " -$i months"));
                $year                    = date("Y", strtotime(date('Y-m-01') . " -$i months"));
                $result                  = Payment::where('status', 1)
                // ->where('created_at', )
                    ->whereRaw('year(`created_at`) = ?', $year)
                    ->whereRaw('month(`created_at`) = ?', $month)
                    ->distinct()->get();
                $finalResult['value'][] = count($result);
            }

            if (!empty($result)) {
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $finalResult);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 400, '');
            }
            $returnData['count_total'] = $queryPayment->count();
            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

}

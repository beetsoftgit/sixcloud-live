<?php

/**
 * Short description for file
 *
 * PHP version 5 and 7
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 */
/*
 * Place includes controller for login & forgot password.
 */

/**
  Pre-Load all necessary library & name space
 */

namespace App\Http\Controllers;

//load required library by use
use App\Userprcase;
use App\Proofreadingpackage;
use App\Accessor;
use App\Userprcaselog;
use App\User;
use App\Prcaseassignment;
use App\Jobs\CheckDelayedCase;
//load session & other useful library
use Auth;
use Validator;
use Image;
use Hash;
use Carbon\Carbon;
//define model
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use OSS\OssClient;
use OSS\Core\OssException;
use OSS\Core\OssUtil;

/**
 * Photos
 * @package    AdminPRController
 * @subpackage Controller
 * @author     Senil Shah <senil@creolestudios.com>
 */
class AdminPRController extends BaseController {

    public function __construct() {
        //Artisan::call('cache:clear');
    }

    public function index() {
        
    }

    //###############################################################
    //Function Name : Getcases
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To get cases for admin
    //In Params : Void
    //Return : json
    //Date : 15th December 2017
    //###############################################################
    public function Getcases(Request $request) {
        try {
            $Input = Input::all();
            // print_r($Input); die;
            if($Input['cases_type'] == 1 || $Input['cases_type'] == 3)
                $getCase = self::Opencases($Input['cases_type'],'');
            if($Input['cases_type'] == 2)
                $getCase = self::Specialcases($Input['cases_type']);
            if(!empty($getCase)){
                $responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$getCase);
            } else
                $responseArray = UtilityController::Generateresponse(false,'GENERAL_ERROR',0);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Opencases
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : Structur the array for open cases
    //In Params : Case details
    //Return : Structured array for open case
    //Date : 22nd December 2017
    //###############################################################
    public function Opencases($cases_type,$id) {
        try {
            $getCase = Userprcase::select('*')->with([
                        'accessment_time' => function($query){
                            $query->select('*')->where('case_status',3)->orderBy('id','DESC')->with(
                                [
                                    'accessor_information' => function($query){
                                        $query->select('*');       
                                    }
                                ]);
                        },
                        'proof_reading_package' => function($query){
                            $query->select('*');
                        },
                        'case_user' => function($query){
                            $query->select('*');
                        }
                    ])->where('cases_type',$cases_type);
            if($id != '')
                $getCase = $getCase->where('id',$id);
            $getCase = $getCase->get();
            $getCase = ($cases_type == 1 ? self::Loopingfunction($getCase,1) : self::Loopingfunction($getCase,1,1));
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
        }
        return $getCase;
    }

    //###############################################################
    //Function Name : Specialcases
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : Structur the array for special cases
    //In Params : Case details
    //Return : Structured array for special case
    //Date : 22nd December 2017
    //###############################################################
    public function Specialcases($cases_type) {
        try {
            $getUnassignedCases = Userprcase::doesntHave('accessment_time')->with([
                        'proof_reading_package' => function($query){
                            $query->select('*');
                        },
                        'case_user' => function($query){
                            $query->select('*');
                        }
                    ])->where('cases_type',$cases_type)->where('is_higher_word_count',0)->get();
            $getUnassignedCases = self::Loopingfunction($getUnassignedCases,4);
            $getHigherWordCountCases = Userprcase::select('*')->with([
                        'accessment_time' => function($query){
                            $query->select('*')->with(
                                [
                                    'accessor_information' => function($query){
                                        $query->select('*');       
                                    }
                                ]);
                        },
                        'proof_reading_package' => function($query){
                            $query->select('*');
                        },
                        'case_user' => function($query){
                            $query->select('*');
                        }
                    ])->where('cases_type',$cases_type)->where('is_higher_word_count',1)->get();
            $getHigherWordCountCases = self::Loopingfunction($getHigherWordCountCases,1);
            $getDelayedCases = Userprcase::whereHas('accessment_time')->with([
                        'accessment_time' => function($query){
                            $query->select('*')->with(
                                [
                                    'accessor_information' => function($query){
                                        $query->select('*');       
                                    }
                                ]);
                        },
                        'proof_reading_package' => function($query){
                            $query->select('*');
                        },
                        'case_user' => function($query){
                            $query->select('*');
                        }
                    ])->where('cases_type',$cases_type)->where('case_status',5)->get();
            $getDelayedCases = self::Loopingfunction($getDelayedCases,1,1);
            $getCase['unassigned_cases']        = $getUnassignedCases;
            $getCase['higher_word_count_cases'] = $getHigherWordCountCases;
            $getCase['delayed_cases']           = $getDelayedCases;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
        }
        return $getCase;
    }

    //###############################################################
    //Function Name : Loopingfunction
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : foreach loop used for getting case status accordingly
    //In Params : case details
    //Return : variable with proper user friendly status strings and classes
    //Date : 17th January 2018
    //###############################################################
    public function Loopingfunction($getCase,$switchCaseValue,$caseWhichTime = "") {
        try {
            switch ($switchCaseValue) {
                case 1:
                    foreach ($getCase as $key => $value) {
                        if($getCase[$key]['case_status'] == 1 )
                            $getCase[$key] = self::Getcasestatus('1',$getCase[$key],'');
                        elseif ($getCase[$key]['case_status'] == 2 )
                            $getCase[$key] = self::Getcasestatus('2',$getCase[$key],'');
                        elseif ($getCase[$key]['case_status'] == 3 )
                            $getCase[$key] = self::Getcasestatus('3',$getCase[$key],'');
                        else
                            $getCase[$key] = self::Getcasestatus('4',$getCase[$key],'');
                        $getCase[$key]['document_submitted_at'] = UtilityController::Changedateformat($value['created_at']);
                        $getCase[$key]['case_created_at'] = UtilityController::Changedateformat($value['created_at'],'d M Y, H:i a');
                        $projectAt = Carbon::parse($value['created_at'])->addHours($value['proof_reading_package']['service_package']);
                        $getCase[$key]['deadline'] = $projectAt->toDateTimeString();
                        foreach ($value['accessment_time'] as $keyInner => $valueInner) {
                            if($valueInner['accessor_information']['level_type'] == 1)
                                ($caseWhichTime == 1 ? $getCase[$key] = self::Getcasestatus('5',$getCase[$key],$valueInner,1) : $getCase[$key] = self::Getcasestatus('5',$getCase[$key],$valueInner));
                            if($valueInner['accessor_information']['level_type'] == 2) 
                                $getCase[$key] = self::Getcasestatus('6',$getCase[$key],$valueInner);
                        }
                    }
                    break;
                case 2:
                    foreach ($getCase['accessment_time'] as $key => $value) {
                        if($value['pr_case']['case_status'] == 1 )
                            $value['pr_case'] = self::Getcasestatus('1',$value['pr_case'],'');
                        elseif ($value['pr_case']['case_status'] == 2 )
                            $value['pr_case'] = self::Getcasestatus('2',$value['pr_case'],'');
                        elseif ($value['pr_case']['case_status'] == 3 )
                            $value['pr_case'] = self::Getcasestatus('3',$value['pr_case'],'');
                        else
                            $value['pr_case'] = self::Getcasestatus('4',$value['pr_case'],'');
                        $value['pr_case']['document_submitted_at'] = UtilityController::Changedateformat($value['pr_case']['created_at']);
                        
                        $projectAt = Carbon::parse($value['created_at'])->addHours($value['proof_reading_package']['service_package']);
                        $value['deadline'] = $projectAt->toDateTimeString();
                        
                        $value['pr_case']['access_1_time']                    = (isset($value['pr_case']['accessment_1_resolve_time'])?UtilityController::Changedateformat($value['pr_case']['accessment_1_resolve_time']):'') ;
                        $value['pr_case']['access_2_time']                    = (isset($value['pr_case']['accessment_2_resolve_time'])?UtilityController::Changedateformat($value['pr_case']['accessment_2_resolve_time']):'');
                        foreach ($value['accessor_information'] as $keyInner => $valueInner) {
                            if($value['accessor_information']['level_type'] == 1)
                                $value['pr_case'] = self::Getcasestatus('5',$value['pr_case'],$value,'1');
                            if($valueInner['level_type'] == 2) 
                                $value['pr_case'] = self::Getcasestatus('6',$value['pr_case'],$value);
                        }
                    }
                    break;
                case 3:
                    $accessor = Auth::guard('pr-admins')->user();
                    if($getCase['pr_case']['case_status'] == 1 )
                        $getCase['pr_case'] = self::Getcasestatus('1',$getCase['pr_case'],'');
                    elseif ($getCase['pr_case']['case_status'] == 2 )
                        $getCase['pr_case'] = self::Getcasestatus('2',$getCase['pr_case'],'');
                    elseif ($getCase['pr_case']['case_status'] == 3 )
                        $getCase['pr_case'] = self::Getcasestatus('3',$getCase['pr_case'],'');
                    else
                        $getCase['pr_case'] = self::Getcasestatus('4',$getCase['pr_case'],'');
                    $getCase['pr_case']['document_submitted_at']      = UtilityController::Changedateformat($getCase['pr_case']['created_at']);
                    $getCase['pr_case']['case_created_at']            = UtilityController::Changedateformat($getCase['pr_case']['created_at'],'d M Y, H:i a');

                    $projectAt = Carbon::parse($value['created_at'])->addHours($value['proof_reading_package']['service_package']);
                    $getCase['pr_case']['deadline'] = $projectAt->toDateTimeString();

                    if($getCase['accessor_information']['level_type'] == 1){
                        $getCase['pr_case']['accessment'] = ($accessor->level_type == 1 ? self::Getcasestatus('5',array(),$getCase,'') : self::Getcasestatus('5',array(),$getCase,'1'));
                        // print_r($getCase['pr_case']['accessment']); die;
                    }
                    if($getCase['accessor_information']['level_type'] == 2){
                        $getCase['pr_case']['accessment'] = ($accessor->level_type == 2 ? self::Getcasestatus('6',array(),$getCase,'') : self::Getcasestatus('6',array(),$getCase,'1'));

                    }
                    break;
                case 4:
                    foreach ($getCase as $key => $value) {
                        $value['document_submitted_at'] = UtilityController::Changedateformat($value['created_at']);
                    }
                    break;
                case 5:
                    $accessor = Auth::guard('pr-admins')->user();
                    foreach ($getCase as $key => $value) {
                        if($value['pr_case']['case_status'] == 1 )
                            $value['pr_case'] = self::Getcasestatus('1',$value['pr_case'],'');
                        elseif ($value['pr_case']['case_status'] == 2 )
                            $value['pr_case'] = self::Getcasestatus('2',$value['pr_case'],'');
                        elseif ($value['pr_case']['case_status'] == 3 )
                            $value['pr_case'] = self::Getcasestatus('3',$value['pr_case'],'');
                        else
                            $value['pr_case'] = self::Getcasestatus('4',$value['pr_case'],'');
                        $value['pr_case']['document_submitted_at']      = UtilityController::Changedateformat($value['pr_case']['created_at']);
                        $value['pr_case']['case_created_at']            = UtilityController::Changedateformat($value['pr_case']['created_at'],'d M Y, H:i a');
                        $value['pr_case']['access_1_time']                    = (isset($value['pr_case']['accessment_1_resolve_time'])?UtilityController::Changedateformat($value['pr_case']['accessment_1_resolve_time']):'') ;
                        $value['pr_case']['access_2_time']                    = (isset($value['pr_case']['accessment_2_resolve_time'])?UtilityController::Changedateformat($value['pr_case']['accessment_2_resolve_time']):'');
                        foreach ($value['accessor_information'] as $keyInner => $valueInner) {
                            if($value['accessor_information']['level_type'] == 1){
                                $value['pr_case']['accessment'] = ($accessor->level_type == 1 ? self::Getcasestatus('5',array(),$value,'') : self::Getcasestatus('5',array(),$value,'1'));
                            }
                            if($value['accessor_information']['level_type'] == 2){
                                $value['pr_case']['accessment'] = ($accessor->level_type == 2 ? self::Getcasestatus('6',array(),$value,'1') : self::Getcasestatus('6',array(),$value,''));
                            }
                        }
                    }
                    break;
            }   
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
        }
        return $getCase;
    }
        
    //###############################################################
    //Function Name : Getcasestatus
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : switch case to get case status accordingly
    //In Params : case status
    //Return : variables to set class and numeric status to user readable format
    //Date : 14th December 2017
    //###############################################################
    public function Getcasestatus($case, $getCase = array(), $valueInner, $caseWhichTime = "") {
        try {
            switch ($case) {
                case 1:
                    $getCase['case_status_to_string'] = 'Document Submitted';
                    $getCase['case_class']            = 'back-lr';
                    break;
                case 2:
                    $getCase['case_status_to_string'] = '1st Level Accessment Completed';
                    $getCase['case_class']            = 'back-lb';
                    $getCase['access_1_resolve_time'] = UtilityController::Changedateformat($getCase['accessment_1_resolve_time']);
                    break;
                case 3:
                    $getCase['case_status_to_string'] = '2nd Level Accessment Completed';
                    $getCase['case_class']            = 'back-ly';
                    $getCase['access_2_resolve_time'] = UtilityController::Changedateformat($getCase['accessment_2_resolve_time']);
                    break;
                case 4:
                    $getCase['case_status_to_string'] = 'Completed';
                    $getCase['case_class']            = 'back-lg';
                    $getCase['file_download_bubble']  = 'back-green';
                    $getCase['file_downloaded']       = "File downloaded";
                    break;
                case 5:
                    $getCase['accessor_one_case_bubble'] = 'back-blue';
                    $getCase['accessor_one_time']        = ($caseWhichTime == 1 ? UtilityController::Changedateformat($valueInner['case_resolved_time']) : UtilityController::Changedateformat($valueInner['case_accept_time']));
                    break;
                case 6:
                    $getCase['accessor_two_case_bubble'] = 'back-yellow';
                    $getCase['accessor_two_time']        = ($caseWhichTime != 1 ? UtilityController::Changedateformat($valueInner['case_resolved_time']) : UtilityController::Changedateformat($valueInner['case_accept_time']));
                    break;
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
        }
        return $getCase;
    }

    /*###############################################################
    Function Name : Getaccessors
    Author : Senil Shah <senil@creolestudios.com>
    Purpose : To get all the accessors
    In Params : Void
    Return : json
    Date : 18th December 2017
    ###############################################################*/
    public function Getaccessors(Request $request) {
        try {
            $Input = Input::all();
            if(isset($Input['whichPage']) && $Input['whichPage'] == 'special'){
                $getAccessors = Accessor::where('level_type',1)->where('availability_status',1);
                if($getAccessors == null)
                    $getAccessors = Accessor::doesntHave('assigned_accessor')->where('level_type',1);
            } else {
                $getAccessors = Accessor::select('*')->with(['parent_accessor' => function($query){
                        $query->select('*');
                    }])->withCount('total_jobs');
            }
            if (isset($Input['type']))
                $getAccessors->where('level_type',$Input['type']);    
            if (isset($Input['sort_by']) && $Input['sort_by'] == 'Team 1- 10')
                $getAccessors->orderBy('team','ASC');
            if (isset($Input['sort_by']) && $Input['sort_by'] == 'Team 10- 1')
                $getAccessors->orderBy('team','DESC');
            $getAccessors = $getAccessors->get();
            if ($getAccessors) {
                foreach ($getAccessors as $key => $value) {
                    $getAccessors[$key]['image'] = UtilityController::Imageexist($value['image'],public_path('').UtilityController::Getmessage('ACCESSOR_IMAGE_PATH'),url('').UtilityController::Getmessage('ACCESSOR_IMAGE_URL'),url('').UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL') );
                }
                $responseArray          = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$getAccessors);
            } else
                $responseArray = UtilityController::Generateresponse(false,'GENERAL_ERROR',0);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Assignaccessor
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To assign a new accessor to the case
    //In Params : Void
    //Return : json
    //Date : 18th December 2017
    //###############################################################
    public function Assignaccessor(Request $request) {
        try {
            \DB::beginTransaction();
            $Input                     = Input::all();
            $Input['case_accept_time'] = Carbon::now();
            if($Input['from_where'] == 'unsiggned' || $Input['from_where'] == 'higherWordCount')
                    $accessorChangeProcess = self::Accessorchangeprocess($Input,1);
            if ($Input['from_where'] == 'open' || $Input['from_where'] == 'delayed')
                    $accessorChangeProcess = self::Accessorchangeprocess($Input,2);
            $newAccessor = $Input['accessor'];
            Mail::send('emails.notify_accessor', $newAccessor, function($message) use ($newAccessor) {
                    $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                    $message->to($newAccessor['email_address'])->subject('You have been assigned a case');
                });
            $result = UtilityController::Makemodelobject($Input,'Userprcaselog');
            if ($result) {
                $responseArray = UtilityController::Generateresponse(true,'ACCESSOR_CHANGED',1,$result);
                \DB::commit();
            } else
                $responseArray = UtilityController::Generateresponse(false,'GENERAL_ERROR',0);
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Accessorchangeprocess
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : make the related changes accordingly from where accessor is changed
    //In Params : Accessor and case details
    //Return : json
    //Date : 30th January 2018
    //###############################################################
    public function Accessorchangeprocess($Input,$switchCaseValue) {
        try {
            switch ($switchCaseValue) {
                case 1:
                    $setAccessorAvailabilityData['availability_status'] = 2;
                    $setAccessorAvailability                            = UtilityController::Makemodelobject($setAccessorAvailabilityData,'Accessor','',$Input['accessor_id']);
                    $changeCaseTypeData['cases_type']                   = 1;
                    $changeCaseType                                     = UtilityController::Makemodelobject($changeCaseTypeData,'Userprcase','',$Input['user_pr_case_id']);
                    break;

                case 2:
                    $alreadyAssignedAccessor = Userprcaselog::where('user_pr_case_id',$Input['user_pr_case_id'])->orderBy('id','DESC')->first();
                    if(!empty($alreadyAssignedAccessor)){
                        $setAccessorAvailabilityData['availability_status'] = 1;
                        $setAccessorAvailabilityData['case_status']         = 4;
                        $setAccessorAvailability = UtilityController::Makemodelobject($setAccessorAvailabilityData,'Accessor','',$alreadyAssignedAccessor['accessor_id']);
                        $setPreviousAccessorStatus = UtilityController::Makemodelobject($setAccessorAvailabilityData,'Userprcaselog','',$alreadyAssignedAccessor['id']);
                    }
                    $setAccessorAvailabilityData['availability_status'] = 2;
                    $setAccessorAvailability                            = UtilityController::Makemodelobject($setAccessorAvailabilityData,'Accessor','',$Input['accessor_id']);
                    break;

            }

        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return 1;
    }

    //###############################################################
    //Function Name : Addaccessor
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To add a new accessor
    //In Params : Accessor details
    //Return : json
    //Date : 26th December 2017
    //###############################################################
    public function Addaccessor(Request $request) {
        try {
            $Input = Input::all();
            $validator = ($Input['validation_type'] == 'new' ? (isset($Input['level_type'])?($Input['level_type'] ==1?UtilityController::ValidationRules($Input, 'Accessor'):UtilityController::ValidationRules($Input, 'Accessor',['accessor_id'])):UtilityController::ValidationRules($Input, 'Accessor')) : UtilityController::ValidationRules($Input, 'Accessor',['team','level_type','accessor_id']));
            $responseArray = $validator;
            if (!$validator['status']) {
                $errorMessage = explode('.', $validator['message']);
                $responseArray = UtilityController::Generateresponse(false,'GENERAL_ERROR',0,$errorMessage['0']);
            } else {
                $user =  Auth::guard('pr-admins')->user();
                if(isset($Input['update']) && $Input['update'] == 'update'){
                    if (!empty(Input::hasFile('image'))) {
                        $fileSize      = $request->file('image')->getClientSize();
                        if ($fileSize <= UtilityController::Getmessage('FILE_SIZE_2')) {
                            $profilePhoto                                                    = rand() . time() . str_replace(' ', '_', Input::file('image')->getClientOriginalName());
                            $destinationPath                                                 = public_path('').UtilityController::Getmessage('ACCESSOR_IMAGE_PATH');
                            /*if ($Input['x1'] && $Input['y1'] && $Input['w'] && $Input['h'] == 0) {
                                $w = $h = 290;
                                $x = $y = 294;
                            } else {
                                $x = (int) $Input['x1'];
                                $y = (int) $Input['y1'];
                                $w = (int) $Input['w'];
                                $h = (int) $Input['h'];
                            }
                            $testimg = Image::make(Input::file('image'))->crop($w, $h, $x, $y)->resize(300, 300, function ($constraint) {*/
                            $testimg = Image::make(Input::file('image'))->resize(100, 100, function ($constraint) {
                                // $constraint->aspectRatio();
                                $constraint->upsize();
                            })->save($destinationPath . $profilePhoto);
                            $Input['image'] = $profilePhoto;
                        }
                        else
                            UtilityController::Generateresponse(false,'PROFILE_PHOTO_SIZE_LARGE',0);
                    }
                    $result = UtilityController::Makemodelobject($Input,'Accessor','',$user->id);
                }
                else{
                    $Input['dob'] = Carbon::createFromFormat('d/m/Y',$Input['dob']);
                    $Input['accessor_password'] = str_random(8);
                    $Input['password'] = Hash::make($Input['accessor_password']);
                    Mail::send('emails.pr.new_accessor', $Input, function($message) use ($Input) {
                            $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                            $message->to($Input['email_address'])->subject('SixClouds : Account Details');
                    });
                    $result = (isset($Input['id']) ? UtilityController::Makemodelobject($Input,'Accessor','',$Input['id']) : UtilityController::Makemodelobject($Input,'Accessor'));
                }
                if ($result) {
                    $responseArray = ($Input['validation_type'] == 'new' ? UtilityController::Generateresponse(true,'ACCESSOR_ADDED',1,$result) : UtilityController::Generateresponse(true,'ACCESSOR_UPDATED',1,$result));
                } else
                    $responseArray = UtilityController::Generateresponse(false,'GENERAL_ERROR',0);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    /*###############################################################
    Function Name : Getaccessorinfo
    Author : Senil Shah <senil@creolestudios.com>
    Purpose : To get the accessor's info
    In Params : Void
    Return : json
    Date : 27th December 2017
    ###############################################################*/
    public function Getaccessorinfo(Request $request) {
        try {
            $Input        = Input::all();
            $user =  Auth::guard('pr-admins')->user();
            $getAccessors = Accessor::select('*')->with([
                'parent_accessor' => function($query){
                    $query->select('*');
                },
                'accessment_time' => function($query){
                    $query->select('*')->where('case_status',3)->with(
                        [
                            'accessor_information' => function($query){
                                $query->select('*');       
                            },
                            'pr_case' => function($query){
                                $query->select('*')->with([
                                    'case_user' => function($query){
                                        $query->select('*');
                                    }
                                ]);       
                            },
                        ]);
                },
            ])->withCount('total_jobs');
            if($Input['accessor_id'] == 'login_id')
               $getAccessors->where('id',$user->id);
            else
                $getAccessors->where('id',$Input['accessor_id']);
            $getAccessors = $getAccessors->first();
            if ($getAccessors) {
                $getAccessors                 = self::Loopingfunction($getAccessors,2);
                $getAccessors['joining_date'] = UtilityController::Changedateformat($getAccessors['created_at'],"d M, Y");
                $getAccessors['dob']          = UtilityController::Changedateformat($getAccessors['dob'],"d M, Y");
                $getAccessors['image']        = UtilityController::Imageexist($getAccessors['image'],public_path('').UtilityController::Getmessage('ACCESSOR_IMAGE_PATH'),url('').UtilityController::Getmessage('ACCESSOR_IMAGE_URL'),url('').UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL') );
                $responseArray                = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$getAccessors);
            } else
                $responseArray = UtilityController::Generateresponse(false,'GENERAL_ERROR',0);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    /*###############################################################
    Function Name : Updatepassword
    Author : Senil Shah <senil@creolestudios.com>
    Purpose : To change accessors password
    In Params : Void
    Return : json
    Date : 28th December 2017
    ###############################################################*/
    public function Updatepassword(Request $request) {
        try {
            $user = Auth::guard('pr-admins')->user();
            $Input = Input::all();
            if (Hash::check($Input['passwords']['old_password'], $user->password)) 
            {
                $userId                         = $user->id;
                $Input['passwords']['password'] = Hash::make($Input['passwords']['password']);
                $result = UtilityController::Makemodelobject($Input['passwords'],'Accessor','',$userId);
                if($result)
                    $responseArray = UtilityController::Generateresponse(true,'PASSWORD_CHANGE_SUCCESS',1,$result);
                else 
                    $responseArray = UtilityController::Generateresponse(false,'PASSWORD_CHANGE_FAIL',0);
            }
            else
            {
                $responseArray = UtilityController::Generateresponse(false,'PASSWORD_OLD_INCORRECT',0);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    /*###############################################################
    Function Name : Myjobcases
    Author : Senil Shah <senil@creolestudios.com>
    Purpose : To get accessor cases details
    In Params : Void
    Return : json
    Date : 2nd January 2018
    ###############################################################*/
    public function Myjobcases(Request $request) {
        try {
            $accessor = Auth::guard('pr-admins')->user();
            $newCase  = Prcaseassignment::with(
                [
                    'pr_case' => function($query){
                        $query->select('*')->with('proof_reading_package');
                    }
                ]
            )->where('accessor_id',$accessor->id)->where('action_status',4)->orderBy('id','DESC')->first();
            if(!isset($newCase)){
                $activeCase = Userprcaselog::with('accessor_information')->with([
                    'pr_case' => function($query){
                        $query->select('*')->with('proof_reading_package','case_user');
                    }
                ])->where('case_status',1)->where('accessor_id',$accessor->id)->orderBy('id','DESC');
                ($accessor->level_type == 1?$activeCase = $activeCase->first():$activeCase=$activeCase->get());
                if(isset($activeCase)){
                    $activeCase = ($accessor->level_type == 1?self::Loopingfunction($activeCase,3):self::Loopingfunction($activeCase,5));
                }
            }
            if (isset($newCase))
                $responseArray = UtilityController::Generateresponse(true,'NEW_CASE',1,$newCase);
            elseif (isset($activeCase))
                $responseArray = UtilityController::Generateresponse(true,'ACTIVE_CASE',1,$activeCase);
            else
                $responseArray = UtilityController::Generateresponse(false,'NO_DATA',0);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    /*###############################################################
    Function Name : Accessorcompletedjobs
    Author : Senil Shah <senil@creolestudios.com>
    Purpose : To get accessor's completed cases details
    In Params : Void
    Return : json
    Date : 13th January 2018
    ###############################################################*/
    public function Accessorcompletedjobs(Request $request) {
        try {
            $accessor       = Auth::guard('pr-admins')->user();
            $completedCases = Userprcaselog::with(
                [
                    'pr_case' => function($query){
                        $query->select('*')->with('proof_reading_package');
                    }
                ]
            )->where('accessor_id',$accessor->id)->where('case_status',3)->get();
            foreach ($completedCases as $key => $value) {
                $completedCases[$key]['pr_case']['document_submitted_at'] = UtilityController::Changedateformat($value['pr_case']['created_at']);
                $completedCases[$key]['pr_case']['case_created_at']       = UtilityController::Changedateformat($value['pr_case']['created_at'],'d M Y, H:i a');
                $completedCases[$key]['access_1_time']                    = (isset($value['pr_case']['accessment_1_resolve_time'])?UtilityController::Changedateformat($value['pr_case']['accessment_1_resolve_time']):'') ;
                $completedCases[$key]['access_2_time']                    = (isset($value['pr_case']['accessment_2_resolve_time'])?UtilityController::Changedateformat($value['pr_case']['accessment_2_resolve_time']):'');
            }
            $responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$completedCases);
            
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    /*###############################################################
    Function Name : Myteaminfo
    Author : Senil Shah <senil@creolestudios.com>
    Purpose : To get accessor's team info
    In Params : Void
    Return : json
    Date : 2nd January 2018
    ###############################################################*/
    public function Myteaminfo(Request $request) {
        try {
            $accessor     = Auth::guard('pr-admins')->user();
            $accessorTeam = Accessor::withCount(['total_jobs' => function($query){
                            $query->where('case_status',3);}])->where('team','Team 1')->where('id','!=',$accessor->id)->get();
            foreach ($accessorTeam as $key => $value) {
                $value['image'] = UtilityController::Imageexist($value['image'],public_path('').UtilityController::Getmessage('ACCESSOR_IMAGE_PATH'),url('').UtilityController::Getmessage('ACCESSOR_IMAGE_URL'),url('').UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));
            }
            $responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$accessorTeam);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    /*###############################################################
    Function Name : Acceptorrejectcase
    Author : Senil Shah <senil@creolestudios.com>
    Purpose : To accept or reject case
    In Params : Void
    Return : json
    Date : 3rd January 2018
    ###############################################################*/
    public function Acceptorrejectcase(Request $request) {
        try {
            $Input                               = Input::all();
            $accessor                            = Auth::guard('pr-admins')->user();
            $Input['detail']['accessor_id']      = $accessor->id;
            $Input['detail']['case_accept_time'] = ($Input['detail']['case_status'] == 1)?date('Y-m-d H:i:s', time()):'';
            $newCaseStatus                       = UtilityController::Makemodelobject($Input['detail'],'Prcaseassignment','',$Input['detail']['prcaseassignment_id']);
            $result                              = UtilityController::Makemodelobject($Input['detail'],'Userprcaselog');
            if($result && $newCaseStatus)
                if($Input['detail']['case_status']==2){
                    $statusChange['cases_type'] = 2;
                    $specialCase                = UtilityController::Makemodelobject($statusChange,'Userprcase','',$Input['detail']['user_pr_case_id']);
                    $setAccessorAvailabilityData['availability_status'] = 1;
                    $setAccessorAvailability = UtilityController::Makemodelobject($setAccessorAvailabilityData,'Accessor','',$accessor->id);
                    $responseArray              = UtilityController::Generateresponse(true,'CASE_REJECTED',1,$result);
                }
                else{
                    $activeCase    = self::Opencases(1,$result['user_pr_case_id']);
                    $setAccessorAvailabilityData['availability_status'] = 2;
                    $setAccessorAvailability = UtilityController::Makemodelobject($setAccessorAvailabilityData,'Accessor','',$accessor->id);
                    $responseArray = UtilityController::Generateresponse(true,'CASE_ACCEPTED',1,$activeCase);
                    $hour = $Input['detail']['level1_assessment_time_limit'];
                    if(Carbon::now()->addHours($hour)->format("H:i:s") <= "23:00:00"){
                        // $jobDelayEmail = (new CheckDelayedCase($Input['detail']['user_pr_case_id']),1)->delay(Carbon::now()->addHours($Input['detail']['level1_assessment_time_limit']-1));
                        // $jobChangeDelayStatus = (new CheckDelayedCase($Input['detail']['user_pr_case_id']),2)->delay(Carbon::now()->addHours($Input['detail']['level1_assessment_time_limit']));
                        dispatch($jobDelayEmail);
                        dispatch($jobChangeDelayStatus);
                    } else {
                        $addAccessTimeToCurrent = Carbon::now()->addHours($hour);
                        $nextDaySevenAM = Carbon::create(Carbon::now()->format("Y"),Carbon::now()->format("m"),Carbon::now()->format("d"),23,0,0);
                        $difference = $nextDaySevenAM->diffInSeconds($addAccessTimeToCurrent, false);
                        $blackOutTime = Carbon::parse('next day')->Format('Y-m-d 07:00:05');
                        $blackOutTime = Carbon::parse($blackOutTime);
                        $blackOutTime = $blackOutTime->addSeconds($difference);
                        $currentTime  = Carbon::now();
                        $difference   = $currentTime->diffInSeconds($blackOutTime);
                        if(Carbon::now()->addSeconds($difference)->subHours(1)->format("H:i:s") <= "07:00:00")
                            $difference = $difference + 3600;
                        // $jobDelayEmail = (new CheckDelayedCase($Input['detail']['user_pr_case_id']),1)->delay(Carbon::now()->addSeconds($difference)->subHours(1));
                        // $jobChangeDelayStatus = (new CheckDelayedCase($Input['detail']['user_pr_case_id']),2)->delay(Carbon::now()->addSeconds($difference));
                        dispatch($jobDelayEmail);
                        dispatch($jobChangeDelayStatus);
                    }
                }
            else
                $responseArray = UtilityController::Generateresponse(false,'GENERAL_ERROR',0);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    /*###############################################################
    Function Name : Helpme
    Author : Senil Shah <senil@creolestudios.com>
    Purpose : To send mail to parent accessor for help
    In Params : Void
    Return : json
    Date : 3rd January 2018
    ###############################################################*/
    public function Helpme(Request $request) {
        try {
            $Input                   = Input::all();
            $parentAccessor          = Accessor::where('level_type',2)->where('team',$Input['accessor_id']['team'])->first();
            $parentAccessor          = json_decode(json_encode($parentAccessor), true);
            $Input['parentAccessor'] = $parentAccessor;
            $Input['accessor_name'] = $Input['accessor_id']['first_name'].' '.$Input['accessor_id']['last_name'];
            $Input['case_name'] = $Input['case_id']['pr_case']['case_original_file_name'];
            Mail::send('emails.help_mail', $Input, function($message) use ($Input) {
                                    $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                                    $message->to($Input['parentAccessor']['email_address'])->subject('Want help in case accessment');
                                });
            $responseArray = UtilityController::Generateresponse(true,'MAIL_SENT_FOR_HELP',1);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    /*###############################################################
    Function Name : Accessedfile
    Author : Senil Shah <senil@creolestudios.com>
    Purpose : To send mail to parent accessor for help
    In Params : Void
    Return : json
    Date : 3rd January 2018
    ###############################################################*/
    public function Accessedfile(Request $request) {
        try {
            $Input = Input::all();
            $accessor = Auth::guard('pr-admins')->user();
            if (Input::hasFile('file_uploaded')) {
                $fileSize = $request->file('file_uploaded')->getClientSize();
                if ($fileSize <= UtilityController::Getmessage('FILE_SIZE_5')) {
                    $originalFileName = Input::file('file_uploaded')->getClientOriginalName();
                    $fileName         = rand() . time() . str_replace(' ', '_', Input::file('file_uploaded')->getClientOriginalName());
                    $extension        = \File::extension($fileName);
                    if (in_array($extension, array('docx','doc','txt','DOCX','DOC','TXT'))) {
                        $request->file('file_uploaded')->move(public_path('').UtilityController::Getpath('PR_DOCUMENT_UPLOAD_PATH'), $fileName);
                        try{
                            $bucket    = UtilityController::Getmessage('BUCKET');
                            $object    = UtilityController::Getpath('BUCKET_PR_FOLDER').$fileName;
                            $filePath  = public_path('').UtilityController::Getpath('PR_DOCUMENT_UPLOAD_PATH').$fileName;
                            $ossClient = new OssClient(UtilityController::Getmessage('ACCESS_KEY_ID'), UtilityController::Getmessage('ACCESS_KEY_SECRET'), UtilityController::Getmessage('END_POINT'));
                            $ossClient->uploadFile($bucket, $object, $filePath);
                            $userPrCaseLog['case_file_name']     = $fileName;
                            $userPrCaseLog['case_status']        = 3;
                            $userPrCaseLog['case_resolved_time'] = Carbon::now();
                            $userPrCaseLog['total_time_taken']   = Carbon::parse($Input['created_at'])->diffInHours(Carbon::now(), false);
                            $result                              = UtilityController::Makemodelobject($userPrCaseLog,'Userprcaselog','',$Input['id']);
                            if($accessor->level_type == 2){
                                $userPrCase['cases_type']  = 3;
                                $userPrCase['case_status'] = 3;
                                $userPrCase['accessment_2_file_name'] = $fileName;
                                $userPrCase['accessment_2_resolve_time'] = Carbon::now();
                                $resultPrCase              = UtilityController::Makemodelobject($userPrCase,'Userprcase','',$Input['user_pr_case_id']);
                                $case_details = $Input['case_details'];
                                Mail::send('emails.notify_user_for_completed_case', $case_details, function($message) use ($case_details) {
                                    $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                                    $message->to($case_details['case_user']['email_address'])->subject('Your case has been accessed');
                                });
                            }
                            if($accessor->level_type == 1){
                                $prCaseStatusChange['case_status']                  = 2;
                                $prCaseStatusChange['accessment_1_file_name'] = $fileName;
                                $prCaseStatusChange['accessment_1_resolve_time'] = Carbon::now();
                                $resultPrCaseStatusChange                           = UtilityController::Makemodelobject($prCaseStatusChange,'Userprcase','',$Input['user_pr_case_id']);
                                $level2Accessment['user_pr_case_id']                = $Input['user_pr_case_id'];
                                $level2Accessment['accessor_id']                    = $accessor->accessor_id;
                                $level2Accessment['case_status']                    = 1;
                                $level2Accessment['case_accept_time']               = Carbon::now();
                                $resultPrCase                                       = UtilityController::Makemodelobject($level2Accessment,'Userprcaselog');
                                $setAccessorAvailabilityData['availability_status'] = 1;
                                $setAccessorAvailability                            = UtilityController::Makemodelobject($setAccessorAvailabilityData,'Accessor','',$accessor->id);
                                $parentAccessor                                     = Accessor::where('id',$accessor->accessor_id)->first();
                                $parentAccessor                                     = json_decode(json_encode($parentAccessor),true);
                                Mail::send('emails.notify_accessor', $parentAccessor, function($message) use ($parentAccessor) {
                                    $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                                    $message->to($parentAccessor['email_address'])->subject('You have been assigned a case');
                                });
                            }
                            if ($result) {
                                $responseArray = UtilityController::Generateresponse(true,'FILE_UPLOADED_SUCCESS',1,$result);
                            } else {
                                $responseArray = UtilityController::Generateresponse(false,'GENERAL_ERROR',0);
                            }
                        } catch(OssException $e) {
                            $responseArray = UtilityController::Generateresponse(false,'TRY_AGAIN',0);
                        }
                        unlink(public_path('').UtilityController::Getpath('PR_DOCUMENT_UPLOAD_PATH').$fileName);
                    } else {
                        $responseArray = UtilityController::Generateresponse(false,'FILE_TYPE_RESTRICTION',0);
                    }
                } else {
                    $responseArray = UtilityController::Generateresponse(false,'FILE_SIZE_LARGE',0);
                }
            } else {
                $fileName = '';
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Download
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To download the file 
    //In Params : Void
    //Return : json
    //Date : 13th January 2018
    //###############################################################
    public function Download(Request $request) {
        try {
            $Input = Input::all();
            switch ($Input['whichFile']) {
                case 1:
                    $fileName = $Input['case_file_name'];
                    break;
                case 2:
                    $fileName = $Input['accessment_time'][0]['case_file_name'];
                    break;
                case 3:
                    $fileName = $Input['accessment_time'][1]['case_file_name'];
                    break;
                case 4:
                    $fileName = $Input['pr_case']['case_file_name'];
                    break;
                case 5:
                    $fileName = $Input['pr_case']['accessment_1_file_name'];
                    break;
                case 6:
                    $fileName = $Input['pr_case']['accessment_2_file_name'];
                    break;
            }
            $bucket    = UtilityController::Getmessage('BUCKET');
            $object    = UtilityController::Getpath('BUCKET_PR_FOLDER').$fileName;
            $localfile = public_path('').UtilityController::Getpath('PR_DOCUMENT_DOWNLOAD_PATH').$fileName;
            $options   = array(
                OssClient::OSS_FILE_DOWNLOAD => $localfile,
            );
            try{
                $result['url']       = UtilityController::Getpath('DOWNLOAD_ON_USER_SYSTEM').$fileName;
                $result['file_name'] = $fileName;
                $ossClient           = new OssClient(UtilityController::Getmessage('ACCESS_KEY_ID'), UtilityController::Getmessage('ACCESS_KEY_SECRET'), UtilityController::Getmessage('END_POINT'));
                $ossClient->getObject($bucket, $object, $options);
                $responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$result);
            } catch(OssException $e) {
                $sendExceptionMail = UtilityController::Sendexceptionmail($e);
                $responseArray = UtilityController::Generateresponse(false,'TRY_AGAIN',0);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Unlinkdownloadedfile
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To unlink the downloaded file from our file system
    //In Params : Void
    //Return : json
    //Date : 13th January 2018
    //###############################################################
    public function Unlinkdownloadedfile(Request $request) {
        try {
            $Input         = Input::all();
            $fileUnlink    = unlink(public_path('').UtilityController::Getpath('PR_DOCUMENT_DOWNLOAD_PATH').$Input['file_name']);
            $responseArray = UtilityController::Generateresponse(true,'FILE_UNLINKED',1,$fileUnlink);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Cancelcase
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To cancel the case
    //In Params : Void
    //Return : json
    //Date : 24th January 2018
    //###############################################################
    public function Cancelcase(Request $request) {
        try {
            $Input               = Input::all();
            $updateAccessor = Userprcaselog::where('user_pr_case_id',$Input['id'])->orderBy('id','DESC')->first();
            \DB::beginTransaction();
            if(!empty($updateAccessor)){
                $updateAccessorResult = Accessor::where('id',$updateAccessor['accessor_id'])->update(['availability_status'=>1]);
                $updateAccessorResult = Userprcaselog::where('id',$updateAccessor['id'])->update(['case_status'=>4]);
            }
            $Input['cases_type'] = 4;
            $result              = UtilityController::Makemodelobject($Input,'Userprcase','',$Input['id']);
            if ($result) {
                Mail::send('emails.notify_user_for_canceled_case', $Input, function($message) use ($Input) {
                    $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                    $message->to($Input['email'])->subject('Case cancellation');
                });
            }
            $responseArray = UtilityController::Generateresponse(true,'CASE_CANCELED',1,$result);
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }
}

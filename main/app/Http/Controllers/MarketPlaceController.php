<?php

/**
 * Short description for file
 *
 * PHP version 5 and 7
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 */
/*
 * Place includes controller for login & forgot password.
 */

/**
Pre-Load all necessary library & name space
*/

namespace App\Http\Controllers;

//load required library by use
use App\DesignerSkill;
use App\Jobs\MpInvitationNotAccepted;
use App\Jobs\MpInvitationNotification;
use App\Jobs\MpMessageBoardMail;
use App\Jobs\MpProjectDeadlineNotification;
use App\Jobs\SignupVerificationLinkNotAccessed;
use App\Jobs\MpNotifyBuyerForRateAndReview;

use App\MarketplaceAction;
use App\MarketPlaceDesignerRating;
use App\MarketPlaceDesignerReview;
use App\MarketPlaceDesignerSignupInvitation;
use App\MarketPlaceGalleryCategory;
use App\MarketPlaceInsBankCategory;
use App\MarketPlaceMessageBoard;
use App\MarketPlacePayment;
use App\MarketPlacePortfolioCategoryDetail;
use App\MarketPlaceProject;
use App\MarketPlaceProjectsDetailInfo;
use App\MarketplaceSellerPayments;
use App\MarketPlaceSellerServiceImage;
use App\MarketPlaceSellerServicePackage;
use App\MarketPlaceSellerServiceProjectExtra;
use App\MarketPlaceSellerServiceSkill;
use App\MarketPlaceUpload;
use App\MarketPlaceUploadAttachment;
use App\MarketPlaceSellerBankDetails;
use App\ProjectSkill;
use App\SkillSet;
use App\SystemNotification;
use App\User;
use App\UserEmailLog;
use App\School;
use App\MarketPlaceCart;
USE App\Country;
use App\MarketPlaceInspirationBankCategoryDetail;
use Auth;
use URL;
use App\MarketPlaceInspirationBankUserFavourite;
use App\MarketPlaceWallet;
//load session & other useful library
use Carbon\carbon;
use DateTime;
use DateTimeZone;
use File;
use Hash;
use View;
use Illuminate\Http\Request;
//define model
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Image;
use OSS\OssClient;
use PDF;
use Validator;
use ZipArchive;

/**
 * Photos
 * @package    MarketPlaceController
 * @subpackage Controller
 * @author     Senil Shah <senil@creolestudios.com>
 */
class MarketPlaceController extends BaseController
{

    public function __construct()
    {
        //Artisan::call('cache:clear');
    }

    public function index()
    {

    }

    //###############################################################
    //Function Name : Getprojectdetail
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get Project Detail for new project invitation page
    //In Params : Void
    //Return : json
    //Date : 20th March 2018
    //###############################################################
    public function Getprojectdetail(Request $request)
    {
        try {
            $validator  = UtilityController::ValidationRules($request->all(), 'MarketPlaceProject');
            $returnData = $validator;
            $input      = Input::all();

            $projectData = MarketPlaceProject::where('id', $input['id'])->with([
                'projectCreator' => function ($query) {
                    $query->select('id', 'first_name', 'last_name')->get();
                },
                'skills'         => function ($query) {
                    $query->with('skillDetail')->get();
                },
            ])->where('project_status', 1)->first();

            if ($projectData) {
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $projectData);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 400, '');
            }

            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, $e->getMessage(), '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Getschools
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get schools
    //In Params : Void
    //Return : json
    //Date : 21th March 2018
    //###############################################################
    public function Getschools(Request $request)
    {
        try {
            $schoolsData = School::select('id', 'name')->where('status', 1)->get();

            if ($schoolsData) {
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $schoolsData);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 400, '');
            }

            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Getskills
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get skills
    //In Params : Void
    //Return : json
    //Date : 21th March 2018
    //###############################################################
    public function Getskills(Request $request)
    {
        try {
            $skillData = SkillSet::select('id', 'skill_name', 'skill_name_chinese')->where('status', 1)->get();
            if ($skillData) {
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $skillData);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 400, '');
            }

            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
    // page in which buyer will add review for any desinger
    //###############################################################
    //Function Name : Adddesignerreview
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To add review of designer
    //In Params : Void
    //Return : json
    //Date : 21th March 2018
    //###############################################################
    public function Adddesignerreview(Request $request)
    {
        try {
            if (Auth::check()) {
                \DB::beginTransaction();
                $Input                  = Input::all();
                $Input['mp_project_id'] = $Input['whichProject']['mp_project_id'];

                if (array_key_exists("rating", $Input) && !empty($Input['rating'])) {
                    $rating_count = 0;
                    foreach ($Input['rating'] as $key => $value) {
                        $rating[$value['key']] = $value['value'];
                    }
                    foreach ($rating as $key => $value) {
                        if ($key == 'quality_rating') {
                            $rating_count += (UtilityController::getMessage('QUALITY_RATING_COUNT') / 100) * $value;
                        }
                        if ($key == 'creativity_rating') {
                            $rating_count += (UtilityController::getMessage('CREATIVITY_RATING_COUNT') / 100) * $value;
                        }
                        if ($key == 'buyers_requirement_rating') {
                            $rating_count += (UtilityController::getMessage('BUYERS_REQUIREMENT_RATING_COUNT') / 100) * $value;
                        }
                        if ($key == 'timeliness_rating') {
                            $rating_count += (UtilityController::getMessage('TIMELINESS_RATING_COUNT') / 100) * $value;
                        }

                        if ($key == 'responsiveness_rating') {
                            $rating_count += (UtilityController::getMessage('RESPONSIVENESS_RATING_COUNT') / 100) * $value;
                        }
                    }
                    $Input['matrix_rating'] = json_encode($rating);
                    $Input['rating']        = $rating_count;
                    $Input['rating_by']     = Auth::user()->id;
                    $Input['rating_to_id']  = $Input['whichProject']['assigned_designer_id'];

                    $validator = UtilityController::ValidationRules($Input, 'MarketPlaceDesignerRating');
                    if (!$validator['status']) {
                        $errorMessage = explode('.', $validator['message']);
                        $returnData   = UtilityController::Generateresponse(false, $errorMessage['0'], 0, '');
                    } else {
                        $resultAddReview = UtilityController::Makemodelobject($Input, 'MarketPlaceDesignerRating');
                        if (!empty($resultAddReview)) {
                            $overallRating                 = MarketPlaceDesignerRating::where('rating_to_id', $Input['rating_to_id'])->get();
                            $totalRating['total_avg_rate'] = 0;
                            foreach ($overallRating as $key => $value) {
                                $totalRating['total_avg_rate'] += $value['rating'];
                            }
                            $totalRating['total_avg_rate'] = $totalRating['total_avg_rate'] / $overallRating->count();
                            $ratingUpdate                  = UtilityController::Makemodelobject($totalRating, 'User', '', $Input['rating_to_id']);
                            $returnRating                  = UtilityController::Generateresponse(true, 'RATED', 200, $resultAddReview['data']);
                        } else {
                            $returnMessage=$Input['language']=='en'?'NOT_RATED':'NOT_RATED_CHINESE';
                            $returnRating = UtilityController::Generateresponse(false, $returnMessage, 400, '');
                        }
                    }
                }
                if (array_key_exists("review", $Input)) {
                    $Input['review_by']    = Auth::user()->id;
                    $Input['review_to_id'] = $Input['whichProject']['assigned_designer_id'];
                    $validator             = UtilityController::ValidationRules($Input, 'MarketPlaceDesignerReview');
                    if (!$validator['status']) {
                        $errorMessage = explode('.', $validator['message']);
                        $returnData   = UtilityController::Generateresponse(false, $errorMessage['0'], 0, '');
                    } else {
                        $resultAddReview = UtilityController::Makemodelobject($Input, 'MarketPlaceDesignerReview');
                        if (!empty($resultAddReview)) {
                            $returnMessage=$Input['language']=='en'?'REVIEW_ADDED':'REVIEW_ADDED_CHINESE';
                            $returnReview = UtilityController::Generateresponse(true, $returnMessage, 200, $resultAddReview['data']);
                        } else {
                            $returnMessage=$Input['language']=='en'?'REVIEW_NOT_ADDED':'REVIEW_NOT_ADDED_CHINESE';
                            $returnReview = UtilityController::Generateresponse(false, $returnMessage, 400, '');
                        }
                    }
                }
                \DB::commit();
                if (array_key_exists("rating", $Input) && !array_key_exists("review", $Input)) {
                    $returnData = $returnRating;
                } else if (array_key_exists("review", $Input) && !array_key_exists("rating", $Input)) {
                    $returnData = $returnReview;
                } else {
                    if (!empty($resultAddReview) && !empty($resultAddReview)) {
                        $returnMessage=$Input['language']=='en'?'RATING_REVIEW_DONE':'RATING_REVIEW_DONE_CHINESE';
                        $returnData = UtilityController::Generateresponse(true, $returnMessage, 1);
                    } else {
                        $returnMessage=$Input['language']=='en'?'RATING_REVIEW_NOT_DONE':'RATING_REVIEW_NOT_DONE_CHINESE';
                        $returnData = UtilityController::Generateresponse(false,  $returnMessage, 0);
                    }

                }
            } else {
                $returnMessage=$Input['language']=='en'?'LOGGED_OUT':'LOGGED_OUT_CHINESE';
                $returnData = UtilityController::Generateresponse(false, $returnMessage, 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, $e->getMessage(), '', '');
        }
        return $returnData;
    }
    // came in both user pages
    //###############################################################
    //Function Name : Getdesignerreview
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get reviews of designer
    //In Params : Void
    //Return : json
    //Date : 21th March 2018
    //###############################################################
    public function Getdesignerreview(Request $request)
    {
        try {
            \DB::beginTransaction();
            $input   = Input::all();
            $reviews = MarketPlaceDesignerReview::select('id', 'review', 'review_by', 'review_to_id')->with([
                'Reviewgivenby' => function ($query) {
                    $query->select('id', 'first_name', 'last_name')->first();
                },
            ])->where('status', 1)->where('review_to_id', $input['id'])->get();
            if (!empty($reviews)) {
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $reviews);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            \DB::commit();
            return $returnData;
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
    // designer
    //###############################################################
    //Function Name : Activeprojects
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get active projects
    //In Params : Void
    //Return : json
    //Date : 21th March 2018
    //###############################################################
    public function Activeprojects(Request $request)
    {
        try {
            $UserID = Auth::user()->id;
            \DB::beginTransaction();
            $Input=Input::all();
            $projectsQuery = MarketPlaceProject::select('*')
                ->whereHas('selected_designer', function ($q) use ($UserID) {
                    $q->where('assigned_designer_id', $UserID);
                })
                ->with([
                    'projectCreator'    => function ($query) {
                        $query->select('id', 'first_name', 'last_name', 'display_name', 'image');
                    },
                    'skills'            => function ($query) {
                        $query->with('skillDetail')->get()->groupby('skill_set_id');
                    },
                    'projectdetailinfo' => function ($query) {
                        $query->get();
                    },

            ])->withCount('message_board')->whereIn('project_status', array(1,7,8))->orderby('show_first','DESC')->orderby('id','DESC')->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))->toArray();
            
            if (!empty($projectsQuery)) {
                foreach ($projectsQuery['data'] as $key => $value) {
                    $date = Carbon::parse($value['created_at'])->timezone(Auth::user()->timezone);
                    $projectsQuery['data'][$key]['created_at']     = UtilityController::Changedateformat($date, 'd F, Y');
                    $projectAt                                     = Carbon::parse($value['created_at']);
                    $diffDays                                      = $projectAt->diffInDays(Carbon::now());
                    $projectsQuery['data'][$key]['days_remaining'] = $value['project_timeline'] - $diffDays;

                    if($projectsQuery['data'][$key]['days_remaining']>=0){
                        $projectAt = Carbon::parse($value['created_at'])->addDays($value['project_timeline']);
                        $projectsQuery['data'][$key]['deadline'] = $projectAt->toDateTimeString(
                        );    
                    }
                    else{
                        $projectsQuery['data'][$key]['deadline'] = '00 hrs 00 mins 00 secs';
                    }

                    $tempCategory = [];
                    if (!empty($value['skills'])) {
                        foreach ($value['skills'] as $keyInner => $valueInner) {

                            $tempSkills     = $valueInner['skill_detail'];
                            $tempCategory[] = $tempSkills['skill_name'];
                        }
                    }

                    $projectsQuery['data'][$key]['category_list'] = implode(',', $tempCategory);

                    $projectsQuery['data'][$key]['project_creator']['image'] = UtilityController::Imageexist($value['project_creator']['image'], public_path('') . UtilityController::Getmessage('USER_PROFILE_PATH'), url('') . UtilityController::Getmessage('USER_PROFILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));
                    if ($value['projectdetailinfo']['invitation_accepted_at'] == '') {
                        $projectsQuery['data'][$key]['accepted_at'] = 'Pending';
                    } else {
                        $date = Carbon::parse($value['projectdetailinfo']['invitation_accepted_at'])->timezone(Auth::user()->timezone);
                        $projectsQuery['data'][$key]['accepted_at'] = UtilityController::Changedateformat($date, 'd F, Y');
                    }
                }
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $projectsQuery);
            } else {
                $returnData = UtilityController::Generateresponse(false,'NO_DATA' , 400, '');
            }
            \DB::commit();
            return $returnData;
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnMessage=$Input['language']=='en'?'GENERAL_ERROR':'GENERAL_ERROR_CHINESE';
            $returnData        = UtilityController::Generateresponse(false,$returnMessage , '', '');
            return $returnData;
        }
    }
    // designer
    //###############################################################
    //Function Name : Openprojects
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get in projects invitations means which have open status
    //In Params : Void
    //Return : json
    //Date : 21th March 2018
    //###############################################################
    public function Openprojects(Request $request)
    {
        try {
            \DB::beginTransaction();
            $page          = 10;
            $input         = Input::all();
            $offset        = $input['offset'];
            $projectsQuery = MarketPlaceProject::select('*')->with([
                'projectCreator' => function ($query) {
                    $query->select('id', 'first_name', 'last_name', 'display_name')->get();
                },
            ])->where('project_status', 2);
            $projects['list'] = $projectsQuery
                ->orderby('id', 'DESC')
                ->offset($offset * $page)
                ->take($page)
                ->get();
            $projects['count'] = $projectsQuery->count();
            if (!empty($projects)) {
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $projects);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            \DB::commit();
            return $returnData;
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Gallerydetail
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : Get gallery detail
    //In Params : Void
    //Return : json
    //Date : 22nd March 2018
    //###############################################################
    public function Gallerydetail(Request $request)
    {
        try {
            \DB::beginTransaction();
            $input       = Input::all();
            $galleryData = MarketPlaceUpload::select('*')->with([
                'uploadedby' => function ($query) {
                    $query->select('id', 'first_name', 'last_name', 'display_name')->get();
                },
                'files'      => function ($query) {
                    $query->get();
                },
            ])->where('status', 1)->where('id', $input['id'])->first();
            if (!empty($galleryData)) {
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $galleryData);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            \DB::commit();
            return $returnData;
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
    //###############################################################
    //Function Name : Getgallerylisting
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : Get gallery listing
    //In Params : Void
    //Return : json
    //Date : 22nd March 2018
    //###############################################################
    public function Getgallerylisting(Request $request)
    {
        try {
            \DB::beginTransaction();
            ## Search string for saerch data.
            $page         = 10;
            $input        = Input::all();
            $offset       = $input['offset'];
            $galleryQuery = MarketPlaceUpload::select('*')->with([
                'uploadedby' => function ($query) {
                    $query->select('id', 'first_name', 'last_name', 'display_name')->get();
                },
                'files'      => function ($query) {
                    $query->get();
                },
            ]);
            if (!empty($input['search'])) {
                $galleryQuery->where('upload_title', 'like', '%' . $input['search'] . '%')
                    ->orWhere('upload_description', 'like', '%' . $input['search'] . '%');
            }
            $galleryQuery->where('status', 1);
            $galleryData['list'] = $galleryQuery
                ->orderby('id', 'DESC')
                ->offset($offset * $page)
                ->take($page)
                ->get();
            $galleryData['count'] = $galleryQuery->count();
            if (!empty($galleryData)) {
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $galleryData);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            \DB::commit();
            return $returnData;
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Getgallerypurchases
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : Get gallery purchases
    //In Params : Void
    //Return : json
    //Date : 22nd March 2018
    //###############################################################
    public function Getgallerypurchases(Request $request)
    {
        try {
            \DB::beginTransaction();
            ## Search string for saerch data.
            $page         = 10;
            $input        = Input::all();
            $offset       = $input['offset'];
            $galleryQuery = MarketPlacePurchases::select('*')->with([
                'purchasedFiles' => function ($query) {
                    $query->select('*')->with([
                        'uploadedby' => function ($query) {
                            $query->select('id', 'first_name', 'last_name', 'display_name')->get();
                        },
                        'files'      => function ($query) {
                            $query->get();
                        },
                    ])->get();
                },
            ])->where('upload_of', 2)->where('user_id', $input['id']);
            $galleryData['list'] = $galleryQuery
                ->orderby('id', 'DESC')
                ->offset($offset * $page)
                ->take($page)
                ->get();
            $galleryData['count'] = $galleryQuery->count();
            if (!empty($galleryData)) {
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $galleryData);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            \DB::commit();
            return $returnData;
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Createnewproject
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To create new project by buyer
    //In Params : Void
    //Return : json
    //Date : 16th March 2018
    //###############################################################
    public function Createnewproject(Request $request)
    {
        try {
            if (Auth::user()) {
                \DB::beginTransaction();
                $Input = Input::all();  

                $current_domain = UtilityController::Getmessage('CURRENT_DOMAIN'); 
                /*if($current_domain=='cn'){
                    if ($Input['project_budget'] < UtilityController::Getmessage('MIN_BUDGET_VALUE')) {
                        $returnMessage = ($Input['language']=='en'?'MY_SERVICE_PRICE':'MY_SERVICE_PRICE_CHINESE');
                        throw new \Exception(UtilityController::Getmessage('$returnMessage'));
                    }
                } else {
                    if ($Input['project_budget'] < UtilityController::Getmessage('MIN_BUDGET_DOLLAR_VALUE')) {
                        $returnMessage = ($Input['language']=='en'?'MY_SERVICE_PRICE_SINGAPORE_DOLLAR':'MY_SERVICE_PRICE_CHINESE_SINGAPORE_DOLLAR');
                        throw new \Exception(UtilityController::Getmessage('$returnMessage'));
                    }
                }*/

                if ($Input['project_timeline'] < UtilityController::Getmessage('MIN_TIMELINE_VALUE')) {
                    $returnMessage = ($Input['language']=='en'?'MIN_TIMELINE':'MIN_TIMELINE_CHINESE');
                    throw new \Exception(UtilityController::Getmessage('MIN_TIMELINE'));
                }
                $Input['project_created_by_id'] = Auth::user()->id;
                $validator                      = UtilityController::ValidationRules($Input, 'MarketPlaceProject');
                if (!$validator['status']) {
                    $errorMessage  = explode('.', $validator['message']);
                    $responseArray = UtilityController::Generateresponse(false, $errorMessage['0'], 0);
                } else {
                    if (isset($Input['attachment_name'])) {
                        /*foreach ($Input['attachment_name'] as $key => $value) {
                            if ($value->getClientSize()==0 || ($value->getClientSize() > UtilityController::Getmessage('MAX_UPLOAD_FILE_SIZE'))) { 
                                throw new \Exception(UtilityController::Getmessage('ALL_FILES_UPTO_MB'));
                            }
                        }*/
                        $sum = 0;
                        foreach ($Input['attachment_name'] as $key => $value) {
                            $sum = $sum + $value->getClientSize();
                            if ($sum > UtilityController::Getmessage('MAX_UPLOAD_FILE_SIZE')) {
                                $returnMessage=$Input['language']=='en'?'ALL_FILES_UPTO_MB':'ALL_FILES_UPTO_MB_CHINESE';
                                throw new \Exception(UtilityController::Getmessage($returnMessage));
                            }
                        }
                    }
                    //Code to insert running number starts here by Ketan Solanki 24th July 2018             
                    $projectRunningNumber = UtilityController::GenerateRunningNumber('MarketPlaceProject','project_number','');                                   
                    $Input['project_number'] = $projectRunningNumber;
                    
                    //Code to insert running number ends here 
                    $resultProject = UtilityController::Makemodelobject($Input, 'MarketPlaceProject');
                    
                    if ($resultProject) {
                        $slug['slug'] = str_slug($Input['project_title'], '-');
                        $slug['slug'] = $slug['slug'] . ":" . base64_encode($resultProject['id']);
                        $updateSlug   = UtilityController::Makemodelobject($slug, 'MarketPlaceProject', '', $resultProject['id']);
                        if (array_key_exists('skills', $Input)) {
                            if($Input['whatAction']=='project-pending'){
                                $Input['skills'] = explode(',', $Input['skills']);
                            }
                            $skillSets = $Input['skills'];
                            foreach ($skillSets as $key => $value) {
                                $projectCategory[$key]['mp_project_id'] = $resultProject['id'];
                                $projectCategory[$key]['skill_set_id']  = $value;
                                $projectCategory[$key]['created_at']    = Carbon::now();
                                $projectCategory[$key]['updated_at']    = Carbon::now();
                            }
                            ProjectSkill::insert($projectCategory);
                        } else {
                            $returnMessage=$Input['language']=='en'?'SELECT_ATLEAST_ONE_CATEGORY':'SELECT_ATLEAST_ONE_CATEGORY_CHINESE';
                            $responseArray = UtilityController::Generateresponse(false, $returnMessage, 0);
                            return response()->json($responseArray);
                        }
                        if (isset($Input['attachment_name'])) {
                            $removeFiles = explode(',', $Input['remove_attachments']);

                            if ((count($Input['attachment_name']) == count($removeFiles)) && $removeFiles['0']!='') {
                                $returnMessage = ($Input['language']=='en'?'SELECT_ATLEAST_ONE_IMAGE':'SELECT_ATLEAST_ONE_IMAGE_CHINESE');
                                throw new \Exception(UtilityController::Getmessage($returnMessage));
                            } else {
                                foreach ($removeFiles as $key => $value) {
                                    foreach ($Input['attachment_name'] as $keyInner => $valueInner) {
                                        if ($value == $valueInner->getClientOriginalName()) {
                                            unset($Input['attachment_name'][$keyInner]);
                                        }
                                    }
                                }
                            }
                            if (count($Input['attachment_name']) > 5) {
                                $returnMessage=$Input['language']=='en'?'FILE_LIMIT_EXCEED_NEW_PROJECT':'FILE_LIMIT_EXCEED_NEW_PROJECT_CHINESE';
                                throw new \Exception(UtilityController::Getmessage($returnMessage));
                            } else {
                                $zip         = new ZipArchive;
                                $zipFileName = rand() . time();
                                $zip->open(public_path('') . UtilityController::Getpath('MP_ATTACHMENT_UPLOAD_PATH') . $zipFileName, ZipArchive::CREATE);
                                foreach ($Input['attachment_name'] as $key => $value) {
                                    $fileName  = rand() . time() . str_replace(' ', '_', $value->getClientOriginalName());
                                    $extension = \File::extension($fileName);
                                    if (!in_array($extension, array('zip', 'ZIP', 'tar', 'TAR', 'rar', 'RAR', 'js', 'JS', 'tar.gz', 'TAR.GZ'))) {
                                        $value->move(public_path('') . UtilityController::Getpath('MP_ATTACHMENT_UPLOAD_PATH'), $fileName);
                                        chmod(public_path('') . UtilityController::Getpath('MP_ATTACHMENT_UPLOAD_PATH') . $fileName, 0777);
                                        $zip->addFile(public_path('') . UtilityController::Getpath('MP_ATTACHMENT_UPLOAD_PATH') . $fileName, $fileName);
                                        $fileList[] = $fileName;
                                    } else {
                                        $returnMessage=$Input['language']=='en'?'NO_COMPRESSED_ALLOWED':'NO_COMPRESSED_ALLOWED_CHINESE';
                                        throw new \Exception(UtilityController::Getmessage($returnMessage));
                                    }
                                }
                                $zip->close();
                                chmod(public_path('') . UtilityController::Getpath('MP_ATTACHMENT_UPLOAD_PATH') . $zipFileName, 0777);
                                $bucket   = UtilityController::Getmessage('BUCKET');
                                $object   = UtilityController::Getpath('BUCKET_MP_FOLDER') . $zipFileName;
                                $filePath = public_path('') . UtilityController::Getpath('MP_ATTACHMENT_UPLOAD_PATH') . $zipFileName;
                                if (filesize($filePath) <= UtilityController::Getmessage('FILE_SIZE_15')) {
                                    $ossClient                     = new OssClient(UtilityController::Getmessage('ACCESS_KEY_ID'), UtilityController::Getmessage('ACCESS_KEY_SECRET'), UtilityController::Getmessage('END_POINT'));
                                    $attachment['mp_project_id']   = $resultProject['id'];
                                    $attachment['attachment_name'] = $zipFileName;
                                    $validator                     = UtilityController::ValidationRules($attachment, 'MarketPlaceProjectAttachment');
                                    if (!$validator['status']) {
                                        $errorMessage  = explode('.', $validator['message']);
                                        $responseArray = UtilityController::Generateresponse(false, $errorMessage['0'], 0);
                                    } else {
                                        foreach ($fileList as $key => $value) {
                                            unlink(public_path('') . UtilityController::Getpath('MP_ATTACHMENT_UPLOAD_PATH') . $value);
                                        }
                                        $ossClient->uploadFile($bucket, $object, $filePath);
                                        $resultAttachments = UtilityController::Makemodelobject($attachment, 'MarketPlaceProjectAttachment');
                                        unlink(public_path('') . UtilityController::Getpath('MP_ATTACHMENT_UPLOAD_PATH') . $zipFileName);
                                    }
                                } else {
                                    $returnMessage=$Input['language']=='en'?'FILE_SIZE_LARGE_15':'FILE_SIZE_LARGE_15_CHINESE';
                                    throw new \Exception(UtilityController::Getmessage($returnMessage));
                                }
                            }
                        }

                        $messsage_text="You have created new project ".$Input['project_title'];
                        $messsage_text_chi=" 您已创建新项目 ".$Input['project_title'];
                        self::Insertmessageboardnotifications(Auth::user()->id,$resultProject['id'],$messsage_text,$messsage_text_chi,4,1,1);

                        /*$messsage_text="New message in ".$Input['project_title'];
                        $messsage_text_chi="新消息 ".$Input['project_title'];
                        self::Insertsystemnotifications(Auth::user()->id,0,1,$messsage_text,$messsage_text_chi,$slug['slug']);*/
                        
                        $buyer_name['first_name']       = Auth::user()->first_name;
                        $buyer_name['last_name']        = Auth::user()->last_name;
                        $buyer_name['current_language'] = Auth::user()->current_language;
                        $buyer_name['project_id']       = $Input['project_number'];

                        $buyer_name['subject']=Auth::user()->current_language==1?'Thank you for creating new project':'感谢您创建新项目';

                        $buyer_name['content']=Auth::user()->current_language==1?'Hello <strong>'.$buyer_name['first_name'].' '.$buyer_name['last_name'].' ,
                            </strong><br/>
                            <br/>
                            Thank you for posting a Project!  <strong>ID: '.$buyer_name['project_id'].' </strong><br/<br/><br/><span>Your selected Seller has been notified and will revert to you soon. The Seller may liaise with you on the Project Page to clarify your Project Brief.<span><br/><br/><br/>':'你好 <strong>'.$buyer_name['first_name'].$buyer_name['last_name'].'</strong> ,
                            <br/>
                            <br/>
                            感谢您发布项目!  <strong>ID: '.$buyer_name['project_id'].'</strong><br/<br/>您选择的卖家已收到通知，并会尽快回复您。卖家可以在项目页面上与您联系, 以更好地了解您的项目简介。<br/><br/><br/>';

                        $buyer_name['footer_content']=Auth::user()->current_language==1?'From,<br /> SixClouds Sixteen':'六云十六 ';

                        $buyer_name['footer']=Auth::user()->current_language==1?'SixClouds':'六云';

                        Mail::send('emails.email_template',$buyer_name , function ($message) use ($buyer_name) {
                        $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                        $message->to(Auth::user()->email_address)->subject($buyer_name['subject']);
                        });

                        if($Input['whatAction']=='project-pending' && array_key_exists('assigned_designer_id', $Input)){

                            $user = User::where('slug',$Input['assigned_designer_id'])->first();
                            $data['mp_project_id']        = $resultProject['id'];
                            $data['assigned_designer_id'] = $user['id'];

                            $resultDetailInfo = UtilityController::Makemodelobject($data, 'MarketPlaceProjectsDetailInfo');
                            if ($resultDetailInfo) {
                                $Input['designer']['current_language'] = $user['current_language'];
                                $Input['designer']['first_name'] = $user['first_name'];
                                $Input['designer']['last_name'] = $user['last_name'];
                                $Input['designer']['email_address'] = $user['email_address'];
                                $Input['projectInfo']['slug'] = $resultProject['slug'];

                                $Input['subject']=$Input['designer']['current_language']==1?'Project Assigned':'项目已分配';

                                $Input['content']=$Input['designer']['current_language']==1?'
                                Congratulations! You have been selected for a Project.<br/><br/>Click <a href="'.URL::to('/seller-project-info-invitation/'.$slug['slug']).'">here</a> to review the Project brief. You can liaise with the Buyer on the Project Page.
                                <br><br>
                                Remember, you have 24 hours to accept this challenge!
                                <br><br>
                                Grab this opportunity to showcase your talents!':'

                                恭喜您！您已被挑选负责一个项目。<br><br>请点击 <a href="'.URL::to('/seller-project-info-invitation/'.$slug['slug']).'">此处</a> 查看项目简介。<br><br>温馨提示，您有24小时来接受这份挑战！
                                <br><br>赶紧抓住这次机会来展示您的才能！';

                                $Input['footer_content']=$Input['designer']['current_language']==1?'From,<br /> SixClouds Sixteen':'六云十六 ';

                                $Input['footer']=$Input['designer']['current_language']==1?'SixClouds':'六云';

                                Mail::send('emails.email_template', $Input, function ($message) use ($Input) {
                                    $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                                    $message->to($Input['designer']['email_address'])->subject($Input['subject']);

                                });
                                $addProjectIdToCart = MarketPlaceCart::where('cart_id',$Input['cart_id'])->update(array('mp_project_id' => $resultProject['id']));
                                $notifyDesigner = (new MpInvitationNotification($resultProject['id'], 12))->delay(Carbon::now()->addHours(12));
                                dispatch($notifyDesigner);
                                $notifyDesigner = (new MpInvitationNotification($resultProject['id'], 18))->delay(Carbon::now()->addHours(18));
                                dispatch($notifyDesigner);
                                $notifybuyer = (new MpInvitationNotAccepted($resultProject['id']))->delay(Carbon::now()->addHours(24));
                                dispatch($notifybuyer);
                            }
                            $returnMessage=$Input['language']=='en'?'PROJECT_CREATED_AND_INVITATION_SENT':'PROJECT_CREATED_AND_INVITATION_SENT_CHINESE';
                            $responseArray = UtilityController::Generateresponse(true, $returnMessage, 1, $resultProject);
                        } else {
                            $returnMessage=$Input['language']=='en'?'PROJECT_CREATED':'PROJECT_CREATED_CHINESE';
                            $responseArray = UtilityController::Generateresponse(true, $returnMessage, 1, $slug['slug']);
                        }
                        \DB::commit();

                    } else {
                        $returnMessage=$Input['language']=='en'?'GENERAL_ERROR':'GENERAL_ERROR_CHINESE';
                        $responseArray = UtilityController::Generateresponse(false, $returnMessage, 0);
                    }
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, $e->getMessage(), 0);
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Createnewdesigner
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To create new designer signing up
    //In Params : Void
    //Return : json
    //Date : 21th March 2018
    //###############################################################
    public function Createnewdesigner(Request $request)
    {
        try {
            \DB::beginTransaction();
            $Input = Input::all();
            
            if($Input['direct_signup'] == 2)
                $Input['email_address'] = base64_decode($Input['email_address']);
            else
                $Input['email_address'] = $Input['email_address'];

            $UserExists             = User::where('email_address', $Input['email_address'])->where('status', 1)->first();
            if (!empty($UserExists)) {
                $returnMessage = $Input['language']=='en'?'EMAIL_ALREADY_EXISTS':($Input['language']=='chi'?'EMAIL_ALREADY_EXISTS_CHINESE':'EMAIL_ALREADY_EXISTS_RU');
                //$returnMessage=$Input['language']=='en'?'EMAIL_ALREADY_EXISTS':'EMAIL_ALREADY_EXISTS_CHINESE';
                throw new \Exception(UtilityController::Getmessage($returnMessage));
            }
            if ($Input['age'] < 16) {
                $returnMessage=$Input['language']=='en'?'DESIGNER_UNDER_AGE':'DESIGNER_UNDER_AGE_CHINESE';
                throw new \Exception(UtilityController::Getmessage($returnMessage));
            }
            if ($Input['age'] >= 16 && $Input['age'] < 18) {
                if (!array_key_exists('terms_and_condition', $Input) || $Input['terms_and_condition'] == false || $Input['terms_and_condition'] == 'undefined') {
                    $returnMessage=$Input['language']=='en'?'ACCEPT_TERMS_AND_CONDITIONS':'ACCEPT_TERMS_AND_CONDITIONS_CHINESE';    
                    throw new \Exception(UtilityController::Getmessage($returnMessage));
                } else {
                    $Input['terms_and_condition'] = 1;
                }

                if (!array_key_exists('guardian_id_proof', $Input)) {
                    $returnMessage=$Input['language']=='en'?'GUARDIAN_ID_PROOF_REQUIRED':'GUARDIAN_ID_PROOF_REQUIRED_CHINESE';
                    throw new \Exception(UtilityController::Getmessage($returnMessage));
                }

            }
            if (!array_key_exists('student_id_proof', $Input)) {
                $returnMessage=$Input['language']=='en'?'STUDENT_ID_PROOF_REQUIRED':'STUDENT_ID_PROOF_REQUIRED_CHINESE';
                throw new \Exception(UtilityController::Getmessage($returnMessage));
            }
            if (!array_key_exists('id_proof', $Input)) {
                $returnMessage=$Input['language']=='en'?'ID_PROOF_REQUIRED':'ID_PROOF_REQUIRED_CHINESE';
                throw new \Exception(UtilityController::Getmessage($returnMessage));
            }
            if (!array_key_exists('skills', $Input)) {
                $returnMessage=$Input['language']=='en'?'SELECT_ATLEAST_ONE_CAPABILITY':'SELECT_ATLEAST_ONE_CAPABILITY_CHINESE';
                throw new \Exception(UtilityController::Getmessage($returnMessage));
            }
            if (!array_key_exists('mp_designer_proj_availability', $Input)) {
                $returnMessage=$Input['language']=='en'?'SELECT_PROJECT_AVAILABILITY':'SELECT_PROJECT_AVAILABILITY_CHINESE';
                 throw new \Exception(UtilityController::Getmessage($returnMessage));
            }
            if ($Input['languages'] == '[],[]') {
                $returnMessage=$Input['language']=='en'?'SELECT_LANGUAGE':'SELECT_LANGUAGE_CHINESE';
                throw new \Exception(UtilityController::Getmessage($returnMessage));
            }
            if ($Input['skills'] == '') {
                $returnMessage=$Input['language']=='en'?'SELECT_CAPABILITIES':'SELECT_CAPABILITIES_CHINESE';
                throw new \Exception(UtilityController::Getmessage($returnMessage));
            }
           
            if($Input['country_id']=='other'){
                $existingCountry = Country::where('name',$Input['country_text'])->first();
                if(empty($existingCountry)){   
                    $country['name'] = $Input['country_text'];
                    $country['phonecode'] = $Input['phonecode'];
                    $country['others']=1;
                    $country['status']=2;
                    $countryResult = UtilityController::Makemodelobject($country,'Country');
                    $Input['country_id'] = $countryResult['id'];
                } else {
                    $Input['country_id'] = $existingCountry['id'];
                }
                
                $state['name'] = $Input['state_text'];
                $state['country_id'] = $Input['country_id'];
                $stateResult = UtilityController::Makemodelobject($state,'State');
                $Input['provience_id'] = $stateResult['id'];

                $city['name'] = $Input['city_text'];
                $city['state_id'] = $stateResult['id'];
                $cityResult = UtilityController::Makemodelobject($city,'City');
                $Input['city_id'] = $cityResult['id'];
            }
            $Input['languages_known'] = array();
            $Input['languages'] = explode('],[', $Input['languages']);
            $Input['languages'][0] = $Input['languages'][0].']';
            $Input['languages'][1] = '['.$Input['languages'][1];
            $languagesEnglish = json_decode($Input['languages'][0], true);
            $languagesChinese = json_decode($Input['languages'][1], true);

            if (!empty($languagesEnglish)) {
                foreach ($languagesEnglish as $key => $value) {
                    $Input['languages_known'][$key]['language']    = $value['language_known'];
                    $Input['languages_known'][$key]['proficiency'] = $value['language_proficiency'];
                }
            }
            $Input['languages_known'] = json_encode($Input['languages_known']);
            $Input['languages_known'] = urldecode(stripslashes($Input['languages_known']));
            /* language chinese */
            if (!empty($languagesChinese)) {
                foreach ($languagesChinese as $key => $value) {
                    $Input['languages_known_chinese'][$key]['language']    = $value['language_known'];
                    $Input['languages_known_chinese'][$key]['proficiency'] = $value['language_proficiency'];
                }
            }
            $Input['languages_known_chinese'] = json_encode($Input['languages_known_chinese']);
            // $Input['languages_known_chinese'] = urldecode(stripslashes($Input['languages_known_chinese']));

            $bucket = UtilityController::Getmessage('BUCKET');
            if (array_key_exists('id_proof', $Input)) {
                $fileName  = date("dmYHis").rand();
                $extension = \File::extension($Input['id_proof']->getClientOriginalName());
                if (!in_array($extension, array('zip', 'ZIP', 'tar', 'TAR', 'rar', 'RAR', 'js', 'JS'))) {
                    $Input['id_proof']->move(public_path('') . UtilityController::Getpath('ID_PROOF_ATTACHMENT_PATH'), $fileName);
                    chmod(public_path('') . UtilityController::Getpath('ID_PROOF_ATTACHMENT_PATH') . $fileName, 0777);
                } else {
                    $returnMessage=$Input['language']=='en'?'NO_COMPRESSED_ALLOWED':'NO_COMPRESSED_ALLOWED_CHINESE';
                    throw new \Exception(UtilityController::Getmessage($returnMessage));
                }
                $object   = UtilityController::Getpath('BUCKET_idproof_FOLDER') . $fileName;
                $filePath = public_path('') . UtilityController::Getpath('ID_PROOF_ATTACHMENT_PATH') . $fileName;
                if (filesize($filePath) <= UtilityController::Getmessage('FILE_SIZE_15')) {
                    $ossClient = new OssClient(UtilityController::Getmessage('ACCESS_KEY_ID'), UtilityController::Getmessage('ACCESS_KEY_SECRET'), UtilityController::Getmessage('END_POINT'));
                    $ossClient->uploadFile($bucket, $object, $filePath);
                    unlink(public_path('') . UtilityController::Getpath('ID_PROOF_ATTACHMENT_PATH') . $fileName);
                } else {
                    $returnMessage=$Input['language']=='en'?'FILE_SIZE_LARGE_15':'FILE_SIZE_LARGE_15_CHINESE';
                    throw new \Exception(UtilityController::Getmessage($returnMessage));
                }
                $Input['designer_id_proof_document'] = $fileName;
                $Input['designer_id_proof_original'] = $Input['id_proof']->getClientOriginalName();
            }
            if (array_key_exists('student_id_proof', $Input)) {
                $fileName  = date("dmYHis").rand();
                $originalFileName=$Input['student_id_proof']->getClientOriginalName();
                $extension = \File::extension($Input['student_id_proof']->getClientOriginalName());
                if (!in_array($extension, array('zip', 'ZIP', 'tar', 'TAR', 'rar', 'RAR', 'js', 'JS'))) {
                    $Input['student_id_proof']->move(public_path('') . UtilityController::Getpath('ID_PROOF_ATTACHMENT_PATH'), $fileName);
                    chmod(public_path('') . UtilityController::Getpath('ID_PROOF_ATTACHMENT_PATH') . $fileName, 0777);
                } else {
                    $returnMessage=$Input['language']=='en'?'NO_COMPRESSED_ALLOWED':'NO_COMPRESSED_ALLOWED_CHINESE';
                    throw new \Exception(UtilityController::Getmessage($returnMessage));
                }
                $object   = UtilityController::Getpath('BUCKET_idproof_FOLDER') . $fileName;
                $filePath = public_path('') . UtilityController::Getpath('ID_PROOF_ATTACHMENT_PATH') . $fileName;
                if (filesize($filePath) <= UtilityController::Getmessage('FILE_SIZE_15')) {
                    $ossClient = new OssClient(UtilityController::Getmessage('ACCESS_KEY_ID'), UtilityController::Getmessage('ACCESS_KEY_SECRET'), UtilityController::Getmessage('END_POINT'));
                    $ossClient->uploadFile($bucket, $object, $filePath);
                    unlink(public_path('') . UtilityController::Getpath('ID_PROOF_ATTACHMENT_PATH') . $fileName);
                } else {
                    $returnMessage=$Input['language']=='en'?'FILE_SIZE_LARGE_15':'FILE_SIZE_LARGE_15_CHINESE';
                    throw new \Exception(UtilityController::Getmessage($returnMessage));
                }
                $Input['student_id_proof'] = $fileName;
                $Input['student_id_proof_original'] =$originalFileName ;
            }
            if (array_key_exists('guardian_id_proof', $Input)) {
                $fileName  = date("dmYHis").rand();
                $extension = \File::extension($Input['guardian_id_proof']->getClientOriginalName());
                $Input['guardian_id_proof_original'] = $Input['guardian_id_proof']->getClientOriginalName();
                if (!in_array($extension, array('zip', 'ZIP', 'tar', 'TAR', 'rar', 'RAR', 'js', 'JS'))) {
                    $Input['guardian_id_proof']->move(public_path('') . UtilityController::Getpath('ID_PROOF_ATTACHMENT_PATH'), $fileName);
                    chmod(public_path('') . UtilityController::Getpath('ID_PROOF_ATTACHMENT_PATH') . $fileName, 0777);
                } else {
                    $returnMessage=$Input['language']=='en'?'NO_COMPRESSED_ALLOWED':'NO_COMPRESSED_ALLOWED_CHINESE';
                    throw new \Exception(UtilityController::Getmessage($returnMessage));
                }
                $object   = UtilityController::Getpath('BUCKET_idproof_FOLDER') . $fileName;
                $filePath = public_path('') . UtilityController::Getpath('ID_PROOF_ATTACHMENT_PATH') . $fileName;
                if (filesize($filePath) <= UtilityController::Getmessage('FILE_SIZE_15')) {
                    $ossClient = new OssClient(UtilityController::Getmessage('ACCESS_KEY_ID'), UtilityController::Getmessage('ACCESS_KEY_SECRET'), UtilityController::Getmessage('END_POINT'));
                    $ossClient->uploadFile($bucket, $object, $filePath);
                    unlink(public_path('') . UtilityController::Getpath('ID_PROOF_ATTACHMENT_PATH') . $fileName);
                } else {
                    $returnMessage=$Input['language']=='en'?'FILE_SIZE_LARGE_15':'FILE_SIZE_LARGE_15_CHINESE';
                    throw new \Exception(UtilityController::Getmessage($returnMessage));
                }
                $Input['guardian_id_proof'] = $fileName;
            }

            if (!empty($Input['image']) && !is_string($Input['image'])) {
                if ($Input['image']->getClientSize() <= UtilityController::Getmessage('FILE_SIZE_2')) {
                    $fileName  =
                    $extension = \File::extension($Input['image']->getClientOriginalName());
                    if (in_array($extension, array('jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG'))) {
                        $file            = $Input['image']->getMimeType();
                        $imageName       = date("dmYHis") . rand();
                        $DestinationPath = public_path() . config('constants.path.USER_PROFILE_PATH');
                        $Input['image']  = $imageName;
                        $data            = url('/') . config('constants.path.USER_PROFILE_URL') . $Input['image'];

                        if (request('x1') && request('y1') && request('w') && request('h') == 0) {
                            $w = $h = 290;
                            $x = $y = 294;
                        } else {
                            $x = (int) request('x1');
                            $y = (int) request('y1');
                            $w = (int) request('w');
                            $h = (int) request('h');
                        }
                        Image::make(Input::file('image'))->crop($w, $h, $x, $y)->resize(300, 300, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        })->save($DestinationPath . $imageName);
                    } else {
                        $returnMessage=$Input['language']=='en'?'ONLY_JPG_PNG_JPEG':'ONLY_JPG_PNG_JPEG_CHINESE';
                        throw new \Exception(UtilityController::Getmessage($returnMessage));
                    }
                } else {
                    $returnMessage=$Input['language']=='en'?'PROFILE_PHOTO_SIZE_LARGE':'PROFILE_PHOTO_SIZE_LARGE_CHINESE';
                    throw new \Exception($Input['image']->getClientOriginalName() . UtilityController::Getmessage($returnMessage));
                }
            } else {
                $Input['image'] = 'NULL';
            }
            /*$budget                         = explode(',', $Input['points']);
            $Input['mp_designer_min_price'] = $budget[0];
            $Input['mp_designer_max_price'] = $budget[1];*/
            //$Input['languages_known']       = json_encode($Input['languages']);
            if (!empty($Input['longterm_availability'])) {
                $Input['longterm_availability'] = 1;
            }
            $Input['password']                 = Hash::make($Input['password']);
            $Input['marketplace_current_role'] = 1;
            $Input['is_designer']              = 1;
            $Input['dob']                      = str_replace('/', '-', $Input['dob']);
            $Input['dob']                      = strtotime($Input['dob']);
            $Input['dob']                      = date('Y-m-d', $Input['dob']);
            $Input['current_language']         = $Input['language']=='en'?1:2;
            $userRunningNumber = UtilityController::GenerateRunningNumber('User','user_number',"U");                                
            $Input['user_number'] = $userRunningNumber;
            $resultProject                     = UtilityController::Makemodelobject($Input, 'User');

            if ($resultProject) {
                $name            = $Input['first_name'] . ' ' . $Input['last_name'];
                $slug['slug']    = str_slug($name, '-');
                $slug['slug']    = $slug['slug'] . ':' . base64_encode($resultProject['id']);
                $result          = UtilityController::Makemodelobject($slug, 'User', '', $resultProject['id']);
                $Input['skills'] = explode(',', $Input['skills']);
                $skillSets       = $Input['skills'];
                foreach ($skillSets as $key => $value) {
                    $skillCategory[$key]['user_id']      = $resultProject['id'];
                    $skillCategory[$key]['skill_set_id'] = $value;
                    $skillCategory[$key]['created_at']   = Carbon::now();
                    $skillCategory[$key]['updated_at']   = Carbon::now();
                }
                DesignerSkill::insert($skillCategory);
                $skillSets    = $Input['skills'];
                foreach ($skillSets as $key => $value) {
                    $serviceCategory[$key]['seller_id']  = $resultProject['id'];
                    $serviceCategory[$key]['skill_id']   = $value;
                    $serviceCategory[$key]['status']     = 1;
                    $serviceCategory[$key]['created_at'] = Carbon::now();
                    $serviceCategory[$key]['updated_at'] = Carbon::now();

                    $skills[]  = $value;
                    $validator = UtilityController::ValidationRules($serviceCategory[$key], 'MarketPlaceSellerServiceSkill');

                    if (!$validator['status']) {
                        $errorMessage = explode('.', $validator['message']);
                        throw new \Exception($errorMessage);
                    } else {
                        continue;
                    }
                }

                $result   = MarketPlaceSellerServiceSkill::insert($serviceCategory);
                $registerEmailToken['account_verification_token'] = rand().base64_encode($resultProject['email_address']).date('dmYHis').rand();
                $registerEmailToken['email'] = base64_encode($resultProject['email_address']);
                $registerEmailToken['language'] = $Input['current_language'];
                $resultEmailToken = UtilityController::Makemodelobject($registerEmailToken,'User','',$resultProject['id']);
                $verificationURL = action('MarketPlaceController@Registrationaccountverification', $registerEmailToken);
                // print_r($resultEmailToken); die;
                $registerEmail['user_name'] = $resultProject['first_name'].' '.$resultProject['last_name'];
                $registerEmail['registerLink'] = $verificationURL;
                $registerEmail['email_address'] = $resultProject['email_address'];
                $registerEmail['current_language'] = $resultProject['current_language'];
                
                $registerEmail['subject']=$registerEmail['current_language']==1?'SixClouds : Account Verification':'六云 ： 帐户验证';
                $supportEmail = (UtilityController::getMessage('CURRENT_DOMAIN')=='cn'?'support@sixclouds.cn':'support@sixclouds.net');
                $registerEmail['content']=$registerEmail['current_language']==1?'
                        Your account has been created. Please click on the button below to complete your registration process. 
                        <br><br><a href="'.$registerEmail['registerLink'].'" class="" style="color: #fff;
                                               background-color: #009bdd;
                                               border-color: #bd383e;
                                               text-decoration: none;display: inline-block;
                                               padding: 5px 10px;
                                               margin-bottom: 0;
                                               margin-top: 3px;
                                               font-size: 14px;
                                               font-weight: 400;
                                               line-height: 1.42857143;
                                               text-align: center;
                                               white-space: nowrap;
                                               vertical-align: middle;
                                               -ms-touch-action: manipulation;
                                               touch-action: manipulation;
                                               cursor: pointer;
                                               -webkit-user-select: none;
                                               -moz-user-select: none;
                                               -ms-user-select: none;
                                               user-select: none;
                                               background-image: none;
                                               border: 1px solid transparent;
                                               border-radius: 4px;">Click Here</a><br><br>
                        If the link does not work, copy the below link to the browser address bar.
                        <br><br>'.$registerEmail['registerLink'].'<br><br>
                        If you have any queries, please contact '.$supportEmail:'

                        您的帐号已经建立。 请点击以下链接完成注册。
                        <br><br><a href="'.$registerEmail['registerLink'].'" class="" style="color: #fff;
                                               background-color: #009bdd;
                                               border-color: #bd383e;
                                               text-decoration: none;display: inline-block;
                                               padding: 5px 10px;
                                               margin-bottom: 0;
                                               margin-top: 3px;
                                               font-size: 14px;
                                               font-weight: 400;
                                               line-height: 1.42857143;
                                               text-align: center;
                                               white-space: nowrap;
                                               vertical-align: middle;
                                               -ms-touch-action: manipulation;
                                               touch-action: manipulation;
                                               cursor: pointer;
                                               -webkit-user-select: none;
                                               -moz-user-select: none;
                                               -ms-user-select: none;
                                               user-select: none;
                                               background-image: none;
                                               border: 1px solid transparent;
                                               border-radius: 4px;">请点击这里</a><br><br>
                        如果链接不起作用，请将链接复制到浏览器地址栏。
                        <br><br>'.$registerEmail['registerLink'].'<br><br>
                        如果您有任何疑问，请联系 '.$supportEmail;

                $registerEmail['footer_content']=$registerEmail['current_language']==1?'From,<br /> SixClouds Customer Support':'六云十六 ';

                $registerEmail['footer']=$registerEmail['current_language']==1?'SixClouds':'六云';


                Mail::send('emails.email_template', $registerEmail, function ($message) use ($registerEmail) {
                    $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                    $message->to($registerEmail['email_address'])->subject($registerEmail['subject']);

                });
                $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1,$resultProject);
                $verificationLinkNotAccessed = (new SignupVerificationLinkNotAccessed($resultProject['id']))->delay(Carbon::now()->addHours(24));
                dispatch($verificationLinkNotAccessed);
                \DB::commit();
            } else {
                $returnMessage=$Input['language']=='en'?'GENERAL_ERROR':'GENERAL_ERROR_CHINESE';
                $responseArray = UtilityController::Generateresponse(false, $returnMessage, 0);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Registrationaccountverification
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To send link for email verification
    //In Params : Email
    //Return : success with email send/error message
    //###############################################################
    public function Registrationaccountverification(Request $Request) {
        //get token and email from url
        $Input = Input::all();
        $token = $Input['account_verification_token'];
        $email = base64_decode($Input['email']);
        $Input['current_language'] = $Input['language'];
        //check if the link is expired or not
        $userObject = User::where('email_address', $email)->where('account_verification_token', $token)->exists();
        // print_r($token); die;
        if ($userObject) {
            $data = User::where('email_address', $email)->where('account_verification_token', $token)->first();
            $resultActicateAccount = User::where('email_address', $email)->where('account_verification_token', $token)->update(array('account_verification_token'=>'','status'=>1));
            $registerEmail['user_name'] = $data['first_name'].' '.$data['last_name'];
            $registerEmail['email_address'] = $data['email_address'];
            $registerEmail['current_language'] = $Input['current_language'];
            $registerEmail['showImageAfterVerification'] = 1; //for passing in email template file
            $registerEmail['is_ignite'] = $data['is_ignite']; //for logo of ignite
            $directSubscription = "Direct Subscription";
            $str = base64_encode($directSubscription);
            $registerEmail['registerLink'] = url('ignite-login/'.$str);
            $registerEmail['subject']=$registerEmail['current_language']==1?'Ready-To-Go!':'Ready-To-Go!';

            // $registerEmail['content']=$registerEmail['current_language']==1?'
            //     You have successfully registered your account':'

            //     您已成功注册您的帐户。';

            $coverImageName = url('resources/assets/images/Lightbulb.png');

            $registerEmail['content']=$registerEmail['current_language']==1?'
                <html style="height: 100%; margin: 0;">
                <div style="background-image: url('.$coverImageName.');
                    background-position: center;
                    background-repeat: no-repeat;
                    background-size: cover;
                    height: 50%;"></div>
                Dear '.$registerEmail['user_name'].',<br><br>
                IT’S TIME to bring back the thirst for learning! At SixClouds, we believe learning should be fun. Let\'s not load your child with another tedious item on his To-Learn List. Designed to give your child the knowledge he needs and fun while he learns, our Learning Programmes will help him build up confidence and proficiency in no time!
                <br><br><br>
                Plus, he can enjoy these:<br><br>
                - On-The-Go<br>
                &nbsp;&nbsp;&nbsp;Anytime, anywhere, learn on any mobile or digital devices – no more fixed schedules, just click and go!<br><br>

                - Did You Say “Free”?<br>
                &nbsp;&nbsp;&nbsp;Some things in life are free! Try out our first video and downloadable worksheet!<br><br>

                - Access to a Ton of Fun<br>
                &nbsp;&nbsp;&nbsp;Get hold of close to a hundred animated videos plus quizzes and worksheets to boost learning.
                <br><br>

                Click on the button to purchase a plan that suits his needs and get him excited about learning again!
                <br><br><a href="'.$registerEmail['registerLink'].'" class="" style="color: #fff;
                                       background-color: #009bdd;
                                       border-color: #bd383e;
                                       text-decoration: none;display: inline-block;
                                       padding: 5px 10px;
                                       margin-bottom: 0;
                                       margin-top: 3px;
                                       font-size: 14px;
                                       font-weight: 400;
                                       line-height: 1.42857143;
                                       text-align: center;
                                       white-space: nowrap;
                                       vertical-align: middle;
                                       -ms-touch-action: manipulation;
                                       touch-action: manipulation;
                                       cursor: pointer;
                                       -webkit-user-select: none;
                                       -moz-user-select: none;
                                       -ms-user-select: none;
                                       user-select: none;
                                       background-image: none;
                                       border: 1px solid transparent;
                                       border-radius: 4px;">Click Here</a><br><br>
                If the link does not work, copy the below link to the browser\'s address bar.
                <br><br>'.$registerEmail['registerLink'].'<br></html>':'

                Dear '.$registerEmail['user_name'].',<br><br>
                So glad you\'re one step closer to getting your child started on a fun learning journey!
                Now, let\'s find out what best suits your child’s needs - click on the button to verify your email and get more info on our Learning Programme. 
                <br><br><a href="'.$registerEmail['registerLink'].'" class="" style="color: #fff;
                                       background-color: #009bdd;
                                       border-color: #bd383e;
                                       text-decoration: none;display: inline-block;
                                       padding: 5px 10px;
                                       margin-bottom: 0;
                                       margin-top: 3px;
                                       font-size: 14px;
                                       font-weight: 400;
                                       line-height: 1.42857143;
                                       text-align: center;
                                       white-space: nowrap;
                                       vertical-align: middle;
                                       -ms-touch-action: manipulation;
                                       touch-action: manipulation;
                                       cursor: pointer;
                                       -webkit-user-select: none;
                                       -moz-user-select: none;
                                       -ms-user-select: none;
                                       user-select: none;
                                       background-image: none;
                                       border: 1px solid transparent;
                                       border-radius: 4px;">Click Here</a><br><br>
                If the link does not work, copy the below link to the browser\'s address bar.
                <br><br>'.$registerEmail['registerLink'].'<br>';

            /*if($data['is_sphere']==1){
                print_r("expression"); die;
            }*/
            if($data['is_sphere']==1){
                $password = str_random(10);
                $registerEmail['content'] = $registerEmail['current_language']==1?$registerEmail['content'].'<br><br>Email Address : '.$data['email_address'].' <br>Password : '.$password.'':$registerEmail['content'].'<br><br>电邮 ： '.$data['email_address'].' <br>密码: '.$password.'';
                $password = Hash::make($password);
                User::where('id',$data['id'])->update(['password'=>$password]);
            }

            $registerEmail['footer_content']=$registerEmail['current_language']==1?'From,<br /> SixClouds Customer Support':'六云十六 ';

            $registerEmail['footer']=$registerEmail['current_language']==1?'SixClouds':'六云';
            
            Mail::send('emails.email_template', $registerEmail, function ($message) use ($registerEmail) {
                $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                $message->to($registerEmail['email_address'])->subject($registerEmail['subject']);

            });
            return view::make("front.verify_registration")->with('current_language', $Input['current_language']);
        } else {
            $userObject = User::where('email_address', $email)->where('status', 3)->exists();
            if($userObject){
                return view::make("front.urlexpired")->with('admin_suspended_account', 1)->with('current_language', $Input['current_language']);
            } else {
                $data = User::where('email_address', $email)->whereIn('status', [0,4])->exists();
                if($data){
                    $registerEmailToken['email'] = base64_encode($email);
                    $registerEmailToken['language'] = $Input['current_language'];
                    $verificationURL = action('MarketPlaceController@RegenerateVerificationLink', $registerEmailToken);
                    return view::make("front.urlexpired")->with('verificationURL', $verificationURL)->with('current_language', $Input['current_language']);
                } else {
                    return view::make("front.urlexpired")->with('expired', 1)->with('current_language', $Input['current_language']);
                }
            }

        }
    }


    //###############################################################
    //Function Name : RegenerateVerificationLink
    //Author : Ramya Iyer <ramya@creolestudios.com>
    //Purpose : To regenerate link for email verification
    //In Params : Email
    //Return : success with email send/error message
    //Date : 8th Aug 2018
    //###############################################################
    public function RegenerateVerificationLink(){
        $Input = Input::all();
        $email = base64_decode($Input['email']);
        $userObject = User::where('email_address', $email)->where('status', 3)->exists();
        if(!$userObject){
            $registerEmailToken['account_verification_token'] = rand().base64_encode($email).date('dmYHis').rand();
            $registerEmailToken['email'] = base64_encode($email);
            $data = User::where('email_address', $email)->first();
            $resultEmailToken = UtilityController::Makemodelobject($registerEmailToken,'User','',$data['id']);
            $Input['current_language'] = $Input['language'];
            $registerEmailToken['language'] = $Input['current_language'];

            $verificationURL = action('MarketPlaceController@Registrationaccountverification', $registerEmailToken);
            $registerEmail['user_name'] = $data['first_name'].' '.$data['last_name'];
         
            $registerEmail['registerLink'] = $verificationURL;
            $registerEmail['email_address'] = $data['email_address'];
            $registerEmail['current_language'] = $Input['current_language'];

            $registerEmail['subject']=$registerEmail['current_language']==1?'SixClouds : Account Verification':'六云 ： 帐户验证';
            $supportEmail = (UtilityController::getMessage('CURRENT_DOMAIN')=='cn'?'support@sixclouds.cn':'support@sixclouds.net');
            $registerEmail['content']=$registerEmail['current_language']==1?'
                        Your account has been created. Please click on the button below to complete your registration process. 
                        <br><br><a href="'.$registerEmail['registerLink'].'" class="" style="color: #fff;
                                               background-color: #009bdd;
                                               border-color: #bd383e;
                                               text-decoration: none;display: inline-block;
                                               padding: 5px 10px;
                                               margin-bottom: 0;
                                               margin-top: 3px;
                                               font-size: 14px;
                                               font-weight: 400;
                                               line-height: 1.42857143;
                                               text-align: center;
                                               white-space: nowrap;
                                               vertical-align: middle;
                                               -ms-touch-action: manipulation;
                                               touch-action: manipulation;
                                               cursor: pointer;
                                               -webkit-user-select: none;
                                               -moz-user-select: none;
                                               -ms-user-select: none;
                                               user-select: none;
                                               background-image: none;
                                               border: 1px solid transparent;
                                               border-radius: 4px;">Click Here</a><br><br>
                        If the link does not work, copy the below link to the browser address bar.
                        <br><br>'.$registerEmail['registerLink'].'<br><br>
                        If you have any queries, please contact '.$supportEmail:'

                        您的帐号已经建立。 请点击以下链接完成注册。
                        <br><br><a href="'.$registerEmail['registerLink'].'" class="" style="color: #fff;
                                               background-color: #009bdd;
                                               border-color: #bd383e;
                                               text-decoration: none;display: inline-block;
                                               padding: 5px 10px;
                                               margin-bottom: 0;
                                               margin-top: 3px;
                                               font-size: 14px;
                                               font-weight: 400;
                                               line-height: 1.42857143;
                                               text-align: center;
                                               white-space: nowrap;
                                               vertical-align: middle;
                                               -ms-touch-action: manipulation;
                                               touch-action: manipulation;
                                               cursor: pointer;
                                               -webkit-user-select: none;
                                               -moz-user-select: none;
                                               -ms-user-select: none;
                                               user-select: none;
                                               background-image: none;
                                               border: 1px solid transparent;
                                               border-radius: 4px;">请点击这里</a><br><br>
                        如果链接不起作用，请将链接复制到浏览器地址栏。
                        <br><br>'.$registerEmail['registerLink'].'<br><br>
                        如果您有任何疑问，请联系 '.$supportEmail;

            $registerEmail['footer_content']=$registerEmail['current_language']==1?'From,<br /> SixClouds Customer Support':'六云十六 ';

            $registerEmail['footer']=$registerEmail['current_language']==1?'SixClouds':'六云';

            Mail::send('emails.email_template', $registerEmail, function ($message) use ($registerEmail) {
                $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                $message->to($registerEmail['email_address'])->subject($registerEmail['subject']);

            });
            return view::make("front.urlexpired")->with('verification_again', 1)->with('current_language', $Input['current_language']);
        } else {
            return view::make("front.urlexpired")->with('admin_suspended_account', 1)->with('current_language', $Input['current_language']);
        }
    }


    //###############################################################
    //Function Name : Myservicepageshown
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To set the account_verification_status of user table indicating that the seller has seen the services page once
    //In Params : Void
    //Return : json
    //Date : 30th Jul 2018
    //###############################################################
    public function Myservicepageshown(Request $request)
    {
        try {
            if (Auth::check()) {
                $Input  = Input::all();
                $userId = Auth::user()->id;
                $accountStatusChange = User::where('id',$userId)->update(['account_varification_status'=>1]);
                if ($accountStatusChange) {
                    $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $accountStatusChange);
                } else {
                    $responseArray = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }

        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);

            $responseArray     = UtilityController::Generateresponse(0, 'GENERAL_ERROR', '', '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Getdesigners
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To get the detilas of registered designers for displaying
    //In Params : Void
    //Return : json
    //Date : 21th March 2018
    //###############################################################
    public function Getdesigners(Request $request)
    {
        try {
            $Input     = Input::all();
            $designers = self::Designerfunction($Input);
            if($designers){
                $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $designers);
            }else{
                $responseArray = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
            }

        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, 'GENERAL_ERROR', '', '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Designerfunction
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To get the designers.(used in Getdesigners and Dofilter method)
    //In Params : Void
    //Return : json
    //Date : 20th April 2018
    //###############################################################
    public function Designerfunction($sellerId)
    {
        // print_r(Auth::user()->id); die;
        $designers = User::with('ratings', 'reviews', 'skills', 'completed_projects')
                    ->with([
                        'services'=> function ($query) use($sellerId) {
                            $query->where('skill_id', $sellerId['skillId']);
                        },
                        'images'=> function ($query) use($sellerId) {
                            $query->where('skill_id', $sellerId['skillId']);
                        }
                    ])
                    ->withCount('reviews', 'ratings')
                    ->where('marketplace_current_role', 1)
                    ->where('status', 1)
                    ->whereHas('services', function($q) use($sellerId){$q->where('skill_id', $sellerId['skillId']);
                    });
        if(array_key_exists('assigned_designer_id', $sellerId)){
            $seller_id = base64_decode($sellerId['assigned_designer_id']);
            $designers = $designers->where('id','!=',$seller_id);
        }
        $designers = $designers->get();
        if ($designers) {
            foreach ($designers as $key => $value) {
                $value['images'] = $value['images']->toArray();
                if(!empty($value['images'])) {
                    foreach ($value['images'] as $keyInner => $valueInner) {
                            $images[] = UtilityController::Imageexist($valueInner['image_name'], public_path('') . UtilityController::Getpath('MP_SERVICES_UPLOAD_PATH'), url('') . UtilityController::Getpath('MP_SERVICES_UPLOAD_URL'), url('') . UtilityController::Getmessage('NO_IMAGE_URL'));
                    }
                } else {
                    $images[] = url('') . UtilityController::Getmessage('NO_IMAGE_URL');
                }
                $designers[$key]['show_images'] = $images;
                $designers[$key]['show_images_count'] = count($images);
                $images = [];
                $designers[$key]['name']  = $value['first_name'] . ' ' . $value['last_name'];
                $designers[$key]['image'] = UtilityController::Imageexist($value['image'], public_path('') . UtilityController::Getmessage('USER_PROFILE_PATH'), url('') . UtilityController::Getmessage('USER_PROFILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));
                if ($value['ratings_count'] > 0) {
                    $array_rating = 0;
                    foreach ($value['ratings'] as $keyInner => $valueInner) {
                        $array_rating += $valueInner['rating'];
                    }
                    $designers[$key]['average_ratings'] = round($array_rating / $value['ratings_count'], 1);
                } else {
                    $designers[$key]['average_ratings'] = 0;
                }

                $completedProjects = 0;
                foreach ($value['completed_projects'] as $keyInner => $valueInner) {
                    if ($valueInner['project_details']['project_status'] == 3) {
                        $completedProjects++;
                    }
                }
                $designers[$key]['projects_completed'] = $completedProjects;

                $skillArray = array();
                foreach ($value['skills'] as $keyInner => $valueInner) {
                    $skillArray[] = $valueInner['skill_set_id'];
                }

                $designers[$key]['skillArray']      = $skillArray;
                $designers[$key]['languages_known'] = json_decode($value['languages_known'], true);
                $languagesArray[]                   = $designers[$key]['languages_known'];
                $designers[$key]['languagesArray']  = $languagesArray;
                $languagesArray                     = array();
            }
            // die;
        }
        return $designers;
    }

    //###############################################################
    //Function Name : Dofilter
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To filter the designers based on user selection
    //In Params : Void
    //Return : json
    //Date : 5th April 2018
    //###############################################################
    public function Dofilter(Request $request)
    {
        try {

            $Input = Input::all();
            
            $filteredDesigner = array();
            if (array_key_exists("skill", $Input)) {
                foreach ($Input['designers'] as $key => $value) {
                    foreach ($value['skillArray'] as $keyInner => $valueInner) {
                        if ($valueInner == $Input['skill']['id']) {
                            $filteredDesigner[] = $value;
                            unset($Input['designers'][$key]);
                        }
                    }
                }
            } else {
                //echo 'm here';
                if (array_key_exists("points", $Input['filter']) && $Input['filter']['points'] != "0") {
                    //$rating     = explode(',', $Input['filter']['points']);
                    $rating = $Input['filter']['points'];
                    //$min_rating = $rating[0];
                    $min_rating = 0;
                    $max_rating = $rating;
                    foreach ($Input['designers'] as $key => $value) {
                        if ($value['average_ratings'] > $min_rating && $value['average_ratings'] <= $max_rating) {
                            $filteredDesigner[] = $value;
                            unset($Input['designers'][$key]);
                        }
                    }
                }
                /*if (array_key_exists("rating", $Input['filter']) && $Input['filter']['rating'] == 1) {
                foreach ($Input['designers'] as $key => $value) {
                if ($value['average_ratings'] >= 4) {
                $filteredDesigner[] = $value;
                unset($Input['designers'][$key]);
                }
                }
                }

                if (array_key_exists("finished_projects", $Input['filter']) && $Input['filter']['finished_projects'] == 1) {
                foreach ($Input['designers'] as $key => $value) {
                if ($value['projects_completed'] >= 10) {
                $filteredDesigner[] = $value;
                unset($Input['designers'][$key]);
                }
                }
                }

                if (array_key_exists("full_time", $Input['filter']) && $Input['filter']['full_time'] == 1) {
                foreach ($Input['designers'] as $key => $value) {
                if ($value['mp_designer_proj_availability'] == 1) {
                $filteredDesigner[] = $value;
                unset($Input['designers'][$key]);
                }
                }
                }

                if (array_key_exists("part_time", $Input['filter']) && $Input['filter']['part_time'] == 1) {
                foreach ($Input['designers'] as $key => $value) {
                if ($value['mp_designer_proj_availability'] == 2) {
                $filteredDesigner[] = $value;
                unset($Input['designers'][$key]);
                }
                }
                }

                if (array_key_exists("hobbyist", $Input['filter']) && $Input['filter']['hobbyist'] == 1) {
                foreach ($Input['designers'] as $key => $value) {
                if ($value['mp_designer_proj_availability'] == 3) {
                $filteredDesigner[] = $value;
                unset($Input['designers'][$key]);
                }
                }
                }

                if (array_key_exists("casual", $Input['filter']) && $Input['filter']['casual'] == 1) {
                foreach ($Input['designers'] as $key => $value) {
                if ($value['mp_designer_proj_availability'] == 4) {
                $filteredDesigner[] = $value;
                unset($Input['designers'][$key]);
                }
                }
                }*/
                if ((array_key_exists("language", $Input['filter']) && $Input['filter']['language'] != '') || (array_key_exists("profeciency", $Input['filter']) && $Input['filter']['profeciency'] != '')) {
                    if (array_key_exists("language", $Input['filter']) && !array_key_exists("profeciency", $Input['filter'])) {
                        foreach ($Input['designers'] as $key => $value) {
                            if ($value['languagesArray'][0] != '0') {
                                foreach ($value['languagesArray'] as $keyInner => $valueInner) {
                                    if ($valueInner['language_known'] == $Input['filter']['language']) {
                                        $filteredDesigner[] = $value;
                                        unset($Input['designers'][$key]);
                                    }
                                }
                            }
                        }
                    } else if (!array_key_exists("language", $Input['filter']) && array_key_exists("profeciency", $Input['filter'])) {
                        foreach ($Input['designers'] as $key => $value) {
                            if ($value['languagesArray'][0] != '0') {
                                foreach ($value['languagesArray'] as $keyInner => $valueInner) {
                                    if ($valueInner['language_proficiency'] == $Input['filter']['profeciency']) {
                                        $filteredDesigner[] = $value;
                                        unset($Input['designers'][$key]);
                                    }
                                }
                            }
                        }
                    } else {
                        foreach ($Input['designers'] as $key => $value) {
                            if ($value['languagesArray'][0] != '0') {
                                foreach ($value['languagesArray'] as $keyInner => $valueInner) {
                                    if ($valueInner['language_known'] == $Input['filter']['language'] && $valueInner['language_proficiency'] == $Input['filter']['profeciency']) {
                                        $filteredDesigner[] = $value;
                                        unset($Input['designers'][$key]);
                                    }
                                }
                            }
                        }
                    }
                }
                if (array_key_exists("min_budget", $Input['filter']) || array_key_exists("max_budget", $Input['filter'])) {
                    if (array_key_exists("min_budget", $Input['filter']) && !array_key_exists("max_budget", $Input['filter'])) {
                        min:
                        foreach ($Input['designers'] as $key => $value) {
                            if ($value['mp_designer_min_price'] >= $Input['filter']['min_budget']) {
                                $filteredDesigner[] = $value;
                                unset($Input['designers'][$key]);
                            }
                        }
                        goto end;
                    } else if (array_key_exists("max_budget", $Input['filter']) && !array_key_exists("min_budget", $Input['filter'])) {
                        max:
                        foreach ($Input['designers'] as $key => $value) {
                            if ($value['mp_designer_max_price'] <= $Input['filter']['max_budget']) {
                                $filteredDesigner[] = $value;
                                unset($Input['designers'][$key]);
                            }
                        }
                        goto end;
                    } else {
                        if ($Input['filter']['min_budget'] == '') {
                            goto max;
                        } else if ($Input['filter']['max_budget'] == '') {
                            goto min;
                        } else {
                            foreach ($Input['designers'] as $key => $value) {
                                if ($value['mp_designer_min_price'] >= $Input['filter']['min_budget'] && $value['mp_designer_max_price'] <= $Input['filter']['max_budget']) {
                                    $filteredDesigner[] = $value;
                                    unset($Input['designers'][$key]);
                                }
                            }
                        }
                        goto end;
                    }
                }
                end:
                if (array_key_exists("sort", $Input['filter']) && $Input['filter']['sort'] != "") {
                    if ($filteredDesigner != []) {
                        $Input['designers'] = $filteredDesigner;
                    }
                    switch ($Input['filter']['sort']) {
                        case 1:
                            usort($Input['designers'], function ($a, $b) {
                                return strcasecmp($a['first_name'], $b['first_name']);
                            });
                            break;
                        case 2:
                            usort($Input['designers'], function ($a, $b) {
                                return strcasecmp($b['first_name'], $a['first_name']);
                            });
                            break;
                        case 3:
                            usort($Input['designers'], function ($a, $b) {
                                // return $b['mp_designer_min_price'] <=> $a['mp_designer_min_price'];
                            });
                            break;
                        case 4:
                            usort($Input['designers'], function ($a, $b) {
                                // return $a['mp_designer_min_price'] <=> $b['mp_designer_min_price'];
                            });
                            break;
                        case 5:
                            usort($Input['designers'], function ($a, $b) {
                                // return $b['average_ratings'] <=> $a['average_ratings'];
                            });
                            break;
                        case 6:
                            usort($Input['designers'], function ($a, $b) {
                                // return $a['average_ratings'] <=> $b['average_ratings'];
                            });
                            break;
                    }
                    $filteredDesigner = $Input['designers'];
                }
            }
            //echo("<pre>");print_r($filteredDesigner);echo("</pre>");
            $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $filteredDesigner);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnMessage=$Input['language']=='en'?'GENERAL_ERROR':'GENERAL_ERROR_CHINESE';
            $responseArray     = UtilityController::Generateresponse(0,$returnMessage, Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Designersuggestions
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To get the designers suitable for project
    //In Params : Void
    //Return : json
    //Date : 29th March 2018
    //###############################################################
    public function Designersuggestions(Request $request)
    {
        try {
            $Input = Input::all();
            $data  = self::Suggestionfunction($Input);

            if ($data != 'error') {
                $responseArray = UtilityController::Generateresponse(true, $Input['mp_project_id'], 1, $data);
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
            }

        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, 'GENERAL_ERROR', '', '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Suggestionfunction
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To get the designers suitable for project common function for methods Designersuggestions and Updatebudgettimeline
    //In Params : Void
    //Return : json
    //Date : 26th March 2018
    //###############################################################
    public function Suggestionfunction($Input)
    {
        try {
            $mp_project_id      = substr($Input['mp_project_id'], strpos($Input['mp_project_id'], ":") + 1);
            $mp_project_id      = base64_decode($mp_project_id);
            $projectInfo        = MarketPlaceProject::where('id', $mp_project_id)->first();
            //$suggestedDesigners = User::with('ratings', 'reviews','services')->where('marketplace_current_role', 1)->limit(5);
            
            if(isset($Input['assigned_designer_id']))
                $assigned_designer_id = base64_decode($Input['assigned_designer_id']);
            else
                $assigned_designer_id = 0;

            ##suggestion algo start##
            $suggestedDesigners = SuggestionalgoController::Suggestionfunction($mp_project_id, $assigned_designer_id);
            ##suggestion algo end##

            /*if (isset($Input['assigned_designer_id'])) {
                $assigned_designer_id = base64_decode($Input['assigned_designer_id']);
                $suggestedDesigners   = $suggestedDesigners->where('id', '!=', $assigned_designer_id);
            }
            $suggestedDesigners         = $suggestedDesigners->get();*/
            

            $data['projectInfo']        = $projectInfo;
            $data['suggestedDesigners'] = $suggestedDesigners;
            if ($suggestedDesigners && $projectInfo) {
               
                foreach ($suggestedDesigners as $key => $value) {
                    if(count($value['services'])>0){
                        foreach ($value['services'] as $keyInner => $valueInner) {
                            foreach ($valueInner['service_details'] as $keyIn => $valueIn) {
                                foreach ($valueIn['service_images'] as $keyService => $valueService) {
                                    $images[] = UtilityController::Imageexist($valueService['image_name'], public_path('') . UtilityController::Getpath('MP_SERVICES_UPLOAD_PATH'), url('') . UtilityController::Getpath('MP_SERVICES_UPLOAD_URL'), url('') . UtilityController::Getmessage('NO_IMAGE_URL'));
                                }
                            }
                        }
                    } else {
                        $images[] = url('') . UtilityController::Getmessage('NO_IMAGE_URL');
                    }
                    
                    $suggestedDesigners[$key]['show_images'] = $images;
                    $suggestedDesigners[$key]['show_images_count'] = count($images);
                    $images = [];

                    $value['image'] = UtilityController::Imageexist($value['image'], public_path('') . UtilityController::Getpath('USER_PROFILE_PATH'), url('') . UtilityController::Getpath('USER_PROFILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));
                    if (count($value['ratings']) > 0) {
                        $array_rating = 0;
                        foreach ($value['ratings'] as $keyInner => $valueInner) {
                            $array_rating += $valueInner['rating'];
                        }
                        $suggestedDesigners[$key]['average_ratings'] = (isset($array_rating) ? round($array_rating / count($value['ratings']), 1) : 0);
                    } else {
                        $suggestedDesigners[$key]['average_ratings'] = 0;
                    }
                    $suggestedDesigners[$key]['total_reviews'] = $value['reviews']->count();
                }
            } else {
                $data = 'error';
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, 'GENERAL_ERROR', Response::HTTP_BAD_REQUEST, '');
        }
        return $data;
    }
    //###############################################################
    //Function Name : Updatebudgettimeline
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To update the budget or timeline for the project
    //In Params : Void
    //Return : json
    //Date : 26th March 2018
    //###############################################################
    public function Updatebudgettimeline(Request $request)
    {
        try {
            \DB::beginTransaction();
            $Input         = Input::all();
            $mp_project_id = substr($Input['mp_project_id'], strpos($Input['mp_project_id'], ":") + 1);
            $mp_project_id = base64_decode($mp_project_id);
            $result        = UtilityController::Makemodelobject($Input, 'MarketPlaceProject', '', $mp_project_id);
            if ($result) {
                \DB::commit();
                $data          = self::Suggestionfunction($Input);
                $responseArray = UtilityController::Generateresponse(true, $Input['mp_project_id'], 1, $data);
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, 'GENERAL_ERROR', '', '');
        }
        return response()->json($responseArray);
    }
    //###############################################################
    //Function Name : Asssigndesigner
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To assign the project to the designer selected by the client for their project
    //In Params : Void
    //Return : json
    //Date : 26th March 2018
    //###############################################################
    public function Asssigndesigner(Request $request)
    {
        try {
            $Input = Input::all();

            \DB::beginTransaction();
            if(array_key_exists('only_change_description', $Input)){
                $Input['mp_project_id'] = $Input['slug'];
                $Input['projectInfo'] = MarketPlaceProject::where('slug',$Input['mp_project_id'])->first();
                $Input['projectInfo'] = json_decode(json_encode($Input['projectInfo']),true);
                $mp_project_id = substr($Input['mp_project_id'], strpos($Input['mp_project_id'], ":") + 1);
                $mp_project_id = base64_decode($mp_project_id);
                $result = UtilityController::Makemodelobject($Input,'MarketPlaceProject','',$mp_project_id);
                $Input['designer'] = User::where('slug',$Input['user_id'])->first();
                $Input['designer'] = json_decode(json_encode($Input['designer']),true);
            }
            $ifExistSeller = MarketPlaceProjectsDetailInfo::where('mp_project_id',$mp_project_id)->where('status',4)->count();
            if($ifExistSeller==0){
                $data['mp_project_id']        = $mp_project_id;
                $data['assigned_designer_id'] = $Input['designer']['id'];

                $result = UtilityController::Makemodelobject($data, 'MarketPlaceProjectsDetailInfo');
                if ($result) {
                    $projectDetails                        = MarketPlaceProject::with('selected_designer')->where('id',$data['mp_project_id'])->first();
                    /*$notification['sender_id']             = $projectDetails['project_created_by_id'];
                    $notification['receiver_id']           = $projectDetails['selected_designer']['assigned_designer_id'];
                    $notification['notification_text']     = 'New project invitation "'.$projectDetails['project_title'].'"';
                    $notification['chi_notification_text'] = '新的项目邀请 "'.$projectDetails['project_title'].'"';
                    $notification['module']                = 1;
                    $notification['project_slug']          = $projectDetails['slug'];
                    $notificationUpdate                    = UtilityController::Makemodelobject($notification, 'SystemNotification');*/
                    $messsage_text='New project invitation "'.$projectDetails['project_title'].'"';
                    $messsage_text_chi='新的项目邀请 "'.$projectDetails['project_title'].'"';
                    self::Insertsystemnotifications($projectDetails['project_created_by_id'],$projectDetails['selected_designer']['assigned_designer_id'],$messsage_text,$messsage_text_chi,$projectDetails['slug']);

                    $Input['subject']=$Input['designer']['current_language']==1?'Project Assigned':'项目已分配';

                    $Input['content']=$Input['designer']['current_language']==1?'
                        Congratulations! You have been selected for a Project.<br/><br/>Click <a href="'.URL::to('/seller-project-info-invitation/'.$Input['projectInfo']['slug']).'">here</a> to review the Project brief. You can liaise with the Buyer on the Project Page.
                        <br><br>
                        Remember, you have 24 hours to accept this challenge!
                        <br><br>
                        Grab this opportunity to showcase your talents!':'

                        恭喜您！您已被挑选负责一个项目。<br><br>请点击 <a href="'.URL::to('/seller-project-info-invitation/'.$Input['projectInfo']['slug']).'">此处</a> 查看项目简介。<br><br>温馨提示，您有24小时来接受这份挑战！
                        <br><br>赶紧抓住这次机会来展示您的才能！';

                    $Input['footer_content']=$Input['designer']['current_language']==1?'From,<br /> SixClouds Sixteen':'六云十六 ';

                    $Input['footer']=$Input['designer']['current_language']==1?'SixClouds':'六云';
                    Mail::send('emails.email_template', $Input, function ($message) use ($Input) {
                        $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                        $message->to($Input['designer']['email_address'])->subject($Input['subject']);

                    });
                    
                    $messsage_text="You have new invitation for project ".$Input['projectInfo']['project_title'];
                    $messsage_text_chi=" 您有新的项目邀请 ".$Input['projectInfo']['project_title'];
                    self::Insertmessageboardnotifications($projectDetails['selected_designer']['assigned_designer_id'],$mp_project_id,$messsage_text,$messsage_text_chi,4,1,2);

                    \DB::commit();
                    $notifyDesigner = (new MpInvitationNotification($result['id'], 12))->delay(Carbon::now()->addHours(12));
                    dispatch($notifyDesigner);
                    $notifyDesigner = (new MpInvitationNotification($result['id'], 18))->delay(Carbon::now()->addHours(18));
                    dispatch($notifyDesigner);
                    $notifybuyer = (new MpInvitationNotAccepted($result['id']))->delay(Carbon::now()->addHours(24));
                    dispatch($notifybuyer);
                    $returnMessage = ($Input['language']=='en'?'DESIGNER_SELECTED':'DESIGNER_SELECTED_CHINESE');
                    $responseArray = UtilityController::Generateresponse(true, $returnMessage, 1);
                    self::Insertadmintimelinenotifications($Input['designer']['id'],$mp_project_id,' accepted request at ',' 接受请求 ',9,0);
                } else {
                    $responseArray = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
                }
            } else {
                $returnMessage = ($Input['language']=='en'?'SELLER_EXIST_FOR_PROJECT':'SELLER_EXIST_FOR_PROJECT_CHINESE');
                throw new \Exception(UtilityController::Getmessage($returnMessage));
            }
            
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, $e->getMessage(), 0);
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Buyerprojects
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To get active projects for buyer
    //In Params : Void
    //Return : json
    //Date : 3rd Apr 2018
    //###############################################################
    public function Buyerprojects(Request $request)
    {
        try {
            if (Auth::check()) {
                $Input = Input::all();
                
                $buyerId = Auth::user()->id;
                if ($Input['whichPage'] == "buyer-past-project") {
                    $activeProjects = MarketPlaceProject::with('selected_designer', 'rating_for_project', 'review_for_project', 'projectCreator', 'extension_details', 'skill_detail')->withCount('message_board')->where('project_status', 3)->where('project_created_by_id', $buyerId)->orderby('id', 'DESC');
                }

                if ($Input['whichPage'] == "buyer-dashboard") {
                    $activeProjects = MarketPlaceProject::with('selected_designer', 'rating_for_project', 'review_for_project', 'projectCreator', 'extension_details', 'seller_payment', 'priority_actioins', 'skill_detail')->where(function ($query) {$query->where('project_status', 1)->orWhere('project_status', 2)->orWhere('project_status', 7)->orWhere('project_status', 8);})->withCount('message_board')->where('project_created_by_id', $buyerId)->orderby('show_first', 'DESC')->orderby('id', 'DESC');
                }
                $activeProjects = $activeProjects->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))->toArray();

                if ($activeProjects) {

                    foreach ($activeProjects['data'] as $key => $value) {

                        $activeProjects['data'][$key]['image'] = UtilityController::Imageexist($value['project_creator']['image'], public_path('') . UtilityController::Getmessage('ACCESSOR_IMAGE_PATH'), url('') . UtilityController::Getmessage('ACCESSOR_IMAGE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));

                        $date = Carbon::parse($value['created_at'])->timezone(Auth::user()->timezone);
                        $activeProjects['data'][$key]['project_created_at'] = UtilityController::Changedateformat($date, 'd F, Y');

                        // $projectAt = Carbon::parse($value['created_at']);
                        $diffDays  = $date->diffInDays(Carbon::now());

                        $activeProjects['data'][$key]['days_remaining'] = $value['project_timeline'] - $diffDays;

                        if($activeProjects['data'][$key]['days_remaining']>=0){
                            $projectAt                                      = Carbon::parse($value['created_at'])->addDays($value['project_timeline']);
                            $activeProjects['data'][$key]['deadline']       = $projectAt->toDateTimeString(
                            );   
                        }
                        else{
                            $activeProjects['data'][$key]['deadline'] = '00 hrs 00 mins 00 secs';
                        }
                       
                        /*if ($activeProjects['data'][$key]['days_remaining'] < 0) {
                            $activeProjects['data'][$key]['days_remaining'] = 0;
                        }*/

                        if ($value['selected_designer']['invitation_accepted_at'] != '') {
                            $date = Carbon::parse($value['selected_designer']['invitation_accepted_at'])->timezone(Auth::user()->timezone);
                            $activeProjects['data'][$key]['accepted_at'] = UtilityController::Changedateformat($date, 'd F, Y');
                        } else {
                            $activeProjects['data'][$key]['accepted_at'] = 'Pending';
                        }

                        if ($value['selected_designer']['project_completed_at'] != '') {
                            $date = Carbon::parse($value['selected_designer']['project_completed_at'])->timezone(Auth::user()->timezone);
                            $activeProjects['data'][$key]['completed_at'] = UtilityController::Changedateformat($date, 'd F, Y');
                        } else {
                            $activeProjects['data'][$key]['completed_at'] = 'Pending';
                        }

                        if ($value['selected_designer']['project_completed_at'] != '') {
                            $date = Carbon::parse($value['created_at'])->timezone(Auth::user()->timezone);
                            $activeProjects['data'][$key]['created_at'] = UtilityController::Changedateformat($date, 'd F, Y');
                        } else {
                            $activeProjects['data'][$key]['completed_at'] = 'Pending';
                        }

                        
                        if ($value['selected_designer'] != '') {
                            $activeProjects['data'][$key]['selected_designer']['designer_details']['image'] = UtilityController::Imageexist($value['selected_designer']['designer_details']['image'], public_path('') . UtilityController::Getmessage('USER_PROFILE_PATH'), url('') . UtilityController::Getmessage('USER_PROFILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));
                            $activeProjects['data'][$key]['selected_designer']['encrypted_assigned_designer_id'] = base64_encode($value['selected_designer']['assigned_designer_id']);
                            if ($value['rating_for_project'] != '' && $value['review_for_project'] != '') {
                                $activeProjects['data'][$key]['review_done'] = true;
                            } else {
                                $activeProjects['data'][$key]['review_done'] = false;
                            }

                        }
                        if ($value['rating_for_project']['rating'] != 0 && $value['rating_for_project']['rating'] != '') {
                            $ratingMatrix = json_decode($value['rating_for_project']['matrix_rating'], true);

                            $activeProjects['data'][$key]['ratingMatrix'] = $ratingMatrix;
                        }
                        $tempCategory = [];
                        if (!empty($value['skill_detail'])) {
                            foreach ($value['skill_detail'] as $keyInner => $valueInner) {

                                $tempSkills     = $valueInner['skill_detail'];
                                $tempCategory[] = $tempSkills['skill_name'];
                            }
                        }

                        $activeProjects['data'][$key]['category_list'] = implode(',', $tempCategory);

                    }
                    $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $activeProjects);
                } else {
                    $returnMessage=$Input['language']=='en'?'GENERAL_ERROR':'GENERAL_ERROR_CHINESE';
                    $responseArray = UtilityController::Generateresponse(false, $returnMessage, 0);
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnMessage=$Input['language']=='en'?'GENERAL_ERROR':'GENERAL_ERROR_CHINESE';
            $responseArray     = UtilityController::Generateresponse(0, $returnMessage, Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Projectinfo
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To get active projects details
    //In Params : Void
    //Return : json
    //Date : 9th Apr 2018
    //###############################################################
    public function Projectinfo(Request $request)
    {
        try {
            if (Auth::check()) {
                $Input         = Input::all();
                $buyerId       = Auth::user()->id;
                $mp_project_id = substr($Input['mp_project_id'], strpos($Input['mp_project_id'], ":") + 1);
                $mp_project_id = base64_decode($mp_project_id);
                $projectInfo   = MarketPlaceProject::with('selected_designer', 'message_board', 'projectinvitation')->where('id', $mp_project_id)->first();

                if ($projectInfo) {

                    $projectInfo['project_created_at'] = $projectInfo['created_at']->format('d F, Y');
                    /* To be replaced when using live code with carbon library
                    $projectInfo['invitation_sent_on']     = $projectInfo['selected_designer']['created_at']->format('d F, Y h:i A');
                    $projectInfo['invitation_accepted_on'] = $projectInfo['selected_designer']['invitation_accepted_at']->format('d F, Y h:i A'); */
                    $projectAt                     = Carbon::parse($projectInfo['created_at']);
                    $diffDays                      = $projectAt->diffInDays(Carbon::now());
                    $projectInfo['days_remaining'] = $projectInfo['project_timeline'] - $diffDays;
                    $projectAt                     = Carbon::parse($projectInfo['created_at'])->addDays($projectInfo['project_timeline']);
                    $projectInfo['deadline']       = $projectAt->toDateTimeString();

                    $projectInfo['image'] = UtilityController::Imageexist($projectInfo['selected_designer']['designer_details']['image'], public_path('') . UtilityController::Getmessage('ACCESSOR_IMAGE_PATH'), url('') . UtilityController::Getmessage('ACCESSOR_IMAGE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));

                    $date = Carbon::parse($projectInfo['selected_designer']['created_at'])->timezone(Auth::user()->timezone);
                    $projectInfo['invitation_sent_on'] = UtilityController::Changedateformat($date, 'd F, Y h:i A');

                    if ($projectInfo['selected_designer']['invitation_accepted_at'] != '') {
                        $date = Carbon::parse($projectInfo['selected_designer']['invitation_accepted_at'])->timezone(Auth::user()->timezone);
                        $projectInfo['invitation_accepted_on'] = UtilityController::Changedateformat($date, 'd F, Y h:i A');
                    } else {
                        $projectInfo['invitation_accepted_on'] = "Pending";
                    }

                    if ($projectInfo['selected_designer']['project_completed_at'] != '') {
                        $date = Carbon::parse($projectInfo['selected_designer']['project_completed_at'])->timezone(Auth::user()->timezone);
                        $projectInfo['project_completed_on'] = UtilityController::Changedateformat($date, 'd F, Y h:i A');
                    } else {
                        $projectInfo['project_completed_on'] = "Pending";
                    }
                    $projects = $projectInfo->toArray();
                    foreach ($projectInfo['message_board'] as $key => $value) {


                            $value['image'] = UtilityController::Imageexist($value['user_details']['image'], public_path('') . UtilityController::Getmessage('ACCESSOR_IMAGE_PATH'), url('') . UtilityController::Getmessage('ACCESSOR_IMAGE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));

                            $date = Carbon::parse($value['created_at'])->timezone(Auth::user()->timezone);
                            $value['message_on'] = UtilityController::Changedateformat($date, 'd F, Y h:i A');

                            $value['side_class'] = ($value['user_role'] == 1 ? 'left' : 'right');

                                if ($value['type'] == 2) {
                                    $projectInfo['message_board'][$key]['placeholder'] = UtilityController::Imageexist('tiny_' . $value['message'], public_path('') . UtilityController::Getmessage('MSG_FILE_PATH'), url('') . UtilityController::Getmessage('MSG_FILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));
                                    $projectInfo['message_board'][$key]['
                                    '] = UtilityController::Imageexist($value['message'], public_path('') . UtilityController::Getmessage('MSG_FILE_PATH'), url('') . UtilityController::Getmessage('MSG_FILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));

                                    $projectInfo['message_board'][$key]['message'] = public_path('') . UtilityController::Getmessage('MSG_FILE_PATH') . $value['message'];
                                }
                                if ($value['type'] == 3) {
                                    $projectInfo['message_board'][$key]['placeholder'] = url('/') . UtilityController::Getmessage('DEFAULT_FILE_URL');
                                    $projectInfo['message_board'][$key]['message']     = public_path('') . UtilityController::Getmessage('MSG_FILE_PATH') . $value['message'];
                                    $projectInfo['message_board'][$key]['download_placeholder'] = $projectInfo['message_board'][$key]['message'];
                                }

                                if($value['type'] == 4) {
                                $date = Carbon::parse($value['created_at'])->timezone(Auth::user()->timezone);
                                $projects['message_board'][$key]['message']=$value['message'].' at '.UtilityController::Changedateformat($date, 'd F, Y');
                            }


                            if($projectInfo['message_board'][$key]['user_role']==3){
                                $value['side_class'] = '';
                                $projectInfo['message_board'][$key]['name']='ADMIN';

                                if($projectInfo['message_board'][$key]['user_id']==0 || $projectInfo['message_board'][$key]['user_id']==Auth::user()->id){
                                }
                                else{
                                    unset($projectInfo['message_board'][$key]);
                                }
                            }else{
                                $projectInfo['message_board'][$key]['name'] = ($value['user_role'] == 1 ? $projects['selected_designer']['designer_details']['first_name'] . ' ' . $projects['selected_designer']['designer_details']['last_name'] : Auth::user()->first_name . ' ' . Auth::user()->last_name);
                            }
                            //$projectInfo['message_board']=array_values($projectInfo['message_board']);
                        

                    }
                    $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $projectInfo);
                } else {
                    $returnMessage=$Input['language']=='en'?'GENERAL_ERROR':'GENERAL_ERROR_CHINESE';
                    $responseArray = UtilityController::Generateresponse(false, $returnMessage, 0);
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnMessage=$Input['language']=='en'?'GENERAL_ERROR':'GENERAL_ERROR_CHINESE';
            $responseArray     = UtilityController::Generateresponse(0, $returnMessage, Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Sendmessage
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To store and send the message
    //In Params : Void
    //Return : json
    //Date : 9th Apr 2018
    //###############################################################
    public function Sendmessage(Request $request)
    {
        try {
            \DB::beginTransaction();
            $Input     = Input::all();
            
            $validator = UtilityController::ValidationRules($Input, 'MarketPlaceMessageBoard');
            if (!$validator['status']) {
                $errorMessage  = explode('.', $validator['message']);
                $responseArray = UtilityController::Generateresponse(false, $errorMessage['0'], 0);
            } else {
                if((array_key_exists('message', $Input)&& $Input['message']!='') || !empty($Input['files'])){

                    if($Input['user_role'] == 2)
                        $Input['message_status_buyer'] = 1;
                    elseif($Input['user_role'] == 1)
                        $Input['message_status_seller'] = 1;

                    $result                    = UtilityController::Makemodelobject($Input, 'MarketPlaceMessageBoard');

                    $Input['receiver_id']       = $Input['receiver_id'];
                    $Input['receiver']         = User::where('id', $Input['receiver_id'])->first();
                    $Input['receiver']         = json_decode(json_encode($Input['receiver']), true);
                    $Input['project_detail']   = MarketPlaceProject::where('id', $Input['mp_project_id'])->first();
                    $Input['project_detail']   = json_decode(json_encode($Input['project_detail']), true);
                    if(array_key_exists('message', $Input))
                        $Input['message_text']     = $Input['message'];
                    $Input['file_count']       = count($Input['files']);
                    $firstMsgId                = $result->id;
                    $arrayUpdate['group_with'] = $firstMsgId;
                    UtilityController::Makemodelobject($arrayUpdate, 'MarketPlaceMessageBoard', 'id', $firstMsgId);
                    if (!empty($Input['files'])) {
                        foreach ($Input['files'] as $key => $value) {
                            $temp                  = array();
                            $temp['user_id']       = $Input['user_id'];
                            $temp['mp_project_id'] = $Input['mp_project_id'];
                            $temp['type']          = $value['type'];
                            $temp['user_role']     = $Input['user_role'];
                            $temp['message']       = $value['message'];
                            $temp['group_with']    = $firstMsgId;
                            $temp['receiver_id']       = $Input['receiver_id'];

                            if($Input['user_role'] == 2)
                                $temp['message_status_buyer'] = 1;
                            elseif($Input['user_role'] == 1)
                                $temp['message_status_seller'] = 1;

                            $result                = UtilityController::Makemodelobject($temp, 'MarketPlaceMessageBoard');
                        }
                    }
                    if ($result) {
                        
                        $project_title                         = MarketPlaceProject::where('id',$Input['mp_project_id'])->first();
                        $notification['sender_id']             = $Input['user_id'];
                        $notification['receiver_id']           = $Input['receiver_id'];
                        $notification['user_role']             = $Input['user_role'];
                        $notification['notification_text']     = 'New message in "'.$project_title['project_title'].'"';
                        $notification['chi_notification_text'] = '新消息"'.$project_title['project_title'].'"';
                        $notification['module']                = 1;
                        $notification['project_slug']          = $project_title['slug'];
                    
                        self::Insertsystemnotifications($Input['user_id'],$Input['receiver_id'],'New message in "'.$project_title['project_title'].'"','新消息"'.$project_title['project_title'].'"',$project_title['slug']);

                        $user = User::findOrFail($Input['receiver_id']);
                        $job  = (new MpMessageBoardMail($user, $notification, $Input['mp_project_id']))->delay(Carbon::now()->addMinutes(15));
                        dispatch($job);

                        $data['mp_project_id'] = $Input['mp_project_id'];
                        $data['action_type']   = 4;
                        $data['action_text']   = UtilityController::Getmessage('NEW_MESSAGE_ARRIVED');
                        $data['show_first']    = 1;
                        $actionResult          = UtilityController::Makemodelobject($data, 'MarketplaceAction');
                        $actionResult          = UtilityController::Makemodelobject($data, 'MarketPlaceProject', '', $data['mp_project_id']);

                        \DB::commit();
                        $returnMessage=$Input['language']='en'? 'MESSAGE_SENT': 'MESSAGE_SENT_CHINESE';
                        $responseArray = UtilityController::Generateresponse(true, $returnMessage, 1);
                    } else {
                        $responseArray = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
                    }
                } else {
                    $returnMessage==$Input['language']='en'? 'SELECT_IMAGE_OR_TYPE_MESSAGE': 'SELECT_IMAGE_OR_TYPE_MESSAGE_CHINESE';
                    throw new \Exception(UtilityController::Getmessage($returnMessage));
                }
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), '', '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Actions
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To store the cancelation reason
    //In Params : Void
    //Return : json
    //Date : 10th Apr 2018
    //###############################################################
    public function Actions(Request $request)
    {
        try {
            \DB::beginTransaction();
            $Input = Input::all();
            if ($Input['modalTitle'] == "Flag Seller" || $Input['modalTitle'] == "举报卖家") {
               
                $validator = UtilityController::ValidationRules($Input, 'MarketPlaceReportDispute');
                if (!$validator['status']) {
                    $errorMessage  = explode('.', $validator['message']);
                    $responseArray = UtilityController::Generateresponse(false, $errorMessage['0'], 0);
                } else {
                    $result = UtilityController::Makemodelobject($Input, 'MarketPlaceReportDispute');
                    if ($result) {
                        Mail::send('emails.notify_admin_for_dispute', $Input, function ($message) use ($Input) {
                            $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                            $message->to('yongsan@sixclouds.cn')->subject('Regarding: Dispute');
                        });
                        $returnMessage=($Input['language']=='en')?'DESIGNER_FLAGGED':'DESIGNER_FLAGGED_CHINESE';
                        $responseArray = UtilityController::Generateresponse(true, $returnMessage, 1);
                    } else {
                        $responseArray = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
                    }
                }
            }
            if ($Input['modalTitle'] == "Cancel Project") {
                $Input['project_status']              = 8;
                $Input['project_cancellation_reason'] = $Input['reason'];
                $Input['project_cancel_request_at']   = Carbon::now();

                $result = UtilityController::Makemodelobject($Input, 'MarketPlaceProject', '', $Input['mp_project_id']);

                $result = UtilityController::Makemodelobject($Input, 'MarketPlaceProject', '', $Input['mp_project_id']);

                $result = MarketPlaceProjectsDetailInfo::where('mp_project_id', $Input['mp_project_id'])->update(array('status' => 11));

                if ($result) {
                    $projectDetails                        = MarketPlaceProject::with('selected_designer')->where('id',$Input['mp_project_id'])->first();
                    $notification['sender_id']             = $projectDetails['project_created_by_id'];
                    $notification['receiver_id']           = $projectDetails['selected_designer']['assigned_designer_id'];
                    $notification['notification_text']     = 'Buyer requested cancellation for project "'.$projectDetails['project_title'].'"';
                    $notification['chi_notification_text'] = '买方要求取消项目"'.$projectDetails['project_title'].'"';
                    $notification['module']                = 1;
                    $notification['project_slug']          = $projectDetails['slug'];
                    $notificationUpdate                    = UtilityController::Makemodelobject($notification, 'SystemNotification');

                    self::Insertadmintimelinenotifications(Auth::user()->id,$Input['mp_project_id'],'Buyer request for cancellation of project at ',' 买方要求取消项目 ',7,0);
                    

                    
                    $responseArray = UtilityController::Generateresponse(true, 'PROJECT_CANCEL_REQUEST_BY_BUYRE', 1);

                    $data['mp_project_id']  = $Input['mp_project_id'];
                    $data['action_type']    = 4;
                    $data['action_text']    = UtilityController::Getmessage('BUYER_REQUESTED_CANCELLATION_SHOW_BUYER');
                    $data['show_first']     = 1;
                    $actionResult           = UtilityController::Makemodelobject($data, 'MarketplaceAction');
                    $actionResult           = UtilityController::Makemodelobject($data, 'MarketPlaceProject', '', $Input['mp_project_id']);

                } else {
                    $responseArray = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
                }
            }
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), '', '');
        }
        return response()->json($responseArray);
    }

    // designer
    //###############################################################
    //Function Name : Designerprojectsinvitation
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get project invitations for designers
    //In Params : Void
    //Return : json
    //Date : 21th March 2018
    //###############################################################
    public function Designerprojectsinvitation(Request $request)
    {
        try {
            \DB::beginTransaction();
            $projectsQuery = MarketPlaceProject::whereHas('projectinvitation', function ($query) {
                $query->where('assigned_designer_id', Auth::user()->id)->where('status', 4);
            })->with([
                'projectCreator' => function ($query) {
                    $query->get();
                },
                'skills'         => function ($query) {
                    $query->with('skillDetail')->get();
                },
            ]);
            $projects = $projectsQuery
                ->orderby('id', 'DESC')
                ->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))
                ->toArray();

            if (!empty($projects)) {
                foreach ($projects['data'] as $key => $value) {
                    $projectdate = new DateTime($value['created_at']);

                    $date = Carbon::parse($value['created_at'])->timezone(Auth::user()->timezone);
                    $projects['data'][$key]['project_date'] = UtilityController::Changedateformat($date, 'd F, Y');
                    
                    $projectAt = Carbon::parse($value['created_at'])->addDays(1);

                    $projects['data'][$key]['current_time']   = Carbon::now()->toDateTimeString();
                    $projects['data'][$key]['days_remaining'] = $projectAt->toDateTimeString();

                    $temp = $value;

                    $projects['data'][$key]['project_creator']['image'] = UtilityController::Imageexist($value['project_creator']['image'], public_path('') . UtilityController::Getmessage('USER_PROFILE_PATH'), url('') . UtilityController::Getmessage('USER_PROFILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));

                    $tempCategory = [];
                    if (!empty($temp['skills'])) {
                        foreach ($temp['skills'] as $keyInner => $valueInner) {
                            $tempSkills     = $valueInner['skill_detail'];
                            $tempCategory[] = $tempSkills['skill_name'];
                        }
                    }

                    $projects['data'][$key]['category_list'] = implode(',', $tempCategory);
                }
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $projects);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            \DB::commit();
            return $returnData;
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    // designer
    //###############################################################
    //Function Name : Designergalleryuploads
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get my gallery's items listing uploaded by designer
    //In Params : Void
    //Return : json
    //Date : 9th April 2018
    //###############################################################
    public function Designergalleryuploads(Request $request)
    {
        try {
            \DB::beginTransaction();
            $projectsQuery = MarketPlaceUpload::withCount('gallerypurchases')->where('upload_of', 2)->where('uploaded_user_id', Auth::user()->id)->where('status', 1);
            $projects      = $projectsQuery
                ->orderby('id', 'DESC')
                ->paginate(Config('constants.other.MP_MY_UPLOADS'))
                ->toArray();
            if (!empty($projects)) {
                foreach ($projects['data'] as $key => $value) {
                    $projectdate                            = new DateTime($value['created_at']);
                    $projects['data'][$key]['project_date'] = $projectdate->format('M d Y');
                }
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $projects);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            \DB::commit();
            return $returnData;
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    // designer
    //###############################################################
    //Function Name : Designerinspirationbankuploads
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get my inspiration bank's items listing uploaded by designer
    //In Params : Void
    //Return : json
    //Date : 9th April 2018
    //###############################################################
    public function Designerinspirationbankuploads(Request $request)
    {
        try {
            \DB::beginTransaction();
            $Input = Input::all();
            $projectsQuery = MarketPlaceUpload::where('upload_of', 1)->where('uploaded_user_id', Auth::user()->id)->where('status', 1);
            if(array_key_exists('searchBy', $Input)&& $Input['searchBy']!='undefined'){
                $projectsQuery = $projectsQuery->where('upload_title', 'like', '%' . $Input['searchBy'] . '%');
            }
            $projects = $projectsQuery->orderby('id', 'DESC')->paginate(Config('constants.other.MP_MY_UPLOADS'));
            $projects = json_decode(json_encode($projects),true);
            if (!empty($projects)) {
                foreach ($projects['data'] as $key => $value) {
                    $projectdate                            = new DateTime($value['created_at']);
                    $projects['data'][$key]['project_date'] = $projectdate->format('M d Y');
                }
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $projects);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            \DB::commit();
            return $returnData;
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    // designer
    //###############################################################
    //Function Name : Designermyuploadscount
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get my upload's count
    //In Params : Void
    //Return : json
    //Date : 9th April 2018
    //###############################################################
    public function Designermyuploadscount(Request $request)
    {
        try {
            \DB::beginTransaction();
            $projects['portfolio_count']   = MarketPlaceUpload::where('upload_of', 3)->where('uploaded_user_id', Auth::user()->id)->where('status', 1)->count();
            $projects['gallery_category']  = MarketPlaceGalleryCategory::where('status', 1)->get();
            $projects['ins_bank_category'] = MarketPlaceInsBankCategory::where('status', 1)->get();
            $returnData                    = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $projects);
            \DB::commit();
            return $returnData;
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
    // designer
    //###############################################################
    //Function Name : Designerinsbankupload
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To upload insperation bank
    //In Params : Void
    //Return : json
    //Date : 10th April 2018
    //###############################################################
    public function Designerinsbankupload(Request $request)
    {
        try {                        
            \DB::beginTransaction();
            $input = Input::all();
            $validator  = UtilityController::ValidationRules($request->all(), 'MarketPlaceUpload', array('uploaded_user_id','upload_description', 'price', 'upload_of'));
            $returnData = $validator;
            if (!$validator['status']) {
                $errorMessage = explode('.', $validator['message']);
                $returnData   = UtilityController::Generateresponse(false, $errorMessage['0'], 0, '');
            } else {
                if(!empty($input['remove_attachments'])){
                    $removeFiles = explode(',', $input['remove_attachments']);
                    foreach ($input['file_name'] as $keyInner => $valueInner) {
                        foreach ($removeFiles as $key => $value) {
                            if ($value == $valueInner->getClientOriginalName()) {
                                unset($input['file_name'][$keyInner]);
                                unset($removeFiles[$key]);
                            }
                        }
                    }    
                }                
                if(!empty($input['remove_cover_files'])){
                    $removeFiles = explode(',', $input['remove_cover_files']);
                    foreach ($removeFiles as $key => $value) {
                        // var_dump($value == $input['cover_image']->getClientOriginalName());exit;
                         if ($value == $input['cover_image']->getClientOriginalName()) {
                                unset($input['cover_image']);
                            }
                    }    
                }
             
                if (array_key_exists('cover_image', $input)) {
                    
                    $rules = array(
                         //'file_name.*' => 'required |' . $categoryDetail['validation'],
                    );
                    $Validate = Validator::make(Input::all(), $rules);

                    if ($Validate->fails()) {
                        $returnData = UtilityController::Generateresponse(false, $Validate->messages()->first(), '', '');
                    } else {
                        if (isset($input['file_name'])) {
                            foreach ($input['file_name'] as $key => $value) {
                                $sum = 0;

                                foreach ($input['file_name'] as $key => $value) {
                                    $sum = $sum + $value->getClientSize();
                                    if ($sum > UtilityController::Getmessage('MAX_UPLOAD_FILE_SIZE')) {
                                        $returnMessage=$input['language']=='en'?'ALL_FILES_UPTO_MB':'ALL_FILES_UPTO_MB_CHINESE';
                                        throw new \Exception(UtilityController::Getmessage($returnMessage));
                                    }
                                }
                            }
                        }
                        if (isset($input['cover_image'])) {
                            foreach ($input['cover_image'] as $key => $value) {
                                $sum = 0;

                                foreach ($input['cover_image'] as $key => $value) {
                                    $sum = $sum + $value->getClientSize();
                                    if ($sum > UtilityController::Getmessage('MAX_UPLOAD_FILE_SIZE')) {
                                        $returnMessage=$input['language']=='en'?'COVER_ALL_FILES_UPTO_MB':'COVER_ALL_FILES_UPTO_MB_CHINESE';
                                        throw new \Exception(UtilityController::Getmessage($returnMessage));
                                    }
                                }
                            }
                        }
                        $categoryData                     = UtilityController::MakeObjectFromArray($request->all(), 'MarketPlaceUpload')->toArray();
                        $categoryData['upload_of']        = 1;
                        $categoryData['uploaded_user_id'] = Auth::user()->id;

                        //============================================
                        // $input['cover_image'] = array_values($input['cover_image']);                       
                        if (!empty($input['cover_image'])) {
                            $fileName  = 'cover_' . rand() . time() . preg_replace('/[^A-Za-z0-9\-.]/', '', $input['cover_image']->getClientOriginalName());
                            $fileName = str_replace('-', '', $fileName);                            
                            $extension = \File::extension($fileName);
            
                            if (in_array($extension, array('png', 'jpg', 'jpeg'))) {

                                $categoryData['cover_img'] = $fileName;
                                $newCategory = UtilityController::Makemodelobject($categoryData, 'MarketPlaceUpload');
                            } else {
                                $returnMessage=$input['language']=='en'?'ONLY_JPG_PNG_JPEG':'ONLY_JPG_PNG_JPEG_CHINESE';
                                throw new \Exception(UtilityController::Getmessage($returnMessage));
                            }
                                // foreach ($input['cover_image'] as $key => $value) {                        
                                $fileName  = rand() . time() . preg_replace('/[^A-Za-z0-9\-.]/', '', $input['cover_image']->getClientOriginalName());
                                $fileName = str_replace('-', '', $fileName);
                                $extension = \File::extension($fileName);

                                //==========================                                
                                $orgfileName  = rand() . time() . preg_replace('/[^A-Za-z0-9\-.]/', '', $input['cover_image']->getClientOriginalName());
                                $orgfileName = str_replace('-', '', $orgfileName);
                                $tinyfileName    = 'tiny_' . $orgfileName;
                                $mediumfileName  = 'medium_' . $orgfileName;
                                $galleryFile     = array();
                                $extension       = \File::extension($orgfileName);

                                if (! File::exists(public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id)) {
                                    File::makeDirectory(public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id, 0777, true, true);                                    
                                }
                                if (! File::exists(public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id ."/Cover_image/")) {
                                    File::makeDirectory(public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id . "/Cover_image/", 0777, true, true);
                                }
                                if (! File::exists(public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id ."/Medium/")) {
                                    File::makeDirectory(public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id . "/Medium/", 0777, true, true);
                                }
                                if (! File::exists(public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id ."/Original/")) {
                                    File::makeDirectory(public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id . "/Original/", 0777, true, true);
                                }
                                if (! File::exists(public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id ."/Tiny/")) {
                                    File::makeDirectory(public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id . "/Tiny/", 0777, true, true);
                                }
                                chmod(public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id , 0777);
                                $DestinationPath = public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id.'/Cover_image/';
                                $DestinationPathTiny = public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id.'/Tiny/';
                                $DestinationPathMedium = public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id.'/Medium/';
                                if (in_array($extension, array('png', 'jpg', 'jpeg', 'ico', 'gif', 'bmp'))) {
                                    Image::make($input['cover_image']->getRealPath())->resize(1280, 720, function ($constraint) {
                                        $constraint->aspectRatio();
                                        $constraint->upsize();
                                    })->save($DestinationPath . $mediumfileName);
                                    Image::make($input['cover_image']->getRealPath())->resize(300, 300, function ($constraint) {
                                        $constraint->aspectRatio();
                                        $constraint->upsize();
                                    })->save($DestinationPath . $tinyfileName);

                                    Image::make($input['cover_image']->getRealPath())->save($DestinationPath . $orgfileName);

                                    $galleryFile['medium_thumbnail'] = $mediumfileName;
                                    $galleryFile['tiny_thumbnail']     = $tinyfileName;
                                    $galleryFile['original_file_name'] = $orgfileName;
                                } else {
                                    $input['cover_image']->move($DestinationPath . $orgfileName);                                     
                                    $galleryFile['original_file_name'] = $orgfileName;
                                }
                                //==========================
                                $galleryFile['mp_upload_id'] = $newCategory->id;
                                $galleryFile['status']=3;
                                $addedFile[]                 = UtilityController::Makemodelobject($galleryFile, 'MarketPlaceUploadAttachment', 'id');
                            // } 
                        }
                        else{
                            $returnMessage=$input['language']=='en'?'UPLOAD_COVER_IMAGES_REQUIRED':'UPLOAD_COVER_IMAGES_REQUIRED_CHINESE';
                            throw new \Exception(UtilityController::Getmessage($returnMessage));
                        }
                        
                        
                        //============================================
                        $zipFileName = "";
                        if (isset($input['file_name']) && !empty($input['file_name'])) {
                            $zip         = new ZipArchive;
                            $zipFileName = rand() . time();
                            $zip->open(public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH') .Auth::user()->id.'/'. $zipFileName, ZipArchive::CREATE);
                            $addedFile = array();
                            foreach ($input['file_name'] as $key => $value) {                                
                                $fileName  = rand() . time() . preg_replace('/[^A-Za-z0-9\-.]/', '', $value->getClientOriginalName());
                                $fileName = str_replace('-', '', $fileName);
                                $extension = \File::extension($fileName);

                                //==========================                                
                                $orgfileName  = rand() . time() . preg_replace('/[^A-Za-z0-9\-.]/', '', $value->getClientOriginalName());
                                $orgfileName = str_replace('-', '', $orgfileName);
                                $tinyfileName    = 'tiny_' . $orgfileName;
                                $mediumfileName  = 'medium_' . $orgfileName;
                                $galleryFile     = array();
                                $extension       = \File::extension($orgfileName);
                                $DestinationPath = public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id.'/Original/';
                                $DestinationPathTiny = public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id.'/Tiny/';
                                $DestinationPathMedium = public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id.'/Medium/';
                            
                                if (in_array($extension, array('png', 'jpg', 'jpeg', 'ico', 'gif', 'bmp'))) {
                                    Image::make($value->getRealPath())->resize(1280, 720, function ($constraint) {
                                        $constraint->aspectRatio();
                                        $constraint->upsize();
                                    })->save($DestinationPathMedium . $mediumfileName);
                                    Image::make($value->getRealPath())->resize(300, 300, function ($constraint) {
                                        $constraint->aspectRatio();
                                        $constraint->upsize();
                                    })->save($DestinationPathTiny . $tinyfileName);

                                    Image::make($value->getRealPath())->save($DestinationPath . $orgfileName);
                                    $galleryFile['medium_thumbnail'] = $mediumfileName;
                                    $galleryFile['tiny_thumbnail']     = $tinyfileName;
                                    $galleryFile['original_file_name'] = $orgfileName;
                                    $galleryFile['mp_upload_id'] = $newCategory->id;
                                    $galleryFile['status']=4;
                                    $addedFile[]                 = UtilityController::Makemodelobject($galleryFile, 'MarketPlaceUploadAttachment', 'id');
                                } else {
                                    $value->move($DestinationPath . $orgfileName);                                    
                                    $galleryFile['original_file_name'] = $orgfileName;
                                }
                                //==========================
                               
                                //++++++++++++++++++++++++++++++++++++++++++++
                                // Zip code started.
                                if (!in_array($extension, array('zip', 'ZIP', 'tar', 'TAR', 'rar', 'RAR', 'js', 'JS'))) {
                                    $value->move(public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id.'/', $fileName);
                                    chmod(public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id ."/". $fileName, 0777);
                                    $zip->addFile(public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id."/" . $fileName, $fileName);
                                    $fileList[] = $fileName;
                                } else {
                                    $returnMessage=$input['language']=='en'?'NO_COMPRESSED_ALLOWED':'NO_COMPRESSED_ALLOWED_CHINESE';
                                    throw new \Exception(UtilityController::Getmessage($returnMessage));
                                }
                                //++++++++++++++++++++++++++++++++++++++++++++
                            }
                            $zip->close();                            
                            chmod(public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id.'/' . $zipFileName, 0777);
                            $bucket   = UtilityController::Getmessage('BUCKET');
                            $object   = UtilityController::Getpath('BUCKET_MP_INS') . $zipFileName;
                            $filePath = public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id.'/' . $zipFileName;
                            if (filesize($filePath) <= UtilityController::Getmessage('FILE_SIZE_15')) {
                                $ossClient            = new OssClient(UtilityController::Getmessage('ACCESS_KEY_ID'), UtilityController::Getmessage('ACCESS_KEY_SECRET'), UtilityController::Getmessage('END_POINT'));                                
                                foreach ($fileList as $key => $value) {
                                    unlink(public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH') .Auth::user()->id.'/'. $value);
                                }
                                $ossClient->uploadFile($bucket, $object, $filePath);
                                unlink(public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH') .Auth::user()->id.'/'. $zipFileName);

                            } else {
                                $returnMessage=$input['language']=='en'?'FILE_SIZE_LARGE_15':'FILE_SIZE_LARGE_15_CHINESE';
                                throw new \Exception(UtilityController::Getmessage($returnMessage));
                            }
                        }
                        else{
                            $returnMessage=$input['language']=='en'?'UPLOAD_FILES_REQUIRED':'UPLOAD_FILES_REQUIRED_CHINESE';
                            $returnData = UtilityController::Generateresponse(false, $returnMessage, 200, '');
                            // throw new \Exception(UtilityController::Getmessage($returnMessage));
                        }
                        //Code to insert data into mp_inspirationbank_category_details table
                        if(!empty($input['skills'])){
                            $list = array();
                            foreach($input['skills'] as $key => $value){
                                $data = [
                                    'mp_upload_id'  => $newCategory->id,
                                    'categoty_id'   => $value,
                                    'created_at'   => Carbon::now(),
                                    'updated_at'   => Carbon::now(),                                    
                                ];
                                array_push($list,$data);                            
                            }                            
                            MarketPlaceInspirationBankCategoryDetail::insert($list);
                        }
                        /*$newUpload = UtilityController::Makemodelobject($categoryData, 'MarketPlaceUpload', 'id', $newCategory->id);*/
                        $slug   = str_slug($input['upload_title'], '-');
                        $slug      = $slug . ":" . base64_encode($newCategory->id);
                        MarketPlaceUpload::where('id',$newCategory->id)->update(array('file' => $zipFileName,'slug' => $slug));
                        MarketPlaceUploadAttachment::where('id', $addedFile)->update(array('mp_upload_id' => $newCategory->id));

                        \DB::commit();
                        $returnMessage=$input['language']=='en'?'ADDED':'ADDED_CHINESE';
                            
                        $returnData = UtilityController::Generateresponse(true, $returnMessage, 200, '');
                    }
                } else {
                    $returnMessage=$input['language']=='en'?'SELECT_ATLEAST_COVER_IMAGE_INSPIRATION_BANK':'SELECT_ATLEAST_COVER_IMAGE_INSPIRATION_BANK_CHINESE';
                    $returnData = UtilityController::Generateresponse(false, $returnMessage, 200, '');
                    // throw new \Exception(UtilityController::Getmessage($returnMessage));
                }

            }
            return $returnData;
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, $e->getMessage(), 0);
            return $returnData;
        }
    }

    // designer
    //###############################################################
    //Function Name : Designergalleryupload
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To upload gallery product
    //In Params : Void
    //Return : json
    //Date : 10th April 2018
    //###############################################################
    public function Designergallerynewupload(Request $request)
    {
        try {
            \DB::beginTransaction();
            $validator  = UtilityController::ValidationRules($request->all(), 'MarketPlaceUpload', array('uploaded_user_id', 'upload_of'));
            $returnData = $validator;
            if (!$validator['status']) {
                $errorMessage = explode('.', $validator['message']);
                $returnData   = UtilityController::Generateresponse(false, $errorMessage['0'], 0, '');
            } else {
                $input          = Input::all();

                $categoryDetail = MarketPlaceGalleryCategory::find($input['category_id']);

                $rules          = array(
                    'file_name.*' => 'required' . '|' . $categoryDetail['validation'],
                );
                $Validate = Validator::make(Input::all(), $rules);

                if ($Validate->fails()) {
                    $returnData = UtilityController::Generateresponse(false, $Validate->messages()->first(), '', '');
                } else {
                    $categoryData                     = UtilityController::MakeObjectFromArray($request->all(), 'MarketPlaceUpload')->toArray();
                    $categoryData['upload_of']        = 2;
                    $categoryData['uploaded_user_id'] = Auth::user()->id;

                    //============================================
                    if (isset($input['cover_img'])) {
                        $fileName  = 'cover_' . rand() . time() . str_replace(' ', '_', $input['cover_img']->getClientOriginalName());
                        $extension = \File::extension($fileName);

                        //========================== Upload Cover Image
                        $extension       = \File::extension($fileName);
                        $DestinationPath = public_path('') . config('constants.path.MP_ATTACHMENT_UPLOAD_PATH');
                        if (in_array($extension, array('png', 'jpg', 'jpeg'))) {
                            Image::make($input['cover_img']->getRealPath())->resize(220, 220, function ($constraint) {
                                $constraint->aspectRatio();
                                $constraint->upsize();
                            })->save($DestinationPath . $fileName);
                            $categoryData['cover_img'] = $fileName;
                        } else {
                            throw new \Exception(UtilityController::Getmessage('ONLY_JPG_PNG_JPEG'));
                        }
                    }
                    $newCategory = UtilityController::Makemodelobject($categoryData, 'MarketPlaceUpload', 'id');
                    //============================================
                    //============================================
                    if (isset($input['file_name'])) {
                        // print_r($input['file_name']);die;
                        $zip         = new ZipArchive;
                        $zipFileName = rand() . time();
                        $zip->open(public_path('') . config('constants.path.MP_ATTACHMENT_UPLOAD_PATH') . $zipFileName, ZipArchive::CREATE);
                        $addedFile = array();
                        foreach ($input['file_name'] as $key => $value) {
                            $fileName  = rand() . time() . str_replace(' ', '_', $value->getClientOriginalName());
                            $extension = \File::extension($fileName);

                            //==========================
                            $orgfileName    = rand() . time() . str_replace(' ', '_', $value->getClientOriginalName());
                            $tinyfileName   = 'tiny_' . $orgfileName;
                            $mediumfileName = 'medium_' . $orgfileName;
                            $galleryFile    = array();
                            $extension      = \File::extension($orgfileName);
                            // print_r($extension);die;
                            $DestinationPath = public_path('') . config('constants.path.MP_ATTACHMENT_UPLOAD_PATH');
                            if (in_array($extension, array('png', 'jpg', 'jpeg', 'ico', 'gif', 'bmp'))) {
                                Image::make($value->getRealPath())->resize(150, 150, function ($constraint) {
                                    $constraint->aspectRatio();
                                    $constraint->upsize();
                                })->save($DestinationPath . $mediumfileName);
                                Image::make($value->getRealPath())->resize(300, 300, function ($constraint) {
                                    $constraint->aspectRatio();
                                    $constraint->upsize();
                                })->save($DestinationPath . $tinyfileName);

                                Image::make($value->getRealPath())->save($DestinationPath . $orgfileName);
                                $galleryFile['medium_thumbnail'] = $mediumfileName;

                                // print_r(1);die;
                                // $coverImg = "coverImg_" . $fileName;
                                // print_r($DestinationPath);die;
                                // $result = self::Resizeimage($value, 220, 220, $tinyfileName, $DestinationPath);
                                // if ($result) {
                                //     $Input['cover_img'] = $tinyfileName;
                                // }
                                $galleryFile['tiny_thumbnail']     = $tinyfileName;
                                $galleryFile['original_file_name'] = $orgfileName;
                            } else {
                                $value->move($DestinationPath . $orgfileName);
                                // file::make($Input['file'])->save($DestinationPath . $orgfileName);
                                $galleryFile['original_file_name'] = $orgfileName;
                            }
                            //==========================
                            $galleryFile['mp_upload_id'] = $newCategory->id;
                            $addedFile[]                 = UtilityController::Makemodelobject($galleryFile, 'MarketPlaceUploadAttachment', 'id');
                            //++++++++++++++++++++++++++++++++++++++++++++
                            // Zip code started.
                            if (!in_array($extension, array('zip', 'ZIP', 'tar', 'TAR', 'rar', 'RAR', 'js', 'JS'))) {
                                $value->move(public_path('') . config('constants.path.MP_ATTACHMENT_UPLOAD_PATH'), $fileName);
                                chmod(public_path('') . config('constants.path.MP_ATTACHMENT_UPLOAD_PATH') . $fileName, 0777);
                                $zip->addFile(public_path('') . config('constants.path.MP_ATTACHMENT_UPLOAD_PATH') . $fileName, $fileName);
                                $fileList[] = $fileName;
                            } else {
                                throw new \Exception(UtilityController::Getmessage('NO_COMPRESSED_ALLOWED'));
                            }
                            //++++++++++++++++++++++++++++++++++++++++++++
                        }
                        $zip->close();
                        // print_r(public_path('') . config('constants.path.MP_ATTACHMENT_UPLOAD_PATH') . $zipFileName);die;
                        chmod(public_path('') . config('constants.path.MP_ATTACHMENT_UPLOAD_PATH') . $zipFileName, 0777);
                        $bucket   = UtilityController::Getmessage('BUCKET');
                        $object   = UtilityController::Getpath('BUCKET_MP_GALLERY') . $zipFileName;
                        $filePath = public_path('') . config('constants.path.MP_ATTACHMENT_UPLOAD_PATH') . $zipFileName;
                        if (filesize($filePath) <= UtilityController::Getmessage('FILE_SIZE_15')) {
                            $ossClient            = new OssClient(UtilityController::Getmessage('ACCESS_KEY_ID'), UtilityController::Getmessage('ACCESS_KEY_SECRET'), UtilityController::Getmessage('END_POINT'));
                            $categoryData['file'] = $zipFileName;
                            foreach ($fileList as $key => $value) {
                                unlink(public_path('') . config('constants.path.MP_ATTACHMENT_UPLOAD_PATH') . $value);
                            }
                            $ossClient->uploadFile($bucket, $object, $filePath);
                            unlink(public_path('') . config('constants.path.MP_ATTACHMENT_UPLOAD_PATH') . $zipFileName);
                        } else {
                            throw new \Exception(UtilityController::Getmessage('FILE_SIZE_LARGE_15'));
                        }
                    }

                    $newUpload = UtilityController::Makemodelobject($categoryData, 'MarketPlaceUpload', 'id', $newCategory);
                    MarketPlaceUploadAttachment::where('id', $addedFile)->update(array('mp_upload_id' => $newCategory->id));
                    \DB::commit();
                    $returnData = UtilityController::Generateresponse(true, 'ADDED', 200, '');
                }
            }
            return $returnData;
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, $e->getMessage(), '', '');
            return $returnData;
        }
    }

    // designer
    //###############################################################
    //Function Name : Designerprojectinfo
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get Designer's project info
    //In Params : Void
    //Return : json
    //Date : 9th April 2018
    //###############################################################
    public function Designerprojectinfo(Request $request)
    {
        try {
            \DB::beginTransaction();
            $input         = Input::all();
            $buyerId       = Auth::user()->id;
            $project_slug  = $input['slug'];
            $mp_project_id = substr($input['slug'], strpos($input['slug'], ":") + 1);
            $mp_project_id = base64_decode($mp_project_id);

            $projects = MarketPlaceProject::where('slug', $request->slug)
                ->with([
                    'projectCreator'    => function ($query) {
                        $query->get();
                    },
                    'projectdetailinfo' => function ($query) {
                        $query->get();
                    },
                    'skills'            => function ($query) {
                        $query->with('skillDetail')->get()->groupby('skill_set_id');
                    },
                    'message_board'     => function ($qry) {
                        $qry->with(['message_board' => function ($q) {
                            $q->where('type', '!=', 1)->get();
                        }])->whereIn('type', array(1, 4))->get();
                    },
                    'seller_payment'    => function ($query) {
                        $query->get();
                    },
                    'projectdetail' => function ($query) {
                        $query->first();
                    },
                ])->with('extension_details')->first();                            
            if ($projects) {
                ($projects['extension_details'] != ''? (($projects['extension_details']['extension_status'] == 3 || $projects['extension_details']['extension_status'] == 1) ? $projects['extension_required'] = 1 : $projects['extension_required'] = 0) : (Auth::user()->marketplace_current_role == 1) ? $projects['extension_required'] = 1 :$projects['extension_required'] = 0);

                ($projects['extension_details'] != '' ? (($projects['extension_details']['extension_status'] == 3 || $projects['extension_details']['extension_status'] == 2) ? $projects['show_buyer_extension'] = 0 : $projects['show_buyer_extension'] = 1) : $projects['show_buyer_extension'] = 0);

                $projects['project_created_at'] = $projects['created_at']->format('d F, Y');
                
                $projects['image']              = UtilityController::Imageexist($projects['buyer']['image'], public_path('') . UtilityController::Getmessage('USER_PROFILE_PATH'), url('') . UtilityController::Getmessage('USER_PROFILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));

                $date = Carbon::parse($projects['selected_designer']['created_at'])->timezone(Auth::user()->timezone);
                $projects['invitation_sent_on'] = UtilityController::Changedateformat($date, 'd F, Y h:i A');
                
                if ($projects['selected_designer']['invitation_accepted_at'] != '') {
                    $date = Carbon::parse($projects['selected_designer']['invitation_accepted_at'])->timezone(Auth::user()->timezone);
                    $projects['invitation_accepted_on'] = UtilityController::Changedateformat($date, 'd F, Y h:i A');
                } else {
                    $projects['invitation_accepted_on'] = "Pending";
                }

                if ($projects['selected_designer']['project_completed_at'] != '') {
                    $date = Carbon::parse($projects['selected_designer']['project_completed_at'])->timezone(Auth::user()->timezone);
                    $projects['project_completed_on'] = UtilityController::Changedateformat($date, 'd F, Y h:i A');
                } else {
                    $projects['project_completed_on'] = "Pending";
                }


                $projects['selected_designer']['designer_details']['image'] = UtilityController::Imageexist($projects['selected_designer']['designer_details']['image'], public_path('') . UtilityController::Getmessage('USER_PROFILE_PATH'), url('') . UtilityController::Getmessage('USER_PROFILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));

                $projects['selected_designer']['encrypted_assigned_designer_id'] = base64_encode($projects['selected_designer']['assigned_designer_id']);

                if(!empty($projects['projectdetail'])){
                    $projects['projectdetail']['status']=$projects['projectdetail'][0]['status'];
                }

                $projectAt = Carbon::parse($projects['created_at'])->addDays($projects['project_timeline']);

                $projects['deadline'] = $projectAt->toDateTimeString();
            
                    foreach ($projects['message_board'] as $key => $value) {

                        if($value['type'] == 4) {
                            $date = Carbon::parse($value['created_at'])->timezone(Auth::user()->timezone);
                            $projects['message_board'][$key]['message']=$value['message'].' at '.UtilityController::Changedateformat($date, 'd F, Y h:i A');
                            $projects['message_board'][$key]['message_in_chinese']=$value['message_in_chinese'].' '.UtilityController::Changedateformat($date, 'd F, Y  h:i A');
                        }
                        
                        if(($projects['message_board'][$key]['user_id']==$projects['selected_designer']['assigned_designer_id'] && $projects['message_board'][$key]['receiver_id']==$projects['projectCreator']['id']) || ($projects['message_board'][$key]['receiver_id']==$projects['selected_designer']['assigned_designer_id'] && $projects['message_board'][$key]['user_id']==$projects['projectCreator']['id'])){

                            // IF designer role is there then designer's image came else buyer's
                            $userImage           = (Auth::user()->marketplace_current_role == 1 ? ($value['user_role'] == 1 ? Auth::user()->image : $value['user_details']['image']) : ($value['user_role'] == 2 ? Auth::user()->image : $value['user_details']['image']));
                            $value['image']      = UtilityController::Imageexist($userImage, public_path('') . UtilityController::Getmessage('USER_PROFILE_PATH'), url('') . UtilityController::Getmessage('USER_PROFILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));
                            $date = Carbon::parse($value['created_at'])->timezone(Auth::user()->timezone);
                            $value['message_on'] = UtilityController::Changedateformat($date, 'd F, Y h:i A');

                            // Class assignement based on role
                            $value['side_class'] = (Auth::user()->marketplace_current_role == 1 ? ($value['user_role'] == 1 ? 'right' : 'left') : ($value['user_role'] == 2 ? 'right' : 'left'));

                            // Name assignement based on role
                            if($projects['message_board'][$key]['user_role']==3){

                                $value['side_class'] = '';
                                $projects['message_board'][$key]['name']='ADMIN';
                                
                            }else{
                                if (Auth::user()->marketplace_current_role == 1) {
                                    $projects['message_board'][$key]['name'] = ($value['user_role'] == 1 ? Auth::user()->first_name . ' ' . Auth::user()->last_name : $value['user_details']['first_name'] . ' ' . $value['user_details']['last_name']);
                                }
                                if (Auth::user()->marketplace_current_role == 2) {
                                    $projects['message_board'][$key]['name'] = ($value['user_role'] == 2 ? Auth::user()->first_name . ' ' . Auth::user()->last_name : $value['user_details']['first_name'] . ' ' . $value['user_details']['last_name']);
                                }    
                            }

                            // If type 2 is there then mity image will be assigned
                            if ($projects['message_board'][$key]['message_board']) {
                                $innerMessageBoard = $projects['message_board'][$key]['message_board'];
                                foreach ($innerMessageBoard as $keyInner => $valueInner) {
                                    if ($valueInner['type'] == 2) {
                                        $innerMessageBoard[$keyInner]['placeholder'] = UtilityController::Imageexist('tiny_' . $valueInner['message'], public_path('') . UtilityController::Getmessage('MSG_FILE_PATH'), url('') . UtilityController::Getmessage('MSG_FILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));
                                        $innerMessageBoard[$keyInner]['download_placeholder'] = UtilityController::Imageexist($valueInner['message'], public_path('') . UtilityController::Getmessage('MSG_FILE_PATH'), url('') . UtilityController::Getmessage('MSG_FILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));

                                        $innerMessageBoard[$keyInner]['message'] = public_path('') . UtilityController::Getmessage('MSG_FILE_PATH') . $valueInner['message'];
                                    }

                                    // If type 3 means non image file is there then desfault file pic will be assigned to return.
                                    if ($valueInner['type'] == 3) {
                                        $innerMessageBoard[$keyInner]['placeholder'] = url('/') . UtilityController::Getmessage('DEFAULT_FILE_URL');
                                        $innerMessageBoard[$keyInner]['download_placeholder'] = UtilityController::Fileexist($valueInner['message'], public_path('') . UtilityController::Getmessage('MSG_FILE_PATH'), url('') . UtilityController::Getmessage('MSG_FILE_URL'));
                                        $innerMessageBoard[$keyInner]['message']     = public_path('') . UtilityController::Getmessage('MSG_FILE_PATH') . $valueInner['message'];
                                    }
                                }
                            }

                            if($projects['message_board'][$key]['user_role']==3){
                                if($projects['message_board'][$key]['user_id']==0 || $projects['message_board'][$key]['user_id']==Auth::user()->id){
                                }
                                else{
                                    unset($projects['message_board'][$key]);
                                }
                            }
                        }else{
                            // $projects['message_board'][$key]=[];
                        }
                    }
                /*****************************************************************************/
                // $projects['messages'] = MarketPlaceMessageBoard::where('id', $mp_project_id)->get();
                if (!empty($projects)) {
                    $projectAt                        = Carbon::parse($projects['created_at']);
                    $diffDays                         = $projectAt->diffInDays(Carbon::now());
                    $projects['days_remaining']       = $projects['project_timeline'] - $diffDays;

                    $invitationAt                     = Carbon::parse($projects['selected_designer']['created_at']);

                    $projectAt = Carbon::parse($projects['created_at'])->addDays($projects['project_timeline']);

                    $projects['deadline'] = $projectAt->toDateTimeString();
                    $projects['timeline_deadline'] = $invitationAt->toDateTimeString();

                    $projects                           = $projects->toArray();
                    
                    $date = Carbon::parse($projects['created_at'])->timezone(Auth::user()->timezone);
                    $projects['created_at']             = UtilityController::Changedateformat($date, 'd F, Y');
                    
                    $date = Carbon::parse($projects['projectdetailinfo']['invitation_accepted_at'])->timezone(Auth::user()->timezone);
                    $projects['invitation_accepted_at'] = UtilityController::Changedateformat($date, 'd F, Y');
                    
                    $date = Carbon::parse($projects['projectdetailinfo']['project_completed_at'])->timezone(Auth::user()->timezone);
                    $projects['project_completed_at']   = UtilityController::Changedateformat($date, 'd F, Y');
                }
                $projects['message_board']=array_values($projects['message_board']);
                //Code to change the Status of Notification
                $notificationData = SystemNotification::where("receiver_id",Auth::user()->id)->where("module",1)->where('project_slug',$project_slug)->update(["read_unread" => 1]);                                
                \DB::commit();
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $projects);
                return $returnData;
            }



        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Designermarkprojectcomplete
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : Mark project as a complete
    //In Params : Void
    //Return : json
    //Date : 11th April 2018
    //###############################################################
    public function Designermarkprojectcomplete(Request $request)
    {
        try {
            \DB::beginTransaction();
            /*$projects = MarketPlaceProjectsDetailInfo::where('mp_project_id', $request->projectId)
                ->where('assigned_designer_id', Auth::user()->id)
                ->update(array('project_completed_at' => date('Y-m-d H:i:s')));*/

            MarketPlaceProjectsDetailInfo::where('mp_project_id', $request->projectId)->update(array(
                'status' => 6));

            $projectData = MarketPlaceProject::where('id', $request->projectId)->first();
            $buyerData   = User::whereHas('marketplaceproject', function ($query) use ($request) {
                $query->where('id', $request->projectId);
            })->first()->toArray();
            $designerData                    = User::where('id', Auth::user()->id)->first()->toArray();
            $buyerData['project']            = $projectData['project_title'];
            $buyerData['designerfirst_name'] = $designerData['first_name'];
            $buyerData['designerlast_name']  = $designerData['last_name'];
            $buyerData['link']               = url('/') . '/buyer-project-info/' .
                $projectData["slug"];




            /*$mailResult = Mail::send('emails.notify_buyer_for_prj_completed_by_designer', $buyerData, function ($message) use ($buyerData) {
                $message->from('noreply@sixclouds.cn', 'SixClouds');
                $message->to($buyerData['email_address'])
                    ->subject('Sixclouds : Project completed by seller');
            });*/

            $returnMessage=$request->language=='en'?'PROJECT_COMPLETED':'PROJECT_COMPLETED_CHINESE';

            $returnData = UtilityController::Generateresponse(true, $returnMessage, 200, '');

            $data['mp_project_id'] = $request->projectId;
            $data['action_type']   = 4;
            $data['action_text']   = UtilityController::Getmessage('SELLER_REQUESTED_COMPLETION');
            $data['show_first']    = 1;
            $actionResult          = UtilityController::Makemodelobject($data, 'MarketplaceAction');
            $actionResult          = UtilityController::Makemodelobject($data, 'MarketPlaceProject', '', $data['mp_project_id']);

            $projectDetails        = MarketPlaceProject::with('projectCreator','selected_designer')->where('id',$data['mp_project_id'])->first()->toArray();
           
            //Seller mail
            $buyerData['project_number']=$projectDetails['project_number'];
            $buyerData['email_address']=$projectDetails['project_creator']['email_address'];
            $buyerData['subject']=Auth::user()->current_language==1?'Request to complete project':'要求完成项目';

            $buyerData['content']=Auth::user()->current_language==1?'Hello <strong>'.$projectDetails['project_creator']['first_name'].$projectDetails['project_creator']['last_name'].' ,
            </strong><br/>
            <br/>
            Your project completion request for Project  <strong>ID: '.$projectDetails['project_number'].' </strong> has been sent to Buyer.
            <br/<br/><br/>':'你好 <strong>'.$projectDetails['project_creator']['first_name'].' '.$projectDetails['project_creator']['last_name'].'</strong> ,
            <br/>
            <br/>
            完成项目申请  <strong>ID: '.$projectDetails['project_number'].'</strong> 已发送给买方<br/<br/><br/>';

            $buyerData['footer_content']=Auth::user()->current_language==1?'From,<br /> SixClouds Sixteen':'六云十六 ';

            $buyerData['footer']=Auth::user()->current_language==1?'SixClouds':'六云';

            Mail::send('emails.email_template', $buyerData, function ($message) use ($buyerData) {
                $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                $message->to($buyerData['email_address'])
                    ->subject($buyerData['subject']);
            });

            self::Insertsystemnotifications($projectDetails['selected_designer']['assigned_designer_id'],$projectDetails['project_created_by_id'],'Seller requested completion for project "'.$projectDetails['project_title'].'"','卖方要求完成项目"'.$projectDetails['project_title'].'"',$projectDetails['slug']);

            self::Insertmessageboardnotifications($projectDetails['project_created_by_id'],$data['mp_project_id'],'Seller has requested for complete the project','卖方已要求完成该项目',4,1,1);

            self::Insertadmintimelinenotifications(Auth::user()->id,$request->projectId,'Seller requested for completion of project at ',' 卖方要求完成项目 ',8,0);

            \DB::commit();
            return $returnData;
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : DesignerCancelProject
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : Designer Cancelled project
    //In Params : Void
    //Return : json
    //Date : 11th April 2018
    //###############################################################
    public function DesignerCancelProject(Request $request)
    {
        try {
            \DB::beginTransaction();
            $input    = Input::all();

            $projects = MarketPlaceProjectsDetailInfo::where('mp_project_id', $input['projectId'])
                ->where('assigned_designer_id', Auth::user()->id)
                ->update(array('status' => 9));

            MarketPlaceProject::where('id', $input['projectId'])->update(array(
                'project_status' => 8, 'project_cancellation_reason' => $input['reason'], 'project_cancel_request_at' => Carbon::now()));

            $returnMessage=$input['language']=='en'?'PROJECT_CANCEL_REQUEST_SENT':'PROJECT_CANCEL_REQUEST_SENT_CHINESE';
            $returnData = UtilityController::Generateresponse(true, $returnMessage, 200, '');

            self::Insertmessageboardnotifications(Auth::user()->id,$input['projectId'],'Seller requested to cancel the project','卖方要求取消该项目',4,1,1);

            $data['mp_project_id'] = $input['projectId'];
            $data['action_type']   = 4;
            $data['action_text']   = UtilityController::Getmessage('SELLER_REQUESTED_CANCELLATION');
            $data['show_first']    = 1;
            $actionResult          = UtilityController::Makemodelobject($data, 'MarketplaceAction');
            $actionResult          = UtilityController::Makemodelobject($data, 'MarketPlaceProject', '', $data['mp_project_id']);

            $projectDetails = MarketPlaceProject::with('projectCreator','selected_designer')->where('id',$data['mp_project_id'])->first();
           
            self::Insertsystemnotifications($projectDetails['selected_designer']['assigned_designer_id'],$projectDetails['project_created_by_id'],'Seller requested Cancellation for project "'.$projectDetails['project_title'].'" due to '.$input['reason'],'卖方要求取消项目"'.$projectDetails['project_title'].'"由于'.$input['reason'],$projectDetails['slug']);

            self::Insertadmintimelinenotifications(Auth::user()->id,$input['projectId'],'Seller requested for cancellation of project at ',' 卖方要求取消项目 ',7,0);
            
            //Seller mail
            $sellerData['project_number']=$projectDetails['project_number'];
            $sellerData['email_address']=$projectDetails['selected_designer']['designer_details']['email_address'];
            $sellerData['subject']=Auth::user()->current_language==1?'Request to cancel project':'要求取消项目';

            $sellerData['content']=Auth::user()->current_language==1?'Hello <strong>'.$projectDetails['selected_designer']['designer_details']['first_name'].$projectDetails['selected_designer']['designer_details']['last_name'].' ,
            </strong><br/>
            <br/>
            Your cancellation request for Project  <strong>ID: '.$sellerData['project_number'].' </strong> has been sent to Buyer.
            <br/<br/><br/>':'你好 <strong>'.$projectDetails['selected_designer']['designer_details']['first_name'].' '.$projectDetails['selected_designer']['designer_details']['last_name'].'</strong> ,
            <br/>
            <br/>
            项目  <strong>ID: '.$sellerData['project_number'].'</strong> 的取消请求已发送给买家。<br/<br/><br/>';

            $sellerData['footer_content']=Auth::user()->current_language==1?'From,<br /> SixClouds Sixteen':'六云十六 ';

            $sellerData['footer']=Auth::user()->current_language==1?'SixClouds':'六云';

            Mail::send('emails.email_template', $sellerData, function ($message) use ($sellerData) {
                $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                $message->to($sellerData['email_address'])
                    ->subject($sellerData['subject']);
            });
            //Buyer mail
            $projectDetails = MarketPlaceProject::with('projectCreator')->where('id',$data['mp_project_id'])->first()->toArray();
            
            $buyerData['email_address']=$projectDetails['project_creator']['email_address'];
            $buyerData['subject']=$projectDetails['project_creator']['current_language'] ==1?'Seller request for cancel project':'卖方要求取消项目';

            $buyerData['content']=$projectDetails['project_creator']['current_language']==1?'Hello <strong>'.$projectDetails['project_creator']['first_name'].' '.$projectDetails['project_creator']['last_name'].' ,
            </strong><br/>
            <br/>
            Seller has requested cancellation for Project <strong>'.$projectDetails['project_number'].'</strong> Please indicate your decision <a href="'.URL::to('/buyer-project-info/'.$projectDetails['slug']).'">here</a>.

            <br/<br/><br/>':'你好 <strong>'.$projectDetails['project_creator']['first_name'].' '.$projectDetails['project_creator']['last_name'].'</strong> ,
            <br/>
            <br/>
            卖家已要求取消项目 <strong>'.$projectDetails['project_number'].'</strong>。请点击 <a href="'.URL::to('/buyer-project-info/'.$projectDetails['slug']).'">这里</a> 表明您的决定。<br/<br/><br/>';

            $buyerData['footer_content']=$projectDetails['project_creator']['current_language']==1?'From,<br /> SixClouds Sixteen':'六云十六 ';

            $buyerData['footer']=$projectDetails['project_creator']['current_language']==1?'SixClouds':'六云';

            Mail::send('emails.email_template', $buyerData, function ($message) use ($buyerData) {
                $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                $message->to($buyerData['email_address'])
                    ->subject($buyerData['subject']);
            });

            \DB::commit();
            return $returnData;
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
    //###############################################################
    //Function Name : BuyerCancelProject
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : Buyer Cancelled project
    //In Params : Void
    //Return : json
    //Date : 11th April 2018
    //###############################################################
    public function Buyercancelproject(Request $request)
    {
        try {
            \DB::beginTransaction();
            $projects = MarketPlaceProjectsDetailInfo::where('mp_project_id', $request->projectId)
                ->where('assigned_designer_id', Auth::user()->id)
                ->update(array('status' => 9));

            MarketPlaceProject::where('id', $request->projectId)->update(array(
                'project_status' => 8));

            $returnMessage=$input['language']=='en'?'PROJECT_CANCEL_REQUEST_SENT':'PROJECT_CANCEL_REQUEST_SENT_CHINESE';
            $returnData = UtilityController::Generateresponse(true,$returnMessage, 200, '');

            // Cancel project request for message board
            $messageBoard['user_id']       = Auth::user()->id;
            $messageBoard['mp_project_id'] = $request->projectId;
            $messageBoard['message']       = Auth::user()->first_name . ' ' . Auth::user()->last_name . ' requested to cancel the project ' . date('d M, Y h:i a');
            $messageBoard['type']          = 4;
            $messageBoard['user_role']     = 1;
            // Added as a messageboard
            $result = UtilityController::Makemodelobject($messageBoard, 'MarketPlaceMessageBoard', '');

            $projectDetails                        = MarketPlaceProject::with('selected_designer')->where('id',$messageBoard['mp_project_id'])->first();
            $notification['sender_id']             = $projectDetails['project_created_by_id'];
            $notification['receiver_id']           = $projectDetails['selected_designer']['assigned_designer_id'];
            $notification['notification_text']     = 'Buyer requested cancellation for project "'.$projectDetails['project_title'].'"';
            $notification['chi_notification_text'] = '买方要求取消项目"'.$projectDetails['project_title'].'"';
            $notification['module']                = 1;
            $notification['project_slug']          = $projectDetails['project_title'];
            $notificationUpdate                    = UtilityController::Makemodelobject($notification, 'SystemNotification');

            \DB::commit();
            return $returnData;
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : BuyerCancelProject
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : Buyer Cancelled project
    //In Params : Void
    //Return : json
    //Date : 11th April 2018
    //###############################################################
    /*public function Buyercancelproject(Request $request)
    {
    try {
    \DB::beginTransaction();
    $projects = MarketPlaceProjectsDetailInfo::where('mp_project_id', $request->projectId)
    ->where('assigned_designer_id', Auth::user()->id)
    ->update(array('status' => 11,
    'reason'                => $request->reason));

    MarketPlaceProject::where('id', $request->projectId)->update(array(
    'project_status' => 8));
    $returnData = UtilityController::Generateresponse(true, 'PROJECT_CANCELLED', 200, '');
    \DB::commit();
    return $returnData;
    } catch (Exception $e) {
    \DB::rollback();
    $sendExceptionMail = UtilityController::Sendexceptionmail($e);
    $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
    return $returnData;
    }
    }*/

    //###############################################################
    //Function Name : DesignerInvitation
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : Designer invitation page
    //In Params : Void
    //Return : json
    //Date : 11th April 2018
    //###############################################################
    public function Designerinvitation(Request $request)
    {
        try {
            \DB::beginTransaction();

            $input         = Input::all();
            $buyerId       = Auth::user()->id;
            $project_slug=$input['slug'];
            $mp_project_id = substr($input['slug'], strpos($input['slug'], ":") + 1);
            $mp_project_id = base64_decode($mp_project_id);

            $projects = MarketPlaceProject::where('slug', $request->slug)
                ->with([
                    'projectCreator'    => function ($query) {
                        $query->get();
                    },
                    'projectdetailinfo' => function ($query) {
                        $query->get();
                    },
                    'skills'            => function ($query) {
                        $query->with('skillDetail')->get();
                    },
                    'projectattachment' => function ($query) {
                        $query->get();
                    },
                    'message_board' => function ($qry) {
                        $qry->with(['message_board' => function ($q) {
                            $q->where('type', '!=', 1)->get();
                        }])->whereIn('type', array(1,4))->get();
                    },
                ])->first();

            if (!empty($projects)) {
                $projects                                           = $projects->toArray();
                $date = Carbon::parse($projects['projectdetailinfo']['created_at'])->timezone(Auth::user()->timezone);
                $projects['projectdetailinfo']['created_at']        = UtilityController::Changedateformat($date, 'd F, Y');
                $projects['projectdetailinfo']['projectattachment'] = UtilityController::Getmessage('BUCKET_LINK').'MarketPlace_attachments/' . $projects['projectattachment']['attachment_name'];

                $projects['project_creator']['image'] = UtilityController::Imageexist($projects['project_creator']['image'], public_path('') . UtilityController::Getmessage('USER_PROFILE_PATH'), url('') . UtilityController::Getmessage('USER_PROFILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));

                    if(!empty($projects['message_board'])){
                        foreach ($projects['message_board'] as $key => $value) {

                        if($projects['message_board'][$key]['type'] != 4){
                            if(($projects['message_board'][$key]['user_id']==$projects['projectdetailinfo']['assigned_designer_id'] && $projects['message_board'][$key]['receiver_id']==$projects['project_created_by_id']) || ($projects['message_board'][$key]['receiver_id']==$projects['projectdetailinfo']['assigned_designer_id'] && $projects['message_board'][$key]['user_id']==$projects['project_created_by_id'])){

                                    $projects['message_board'][$key]['image']      = UtilityController::Imageexist($value['user_details']['image'], public_path('') . UtilityController::Getmessage('USER_PROFILE_PATH'), url('') . UtilityController::Getmessage('USER_PROFILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));
                                    
                                    $date = Carbon::parse($value['created_at'])->timezone(Auth::user()->timezone);
                                    $projects['message_board'][$key]['message_on'] = UtilityController::Changedateformat($date, 'd F, Y h:i A');
                                    $projects['message_board'][$key]['side_class'] = ($value['user_role'] == 1 ? 'right' : 'left');


                                    $date = Carbon::parse($value['created_at'])->timezone(Auth::user()->timezone);
                                    $projects['message_board'][$key]['message']=$value['message'].' at '.UtilityController::Changedateformat($date, 'd F, Y h:i A');
                                    $projects['message_board'][$key]['message_in_chinese']=$value['message_in_chinese'].' '.UtilityController::Changedateformat($date, 'd F, Y  h:i A');

                                    // Name assignement based on role
                                    
                                        if (Auth::user()->marketplace_current_role == 1) {
                                            $projects['message_board'][$key]['name'] = ($value['user_role'] == 1 ? Auth::user()->first_name . ' ' . Auth::user()->last_name : $value['user_details']['first_name'] . ' ' . $value['user_details']['last_name']);
                                        }
                                        if (Auth::user()->marketplace_current_role == 2) {
                                            $projects['message_board'][$key]['name'] = ($value['user_role'] == 2 ? Auth::user()->first_name . ' ' . Auth::user()->last_name : $value['user_details']['first_name'] . ' ' . $value['user_details']['last_name']);
                                        }    
                                    
                                    
                                    if (!empty($projects['message_board'][$key]['message_board'])) {
                                        $innerMessageBoard = $projects['message_board'][$key]['message_board'];

                                        foreach ($innerMessageBoard as $keyInner => $valueInner) {
                                            if ($valueInner['type'] == 2) {

                                                $projects['message_board'][$key]['message_board'][$keyInner]['placeholder'] = UtilityController::Imageexist('tiny_' . $valueInner['message'], public_path('') . UtilityController::Getmessage('MSG_FILE_PATH'), url('') . UtilityController::Getmessage('MSG_FILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));
                                                $projects['message_board'][$key]['message_board'][$keyInner]['download_placeholder'] = UtilityController::Imageexist($valueInner['message'], public_path('') . UtilityController::Getmessage('MSG_FILE_PATH'), url('') . UtilityController::Getmessage('MSG_FILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));

                                                $projects['message_board'][$key]['message_board'][$keyInner]['message'] = public_path('') . UtilityController::Getmessage('MSG_FILE_PATH') . $valueInner['message'];
                                            }

                                        }
                                    }

                            }
                            else{
                                $projects['message_board'][$key]=[];
                            } 
                        } else{
                            if($projects['message_board'][$key]['user_id']==Auth::user()->id){
                                $date = Carbon::parse($value['created_at'])->timezone(Auth::user()->timezone);
                                $projects['message_board'][$key]['message']=$value['message'].' at '.UtilityController::Changedateformat($date, 'd F, Y h:i A');
                                $projects['message_board'][$key]['message_in_chinese']=$value['message_in_chinese'].' '.UtilityController::Changedateformat($date, 'd F, Y  h:i A');
                            }
                            else{
                                $projects['message_board'][$key]=[];
                            } 
                                
                        }  
                            
                    }
                }

            }
            $notificationData = SystemNotification::where("receiver_id",Auth::user()->id)->where("module",1)->where('project_slug',$project_slug)->update(["read_unread" => 1]);                                
            $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $projects);
            \DB::commit();
            return $returnData;
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Designerinvitationresponse
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : Designer invitation page
    //In Params : Void
    //Return : json
    //Date : 11th April 2018
    //###############################################################

    public function Designerinvitationresponse(Request $request)
    {
        try {
            \DB::beginTransaction();
            $input         = Input::all();
        
            $mp_project_id = substr($input['slug'], strpos($input['slug'], ":") + 1);
            $mp_project_id = base64_decode($mp_project_id);

            $arraySubmit = array();
            if ($request->status == 1) {
                MarketPlaceProject::where('id', $mp_project_id)->update(array('project_status' => 7)); //status here 7 means that desinger has accepted the invitation but escrow is pending on buyers end.
                $arraySubmit = array('status' => 1, 'invitation_accepted_at' => Carbon::now());

            } else {
                $arraySubmit = array('status' => 2);
            }
            $projects = MarketPlaceProjectsDetailInfo::where('mp_project_id', $mp_project_id)
                ->where('assigned_designer_id', Auth::user()->id)
                ->update($arraySubmit);
            $message = '';

            $projects = MarketPlaceProject::where('id', $mp_project_id)
                ->with([
                    'projectCreator' => function ($query) {
                        $query->get();
                    },
                ])->first()->toArray();

            $projectDetails = MarketPlaceProject::with('selected_designer')->where('id',$mp_project_id)->first();
           
            $sender_id=$projectDetails['selected_designer']['assigned_designer_id'];
            $receiver_id=$projectDetails['project_created_by_id'];
            $slug=$projectDetails['slug'];

            if ($request->status == 1) {
               
                //$projects['project_creator']['email_address']
                $message = 'PROJECT_ACCEPTED';
                $projects['current_language'] = $projects['project_creator']['current_language'];

                $projects['project_number']=$projects['project_number'];

                $projects['subject']=$projects['current_language']==1?'Regarding: Project accepted by seller':'关于：卖方接受的项目';

                $projects['content']=$projects['current_language']==1?'Hello <strong>'.$projects['project_creator']['first_name'].' '.$projects['project_creator']['last_name'].' ,
                    </strong><br/>
                    <br/>
                    Your selected Seller has accepted your Project <strong>ID: '.$projects['project_number'].' </strong> 
                    <br/><br/>Please proceed <a href="'.URL::to('/buyer-project-info/'.$projects['slug']).'">here</a> to make payment. Your Project will start once your payment has been processed.

                    ':'你好 <strong>'.$projects['project_creator']['first_name'].' '.$projects['project_creator']['last_name'].'</strong> ,
                    <br/>
                    <br/>
                    您选择的卖家已接受您的项目  <strong>ID: '.$projects['project_number'].'</strong> ! <br/><br/>请点击<a href="'.URL::to('/buyer-project-info/'.$projects['slug']).'">此处</a>付款。 您的项目将在您的付款处理完成后开始。
                        <br/><br/>';

                $projects['footer_content']=$projects['current_language']==1?'From,<br /> SixClouds Sixteen':'六云十六 ';

                $projects['footer']=$projects['current_language']==1?'SixClouds':'六云';

                Mail::send('emails.email_template', $projects, function ($message) use ($projects) {
                    $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                    $message->to($projects['project_creator']['email_address'])->subject($projects['subject']);
                });
                
                //For system Notification
                $system_messsage_text='Seller accepted invitation for "'.$projectDetails['project_title'].'"';
                $system_message_text_chi='卖方接受了邀请"'.$projectDetails['project_title'].'"';
                
                $data['action_text']                   = UtilityController::Getmessage('SELLER_ACCEPTED_PROJECT_REQUEST');

                //For Message Board
                $message_text = 'Seller accepted invitation for "'.$projectDetails['project_title'].'"';
                $message_text_chi = '卖方接受了邀请"'.$projectDetails['project_title'].'"';
                
            } else {
                //$projects['project_creator']['email_address']
                $projects['current_language'] = $projects['project_creator']['current_language'];
                $projects['project_number']=$projects['project_number'];

                $projects['subject']=$projects['current_language']==1?'Your selected Seller has rejected your Project.':'您选择的卖家拒绝了您的项目。';

                $projects['content']=$projects['current_language']==1?'Hello <strong>'.$projects['project_creator']['first_name'].' '.$projects['project_creator']['last_name'].' ,
                    </strong><br/>
                    <br/>
                    Thank you for posting a Project <strong>ID: '.$projects['project_number'].' </strong> !
                    <br/><br/>Your selected Seller is unable to take on your Project.
                    <br/><br/>Please click <a href="'.URL::to('/suggested-sellers/'.$projects['slug']).'">here</a> to select another Seller.

                    ':'你好 <strong>'.$projects['project_creator']['first_name'].' '.$projects['project_creator']['last_name'].'</strong> ,
                    <br/>
                    <br/>
                    感谢您发布项目  <strong>ID: '.$projects['project_number'].'</strong> ! <br/><br/>您选择的卖家无法执行您的项目。<br/><br/>请点击<a href="'.URL::to('/suggested-sellers/'.$projects['slug']).'">此处</a>选择另一个卖家。
                        <br/><br/>';

                $projects['footer_content']=$projects['current_language']==1?'From,<br /> SixClouds Sixteen':'六云十六 ';

                $projects['footer']=$projects['current_language']==1?'SixClouds':'六云';

                $message = 'PROJECT_REJECTED';
                Mail::send('emails.email_template', $projects, function ($message) use ($projects) {
                    $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                    $message->to($projects['project_creator']['email_address'])->subject($projects['subject']);
                });
                //For system Notification
                $system_messsage_text='Seller rejected invitation for "'.$projectDetails['project_title'].'"';
                $system_message_text_chi='卖方拒绝邀请"'.$projectDetails['project_title'].'"';
                
                $data['action_text']                   = UtilityController::Getmessage('SELLER_REJECTED_PROJECT_REQUEST');

                $message_text = 'Seller rejected invitation for "'.$projectDetails['project_title'].'"';
                $message_text_chi = '卖方拒绝邀请"'.$projectDetails['project_title'].'"';
                
            }

            self::Insertsystemnotifications($sender_id,$receiver_id,$system_messsage_text,$system_message_text_chi,$slug);
            self::Insertmessageboardnotifications($projectDetails['project_created_by_id'],$mp_project_id,$message_text,$message_text_chi,4,2,1);
            $data['mp_project_id']  = $mp_project_id;
            $data['action_type']    = 4;
            $data['show_first']     = 1;
            $actionResult           = UtilityController::Makemodelobject($data, 'MarketplaceAction');
            $actionResult           = UtilityController::Makemodelobject($data, 'MarketPlaceProject', '', $mp_project_id);            
            $returnData = UtilityController::Generateresponse(true, $message, 200, '');
            \DB::commit();
            return $returnData;
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Returnalipayurldetails
    //Author : Nivedita <nivedita@creolestudios.com>
    //Purpose : To store the data that alipay will return after payment
    //In Params : Void
    //Return :
    //Date : 9th May 2018
    //###############################################################
    public function Returnalipayurldetails(Request $request)
    {
        try {
            $input = Input::all();
           
            \DB::beginTransaction();

            if(!empty($input)){
                $outTradeNo = explode('-', $input['out_trade_no']);

                $mpPaymentId          = $outTradeNo[0];
                                
                $updateData                        = array();
                $updateData['alipay_out_trade_no'] = $input['out_trade_no'];
                $updateData['alipay_trade_no']     = $input['trade_no'];
                $updateData['payment_status']      = 1;
                $updateData['alipay_timestamp']    = $input['timestamp'];
                $updateDataResult                  = UtilityController::Makemodelobject($updateData, 'MarketPlacePayment', '', $mpPaymentId);

                self::Afterpaymentprocess($mpPaymentId);

            }
            
            if($updateDataResult){
                \DB::commit();
                return redirect(url('/') . '/buyer-dashboard');
                //$responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$updateDataResult);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
            return response()->json($responseArray);
        }
        
    }

    //###############################################################
    //Function Name : Returnfromcreditpayment
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : To store the data if payment deduct from credits
    //In Params : Void
    //Return :
    //Date : 20th July 2018
    //###############################################################
    public function Returnfromcreditpayment(Request $request)
    {
        try {
            $input = Input::all();

            $mp_payment_id = substr($input['payment_id'], strpos($input['payment_id'], ":") + 1);
            $mp_payment_id = base64_decode($mp_payment_id);

            \DB::beginTransaction();

            if(!empty($input)){
                $updateData                        = array();
                $updateData['payment_status']      = 1;
                $updateDataResult                  = UtilityController::Makemodelobject($updateData, 'MarketPlacePayment', '', $mp_payment_id);

                $paymentData=MarketPlacePayment::where('id',$mp_payment_id)->with('userwalletdetail','projectpaymentdetailinfo')->first();   

                self::Afterpaymentprocess($mp_payment_id);
            }

            if($updateDataResult){
                \DB::commit();

                $responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$updateDataResult);
                return $responseArray;
                //return redirect('buyer-dashboard');
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
            return response()->json($responseArray);
        }
        
    }

    //###############################################################
    //Function Name : Afterpaymentprocess
    //Author : Nivedita <nivedita@creolestudios.com>
    //Purpose : Ater payment process emails should be sent
    //In Params : Void
    //Return :
    //Date : 13th july 2018
    //###############################################################
    public function Afterpaymentprocess($mpPaymentId){

        $projectId = MarketPlacePayment::select('*')->where('id', $mpPaymentId)->with('projectpaymentdetailinfo','userwalletdetail','projectpaymentuserdetailinfo')->get();
        
        $buyerFullName = $projectId[0]['projectpaymentuserdetailinfo']['first_name'] . $projectId[0]['projectpaymentuserdetailinfo']['last_name'];
        $project_number=$projectId[0]['projectpaymentdetailinfo']['project_number'];
        $email_address = $projectId[0]['projectpaymentuserdetailinfo']['email_address'];
        self::Insertadmintimelinenotifications($projectId[0]['projectpaymentuserdetailinfo']['id'],$projectId[0]['mp_project_id'],'Buyer escrowed funds at  ',' 托管资金 ',1,0);

        MarketplaceProject::where('id', $projectId[0]['mp_project_id'])->update(array('project_status' => 1));

        if($projectId[0]['projectpaymentdetailinfo']['project_budget']>=$projectId[0]['userwalletdetail']['wallet_amount']){
            MarketPlaceWallet::where('user_id', $projectId[0]['projectpaymentdetailinfo']['project_created_by_id'])->update(array('wallet_amount' => 0));
        }
        else{
            $wallet_amount=$projectId[0]['userwalletdetail']['wallet_amount']-$projectId[0]['projectpaymentdetailinfo']['project_budget'];
            MarketPlaceWallet::where('user_id', $projectId[0]['projectpaymentdetailinfo']['project_created_by_id'])->update(array('wallet_amount' => $wallet_amount));
        }

        $info['pathToFile']    = self::Receiptattachement($projectId[0]['payment_amount'], $email_address,$projectId[0]['projectpaymentuserdetailinfo']['first_name'],$projectId[0]['projectpaymentuserdetailinfo']['last_name'] , $projectId[0]['alipay_timestamp'], $projectId[0]['system_transaction_number'],$projectId[0]['projectpaymentdetailinfo']['project_title'],$packageExtra,$projectId[0]['projectpaymentuserdetailinfo']['current_language']);
        
        $info['email_address'] = $email_address;
        $info['current_language'] = $projectId[0]['projectpaymentuserdetailinfo']['current_language'];
        
        $info['subject']=$info['current_language']==1?'Thank you for your payment. Enclosed is your project receipt.':'谢谢您的付款。 附件的是您的项目收据。';

        $info['content']=$info['current_language']==1?'Hello <strong>'.$buyerFullName.' ,
            </strong><br/><br/>
            Thank you for your payment. Enclosed is your project receipt.<br/><br/>Project '.$project_number.' work will commence, and you can liaise with your Seller via the Project Page.
            ':'你好 <strong>'.$buyerFullName.'</strong> ,
            <br/><br/>
            项目  <strong>ID: '.$project_number.' </strong> 工作将开始，您可以通过项目页面与您的卖家联系。<br/><br/><br/>';

        $info['footer_content']=$info['current_language']==1?'From,<br /> SixClouds Sixteen':'六云十六 ';

        $info['footer']=$info['current_language']==1?'SixClouds':'六云';

        Mail::send('emails.email_template', $info, function ($message) use ($info) {
            $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
            $message->attach($info['pathToFile']);
            $message->to($info['email_address'])->subject($info['subject']);
        });

        $getDesignerDetails = MarketPlaceProjectsDetailInfo::with('designer_details', 'project_details')->where('mp_project_id', $projectId[0]['mp_project_id'])->first()->toArray();

        $getDesignerDetails['subject']=$getDesignerDetails['designer_details']['current_language']==1?'Begin work for the job assigned to you.':'开始分配给您的工作。';

        $getDesignerDetails['content']=$getDesignerDetails['designer_details']['current_language']==1?'Hello <strong>'.$getDesignerDetails['designer_details']['first_name'].' '.$getDesignerDetails['designer_details']['last_name'].' ,
            </strong><br/>
            <br/>
            The Buyer has made payment for the Project.  <strong>ID: '.$getDesignerDetails['project_details']['project_number'].' </strong> and agreed Project timeline has commenced. You can liaise with the Buyer via the Project Page.  <span><br/><br/><br/>':'你好 <strong>'.$getDesignerDetails['designer_details']['first_name'].$getDesignerDetails['designer_details']['last_name'].'</strong> ,
            <br/>
            <br/>
            买家已经为该项目  <strong>ID: '.$getDesignerDetails['project_details']['project_number'].' </strong> 付款,项目时间表已经开始。您可以在项目页面上与买家联系。<br/><br/><br/>';

        $getDesignerDetails['footer_content']=$getDesignerDetails['designer_details']['current_language']==1?'From,<br /> SixClouds Sixteen':'六云十六 ';

        $getDesignerDetails['footer']=$getDesignerDetails['designer_details']['current_language']==1?'SixClouds':'六云';

        Mail::send('emails.email_template', $getDesignerDetails, function ($message) use ($getDesignerDetails) {
            $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
            $message->to($getDesignerDetails['designer_details']['email_address'])->subject($getDesignerDetails['subject']);
        });

        /* before 2 days */
        $delay2Days      = ($getDesignerDetails['project_details']['project_timeline'] + $getDesignerDetails['project_details']['extended_days']) - UtilityController::Getmessage('EMAIL_TIMELINE_DEADLINE_NOTIFICATION_VALUE_2_days');
        $notify2Designer = (new MpProjectDeadlineNotification($getDesignerDetails['project_details']['id'], $delay2Days, 'EMAIL_TIMELINE_DEADLINE_NOTIFICATION_VALUE_2_days'))->delay(Carbon::now()->addDays($delay2Days));
        dispatch($notify2Designer);

        /* before 1 day */
        $delay1Days      = ($getDesignerDetails['project_details']['project_timeline'] + $getDesignerDetails['project_details']['extended_days']) - UtilityController::Getmessage('EMAIL_TIMELINE_DEADLINE_NOTIFICATION_VALUE_1_days');
        $notify1Designer = (new MpProjectDeadlineNotification($getDesignerDetails['project_details']['id'], $delay1Days, 'EMAIL_TIMELINE_DEADLINE_NOTIFICATION_VALUE_1_days'))->delay(Carbon::now()->addDays($delay1Days));
        dispatch($notify1Designer);

        /* on day */
        $onDay       = ($getDesignerDetails['project_details']['project_timeline'] + $getDesignerDetails['project_details']['extended_days']) - UtilityController::Getmessage('EMAIL_TIMELINE_DEADLINE_NOTIFICATION_VALUE_0_days');
        $notifyOnDay = (new MpProjectDeadlineNotification($getDesignerDetails['project_details']['id'], $onDay, 'EMAIL_TIMELINE_DEADLINE_NOTIFICATION_VALUE_0_days'))->delay(Carbon::now()->addDays($onDay));
        dispatch($notifyOnDay);
        
    }

    //###############################################################
    //Function Name : Receiptattachement
    //Author : Nivedita <nivedita@creolestudios.com>
    //Purpose : generate invoice of the payment
    //In Params : Void
    //Return :
    //Date : 9th May 2018
    //###############################################################
    public function Receiptattachement($totalAmount, $email_address, $first_name,$last_name, $date, $tradeNo,$projectTitle,$serviceExtra,$current_language)
    {
        $serviceExtra=$serviceExtra->toArray();
        $no_of_revisions=[];
        if(!empty($serviceExtra)){
            foreach ($serviceExtra as $key => $value) {
                if (!empty($value['packages'])) {
                    foreach ($value['packages'] as $keyInner => $valueInner) {
                        $no_of_revisions[] = $valueInner['no_of_revisions'];
                    }
                }        
            }
        }

        $totalNoOfRevisions=array_sum($no_of_revisions);
        
        $current_domain = UtilityController::Getmessage('CURRENT_DOMAIN'); 
        if($current_domain=='cn'){
            $priceUnit='¥';
        }else{
            $priceUnit='$S';
        }
        if($current_language==1){
            $title='Your Receipt From SixClouds';
            $invoice='Invoice';
            $date_label='Date';
            $item='Item';
            $price='Price';
            $name=$first_name.' '.$last_name;
        }else{
            $title='收据';
            $invoice='发票';
            $date_label='日期';
            $item='项目';
            $price='价格';
            $name=$first_name.$last_name;
        }
        $date=UtilityController::Changedateformat($date, 'd/m/Y');
        $html='<div class="container" style="font-size:13px; ">
                <div class="row">
                    <div class="col-md-12" align="right">
                        <img src="' . url('/') . '/resources/assets/images/logo.png" height="80" />
                    </div>
                </div>
                <br/>
                <br/>
                <div class="row" style="font-size:16px; ">
                    <div class="col-md-12" align="center">
                        '.$title.'
                    </div>
                </div>
                <hr>
                <br/>
                <div class="row" style="font-size:16px; ">
                    <div class="col-md-6" align="left">
                        <b> '. $name .' </b>
                    </div>
                    <div class="col-md-6" align="right">
                        '.$invoice.' : '.$tradeNo.' 
                    </div>
                    <div class="col-md-12" align="right">
                        '.$date_label.' : '. $date .' 
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="table">
                        <table  width="100%" cellspacing="0" style="border-collapse: separate;border-spacing: 0px 5px !important; ">
                            <tr  class="col-md-12">
                                <th class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:16px; text-align:left;"><b>'.$item.'</b></th>
                                <th class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:16px; text-align:left;"></th>
                                <th class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:16px; text-align:right;"><b>'.$price.'</b></th>
                            </tr>
                        
                            <tbody class="row" style="margin-top:5px;">
                                <tr  class="col-md-12">
                                    <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:13px; text-align:left;"><b> '.$projectTitle.' </b></td>
                                    <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:13px; text-align:left;"></td>
                                    <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:13px; text-align:left;"></td>
                                </tr>';
                                if(!empty($serviceExtra)){
                                    foreach ($serviceExtra as $key => $value) {
                                        
                                        if (!empty($value['packages'])) {
                                            
                                            foreach ($value['packages'] as $keyInner => $valueInner) {
                                               $html.='<tr  class="col-md-12">
                                                    <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:12px; text-align:left;"><b> '.$valueInner['service_title'].' </b></td>
                                                    <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:12px; text-align:left;"></td>
                                                    <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:12px; text-align:right;"><b> '.$priceUnit.' '.$valueInner['price'].' </b></td>
                                               </tr>';
                                            }    
                                        }
                                    }
                                }
                                
                                $html.='<tr><td class="col-md-4" style="background-color: #f5f5f5;text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> Total : '.$priceUnit.' '. $totalAmount.' </td></tr>
                            </tbody> 
                        </table>   
                    </div>

                </div>
            </div>';
           
        $filename   = "payment_receipt_" . $tradeNo . ".pdf";
        $pdf        = PDF::loadHtml($html);
        $finalPath = public_path() . UtilityController::Getpath('MP_RECEIPT_ATTACHMENT_UPLOAD_PATH') . $filename;
        $pdf->save($finalPath);
        return $finalPath;
    }

    //###############################################################
    //Function Name : Paypalpaymentipndata
    //Author : Nivedita <nivedita@creolestudios.com>
    //Purpose : Get the IPN data returned from paypal 
    //In Params : Void
    //Return :
    //Date : 11th july 2018
    //###############################################################
    public function Paypalpaymentipndata(Request $request){

        
        try {
            $data = Input::all();
           
            \DB::beginTransaction();

            if(!empty($data) && isset($data['txn_id'])){
                $getInvoice = explode('_', $data['invoice']);
                $mpPaymentId          = $getInvoice[0];
                $transactionReference = $data['txn_id'];
                if($data['payment_status'] == 'Completed')
                    $paymentStatus = 1;
                else if($data['payment_status'] == 'Failed')
                    $paymentStatus = 3; 

                $updateData                          = array();
                $updateData['transaction_reference'] = $transactionReference;
                $updateData['payment_status']        = $paymentStatus;
                $updateDataResult                    = UtilityController::Makemodelobject($updateData, 'MarketPlacePayment', '', $mpPaymentId);
                self::Afterpaymentprocess($mpPaymentId);
            }

            if($updateDataResult){
                \DB::commit();
                $responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$updateDataResult);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Escrowpaymentdata
    //Author : Nivedita <nivedita@creolestudios.com>
    //Purpose : To add payment data in table
    //In Params : Void
    //Return : json
    //Date : 11th July 2018
    //###############################################################
    public function Escrowpaymentdata(Request $request) {
        try {
            if(Auth::check()){
                $Input = Input::all();

                \DB::beginTransaction();
                //Code to insert Running transaction number starts here
                $Input[''] = "";
                $runningTransactionNumber = UtilityController::GenerateRunningNumber('MarketPlacePayment','system_transaction_number',"MP");                
                $Input['system_transaction_number'] = $runningTransactionNumber;

                $current_domain = UtilityController::Getmessage('CURRENT_DOMAIN'); 
                if($current_domain=='cn'){
                    $Input['payment_method']=1;
                }else{
                    $Input['payment_method']=2;
                }
                $credit_status=0;
                $walletData=MarketPlaceWallet::where('user_id',Auth::user()->id)->first();

                if(!empty($walletData)){
                    if($walletData['wallet_amount']>=$Input['payment_amount']){
                        $credit_status=1;
                        $Input['payment_method']=3;
                    }    
                    else {
                        $Input['payment_amount']=$Input['payment_amount']-$walletData['wallet_amount'];
                    }   
                }

                $result = UtilityController::Makemodelobject($Input, 'MarketPlacePayment');

                $name = Auth::user()->first_name.' '.Auth::user()->last_name;
                $slug['slug'] = str_slug($name, '-');
                $slug['slug'] = $slug['slug'].':'.base64_encode($result['id']);                
                $resultUpdate = UtilityController::Makemodelobject($slug, 'MarketPlacePayment','',$result['id']);
                $resultUpdate['credit_status']=$credit_status;
                
                if($result){
                    \DB::commit();
                    $responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$resultUpdate);
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Paymentinfodata
    //Author : Nivedita Mitra <nivedita@creolestudios.com>
    //Purpose : To get Information of the payment data to send to paypal/alipay form submit
    //In Params : Void
    //Return : json
    //Date : 11th july 2018
    //###############################################################
    public function Paymentinfodata(Request $request)
    {
        try {
            \DB::beginTransaction();
            $input        = Input::all();
            $paymentsId   = substr($input['payment_id'], strpos($input['payment_id'], ":") + 1);
            $paymentsId   = base64_decode($paymentsId);
            
            $paymentsData = MarketPlacePayment::where('id', $paymentsId)->with('projectpaymentdetailinfo','projectpaymentuserdetailinfo')->get();
            
            $paymentsData['domain'] = UtilityController::Getmessage('CURRENT_DOMAIN'); 
            $paymentsData['invoice'] = $paymentsData[0]['id'].'_'.rand();

            if ($paymentsData) { 
                \DB::commit();
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $paymentsData);
                return $returnData;
            }
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
    //###############################################################
    //Function Name : Portfoliocategory
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To get the categories for new portfolio upload
    //In Params : Void
    //Return : json
    //Date : 11th April 2018
    //###############################################################
    public function Portfoliocategory(Request $request)
    {
        try {
            $Input=Input::all();
            $categotyData = SkillSet::select('id', 'skill_name', 'skill_name_chinese')->where('status', 1)->get();
            if ($categotyData) {
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $categotyData);
            } else {
                $returnMessage=$Input['language']=='en'?'GENERAL_ERROR':'GENERAL_ERROR_CHINESE';
                $returnData = UtilityController::Generateresponse(false,$returnMessage , 400, '');
            }
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnMessage=$Input['language']=='en'?'GENERAL_ERROR':'GENERAL_ERROR_CHINESE';
            $returnData        = UtilityController::Generateresponse(false, $returnMessage, '', '');
        }
        return $returnData;
    }

    //###############################################################
    //Function Name : Portfolio
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To upload new portfolio
    //In Params : Void
    //Return : json
    //Date : 11th April 2018
    //###############################################################
    public function Portfolio(Request $request)
    {
        try {
            if (Auth::check()) {
                \DB::beginTransaction();
                $Input = Input::all();                                
                if (!array_key_exists("attachments", $Input) || !array_key_exists("skills", $Input)) {
                    if (!array_key_exists("attachments", $Input) && array_key_exists("skills", $Input)) {
                        $returnMessage = ($Input['language']=='en'?'SELECT_ATLEAST_ONE_IMAGE':'SELECT_ATLEAST_ONE_IMAGE_CHINESE');
                        throw new \Exception(UtilityController::Getmessage($returnMessage));
                    } else if (!array_key_exists("skills", $Input) && array_key_exists("attachments", $Input)) {
                        $returnMessage=$Input['language']=='en'?'SELECT_ATLEAST_ONE_CATEGORY':'SELECT_ATLEAST_ONE_CATEGORY_CHINESE';
                        throw new \Exception(UtilityController::Getmessage($returnMessage));
                    } else {
                        $returnMessage=$Input['language']=='en'?'SELECT_IMAGE_CATEGORY':'SELECT_IMAGE_CATEGORY_CHINESE';
                        throw new \Exception(UtilityController::Getmessage($returnMessage));
                    }
                } else {
                    if ($Input['portfolio_type'] == 1) {
                        $Input['project_details'] = json_decode($Input['project_details'], true);
                        $Input['upload_title']    = $Input['upload_title'];
                        $Input['mp_project_id']   = $Input['project_details']['id'];
                    }
                    $removeFiles = explode(',', $Input['remove_attachments']);
                    $filesAttachments = explode(',', $Input['file_attachments']);                    
                    if ((count($Input['attachments']) == count($removeFiles)) && $removeFiles[0] != '') {
                        $returnMessage = ($Input['language']=='en'?'SELECT_ATLEAST_ONE_IMAGE':'SELECT_ATLEAST_ONE_IMAGE_CHINESE');
                        throw new \Exception(UtilityController::Getmessage($returnMessage));
                    } else {
                        foreach ($filesAttachments as $key => $value) {
                            foreach ($Input['attachments'] as $keyInner => $valueInner) {
                                if ($value == $valueInner->getClientOriginalName()) {
                                    $attachments[]=$Input['attachments'][$keyInner];
                                }                                
                            }
                        }
                        $dummyFileName='';
                        foreach ($attachments as $key => $value) {                            
                            if($value->getClientOriginalName()!=$dummyFileName){
                                $attachments_final[]=$attachments[$key];
                            }
                            $dummyFileName=$value->getClientOriginalName();
                        }                        
                        $Input['attachments']=$attachments_final;                        
                        if (array_key_exists("attachments", $Input) && array_key_exists("cover_image", $Input)) {
                            $fileCountAllowed = 4;
                        } else {
                            $fileCountAllowed = 5;
                        }                        
                        $Input['uploaded_user_id'] = Auth::user()->id;
                        $validator                 = UtilityController::ValidationRules($Input, 'MarketPlaceUpload');
                        if (!$validator['status']) {
                            $errorMessage  = explode('.', $validator['message']);
                            $responseArray = UtilityController::Generateresponse(false, $errorMessage['0'], 0);
                        } else {
                            // Check for file size
                            $sum = 0;
                            foreach ($Input['attachments'] as $key => $value) {
                                $sum = $sum + $value->getClientSize();
                                if ($sum > UtilityController::Getmessage('MAX_UPLOAD_FILE_SIZE')) {
                                    $returnMessage=$Input['language']=='en'?'ALL_FILES_UPTO_MB':'ALL_FILES_UPTO_MB_CHINESE';
                                    throw new \Exception(UtilityController::Getmessage($returnMessage));
                                }
                            }
                            $resultUpload = UtilityController::Makemodelobject($Input, 'MarketPlaceUpload');
                            if ($resultUpload) {
                                if (array_key_exists('skills', $Input)) {
                                    $skillSets = $Input['skills'];
                                    foreach ($skillSets as $key => $value) {
                                        $portfolioCategory[$key]['mp_upload_id'] = $resultUpload['id'];
                                        $portfolioCategory[$key]['categoty_id']  = $value;
                                        $portfolioCategory[$key]['created_at']   = Carbon::now();
                                        $portfolioCategory[$key]['updated_at']   = Carbon::now();
                                    }
                                    MarketPlacePortfolioCategoryDetail::insert($portfolioCategory);
                                } else {
                                    $errorMessage  = UtilityController::Getmessage('SELECT_ATLEAST_ONE_CATEGORY');
                                    $responseArray = UtilityController::Generateresponse(false, $errorMessage, 0);
                                    return response()->json($responseArray);
                                }
                                $increment = 0;
                                foreach ($Input['attachments'] as $key => $value) {
                                    $forCover = $key;
                                    $fileName  = date("dmYHis") . rand();
                                    $extension = \File::extension($value->getClientOriginalName());                                        
                                    if (in_array($extension, array('jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG'))) {
                                        $value->move(public_path('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_UPLOAD_PATH'), $fileName);
                                        chmod(public_path('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_UPLOAD_PATH') . $fileName, 0777);
                                        $path = public_path('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_UPLOAD_PATH') . $fileName;
                                    } elseif(in_array($extension, array("mov", "mp4", "3gp", "ogg", "gif"))){                                            
                                        $fileName = $fileName.".".$extension;                                            
                                        $value->move(public_path('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_ORIGINAL_PATH'), $fileName);
                                        chmod(public_path('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_ORIGINAL_PATH') . $fileName, 0777);
                                        $path = public_path('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_ORIGINAL_PATH') . $fileName;
                                        $uploadAttachment[$key]['original_file_name'] = $fileName;
                                        $uploadAttachment[$key]['medium_thumbnail'] = "";
                                        $uploadAttachment[$key]['tiny_thumbnail'] = "";
                                        $uploadAttachment[$key]['attachment_type'] = 2;
                                        $uploadAttachment[$key]['created_at'] = Carbon::now();
                                        $uploadAttachment[$key]['updated_at'] = Carbon::now();
                                        $uploadAttachment[$key]['mp_upload_id'] = $resultUpload['id'];
                                    } else {
                                        $returnMessage=$Input['language']=='en'?'ONLY_JPG_PNG_JPEG_FOR_INTERNAL_IMAGES':'ONLY_JPG_PNG_JPEG_FOR_INTERNAL_IMAGES_CHINESE';
                                        throw new \Exception(UtilityController::Getmessage($returnMessage));                                             
                                    }
                                    $uploadAttachment[$key]['mp_upload_id'] = $resultUpload['id'];
                                    if(in_array($extension, array('jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG'))){                                           
                                        ## original image start
                                        $originalThumbnail = "original_" . $fileName;
                                        $result            = self::Resizeimage($value, 1920, 1920, $originalThumbnail, $path, 'MP_PORTFOLIO_ATTACHMENT_ORIGINAL_PATH');
                                        if ($result) {
                                            $uploadAttachment[$key]['original_file_name'] = $originalThumbnail;
                                        }
                                        $uploadAttachment[$key]['attachment_type'] = 1;
                                        ## original image end

                                        ## medium image start
                                        $mediumThumbnail = "medium_" . $fileName;
                                        $result          = self::Resizeimage($value, 1280, 720, $mediumThumbnail, $path, 'MP_PORTFOLIO_ATTACHMENT_MEDIUM_PATH');
                                        if ($result) {
                                            $uploadAttachment[$key]['medium_thumbnail'] = $mediumThumbnail;
                                        }
                                        ## medium image end

                                        ## tiny image start
                                        $tinyThumbnail = "tiny_" . $fileName;
                                        $result        = self::Resizeimage($value, 33, 33, $tinyThumbnail, $path, 'MP_PORTFOLIO_ATTACHMENT_TINY_PATH', 1);
                                        if ($result) {
                                            $uploadAttachment[$key]['tiny_thumbnail'] = $tinyThumbnail;
                                        }
                                        ## tiny image end
                                        $uploadAttachment[$key]['created_at'] = Carbon::now();
                                        $uploadAttachment[$key]['updated_at'] = Carbon::now();
                                         if ($increment == 0) {
                                       
                                    }
                                    $increment++;
                                    unlink($path);
                                    }                                        
                                }                                    
                                MarketPlaceUploadAttachment::insert($uploadAttachment);
                                if (!array_key_exists("cover_image", $Input)) {
                                    ## cover image if not uploaded by user start
                                    $coverImg = "coverImg_" . $fileName;
                                    $result   = self::Resizeimage($Input['attachments'][$forCover], 1280, 720, $coverImg, $path, 'MP_PORTFOLIO_ATTACHMENT_COVER_IMAGE_PATH');
                                    if ($result) {
                                        $Input['cover_img'] = $coverImg;
                                    }
                                    ## cover image if not uploaded by user end
                                } else {                                                                                              
                                    $CoverfileName = date("dmYHis");
                                    $extension     = \File::extension($Input['cover_image']->getClientOriginalName());
                                    if (in_array($extension, array('jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG'))) {
                                        $Input['cover_image']->move(public_path('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_UPLOAD_PATH'), $CoverfileName);
                                        chmod(public_path('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_UPLOAD_PATH') . $CoverfileName, 0777);
                                        $coverImagePath = public_path('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_UPLOAD_PATH') . $CoverfileName;
                                        ## cover image if uploaded by user start
                                        $coverImg = "coverImg_" . $CoverfileName;
                                        $result   = self::Resizeimage($Input['cover_image'], 1280, 720, $coverImg, $coverImagePath, 'MP_PORTFOLIO_ATTACHMENT_COVER_IMAGE_PATH');
                                        if ($result) {
                                            $Input['cover_img'] = $coverImg;
                                            unlink($coverImagePath);
                                        }
                                        ## cover image if uploaded by user end
                                    } else {
                                        $returnMessage=$Input['language']=='en'?'ONLY_JPG_PNG_JPEG_FOR_INTERNAL_IMAGES':'ONLY_JPG_PNG_JPEG_FOR_INTERNAL_IMAGES_CHINESE';
                                        throw new \Exception(UtilityController::Getmessage($returnMessage));
                                    }                                                
                                }
                                $slug                        = array();
                                $slug['slug']                = str_slug($Input['upload_title'], '-');
                                $slug['slug']                = $slug['slug'] . ":" . base64_encode($resultUpload['id']);
                                $slug['cover_img']           = $coverImg;
                                $updateSlug                  = UtilityController::Makemodelobject($slug, 'MarketPlaceUpload', '', $resultUpload['id']);
                                if ($Input['portfolio_type'] == 1) {
                                    $updateProjectStatus['allow_for_portfolio'] = 3;
                                    $updatestatus                               = UtilityController::Makemodelobject($updateProjectStatus, 'MarketPlaceProject', '', $Input['mp_project_id']);
                                }
                                \DB::commit();
                            }
                        }
                        $returnMessage = ($Input['language'] == 'en'?'PORTFOLIO_UPLOADED':'PORTFOLIO_UPLOADED_CHINESE');
                        $responseArray = UtilityController::Generateresponse(true, $returnMessage, 1);                        
                    }
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, $e->getMessage(), 0);
        }
        return $responseArray;
    }

    //###############################################################
    //Function Name : Editportfolio
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To edit the current portfolio
    //In Params : Void
    //Return : json
    //Date : 26th April 2018
    //###############################################################
    public function Editportfolio(Request $request)
    {
        try {
            if (Auth::check()) {
                \DB::beginTransaction();
                $Input                     = Input::all();                
                $Input['portfolioInfo']    = json_decode($Input['portfolioInfo'], true);
                $Input['uploaded_user_id'] = Auth::user()->id;
                $Input['upload_of']        = 3;
                $validator                 = UtilityController::ValidationRules($Input, 'MarketPlaceUpload');
                if (!$validator['status']) {
                    $errorMessage  = explode('.', $validator['message']);
                    $responseArray = UtilityController::Generateresponse(false, $errorMessage['0'], 0);
                } else {
                    if (array_key_exists('skills', $Input)) {
                        $removeOldSkill = MarketPlacePortfolioCategoryDetail::where('mp_upload_id', $Input['portfolioInfo']['id'])->forceDelete();
                        $skillSets      = array_unique($Input['skills']);
                        foreach ($skillSets as $key => $value) {
                            $portfolioCategory[$key]['mp_upload_id'] = $Input['portfolioInfo']['id'];
                            $portfolioCategory[$key]['categoty_id']  = $value;
                            $portfolioCategory[$key]['created_at']   = Carbon::now();
                            $portfolioCategory[$key]['updated_at']   = Carbon::now();
                        }
                        MarketPlacePortfolioCategoryDetail::insert($portfolioCategory);
                    } else {
                        $returnMessage=$Input['language']=='en'?'SELECT_ATLEAST_ONE_CATEGORY':'SELECT_ATLEAST_ONE_CATEGORY_CHINESE';
                        $errorMessage  = UtilityController::Getmessage($returnMessage);
                        $responseArray = UtilityController::Generateresponse(false, $errorMessage, 0);
                        return response()->json($responseArray);
                    }
                    if (array_key_exists('attachments', $Input)) {
                        foreach ($Input['attachments'] as $key => $value) {
                            if ($value->getClientSize() <= UtilityController::Getmessage('FILE_SIZE_2')) {
                                $fileName  = date("dmYHis") . rand();
                                $extension = \File::extension($value->getClientOriginalName());
                                if (in_array($extension, array('jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG'))) {
                                    $value->move(public_path('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_UPLOAD_PATH'), $fileName);
                                    chmod(public_path('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_UPLOAD_PATH') . $fileName, 0777);
                                    $path = public_path('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_UPLOAD_PATH') . $fileName;
                                } elseif(in_array($extension, array("mov", "mp4", "3gp", "ogg", "gif"))){                                            
                                    $fileName = $fileName.".".$extension;                                            
                                    $value->move(public_path('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_ORIGINAL_PATH'), $fileName);
                                    chmod(public_path('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_ORIGINAL_PATH') . $fileName, 0777);
                                    $path = public_path('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_ORIGINAL_PATH') . $fileName;
                                    $uploadAttachment[$key]['original_file_name'] = $fileName;
                                    $uploadAttachment[$key]['medium_thumbnail'] = "";
                                    $uploadAttachment[$key]['tiny_thumbnail'] = "";
                                    $uploadAttachment[$key]['attachment_type'] = 2;
                                    $uploadAttachment[$key]['created_at'] = Carbon::now();
                                    $uploadAttachment[$key]['updated_at'] = Carbon::now();
                                    $uploadAttachment[$key]['mp_upload_id'] = $Input['portfolioInfo']['id'];
                                } else {
                                    $returnMessage=$Input['language']=='en'?'ONLY_JPG_PNG_JPEG_FOR_INTERNAL_IMAGES':
                                    'ONLY_JPG_PNG_JPEG_FOR_INTERNAL_IMAGES_CHINESE';
                                    throw new \Exception(UtilityController::Getmessage($returnMessage));
                                }
                                $uploadAttachment[$key]['mp_upload_id'] = $Input['portfolioInfo']['id'];
                                if(in_array($extension, array('jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG'))){
                                    ## original image start
                                    $originalThumbnail = "original_" . $fileName;
                                    $result            = self::Resizeimage($value, 400, 400, $originalThumbnail, $path, 'MP_PORTFOLIO_ATTACHMENT_ORIGINAL_PATH');
                                    if ($result) {
                                        $uploadAttachment[$key]['original_file_name'] = $originalThumbnail;
                                    }
                                    ## original image end

                                    ## medium image start
                                    $mediumThumbnail = "medium_" . $fileName;
                                    $result          = self::Resizeimage($value, 180, 180, $mediumThumbnail, $path, 'MP_PORTFOLIO_ATTACHMENT_MEDIUM_PATH');
                                    if ($result) {
                                        $uploadAttachment[$key]['medium_thumbnail'] = $mediumThumbnail;
                                    }
                                    ## medium image end

                                    ## tiny image start
                                    $tinyThumbnail = "tiny_" . $fileName;
                                    $result        = self::Resizeimage($value, 33, 33, $tinyThumbnail, $path, 'MP_PORTFOLIO_ATTACHMENT_TINY_PATH', 1);
                                    if ($result) {
                                        $uploadAttachment[$key]['tiny_thumbnail'] = $tinyThumbnail;
                                    }
                                    ## tiny image end
                                    $uploadAttachment[$key]['created_at'] = Carbon::now();
                                    $uploadAttachment[$key]['updated_at'] = Carbon::now();
                                    $uploadAttachment[$key]['attachment_type'] = "1";
                                    unlink($path);
                                }                               
                            } else {
                                $returnMessage=$Input['language']=='en'?'FILE_SIZE_LARGE_2_FOR_PORTFOLIO':'FILE_SIZE_LARGE_2_FOR_PORTFOLIO_CHINESE';
                                throw new \Exception($value->getClientOriginalName() . UtilityController::Getmessage($returnMessage));
                            }                            
                        }                       
                        MarketPlaceUploadAttachment::insert($uploadAttachment);
                    }
                    if (!array_key_exists("cover_image", $Input)) {
                        $edit['cover_img'] = $Input['portfolioInfo']['cover_img'];
                    } else {
                        if ($Input['cover_image']->getClientSize() <= UtilityController::Getmessage('FILE_SIZE_2')) {
                            $CoverfileName = date("dmYHis");
                            $extension     = \File::extension($Input['cover_image']->getClientOriginalName());
                            if (in_array($extension, array('jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG'))) {
                                $Input['cover_image']->move(public_path('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_UPLOAD_PATH'), $CoverfileName);
                                chmod(public_path('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_UPLOAD_PATH') . $CoverfileName, 0777);
                                $coverImagePath = public_path('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_UPLOAD_PATH') . $CoverfileName;
                                ## cover image if uploaded by user start
                                $coverImg = "coverImg_" . $CoverfileName;
                                $result   = self::Resizeimage($Input['cover_image'], 220, 220, $coverImg, $coverImagePath, 'MP_PORTFOLIO_ATTACHMENT_COVER_IMAGE_PATH');
                                if ($result) {
                                    $edit['cover_img'] = $coverImg;
                                }
                                ## cover image if uploaded by user end
                            } else {
                                $returnMessage=$Input['language']=='en'?'ONLY_JPG_PNG_JPEG_FOR_INTERNAL_IMAGES':
                                    'ONLY_JPG_PNG_JPEG_FOR_INTERNAL_IMAGES_CHINESE';
                                throw new \Exception(UtilityController::Getmessage($returnMessage));
                            }
                        } else {
                            $returnMessage=$Input['language']=='en'?'FILE_SIZE_LARGE_2_FOR_PORTFOLIO':'FILE_SIZE_LARGE_2_FOR_PORTFOLIO_CHINESE';
                            throw new \Exception($Input['cover_image']->getClientOriginalName() . UtilityController::Getmessage($returnMessage));
                        }
                    }
                    $edit['upload_title']       = $Input['upload_title'];
                    $edit['upload_description'] = $Input['upload_description'];
                    $edit['slug']               = str_slug($Input['upload_title'], '-');
                   
                    $edit['slug']               = $edit['slug'] . ":" . base64_encode($Input['portfolioInfo']['id']);
                    $updateSlug                 = UtilityController::Makemodelobject($edit, 'MarketPlaceUpload', '', $Input['portfolioInfo']['id']);
                    if (isset($coverImagePath)) {
                        unlink($coverImagePath);
                    }

                    \DB::commit();
                    // if(isset($path))
                    //     unlink($path);
                     $returnMessage=$Input['language']=='en'?'PORTFOLIO_EDITED':'PORTFOLIO_EDITED_CHINESE';
                $responseArray = UtilityController::Generateresponse(true,$returnMessage, 1);
                }               

            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, $e->getMessage(), 0);
        }
        return $responseArray;
    }

    //###############################################################
    //Function Name : Resizeimage
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To resize the given iamge to given pixels for new portfolio upload
    //In Params : Void
    //Return : json
    //Date : 12th April 2018
    //###############################################################
    public function Resizeimage($value, $width, $height, $fileName, $path, $storagePath, $tiny = "")
    {
        $destinationPath = public_path('') . UtilityController::Getpath($storagePath);
        if ($tiny != 1) {
            $testimg = Image::make($path)->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save($destinationPath . $fileName);
        }

        if ($tiny == 1) {
            $testimg = Image::make($path)->resize($width, $height, function ($constraint) {
                $constraint->upsize();
            })->save($destinationPath . $fileName);
        }

        chmod(public_path('') . UtilityController::Getpath($storagePath) . $fileName, 0777);
        $bucket    = UtilityController::Getmessage('BUCKET');
        $object    = UtilityController::Getpath('BUCKET_MP_PORTFOLIO_FOLDER') . $fileName;
        $filePath  = public_path('') . UtilityController::Getpath($storagePath) . $fileName;
        $ossClient = new OssClient(UtilityController::Getmessage('ACCESS_KEY_ID'), UtilityController::Getmessage('ACCESS_KEY_SECRET'), UtilityController::Getmessage('END_POINT'));
        try {
            // $ossClient->uploadFile($bucket, $object, $filePath);
            $result = true;
        } catch (OssException $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false,'GENERAL_ERROR', 0);
            return $responseArray;
        }
        return $result;
    }

    //###############################################################
    //Function Name : Getportfolio
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To get all the uploaded portfolio(s) by designer
    //In Params : Void
    //Return : json
    //Date : 12th April 2018
    //###############################################################
    public function Getportfolio(Request $request)
    {
        try {
            if (Auth::check()) {
                $Input = Input::all();
                if (isset($Input['id']) && $Input['id'] != '') {
                    $id         = substr($Input['id'], strpos($Input['id'], ":") + 1);
                    $designerId = base64_decode($id);
                } else {
                    $designerId = Auth::user()->id;
                }
                /*$activePortfolio = MarketPlaceUpload::with('attachments')->where('upload_of', 3)->where('uploaded_user_id', $designerId)->where('status', 1)->orderby('id', 'DESC')->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))->toArray();   */
                $activePortfolio = MarketPlaceUpload::with('attachments', 'uploadedby')->where('upload_of', 3)->where('uploaded_user_id', $designerId)->where('status', 1)->orderby('id', 'DESC');

                if (isset($Input['id']) && $Input['id'] != '') {
                    $activePortfolio = $activePortfolio->paginate(Config('constants.other.MP_DESIGNER_PROFILE_PAGINATE'))->toArray();
                } else {
                    $activePortfolio = $activePortfolio->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))->toArray();
                }

                if (Auth::user()->total_avg_rate >= 3.5 && Auth::user()->total_avg_rate <= 4.19) {
                    $activePortfolio['available_slots'] = 10;
                } elseif (Auth::user()->total_avg_rate >= 4.2 && Auth::user()->total_avg_rate <= 4.49) {
                    $activePortfolio['available_slots'] = 26;
                } elseif (Auth::user()->total_avg_rate >= 4.5) {
                    $activePortfolio['available_slots'] = 50;
                } else {
                    $activePortfolio['available_slots'] = 6;
                }

                $existingPortfolioCount = MarketPlaceUpload::where('uploaded_user_id', $designerId)->where('upload_of',3)->where('status', 1)->count();

                $activePortfolio['available_slots'] = ($activePortfolio['available_slots'] - $existingPortfolioCount < 0 ? 0 : $activePortfolio['available_slots'] - $existingPortfolioCount);
                if (!empty($activePortfolio) && !empty($activePortfolio['data'])) {

                    foreach ($activePortfolio['data'] as $key => $value) {
                        $activePortfolio['data'][$key]['cover_image'] = UtilityController::Imageexist($value['cover_img'], public_path('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_COVER_IMAGE_PATH'), url('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_COVER_IMAGE_URL'), url('') . UtilityController::Getmessage('NO_IMAGE_URL'));

                        foreach ($value['attachments'] as $keyInner => $valueInner) {

                            $activePortfolio['data'][$key]['attachments'][$keyInner]['cover_image'] = UtilityController::Imageexist($value['cover_img'], public_path('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_COVER_IMAGE_PATH'), url('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_COVER_IMAGE_URL'), url('') . UtilityController::Getmessage('NO_IMAGE_URL'));

                            $tinyImages[] = UtilityController::Imageexist($valueInner['tiny_thumbnail'], public_path('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_TINY_PATH'), url('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_TINY_URL'));
                        }
                        if (!empty($tinyImages)) {
                            $activePortfolio['data'][$key]['tiny_images']             = $tinyImages;
                            $activePortfolio['data'][$key]['total_tiny_images']       = count($tinyImages);
                            $activePortfolio['data'][$key]['total_tiny_images_count'] = $activePortfolio['data'][$key]['total_tiny_images'] - 3;
                            $tinyImages                                               = [];
                        }

                    }

                    $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $activePortfolio);

                } else {
                    //$returnMessage=$Input['language']=='en'?'GENERAL_ERROR':'GENERAL_ERROR_CHINESE';
                    $responseArray = UtilityController::Generateresponse(false,'GENERAL_ERROR' , 0, $activePortfolio);
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false,'GENERAL_ERROR', 0);
        }
        return $responseArray;
    }

    //###############################################################
    //Function Name : Portfolioinfo
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To get all the detail of uploaded portfolio(s) by designer
    //In Params : Void
    //Return : json
    //Date : 13th April 2018
    //###############################################################
    public function Portfolioinfo(Request $request)
    {
        try {
            if (Auth::check()) {
                $Input = Input::all();

                if (isset($Input['designer_id'])) {
                    $designerId = substr($Input['designer_id'], strpos($Input['designer_id'], ":") + 1);
                    $designerId = base64_decode($designerId);
                } else {
                    $designerId = Auth::user()->id;
                }

                $mp_portfolio_id = substr($Input['mp_portfolio_id'], strpos($Input['mp_portfolio_id'], ":") + 1);
                $mp_portfolio_id = base64_decode($mp_portfolio_id);
                $activePortfolio = MarketPlaceUpload::with('attachments', 'portfolio_categories', 'uploadedMpDesignBy')->where('upload_of', 3)->where('uploaded_user_id', $designerId)->where('status', 1)->where('slug', $Input['mp_portfolio_id'])->first();

                $activePortfolio['is_designer']              = Auth::user()->is_designer;
                $activePortfolio['marketplace_current_role'] = Auth::user()->marketplace_current_role;

                if (!empty($activePortfolio)) {
                    if ($activePortfolio['portfolio_type'] == 1) {
                        $activePortfolio['rating'] = MarketPlaceDesignerRating::where('mp_project_id', $activePortfolio['mp_project_id'])->first();
                        if (!empty($activePortfolio['rating'])) {
                            $activePortfolio['rating']['matrix_rating'] = json_decode($activePortfolio['rating']['matrix_rating'], true);
                            foreach ($activePortfolio['rating']['matrix_rating'] as $key => $value) {
                                if ($key == 'quality_rating') {
                                    $rating_array[0]['key']   = 'Quality';
                                    $rating_array[0]['value'] = $value;
                                }
                                if ($key == 'buyers_requirement_rating') {
                                    $rating_array[4]['key']   = 'Comprehension Skills';
                                    $rating_array[4]['value'] = $value;
                                }
                                if ($key == 'responsiveness_rating') {
                                    $rating_array[2]['key']   = 'Responsiveness';
                                    $rating_array[2]['value'] = $value;
                                }
                                if ($key == 'creativity_rating') {
                                    $rating_array[1]['key']   = 'Creativity';
                                    $rating_array[1]['value'] = $value;
                                }
                                if ($key == 'timeliness_rating') {
                                    $rating_array[3]['key']   = 'Timeliness';
                                    $rating_array[3]['value'] = $value;
                                }
                            }
                            $activePortfolio['rating']['matrix_rating'] = $rating_array;
                        }
                        $activePortfolio['review'] = MarketPlaceDesignerReview::where('mp_project_id', $activePortfolio['mp_project_id'])->first();
                    }

                    if (Auth::user()->id != $activePortfolio['uploaded_user_id']) {
                        $activePortfolio['image'] = UtilityController::Imageexist($activePortfolio['uploaded_mp_design_by']['image'], public_path('') . UtilityController::Getmessage('ACCESSOR_IMAGE_PATH'), url('') . UtilityController::Getmessage('ACCESSOR_IMAGE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));

                        $activePortfolio['display_designer'] = 1;
                    } else {
                        $activePortfolio['display_designer'] = 0;
                    }

                    $activePortfolio['cover_image'] = UtilityController::Imageexist($activePortfolio['cover_img'], public_path('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_COVER_IMAGE_PATH'), url('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_COVER_IMAGE_URL'), url('') . UtilityController::Getmessage('NO_IMAGE_URL'));
                    foreach ($activePortfolio['attachments'] as $keyInner => $valueInner) {
                        if($valueInner['attachment_type'] == 2){
                            $mediumImages[]   = array('id' => $valueInner['id'], 'type' => $valueInner['attachment_type'], 'medium_path' => UtilityController::Imageexist($valueInner['original_file_name'], public_path('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_ORIGINAL_PATH'), url('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_ORIGINAL_URL')));
                        }else{
                            $mediumImages[]   = array('id' => $valueInner['id'], 'type' => $valueInner['attachment_type'], 'medium_path' => UtilityController::Imageexist($valueInner['medium_thumbnail'], public_path('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_MEDIUM_PATH'), url('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_MEDIUM_URL')));
                        }                        
                        $originalImages[] = array('id' => $valueInner['id'], 'type' => $valueInner['attachment_type'], 'original_path' => UtilityController::Imageexist($valueInner['original_file_name'], public_path('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_ORIGINAL_PATH'), url('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_ORIGINAL_URL')));
                    }
                    foreach ($activePortfolio['portfolio_categories'] as $key => $value) {
                        $selectedCategotyId[] = $value['categoty_id'];
                    }
                    if(array_key_exists('detailPage', $Input)){
                        $coverImageArray['id'] = $activePortfolio['id'];
                        $coverImageArray['type'] = 1;
                        $coverImageArray['medium_path'] = $activePortfolio['cover_image'];
                        $coverImageArray['original_path'] = $activePortfolio['cover_image'];
                        array_unshift($mediumImages, $coverImageArray);
                        array_unshift($originalImages, $coverImageArray);
                    }
                    $activePortfolio['medium_images']     = $mediumImages;
                    $activePortfolio['original_images']   = $originalImages;
                    $activePortfolio['categoty_selected'] = $selectedCategotyId;
                    $activePortfolio['next']              = MarketPlaceUpload::select('id', 'slug')->where('upload_of', 3)->where('uploaded_user_id', $designerId)->where('status', 1)->where('id', '>', $mp_portfolio_id)->orderby('id', 'ASC')->first();
                    $activePortfolio['previous']          = MarketPlaceUpload::select('id', 'slug')->where('upload_of', 3)->where('uploaded_user_id', $designerId)->where('status', 1)->where('id', '<', $mp_portfolio_id)->orderby('id', 'DESC')->first();
                    $responseArray                        = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $activePortfolio);
                } else {
                    $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 0);
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
        }
        return $responseArray;
    }

    //###############################################################
    //Function Name : Deleteportfolio
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To delete uploaded portfolio(s) by designer
    //In Params : Void
    //Return : json
    //Date : 13th April 2018
    //###############################################################
    public function Deleteportfolio(Request $request)
    {
        try {
            if (Auth::check()) {
                \DB::beginTransaction();
                $Input           = Input::all();
                $designerId      = Auth::user()->id;
                $mp_portfolio_id = substr($Input['mp_portfolio_id'], strpos($Input['mp_portfolio_id'], ":") + 1);
                $mp_portfolio_id = base64_decode($mp_portfolio_id);
                $data['status']  = 2;
                $result          = UtilityController::Makemodelobject($data, 'MarketPlaceUpload', '', $mp_portfolio_id);
                if ($result) {
                    \DB::commit();
                    $returnMessage=$Input['language']=='en'? 'PORTFOLIO_DELETED':'PORTFOLIO_DELETED_CHINESE';
                    $responseArray = UtilityController::Generateresponse(true, $returnMessage, 1);
                } else {
                    \DB::rollback();
                    $returnMessage=$Input['language']=='en'?'GENERAL_ERROR':'GENERAL_ERROR_CHINESE';
                    $responseArray = UtilityController::Generateresponse(false, $returnMessage, 0);
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $returnMessage=$Input['language']=='en'?'GENERAL_ERROR':'GENERAL_ERROR_CHINESE';
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, $returnMessage, 0);
        }
        return $responseArray;
    }

    //###############################################################
    //Function Name : Deleteportfolioimage
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To delete images in uploaded portfolio(s) by designer
    //In Params : Void
    //Return : json
    //Date : 26th April 2018
    //###############################################################
    public function Deleteportfolioimage(Request $request)
    {
        try {
            if (Auth::check()) {
                \DB::beginTransaction();
                $Input     = Input::all();
                $portfolio = MarketPlaceUploadAttachment::where('mp_upload_id', $Input['project_id'])->where('status', 1)->get();
                $portfolio = $portfolio->toArray();
                $count     = count($portfolio);
                if ($count > 1) {
                    $imageID        = $Input['image_id'];
                    $data['status'] = 2;
                    $result         = UtilityController::Makemodelobject($data, 'MarketPlaceUploadAttachment', '', $imageID);
                    if ($result) {
                        unlink(public_path('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_ORIGINAL_PATH') . $result['original_file_name']);
                        unlink(public_path('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_MEDIUM_PATH') . $result['medium_thumbnail']);
                        unlink(public_path('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_TINY_PATH') . $result['tiny_thumbnail']);
                        \DB::commit();
                        $returnMessage=$Input['language']=='en'?'PORTFOLIO_IMAGE_DELETED':'PORTFOLIO_IMAGE_DELETED_CHINESE';
                        $responseArray = UtilityController::Generateresponse(true, $returnMessage, 1);
                    } else {
                        \DB::rollback();
                        $returnMessage=$Input['language']=='en'?'GENERAL_ERROR':'GENERAL_ERROR_CHINESE';
                        $responseArray = UtilityController::Generateresponse(false,$returnMessage , 0);
                    }
                } else {
                    $returnMessage=$Input['language']=='en'?'CANNOT_DELETE_LAST_IMAGE':'CANNOT_DELETE_LAST_IMAGE_CHINESE';
                    throw new \Exception(UtilityController::Getmessage($returnMessage));
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, $e->getMessage(), 0);
        }
        return $responseArray;
    }

    //###############################################################
    //Function Name : DeleteportfolioVideo
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To delete images in uploaded portfolio(s) by designer
    //In Params : Void
    //Return : json
    //Date : 26th April 2018
    //###############################################################
    public function DeleteportfolioVideo(Request $request)
    {
        try {
            if (Auth::check()) {
                \DB::beginTransaction();
                $Input     = Input::all();
                $portfolio = MarketPlaceUploadAttachment::where('mp_upload_id', $Input['project_id'])->where('status', 1)->get();
                $portfolio = $portfolio->toArray();
                $count     = count($portfolio);
                if ($count > 1) {
                    $imageID        = $Input['image_id'];
                    $data['status'] = 2;
                    $result         = UtilityController::Makemodelobject($data, 'MarketPlaceUploadAttachment', '', $imageID);
                    if ($result) {
                        unlink(public_path('') . UtilityController::Getpath('MP_PORTFOLIO_ATTACHMENT_ORIGINAL_PATH') . $result['original_file_name']);                        
                        \DB::commit();
                        $returnMessage=$Input['language']=='en'?'PORTFOLIO_VIDEO_DELETED':'PORTFOLIO_VIDEO_DELETED_CHINESE';
                        $responseArray = UtilityController::Generateresponse(true, $returnMessage, 1);
                    } else {
                        \DB::rollback();
                        $returnMessage=$Input['language']=='en'?'GENERAL_ERROR':'GENERAL_ERROR_CHINESE';
                        $responseArray = UtilityController::Generateresponse(false,$returnMessage , 0);
                    }
                } else {
                    $returnMessage=$Input['language']=='en'?'CANNOT_DELETE_LAST_IMAGE':'CANNOT_DELETE_LAST_IMAGE_CHINESE';
                    throw new \Exception(UtilityController::Getmessage($returnMessage));
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, $e->getMessage(), 0);
        }
        return $responseArray;
    }

    //###############################################################
    //Function Name : Designerinvitationresponse
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : Designer invitation page
    //In Params : Void
    //Return : json
    //Date : 11th April 2018
    //###############################################################

    public function Designerpastprojects(Request $request)
    {
        try {
            \DB::beginTransaction();
            $UserID        = Auth::user()->id;
            $projectsQuery = MarketPlaceProject::select('*')
                ->whereHas('projectdetailinfo', function ($q) use ($UserID) {
                    $q->where('assigned_designer_id', $UserID);
                })
                ->with([
                    'projectCreator'     => function ($query) {
                        $query->select('id', 'first_name', 'last_name', 'display_name', 'image');
                    },
                    'skills'             => function ($query) {
                        $query->with('skillDetail')->get()->groupby('skill_set_id');
                    },
                    'rating_for_project' => function ($query) {
                        $query->get();
                    },
                    'review_for_project' => function ($query) {
                        $query->get();
                    },

                ])->withCount('message_board')->where('project_status', 3)->orderby('id','desc')->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))->toArray();

            if (!empty($projectsQuery)) {
                foreach ($projectsQuery['data'] as $key => $value) {
                    $date = Carbon::parse($value['created_at'])->timezone(Auth::user()->timezone);
                    $projectsQuery['data'][$key]['created_at'] = UtilityController::Changedateformat($date, 'd F, Y');
                    if (isset($value['projectdetailinfo'])) {
                        if ($value['projectdetailinfo']['invitation_accepted_at'] != '') {
                            $date = Carbon::parse($value['projectdetailinfo']['invitation_accepted_at'])->timezone(Auth::user()->timezone);
                            $projectsQuery['data'][$key]['accepted_at'] = UtilityController::Changedateformat($date, 'd F, Y');
                        } else {
                            $projectsQuery['data'][$key]['accepted_at'] = 'Pending';
                        }

                        if ($value['projectdetailinfo']['project_completed_at'] != '') {
                            $date = Carbon::parse($value['projectdetailinfo']['project_completed_at'])->timezone(Auth::user()->timezone);
                            $projectsQuery['data'][$key]['completed_at'] = UtilityController::Changedateformat($date, 'd F, Y');
                        } else {
                            $projectsQuery['data'][$key]['completed_at'] = 'Pending';
                        }

                    }
                    $tempCategory = [];
                    if (!empty($value['skills'])) {
                        foreach ($value['skills'] as $keyInner => $valueInner) {

                            $tempSkills     = $valueInner['skill_detail'];
                            $tempCategory[] = $tempSkills['skill_name'];
                        }
                    }

                    $projectsQuery['data'][$key]['category_list'] = implode(',', $tempCategory);

                    $projectsQuery['data'][$key]['project_creator']['image'] = UtilityController::Imageexist($value['project_creator']['image'], public_path('') . UtilityController::Getmessage('USER_PROFILE_PATH'), url('') . UtilityController::Getmessage('USER_PROFILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));

                    if ($value['rating_for_project']['rating'] != 0 && $value['rating_for_project']['rating'] != '') {
                        $ratingMatrix = json_decode($value['rating_for_project']['matrix_rating'], true);

                        $projectsQuery['data'][$key]['ratingMatrix'] = $ratingMatrix;
                    }
                }
                //echo("<pre>");print_r($projectsQuery);echo("</pre>");
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $projectsQuery);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            \DB::commit();
            return $returnData;
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
    //###############################################################
    //Function Name : Designerinvitationresponse
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : Designer invitation page
    //In Params : Void
    //Return : json
    //Date : 11th April 2018
    //###############################################################

    public function Designerprojectinvitationrejected(Request $request)
    {
        try {
            \DB::beginTransaction();

            $projectsQuery = MarketPlaceProjectsDetailInfo::where('assigned_designer_id', Auth::user()->id)->where('status', 2)->groupby('mp_project_id')
                ->with([
                    'project_details_with_skill_detail' => function ($query) {
                        $query->get();
                    },
                ])->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))->toArray();

            if (!empty($projectsQuery)) {
                foreach ($projectsQuery['data'] as $key => $value) {
                    $date = Carbon::parse($value['created_at'])->timezone(Auth::user()->timezone);
                    $projectsQuery['data'][$key]['project_details_with_skill_detail']['created_at'] = UtilityController::Changedateformat($date, 'd F, Y');
                    $tempCategory                                                                   = [];
                    if (!empty($value['project_details_with_skill_detail']['skill_detail'])) {
                        foreach ($value['project_details_with_skill_detail']['skill_detail'] as $keyInner => $valueInner) {

                            $tempSkills     = $valueInner['skill_detail'];
                            $tempCategory[] = $tempSkills['skill_name'];
                        }
                    }
                    $projectsQuery['data'][$key]['category_list'] = implode(',', $tempCategory);

                    $projectsQuery['data'][$key]['project_details_with_skill_detail']['project_creator']['image'] = UtilityController::Imageexist($value['project_details_with_skill_detail']['project_creator']['image'], public_path('') . UtilityController::Getmessage('USER_PROFILE_PATH'), url('') . UtilityController::Getmessage('USER_PROFILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));

                }
                //echo("<pre>");print_r($projectsQuery);echo("</pre>");
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $projectsQuery);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            \DB::commit();
            return $returnData;
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    // designer
    //###############################################################
    //Function Name : Designercancelledprojects
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : Designer cancelled projects
    //In Params : Void
    //Return : json
    //Date : 14th May 2018
    //###############################################################
    public function Designercancelledprojects(Request $request)
    {
        try {
            \DB::beginTransaction();

            $projects=MarketplaceProject::whereIn('project_status',[6,10])->with('projectCreator','rating_for_project','review_for_project','skill_detail')
            ->with([
                'projectdetailinfo'=> function ($query) {
                        $query->whereIn('status', [3,14])->get();
                },
            ])->orderby('project_cancelled_at','DESC')->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))->toArray();
            
            if (!empty($projects)) {
                foreach ($projects['data'] as $key => $value) {
                    
                    $date = Carbon::parse($value['created_at'])->timezone(Auth::user()->timezone);
                    
                    $projects['data'][$key]['created_at'] = UtilityController::Changedateformat($date, 'd F, Y');

                    $date = Carbon::parse($value['project_cancelled_at'])->timezone(Auth::user()->timezone);
                    $projects['data'][$key]['cancelled_at'] = UtilityController::Changedateformat($date, 'd F, Y');

                    $projects['data'][$key]['project_creator']['image'] = UtilityController::Imageexist($value['project_creator']['image'], public_path('') . UtilityController::Getmessage('USER_PROFILE_PATH'), url('') . UtilityController::Getmessage('USER_PROFILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));

                    $tempCategory = [];
                    if (!empty($value['skill_detail'])) {
                        foreach ($value['skill_detail'] as $keyInner => $valueInner) {

                            $tempSkills     = $valueInner['skill_detail'];
                            $tempCategory[] = $tempSkills['skill_name'];
                        }
                    }
                    $projects['data'][$key]['category_list'] = implode(',', $tempCategory);

                    if ($value['rating_for_project'] != 0 && $value['review_for_project'] != '') {
                        
                        $ratingMatrix = json_decode($value['rating_for_project']['matrix_rating'], true);

                        $projects['data'][$key]['ratingMatrix'] = $ratingMatrix;
                        $projects['data'][$key]['review_for_project'] = $value['review_for_project']['review'];
                    }
                }
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $projects);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            \DB::commit();
            $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $projects);
            return $returnData;

        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    // designer
    //###############################################################
    //Function Name : Buyercancelledprojects
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : Buyer canceeled projects
    //In Params : Void
    //Return : json
    //Date : 14th May 2018
    //###############################################################
    public function Buyercancelledprojects(Request $request)
    {
        try {
            \DB::beginTransaction();

            $projects = MarketPlaceProjectsDetailInfo::where('status', 9)
                ->with([
                    'designer_details'           => function ($query) {
                        $query->get();
                    },
                    'project_details_with_skill' => function ($query) {
                        $query->where('project_status', 8)->where('project_created_by_id', Auth::user()->id)->get();
                    },

                ])->orderby('created_at','desc')->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))->toArray();

            if (!empty($projects)) {
                foreach ($projects['data'] as $key => $value) {
                    
                    $date = Carbon::parse($value['created_at'])->timezone(Auth::user()->timezone);
                    $projects['data'][$key]['created_at']   = UtilityController::Changedateformat($date, 'd F, Y');
                    
                    $projectAt = Carbon::parse($value['created_at']);
                    
                    $date = Carbon::parse($value['project_details_with_skill']['project_cancelled_at'])->timezone(Auth::user()->timezone);
                    $projects['data'][$key]['cancelled_at'] = UtilityController::Changedateformat($date, 'd F, Y');
                    
                    $date = Carbon::parse($value['project_details_with_skill']['project_cancel_request_at'])->timezone(Auth::user()->timezone);
                    $projects['data'][$key]['cancelled_request_at'] = UtilityController::Changedateformat($date, 'd F, Y');

                    $projects['data'][$key]['designer_details']['image'] = UtilityController::Imageexist($value['designer_details']['image'], public_path('') . UtilityController::Getmessage('USER_PROFILE_PATH'), url('') . UtilityController::Getmessage('USER_PROFILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));

                    $tempCategory = [];
                    if (!empty($value['project_details_with_skill']['skill_detail'])) {
                        foreach ($value['project_details_with_skill']['skill_detail'] as $keyInner => $valueInner) {

                            $tempSkills     = $valueInner['skill_detail'];
                            $tempCategory[] = $tempSkills['skill_name'];
                        }
                    }

                    $projects['data'][$key]['category_list'] = implode(',', $tempCategory);
                }
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $projects);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            \DB::commit();
            $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $projects);
            return $returnData;

        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    // designer
    //###############################################################
    //Function Name : Changecancelprojectstatus
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : change cancel project status
    //In Params : Void
    //Return : json
    //Date : 15th May 2018
    //###############################################################
    public function Changecancelprojectstatus(Request $request)
    {
        try {

            \DB::beginTransaction();
            $input = Input::all();
            // print_r($input); die;
            if ($input['accept_or_reject'] == 'accept') {
                MarketplaceProject::where('id', $input['project_id'])->update(array('project_status' => $input['mp_project_status'],'project_cancelled_at' => Carbon::now()));
                $result=MarketPlaceProjectsDetailInfo::where('mp_project_id', $input['project_id'])->where('assigned_designer_id',$input['designer_id'])->update(array('status' => $input['mp_project_detail_status']));
                $projectDetails = MarketPlaceProject::with('selected_designer')->where('id',$input['project_id'])->first();
            
                $sender_id=Auth::user()->id;
                if(Auth::user()->id == $projectDetails['project_created_by_id']){
                    
                    $receiver_id = $projectDetails['selected_designer']['assigned_designer_id'];
                    $user_id=$projectDetails['project_created_by_id'];
                    $message_text_en='Buyer accepted request for cancellation of project at';
                    $message_text_chi='买方接受了取消项目的请求';
                    
                }
                else{
                    $notification['receiver_id'] = $projectDetails['project_created_by_id'];
                    $user_id=$projectDetails['selected_designer']['assigned_designer_id'];
                    $message_text_en='Seller accepted request for cancellation of project at';
                    $message_text_chi='卖方接受了取消项目的请求';
                }

                $mp_project_id=$input['project_id'];
                $status=1;
                $is_admin=0;

                self::Insertsystemnotifications($sender_id,$receiver_id,'Cancellation request accepted for project "'.$projectDetails['project_title'].'"','项目接受取消请求"'.$projectDetails['project_title'].'"',$projectDetails['slug']);

                self::Insertmessageboardnotifications($receiver_id,$mp_project_id,'Cancellation request accepted ','取消请求已被接受',4,2,2);
                self::Insertadmintimelinenotifications($user_id,$mp_project_id,$message_text_en,$message_text_en,$status,$is_admin);
                $returnMessage=$input['language']=='en'?'REQUEST_ACCEPTED':'REQUEST_ACCEPTED_CHINESE';
                

                $data['project_number']=$projectDetails['project_number'];
                $data['email_address']=$projectDetails['selected_designer']['designer_details']['email_address'];

                $data['subject']=$projectDetails['selected_designer']['designer_details']['current_language']==1?'Cancel project request accepted.':'取消取消项目请求。';

                $data['content']=$projectDetails['selected_designer']['designer_details']['current_language']==1?'Hello <strong>'.$projectDetails['selected_designer']['designer_details']['first_name'].' '.$projectDetails['selected_designer']['designer_details']['last_name'].' ,
                    </strong><br/>
                    <br/>
                    Your cancellation request for Project  <strong>ID: '.$data['project_number'].' </strong> has been accepted by Buyer.<br/><br/><br/>':'你好 <strong>'.$projectDetails['selected_designer']['designer_details']['first_name'].' '.$projectDetails['selected_designer']['designer_details']['last_name'].'</strong> ,
                    <br/>
                    <br/>
                    您的项目 <strong>ID: '.$data['project_number'].'</strong> 取消请求已被买家接受。<br/><br/><br/>';

                $data['footer_content']=$projectDetails['selected_designer']['designer_details']['current_language']==1?'From,<br /> SixClouds Sixteen':'六云十六 ';

                $data['footer']=$projectDetails['selected_designer']['designer_details']['current_language']==1?'SixClouds':'六云';

                Mail::send('emails.email_template', $data, function ($message) use ($data) {
                    $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                    $message->to($data['email_address'])->subject($data['subject']);
                });
                
            }
            else{
                MarketplaceProject::where('id', $input['project_id'])->update(array('project_status' => $input['mp_project_status'], 'project_cancellation_reject_reason' => $input['reason']));
                $result = MarketPlaceProjectsDetailInfo::where('mp_project_id', $input['project_id'])->where('assigned_designer_id', $input['designer_id'])->update(array('status' => $input['mp_project_detail_status']));

                $project_creator=User::where('id',$input['creator_id'])->get()->toArray();
                $selected_designer=User::where('id',$input['designer_id'])->get()->toArray();

                $data['project_creator']=$project_creator[0];
                $data['selected_designer']=$selected_designer[0];
                $data['reason']=$input['reason'];
                $projectDetails = MarketPlaceProject::with('selected_designer')->where('id',$input['project_id'])->first();
                if(Auth::user()->id==$data['project_creator']['id']){
                    
                    $data['project_number']=$projectDetails['project_number'];
                    $data['email_address']=$projectDetails['selected_designer']['designer_details']['email_address'];

                    $data['subject']=$projectDetails['selected_designer']['designer_details']['current_language']==1?'Cancel project request rejected.':'取消项目请求被拒绝';

                    $data['content']=$projectDetails['selected_designer']['designer_details']['current_language']==1?'Hello <strong>'.$projectDetails['selected_designer']['designer_details']['first_name'].' '.$projectDetails['selected_designer']['designer_details']['last_name'].' ,
                        </strong><br/>
                        <br/>
                        Your cancellation request for Project  <strong>ID: '.$data['project_number'].' </strong> has been rejected by Buyer.<br/><br/><br/>':'你好 <strong>'.$projectDetails['selected_designer']['designer_details']['first_name'].' '.$projectDetails['selected_designer']['designer_details']['last_name'].'</strong> ,
                        <br/>
                        <br/>
                        您的项目 <strong>ID: '.$data['project_number'].'</strong> 取消请求已被买家拒绝。<br/><br/><br/>';

                    $data['footer_content']=$projectDetails['selected_designer']['designer_details']['current_language']==1?'From,<br /> SixClouds Sixteen':'六云十六 ';

                    $data['footer']=$projectDetails['selected_designer']['designer_details']['current_language']==1?'SixClouds':'六云';

                    Mail::send('emails.email_template', $data, function ($message) use ($data) {
                        $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                        $message->to($data['email_address'])->subject($data['subject']);
                    });

                    $action['action_text']    = UtilityController::Getmessage('BUYER_REJECTED_SELLER_CANCELLATION_REQUEST');

                    self::Insertsystemnotifications($projectDetails['project_created_by_id'],$projectDetails['selected_designer']['assigned_designer_id'],'Buyer rejected cancellation request for project "'.$projectDetails['project_title'].'" due to '.$input['reason'],'买方拒绝了项目的取消请求"'.$projectDetails['project_title'].'"由于'.$input['reason'],$projectDetails['slug']);
                
                    self::Insertmessageboardnotifications($projectDetails['selected_designer']['assigned_designer_id'],$input['project_id'],'Cancellation request rejected ','取消请求被拒绝',4,2,2);  

                    self::Insertsystemnotifications($projectDetails['selected_designer']['assigned_designer_id'],$projectDetails['project_created_by_id'],'A violation has been raised for project "'.$projectDetails['project_title'],'项目违规行为已经提出"'.$projectDetails['project_title'],$projectDetails['slug']);
                    self::Insertsystemnotifications($projectDetails['project_created_by_id'],$projectDetails['selected_designer']['assigned_designer_id'],'A violation has been raised for project "'.$projectDetails['project_title'],'项目违规行为已经提出"'.$projectDetails['project_title'],$projectDetails['slug']);
                
                    self::Insertmessageboardnotifications($projectDetails['selected_designer']['assigned_designer_id'],$input['project_id'],'A violation has been raised for project ','项目违规行为已经提出',4,2,3);  


                    $user_id                               = $projectDetails['project_created_by_id'];
                    $message_text_en                       = 'Buyer rejected request for cancellation of project at';
                    $message_text_chi                      = '买方接受了取消项目的请求';
                }else{
                    //currentlly it is not used. Before removing tested out.
                    $action['action_text']    = UtilityController::Getmessage('SELLER_REJECTED_BUYER_CANCELLATION_REQUEST');

                    $notification['sender_id']             = $projectDetails['selected_designer']['assigned_designer_id'];
                    $notification['receiver_id']           = $projectDetails['project_created_by_id'];
                    $notification['notification_text']     = 'Seller rejected cancellation request for project "'.$projectDetails['project_title'].'" due to '.$input['reason'];
                    $notification['project_slug']          = $projectDetails['slug'];
                    $notification['chi_notification_text'] = '卖方拒绝了项目的取消请求"'.$projectDetails['project_title'].'"由于'.$input['reason'];
                    $user_id                               = $projectDetails['selected_designer']['assigned_designer_id'];
                    $message_text_en                       = 'Seller rejected request for cancellation of project at';
                    $message_text_chi                      = '卖方拒绝了取消项目的请求';
                    
                }

                $action['mp_project_id'] = $input['project_id'];
                $action['action_type']   = 4;
                $action['show_first']    = 1;
                $actionResult            = UtilityController::Makemodelobject($action, 'MarketplaceAction');
                $actionResult            = UtilityController::Makemodelobject($action,'MarketPlaceProject','',$input['project_id']);

                $notification['module']      = 1;
                $notificationUpdate = UtilityController::Makemodelobject($notification, 'SystemNotification');

                
                $mp_project_id=$input['project_id'];
                $is_admin=0;
                $status=2;

                self::Insertadmintimelinenotifications($user_id,$mp_project_id,$message_text_en,$message_text_chi,$status,$is_admin);
                $returnMessage=$input['language']=='en'?'REQUEST_REJECTED':'REQUEST_REJECTED_CHINESE';
            }
            \DB::commit();

            if ($result) {
                $returnData = UtilityController::Generateresponse(true, $returnMessage, 200, '');
            } else {
                $returnData = UtilityController::Generateresponse(false, '', 400, '');
            }
            return $returnData;

        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    // designer
    //###############################################################
    //Function Name : MutualCancelprojectlisting
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : to display list of project cancelled by buyer and seller
    //In Params : Void
    //Return : json
    //Date : 15th May 2018
    //###############################################################

    public function Mutualcancelprojectlisting()
    {
        try {
            \DB::beginTransaction();

            $projects = MarketPlaceProject::whereIn('project_status', [6,10])
                ->where('project_created_by_id', Auth::user()->id)
                ->with([
                    'selected_designer' => function ($query) {
                        $query->whereIn('status', [3,14])->get();
                    },
                    'skill_detail'      => function ($query) {
                        $query->get();
                    },
                    'rating_for_project'      => function ($query) {
                        $query->get();
                    },
                    'review_for_project'      => function ($query) {
                        $query->get();
                    },
                ])->orderby('created_at','DESC')->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))->toArray();
            
            if (!empty($projects)) {
                foreach ($projects['data'] as $key => $value) {

                    $date = Carbon::parse($value['created_at'])->timezone(Auth::user()->timezone);
                    $projects['data'][$key]['created_at'] = UtilityController::Changedateformat($date, 'd F, Y');

                    $projectAt = Carbon::parse($value['created_at']);

                    $date = Carbon::parse($value['project_cancelled_at'])->timezone(Auth::user()->timezone);
                    $projects['data'][$key]['cancelled_at'] = UtilityController::Changedateformat($date, 'd F,Y');

                    $projects['data'][$key]['image'] = UtilityController::Imageexist($value['selected_designer']['designer_details']['image'], public_path('') . UtilityController::Getmessage('USER_PROFILE_PATH'), url('') . UtilityController::Getmessage('USER_PROFILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));

                    $tempCategory = [];
                    if (!empty($value['skill_detail'])) {
                        foreach ($value['skill_detail'] as $keyInner => $valueInner) {

                            $tempSkills     = $valueInner['skill_detail'];
                            $tempCategory[] = $tempSkills['skill_name'];
                        }
                    }

                    $projects['data'][$key]['category_list'] = implode(',', $tempCategory);
                }

                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $projects);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            \DB::commit();
            $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $projects);
            return $returnData;

        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    // designer
    //###############################################################
    //Function Name : Disputedprojectlisting
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : to display list of disputed project
    //In Params : Void
    //Return : json
    //Date : 15th May 2018
    //###############################################################

    public function Disputedprojectlisting()
    {
        try {
            \DB::beginTransaction();

            $projects = MarketPlaceProject::where('project_status', 9)->where('project_created_by_id', Auth::user()->id)
                ->with([
                    'projectCreator'    => function ($query) {
                        $query->get();
                    },
                    'skill_detail'      => function ($query) {
                        $query->get();
                    },
                    'selected_designer' => function ($query) {
                        $query->get();
                    },

                ])->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))->toArray();

            if (!empty($projects)) {
                foreach ($projects['data'] as $key => $value) {
                    
                    $date = Carbon::parse($value['created_at'])->timezone(Auth::user()->timezone);
                    $projects['data'][$key]['created_at'] = UtilityController::Changedateformat($date, 'd F, Y');
                    
                    $date = Carbon::parse($value['project_cancelled_at'])->timezone(Auth::user()->timezone);
                    $projects['data'][$key]['cancelled_at'] = UtilityController::Changedateformat($date, 'd F, Y');

                    $projects['data'][$key]['selected_designer']['designer_details']['image'] = UtilityController::Imageexist($value['selected_designer']['designer_details']['image'], public_path('') . UtilityController::Getmessage('USER_PROFILE_PATH'), url('') . UtilityController::Getmessage('USER_PROFILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));

                    $tempCategory = [];
                    if (!empty($value['skill_detail'])) {
                        foreach ($value['skill_detail'] as $keyInner => $valueInner) {

                            $tempSkills     = $valueInner['skill_detail'];
                            $tempCategory[] = $tempSkills['skill_name'];
                        }
                    }

                    $projects['data'][$key]['category_list'] = implode(',', $tempCategory);
                }
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $projects);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            \DB::commit();
            $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $projects);
            return $returnData;

        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    // designer
    //###############################################################
    //Function Name : Sellerdisputedprojectlisting
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : to display list of disputed project by seller
    //In Params : Void
    //Return : json
    //Date : 15th May 2018
    //###############################################################

    public function Sellerdisputedprojectlisting()
    {
        try {
            \DB::beginTransaction();

            $projects = MarketPlaceProject::where('project_status', 9)
                ->with([
                    'projectCreator'    => function ($query) {
                        $query->get();
                    },
                    'skill_detail'      => function ($query) {
                        $query->get();
                    },
                    'selected_designer' => function ($query) {
                        $query->where('assigned_designer_id', Auth::user()->id)->get();
                    },
                ])->whereHas('selected_designer', function ($query){
                        $query->where('assigned_designer_id', Auth::user()->id);
            })->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))->toArray();

            if (!empty($projects)) {
                foreach ($projects['data'] as $key => $value) {

                    $date = Carbon::parse($value['created_at'])->timezone(Auth::user()->timezone);
                    $projects['data'][$key]['created_at'] = UtilityController::Changedateformat($date, 'd F, Y');

                    $date = Carbon::parse($value['project_cancelled_at'])->timezone(Auth::user()->timezone);
                    $projects['data'][$key]['cancelled_at'] = UtilityController::Changedateformat($date, 'd F, Y');

                    $projects['data'][$key]['project_creator']['image'] = UtilityController::Imageexist($value['project_creator']['image'], public_path('') . UtilityController::Getmessage('USER_PROFILE_PATH'), url('') . UtilityController::Getmessage('USER_PROFILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));

                    $tempCategory = [];
                    if (!empty($value['skill_detail'])) {
                        foreach ($value['skill_detail'] as $keyInner => $valueInner) {

                            $tempSkills     = $valueInner['skill_detail'];
                            $tempCategory[] = $tempSkills['skill_name'];
                        }
                    }

                    $projects['data'][$key]['category_list'] = implode(',', $tempCategory);
                }
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $projects);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            \DB::commit();
            $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $projects);
            return $returnData;

        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
    // designer
    //###############################################################
    //Function Name : Sellercanceldprojectrequestlisting
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : to display list of cancel request project by buyer
    //In Params : Void
    //Return : json
    //Date : 15th May 2018
    //###############################################################

    public function Sellercanceldprojectrequestlisting()
    {
        try {
            \DB::beginTransaction();

            $projects = MarketPlaceProjectsDetailInfo::where('status', 11)
                ->where('assigned_designer_id', Auth::user()->id)
                ->with([
                    'project_details_with_skill_detail' => function ($query) {
                        $query->where('project_status', 8)->get();
                    },

                ])->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))->toArray();

            if (!empty($projects)) {
                foreach ($projects['data'] as $key => $value) {

                    $date = Carbon::parse($value['created_at'])->timezone(Auth::user()->timezone);
                    $projects['data'][$key]['created_at'] = UtilityController::Changedateformat($date, 'd F, Y');

                    $date = Carbon::parse($value['project_details_with_skill_detail']['project_cancel_request_at'])->timezone(Auth::user()->timezone);
                    $projects['data'][$key]['cancelled_request_at'] = UtilityController::Changedateformat($date, 'd F, Y');

                    $projects['data'][$key]['image'] = UtilityController::Imageexist($value['project_details_with_skill_detail']['project_creator']['image'], public_path('') . UtilityController::Getmessage('USER_PROFILE_PATH'), url('') . UtilityController::Getmessage('USER_PROFILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));

                    // $projects['data'][$key]['cancelled_at'] = UtilityController::Changedateformat($value['project_details_with_skill_detail']['project_cancelled_at'], 'd F,Y');
                    // $projects['data'][$key]['image']        = UtilityController::Imageexist($value['project_details_with_skill_detail']['project_creator']['image'], public_path('') . UtilityController::Getmessage('USER_PROFILE_PATH'), url('') . UtilityController::Getmessage('USER_PROFILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));

                    $tempCategory = [];
                    if (!empty($value['project_details_with_skill_detail']['skill_detail'])) {
                        foreach ($value['project_details_with_skill_detail']['skill_detail'] as $keyInner => $valueInner) {

                            $tempSkills     = $valueInner['skill_detail'];
                            $tempCategory[] = $tempSkills['skill_name'];
                        }
                    }

                    $projects['data'][$key]['category_list'] = implode(',', $tempCategory);
                }

                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $projects);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            \DB::commit();
            $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $projects);
            return $returnData;

        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    // designer
    //###############################################################
    //Function Name : Designerpastprojectinfo
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get Designer's past project info
    //In Params : Void
    //Return : json
    //Date : 9th April 2018
    //###############################################################
    public function Designerpastprojectinfo(Request $request)
    {
        try {
            \DB::beginTransaction();
            $input         = Input::all();
            $buyerId       = Auth::user()->id;
            $mp_project_id = substr($input['slug'], strpos($input['slug'], ":") + 1);
            $mp_project_id = base64_decode($mp_project_id);

            $projects = MarketPlaceProject::where('slug', $request->slug)
                ->with([
                    'projectCreator'    => function ($query) {
                        $query->get();
                    },
                    'projectdetailinfo' => function ($query) {
                        $query->get();
                    },
                    'message_board'     => function ($query) {
                        $query->get();
                    },
                ])->where('project_status', 3)->first();

            if ($projects) {
                $projects['project_created_at']   = $projects['created_at']->format('d F, Y');
                $projects['image']                = UtilityController::Imageexist($projects['selected_designer']['designer_details']['image'], public_path('') . UtilityController::Getmessage('USER_PROFILE_PATH'), url('') . UtilityController::Getmessage('USER_PROFILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));
                
                $date = Carbon::parse($projects['selected_designer']['created_at'])->timezone(Auth::user()->timezone);
                $projects['invitation_sent_on']   = UtilityController::Changedateformat($date, 'd F, Y h:i A');
                
                $date = Carbon::parse($projects['selected_designer']['project_completed_at'])->timezone(Auth::user()->timezone);
                $projects['project_completed_on'] = UtilityController::Changedateformat($date, 'd F, Y h:i A');
                if ($projects['selected_designer']['invitation_accepted_at'] != '') {
                    $date = Carbon::parse($projects['selected_designer']['invitation_accepted_at'])->timezone(Auth::user()->timezone);
                    $projects['invitation_accepted_on'] = UtilityController::Changedateformat($date, 'd F, Y h:i A');
                } else {
                    $projects['invitation_accepted_on'] = "Pending";
                }

                foreach ($projects['message_board'] as $key => $value) {
                    $value['image']      = UtilityController::Imageexist($value['user_details']['image'], public_path('') . UtilityController::Getmessage('USER_PROFILE_PATH'), url('') . UtilityController::Getmessage('USER_PROFILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));
                    
                    $date = Carbon::parse($value['created_at'])->timezone(Auth::user()->timezone);
                    $value['message_on'] = UtilityController::Changedateformat($date, 'd F, Y h:i A');
                    $value['side_class'] = ($value['user_role'] == 1 ? 'left' : 'right');
                }
                $projects['messages'] = MarketPlaceMessageBoard::where('id', $mp_project_id)->get();

                if (!empty($projects)) {
                    $projects = $projects->toArray();
                    
                    $date = Carbon::parse($projects['created_at'])->timezone(Auth::user()->timezone);
                    $projects['created_at']             = UtilityController::Changedateformat($date, 'd F, Y');
                    
                    $date = Carbon::parse($projects['projectdetailinfo']['invitation_accepted_at'])->timezone(Auth::user()->timezone);
                    $projects['invitation_accepted_at'] = UtilityController::Changedateformat($date, 'd F, Y');
                    
                    $date = Carbon::parse($projects['projectdetailinfo']['project_completed_at'])->timezone(Auth::user()->timezone);
                    $projects['project_completed_at']   = UtilityController::Changedateformat($date, 'd F, Y');
                }
                \DB::commit();
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $projects);
                return $returnData;
            }

        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    // designer
    //###############################################################
    //Function Name : Uploadmessagefile
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To upload message file
    //In Params : Void
    //Return : json
    //Date : 17th April 2018
    //###############################################################
    public function Uploadmessagefile(Request $request)
    {
        try {
            \DB::beginTransaction();
            $Input = Input::all();
            if (!empty($Input['file'][0])) {
                if ($Input['file'][0]->getClientSize() > UtilityController::Getmessage('MAX_UPLOAD_FILE_SIZE')) {
                    throw new \Exception(UtilityController::Getmessage('FILE_UPTO_MB'));
                }
                $file = $Input['file'][0]->getMimeType();
                // create file unique name
                $orgfileName = rand() . time() . str_replace(' ', '_', $Input['file'][0]->getClientOriginalName());
                $extension   = \File::extension($Input['file'][0]->getClientOriginalName());
                if (in_array($extension, array('zip', 'ZIP', 'tar', 'TAR', 'rar', 'RAR', 'js', 'JS', 'tar.gz', 'TAR.GZ', 'gz', 'GZ'))) {
                    throw new \Exception(UtilityController::Getmessage('NO_COMPRESSED_ALLOWED'));
                } else {
                    $tinyfileName = 'tiny_' . $orgfileName;
                    if (!empty($Input['path'])) {
                        $DestinationPath = config('constants.path.MP_ATTACHMENT_UPLOAD_PATH');
                    } else {
                        $DestinationPath = config('constants.messages.MSG_FILE_PATH');
                    }
                    $DestinationPath = public_path() . $DestinationPath;
                    if (strpos($file, 'image/') !== false) {
                        Image::make($Input['file'][0])->resize(300, 300, function ($constraint) {
                            $constraint->aspectRatio();
                            $constraint->upsize();
                        })->save($DestinationPath . $tinyfileName);
                        chmod($DestinationPath . $tinyfileName, 0777);
                        $Input['file'][0]->move($DestinationPath , $orgfileName);
                        chmod($DestinationPath . $orgfileName, 0777);
                        $result['type'] = 2;
                    } else {
                        $Input['file'][0]->move($DestinationPath , $orgfileName);
                        chmod($DestinationPath . $orgfileName, 0777);
                        $result['type'] = 3;
                    }
                    $result['message'] = $orgfileName;
                    $result['size']    = $Input['file'][0]->getClientSize();
                    $returnData        = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $result);
                }
            } else {
                $returnData = UtilityController::Generateresponse(false, '', '', '');
            }

            return $returnData;
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false,$e->getMessage() , '', '');
            return $returnData;
        }
    } 
    // designer
    //###############################################################
    //Function Name : Unlinkmsgbrdremovedfile
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To unlink file which is cancelled by user in message board
    //In Params : Void
    //Return : json
    //Date : 19th April 2018
    //###############################################################
    public function Unlinkmsgbrdremovedfile(Request $request)
    {
        try {
            \DB::beginTransaction();
            $Input = Input::all();
            if (file_exists(public_path() . config('constants.messages.MSG_FILE_PATH') . $Input['file_name'])) {
                unlink(public_path() . config('constants.messages.MSG_FILE_PATH') . $Input['file_name']);
            }
            if (file_exists(public_path() . config('constants.messages.MSG_FILE_PATH') . 'tiny_' . $Input['file_name'])) {
                unlink(public_path() . config('constants.messages.MSG_FILE_PATH') . 'tiny_' . $Input['file_name']);
            }
            $returnData = UtilityController::Generateresponse(true, '', '', '');
            return $returnData;
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Designerinspirationbanklisting
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : To list inspiration bank page data
    //In Params : Void
    //Return : json
    //Date : 19th April 2018
    //###############################################################
    public function Designerinspirationbanklisting(Request $request)
    {
        $input = Input::all();                       
        try
        {
            \DB::beginTransaction();
            $search                 = (!empty($input['search']) ? $input['search'] : '');                
            $listMarketPlaceUploads = MarketPlaceUpload::where('status', 1)
                ->where('upload_of', '=', 1)
                ->with([
                    'cover_image_attachments'    => function ($query) {
                        $query->get();
                    },
                    'inspiration_bank_files_attachments'    => function ($query) {
                        $query->get();
                    },
                    'user_favourite'    => function ($query) {
                        $query->where("user_id",Auth::User()->id)->get();
                    },                    
                ]);
            if (isset($input['type']) && $input['type'] != '') {
                if($input['type'] == 'uploads'){
                    $listMarketPlaceUploads->where('uploaded_user_id', Auth::User()->id);
                }elseif($input['type'] == 'favourites'){
                    $listMarketPlaceUploads->whereHas('user_favourite', function ($query) {
                        $query->where('user_id', Auth::User()->id);
                    });                    
                }
            }            
            if (isset($input['id']) && $input['id'] != '') {
                $id               = substr($input['id'], strpos($input['id'], ":") + 1);
                $uploaded_user_id = base64_decode($id);
                $listMarketPlaceUploads->where('uploaded_user_id', $uploaded_user_id);
            }
            if (!empty($search)) {
                $listMarketPlaceUploads = $listMarketPlaceUploads
                    ->whereHas('uploadedby', function ($query) use ($search) {
                        $query->where('first_name', 'like', '%' . $search . '%')
                            ->orWhere('last_name', 'like', '%' . $search . '%');
                    })->with([
                    'uploadedby' => function ($query) use ($search) {
                        $query->get();
                    },
                ]);
            } else {
                $listMarketPlaceUploads = $listMarketPlaceUploads
                    ->has('uploadedby')->with([
                    'uploadedby' => function ($query) use ($search) {
                        $query->get();
                    },
                ]);
            }

            if (isset($input['search']) && $input['search'] != '') {
                $listMarketPlaceUploads->orWhere('upload_title', 'like', '%' . $search . '%')->where("status",1);
            }
            if (isset($input['categoryId']) && $input['categoryId'] != 'all') {
                $listMarketPlaceUploads->whereHas('inspiration_categories', function ($query) use ($input) {
                    $query->where('categoty_id',$input['categoryId']);
                });
            }

            if (isset($input['id']) && $input['id'] != '') {

                $listMarketPlaceUploads = $listMarketPlaceUploads->paginate(config('constants.other.MP_DESIGNER_PROFILE_PAGINATE'))->toArray();
            } else {
                $listMarketPlaceUploads = $listMarketPlaceUploads->paginate(config('constants.other.MP_INSPIRATION_BANK_PAGINATE'))->toArray();
            }                      
            if (!empty($listMarketPlaceUploads)) {

                foreach ($listMarketPlaceUploads['data'] as $key => $value) { 
                    if(!empty($value['user_favourite'])){
                        $listMarketPlaceUploads['data'][$key]['user_favourite'] = true;
                    }else{
                        $listMarketPlaceUploads['data'][$key]['user_favourite'] = false;
                    }                
                    
                    if(!empty($value['cover_image_attachments'])){
                        foreach ($value['cover_image_attachments'] as $keyCoverInner => $valueCoverInner) {                             
                            $listMarketPlaceUploads['data'][$key]['cover_image_attachments_files'][$keyCoverInner]['medium_thumbnail'] = UtilityController::Imageexist($valueCoverInner['medium_thumbnail'], public_path('') . UtilityController::Getpath('MP_INSPIRATION_ATTACHMENT_MEDIUM_PATH').$value['uploadedby']['id'].'/Cover_image/', url('') . UtilityController::Getpath('MP_INSPIRATION_ATTACHMENT_MEDIUM_URL').$value['uploadedby']['id'].'/Cover_image/', url('') . UtilityController::Getmessage('NO_IMAGE_URL'));
                            $listMarketPlaceUploads['data'][$key]['cover_image_attachments_files'][$keyCoverInner]['original_file_name'] = UtilityController::Imageexist($valueCoverInner['original_file_name'], public_path('') . UtilityController::Getpath('MP_INSPIRATION_ATTACHMENT_MEDIUM_PATH').$value['uploadedby']['id'].'/Cover_image/', url('') . UtilityController::Getpath('MP_INSPIRATION_ATTACHMENT_MEDIUM_URL').$value['uploadedby']['id'].'/Cover_image/', url('') . UtilityController::Getmessage('NO_IMAGE_URL'));
                        }
                    }else{
                         $listMarketPlaceUploads['data'][$key]['cover_image_attachments_files'][0]['medium_thumbnail']   = url('') . UtilityController::Getmessage('NO_IMAGE_URL');
                         $listMarketPlaceUploads['data'][$key]['cover_image_attachments_files'][0]['original_file_name'] = url('') . UtilityController::Getmessage('NO_IMAGE_URL');
                    }
                    if(!empty($value['inspiration_bank_files_attachments'])){
                        foreach ($value['inspiration_bank_files_attachments'] as $keyInner => $valueInner) {
                            $listMarketPlaceUploads['data'][$key]['inspiration_bank_files_attachments'][$keyInner]['medium_thumbnail'] = UtilityController::Imageexist($valueInner['medium_thumbnail'], public_path('') . UtilityController::Getpath('MP_INSPIRATION_ATTACHMENT_MEDIUM_PATH').$value['uploadedby']['id'].'/Medium/', url('') . UtilityController::Getpath('MP_INSPIRATION_ATTACHMENT_MEDIUM_URL').$value['uploadedby']['id'].'/Medium/', url('') . UtilityController::Getmessage('NO_IMAGE_URL'));
                            $listMarketPlaceUploads['data'][$key]['inspiration_bank_files_attachments'][$keyInner]['original_file_name'] = UtilityController::Imageexist($valueInner['original_file_name'], public_path('') . UtilityController::Getpath('MP_INSPIRATION_ATTACHMENT_MEDIUM_PATH').$value['uploadedby']['id'].'/Original/', url('') . UtilityController::Getpath('MP_INSPIRATION_ATTACHMENT_MEDIUM_URL').$value['uploadedby']['id'].'/Original/', url('') . UtilityController::Getmessage('NO_IMAGE_URL'));
                        }
                    }else{
                        $listMarketPlaceUploads['data'][$key]['inspiration_bank_files_attachments'][0]['medium_thumbnail'] = url('') . UtilityController::Getmessage('NO_IMAGE_URL');
                        $listMarketPlaceUploads['data'][$key]['inspiration_bank_files_attachments'][0]['original_file_name'] = url('') . UtilityController::Getmessage('NO_IMAGE_URL');
                    }
                    $listMarketPlaceUploads['data'][$key]['all_images']  = array_merge($listMarketPlaceUploads['data'][$key]['cover_image_attachments_files'],$listMarketPlaceUploads['data'][$key]['inspiration_bank_files_attachments']);
                    $listMarketPlaceUploads['data'][$key]['uploadedby']['image'] = UtilityController::Imageexist($value['uploadedby']['image'], public_path('') . UtilityController::Getpath('USER_PROFILE_PATH'), url('') . UtilityController::Getpath('USER_PROFILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));
                    $listMarketPlaceUploads['data'][$key]['filename'] =  $value['file'];
                    $listMarketPlaceUploads['data'][$key]['file'] = UtilityController::Getmessage('BUCKET_LINK'). $value['file'];
                }                     
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $listMarketPlaceUploads);
            } else {

                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            \DB::commit();

            return $returnData;
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }

    }

    //###############################################################
    //Function Name : Designerinspirationbankcategorylistsing
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : To list inspiration bank category data
    //In Params : Void
    //Return : json
    //Date : 19th April 2018
    //###############################################################
    public function Designerinspirationbankcategorylistsing()
    {
        try
        {
            \DB::beginTransaction();

            $listInspirationBankCategory = MarketPlaceInsBankCategory::select('id', 'category_name_en')->get();

            if (!empty($listInspirationBankCategory)) {

                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $listInspirationBankCategory);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            \DB::commit();

            return $returnData;
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    // designer
    //###############################################################
    //Function Name : Addextensionforproject
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To add project extension
    //In Params : Void
    //Return : json
    //Date : 26th April 2018
    //###############################################################
    public function Addextensionforproject(Request $request)
    {
        try {

            \DB::beginTransaction();
            $Input                  = Input::all();

            $Input['designer_id']   = Auth::user()->id;
            $Input['project_id']    = $mp_project_id    = substr($Input['project_id'], strpos($Input['project_id'], ":") + 1);
            $Input['mp_project_id'] = base64_decode($mp_project_id);
            $result                 = UtilityController::Makemodelobject($Input, 'MarketPlaceProjectExtension', '');
            $data['mp_project_id']  = $Input['mp_project_id'];
            $data['action_type']    = 1;
            $data['action_text']    = UtilityController::Getmessage('SELLER_REQUESTED_EXTENSION');
            $data['show_first']     = 1;
            $actionResult           = UtilityController::Makemodelobject($data, 'MarketplaceAction');
            $actionResult           = UtilityController::Makemodelobject($data, 'MarketPlaceProject', '', $Input['mp_project_id']);

            $projectDetails                        = MarketPlaceProject::with('selected_designer')->where('id',$Input['mp_project_id'])->first();
        
            self::Insertsystemnotifications($projectDetails['selected_designer']['assigned_designer_id'],$projectDetails['project_created_by_id'],'Seller requested timeline extension for "'.$projectDetails['project_title'].'"','卖方要求延长时间"'.$projectDetails['project_title'].'"',$projectDetails['slug']);

            self::Insertmessageboardnotifications($Input['designer_id'],$Input['mp_project_id'],'Seller requested for extension of ' . $Input['days'] . ' day(s)','卖方要求延期' . $Input['days'] . '天',4,1,1);
            self::Insertadmintimelinenotifications(Auth::user()->id,$Input['mp_project_id'],'Seller requested for extension of '.$Input['days']. ' days at','卖方要求 '.$Input['days'].'  天',6,0);

            if ($result) {
                \DB::commit();
                $returnMessage=$Input['language']=='en'?'EXTENSION_SUBMITED':'EXTENSION_SUBMITED_CHINESE';
                $returnData = UtilityController::Generateresponse(true, $returnMessage, 200, '');
            } else {
                $returnData = UtilityController::Generateresponse(false, '', 400, '');
            }
            
            \DB::commit();
            return $returnData;
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    // designer
    //###############################################################
    //Function Name : Designerprofileinfo
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To add project extension
    //In Params : Void
    //Return : json
    //Date : 2nd May 2018
    //###############################################################
    public function Designerprofileinfo(Request $request)
    {

        try {
            \DB::beginTransaction();
            $input = Input::all();

            $id      = substr($input['id'], strpos($input['id'], ":") + 1);
            $user_id = base64_decode($id);

            //$getId=User::select('id')->where('slug',$input['id'])->get();

            $designerProfileData = User::with([
                'skills'             => function ($query) {
                    $query->groupby('skill_set_id')->get();
                },
                'school'             => function ($query) {
                    $query->get();
                },
                'country'            => function ($query) {
                    $query->get();
                },
                'completed_projects' => function ($query) {
                    $query->get();
                },
                'ratings'            => function ($query) {
                    $query->get();
                },
            ])
                ->where('id', $user_id)->get();

            $designerProfileData = $designerProfileData->toArray();

            if ($designerProfileData) {
                foreach ($designerProfileData as $key => $value) {

                    $designerProfileData[$key]['image'] = UtilityController::Imageexist($value['image'], public_path('') . UtilityController::Getpath('USER_PROFILE_PATH'), url('') . UtilityController::Getpath('USER_PROFILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));
                    // if ($designerProfileData[$key]['mp_designer_proj_availability'] == 1) {
                    //     $designerProfileData[$key]['mp_designer_proj_availability'] = 'Full-time (40+ hours per week)';
                    // }
                    // if ($designerProfileData[$key]['mp_designer_proj_availability'] == 2) {
                    //     $designerProfileData[$key]['mp_designer_proj_availability'] = 'Part-time (25-36 hours per week)';
                    // }
                    // if ($designerProfileData[$key]['mp_designer_proj_availability'] == 3) {
                    //     $designerProfileData[$key]['mp_designer_proj_availability'] = 'Hobbist (12-24 hours per week)';
                    // }
                    // if ($designerProfileData[$key]['mp_designer_proj_availability'] == 4) {
                    //     $designerProfileData[$key]['mp_designer_proj_availability'] = 'Casual (less than 12 hours per week)';
                    // }
                    $completedProjects = 0;
                    foreach ($value['completed_projects'] as $keyInner => $valueInner) {
                        if ($valueInner['project_details']['project_status'] == 3) {
                            $completedProjects++;
                        }

                    }
                    $designerProfileData[$key]['projects_completed'] = $completedProjects;

                    $onGoingProjects = 0;
                    foreach ($value['completed_projects'] as $keyInner => $valueInner) {
                        if ($valueInner['project_details']['project_status'] == 1) {
                            $onGoingProjects++;
                        }

                    }
                    $designerProfileData[$key]['projects_ongoing'] = $onGoingProjects;
                    if (!empty($value['ratings'])) {
                        foreach ($value['ratings'] as $keyInner => $valueInner) {
                            $array_rating[] = $valueInner['rating'];
                        }
                    }
                        $designerProfileData[$key]['average_ratings'] = (isset($array_rating) ? round(array_sum($array_rating) / count($array_rating), 1) : 0);

                }

                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $designerProfileData);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
            return $returnData;
        } catch (\Exception $e) {
            \DB::rollback();
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    // designer
    //###############################################################
    //Function Name : Designerworkhistoryandreviews
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : to display designer work history and reviews
    //In Params : Void
    //Return : json
    //Date : 3rd May 2018
    //###############################################################
    public function Designerworkhistoryandreviews(Request $request)
    {
        try {
            \DB::beginTransaction();
            $input = Input::all();

            //$getId=User::select('id')->where('slug',$input['id'])->get();
            $id                   = substr($input['id'], strpos($input['id'], ":") + 1);
            $assigned_designer_id = base64_decode($id);

            $designerWorkReviewDataQuery = MarketPlaceProjectsDetailInfo::
            whereHas('project_details_designer.review_for_project')
            ->whereHas('project_details_designer.rating_for_project')
            ->with([
                'project_details_designer' => function ($query) {
                },
            ])
            ->with('payment_details')
            ->where('status', 7)
            ->where('assigned_designer_id', $assigned_designer_id)->orderby('project_completed_at','DESC');


            $designerWorkReviewData = $designerWorkReviewDataQuery
                ->paginate(Config('constants.other.MP_DASHOARD_PAGINATE'))
                ->toArray();
            if ($designerWorkReviewData) {
                foreach ($designerWorkReviewData['data'] as $key => $value) {

                    $completed_project_data = $value;

                    $date = Carbon::parse($value['project_completed_at'])->timezone(Auth::user()->timezone);
                    $date_project_completed_on = UtilityController::Changedateformat($date, 'd M, Y');

                    $designerWorkReviewData['data'][$key]['project_completed_on'] = $date_project_completed_on;

                    $date = Carbon::parse($value['payment_details']['created_at'])->timezone(Auth::user()->timezone);
                    $designerWorkReviewData['data'][$key]['project_started_at']   = UtilityController::Changedateformat($date, 'd M, Y');

                    $designerWorkReviewData['data'][$key]['project_details_designer']['project_timeline'] = $value['project_details_designer']['project_timeline'] + $value['project_details_designer']['extended_days'];

                    $designerWorkReviewData['data'][$key]['project_details_designer']['project_creator']['image'] = UtilityController::Imageexist($value['project_details_designer']['project_creator']['image'], public_path('') . UtilityController::Getmessage('USER_PROFILE_PATH'), url('') . UtilityController::Getmessage('USER_PROFILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));

                    $designerWorkReviewData['data'][$key]['time_taken'] = Carbon::parse($value['payment_details']['created_at'])->diffInHours(Carbon::parse($value['project_completed_at']));
                    $hours                                              = $designerWorkReviewData['data'][$key]['time_taken'];
                    $days                                               = 0;

                    time:
                    if ($hours >= 24) {
                        $days++;
                        $hours -= 24;
                        goto time;
                    }
                    $designerWorkReviewData['data'][$key]['time_taken_days']  = $days;
                    $designerWorkReviewData['data'][$key]['time_taken_hours'] = $hours;

                    $message_count = 0;

                    $designerWorkReviewData['data'][$key]['message_count'] = $message_count;

                    if (!empty($designerWorkReviewData['data'][$key]['project_details_designer']['message_board'])) {
                        foreach ($designerWorkReviewData['data'][$key]['project_details_designer']['message_board'] as $keyInner => $valueInner) {
                            if ($designerWorkReviewData['data'][$key]['project_details_designer']['message_board'][$keyInner]['user_id'] == $input['id']) {

                                $message_count++;
                                $designerWorkReviewData['data'][$key]['message_count'] = $message_count;
                            }

                        }
                    }

                    if ($value['project_details_designer']['rating_for_project']['rating'] != 0 && $value['project_details_designer']['rating_for_project']['rating'] != '') {
                        $ratingMatrix = json_decode($value['project_details_designer']['rating_for_project']['matrix_rating'], true);

                        $designerWorkReviewData['data'][$key]['ratingMatrix'] = $ratingMatrix;
                    }
                    else{
                        unset($designerWorkReviewData['data'][$key]);
                    }

                }

                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $designerWorkReviewData);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }

            return $returnData;
        } catch (\Exception $e) {
            \DB::rollback();
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Deleteportfolioimage
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To delete images in uploaded portfolio(s) by designer
    //In Params : Void
    //Return : json
    //Date : 30th April 2018
    //###############################################################
    public function Acceptrejectextension(Request $request)
    {
        try {
            if (Auth::check()) {
                \DB::beginTransaction();
                $extensionMessage = '';
                $Input            = Input::all();                
                $data['extension_status'] = $Input['acceptReject'];
                $result                   = UtilityController::Makemodelobject($data, 'MarketPlaceProjectExtension', '', $Input['extensionDetails']['id']);
                if ($result) {                    
                    $sender_id   = $Input['project']['project_created_by_id'];
                    $receiver_id = $Input['project']['selected_designer']['assigned_designer_id'];
                    $slug        =$Input['project']['slug'];

                    if ($Input['acceptReject'] == 2) {
                        $extensionMessage                  = 'accepted ';
                        $data['extended_days']             = $Input['extensionDetails']['days'];
                        $projectExtension                  = UtilityController::Makemodelobject($data, 'MarketPlaceProject', '', $Input['extensionDetails']['mp_project_id']);
                        $returnMessage=$Input['language']=='en'?'EXTENSION_ACCEPTED':'EXTENSION_ACCEPTED_CHINESE';
                        $responseArray                     = UtilityController::Generateresponse(true, $returnMessage, 1);

                        $system_messsage_text = 'Buyer accepted timeline extension for "'.$Input['project']['project_title'].'"';
                        $system_message_text_chi = '买方接受时间延长"'.$Input['project']['project_title'].'"';

                        $msgboard_messsage_text = 'Buyer accepted timeline extension ';
                        $msgboard_messsage_text_chi = '买方接受时间延长';

                        $Input['extension_status_string']  = $Input['project']['selected_designer']['designer_details']['current_language']==1?"accepted":"公认";
                        $message_text_en='Buyer accepted request for extension at ';
                        $message_text_chi='买方接受延期请求。';
                        $status=1;
                        
                    } else {
                        $extensionMessage                  = ' rejected ';
                        $returnMessage=$Input['language']=='en'?'EXTENSION_REJECTED':'EXTENSION_REJECTED_CHINESE';
                        $responseArray                     = UtilityController::Generateresponse(true, $returnMessage, 1);
                        $system_messsage_text = 'Buyer rejected timeline extension for "'.$Input['project']['project_title'].'"';
                        $system_message_text_chi = '买方拒绝时间延期"'.$Input['project']['project_title'].'"';

                        $msgboard_messsage_text = 'Buyer rejected timeline extension';
                        $msgboard_messsage_text_chi = '买方接受时间延长';

                        $Input['extension_status_string']  = $Input['project']['selected_designer']['designer_details']['current_language']==1?"rejected":"拒绝";

                        $message_text_en='Buyer rejected request for extension at ';
                        $message_text_chi='买方拒绝了延期请求。';
                        $status=2;
                        
                    }
                    
                    self::Insertmessageboardnotifications($Input['extensionDetails']['designer_id'],$Input['extensionDetails']['mp_project_id'],$msgboard_messsage_text,$msgboard_messsage_text_chi,4,2,2);

                    self::Insertsystemnotifications($sender_id,$receiver_id,$system_messsage_text,$system_message_text_chi,$slug);

                    self::Insertadmintimelinenotifications($Input['project']['selected_designer']['assigned_designer_id'],$Input['extensionDetails']['mp_project_id'],$message_text_en,$message_text_chi,$status,0);
                    
                    $Input['subject']=$Input['project']['selected_designer']['designer_details']['current_language']==1?'Regarding: Timeline extension request':'关于：时间线延长请求';

                    $Input['content']=$Input['project']['selected_designer']['designer_details']['current_language']==1?'Hello <strong>'.$Input['project']['selected_designer']['designer_details']['first_name'].' '.$Input['project']['selected_designer']['designer_details']['last_name'].' ,
                        </strong><br/>
                        <br/>
                        Your request for extension of timeline for project <strong> '.$Input['project']['project_number'].' </strong> has been <strong> '.$Input['extension_status_string'].' </strong> by the buyer.<br><br>':'你好 <strong>'.$Input['project']['selected_designer']['designer_details']['first_name'].' '.$Input['project']['selected_designer']['designer_details']['last_name'].'</strong> ,
                        <br/>
                        <br/>
                        您延长项目时间表的请求<strong>'.$Input['project']['project_number'].' </ strong> 已由买方<strong> '.$Input['extension_status_string'].' </ strong><br> <br>';

                    $Input['footer_content']=$Input['project']['selected_designer']['designer_details']['current_language']==1?'From,<br /> SixClouds Sixteen':'六云十六 ';

                    $Input['footer']=$Input['project']['selected_designer']['designer_details']['current_language']==1?'SixClouds':'六云';

                    $mailResult = Mail::send('emails.email_template', $Input, function ($message) use ($Input) {
                        $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                        $message->to($Input['project']['selected_designer']['designer_details']['email_address'])
                            ->subject($Input['subject']);
                    });
                    /*Mail::send('emails.update_designer_for_extension_timeline', $Input, function ($message) use ($Input) {
                        $message->from('noreply@sixclouds.cn', 'SixClouds');
                        $message->to($Input['project']['selected_designer']['designer_details']['email_address'])->subject('');
                    });*/
                    \DB::commit();
                } else {
                    \DB::rollback();
                    $returnMessage=$Input['language']=='en'?'GENERAL_ERROR':'GENERAL_ERROR_CHINESE';
                    $responseArray = UtilityController::Generateresponse(false, $returnMessage, 0);
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
        }
        return $responseArray;
    }

    //###############################################################
    //Function Name : Searchdesigner
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To search the designer by his name
    //In Params : Void
    //Return : json
    //Date : 3rd may 2018
    //###############################################################
    public function Searchdesigner(Request $request)
    {
        try {
            $Input              = Input::all();
            $designerSuggestion = User::where(function ($query) use ($Input) {$query->where('first_name', 'like', '%' . $Input['name'] . '%')->orWhere('last_name', 'like', '%' . $Input['name'] . '%');})->where('marketplace_current_role', 1)->where('status', 1)->get();
            if ($designerSuggestion) {
                foreach ($designerSuggestion as $key => $value) {
                    $designerSuggestion[$key]['image'] = ($value['image'] != '' ? UtilityController::Imageexist($value['image'], public_path('') . UtilityController::Getmessage('USER_PROFILE_PATH'), url('') . UtilityController::Getmessage('USER_PROFILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL')) : url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));
                }
                $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $designerSuggestion);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
        }
        return $responseArray;
    }

    //###############################################################
    //Function Name : Getcompletedprojectsforportfolio
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To get the completed projects of designer for portfolio
    //In Params : Void
    //Return : json
    //Date : 3rd may 2018
    //###############################################################
    public function Getcompletedprojectsforportfolio(Request $request)
    {
        try {
            $userId            = Auth::user()->id;
            $completedProjects = MarketPlaceProjectsDetailInfo::whereHas('project_for_portfolio', function ($query) {
                $query->where('allow_for_portfolio', 1);
            })->where('assigned_designer_id', $userId)->where('status', 7)->get();
            if ($completedProjects) {
                foreach ($completedProjects as $key => $value) {
                    foreach ($value['project_for_portfolio']['message_board_for_portfolio'] as $keyInner => $valueInner) {
                        $valueInner['image_file'] = UtilityController::Imageexist($valueInner['message'], public_path('') . UtilityController::Getmessage('MSG_FILE_PATH'), url('') . UtilityController::Getmessage('MSG_FILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));
                    }
                }
                $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $completedProjects);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
        }
        return $responseArray;
    }

    //###############################################################
    //Function Name : Makeprojectportfolio
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To make the project a portfolio
    //In Params : Void
    //Return : json
    //Date : 26th April 2018
    //###############################################################
    public function Makeprojectportfolio(Request $request)
    {
        try {
            \DB::beginTransaction();
            $Input = Input::all();
            if (!array_key_exists('image', $Input)) {
                $returnMessage = ($Input['language']=='en'?'SELECT_ATLEAST_ONE_IMAGE':'SELECT_ATLEAST_ONE_IMAGE_CHINESE');
                throw new \Exception(UtilityController::Getmessage($returnMessage));
            }
            $Input['whichProject']                  = json_decode($Input['whichProject'], true);
            $projectPortfolio['uploaded_user_id']   = Auth::user()->id;
            $projectPortfolio['upload_title']       = $Input['whichProject']['project_for_portfolio']['project_title'];
            $projectPortfolio['upload_description'] = $Input['whichProject']['project_for_portfolio']['project_description'];
            $projectPortfolio['upload_of']          = $Input['upload_of'];
            $projectPortfolio['portfolio_type']     = $Input['portfolio_type'];
            $resultUpload                           = UtilityController::Makemodelobject($projectPortfolio, 'MarketPlaceUpload');
            if ($resultUpload) {

                $skillSets = $Input['whichProject']['project_for_portfolio']['skills'];
                foreach ($skillSets as $key => $value) {
                    $portfolioCategory[$key]['mp_upload_id'] = $resultUpload['id'];
                    $portfolioCategory[$key]['categoty_id']  = $value['skill_set_id'];
                    $portfolioCategory[$key]['created_at']   = Carbon::now();
                    $portfolioCategory[$key]['updated_at']   = Carbon::now();
                }
                MarketPlacePortfolioCategoryDetail::insert($portfolioCategory);
                foreach ($Input['image'] as $key => $value) {
                    $forCover                               = $key;
                    $value                                  = json_decode($value, true);
                    $fileName                               = date("dmYHis") . rand();
                    $path                                   = public_path('') . UtilityController::getMessage('MSG_FILE_PATH') . $value['message'];
                    $uploadAttachment[$key]['mp_upload_id'] = $resultUpload['id'];

                    ## original image start
                    $originalThumbnail = "original_" . $fileName;
                    $result            = self::Resizeimage($value, 400, 400, $originalThumbnail, $path, 'MP_PORTFOLIO_ATTACHMENT_ORIGINAL_PATH');
                    if ($result) {
                        $uploadAttachment[$key]['original_file_name'] = $originalThumbnail;
                    }
                    ## original image end

                    ## medium image start
                    $mediumThumbnail = "medium_" . $fileName;
                    $result          = self::Resizeimage($value, 180, 180, $mediumThumbnail, $path, 'MP_PORTFOLIO_ATTACHMENT_MEDIUM_PATH');
                    if ($result) {
                        $uploadAttachment[$key]['medium_thumbnail'] = $mediumThumbnail;
                    }
                    ## medium image end

                    ## tiny image start
                    $tinyThumbnail = "tiny_" . $fileName;
                    $result        = self::Resizeimage($value, 33, 33, $tinyThumbnail, $path, 'MP_PORTFOLIO_ATTACHMENT_TINY_PATH', 1);
                    if ($result) {
                        $uploadAttachment[$key]['tiny_thumbnail'] = $tinyThumbnail;
                    }
                    ## tiny image end
                    $uploadAttachment[$key]['created_at'] = Carbon::now();
                    $uploadAttachment[$key]['updated_at'] = Carbon::now();
                    if ($key == 0) {
                        ## cover image if not uploaded by user start
                        $coverImg = "coverImg_" . $fileName;
                        $result   = self::Resizeimage($Input['image'][$forCover], 220, 220, $coverImg, $path, 'MP_PORTFOLIO_ATTACHMENT_COVER_IMAGE_PATH');
                        if ($result) {
                            $Input['cover_img'] = $coverImg;
                        }
                        ## cover image if not uploaded by user end
                    }
                }
                MarketPlaceUploadAttachment::insert($uploadAttachment);
                $slug['slug']      = str_slug($Input['whichProject']['project_for_portfolio']['project_title'], '-');
                $slug['slug']      = $slug['slug'] . ":" . base64_encode($resultUpload['id']);
                $slug['cover_img'] = $coverImg;
                $updateSlug        = UtilityController::Makemodelobject($slug, 'MarketPlaceUpload', '', $resultUpload['id']);

                $updateProjectStatus['allow_for_portfolio'] = 3;
                $updatestatus                               = UtilityController::Makemodelobject($updateProjectStatus, 'MarketPlaceProject', '', $Input['whichProject']['project_for_portfolio']['id']);

                \DB::commit();
                $responseArray = UtilityController::Generateresponse(true, 'PORTFOLIO_UPLOADED', 1);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, $e->getMessage(), 0);
        }
        return $responseArray;
    }

    //###############################################################
    //Function Name : Getprojectinfo
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To get the project details for portfolio
    //In Params : Void
    //Return : json
    //Date : 10th May 2018
    //###############################################################
    public function Getprojectinfo(Request $request)
    {
        try {
            $Input         = Input::all();
            if(array_key_exists('slug', $Input)){   
                $result        = MarketPlaceProject::with('skills')->where('slug', $Input['slug'])->first();
                $selectedSkills = $result['skills']; 
                $skills = SkillSet::where('status',1)->get();
                foreach ($skills as $key => $value) {
                    foreach ($selectedSkills as $keyInner => $valueInner) {
                        if($value['id']==$valueInner['skill_set_id'])
                            $value['select'] = 1;
                        else 
                            $value['select'] = 0;
                    }
                }
                $result['skills_set'] = $skills;
            }
            else
                $result        = MarketPlaceProject::where('id', $Input['id'])->first();
            
            $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $result);
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
        }
        return $responseArray;
    }

    // designer
    //###############################################################
    //Function Name : Acceptprojectcomplitionrequest
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : Accept or reject project completion request
    //In Params : Void
    //Return : json
    //Date : 26th April 2018
    //###############################################################
    public function Acceptprojectcomplitionrequest(Request $request)
    {
        try {

            \DB::beginTransaction();
            $Input                           = Input::all();
            $Input['project_id']             = $mp_project_id = substr($Input['project_id'], strpos($Input['project_id'], ":") + 1);
            $mp_project_id                   = base64_decode($mp_project_id);
            $projectData                     = MarketPlaceProject::with('selected_designer')->where('id',$mp_project_id)->first();
            $designerData                    = User::where('id', $Input['designer_id'])->first();
            $buyerData['project']            = $projectData['project_title'];
            $buyerData['project_number']     = $projectData['project_number'];
            $buyerData['designerfirst_name'] = $designerData['first_name'];
            $buyerData['designerlast_name']  = $designerData['last_name'];
            $buyerData['current_language']   = $designerData['current_language'];
            if ($Input['status']             == 3) {
                MarketplaceProject::where('id', $mp_project_id)->update(array('project_status' => $Input['status']));
                $result = MarketPlaceProjectsDetailInfo::where('mp_project_id', $mp_project_id)
                    ->where('assigned_designer_id', $Input['designer_id'])
                    ->update(array('status' => 7,'project_completed_at' => date('Y-m-d H:i:s')));

                //$designerData['email_address']
                $designerData['subject']=$buyerData['current_language']==1?'Project complete request accepted':'已接受项目完成请求';

                $buyerData['content']=$buyerData['current_language']==1?'Hello <strong>'.$buyerData['designerfirst_name'].' '.$buyerData['designerlast_name'].' ,
                    </strong><br/>
                    <br/>
                    Congratulations! Your Project  <strong>ID: '.$buyerData['project_number'].' </strong>  has been marked Complete.
                    <br/><br/><br/>Do remember to delete all copies of Project related files that reside outside of the SixClouds Sixteen platform.
                    <br/><br/><br/>':'你好 <strong>'.$buyerData['designerfirst_name'].$buyerData['designerlast_name'].'</strong> ,
                    <br/>
                    <br/>
                    恭喜！ 您的项目 <strong>ID: '.$buyerData['project_number'].' </strong>  已被标记为完成。<br/><br/><br/>请记得删除六云十六平台之外的项目相关文件的所有副本。 <br/><br/><br/>';

                $buyerData['footer_content']=$buyerData['current_language']==1?'From,<br /> SixClouds Sixteen':'六云十六 ';

                $buyerData['footer']=$buyerData['current_language']==1?'SixClouds':'六云';

                $mailResult = Mail::send('emails.email_template', $buyerData, function ($message) use ($designerData) {
                    $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                    $message->to($designerData['email_address'])
                        ->subject($designerData['subject']);
                });

                $notifyBuyer = (new MpNotifyBuyerForRateAndReview($mp_project_id,24))->delay(Carbon::now()->addHours(24));
                dispatch($notifyBuyer);

                $buyerDetails['buyerfirst_name'] = Auth::user()->first_name;
                $buyerDetails['buyerlast_name']  = Auth::user()->last_name;
                $buyerDetails['email_address']   = Auth::user()->email_address;
                $buyerDetails['current_language']   = Auth::user()->current_language;

                //$buyerDetails['email_address']

                $buyerDetails['subject']=$buyerData['current_language']==1?'SixClouds : Congratulations! Your Project has been marked Complete.':'六云 : 恭喜！您的项目已标记为完成。';

                $buyerDetails['content']=$buyerData['current_language']==1?'Hello <strong>'.$buyerDetails['buyerfirst_name'].' '.$buyerDetails['buyerlast_name'].' ,
                    </strong><br/>
                    <br/>
                    Thank you for using SixClouds Sixteen.
                    <br/><br/><br/>Please note that all copies of Project related files, will be permanently removed from our site, 14 days from the date of this notification.
                    <br/>':'你好 <strong>'.$buyerData['designerfirst_name'].$buyerData['designerlast_name'].'</strong> ,
                    <br/>
                    <br/>
                    感谢您使用六云十六.
                    <br/><br/><br/>请切记，项目所有相关的文件将在本通知14天后从我们的网站永久删除。
                     <br/>';

                $buyerDetails['footer_content']=$buyerData['current_language']==1?'From,<br /> SixClouds Customer Support':'六云客服中心 ';

                $buyerDetails['footer']=$buyerData['current_language']==1?'SixClouds':'六云';
                $mail = Mail::send('emails.email_template', $buyerDetails, function ($message) use ($buyerDetails) {
                    $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                    $message->to($buyerDetails['email_address'])
                        ->subject($buyerDetails['subject']);
                });
                $system_notification_text = 'Buyer accepted completion request for project "'.$projectData['project_title'].'"';
                $system_notification_text_chi = '买方接受项目的完成请求"'.$projectData['project_title'].'"';

                $msgboard_messsage_text='Buyer accepted project completion request';
                $msgboard_messsage_text_chi='买方接受了项目完成请求';

                $message_text_en='Buyer accepted for completion of project at ';
                $message_text_chi='买方接受完成项目';
                $status=1;
                
            } else {
                //$designerData['email_address']
                MarketplaceProject::where('id', $mp_project_id)->update(array('project_status' => 1));
                $result = MarketPlaceProjectsDetailInfo::where('mp_project_id', $mp_project_id)
                    ->where('assigned_designer_id', $Input['designer_id'])
                    ->update(array('status' => 8));

                $buyerData['subject']=$buyerData['current_language']==1?'SixClouds : Project complete request rejected.':'六云 : 项目完成请求被拒绝';

                $buyerData['content']=$buyerData['current_language']==1?'Hello <strong>'.$buyerData['designerfirst_name'].' '.$buyerData['designerlast_name'].' ,
                    </strong><br/>
                    <br/>
                    Your completion request for Project '.$buyerData['project_number'] .' has beed rejected by buyer.
                    <br/>':'你好 <strong>'.$buyerData['designerfirst_name'].$buyerData['designerlast_name'].'</strong> ,
                    <br/>
                    <br/>
                    您对项目 '.$buyerData['project_number'].' 的完成请求已被买方拒绝
                     <br/>';

                $buyerData['footer_content']=$buyerData['current_language']==1?'From,<br /> SixClouds Customer Support':'六云客服中心 ';

                $buyerData['footer']=$buyerData['current_language']==1?'SixClouds':'六云';

                $mail = Mail::send('emails.email_template', $buyerData, function ($message) use ($designerData) {
                    $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                    $message->to($designerData['email_address'])
                        ->subject($designerData['subject']);
                });
                /*$mailResult = Mail::send('emails.prj_completionreq_rejected', $buyerData, function ($message) use ($designerData) {
                    $message->from('noreply@sixclouds.cn', 'Sixclouds');
                    $message->to($designerData['email_address'])
                        ->subject('Sixclouds : Project request rejected.');
                });*/
                $system_notification_text = 'Buyer rejected completion request for project "'.$projectData['project_title'].'"';
                $system_notification_text_chi = '买方拒绝了项目的完成请求"'.$projectData['project_title'].'"';

                $msgboard_messsage_text='Buyer rejected project completion request';
                $msgboard_messsage_text_chi='买方拒绝了项目完成请求';

                $message_text_en='Buyer rejected for completion of project at ';
                $message_text_chi='买方拒绝完成项目';
                $status=2;
                
            }
            if ($result) {
                
                self::Insertsystemnotifications($projectData['project_created_by_id'],$projectData['selected_designer']['assigned_designer_id'],$system_notification_text,$system_notification_text_chi,$projectData['slug']);

                self::Insertmessageboardnotifications($projectData['selected_designer']['assigned_designer_id'],$mp_project_id,$msgboard_messsage_text,$msgboard_messsage_text_chi,4,2,2);

                self::Insertadmintimelinenotifications(Auth::user()->id,$mp_project_id,$message_text_en,$message_text_chi,$status,0);
                

                $returnData     = UtilityController::Generateresponse(true, '', 200, '');
            } else {
                $returnData = UtilityController::Generateresponse(false, '', 400, '');
            }
            \DB::commit();

            return $returnData;
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    // designer
    //###############################################################
    //Function Name : Addprojectcompletionrequestrejectreason
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To add reson to reject project completion request
    //In Params : Void
    //Return : json
    //Date : 4th May 2018
    //###############################################################
    public function Addprojectcompletionrequestrejectreason(Request $request)
    {
        try {
            \DB::beginTransaction();
            $Input                = Input::all();
            $Input['designer_id'] = $Input['designer_id'];
            $Input['project_id']  = $mp_project_id  = substr($Input['project_id'], strpos($Input['project_id'], ":") + 1);
            $mp_project_id        = base64_decode($mp_project_id);
            $result               = UtilityController::Makemodelobject($Input, 'MarketPlaceProjectCompetionReqRejectReason', '');
            if ($result) {
                $projectData                     = MarketPlaceProject::where('id', $mp_project_id)->first();
                $designerData                    = User::find($Input['designer_id']);
                $buyerData['project']            = $projectData['project_title'];
                $buyerData['designerfirst_name'] = $designerData['first_name'];
                $buyerData['designerlast_name']  = $designerData['last_name'];
                $buyerData['current_language']  = $designerData['current_language'];
                $buyerData['reason']             = $Input['reason'];

                $mailResult = Mail::send('emails.reason_to_reject_prjCmpltReq', $buyerData, function ($message) use ($designerData) {
                    $message->from(Config('constants.messages.MAIL_ID'), 'Sixclouds');
                    $message->to($designerData['email_address'], 'komal@creolestudios.com')
                        ->subject('Sixclouds : Project request rejected.');
                });
                $returnMessage=$Input['language']=='en'?'SUBMITED':'SUBMITED_CHINESE';
                $returnData = UtilityController::Generateresponse(true, $returnMessage, 200, '');
            } else {
                $returnData = UtilityController::Generateresponse(false, '', 400, '');
            }
            \DB::commit();
            return $returnData;
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Sendinvitationfordesignersignup
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To send an invitation for new designer signup
    //In Params : Void
    //Return : json
    //Date : 8th May 2018
    //###############################################################
    public function Sendinvitationfordesignersignup(Request $request)
    {
        try {
            if (Auth::check()) {
                \DB::beginTransaction();
                $Input = Input::all();
                $noOfInvitation = MarketPlaceDesignerSignupInvitation::where('invited_by',Auth::user()->id)->count();
                if($noOfInvitation<2){
                    if ($Input['email_address'] == Auth::user()->email_address) {
                        $returnMessage=$Input['language']=='en'?'NO_INVITATION_TO_SELF':'NO_INVITATION_TO_SELF_CHINESE';
                        throw new \Exception(UtilityController::Getmessage($returnMessage));
                    } 
                    else {

                        $emailExists = MarketPlaceDesignerSignupInvitation::where('email_address', $Input['email_address'])->count();
                        
                        if($emailExists){
                            $returnMessage=$Input['language']=='en'?'INVITATION_ALREADY_SENT':'INVITATION_ALREADY_SENT_CHINESE';
                            throw new \Exception(UtilityController::Getmessage($returnMessage));    
                        }
                        else{
                            $existsUser = User::where('email_address', $Input['email_address'])->where('is_designer', 1)->count();
                            
                            if($existsUser){
                                    $returnMessage=$Input['language']=='en'?'INVITATION_NOT_SENT_TO_USER':'INVITATION_NOT_SENT_TO_USER_CHINESE';
                                    throw new \Exception(UtilityController::Getmessage($returnMessage));
                            }
                            else{
                                $emailExists = UserEmailLog::where('new_email_address', $Input['email_address'])->orWhere('old_email_address', $Input['email_address'])->count();
                    
                                if($emailExists){
                                        $returnMessage=$Input['language']=='en'?'INVITATION_ALREADY_SENT_EMAIL_CHANGE':'INVITATION_ALREADY_SENT_EMAIL_CHANGE_CHINESE';
                                        throw new \Exception(UtilityController::Getmessage($returnMessage));
                                }
                                else{

                                    $Input['invited_by']      = Auth::user()->id;
                                    $Input['invitee']         = json_decode(json_encode(User::where('id', Auth::user()->id)->first()), true);
                                    $Input['invitation_link'] = url('/') . '/seller-signup/' . base64_encode($Input['email_address']);
                                    $validator                = UtilityController::ValidationRules($Input, 'MarketPlaceDesignerSignupInvitation');
                                    if (!$validator['status']) {
                                        $errorMessage  = explode('.', $validator['message']);
                                        $responseArray = UtilityController::Generateresponse(false, $errorMessage['0'], 0);
                                    } else {
                                       
                                        $result = UtilityController::Makemodelobject($Input, 'MarketPlaceDesignerSignupInvitation');
                                        if ($result) {
                                            $changeCount['invitation_signup_slot'] = $Input['invitee']['invitation_signup_slot'] - 1;
                                            if ($changeCount <= 0) {
                                                $changeCount = 0;
                                            }

                                            $resultChangeCount = UtilityController::Makemodelobject($changeCount, 'User', '', $Input['invited_by']);
                                            $mailResult = Mail::send('emails.notify_designer_for_signup', $Input, function ($message) use ($Input) {
                                                $message->from(Config('constants.messages.MAIL_ID'), 'Six clouds');
                                                $message->to($Input['email_address'])->subject('Six clouds : Invitation for Sign-up');
                                            });
                                            $returnMessage = ($Input['language']=='en'?'INVITATION_SENT':'INVITATION_SENT_CHINESE');
                                            $responseArray = UtilityController::Generateresponse(true, $returnMessage, 1);
                                            \DB::commit();
                                        }
                                    }

                                }

                            }
                            
                        }
                            
                    }  
                }
                else{
                    $returnMessage = ($Input['language']=='en'?'INVITATION_MORE_THAN_TWO':'INVITATION_MORE_THAN_TWO_CHINESE');
                                            
                    throw new \Exception(UtilityController::Getmessage($returnMessage));
                }
                
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, $e->getMessage(), 0);
        }
        return $responseArray;
    }

    //###############################################################
    //Function Name : Decodeemail
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To decode the email sent into invitation for designer signup
    //In Params : Void
    //Return : json
    //Date : 8th May 2018
    //###############################################################
    public function Decodeemail(Request $request)
    {
        try {
            $Input = Input::all();
            $email = base64_decode($Input['email_address']);

            $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $email);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
        }
        return $responseArray;
    }

    //###############################################################
    //Function Name : Getsentinvitationdetails
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To get the data of signup invitations sent
    //In Params : Void
    //Return : json
    //Date : 8th May 2018
    //###############################################################
    public function Getsentinvitationdetails(Request $request)
    {
        try {
            $userId = Auth::user()->id;
            $data   = MarketPlaceDesignerSignupInvitation::where('invited_by', $userId)->get();
            foreach ($data as $key => $value) {
                $date = Carbon::parse($value['created_at'])->timezone(Auth::user()->timezone);
                $value['invitation_sent_on'] = UtilityController::Changedateformat($date, 'd M, Y');
            }
            $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $data);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
        }
        return $responseArray;
    }

    // designer
    //###############################################################
    //Function Name : Addwithdrawrequest
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To add withdraw request
    //In Params : Void
    //Return : json
    //Date : 10th May 2018
    //###############################################################
    public function Addwithdrawrequest(Request $request)
    {
        try {
            \DB::beginTransaction();
            $Input            = Input::all();
            $Input['user_id'] = Auth::user()->id;

            $Input['mp_project_id']  = $Input['projectId'];
            $Input['payment_status'] = 1;

            $projectData = MarketPlaceProject::where('id', $Input['mp_project_id'])->get();
            // Calculated resrtiction amount

            $alreadyRequested = MarketplaceSellerPayments::where('mp_project_id', $Input['mp_project_id'])->where('user_id', $Input['user_id'])->where('payment_status', 1)->get();

            $array_total_withdraw = [];
            if (count($alreadyRequested) == 1) {
                $returnMessage=$Input['language']=='en'?'RESPONSE_FOR_LAST_WITHDRAW_REQUEST':'RESPONSE_FOR_LAST_WITHDRAW_REQUEST_CHINESE';
                throw new \Exception(UtilityController::Getmessage($returnMessage));
            }
            if (count($alreadyRequested) > 0) {
                $total_amount = $alreadyRequested[0]['total_amount'];
                $total_amount = $total_amount + $Input['total_amount'];
                if ($total_amount > $projectData[0]['project_budget']) {
                    $returnMessage=$Input['language']=='en'?'WITHDRAW_UPTO_BUDGET':'WITHDRAW_UPTO_BUDGET_CHINESE';
                    throw new \Exception(UtilityController::Getmessage($returnMessage));
                } else {
                    $result = UtilityController::Makemodelobject($Input, 'MarketplaceSellerPayments', '');
                }
            } else {
                if ($Input['total_amount'] <= $projectData[0]['project_budget']) {
                    $result = UtilityController::Makemodelobject($Input, 'MarketplaceSellerPayments', '');
                } else {
                    $returnMessage=$Input['language']=='en'?'WITHDRAW_UPTO_BUDGET':'WITHDRAW_UPTO_BUDGET_CHINESE';
                    throw new \Exception(UtilityController::Getmessage($returnMessage));
                }
            }

            if ($result) {
                // Withdraw request
                
                $data['mp_project_id'] = $Input['mp_project_id'];
                $data['action_type']   = 2;

                $returnMessage=$Input['language']=='en'?'SELLER_REQUESTED_WITHDRAW':'SELLER_REQUESTED_WITHDRAW_CHINESE';
                $data['action_text']   = UtilityController::Getmessage($returnMessage);
                $data['show_first']    = 1;
                $actionResult          = UtilityController::Makemodelobject($data, 'MarketplaceAction');
                $actionResult          = UtilityController::Makemodelobject($data, 'MarketPlaceProject', '', $Input['mp_project_id']);

                $projectDetails                        = MarketPlaceProject::with('selected_designer')->where('id',$Input['mp_project_id'])->first();

                self::Insertsystemnotifications($projectDetails['selected_designer']['assigned_designer_id'],$projectDetails['project_created_by_id'],'Seller requested milestone payment for project "'.$projectDetails['project_title'].'" of ¥'.$Input['total_amount'],'卖方请求项目的里程碑付款"'.$projectDetails['project_title'].'" 的 ¥'.$Input['total_amount'],$projectDetails['slug']);

                self::Insertmessageboardnotifications($Input['user_id'],$Input['mp_project_id'],'Seller requested milestone payment for project "'.$projectDetails['project_title'].'" of ¥'.$Input['total_amount'],'卖方请求项目的里程碑付款"'.$projectDetails['project_title'].'" 的 ¥'.$Input['total_amount'],4,1,1);

                $calculate_persentage=(100*$Input['total_amount'])/$projectData[0]['project_budget'];
                $calculate_persentage=sprintf('%0.2f', $calculate_persentage);

                self::Insertadmintimelinenotifications($Input['user_id'],$Input['mp_project_id'],'Seller requested '.$calculate_persentage.'%  of the escrow at','卖方要求 '.$calculate_persentage.'  % 的代管人',3,0,$Input['total_amount']);

                $data['project_number']=$projectDetails['project_number'];
                $data['email_address']=$projectDetails['selected_designer']['designer_details']['email_address'];

                $data['subject']=$projectDetails['selected_designer']['designer_details']['current_language']==1?'Milestone payment request.':'里程碑付款请求';

                $data['content']=$projectDetails['selected_designer']['designer_details']['current_language']==1?'Hello <strong>'.$projectDetails['selected_designer']['designer_details']['first_name'].' '.$projectDetails['selected_designer']['designer_details']['last_name'].' ,
                    </strong><br/>
                    <br/>
                    Your request for Milestone payment for Project  <strong>ID: '.$data['project_number'].' </strong> has been sent to Buyer.
                    <br/><br/><br/>':'你好 <strong>'.$projectDetails['selected_designer']['designer_details']['first_name'].' '.$projectDetails['selected_designer']['designer_details']['last_name'].'</strong> ,
                    <br/>
                    <br/>
                    项目  <strong>ID: '.$data['project_number'].'</strong> 的里程碑付款请求已发送给买家。<br/><br/><br/>';

                $data['footer_content']=$projectDetails['selected_designer']['designer_details']['current_language']==1?'From,<br /> SixClouds Sixteen':'六云十六 ';

                $data['footer']=$projectDetails['selected_designer']['designer_details']['current_language']==1?'SixClouds':'六云';

                Mail::send('emails.email_template', $data, function ($message) use ($data) {
                    $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                    $message->to($data['email_address'])->subject($data['subject']);
                });

                //Buyer Mail
                $projectDetails = MarketPlaceProject::with('projectCreator')->where('id',$Input['mp_project_id'])->first()->toArray();
                $buyerData['email_address']=$projectDetails['project_creator']['email_address'];
                $buyerData['subject']=$projectDetails['project_creator']['current_language'] ==1?'Seller request for milestone payment':'卖方要求里程碑付款';

                $buyerData['content']=$projectDetails['project_creator']['current_language']==1?'Hello <strong>'.$projectDetails['project_creator']['first_name'].' '.$projectDetails['project_creator']['last_name'].' ,
                </strong><br/>
                <br/>
                Seller has requested Milestone payment for Project <strong>'.$projectDetails['project_number'].'</strong>. Please indicate your decision <a href="'.URL::to('/buyer-project-info/'.$projectDetails['slug']).'">here</a>.

                <br/<br/><br/>':'你好 <strong>'.$projectDetails['project_creator']['first_name'].' '.$projectDetails['project_creator']['last_name'].'</strong> ,
                <br/>
                <br/>
                卖家要求为项目 <strong>'.$projectDetails['project_number'].'</strong> 采取里程碑付款模式。 请点击<a href="'.URL::to('/buyer-project-info/'.$projectDetails['slug']).'">这里</a>表明您的决定。<br/<br/><br/>';

                $buyerData['footer_content']=$projectDetails['project_creator']['current_language']==1?'From,<br /> SixClouds Sixteen':'六云十六 ';

                $buyerData['footer']=$projectDetails['project_creator']['current_language']==1?'SixClouds':'六云';

                Mail::send('emails.email_template', $buyerData, function ($message) use ($buyerData) {
                    $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                    $message->to($buyerData['email_address'])->subject($buyerData['subject']);
                });
                
                \DB::commit();
                /*For entry in the actions table for withdraw*/
                $data['mp_project_id'] = $Input['mp_project_id'];
                $data['action_type']   = 1;
                $data['show_first']    = 1;
                $actionResult          = UtilityController::Makemodelobject($data, 'MarketplaceAction');
                $actionResult          = UtilityController::Makemodelobject($data, 'MarketPlaceProject', '', $Input['mp_project_id']);
                /*For entry in the actions table for withdraw*/
                $returnMessage=$Input['language']=='en'?'WITHDRAW_SUBMITED':'WITHDRAW_SUBMITED_CHINESE';
                $returnData = UtilityController::Generateresponse(true, $returnMessage, 200, '');
            } else {
                $returnData = UtilityController::Generateresponse(false, '', 400, '');
            }
            \DB::commit();
            return $returnData;
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, $e->getMessage(), '', '');
            return $returnData;
        }
    }

    // designer
    //###############################################################
    //Function Name : paymentStatusChange
    //Author : Komal Kapadi <zalak@creolestudios.com>
    //Purpose : To change status of payment
    //In Params : Void
    //Return : json
    //Date : 10th May 2018
    //###############################################################
    public function Paymentstatuschange(Request $request)
    {
        $Input = Input::all();
        try {
            \DB::beginTransaction();
            $result = MarketplaceSellerPayments::where('id', $Input['id'])->update(array('payment_status' => $Input['status']));

            if ($Input['status'] == 3) {
                if (isset($Input['reason']) && $Input['reason'] != '') {
                    $result = MarketplaceSellerPayments::where('id', $Input['id'])->update(array('reject_reason' => $Input['reason']));

                    $paymentDetail = MarketplaceSellerPayments::where('id', $Input['id'])->first();
                    // Withdraw request
                    self::Insertmessageboardnotifications(Auth::user()->id,$paymentDetail['mp_project_id'],'Buyer rejected request for milestone payment','买方接受里程碑付款请求',4,2,2);

                    $projectDetails                        = MarketPlaceProject::with('selected_designer')->where('id',$paymentDetail['mp_project_id'])->first();
                    
                    self::Insertsystemnotifications($projectDetails['project_created_by_id'],$projectDetails['selected_designer']['assigned_designer_id'],'Buyer rejected milestone payment for "'.$projectDetails['project_title'].'"','买方拒绝了里程碑付款"'.$projectDetails['project_title'].'"',$projectDetails['slug']);


                    self::Insertadmintimelinenotifications($projectDetails['project_created_by_id'],$paymentDetail['mp_project_id'],'Buyer rejected the request for payment at ',' 买方拒绝了发布请求 ',5,0);

                    $data['project_number']=$projectDetails['project_number'];
                    $data['email_address']=$projectDetails['selected_designer']['designer_details']['email_address'];

                    $data['subject']=$projectDetails['selected_designer']['designer_details']['current_language']==1?'Milestone payment request rejected.':'里程碑付款请求被拒绝';

                    $data['content']=$projectDetails['selected_designer']['designer_details']['current_language']==1?'Hello <strong>'.$projectDetails['selected_designer']['designer_details']['first_name'].' '.$projectDetails['selected_designer']['designer_details']['last_name'].' ,
                        </strong><br/>
                        <br/>
                        Your Milestone payment request for Project  <strong>ID: '.$data['project_number'].' </strong> has been rejected by Buyer.
                        <br/><br/><br/>':'你好 <strong>'.$projectDetails['selected_designer']['designer_details']['first_name'].' '.$projectDetails['selected_designer']['designer_details']['last_name'].'</strong> ,
                        <br/>
                        <br/>
                        您的项目  <strong>ID: '.$data['project_number'].'</strong> 里程碑付款请求已被买家拒绝。   <br/><br/>';

                    $data['footer_content']=$projectDetails['selected_designer']['designer_details']['current_language']==1?'From,<br /> SixClouds Sixteen':'六云十六 ';

                    $data['footer']=$projectDetails['selected_designer']['designer_details']['current_language']==1?'SixClouds':'六云';

                    Mail::send('emails.email_template', $data, function ($message) use ($data) {
                        $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                        $message->to($data['email_address'])->subject($data['subject']);
                    });
                       

                    \DB::commit();
                    $returnMessage = ($Input['language']=='en'?'REQUEST_REJECTED':'REQUEST_REJECTED_CHINESE');
                    $returnData = UtilityController::Generateresponse(true, $returnMessage, 200, '');
                    return $returnData;
                }

            } else {
                $paymentDetail = MarketplaceSellerPayments::where('id', $Input['id'])->first();
                // Withdraw request
                
                $projectDetails                        = MarketPlaceProject::with('selected_designer')->where('id',$paymentDetail['mp_project_id'])->first();
                self::Insertmessageboardnotifications(Auth::user()->id,$paymentDetail['mp_project_id'],'Buyer accepted request for milestone payment','买方拒绝了里程碑付款请求',4,2,2);
                
                self::Insertsystemnotifications($projectDetails['project_created_by_id'],$projectDetails['selected_designer']['assigned_designer_id'],'Buyer accepted milestone payment for "'.$projectDetails['project_title'].'"','买方接受里程碑付款"'.$projectDetails['project_title'].'"',$projectDetails['slug']);
                
                self::Insertadmintimelinenotifications($projectDetails['project_created_by_id'],$paymentDetail['mp_project_id'],'Buyer approved the request for payment at ','买方批准了发布请求 ',4,0);

                $data['project_number']=$projectDetails['project_number'];
                $data['email_address']=$projectDetails['selected_designer']['designer_details']['email_address'];

                $data['subject']=$projectDetails['selected_designer']['designer_details']['current_language']==1?'Milestone payment request accepted.':'接受里程碑付款请求';

                $data['content']=$projectDetails['selected_designer']['designer_details']['current_language']==1?'Hello <strong>'.$projectDetails['selected_designer']['designer_details']['first_name'].' '.$projectDetails['selected_designer']['designer_details']['last_name'].' ,
                    </strong><br/>
                    <br/>
                    Your Milestone payment request for Project <strong>ID: '.$data['project_number'].' </strong> has been accepted by Buyer.
                    <br/><br/><br/>':'你好 <strong>'.$projectDetails['selected_designer']['designer_details']['first_name'].' '.$projectDetails['selected_designer']['designer_details']['last_name'].'</strong> ,
                    <br/>
                    <br/>
                    您的项目  <strong>ID: '.$data['project_number'].'</strong> 里程碑付款求已被买家接受。<br/><br/>';

                $data['footer_content']=$projectDetails['selected_designer']['designer_details']['current_language']==1?'From,<br /> SixClouds Sixteen':'六云十六 ';

                $data['footer']=$projectDetails['selected_designer']['designer_details']['current_language']==1?'SixClouds':'六云';

                Mail::send('emails.email_template', $data, function ($message) use ($data) {
                    $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                    $message->to($data['email_address'])->subject($data['subject']);
                });

                \DB::commit();
                $returnMessage = ($Input['language']=='en'?'REQUEST_ACCEPTED':'REQUEST_ACCEPTED_CHINESE');
                $returnData = UtilityController::Generateresponse(true,$returnMessage, 200, '');
                return $returnData;
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }

    }
    // designer
    //###############################################################
    //Function Name : Readmessages
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To change read/unread status of message
    //In Params : Void
    //Return : json
    //Date : 14th May 2018
    //###############################################################
    public function Readmessages(Request $request)
    {
        try {
            \DB::beginTransaction();
            $userId        = Auth::user()->id;
            $input         = Input::all();
            $userData      = Auth::user();
            $mp_project_id = substr($input['slug'], strpos($input['slug'], ":") + 1);
            $mp_project_id = base64_decode($mp_project_id);
            
            if ($userData['marketplace_current_role'] == 1) { ##if seller##
                //$result = MarketPlaceMessageBoard::where('user_id', $userId)->update(array('message_status' => 1));
                $result = MarketPlaceMessageBoard::where('mp_project_id', $mp_project_id)->update(array('message_status_seller' => 1));
            } elseif($userData['marketplace_current_role'] == 2) {##if buyer##
                $result = MarketPlaceMessageBoard::where('mp_project_id', $mp_project_id)->update(array('message_status_buyer' => 1));
            }
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Readnotifications
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : To change read/unread status of notification
    //In Params : Void
    //Return : json
    //Date : 13th August 2018
    //###############################################################
    public function Readnotifications(Request $request)
    {
        try {
            \DB::beginTransaction();
            
            $result = SystemNotification::where('module', 1)->update(array('read_unread' => 1));
            
            if($result){
                \DB::commit();    
            }
            
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Attentiongained
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To change the status for action attention gained
    //In Params : Void
    //Return : json
    //Date : 16th May 2018
    //###############################################################
    public function Attentiongained(Request $request)
    {
        try {
            if (Auth::check()) {
                \DB::beginTransaction();
                $Input           = Input::all();
                $mpProjectId   = substr($Input['slug'], strpos($Input['slug'], ":") + 1);
                $mpProjectId   = base64_decode($mpProjectId);
                $result          = MarketPlaceProject::where('id', $mpProjectId)->update(array('show_first' => 0));
                $removeAttention = MarketplaceAction::where('mp_project_id', $mpProjectId)->forceDelete();
                \DB::commit();
                $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1);
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
        }
        return $responseArray;
    }

    //###############################################################
    //Function Name : Getoutstandingbalance
    //Author : Zalak kapadia <zalak@creolestudios.com>
    //Purpose : to get outstanding balance for seller
    //In Params : Void
    //Return : json
    //Date : 16th May 2018
    //###############################################################
    public function Getoutstandingbalance()
    {
        try {

            if (Auth::check()) {
                $balance = MarketplaceSellerPayments::where('user_id', Auth::user()->id)->where('payment_status', 2)->sum('total_amount');
                if ($balance > 0) {
                    return $balance;
                } else {
                    return 0;
                }

            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
            return $responseArray;
        }

    }

    //###############################################################
    //Function Name : Getserviceskills
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To get the active services of seller
    //In Params : Void
    //Return : json
    //Date : 17th May 2018
    //###############################################################
    public function Getserviceskills(Request $request)
    {
        try {
            if (Auth::check()) {
                $Input              = Input::all();
                $Input['seller_id'] = Auth::user()->id;
                $result             = MarketPlaceSellerServiceSkill::with('skill_detail')->where('seller_id', $Input['seller_id'])->where('status', 1)->get();
                $categoryList      = SkillSet::select('id', 'skill_name', 'skill_name_chinese')->where('status', 1)->get();
                if (count($result) > 0) {

                    foreach ($result as $key => $value) {
                        foreach ($value['skill_detail'] as $keyInner => $valueInner) {
                            $category[] = $valueInner;
                        }
                    }
                    foreach ($categoryList as $key => $value) {
                        $value['select'] = 0;
                        foreach ($category as $keyInner => $valueInner) {
                            if ($value['id'] == $valueInner['id']) {
                                $value['select'] = 1;
                            }

                        }
                    }
                    $finalResult['category_selected'] = $result;
                    $finalResult['category_details']  = $category;
                } else {
                    $finalResult['category_selected'] = 0;
                    $finalResult['category_details']  = 0;
                }
                $finalResult['skills'] = $categoryList;
                $finalResult['current_domain'] = UtilityController::Getmessage('CURRENT_DOMAIN'); 
               
                $responseArray         = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $finalResult);
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
        }
        return $responseArray;
    }

    //###############################################################
    //Function Name : Getexistingservices
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To get the active services details of seller
    //In Params : Void
    //Return : json
    //Date : 24th May 2018
    //###############################################################
    public function Getexistingservices(Request $request)
    {
        try {
            if (Auth::check()) {
                $Input              = Input::all();
                $Input['seller_id'] = Auth::user()->id;
                $result             = MarketPlaceSellerServiceSkill::with('skill_detail', 'service_details')->where('seller_id', $Input['seller_id'])->where('skill_id', $Input['id'])->whereIn('status', [1,3])->orderby('id', 'DESC')->get();
                foreach ($result as $key => $value) {
                    foreach ($value['service_details'] as $keyInner => $valueInner) {
                            if(!empty($valueInner['service_images'])){
                                foreach ($valueInner['service_images'] as $keyIn => $valueIn) {
                                    // $valueInner['service_images']['image'] = UtilityController::Imageexist($valueInner['service_images']['image_name'], public_path('') . UtilityController::Getpath('MP_SERVICES_UPLOAD_PATH'), url('') . UtilityController::Getpath('MP_SERVICES_UPLOAD_URL'), url('') . UtilityController::Getmessage('SERVICE_NO_IMAGE_URL'));
                                    $valueIn['image'] = UtilityController::Imageexist($valueIn['image_name'], public_path('') . UtilityController::Getpath('MP_SERVICES_UPLOAD_PATH'), url('') . UtilityController::Getpath('MP_SERVICES_UPLOAD_URL'), url('') . UtilityController::Getmessage('NO_IMAGE_URL'));  
                                }
                            }
                    }
                }
                $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $result);
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
        }
        return $responseArray;
    }

    //###############################################################
    //Function Name : Addserviceskills
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To add skills for services
    //In Params : Void
    //Return : json
    //Date : 17th May 2018
    //###############################################################
    public function Addserviceskills(Request $request)
    {
        try {
            if (Auth::check()) {
                \DB::beginTransaction();
                $Input              = Input::all();
                $Input['seller_id'] = Auth::user()->id;
                if (array_key_exists('skills', $Input)) {
                    
                    $statusChange = DesignerSkill::where('user_id', $Input['seller_id'])->forceDelete();
                    $skillSets    = $Input['skills'];

                    foreach ($skillSets as $key => $value) {
                        $skillCategory[$key]['user_id']      = $Input['seller_id'];
                        $skillCategory[$key]['skill_set_id'] = $value;
                        $skillCategory[$key]['created_at']   = Carbon::now();
                        $skillCategory[$key]['updated_at']   = Carbon::now();

                        $skills[] = $value;
                    }
                    DesignerSkill::insert($skillCategory);

                    $statusChange = MarketPlaceSellerServiceSkill::select('id','skill_id')->where('seller_id', $Input['seller_id'])->where('status',1)->get();
                    foreach ($statusChange as $key => $value) {
                        foreach ($Input['skills'] as $keyInner => $valueInner) {
                            if($valueInner==$value['skill_id']){
                                unset($Input['skills'][$keyInner]);
                                unset($statusChange[$key]);  
                            }
                        }
                    }
                    if(!empty($statusChange->count()>0)){
                        foreach ($statusChange as $key => $value) {
                            $dataChange[] = $value['id'];
                        }
                        $activeStatusChange = MarketPlaceSellerServiceSkill::whereIn('id',$dataChange)->update(array('status'=>2));
                    }
                    if(!empty($Input['skills'])){
                        $skillSets    = $Input['skills'];
                        foreach ($skillSets as $key => $value) {
                            $serviceCategory[$key]['seller_id']  = $Input['seller_id'];
                            $serviceCategory[$key]['skill_id']   = $value;
                            $serviceCategory[$key]['status']     = 1;
                            $serviceCategory[$key]['created_at'] = Carbon::now();
                            $serviceCategory[$key]['updated_at'] = Carbon::now();

                            $validator = UtilityController::ValidationRules($serviceCategory[$key], 'MarketPlaceSellerServiceSkill');

                            if (!$validator['status']) {
                                $errorMessage = explode('.', $validator['message']);
                                throw new \Exception($errorMessage);
                            } else {
                                continue;
                            }
                        }
                        $result   = MarketPlaceSellerServiceSkill::insert($serviceCategory);
                    }
                    $category = SkillSet::whereIn('id', $skills)->get();

                } else {
                    $returnMessage = ($Input['language']=='en'?'SELECT_ATLEAST_ONE_CATEGORY':'SELECT_ATLEAST_ONE_CATEGORY_CHINESE');
                    $responseArray = UtilityController::Generateresponse(false, $returnMessage, 0);
                    return response()->json($responseArray);
                }
                \DB::commit();
                $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $category);
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, $e->getMessage(), 0);
        }
        return $responseArray;
    }

    //###############################################################
    //Function Name : Addservices
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To add new service
    //In Params : Void
    //Return : json
    //Date : 17th May 2018
    //###############################################################
    public function Addservices(Request $request)
    {
        try {
            if (Auth::check()) {
                \DB::beginTransaction();
                $Input = Input::all();
                $parentSkillId = MarketPlaceSellerServiceSkill::where('skill_id',$Input['mp_seller_service_skill_id'])->where('seller_id',Auth::user()->id)->where('status',1)->orderby('id','DESC')->first();
                normal_flow:
                $Input['seller_id']                   = Auth::user()->id;
                /*$data['mp_seller_service_skill_id'] = $Input['mp_seller_service_skill_id'];*/
                $data['mp_seller_service_skill_id']   = $parentSkillId['id'];
                $data['service_title']                = $Input['service_title'];
                $data['service_description']          = $Input['service_details'];
                $data['no_of_revisions']              = $Input['service_revisions'][0];
                $data['delivery_time_in_days']        = $Input['service_delivery_time'][0];   
                $data['price']                        = $Input['service_price'][0];
                $data['status']                       = $Input['status'];
                $data['skill_id']                     = $Input['mp_seller_service_skill_id'];
                unset($Input['service_revisions'][0]);
                unset($Input['service_delivery_time'][0]);
                unset($Input['service_price'][0]);
                $validator = UtilityController::ValidationRules($data, 'MarketPlaceSellerServicePackage');

                if (!$validator['status']) {
                    $errorMessage = explode('.', $validator['message']);
                    $errorMessage = $errorMessage[0];
                    if($errorMessage=='The service title field is required'){
                        $returnMessage = ($Input['language']=='en'?'SERVICE_TITLE_REQUIRED':'SERVICE_TITLE_REQUIRED_CHINESE');
                        throw new \Exception(UtilityController::Getmessage($returnMessage));
                    } else if($errorMessage=='The service description field is required'){
                        $returnMessage = ($Input['language']=='en'?'SERVICE_DESCRIPTION_REQUIRED':'SERVICE_DESCRIPTION_REQUIRED_CHINESE');
                        throw new \Exception(UtilityController::Getmessage($returnMessage));
                    } else if($errorMessage=='The no of revisions field is required'){
                        $returnMessage = ($Input['language']=='en'?'SERVICE_NO_OF_REVISION_REQUIRED':'SERVICE_NO_OF_REVISION_REQUIRED_CHINESE');
                        throw new \Exception(UtilityController::Getmessage($returnMessage));
                    } else if($errorMessage=='The delivery time in days field is required'){
                        $returnMessage = ($Input['language']=='en'?'SERVICE_DELIVERY_TIME_REQUIRED':'SERVICE_DELIVERY_TIME_REQUIRED_CHINESE');
                        throw new \Exception(UtilityController::Getmessage($returnMessage));
                    } else if($errorMessage=='The delivery time in days must be at least 3'){
                        $returnMessage = ($Input['language']=='en'?'SERVICE_DELIVERY_TIME_MIM':'SERVICE_DELIVERY_TIME_MIM_CHINESE');
                        throw new \Exception(UtilityController::Getmessage($returnMessage));
                    } else {
                        $returnMessage = ($Input['language']=='en'?'SERVICE_PRICE_REQUIRED':'SERVICE_PRICE_REQUIRED_CHINESE');
                        throw new \Exception(UtilityController::Getmessage($returnMessage));
                    }
                } else {
                    if($data['price'] <=0){
                        $returnMessage = ($Input['language']=='en'?'MY_SERVICE_PRICE_0':'MY_SERVICE_PRICE_0_CHINESE');
                        throw new \Exception(UtilityController::Getmessage($returnMessage));
                    }
                    // $current_domain = UtilityController::Getmessage('CURRENT_DOMAIN'); 
                
                    // if($current_domain=='cn'){
                    //     if($data['price']<31){
                    //         $returnMessage = ($Input['language']=='en'?'MY_SERVICE_PRICE':'MY_SERVICE_PRICE_CHINESE');
                    //         throw new \Exception(UtilityController::Getmessage($returnMessage));
                    //     }
                    // }else {
                    //     if($data['price']<6){
                    //         $returnMessage = ($Input['language']=='en'?'MY_SERVICE_PRICE_SINGAPORE_DOLLAR':'MY_SERVICE_PRICE_CHINESE_SINGAPORE_DOLLAR');
                    //         throw new \Exception(UtilityController::Getmessage($returnMessage));
                    //      }
                    // }
                    if (array_key_exists('draftFormID', $Input)) {
                        $firstPackage = UtilityController::Makemodelobject($data, 'MarketPlaceSellerServicePackage', '', $Input['draftFormID']);
                    } else {
                        $firstPackage = UtilityController::Makemodelobject($data, 'MarketPlaceSellerServicePackage');
                    }

                    if(count($Input['service_revisions'])>=1 && $Input['service_revisions'][1]!=''){
                        for ($i = 1; $i <= count($Input['service_revisions']); $i++) {
                            $packageExtra[$i]['mp_seller_service_pacakage_id'] = $firstPackage['id'];
                            $packageExtra[$i]['no_of_revisions']               = $Input['service_revisions'][$i];
                            $packageExtra[$i]['delivery_time_in_days']         = $Input['service_delivery_time'][$i];
                            $packageExtra[$i]['price']                         = $Input['service_price'][$i];
                            $packageExtra[$i]['status']                        = 1;
                            $packageExtra[$i]['created_at']                    = Carbon::now();
                            $packageExtra[$i]['updated_at']                    = Carbon::now();
                            $validator                                         = UtilityController::ValidationRules($packageExtra[$i], 'MarketPlaceSellerServiceProjectExtra');
                            if (!$validator['status']) {
                                $errorMessage = explode('.', $validator['message']);
                                $errorMessage = $errorMessage[0];
                                if($errorMessage=='The no of revisions field is required'){
                                    $returnMessage = ($Input['language']=='en'?'SERVICE_NO_OF_REVISION_REQUIRED':'SERVICE_NO_OF_REVISION_REQUIRED_CHINESE');
                                    throw new \Exception(UtilityController::Getmessage($returnMessage));
                                } else if($errorMessage=='The delivery time in days field is required'){
                                    $returnMessage = ($Input['language']=='en'?'SERVICE_DELIVERY_TIME_REQUIRED':'SERVICE_DELIVERY_TIME_REQUIRED_CHINESE');
                                    throw new \Exception(UtilityController::Getmessage($returnMessage));
                                } else if($errorMessage=='The delivery time in days must be at least 3'){
                                    $returnMessage = ($Input['language']=='en'?'SERVICE_DELIVERY_TIME_MIM':'SERVICE_DELIVERY_TIME_MIM_CHINESE');
                                    throw new \Exception(UtilityController::Getmessage($returnMessage));
                                }  else {
                                    $returnMessage = ($Input['language']=='en'?'SERVICE_PRICE_REQUIRED':'SERVICE_PRICE_REQUIRED_CHINESE');
                                    throw new \Exception(UtilityController::Getmessage($returnMessage));
                                }
                            } else {
                                if($packageExtra[$i]['price'] <=0){
                                    $returnMessage = ($Input['language']=='en'?'MY_SERVICE_PRICE_0':'MY_SERVICE_PRICE_0_CHINESE');
                                    throw new \Exception(UtilityController::Getmessage($returnMessage));
                                }
                                /*if($current_domain=='cn'){
                                    if($packageExtra[$i]['price'] <31){
                                        $returnMessage = ($Input['language']=='en'?'MY_SERVICE_PRICE':'MY_SERVICE_PRICE_CHINESE');
                                        throw new \Exception(UtilityController::Getmessage($returnMessage));
                                    }
                                } else {
                                    if($packageExtra[$i]['price'] <6){
                                        $returnMessage = ($Input['language']=='en'?'MY_SERVICE_PRICE_SINGAPORE_DOLLAR':'MY_SERVICE_PRICE_CHINESE_SINGAPORE_DOLLAR');
                                        throw new \Exception(UtilityController::Getmessage($returnMessage));
                                    }
                                }*/
                                continue;
                            }
                        }
                        if (array_key_exists('draftFormID', $Input)) {
                            $deleteExisting = MarketPlaceSellerServiceProjectExtra::where('mp_seller_service_pacakage_id', $Input['draftFormID'])->forceDelete();
                            $result         = MarketPlaceSellerServiceProjectExtra::insert($packageExtra);
                        } else {
                            $result = MarketPlaceSellerServiceProjectExtra::insert($packageExtra);
                        }
                    }
                    if (array_key_exists('remove_image', $Input)) {
                        array_unique($Input['remove_image']); 
                        foreach ($Input['remove_image'] as $key => $value) {
                            $deleteExistingImages = MarketPlaceSellerServiceImage::where('id', $value)->forceDelete();
                        }
                    }
                    if (array_key_exists('images', $Input)) {
                        if(!empty($Input['images'])){
                            foreach ($Input['images'] as $key => $value) {
                                $fileName  = date("dmYHis") . rand();
                                $extension = \File::extension($value->getClientOriginalName());
                                if (in_array($extension, array('jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG'))) {
                                    $value->move(public_path('') . UtilityController::Getpath('MP_SERVICES_UPLOAD_PATH'), $fileName);
                                    chmod(public_path('') . UtilityController::Getpath('MP_SERVICES_UPLOAD_PATH') . $fileName, 0777);
                                    $path = public_path('') . UtilityController::Getpath('MP_SERVICES_UPLOAD_PATH') . $fileName;

                                } else {
                                    $returnMessage = ($Input['language']=='en'?'ONLY_JPG_PNG_JPEG_FOR_INTERNAL_IMAGES':'ONLY_JPG_PNG_JPEG_FOR_INTERNAL_IMAGES_CHINESE');
                                    throw new \Exception(UtilityController::Getmessage($returnMessage));
                                }
                                $result = self::Resizeimage($value, 400, 400, $fileName, $path, 'MP_SERVICES_UPLOAD_PATH');
                                if ($result) {
                                    $uploadAttachment[$key]['image_name'] = $fileName;
                                }
                                $uploadAttachment[$key]['mp_seller_service_pacakage_id'] = $firstPackage['id'];
                                $uploadAttachment[$key]['status']                        = 1;
                                $uploadAttachment[$key]['created_at']                    = Carbon::now();
                                $uploadAttachment[$key]['updated_at']                    = Carbon::now();

                                $validator = UtilityController::ValidationRules($uploadAttachment[$key], 'MarketPlaceSellerServiceImage');
                                if (!$validator['status']) {
                                    $errorMessage = explode('.', $validator['message']);
                                    throw new \Exception($errorMessage['0']);
                                } else {
                                    continue;
                                }
                            }

                            if (array_key_exists('draftFormID', $Input)) {
                                // $deleteExisting = MarketPlaceSellerServiceImage::where('mp_seller_service_pacakage_id', $Input['draftFormID'])->forceDelete();
                                MarketPlaceSellerServiceImage::insert($uploadAttachment);
                            } else {
                                MarketPlaceSellerServiceImage::insert($uploadAttachment);
                            }
                        }
                    } else {
                        if(!array_key_exists('draftFormID', $Input)){
                            $returnMessage = ($Input['language']=='en'?'SELECT_SERVICE_IMAGE':'SELECT_SERVICE_IMAGE_CHINESE');
                            throw new \Exception(UtilityController::Getmessage($returnMessage));
                        }
                    }
                    
                }
                if ($Input['status'] == 1) {
                    $returnMessage = ($Input['language']=='en'?'SERVICE_SAVED':'SERVICE_SAVED_CHINESE');
                    $message = UtilityController::Getmessage($returnMessage);
                } else {
                    $returnMessage = ($Input['language']=='en'?'SERVICE_DRAFTED':'SERVICE_DRAFTED_CHINESE');
                    $message = UtilityController::Getmessage($returnMessage);
                }
                $responseArray = UtilityController::Generateresponse(true, $message, 1, $firstPackage);
                
                /* seller minimum and maximum amount based on services created : start */
                    $packagePrice = MarketPlaceSellerServicePackage::with('skill_parent','service_extras')->whereHas('skill_parent', function ($query){
                                $query->where('seller_id', Auth::user()->id);
                            })
                            ->whereIn('status', array(1,3))->get();

                    if($packagePrice){
                        foreach ($packagePrice as $key => $value) {
                            $price[] = $value['price'];
                            foreach ($value['service_extras'] as $keyInner => $valueInner) {
                                $price[] = $valueInner['price'];
                            }
                        }
                        $user['mp_designer_min_price'] = min($price);
                        $user['mp_designer_max_price'] = max($price);
                        $result = User::where('id',Auth::user()->id)->update($user);
                    }
                    
                /* seller minimum and maximum amount based on services created : end */
                \DB::commit();
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, $e->getMessage(), 0);
        }
        return $responseArray;
    }

    //###############################################################
    //Function Name : Deleteservices
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To delete the active services of seller
    //In Params : Void
    //Return : json
    //Date : 24th May 2018
    //###############################################################
    public function Deleteservices(Request $request)
    {
        try {
            if (Auth::check()) {
                $Input              = Input::all();
                \DB::beginTransaction();
                $Input['seller_id'] = Auth::user()->id;

                $statusChangePackage       = MarketPlaceSellerServicePackage::where('id', $Input['draftFormID'])->update(array('status' => 2));
                $statusChangeImages       = MarketPlaceSellerServiceImage::where('mp_seller_service_pacakage_id', $Input['draftFormID'])->update(array('status' => 2));
                $statusChangeProjectExtra       = MarketPlaceSellerServiceProjectExtra::where('mp_seller_service_pacakage_id', $Input['draftFormID'])->update(array('status' => 2));
                
                if ($statusChangePackage) {
                    \DB::commit();
                    $returnMessage = ($Input['language']=='en'?'SERVICE_DELETED':'SERVICE_DELETED_CHINESE');
                    $responseArray = UtilityController::Generateresponse(true, $returnMessage, 1, $statusChangePackage);
                } else {
                    $returnMessage = ($Input['language']=='en'?'GENERAL_ERROR':'GENERAL_SUCCESS_CHINESE');
                    $responseArray = UtilityController::Generateresponse(true, $returnMessage, 1);
                }

            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
        }
        return $responseArray;
    }

    //###############################################################
    //Function Name : Changecurrentlanguage
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To change the language of user
    //In Params : Void
    //Return : json
    //Date : 31st May 2018
    //###############################################################
    public function Changecurrentlanguage(Request $request)
    {
        try {
            if(Auth::check()){
                $Input = Input::all();
                $statusChange = UtilityController::Makemodelobject($Input,'User','',Auth::user()->id);
                if($statusChange){
                    $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1,$statusChange);
                }
                else{
                    $returnMessage = ($Input['language']=='en'?'GENERAL_ERROR':'GENERAL_SUCCESS_CHINESE');
                    $responseArray = UtilityController::Generateresponse(true, $returnMessage, 1);
                }

            }else{
                $responseArray = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false,'GENERAL_ERROR', 0);
        }
        return $responseArray;
    }

    //###############################################################
    //Function Name : Getnotifications
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get all notification lists
    //In Params : Void
    //Return : json
    //Date : 31st May 2018
    //###############################################################
    public function Getnotifications(Request $request)
    {
        try {
            if (Auth::check()) {
                $notifications = SystemNotification::where('receiver_id', Auth::user()->id)
                // ->where("read_unread",2)
                    ->orderby('read_unread', 'desc')
                    ->orderby('id', 'desc')
                    ->paginate()
                    ->toArray();
                if (!empty($notifications)) {
                    foreach ($notifications['data'] as $key => $value) {
                        $date = Carbon::parse($value['created_at'])->timezone(Auth::user()->timezone);
                        $notifications['data'][$key]['created_at'] = UtilityController::Changedateformat($date, 'd F, Y h:i A');
                    }
                }
                $responseArray = UtilityController::Generateresponse(true, '', 1, $notifications);
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
        }
        return $responseArray;
    }
    //###############################################################
    //Function Name: Getunreadnotificationscount
    //Author:        Ketan Solanki <ketan@creolestudios.com>
    //Purpose:       To get all unread notification count
    //In Params:     Void
    //Return:        json
    //Date:          09th August 2018
    //###############################################################
    public function Getunreadnotificationscount(Request $request)
    {
        try {
            if (Auth::check()) {
                $notifications['totalCount'] = SystemNotification::where('receiver_id', Auth::user()->id)->where("read_unread",2)->count();                    
                $responseArray = UtilityController::Generateresponse(true, '', 1, $notifications);
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
        }
        return $responseArray;
    }

    //###############################################################
    //Function Name : Cancelwithoutseller
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get all notification lists
    //In Params : Void
    //Return : json
    //Date : 31st May 2018
    //###############################################################
    public function Cancelwithoutseller(Request $request)
    {
        try {
            if (Auth::check()) {
                $Input = Input::all();
                $data['project_status'] = 5;
                $data['project_cancellation_reason'] = $Input['reason'];
                $data['project_cancel_request_at'] = Carbon::now();
                $data['project_cancelled_at'] = Carbon::now();
                $result = UtilityController::Makemodelobject($data,'MarketPlaceProject','',$Input['mp_project_id']);
                if($result)

                    $returnMessage=$Input['language']=='en'?'GENERAL_SUCCESS':'GENERAL_SUCCESS_CHINESE';
                    $responseArray = UtilityController::Generateresponse(true, $returnMessage, 1, $result);
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false,'GENERAL_ERROR', 0);
        }
        return $responseArray;
    }
    //###############################################################
    //Function Name : Download
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : To download the file 
    //In Params : Void
    //Return : json
    //Date : 7th June 2018
    //###############################################################
    public function Download(Request $request) {
        try {

            $Input = Input::all();
            $fileName = $Input['file_name'][0][0];
            $count=0;
            if(isset($Input['id'][0][0])){
                $id=$Input['id'][0][0];
                $count_downloads=MarketPlaceUpload::where('id',$id)->first();
                $count=$count_downloads['inspiration_download_count']+1;
                $changeCount       = MarketPlaceUpload::where('id',$id )->update(array('inspiration_download_count' => $count));
            }
                  
            $bucket    = UtilityController::Getmessage('BUCKET');

            if(isset($Input['inspiration_bank']) && $Input['inspiration_bank']==1){
                $object    = UtilityController::Getpath('BUCKET_MP_INS').$fileName;
                $localfile = public_path('').UtilityController::Getpath('INSPIRATION_DOWNLOAD_PATH').$fileName;
            }else{
                $object    = UtilityController::Getpath('BUCKET_idproof_FOLDER').$fileName;
                $localfile = public_path('').UtilityController::Getpath('PR_DOCUMENT_DOWNLOAD_PATH').$fileName; 
            }
            $options   = array(
                    OssClient::OSS_FILE_DOWNLOAD => $localfile,
                );  
            try{
                if(isset($Input['inspiration_bank']) && $Input['inspiration_bank']==1){
                    $result['url']       = UtilityController::Getpath('DOWNLOAD_ON_USER_SYSTEM_INSPIRATION').$fileName;
                    $result['count']     = $count;
                }else{
                    $result['url']       = UtilityController::Getpath('DOWNLOAD_ON_USER_SYSTEM').$fileName;
                }
                
                $result['file_name'] = $fileName;
                $ossClient           = new OssClient(UtilityController::Getmessage('ACCESS_KEY_ID'), UtilityController::Getmessage('ACCESS_KEY_SECRET'), UtilityController::Getmessage('END_POINT'));
                $ossClient->getObject($bucket, $object, $options);
                $responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$result);
            } catch(OssException $e) {
                $sendExceptionMail = UtilityController::Sendexceptionmail($e);
                $responseArray = UtilityController::Generateresponse(false,'TRY_AGAIN',0);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, 'GENERAL_ERROR', Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }
    //###############################################################
    //Function Name : Unlinkdownloadedfile
    //Author : Zalak Kapadia <zalak@creolestudios.com>
    //Purpose : To unlink the downloaded file from our file system
    //In Params : Void
    //Return : json
    //Date : 7th June 2018
    //###############################################################
    public function UnlinkdownloadedfileIdProof(Request $request) {
        try {
            $Input         = Input::all();
            if(isset($Input['inspiration_bank']) && $Input['inspiration_bank']==1){
                $fileUnlink    = unlink(public_path('').UtilityController::Getpath('INSPIRATION_DOWNLOAD_PATH').$Input['file_name']);
            }else{
                $fileUnlink    = unlink(public_path('').UtilityController::Getpath('PR_DOCUMENT_DOWNLOAD_PATH').$Input['file_name']);    
            }
            
            $responseArray = UtilityController::Generateresponse(true,'FILE_UNLINKED',1,$fileUnlink);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, 'GENERAL_ERROR', Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Getservices
    //Author : Senil shah <senil@creolestudios.com>
    //Purpose : To get the services for seller profile page
    //In Params : Void
    //Return : json
    //Date : 18th June 2018
    //###############################################################
    public function Getservices(Request $request) {
        try {
            $Input    = Input::all();
            if(array_key_exists('mp_project_id', $Input)){
                $mp_project_id = substr($Input['mp_project_id'], strpos($Input['mp_project_id'], ":") + 1);
                $mp_project_id = base64_decode($mp_project_id);
                $projectDetails = ProjectSkill::where('mp_project_id',$mp_project_id)->get();
                $projectDetails = json_decode(json_encode($projectDetails),true);
                foreach ($projectDetails as $key => $value) {
                    $skillId[] = $value['skill_set_id'];
                }
            }
            $sellerId = substr($Input['id'], strpos($Input['id'], ":") + 1);
            $sellerId = base64_decode($sellerId);
            $services = MarketPlaceSellerServiceSkill::with('service_skill_detail','service_details')->where('seller_id',$sellerId)->where('status',1);
            if(!empty($skillId))
                $services = $services->whereIn('skill_id',$skillId);    
            $services = $services->get();
            foreach ($services as $key => $value) {
                foreach ($value['service_details'] as $keyInner => $valueInner) {
                    $value['service_details'][$keyInner]['parent_service_name_english'] = $value['service_skill_detail']['skill_name'];
                    $value['service_details'][$keyInner]['parent_service_name_chinese'] = $value['service_skill_detail']['skill_name_chinese'];
                    $value['service_details'][$keyInner]['parent_service_skill_id']     = $value['service_skill_detail']['id'];
                    
                    if(!empty($valueInner['service_images'])){
                        foreach ($valueInner['service_images'] as $keyIn => $valueIn) {
                            $valueIn['image'] = UtilityController::Imageexist($valueIn['image_name'], public_path('') . UtilityController::Getpath('MP_SERVICES_UPLOAD_PATH'), url('') . UtilityController::Getpath('MP_SERVICES_UPLOAD_URL'), url('') . UtilityController::Getmessage('NO_IMAGE_URL'));        
                        }
                    }
                    /*else{
                        echo "string";
                        $value['service_details'][$keyInner]['service_images']['image'] = url('') . UtilityController::Getmessage('NO_IMAGE_URL');   
                    }*/
                    
                }
            }
            $responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$services);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0,'GENERAL_ERROR', Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Insertadmintimelinenotifications
    //Author : Zalak kapadia <zalak@creolestudios.com>
    //Purpose : To add timeline notificatiosn for admin
    //In Params : Void
    //Return : json
    //Date : 18th June 2018
    //###############################################################
    public function Insertadmintimelinenotifications($user_id,$project_id,$message_text_en,$message_text_chi,$status,$is_admin,$amount=NULL){

        $adminNotificationData['user_id']          = $user_id;
        $adminNotificationData['mp_project_id']    = $project_id;
        $adminNotificationData['message_text_en']  = $message_text_en;
        $adminNotificationData['message_text_chi'] = $message_text_chi;
        $adminNotificationData['status']           = $status;
        $adminNotificationData['is_admin']         = $is_admin;
        $adminNotificationData['amount']           = $amount;
        $adminNotificationData['created_at']       = Carbon::now();
        $adminNotificationData['updated_at']       = Carbon::now();
        $resultDetailInfo                          = UtilityController::Makemodelobject($adminNotificationData, 'AdminTimeline');
    }

    //###############################################################
    //Function Name : Insertmessageboardnotifications
    //Author : Zalak kapadia <zalak@creolestudios.com>
    //Purpose : To add messageboard notificatiosn 
    //In Params : Void
    //Return : json
    //Date : 13th August 2018
    //###############################################################
    public function Insertmessageboardnotifications($user_id,$project_id,$message_text_en,$message_text_chi,$type,$user_role,$notification_msg_show_to){

        $messageBoard['user_id']            = $user_id;
        $messageBoard['mp_project_id']      = $project_id;
        $messageBoard['message']            = $message_text_en;
        $messageBoard['message_in_chinese'] = $message_text_chi;
        $messageBoard['type']               = $type;
        $messageBoard['user_role']          = $user_role;
        $messageBoard['notification_msg_show_to']     = $notification_msg_show_to;
        // Added as a messageboard
        $result = UtilityController::Makemodelobject($messageBoard, 'MarketPlaceMessageBoard', '');
    }

    //###############################################################
    //Function Name : Insertsystemnotifications
    //Author : Zalak kapadia <zalak@creolestudios.com>
    //Purpose : To add messageboard notificatiosn 
    //In Params : Void
    //Return : json
    //Date : 13th August 2018
    //###############################################################
    public function Insertsystemnotifications($user_id,$receiver_id,$messsage_text,$messsage_text_chi,$project_slug){

        $notification['sender_id']             = $user_id;
        $notification['receiver_id']           = $receiver_id;
        $notification['notification_text']     = $messsage_text;
        $notification['chi_notification_text'] = $messsage_text_chi;
        $notification['module']                = 1;
        $notification['project_slug']          = $project_slug;

        $sendNotification                      = UtilityController::Makemodelobject($notification, 'SystemNotification');
    }

    //###############################################################
    //Function Name : Storecart
    //Author : Senil shah <senil@creolestudios.com>
    //Purpose : To store the cart
    //In Params : Void
    //Return : json
    //Date : 21st June 2018
    //###############################################################
    public function Storecart(Request $request) {
        try {
            if (Auth::check()) {
                $Input    = Input::all();
                \DB::beginTransaction();
                $cart = $Input['cart'];
                if(array_key_exists('mp_project_id', $Input)){
                    $mp_project_id = substr($Input['mp_project_id'], strpos($Input['mp_project_id'], ":") + 1);
                    $mp_project_id = base64_decode($mp_project_id);
                }
                $cart_id = date('dmYHis').rand();
                foreach ($cart as $key => $value) {
                    if(array_key_exists('mp_project_id', $Input)){
                        $cartData[$key]['mp_project_id'] = $mp_project_id;
                    }
                    $cartData[$key]['mp_seller_service_package_id'] = $value['id'];
                    $cartData[$key]['skill_id']                     = $value['parent_service_skill_id'];
                    $cartData[$key]['cart_id']                      = $cart_id;
                    $cartData[$key]['price']                        = $value['price'];
                    $cartData[$key]['created_at']                   = Carbon::now();
                    $cartData[$key]['updated_at']                   = Carbon::now();

                    $validator = UtilityController::ValidationRules($cartData[$key], 'MarketPlaceCart');
                    if (!$validator['status']) {
                        $errorMessage = explode('.', $validator['message']);
                        throw new \Exception($errorMessage['0']);
                    } else {
                        continue;
                    }
                }
                $result = MarketPlaceCart::insert($cartData);
                \DB::commit();
                $responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$cart_id);
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Getcartandservices
    //Author : Senil shah <senil@creolestudios.com>
    //Purpose : To get the stored cart value and rest services
    //In Params : Void
    //Return : json
    //Date : 21st June 2018
    //###############################################################
    public function Getcartandservices(Request $request) {
        try {
            if(Auth::check()){
                $Input    = Input::all();
                if($Input['flow']=='reverse'){
                    $sellerId = $Input['mp_project_id'];
                } else {
                    $sellerId = $Input['selectedDesigner'];
                }
                $sellerId = substr($sellerId, strpos($sellerId, ":") + 1);
                $sellerId = base64_decode($sellerId);
                $cart['added_tO_cart'] = MarketPlaceCart::with('package_details')->where('cart_id',$Input['cartId'])->get();
                if($cart['added_tO_cart']->count()>0){
                    foreach ($cart['added_tO_cart'] as $key => $value) {
                        $ignoreSkills[] = $value['mp_seller_service_package_id'];
                        $ignoreSkillsForSelect[] = $value['skill_id'];
                    }
                    asort($ignoreSkills);
                    asort($ignoreSkillsForSelect);
                    $cart['purchsed_services'] = MarketPlaceSellerServicePackage::with('skill_parent')->whereIn('id',$ignoreSkills)
                        ->whereHas('skill_parent', function ($query) use($sellerId) {
                            $query->where('seller_id', $sellerId);
                        })
                        ->where('status',1)->get();
                    $cart['remaining_services'] = MarketPlaceSellerServicePackage::with('skill_parent')->whereNotIn('id',$ignoreSkills)
                        ->whereHas('skill_parent', function ($query) use($sellerId) {
                            $query->where('seller_id', $sellerId);
                        })
                        ->where('status',1)->get();

                    $cart['selected_skills'] = SkillSet::select('id', 'skill_name', 'skill_name_chinese')->where('status', 1)->get();
                    $seller_service = MarketPlaceSellerServiceSkill::where('seller_id',$sellerId)->where('status',1)->get();
                    foreach ($cart['selected_skills'] as $key => $value) {
                        $value['select'] = 0;
                        $value['keepDisable'] = 1;
                        foreach ($ignoreSkillsForSelect as $keyInner => $valueInner)
                            if($value['id']==$valueInner){
                                $value['select'] = 1;
                                $projectSkill[] = $value['id'];
                            }
                    }
                    $ignoreSkillsForSelect = array_unique($ignoreSkillsForSelect);
                    $cart['projectSkill'] = $ignoreSkillsForSelect;
                    $responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$cart);
                } else {
                    $responseArray = UtilityController::Generateresponse(false,'NO_PACKAGE_SELECTED',0);
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, 'GENERAL_ERROR', Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Removefromcart
    //Author : Senil shah <senil@creolestudios.com>
    //Purpose : To remove the cart value
    //In Params : Void
    //Return : json
    //Date : 22nd June 2018
    //###############################################################
    public function Removefromcart(Request $request) {
        try {
            if(Auth::check()){
                $Input    = Input::all();
                \DB::beginTransaction();
                $result = MarketPlaceCart::where('id',$Input['id'])->forceDelete();
                \DB::commit();
                $responseArray = UtilityController::Generateresponse(true,'PACKAGE_REMOVED',1,$result);
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, 'GENERAL_ERROR', Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Addtocart
    //Author : Senil shah <senil@creolestudios.com>
    //Purpose : To add a package to cart
    //In Params : Void
    //Return : json
    //Date : 22nd June 2018
    //###############################################################
    public function Addtocart(Request $request) {
        try {
            if(Auth::check()){
                $Input    = Input::all();
                \DB::beginTransaction();
                if(array_key_exists('mp_project_id', $Input)){
                    $Input['mp_project_id'] = substr($Input['mp_project_id'], strpos($Input['mp_project_id'], ":") + 1);
                    $Input['mp_project_id'] = base64_decode($Input['mp_project_id']);
                    $cartData['mp_project_id'] = $Input['mp_project_id'];
                }
                $cartData['mp_seller_service_package_id'] = $Input['service']['id'];
                $cartData['skill_id']                     = $Input['service']['skill_parent']['skill_id'];
                $cartData['cart_id']                      = $Input['cart_id'];
                $cartData['price']                        = $Input['service']['price'];
                $result = UtilityController::Makemodelobject($cartData,'MarketPlaceCart');
                \DB::commit();
                $responseArray = UtilityController::Generateresponse(true,'PACKAGE_ADDED',1,$result);
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, 'GENERAL_ERROR', Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Getbankfields
    //Author : Senil shah <senil@creolestudios.com>
    //Purpose : To generate the fields for bank details
    //In Params : Void
    //Return : json
    //Date : 26th June 2018
    //###############################################################
    public function Getbankfields(Request $request) {
        try {
            if(Auth::check()){
                $Input    = Input::all();
                if(!empty($Input)){
                    $array = ['44','132','102','36','238','119','217','173','32','13','109','116','146','14','18','20','21','33','38','54','57','58','62','68','74','75','81','82','99','101','105','107','111','112','117','118','120','126','127','155','157','165','166','175','176','191','202','205','206','211','212','215','223','224','228','229','230','231','234'];

                    if(in_array($Input['id'], $array)){
                        $idNumberArray = ['44','116','223'];
                        
                        $bankNameArray = ['44','132','102','36','238','119','217','173','32','13','109','116','146','14','18','20','21','33','38','54','57','58','62','68','74','75','81','82','99','101','105','107','111','112','117','118','120','126','127','155','157','165','166','175','176','191','202','205','206','211','212','215','223','224','228','229','230','231','234'];
                        
                        $accountNameChineseArray = ['44'];
                        
                        $accountNameEnglishArray = ['44', '109','116','146'];
                        
                        $accountNameArray = ['132','102','36','238','119','217','173','32','13','116','146','14','18','20','21','33','38','54','57','58','62','68','74','75','81','82','99','101','105','107','111','112','117','118','120','126','127','155','157','165','166','175','176','191','202','205','206','211','212','215','223','224','228','229','230','231','234'];
                        
                        $accountNumberArray = ['44','132','102','36','238','119','217','173','32','13','109','116','146','18','38','62','101','118','155','157','165','202','206','215','224','228','230','231','234'];
                        
                        $subBankNameArray = ['44'];
                        
                        $subBankProvienceArray = ['44'];
                        
                        $subBankCityArray = ['44'];
                        
                        $storeFrontUrlArray = ['44'];
                        
                        $branchCodeArray = ['102','36','13','109','157','202','206'];
                        
                        $branchNameArray = ['18','38'];
                        
                        $swiftbicArray = ['36','238','119','32','146','38','62','81','111','112','117','118','155','165','202','223','224','234'];
                        
                        $bankProvienceArray = ['238'];
                        
                        $bankCodeArray = ['13','38','157'];
                        
                        $accountTypeArray = ['109','38','101','202','231'];
                        
                        $japanArray = ['109'];
                        
                        $ibanArray = ['14','20','21','33','54','57','58','68','74','75','81','82','99','105','107','111','112','117','120','126','127','166','175','176','191','205','211','212','215','223','229','230','231','234'];
                        
                        $panArray = ['101'];
                        
                        $ifscArray = ['101'];
                        
                        $suffixArray = ['157'];
                        
                        $sortCodeArray = ['230'];

                        $routingArray = ['231'];

                        $branchNumberArray = ['38'];

                        if(in_array($Input['id'], $idNumberArray))
                            $fields[] = 'id_number';
                        
                        if(in_array($Input['id'], $bankNameArray))
                            $fields[] = 'bank_name';
                        
                        if(in_array($Input['id'], $accountNameChineseArray))
                            $fields[] = 'account_name_chinese';
                        
                        if(in_array($Input['id'], $accountNameEnglishArray))
                            $fields[] = 'account_name_eng';
                        
                        if(in_array($Input['id'], $accountNameArray))
                            $fields[] = 'account_name_eng';
                        
                        if(in_array($Input['id'], $accountNumberArray))
                            $fields[] = 'account_number';
                        
                        if(in_array($Input['id'], $subBankNameArray))
                            $fields[] = 'sub_bank_name';
                        
                        if(in_array($Input['id'], $subBankProvienceArray))
                            $fields[] = 'bank_province';
                        
                        if(in_array($Input['id'], $subBankCityArray))
                            $fields[] = 'sub_bank_city';
                        
                        if(in_array($Input['id'], $storeFrontUrlArray))
                            $fields[] = 'store_front_url';
                        
                        if(in_array($Input['id'], $branchCodeArray))
                            $fields[] = 'branch_code';
                        
                        if(in_array($Input['id'], $branchNameArray))
                            $fields[] = 'branch_name';
                        
                        if(in_array($Input['id'], $swiftbicArray))
                            $fields[] = 'swift_bic';
                        
                        if(in_array($Input['id'], $bankProvienceArray))
                            $fields[] = 'bank_province';
                        
                        if(in_array($Input['id'], $bankCodeArray))
                            $fields[] = 'bank_code';
                        
                        if(in_array($Input['id'], $accountTypeArray))
                            $fields[] = 'account_type';
                        
                        if(in_array($Input['id'], $japanArray))
                            $fields[] = 'account_name_chinese';
                        
                        if(in_array($Input['id'], $ibanArray))
                            $fields[] = 'iban';
                        
                        if(in_array($Input['id'], $panArray))
                            $fields[] = 'pan';
                        
                        if(in_array($Input['id'], $ifscArray))
                            $fields[] = 'ifsc_code';
                        
                        if(in_array($Input['id'], $suffixArray))
                            $fields[] = 'suffix';
                        
                        if(in_array($Input['id'], $sortCodeArray))
                            $fields[] = 'sort_code';
                        
                        if(in_array($Input['id'], $routingArray))
                            $fields[] = 'routing_number';

                        if(in_array($Input['id'], $branchNumberArray))
                            $fields[] = 'branch_number';
                    } else {
                        $fields[] = 'account_name_eng';
                        $fields[] = 'account_number';
                        $fields[] = 'bank_name';
                    }
                    $responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$fields);
                } else {
                    $returnMessage=$Input['language']=='en'?'GENERAL_ERROR':'GENERAL_ERROR_CHINESE';
                    $responseArray = UtilityController::Generateresponse(false,$returnMessage,0);
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, 'GENERAL_ERROR', Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Getbankdetails
    //Author : Senil shah <senil@creolestudios.com>
    //Purpose : To get the bank details of user if exist
    //In Params : Void
    //Return : json
    //Date : 27th June 2018
    //###############################################################
    public function Getbankdetails(Request $request) {
        try {
            if(Auth::check()){
                $Input    = Input::all();
                $userId = Auth::user()->id;
                $bankDetails = MarketPlaceSellerBankDetails::where('user_id',$userId)->orderby('id','DESC')->first();
                if($bankDetails){
                    $bankDetails = json_decode(json_encode($bankDetails),true);
                    $result['bank_select'] = $bankDetails['country_id'];
                    foreach($bankDetails as $key=>$value)
                    {
                        if(is_null($value) || $value == '')
                            unset($bankDetails[$key]);

                        if($key=='country_id'||$key=='user_id'||$key=='id'||$key=='created_at'||$key=='updated_at')
                            unset($bankDetails[$key]);
                    }
                    $result['fieldsValue'] = $bankDetails;
                    foreach ($result['fieldsValue'] as $key => $value) {
                        $result['fields'][] = $key;
                    }
                    $responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$result);
                } else {
                    $responseArray = UtilityController::Generateresponse(false,'GENERAL_ERROR',0);
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, 'GENERAL_ERROR', Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Getprojectextras
    //Author : Senil shah <senil@creolestudios.com>
    //Purpose : To get the project extras of project categories if exist
    //In Params : Void
    //Return : json
    //Date : 9th July 2018
    //###############################################################
    public function Getprojectextras(Request $request) {
        try {
            if(Auth::check()){
                $Input    = Input::all();
                $mp_project_id = substr($Input['mp_project_id'], strpos($Input['mp_project_id'], ":") + 1);
                $mp_project_id = base64_decode($mp_project_id);
                $packages = MarketPlaceCart::where('mp_project_id',$mp_project_id)->whereIn('skill_id',$Input['skills'])->pluck('mp_seller_service_package_id')->toArray();

                $packageExtra = MarketPlaceSellerServiceSkill::with([
                    'packages' => function($query) use($packages) {
                        $query->whereIn('id',$packages)->where('status',1);
                    }
                ])->with('service_skill_detail')->whereIn('skill_id',$Input['skills'])->where('seller_id',$Input['seller_id'])->where('status',1)->get();
                $responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$packageExtra);
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, 'GENERAL_ERROR', Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Addprojectextra
    //Author : Senil shah <senil@creolestudios.com>
    //Purpose : To get details of extra packages and total it
    //In Params : Void
    //Return : json
    //Date : 10th July 2018
    //###############################################################
    public function Addprojectextra(Request $request) {
        try {
            if(Auth::check()){
                $Input    = Input::all();
                foreach ($Input as $key => $value)
                    $data[] = $value;
                $detailData = MarketPlaceSellerServiceProjectExtra::with('package_detail')->whereIn('id',$data)->get();
                $totalPrice = MarketPlaceSellerServiceProjectExtra::whereIn('id',$data)->sum('price');
                $detail['detailData'] = $detailData;
                $detail['cartTotal'] = $totalPrice;
                $responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$detail);
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }

    //###############################################################
    //Function Name : Makeextrapayment
    //Author : Senil shah <senil@creolestudios.com>
    //Purpose : To add project extras to project
    //In Params : Void
    //Return : json
    //Date : 10th July 2018
    //###############################################################
    public function Makeextrapayment(Request $request) {
        try {
            if(Auth::check()){
                $Input = Input::all();
                \DB::beginTransaction();
                $mp_project_id = substr($Input['mp_project_id'], strpos($Input['mp_project_id'], ":") + 1);
                $mp_project_id = base64_decode($mp_project_id);
                $packages      = MarketPlaceCart::where('mp_project_id',$mp_project_id)->first();
                $newTimeline = 0;
                foreach ($Input['cart'] as $key => $value) {
                    $newTimeline+=$value['delivery_time_in_days'];

                    $data[$key]['mp_seller_service_package_id']       = $packages['mp_seller_service_package_id'];
                    $data[$key]['mp_seller_service_project_extra_id'] = $value['id'];
                    $data[$key]['mp_project_id']                      = $mp_project_id;
                    $data[$key]['skill_id']                           = $packages['skill_id'];
                    $data[$key]['cart_id']                            = $packages['cart_id'];
                    $data[$key]['price']                              = $value['price'];
                    $data[$key]['created_at']                         = Carbon::now();
                    $data[$key]['updated_at']                         = Carbon::now();
                }
                $result = MarketPlaceCart::insert($data);
                if($result){
                    $projectDetails = MarketPlaceProject::where('id',$mp_project_id)->first();
                    $updateProjectBudget['project_budget'] = $Input['cartTotal'] + $projectDetails['project_budget'];
                    $updateProjectBudget['project_timeline'] = $newTimeline + $projectDetails['project_timeline'];
                    $updateProjectBudgetResult = UtilityController::Makemodelobject($updateProjectBudget,'MarketPlaceProject','',$projectDetails['id']);
                    \DB::commit();
                    $responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$updateProjectBudgetResult);
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, 'GENERAL_ERROR', Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
    }    

    //###############################################################
    //Function Name : InspirationBankinfo
    //Author : Ketan Solanki <ketan@creolestudios.com>
    //Purpose : To get all the detail of uploaded inspiration item by designer
    //In Params : Void
    //Return : json
    //Date : 17th July 2018
    //###############################################################
    public function InspirationBankinfo(Request $request)
    {
        try {
            if (Auth::check()) {
                $Input = Input::all();                                
                if (isset($Input['category_id'])) {                    
                    $categoryId = base64_decode($Input['category_id']);
                }                              
                $activeInspirationBank = MarketPlaceUpload::with('cover_image_attachments', 'inspiration_bank_files_attachments', 'inspiration_categories','uploadedMpDesignBy')->where('upload_of', 1)->where('id', $categoryId)->where('status', 1)->where('slug', $Input['slug'])->first()->toArray();                
                $activeInspirationBank['is_designer']              = Auth::user()->is_designer;
                $activeInspirationBank['marketplace_current_role'] = Auth::user()->marketplace_current_role;                                
                if (!empty($activeInspirationBank)) {
                    $selectedCategotyId = array();                    
                    //Check if this is uploaded by the same user
                    $activeInspirationBank['sameUserUploaded'] = false;                    
                    if(!empty($activeInspirationBank['uploaded_mp_design_by'])){                        
                        if($activeInspirationBank['uploaded_mp_design_by']['id'] == Auth::User()->id){
                            $activeInspirationBank['sameUserUploaded'] = true;
                        }
                    }                    
                    $coverImages =array();
                    $mediumImages =array();
                    $mediumImagesForEdit =array();
                    $originalImages = array();
                    foreach ($activeInspirationBank['cover_image_attachments'] as $keyInnerCover => $valueInnerCover) {                                                                                                          
                        $mediumImagesData   = array('id' => $valueInnerCover['id'], 'medium_path' => UtilityController::Imageexist($valueInnerCover['medium_thumbnail'], public_path('') . UtilityController::Getpath('MP_INSPIRATION_ATTACHMENT_MEDIUM_PATH'). Auth::user()->id .'/Cover_image/', url('') . UtilityController::Getpath('MP_INSPIRATION_ATTACHMENT_MEDIUM_URL').Auth::user()->id .'/Cover_image/'));                        
                        array_push($mediumImages,$mediumImagesData);
                        $originalImagesData = array('id' => $valueInnerCover['id'], 'original_path' => UtilityController::Imageexist($valueInnerCover['original_file_name'], public_path('') . UtilityController::Getpath('MP_INSPIRATION_ATTACHMENT_MEDIUM_PATH').Auth::user()->id .'/Cover_image/', url('') . UtilityController::Getpath('MP_INSPIRATION_ATTACHMENT_MEDIUM_URL').Auth::user()->id .'/Cover_image/'));
                        array_push($originalImages,$originalImagesData);
                        $coverImagesData   = array('id' => $valueInnerCover['id'], 'tiny_thumbnail' => UtilityController::Imageexist($valueInnerCover['tiny_thumbnail'], public_path('') . UtilityController::Getpath('MP_INSPIRATION_ATTACHMENT_MEDIUM_PATH'). Auth::user()->id .'/Cover_image/', url('') . UtilityController::Getpath('MP_INSPIRATION_ATTACHMENT_MEDIUM_URL').Auth::user()->id .'/Cover_image/'));                        
                        array_push($coverImages,$coverImagesData);                        
                        
                    }  
                    foreach ($activeInspirationBank['inspiration_bank_files_attachments'] as $keyInner => $valueInner) {                                                                                                                        
                        $mediumImagesData   = array('id' => $valueInner['id'], 'medium_path' => UtilityController::Imageexist($valueInner['medium_thumbnail'], public_path('') . UtilityController::Getpath('MP_INSPIRATION_ATTACHMENT_MEDIUM_PATH'). Auth::user()->id .'/Medium/', url('') . UtilityController::Getpath('MP_INSPIRATION_ATTACHMENT_MEDIUM_URL').Auth::user()->id .'/Medium/'));                        
                        array_push($mediumImages,$mediumImagesData);
                        array_push($mediumImagesForEdit,$mediumImagesData);
                        $originalImagesData = array('id' => $valueInner['id'], 'original_path' => UtilityController::Imageexist($valueInner['original_file_name'], public_path('') . UtilityController::Getpath('MP_INSPIRATION_ATTACHMENT_MEDIUM_PATH').Auth::user()->id .'/Original/', url('') . UtilityController::Getpath('MP_INSPIRATION_ATTACHMENT_MEDIUM_URL').Auth::user()->id .'/Original/'));
                        array_push($originalImages,$originalImagesData);
                        
                    }                    
                    foreach ($activeInspirationBank['inspiration_categories'] as $keyCategory=> $valueCategory) {
                        $selectedCategotyId[] = $valueCategory['categoty_id'];
                    }                                      
                    $activeInspirationBank['medium_images']     = $mediumImages;
                    $activeInspirationBank['medium_images_edit']     = $mediumImagesForEdit;
                    $activeInspirationBank['original_images']   = $originalImages;                    
                    $activeInspirationBank['cover_images']   = $coverImages;                    
                    $activeInspirationBank['categoty_selected'] = $selectedCategotyId;
                    $activeInspirationBank['next']              = MarketPlaceUpload::select('id', 'slug')->where('upload_of', 3)->where('id', $categoryId)->where('status', 1)->where('id', '>', $categoryId)->orderby('id', 'ASC')->first();
                    $activeInspirationBank['previous']          = MarketPlaceUpload::select('id', 'slug')->where('upload_of', 3)->where('id', $categoryId)->where('status', 1)->where('id', '<', $categoryId)->orderby('id', 'DESC')->first();                                                            
                    $responseArray                        = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $activeInspirationBank);
                } else {
                    $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 0);
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
        }
        return $responseArray;
    }

    //###############################################################
    //Function Name : MakeUserFavourite
    //Author : Ketan Solanki <ketan@creolestudios.com>
    //Purpose : To get all the detail of uploaded portfolio(s) by designer
    //In Params : Void
    //Return : json
    //Date : 13th April 2018
    //###############################################################
    public function MakeUserFavourite(Request $request)
    {
        try {
            if (Auth::check()) {
                $Input = Input::all();                                                               
                if(isset($Input['inspirationBankID']) && is_numeric($Input['inspirationBankID']) && $Input['inspirationBankID'] > 0){
                    //Check if the data already exists
                    $data = MarketPlaceInspirationBankUserFavourite::where("mp_upload_id",$Input['inspirationBankID'])->where("user_id",Auth::User()->id)->get()->toArray();                    
                    if(!empty($data)){
                        \DB::beginTransaction();
                        //Delete the data of user favourite and send message that you have made it unfavourite
                        $deleted = MarketPlaceInspirationBankUserFavourite::where("mp_upload_id",$Input['inspirationBankID'])->where("user_id",Auth::User()->id)->delete();
                        \DB::commit();
                        $returnMessage=$Input['language']=='en'?'FAVOURITE_REMOVED':'FAVOURITE_REMOVED_CHINESE';
                        $responseArray =  UtilityController::Generateresponse(true, $returnMessage, 1);
                    }else{
                        \DB::beginTransaction();
                        $marketPlaceInspirationBankUserFavouriteObject = new MarketPlaceInspirationBankUserFavourite;
                        $marketPlaceInspirationBankUserFavouriteObject->mp_upload_id = $Input['inspirationBankID'];
                        $marketPlaceInspirationBankUserFavouriteObject->user_id = Auth::User()->id;
                        if($marketPlaceInspirationBankUserFavouriteObject->save()){
                            \DB::commit();
                            $returnMessage=$Input['language']=='en'?'FAVOURITE_ADDED':'FAVOURITE_ADDED_CHINESE';
                            $responseArray =  UtilityController::Generateresponse(true, $returnMessage, 1);
                        }else{
                            $returnMessage=$Input['language']=='en'?'GENERAL_ERROR':'GENERAL_ERROR_CHINESE';
                            $responseArray = UtilityController::Generateresponse(false, $returnMessage, 2);
                        }
                    }
                }else{
                    $returnMessage=$Input['language']=='en'?'INSPIRATION_ID_NOT_PROVIDED':'INSPIRATION_ID_NOT_PROVIDED_CHINESE';
                    $responseArray = UtilityController::Generateresponse(false, $returnMessage, 2);
                }                
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
        }
        return $responseArray;
    }

    //###############################################################
    //Function Name : Deleteinspirationbankimage
    //Author : Ketan Solanki <ketan@creolestudios.com>
    //Purpose : To delete images in uploaded inspiration Bank(s) by designer
    //In Params : Void
    //Return : json
    //Date : 18th July 2018
    //###############################################################
    public function Deleteinspirationbankimage(Request $request)
    {
        try {
            if (Auth::check()) {
                \DB::beginTransaction();
                $Input     = Input::all();                                                

                if(isset($Input['imageType']) && $Input['imageType'] != ""){
                    $imageType = $Input['imageType'];
                }
                $inspirationBank = MarketPlaceUploadAttachment::where('mp_upload_id', $Input['project_id'])
                ->where('status', $imageType)
                ->get();                
                $inspirationBank = $inspirationBank->toArray();
                $count     = count($inspirationBank);
                if($imageType == 3){
                    $imageID        = $Input['image_id'];
                    $data['status'] = 2;
                    $result         = UtilityController::Makemodelobject($data, 'MarketPlaceUploadAttachment', '', $imageID);
                    if ($result) {
                        unlink(public_path('') . UtilityController::Getpath('MP_INSPIRATION_ATTACHMENT_MEDIUM_PATH') . Auth::User()->id ."/Cover_image/".$result['original_file_name']);
                        unlink(public_path('') . UtilityController::Getpath('MP_INSPIRATION_ATTACHMENT_MEDIUM_PATH') . Auth::User()->id ."/Cover_image/".$result['medium_thumbnail']);
                        unlink(public_path('') . UtilityController::Getpath('MP_INSPIRATION_ATTACHMENT_MEDIUM_PATH') . Auth::User()->id ."/Cover_image/".$result['tiny_thumbnail']);                        
                        \DB::commit();
                        $returnMessage=$Input['language']=='en'?'PORTFOLIO_IMAGE_DELETED':'PORTFOLIO_IMAGE_DELETED_CHINESE';
                        $responseArray = UtilityController::Generateresponse(true, $returnMessage, 1);
                    } else {
                        \DB::rollback();
                        $returnMessage=$Input['language']=='en'?'CANNOT_DELETE_LAST_IMAGE_INSPIRATION':'CANNOT_DELETE_LAST_IMAGE_INSPIRATION_CHINESE';
                        $responseArray = UtilityController::Generateresponse(true, $returnMessage, 0);
                    }
                }else{
                    if ($count > 1) {
                    $imageID        = $Input['image_id'];
                    $data['status'] = 2;
                    $result         = UtilityController::Makemodelobject($data, 'MarketPlaceUploadAttachment', '', $imageID);
                    if ($result) {
                        unlink(public_path('') . UtilityController::Getpath('MP_INSPIRATION_ATTACHMENT_MEDIUM_PATH') . Auth::User()->id ."/Original/".$result['original_file_name']);
                        unlink(public_path('') . UtilityController::Getpath('MP_INSPIRATION_ATTACHMENT_MEDIUM_PATH') . Auth::User()->id ."/Medium/".$result['medium_thumbnail']);
                        unlink(public_path('') . UtilityController::Getpath('MP_INSPIRATION_ATTACHMENT_MEDIUM_PATH') . Auth::User()->id ."/Tiny/".$result['tiny_thumbnail']);                        
                        \DB::commit();
                        $returnMessage=$Input['language']=='en'?'PORTFOLIO_IMAGE_DELETED':'PORTFOLIO_IMAGE_DELETED_CHINESE';
                        $responseArray = UtilityController::Generateresponse(true, $returnMessage, 1);
                    } else {
                        \DB::rollback();
                        $returnMessage=$Input['language']=='en'?'GENERAL_ERROR':'GENERAL_ERROR_CHINESE';
                        $responseArray = UtilityController::Generateresponse(false, $returnMessage, 0);
                    }
                } else {
                    $returnMessage=$Input['language']=='en'?'CANNOT_DELETE_LAST_IMAGE_INSPIRATION':'CANNOT_DELETE_LAST_IMAGE_INSPIRATION_CHINESE';
                    $responseArray = UtilityController::Generateresponse(false, $returnMessage, 0);
                }
                }                
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, $e->getMessage(), 0);
        }
        return $responseArray;
    }

     //###############################################################
    //Function Name : Editinspirationbank
    //Author : Ketan Solanki <ketan@creolestudios.com>
    //Purpose : To edit the current inspiration bank details
    //In Params : Void
    //Return : json
    //Date : 18th July 2018
    //###############################################################
    public function Editinspirationbank(Request $request)
    {
        try {
            if (Auth::check()) {
                \DB::beginTransaction();
                $Input                        = Input::all();
                $Input['inspirationBankInfo'] = json_decode($Input['inspirationBankInfo'], true);
                $Input['uploaded_user_id']    = Auth::user()->id;
                $Input['upload_of']           = 1;
                $validator                    = UtilityController::ValidationRules($Input, 'MarketPlaceUpload');
                if (!$validator['status']) {
                    $errorMessage  = explode('.', $validator['message']);
                    $responseArray = UtilityController::Generateresponse(false, $errorMessage['0'], 0);
                } else {
                    if (array_key_exists('skills', $Input)) {
                        $removeOldSkill = MarketPlaceInspirationBankCategoryDetail::where('mp_upload_id', $Input['inspirationBankInfo']['id'])->forceDelete();
                        $skillSets      = array_unique($Input['skills']);                
                        foreach ($skillSets as $key => $value) {
                            $inspirationCategory[$key]['mp_upload_id'] = $Input['inspirationBankInfo']['id'];
                            $inspirationCategory[$key]['categoty_id']  = $value;
                            $inspirationCategory[$key]['created_at']   = Carbon::now();
                            $inspirationCategory[$key]['updated_at']   = Carbon::now();
                        }                        
                        MarketPlaceInspirationBankCategoryDetail::insert($inspirationCategory);
                    } else {
                        $returnMessage=$Input['language']=='en'?'SELECT_ATLEAST_ONE_CATEGORY':'SELECT_ATLEAST_ONE_CATEGORY_CHINESE';
                        $errorMessage  = UtilityController::Getmessage($returnMessage);
                        $responseArray = UtilityController::Generateresponse(false, $errorMessage, 0);
                        return response()->json($responseArray);
                    }
                    if (array_key_exists('attachments', $Input)) {
                        foreach ($Input['attachments'] as $key => $value) {
                            $fileName  = rand() . time() . preg_replace('/[^A-Za-z0-9\-.]/', '', $value->getClientOriginalName());
                            $fileName = str_replace('-', '', $fileName);
                            $extension = \File::extension($fileName);

                            //==========================                            
                            $orgfileName  = rand() . time() . preg_replace('/[^A-Za-z0-9\-.]/', '', $value->getClientOriginalName());
                            $orgfileName = str_replace('-', '', $orgfileName);
                            $tinyfileName    = 'tiny_' . $orgfileName;
                            $mediumfileName  = 'medium_' . $orgfileName;
                            $galleryFile     = array();
                            $extension       = \File::extension($orgfileName);

                            if (! File::exists(public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id)) {
                                File::makeDirectory(public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id, 0777, true, true);                                    
                            }
                            if (! File::exists(public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id ."/Cover_image/")) {
                                File::makeDirectory(public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id . "/Cover_image/", 0777, true, true);
                            }
                            if (! File::exists(public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id ."/Medium/")) {
                                File::makeDirectory(public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id . "/Medium/", 0777, true, true);
                            }
                            if (! File::exists(public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id ."/Original/")) {
                                File::makeDirectory(public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id . "/Original/", 0777, true, true);
                            }
                            if (! File::exists(public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id ."/Tiny/")) {
                                File::makeDirectory(public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id . "/Tiny/", 0777, true, true);
                            }
                            chmod(public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id , 0777);
                            $DestinationPath = public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id.'/Cover_image/';
                            $DestinationPathTiny = public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id.'/Tiny/';
                            $DestinationPathOriginal = public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id.'/Original/';
                            $DestinationPathMedium = public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id.'/Medium/';
                            if (in_array($extension, array('png', 'jpg', 'jpeg', 'ico', 'gif', 'bmp'))) {
                                Image::make($value->getRealPath())->resize(1280, 720, function ($constraint) {
                                    $constraint->aspectRatio();
                                    $constraint->upsize();
                                })->save($DestinationPathMedium . $mediumfileName);
                                Image::make($value->getRealPath())->resize(300, 300, function ($constraint) {
                                    $constraint->aspectRatio();
                                    $constraint->upsize();
                                })->save($DestinationPathTiny . $tinyfileName);
                                Image::make($value->getRealPath())->resize(300, 300, function ($constraint) {
                                    $constraint->aspectRatio();
                                    $constraint->upsize();
                                })->save($DestinationPathOriginal . $orgfileName);

                                Image::make($value->getRealPath())->save($DestinationPath . $orgfileName);

                                $galleryFile['medium_thumbnail'] = $mediumfileName;
                                $galleryFile['tiny_thumbnail']     = $tinyfileName;
                                $galleryFile['original_file_name'] = $orgfileName;
                            } else {
                                $value->move($DestinationPathOriginal . $orgfileName);                                
                                $galleryFile['original_file_name'] = $orgfileName;
                            }
                            //==========================     
                            $uploadAttachment[$key]['mp_upload_id'] = $Input['inspirationBankInfo']['id'];                       
                            $uploadAttachment[$key]['original_file_name'] = $orgfileName;
                            $uploadAttachment[$key]['medium_thumbnail'] = $mediumfileName;
                            $uploadAttachment[$key]['tiny_thumbnail'] = $tinyfileName;
                            $uploadAttachment[$key]['status'] = 4;
                            $uploadAttachment[$key]['created_at'] = Carbon::now();
                            $uploadAttachment[$key]['updated_at'] = Carbon::now();                            
                        }
                        MarketPlaceUploadAttachment::insert($uploadAttachment);                         
                    }
                    if (!array_key_exists("cover_image", $Input)) {
                        $edit['cover_img'] = $Input['inspirationBankInfo']['cover_img'];
                    } else {
                        foreach($Input['cover_image'] as $key => $value){                            
                            if ($value->getClientSize() <= UtilityController::Getmessage('FILE_SIZE_2')) {                                
                                $fileName  =  rand() . time() . preg_replace('/[^A-Za-z0-9\-.]/', '', $value->getClientOriginalName());
                                $fileName = str_replace('-', '', $fileName);
                                $extension = \File::extension($fileName);
    
                                //==========================                                
                                $orgfileName  =  rand() . time() . preg_replace('/[^A-Za-z0-9\-.]/', '', $value->getClientOriginalName());
                                $orgfileName = str_replace('-', '', $orgfileName);
                                $tinyfileName    = 'tiny_' . $orgfileName;
                                $mediumfileName  = 'medium_' . $orgfileName;
                                $galleryFile     = array();
                                $extension       = \File::extension($orgfileName);
                                $DestinationPath = public_path('') . config('constants.path.MP_ATTACHMENT_INSPIRATION_PATH').Auth::user()->id.'/Cover_image/';
                                if (in_array($extension, array('png', 'jpg', 'jpeg', 'ico', 'gif', 'bmp'))) {
                                    Image::make($value->getRealPath())->resize(1280, 720, function ($constraint) {
                                        $constraint->aspectRatio();
                                        $constraint->upsize();
                                    })->save($DestinationPath . $mediumfileName);
                                    Image::make($value->getRealPath())->resize(300, 300, function ($constraint) {
                                        $constraint->aspectRatio();
                                        $constraint->upsize();
                                    })->save($DestinationPath . $tinyfileName);
    
                                    Image::make($value->getRealPath())->save($DestinationPath . $orgfileName);
    
                                    $galleryFile['medium_thumbnail'] = $mediumfileName;
                                    $galleryFile['tiny_thumbnail']     = $tinyfileName;
                                    $galleryFile['original_file_name'] = $orgfileName;
                                    $galleryFile['mp_upload_id'] = $Input['inspirationBankInfo']['id'];
                                    $galleryFile['status']=3;
                                    $addedFile[]                 = UtilityController::Makemodelobject($galleryFile, 'MarketPlaceUploadAttachment', 'id');
                                }else {
                                    $value->move($DestinationPath . $orgfileName);
                                    // file::make($Input['file'])->save($DestinationPath . $orgfileName);
                                    $galleryFile['original_file_name'] = $orgfileName;
                                }
                               
                            }else {
                                $returnMessage=$Input['language']=='en'?'FILE_SIZE_LARGE_2_FOR_INSPIRATION':'FILE_SIZE_LARGE_2_FOR_INSPIRATION_CHINESE';
                                throw new \Exception($Input['cover_image']->getClientOriginalName() . UtilityController::Getmessage($returnMessage));
                            }
                        }
                        } 
                        //soft delete the old cover images
                        if (array_key_exists("changedCoverImages", $Input)) {
                            MarketPlaceUploadAttachment::whereIn("id",json_decode($Input['changedCoverImages']))->where("status",3)->update(array('status' => 0));
                        }                  
                    $edit['upload_title']       = $Input['upload_title'];
                    $edit['upload_description'] = $Input['upload_description'];
                    $edit['slug']               = str_slug($Input['upload_title'], '-');
                   
                    $edit['slug']               = $edit['slug'] . ":" . base64_encode($Input['inspirationBankInfo']['id']);
                    $updateSlug                 = UtilityController::Makemodelobject($edit, 'MarketPlaceUpload', '', $Input['inspirationBankInfo']['id']);
                    if (isset($coverImagePath)) {
                        unlink($coverImagePath);
                    }
                    \DB::commit();                  
                    $returnMessage=$Input['language']=='en'?'INPIRATION_EDITED':'INPIRATION_EDITED_CHINESE';
                    $responseArray = UtilityController::Generateresponse(true, $returnMessage, 1);
                }                

            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, $e->getMessage(), 0);
        }
        return $responseArray;
    }

    //###############################################################
    //Function Name : DeleteinspirationBank
    //Author : Ketan Solanki <ketan@creolestudios.com>
    //Purpose : To delete uploaded inspiration bank item by designer
    //In Params : Void
    //Return : json
    //Date : 18th July 2018
    //###############################################################
    public function DeleteinspirationBank(Request $request)
    {
        try {
            if (Auth::check()) {
                \DB::beginTransaction();
                $Input           = Input::all();                
                $designerId      = Auth::user()->id;
                $inspiration_bank_id = substr($Input['inspiration_bank_details_title'], strpos($Input['inspiration_bank_details_title'], ":") + 1);
                $inspiration_bank_id = base64_decode($inspiration_bank_id);                
                $data['status']  = 2;
                $result          = UtilityController::Makemodelobject($data, 'MarketPlaceUpload', '', $inspiration_bank_id);
                if ($result) {
                    \DB::commit();
                    $returnMessage=$Input['language']=='en'?'INSPIRATION_DELETED':'INSPIRATION_DELETED_CHINESE';
                    $responseArray = UtilityController::Generateresponse(true,$returnMessage , 1);
                } else {
                    \DB::rollback();
                    $returnMessage=$Input['language']=='en'?'GENERAL_ERROR':'GENERAL_ERROR_CHINESE';
                    $responseArray = UtilityController::Generateresponse(false, $returnMessage, 0);
                }
            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
        }
        return $responseArray;
    }
    //###############################################################
    //Function Name : Getbuyercredit
    //Author : Zalak kapadia <zalak@creolestudios.com>
    //Purpose : to get outstanding balance for seller
    //In Params : Void
    //Return : json
    //Date : 16th May 2018
    //###############################################################
    public function Getbuyercredit()
    {
        try {

            if (Auth::check()) {
                $balance = MarketPlaceWallet::select('wallet_amount')->where('user_id', Auth::user()->id)->first();
                if ($balance['wallet_amount'] > 0) {
                    return $balance['wallet_amount'];
                } else {
                    return 0;
                }

            } else {
                $responseArray = UtilityController::Generateresponse(false, 'LOGGED_OUT', 2);
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 0);
            return $responseArray;
        }

    }

    
    //###############################################################
    //Function Name : Createexpressinterestuser
    //Author : Zalak kapadia <zalak@creolestudios.com>
    //Purpose : Create express Interest
    //In Params : Void
    //Return : json
    //Date : 25th July 2018
    //###############################################################
    public function Createexpressinterestuser(Request $request)
    {
        try {
            \DB::beginTransaction();
            
            $input=Input::all();
            $UserExists             = User::where('email_address', $input['email_address'])->where('status', 1)->first();
            if (!empty($UserExists)) {
                $returnMessage = $input['language']=='en'?'EMAIL_ALREADY_EXISTS':($input['language']=='chi'?'EMAIL_ALREADY_EXISTS_CHINESE':'EMAIL_ALREADY_EXISTS_RU');
                //$returnMessage=$input['language']=='en'?'EMAIL_ALREADY_EXISTS':'EMAIL_ALREADY_EXISTS_CHINESE';
                throw new \Exception(UtilityController::Getmessage($returnMessage));
            }
            else{
                
                $input['express_interest_status']=1;
                $result = UtilityController::Makemodelobject($input, 'User', '','');
                if($result){

                    \DB::commit();  
                    $returnMessage=$input['language']=='en'?'THANK_FOR_INTEREST':'THANK_FOR_INTEREST_CHINESE';
                    $responseArray     = UtilityController::Generateresponse(true, $returnMessage, 0); 
                    return $responseArray;     
                }
            }
            
            
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, $e->getMessage(), 0);
            return $responseArray;
        }

    }

    //###############################################################
    //Function Name : Showsellersonlandingpage
    //Author : Zalak kapadia <zalak@creolestudios.com>
    //Purpose : Show sellers on landing page
    //In Params : Void
    //Return : json
    //Date : 25th July 2018
    //###############################################################
    public function Showsellersonlandingpage(Request $request)
    {
        try {

            $sellerData=User::where('is_designer',1)->orderby('total_avg_rate','DESC')->withCount('reviews')->with('service_title','images')->where('marketplace_current_role', 1)
                    ->where('status', 1)->limit(8)->get();


            if(!empty($sellerData)){

                foreach ($sellerData as $key => $value) {
                    
                    $sellerData[$key]['seller_image']=UtilityController::Imageexist($value['image'], public_path('') . UtilityController::Getmessage('USER_PROFILE_PATH'), url('') . UtilityController::Getmessage('USER_PROFILE_URL'), url('') . UtilityController::Getmessage('ACCESSOR_NO_IMAGE_URL'));

                    if(!empty($value['service_title'])){

                        $sellerData[$key]['skill_count'] = count($value['service_title'])-1;

                        if($sellerData[$key]['skill_count']>0){
                            $sellerData[$key]['skill_name']=$value['service_title'][0]['skill_name'].' & '.$sellerData[$key]['skill_count'].' more';
                        }else{
                            $sellerData[$key]['skill_name']=$value['service_title'][0]['skill_name'];
                        }

                    }
                   
                    $images = [];
                    if(count($value['images'])>0){
                        
                        foreach($value['images'] as $keyInner => $valueInner){
                            
                            $images[] = UtilityController::Imageexist($valueInner['image_name'], public_path('') . UtilityController::Getpath('MP_SERVICES_UPLOAD_PATH'), url('') . UtilityController::Getpath('MP_SERVICES_UPLOAD_URL'), url('') . UtilityController::Getmessage('NO_IMAGE_URL'));
                            }
                            
                    }else{
                        $images[] = url('') . UtilityController::Getmessage('NO_IMAGE_URL');    
                    }
                    $sellerData[$key]['show_images'] = $images;
                    
                    }
                
                $responseArray     = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $sellerData);           
            }else{
                $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
            }
           
            return $responseArray; 
            
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, $e->getMessage(), 0);
            return $responseArray;
        

        }    
    }

    //###############################################################
    //Function Name : Checkemailexist
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : check email address for duplication
    //In Params : Void
    //Return : json
    //Date : 25th July 2018
    //###############################################################
    public function Checkemailexist(Request $request)
    {
        try {
            $Input=Input::all();
            $UserExists = User::where('email_address', $Input['email_address'])->where('status', 1)->where('marketplace_current_role',$Input['marketplace_current_role'])->exists();
            $responseArray = ($UserExists?UtilityController::Generateresponse(false,'EMAIL_ALREADY_EXISTS',0):UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1));
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(false, $e->getMessage(), 0);
        }
        return response()->json($responseArray);
    }

    public function Mywidget(Request $request){
        $data['widgetinfo']        = false;
        $data['widgetprojectinfo'] = false;
        
        return view('front/registerwidget', $data);
    }

    public function Mymembershipwidget(Request $request){
        $data['widgetinfo']        = false;
        $data['widgetprojectinfo'] = false;
        
        return view('front/widget', $data);
    }

}
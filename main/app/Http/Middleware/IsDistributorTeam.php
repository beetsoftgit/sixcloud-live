<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

class IsDistributorTeam
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::guard('distributor_team_members')->check()){
            if(Auth::guard('distributor_team_members')->user()->status==4){
                return redirect('/verify-phone-number');
            }
            if(Auth::guard('distributor_team_members')->user()->status==5){
                return redirect('/team-my-account');
            }
            if(isset(Auth::guard('distributor_team_members')->user()->userType)&&Auth::guard('distributor_team_members')->user()->userType=='distributor_team_member'&&Auth::guard('distributor_team_members')->user()->status==1) 
            {
                return redirect('/my-referral');
            }
        } else {
            if(Auth::check())
                return redirect('/ignite-categories');
            elseif(Auth::guard('distributors')->check())
                return redirect('/subscribers');
            else
                return redirect('/distributor/team-login');
        }
        return $next($request);
    }
}

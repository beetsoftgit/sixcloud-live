<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if(!isset(Auth::user()->userType)){
                if(Auth::user()->marketplace_current_role==1){
                    return redirect('seller-dashboard');
                    // return '/register/step-1';
                } else {
                    return redirect('buyer-dashboard');
                }
            } else {
                return redirect('ignite-categories');
            }
            // return redirect('/home');
        }

        return $next($request);
    }
}

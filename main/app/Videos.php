<?php

//###############################################################
//File Name : Countries.php
//Author : Komal Kapadi <komal@creolestudios.com>
//Purpose : to get list of countries
//Date : 5th Dec 2017
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\VideoNotes;
use App\VideoCategory;
use App\Subject;

class Videos extends Model {

    protected $table = 'videos';

    public function notes() {
        return $this->hasMany('App\VideoNotes', 'video_id');
    }

    public $rules = array(
        'video_category_id' => 'required',
        // 'title'             => 'required',
        // 'description'       => 'required',
        'video_ordering'    => 'required',
        'video_status'      => 'required',
    );

    public function category() {
        return $this->belongsTo('App\VideoCategory', 'video_category_id');
    }
    public function categoryWithSubject()
    {
        return $this->belongsTo('App\VideoCategory', 'video_category_id')->select('id', 'subject_id')->with('subjectZones');
    }
    public function video_urls() {
        return $this->hasMany('App\VideoLanguageUrl', 'video_id');
    }
    public function video_url() {
        return $this->hasOne('App\VideoLanguageUrl', 'video_id');
    }
    public function pre_requisites() {
        return $this->hasMany('App\IgnitePrerequisite', 'video_id');
    }
    public function zones() {
        return $this->hasOne('App\ZoneVideo', 'video_id','id');
    }
    public function videoszones() {
        return $this->hasOne('App\ZoneVideo', 'video_id');
    }
    public function subcategoryWithOnlyName() {
        return $this->belongsTo('App\VideoCategory', 'subcategory_id')->select('id','subcategory_name','subcategory_name_chi','subcategory_name_ru','category_name');
    }
    public function subject_details()
    {
        return $this->belongsTo('App\Subject', 'subject_id', 'id')->select('id','subject_name');
    }
    public function category_details()
    {
        return $this->belongsTo('App\VideoCategory', 'subcategory_id')->select('id','subcategory_name','category_name')->with('subscription_details');
    }
    public function subcategory_details()
    {
        return $this->belongsTo('App\VideoCategory', 'category_id')->select('id','subcategory_name');
    }
    public function video_views_details($value='')
    {
        return $this->hasMany('App\VideoViews', 'id');
    }
}

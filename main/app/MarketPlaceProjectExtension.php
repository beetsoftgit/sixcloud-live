<?php

//###############################################################
//File Name : MarketPlaceProjectExtension.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to project extension given by designer
//Date : 26th April, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketPlaceProjectExtension extends Model
{
    protected $table  = 'mp_project_extension';
}
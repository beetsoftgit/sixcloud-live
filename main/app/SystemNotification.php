<?php

//###############################################################
//File Name : SystemNotification.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to notifications for the entire system
//Date : 24th Apr, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
class SystemNotification extends Model
{
    protected $table = 'system_notifications';
}

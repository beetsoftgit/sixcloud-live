<?php

//###############################################################
//File Name : MarketplaceAction.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to projects created by buyer from front end
//Date : 19th Mar, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
class MarketplaceAction extends Model
{
    protected $table = 'mp_actions';
    public $rules    = array(
        'mp_project_id'       => 'required',
        'action_type'         => 'required',
    );
}

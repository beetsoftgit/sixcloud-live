<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoCategoryHistory extends Model
{
    protected $table = 'video_categories_history';

    public function payment_usd() {
    	return $this->hasMany('App\MarketPlacePayment', 'history_id')->where('payment_amount_type', 1)->where('payment_status',1);
    }
    public function payment_sgd() {
        return $this->hasMany('App\MarketPlacePayment', 'history_id')->where('payment_amount_type', 2)->where('payment_status',1);
    }
}

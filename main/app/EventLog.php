<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventLog extends Model
{
	protected $table = 'event_log';
	
    public $rules = array(
        'user_id'      => 'required',
    );

    public function user_details() {
        return $this->belongsTo('App\User', 'user_id');
    }
    public function video_details() {
        return $this->belongsTo('App\Videos', 'video_id');
    }
    public function quiz_details()
    {
    	return $this->belongsTo('App\Quizzes', 'quiz_id');
    }
    public function worksheet_details()
    {
    	return $this->belongsTo('App\Worksheets', 'worksheet_id')->select('id','title');
    }
}

<?php

//###############################################################
//File Name : MarketPlaceUploadAttachment.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to files uploaded by designer for inspiration bank or gallery or portfolio
//Date : 19th Mar, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketPlaceUploadAttachment extends Model
{
    protected $table = 'mp_upload_attachments';
    public $rules    = array(
        'mp_upload_id'       => 'required',
        'original_file_name' => 'required',
    );

    public function uploadedby()
    {
        return $this->hasOne('App\User', 'id', 'uploaded_user_id');
    }
    public function files()
    {
        return $this->hasMany('App\MarketPlaceGalleryFiles', 'id');
    }

}

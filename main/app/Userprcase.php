<?php

//###############################################################
//File Name : Userprcase.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to PR cases
//Date : 6th Dec 2017
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userprcase extends Model
{
    protected $table  = 'user_pr_cases';

    public function accessment_time(){
    	return $this->hasMany('App\Userprcaselog', 'user_pr_case_id');
    }
    public function proof_reading_package(){
    	return $this->belongsTo('App\Proofreadingpackage', 'proof_reading_package_id');
    }
    public function case_user(){
    	return $this->belongsTo('App\User', 'user_id');
    }
}
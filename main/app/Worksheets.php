<?php

//###############################################################
//File Name : Countries.php
//Author : Komal Kapadi <komal@creolestudios.com>
//Purpose : to get list of countries
//Date : 5th Dec 2017
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\VideoNotes;
use App\VideoCategory;

class Worksheets extends Model {

    protected $table = 'worksheets';

    public $rules = array(
        'worksheet_category_id' => 'required',
        // 'title'                 => 'required',
        // 'description'           => 'required',
        'worksheet_ordering'    => 'required',
        'status'                => 'required',
    );

    public function category() {
        return $this->belongsTo('App\VideoCategory', 'worksheet_category_id');
    }
    public function categoryWithSubject()
    {
        return $this->belongsTo('App\VideoCategory', 'worksheet_category_id')->select('id', 'subject_id')->with('subjectZones');
    }
    public function zones() {
        return $this->hasOne('App\ZoneWorksheet', 'worksheet_id','id');
    }
    public function worksheet_language() {
        return $this->hasMany('App\WorksheetLanguage', 'worksheet_id');
    }
    public function worksheetlanguage() {
        return $this->hasone('App\WorksheetLanguage', 'worksheet_id');
    }
    public function worksheet_downloads() {
        return $this->belongsTo('App\WorksheetDownloads', 'id','worksheet_id');
    }
}

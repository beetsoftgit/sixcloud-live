<?php

//###############################################################
//File Name : MarketPlaceSellerServicePackage.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related seller service Package
//Date : 22nd May, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketPlaceSellerServicePackage extends Model
{
    protected $table  = 'mp_seller_service_packages'; 
    public $rules = array(
        'mp_seller_service_skill_id' => 'required',
        'service_title'              => 'required',
        'service_description'        => 'required',
        'no_of_revisions'            => 'required|numeric',
        'delivery_time_in_days'      => 'required|numeric|min:3',
        'price'                      => 'required|numeric',
        'status'                     => 'required',
    );

    public function skill_detail()
    {
        return $this->hasMany('App\SkillSet', 'id', 'skill_id');
    }

    public function service_extras()
    {
        return $this->hasMany('App\MarketPlaceSellerServiceProjectExtra', 'mp_seller_service_pacakage_id', 'id')->where('status',1);
    }

    public function service_images()
    {
        return $this->hasMany('App\MarketPlaceSellerServiceImage', 'mp_seller_service_pacakage_id', 'id')->where('status',1);
    }
    public function skill_parent()
    {
        return $this->belongsTo('App\MarketPlaceSellerServiceSkill', 'mp_seller_service_skill_id', 'id')->where('status',1)->with('service_skill_detail');
    }

    
}
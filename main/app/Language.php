<?php

//###############################################################
//File Name : Language.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to skill sets i.e category for market place
//Date : 19th Mar, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $table  = 'languages';
}
<?php

//###############################################################
//File Name : MarketPlaceUpload.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to files uploaded by designer for inspiration bank or gallery or portfolio
//Date : 19th Mar, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class MarketPlaceGalleryFiles extends Model
{
    protected $table = 'mp_gallery_files';
}

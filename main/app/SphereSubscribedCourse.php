<?php

//###############################################################
//File Name : MarketPlacePayment.php
//Author : Nivedita <nivedita@creolestudios.com>
//Purpose : related to MP alipay payemnt
//Date : 9th May, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;
use Carbon\carbon;

class SphereSubscribedCourse extends Model
{
    // protected $table  = 'mp_payments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','course_id','created_at', 'updated_at','deleted_at'
    ];

    public function course_details()
    {
        return $this->hasOne('App\SphereCourse', 'id', 'course_id');
    }
    
    protected $appends = ['course_detail'];
    
    function getCourseDetailAttribute() {
      return $this->course_details;
    }
}


<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorksheetDownloads extends Model
{
    protected $table = 'worksheet_downloads';

    public function worksheet_details() {
        return $this->belongsTo('App\Worksheets', 'worksheet_id','id');
    }
}

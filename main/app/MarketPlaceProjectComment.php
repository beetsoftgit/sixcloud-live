<?php

//###############################################################
//File Name : MarketPlaceProjectComment.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to comments done on projects
//Date : 19th Mar, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketPlaceProjectComment extends Model
{
    protected $table  = 'mp_project_comments'; 
    public $rules = array(
        'mp_project_id' => 'required',
        'user_id'       => 'required',
        'comment'       => 'required',
    ); 
}
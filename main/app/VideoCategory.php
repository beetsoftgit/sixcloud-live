<?php

//###############################################################
//File Name : Countries.php
//Author : Komal Kapadi <komal@creolestudios.com>
//Purpose : to get list of countries
//Date : 5th Dec 2017
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoCategory extends Model {

    public $rules = array(
        'category_name' => 'required'
    );
    protected $table = 'video_categories';

    public function videos() {
        return $this->hasMany('App\Videos', 'video_category_id');
    }

    public function quizzes() {
        return $this->hasMany('App\Quizzes', 'category_id');
    }

    public function worksheets() {
        return $this->hasMany('App\Worksheets', 'worksheet_category_id');
    }

    public function subcategories() {
        return $this->hasMany('App\VideoCategory', 'parent_id');
    }

    public function subject() {
        return $this->belongsTo('App\Subject');
    }

    public function subjectZones() {
        return $this->belongsTo('App\Subject', 'subject_id')->select('id', 'zones');
    }
    public function subscription_details() {
        return $this->belongsTo('App\MarketPlacePayment', 'video_category_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaxRate extends Model
{
 	protected $table = 'tax_rate';
    public $rules    = array(
        'tax_rate'  	=> 'required|numeric',
        'date'			=> 'required',
        'gst_number' 	=> 'required',
    );

    public function admin()
    {
        return $this->hasOne('App\Admin', 'id', 'admin_id');
    }
}

<?php

//###############################################################
//File Name : MarketPlaceSellerServiceImage.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related seller service package images
//Date : 22nd May, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketPlaceSellerServiceImage extends Model
{
    protected $table  = 'mp_seller_service_images'; 
    public $rules = array(
        'mp_seller_service_pacakage_id' => 'required',
        'image_name'                    => 'required',
        'status'                        => 'required',
    );

    public function skill_detail()
    {
        return $this->hasMany('App\SkillSet', 'id', 'skill_id');
    }
}
<?php

//###############################################################
//File Name : MarketPlaceProjectAttachment.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to projects attachments uploaded by the buyer for reference
//Date : 19th Mar, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketPlaceProjectAttachment extends Model
{
    protected $table  = 'mp_project_attachments'; 
    public $rules = array(
        'mp_project_id'       => 'required',
        'attachment_name' => 'required',
    ); 
}
<?php

//###############################################################
//File Name : Education.php
//Author : Jainam Shah
//Date : 29th March, 2019
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Education extends Model
{
    protected $table  = 'educations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'education',
    ];
}
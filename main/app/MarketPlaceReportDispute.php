<?php

//###############################################################
//File Name : MarketPlaceUpload.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to files uploaded by designer for inspiration bank or gallery or portfolio
//Date : 19th Mar, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketPlaceReportDispute extends Model
{
    protected $table = 'mp_report_disputes';
    public $rules = array(
        'mp_project_id'   => 'required',
        'reported_by_id'  => 'required',
        'reported_for_id' => 'required',
        'reason'          => 'required',
    ); 

    public function purchasedFiles()
    {
        return $this->hasOne('App\MarketPlaceUpload', 'id', 'mp_upload_id');
    }

    public function reportedfor()
    {
        return $this->belongsTo('App\User', 'reported_for_id');
    }

    public function reportedby()
    {
        return $this->belongsTo('App\User', 'reported_by_id');
    }
}

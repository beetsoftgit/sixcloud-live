<?php

//###############################################################
//File Name : SphereMessages.php
//Author : Jainam Shah
//Date : 15th March, 2019
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class SphereMessages extends Model
{
    protected $table  = 'sphere_messages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sphere_message_board_id', 'from_id', 'to_id', 'message', 'status'
    ];
}
<?php

//###############################################################
//File Name : Countries.php
//Author : Komal Kapadi <komal@creolestudios.com>
//Purpose : to get list of countries
//Date : 5th Dec 2017
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table  = 'countries';
    //Added By ketan Solanki for coiuntry list with States and Cities
    public function states() {
        return $this->hasMany('App\State', 'country_id');
    }

}

<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use App\User;
/*use App\MarketPlaceProjectsDetailInfo;*/

class SellerUnavailibilitySettingFlag implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user_id;
    protected $set_unavailable;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user_id,$set_unavailable)
    {
        $this->user_id         = $user_id;
        $this->set_unavailable = $set_unavailable;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            $userId          = $this->user_id;
            $set_unavailable = $this->set_unavailable;

            $user = User::where('id',$userId)->first();
            $user = json_decode(json_encode($user),true);
            if(!empty($user)){

                $updateData['availability_unavailability_satus'] = $set_unavailable;
                $result = \App\Http\Controllers\UtilityController::Makemodelobject($updateData, 'User', '', $userId);

                $data['designer']['first_name']       = $user['first_name'];
                $data['designer']['last_name']        = $user['last_name'];
                $data['designer']['email_address']    = $user['email_address'];
                $data['designer']['current_language'] = $user['current_language'];
                $data['designer']['set_unavailable']  = $set_unavailable;

                Mail::send('emails.notify_designer_for_availibility_status', $data, function ($message) use ($data) {
                        $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                        $message->to($data['designer']['email_address'])->subject('You will be again featured in suggestions');
                });
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = \App\Http\Controllers\UtilityController::Sendexceptionmail($e);
        }
    }
}

<?php

namespace App\Jobs;


use App\MarketPlaceDesignerRating;
use App\MarketPlaceDesignerReview;
use App\MarketplaceProject;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use URL;

class MpNotifyBuyerForRateAndReview implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $mp_project_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($mp_project_id,$after_hours)
    {
        $this->mp_project_id = $mp_project_id;
        $this->after_hours = $after_hours; 
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {

            $mp_project_id = $this->mp_project_id;
            $after_hours=24;

            $countRate=MarketPlaceDesignerRating::where('mp_project_id',$mp_project_id)->count();
            $countReview=MarketPlaceDesignerReview::where('mp_project_id',$mp_project_id)->count();

            if($countRate<=0 || $countReview<=0){
                $projectData  = MarketplaceProject::with('projectCreator')->where('id',$mp_project_id)->first();
                $projectData=$projectData->toArray();
                $projectData['subject']=$projectData['project_creator']['current_language']==1?'Notification for Rate and Review seller':'费率和评论卖家通知';

                $projectData['content']=$projectData['project_creator']['current_language']==1?'Hello <strong>'.$projectData['project_creator']['first_name'].' '.$projectData['project_creator']['last_name'].' ,
                    </strong><br/>
                    <br/>
                    Your Project
                     <strong>ID: '.$projectData['project_number'].' </strong>  has been delivered!
                    <br/><br/>You can help your Seller improve their services by completing this <a href="'.URL::to('/buyer-past-projects').'">review</a><br/><br/>Thank you for using SixClouds Sixteen. We look forward to more Projects from you.
                    <br/><br/><br/>':'你好 <strong>'.$projectData['project_creator']['first_name'].' '.$projectData['project_creator']['last_name'].'</strong> ,
                    <br/>
                    <br/>
                    您的项目 <strong>ID: '.$projectData['project_number'].' </strong> 已经交付！ <br/><br/> 您可以通过完成<a href="'.URL::to('/buyer-past-projects').'">此评价</a>来帮助卖家改善服务。
                    <br/><br/><br/>请记得删除六云十六平台之外的项目相关文件的所有副本。 <br/><br/>感谢您使用六云十六。我们期待在未来能再次与您合作。<br/><br/>';

                $projectData['footer_content']=$projectData['project_creator']['current_language']==1?'From,<br /> SixClouds Sixteen':'六云十六 ';

                $projectData['footer']=$projectData['project_creator']['current_language']==1?'SixClouds':'六云'; 

                Mail::send('emails.email_template', $projectData, function ($message) use ($projectData) {
                    $message->from(Config('constants.messages.MAIL_ID'), 'SixClouds');
                    $message->to($projectData['project_creator']['email_address'])->subject($projectData['subject']);
                }); 
            }
            /*if (!empty($projectData)) {
              
                Mail::send('emails.MpMessageBorad', $data, function ($message) use ($data) {
                    $message->from('noreply@sixclouds.cn', 'SixClouds');
                    $message->to($this->user['email_address'])->subject('Unread messages');
                });
            }*/
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = \App\Http\Controllers\UtilityController::Sendexceptionmail($e);
        }
    }
}

<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use App\CustomerSupport;
/*use App\MarketPlaceProjectsDetailInfo;*/

class MpAdminMarkTicketComplete implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $ticket_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($ticket_id)
    {
        $this->ticket_id         = $ticket_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            $ticketId          = $this->ticket_id;
            
            $result=CustomerSupport::where('id', $ticketId)->update(array('ticket_status' => 2));

        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = \App\Http\Controllers\UtilityController::Sendexceptionmail($e);
        }
    }
}

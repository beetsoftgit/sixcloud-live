<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\DistributorsTeamMembers;

class ChangeSalespersonCommission implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $newCommission, $renewCommission,$teamMemberId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($newCommission, $renewCommission, $teamMemberId)
    {
        $this->newCommission = $newCommission;
        $this->renewCommission = $renewCommission;
        $this->teamMemberId = $teamMemberId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try{
            $newCommission = $this->newCommission;
            $renewCommission = $this->renewCommission;
            $teamMemberId = $this->teamMemberId;
            DistributorsTeamMembers::where('id',$teamMemberId)->update(['commission_percentage'=>$newCommission,'renewal_commission_percentage'=>$renewCommission]);
        } catch (\Exception $e) {
            $sendExceptionMail = \App\Http\Controllers\UtilityController::Sendexceptionmail($e);
        }
    }
}

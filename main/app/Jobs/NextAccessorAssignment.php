<?php

namespace App\Jobs;
// namespace App\Http\Controllers;
use App\Userprcase;
use App\Userprcaselog;
use App\Prcaseassignment;
use App\Accessor;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class NextAccessorAssignment implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $assignment;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($assignment)
    {
        $this->assignment = $assignment;
    }
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 50;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $userPrCaseID = $this->assignment['user_pr_case_id'];
        $accessorID = $this->assignment['accessor_id'];
        $case = Prcaseassignment::withCount('case_assignment')->where('user_pr_case_id',$userPrCaseID)->where('accessor_id',$accessorID)->first();
        $case = json_decode(json_encode($case),true);
        if($case['case_assignment_count'] < 5){
            if($case['action_status'] == 4 ){
                // call CaseAssignment with the current accessor id
                $data['action_status'] = 3;
                $result = \App\Http\Controllers\UtilityController::Makemodelobject($data,'Prcaseassignment','',$case['id']);
                $setAccessorAvailabilityData['availability_status'] = 1;
                $setAccessorAvailability = \App\Http\Controllers\UtilityController::Makemodelobject($setAccessorAvailabilityData,'Accessor','',$this->assignment['accessor_id']);
                $job = dispatch(new CaseAssignment($case['user_pr_case_id'],$accessorID));
            }
        }
        else{
            $statusChange['cases_type'] = 2;
            $specialCase                = \App\Http\Controllers\UtilityController::Makemodelobject($statusChange,'Userprcase','',$this->assignment['user_pr_case_id']);
        }
    }
}

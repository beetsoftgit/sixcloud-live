<?php

//###############################################################
//File Name : MarketPlaceDesignerReview.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to reviews given to designers by the buyers
//Date : 19th Mar, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketPlaceDesignerReview extends Model
{
    protected $table = 'mp_designer_reviews';
    public $rules    = array(
        'review_by'    => 'required',
        'review_to_id' => 'required',
        'review'       => 'required',
    );
    public function Reviewgivenby()
    {
        return $this->belongsTo('App\User', 'review_by');
    }
}

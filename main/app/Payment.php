<?php

//###############################################################
//File Name : Payment.php
//Author : Komal Kapadi <komal@creolestudios.com>
//Purpose : to get list of payments
//Date : 21st Dec 2017
//###############################################################

namespace App;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\SubscriptionPlan;
class Payment extends Model
{
    protected $table  = 'payments';
    public function users() {
        return $this->belongsTo('App\User', 'user_id');
    }
    
    public function subscription() {
        return $this->belongsTo('App\SubscriptionPlan', 'subscription_plan_id');
    }
}

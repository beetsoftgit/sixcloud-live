<?php

//###############################################################
//File Name : IgniteCommissions.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to ticket types for customer support
//Date : 19th Mar, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class IgniteCommissions extends Model
{
    protected $table  = 'ignite_commissions';

    public function video_details()
    {
        return $this->hasOne('App\VideoCategory', 'id','video_category_id');
    }
    
}


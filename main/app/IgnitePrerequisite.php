<?php

//###############################################################
//File Name : IgnitePrerequisite.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to prerequisites for video or quiz
//Date : 13th July 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\VideoNotes;
use App\VideoCategory;

class IgnitePrerequisite extends Model {

    protected $table = 'ignite_pre_requisites';
    protected $fillable = ['quiz_id','video_id','pre_requist_video_id','pre_requist_quiz_id'];
    protected $dates = ['created_at','updated_at'];

    public function category() {
        return $this->belongsTo('App\VideoCategory', 'video_category_id');
    }
}

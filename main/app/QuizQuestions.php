<?php

//###############################################################
//File Name : Countries.php
//Author : Komal Kapadi <komal@creolestudios.com>
//Purpose : to get list of countries
//Date : 5th Dec 2017
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuizQuestions extends Model
{
    protected $table = 'quiz_questions';
    public function quiz()
    {
        return $this->belongsTo('App\Quizzes', 'quiz_id');
    }    
    public function quizquestionsoptions() {
        return $this->hasMany('App\QuizQuestionOptions', 'quiz_question_id');
    }
    public function alreadyattempted()
    {
        return $this->hasmany('App\QuizAttempts', 'question_id');
    }
}

<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable {

    use Notifiable;

    protected $table = 'admins';
    protected $guard = 'admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'job_title',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public $rules = array(
        'first_name' => 'required',
        'last_name' => 'required',
        'email_address' => 'required|email',
        'password' => 'required',
        'phone' => 'required',
        'image' => 'required',
        'dob' => 'required',
        'modules' => 'required'
    );

    public function admins() {
        return $this->belongsTo('App\Admin', 'reporting_officer_id');
    }
    public function reporting_officer() {
        return $this->hasOne('App\Admin', 'reporting_officer_id');
    }
}

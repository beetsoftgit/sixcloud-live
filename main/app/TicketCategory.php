<?php

//###############################################################
//File Name : TicketCategory.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to ticket categories for ticket types
//Date : 19th Mar, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketCategory extends Model
{
    protected $table  = 'ticket_category';
}
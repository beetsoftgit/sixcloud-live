<?php

//###############################################################
//File Name : Accessor.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to PR cases accessors
//Date : 7th Dec 2017
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accessor extends Model
{
    protected $table  = 'accessors';
    public $rules = array(
        'first_name'    => 'required',
        'last_name'     => 'required',
        'email_address' => 'required|email',
        'phone_number'  => 'required|numeric',
        'team'          => 'required',
        'level_type'    => 'required',
        'accessor_id'   => 'required',
    ); 

    public function parent_accessor(){
    	return $this->hasOne('App\Accessor', 'id', 'accessor_id');
    }
    public function total_jobs(){
    	return $this->hasMany('App\Userprcaselog', 'accessor_id')->where('case_status',3);
    }
    public function accessment_time(){
    	return $this->hasMany('App\Userprcaselog', 'accessor_id');
    }
    public function case_user(){
        return $this->belongsTo('App\User', 'user_id');
    }
    public function case_log(){
        return $this->hasOne('App\Userprcaselog', 'accessor_id');
    }
    public function assigned_accessor(){
        return $this->hasOne('App\Prcaseassignment', 'accessor_id');
    }
}
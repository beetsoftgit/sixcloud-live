<?php

//###############################################################
//File Name : School.php
//Author : Komal Kapadi <komal@creolestudios.com>
//Purpose : to get schools
//Date : 21st March 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model {

    protected $table = 'schools';

}

<?php

//###############################################################
//File Name : SubscriptionPlan.php
//Author : Komal Kapadi <komal@creolestudios.com>
//Purpose : to get list of subscription plans
//Date : 21st Dec 2017
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Payment;
class SubscriptionPlan extends Model
{
    protected $table = 'subscription_plan';

    public function uniquePayment()
    {
        return $this->hasMany('App\Payment', 'subscription_plan_id');
    }
}

<?php

//###############################################################
//File Name : VideoNotes.php
//Author : Komal Kapadi <komal@creolestudios.com>
//Purpose : to get Notes of video
//Date : 13th Dec 2017
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoNotes extends Model {

    protected $table = 'video_notes';
    public $rules = array(
        'video_id' => 'required',
        'user_id' => 'required',
        'notes' => 'required|email'
    );

}

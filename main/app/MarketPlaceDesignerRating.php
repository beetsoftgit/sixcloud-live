<?php

//###############################################################
//File Name : MarketPlaceDesignerRating.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to ratings given to designers by the buyers
//Date : 19th Mar, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketPlaceDesignerRating extends Model
{
    protected $table  = 'mp_designer_ratings'; 
    public $rules = array(
        'mp_project_id' => 'required',
        'rating_by'     => 'required',
        'rating_to_id'  => 'required',
        'rating'        => 'required',
    ); 
}
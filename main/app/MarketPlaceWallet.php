<?php

//###############################################################
//File Name : MarketPlaceProject.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to projects created by buyer from front end
//Date : 19th Mar, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;
class MarketPlaceWallet extends Model
{
    protected $table = 'user_wallet';
}
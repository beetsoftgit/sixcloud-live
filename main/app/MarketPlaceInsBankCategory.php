<?php

//###############################################################
//File Name : MarketPlaceInsBankCategory.php
//Author : Komal Kapadi <komal@creolestudios.com>
//Purpose : to get Inspiration bank's category
//Date : 5th Dec 2017
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketPlaceInsBankCategory extends Model
{

    protected $table = 'mp_ins_bank_category';

    public function uploads()
    {
        return $this->hasMany('App\MarketPlaceUpload', 'category_id')->where('upload_of',1);
    }

}

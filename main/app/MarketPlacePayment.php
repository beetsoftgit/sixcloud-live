<?php

//###############################################################
//File Name : MarketPlacePayment.php
//Author : Nivedita <nivedita@creolestudios.com>
//Purpose : related to MP alipay payemnt
//Date : 9th May, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;
use Carbon\carbon;

class MarketPlacePayment extends Model
{
    protected $table  = 'mp_payments';
    //protected $table  = 'payments_temp';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'payment_method', 'paid_by', 'mp_project_id', 'video_category_id', 'video_category_name', 'distributor_id', 'distributer_team_member_id', 'subscription_end_date', 'remarks', 'access_platform', 'alipay_out_trade_no', 'alipay_trade_no', 'transaction_reference', 'promo_code', 'discount_type', 'discount_of', 'promo_code_id', 'paid_for', 'payment_amount', 'payment_status', 'alipay_seller_id', 'alipay_timestamp', 'alipay_method', 'alipay_auth_app_id','alipay_version', 'alipay_app_id', 'alipay_sign_type','alipay_sign', 'system_transaction_number', 'slug','created_at', 'updated_at'
    ];

    public function projectpaymentdetailinfo()
    {
        return $this->hasOne('App\MarketPlaceProject', 'id', 'mp_project_id')->with('selected_designer','skills');
    }
    public function projectpaymentuserdetailinfo()
    {
        return $this->hasOne('App\User', 'id', 'paid_by');
    }
    public function userwalletdetail()
    {
        return $this->hasOne('App\MarketPlaceWallet', 'user_id', 'paid_by');
    }
    public function video_categories()
    {
        return $this->hasOne('App\VideoCategory', 'id', 'video_category_id');
    }
    public function distributor()
    {
        return $this->hasOne('App\Distributors', 'id', 'distributor_id');
    }
    public function distributor_team_member()
    {
        return $this->hasOne('App\DistributorsTeamMembers', 'id', 'distributer_team_member_id');
    }
    public function user_detail()
    {
        return $this->hasOne('App\User', 'id', 'paid_by');
    }
    public function switching_category()
    {
        return $this->hasMany('App\MarketPlacePayment', 'switch_id');
    }
    public function parent_category()
    {
        return $this->hasMany('App\MarketPlacePayment', 'parent_id');
    }
    public function promocode()
    {
        return $this->hasOne('App\StorePromocode', 'category_id', 'video_category_id')->where('user_id',Auth::user()->id);
    }
    public function promocodeDetails()
    {
        return $this->hasOne('App\StorePromocode', 'category_id', 'video_category_id');
    }
    public function promoDetails()
    {
        return $this->belongsTo('App\PromoCode', 'promo_code_id');
    }
    public function renewal_category()
    {
        return $this->hasMany('App\MarketPlacePayment', 'renewal_id');
    }
    public function switch_category()
    {
        return $this->belongsTo('App\MarketPlacePayment', 'switch_id');
    }
    protected $appends = ['payment_at','end_date','payment_mode', 'upcoming_end_date', 'promo_encode'];
    function getPaymentAtAttribute() {
      return Carbon::parse($this->created_at)->format('d M, Y');
    }
    function getEndDateAttribute() {
        if(!is_null($this->subscription_end_date)){
            return Carbon::parse($this->subscription_end_date)->format('d M, Y');
        }
    }
    function getUpcomingEndDateAttribute() {
        if(!is_null($this->upcoming_plan_date)){
            return Carbon::parse($this->upcoming_plan_date)->format('d M, Y');
        }
    }
    function getPaymentModeAttribute() {
        /*1: Alipay, 2: Paypal, 3: Credit, 4 : Wechat, 5 : Strip, 6 : Apple in-App purchase*/
        switch ($this->payment_method) {
            case 1:
                $paymentMode = 'Alipay';
                break;
            case 2:
                $paymentMode = 'Paypal';
                break;
            case 3:
                $paymentMode = 'Credit';
                break;
            case 4:
                $paymentMode = 'Wechat';
                break;
            case 5:
                $paymentMode = 'Strip';
                break;
            case 6:
                $paymentMode = 'Apple Pay';
                break;
            default:
                $paymentMode = 'Wechat';
                break;
        }
        return $paymentMode;
    }

    function getPromoEncodeAttribute() {
        if(!is_null($this->promo_code)){
            return base64_encode($this->promo_code);
        }
    }
}


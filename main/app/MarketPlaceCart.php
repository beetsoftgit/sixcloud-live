<?php

//###############################################################
//File Name : MarketPlaceCart.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to services added to cart
//Date : 21st Jun 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketPlaceCart extends Model
{
    protected $table  = 'mp_cart';
    public $rules = array(
        'mp_seller_service_package_id' => 'required',
        'cart_id'                      => 'required',
        'skill_id'                     => 'required',
        'price'                        => 'required',
    ); 

    public function package_details()
    {
        return $this->hasOne('App\MarketPlaceSellerServicePackage', 'id', 'mp_seller_service_package_id')->with('skill_parent');
    }
}

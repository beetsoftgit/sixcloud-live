<?php

//###############################################################
//File Name : TeacherSubjects.php
//Author : Jainam Shah
//Date : 22nd March, 2019
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherSubjects extends Model
{
    protected $table  = 'teacher_subjects';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'teacher_id', 'subject_id',
    ];

    public function subjectName()
    {
    	return $this->belongsTo('App\SphereSubjects', 'subject_id');
    }
}
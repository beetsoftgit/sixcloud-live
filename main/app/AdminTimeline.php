<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminTimeline extends Model
{
    protected $table = 'mp_admin_timeline';

    public function user_detail()
    {
        return $this->hasOne('App\User', 'id','user_id');
    }
    public function project_detail_info()
    {
        return $this->hasOne('App\MarketPlaceProjectsDetailInfo', 'mp_project_id','mp_project_id');
    }
}

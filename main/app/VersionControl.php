<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VersionControl extends Model
{
    protected $table = 'version_control';
}

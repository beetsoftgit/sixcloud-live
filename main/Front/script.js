// script.js
// create the module and name it sixcloudApp
// also include ngRoute for all our routing needs
var sixcloudApp = angular.module('sixcloudApp', ['ngRoute', 'ui.bootstrap']);
// configure our routes
sixcloudApp.config(function($routeProvider) {
    $routeProvider
    // route for the home page
        .when('/', {
            templateUrl: 'pages/elt-home.html',
            controller: 'mainController'
        })
        .when('/after-signup', {
            templateUrl: 'pages/after-signup.html',
            controller: 'after-signupController'
        })
        .when('/user-dashboard', {
            templateUrl: 'pages/ELT - user-dashboard.html',
            controller: 'user-dashboardController'
        })
        .when('/video-viewing', {
            templateUrl: 'pages/ELT - video-viewing.html',
            controller: 'video-viewingController'
        })
        .when('/my-cases', {
            templateUrl: 'pages/PR - my-cases.html',
            controller: 'my-casesController'
        })
        .when('/PR-home', {
            templateUrl: 'pages/PR - home.html',
            controller: 'pr-homeController'
        })     
        .when('/marketplace-user-step2', {
            templateUrl: 'pages/marketplace-user-step2.html',
            controller: 'marketplace-user-step2Controller'
        })
        .when('/mp-jobinfo-apply', {
            templateUrl: 'pages/mp-jobinfo-apply.html',
            controller:'mp-jobinfo-applyController'
        })
        .when('/mp-job-list', {
            templateUrl: 'pages/mp-job-list.html',
            controller: 'mp-job-listController'
        })
         .when('/mp-new-job', {
            templateUrl: 'pages/mp-new-job.html',
            controller: 'mp-new-jobController'
        })
        .when('/mp-cv-portfolio', {
            templateUrl: 'pages/mp-cv-portfolio.html',
            controller: 'mp-cv-portfolio-jobController'
        })
           .when('/mp-project-details', {
            templateUrl: 'pages/mp-project-details.html',
            controller: 'mp-project-details-Controller'
        })
           .when('/about-us', {
            templateUrl: 'pages/about-us.html',
            controller: 'about-us-Controller'
        })
        .when('/my-account', {
            templateUrl: 'pages/my-account.html',
            controller: 'my-accound-Controller'
        })
          .when('/mp-job-client-info', {
            templateUrl: 'pages/mp-job-client-info.html',
            controller: 'mp-job-client-infoController'
        })
        .when('/edit-profile', {
            templateUrl: 'pages/edit-profile.html',
            controller: 'edit-profileController'
        })
        .when('/marketplace-post-new-job', {
            templateUrl: 'pages/marketplace-post-new-job.html',
            controller: 'marketplace-post-new-jobController'
        }).when('/mp-open-jobinfo', {
            templateUrl: 'pages/mp-open-jobinfo.html',
            controller: 'mp-open-jobinfoController'
        });
    
});
// create the controller and inject Angular's $scope
sixcloudApp.controller('mainController', function($scope) {
    $('.content-list table tr').on('click', function() {
        var url = window.location.href;
        window.location = url + "video-viewing";
    });
});
sixcloudApp.controller('after-signupController', function($scope) {
    $(document).ready(function() {

         $('.select-plan.box-list .box-btn a.btn').click(function(){     
                $(this).parents('.box').siblings('.box').removeClass('select-the-plan');
                 $(this).parents('.box').addClass('select-the-plan');
            });

       $(".input-label input").focus(function()
             {
                $(this).parent(".input-label").addClass('typing');
                
             });
         $('.input-label input').blur(function(){
                if($(this).val() !='' ) {
                  $(this).parent(".input-label").addclass('typing');
                }else{
                $(this).parent(".input-label").removeClass('typing');
                }   
            });
    });
    $(".single-datepicker").daterangepicker({
        singleDatePicker: true,
         showDropdowns: true
    });   
});
sixcloudApp.controller('user-dashboardController', function($scope) {
    $('.content-list table tr').on('click', function() {
        var url = window.location.href;
        window.location = url + "#/video-viewing";
    });
});
sixcloudApp.controller('video-viewingController', function($scope) {
     // Configure/customize these variables.
    var showChar = 300;  // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "View more";
    var lesstext = "View less";    

    $('.more').each(function() {
        var content = $(this).html(); 
        if(content.length > showChar) { 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar); 
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<div class="continue-reading text-center" ><a href="" class="btn morelink"> <i class="sci-View-More"> </i> &nbsp;' + moretext + '</a></div></span>';
             $(this).html(html);
        } 
    });
    $('[data-toggle="tooltip"]').tooltip();
    $('[rel=popover]').popover({ 
      html : true, 
      content: function() {
        return $('#popover_content_wrapper').html();
      }
    });
 
    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
});
sixcloudApp.controller('marketplace-user-step2Controller', function($scope) {
    $(".marketplace ul li").click(function() {
        $(this).toggleClass('bg-green');
    });
});
sixcloudApp.controller('pr-homeController', function($scope) {
    new WOW().init();
});
sixcloudApp.controller('my-casesController', function($scope) {

     $('.new-cases-details .radio-button input[type="radio"]').click(function(){
        if ($(this).is(':checked'))
        {
          $(this).parents('tr').addClass('details');
           $(this).parents('tr').siblings('tr').removeClass('details');
        }
      });
 
    $(".icon-dd i").click(function(){

        $('.my-cases ul li .media').removeClass('remove'); 
        $(this).parents('.media').addClass('remove');
        if($(this).parents('.media').siblings(".collapse").hasClass('in'))
        {
            $(this).parents('.media').removeClass('remove');
        }
    });
     $('[data-toggle="tooltip"]').tooltip();
});
sixcloudApp.controller('mp-job-listController', function($scope) {
   
});
sixcloudApp.controller('mp-jobinfo-applyController', function($scope) {
     $('.fileinput').fileinput()
});
sixcloudApp.controller('mp-open-jobinfoController', function($scope) {
     $('.fileinput').fileinput()
});
sixcloudApp.controller('marketplace-post-new-jobController', function($scope) {
    $(".marketplace ul li").click(function() {
        $(this).toggleClass('bg-green');
    });
    $('input[name="singledate-picker"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true
    });
     $('.fileinput').fileinput()
});
sixcloudApp.controller('mp-cv-portfolio-jobController', function($scope){

});
sixcloudApp.controller('mp-project-details-Controller', function($scope){
     $(window).load(function(){
          $('.flexslider').flexslider({
            animation: "slide",
            controlNav: "thumbnails",
            start: function(slider){
              $('body').removeClass('loading');
            }
          });
        });
});
sixcloudApp.controller('about-us-Controller', function($scope){

});
sixcloudApp.controller('my-accound-Controller', function($scope){

});
sixcloudApp.controller('edit-profileController', function($scope){
$('.fileinput').fileinput();
 $(".single-datepicker,input[name='singledate-picker']").daterangepicker({
        singleDatePicker: true,
         showDropdowns: true,          
   });  

 $('.select2').select2({
      placeholder: {text: "Add Skills"},
  });
 

});
sixcloudApp.controller('mp-job-client-infoController', function($scope){
     $(".icon-dd i").click(function(){

        $('.my-cases li .empolyee-dec').removeClass('remove'); 
        $(this).parents('.empolyee-dec').addClass('remove');
         if($(this).parents('.panel').children('.collapse').hasClass('in'))
         {
             $(this).parents('.empolyee-dec').removeClass('remove');
         }
    });    
     $('.fileinput').fileinput();
});





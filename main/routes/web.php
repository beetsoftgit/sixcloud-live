<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
/*Event::listen('Illuminate\Database\Events\QueryExecuted', function ($query) {
echo("<pre>");print_r($query->sql);echo("</pre>");
echo("<pre>");print_r($query->bindings);echo("</pre>");
echo("<pre>");print_r($query->time);echo("</pre>");
}); */

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
------------------------------------------------------------- FRONT ROUTES -------------------------------------------------------------
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ************************************ 
        Display Routes front : start 
************************************ */
    // Route::get('/', function () {
    //     return view('front/layouts.mp');
    // });

    // Route::get('/home', function () {
    //     return view('front/layouts/mp');
    // });

    /* ===================================== 
        PR : start 
    ===================================== */
        Route::get('/proof-reading', function () {
            return view('front/layouts/mp');
        });

        Route::get('/my-cases', function () {
            return view('front/layouts/mp');
        })->middleware('ManageAuth');

        /*Route::get('/signin', function () {
            return view('front.login');
        });*/
    /* ===================================== 
        PR : end 
    ===================================== */

    /* ===================================== 
        VLT : start 
    ===================================== */
        // Route::get('/distributor', function () {
        //     return view('front/layouts/mp');
        // });
        /*Route::get('/elt-video/{slug}', function () {
            return view('front/layouts/master');
        });
        Route::get('/Download/{file}/{original_name}', function () {
            return view('front/layouts/master');
        });

        Route::get('/subscription-plan', function () {
            return view('front/layouts/master');
        });*/
    /* ===================================== 
        VLT : end 
    ===================================== */
    /* ===================================== 
        Market place : start 
    ===================================== */

        /*Marketplace Suggestion Algo*/
        //Route::any('/suggestion', 'SuggestionalgoController@Suggestionfunction');
        /*end*/

        Route::get('/', function () {
            $meta = 'SixClouds specialises in delivering quality educational programmes through a variety of bespoke multi-media platforms.';
            $title = 'SixClouds | The Future Makers';
            if(Config('constants.messages.CURRENT_DOMAIN')=='cn'){
                $alternate     = "https://www.sixclouds.net";
                $canonical     = "https://www.sixclouds.cn/";
                $alternatelang = "en-US";
            } else {
                $alternate = "https://www.sixclouds.cn";
                $canonical = "https://www.sixclouds.net/";
                $alternatelang = "zh-Hans";
            }
            return view('front/corporate',['title' => $title,'meta' => $meta,'alternate'=>$alternate,'canonical'=>$canonical,'alternatelang'=>$alternatelang]);
            // return view('front/corporate');
        });
        Route::get('/home', function () {
            $meta = 'SixClouds specialises in delivering quality educational programmes through a variety of bespoke multi-media platforms.';
            $title = 'SixClouds | The Future Makers';
            if(Config('constants.messages.CURRENT_DOMAIN')=='cn'){
                $alternate = "https://www.sixclouds.net";
                $canonical = "https://www.sixclouds.cn/";
                $alternatelang = "en-US";
            } else {
                $alternate = "https://www.sixclouds.cn";
                $canonical = "https://www.sixclouds.net/";
                $alternatelang = "zh-Hans";
            }
            return view('front/corporate',['title' => $title,'meta' => $meta,'alternate'=>$alternate,'canonical'=>$canonical,'alternatelang'=>$alternatelang]);
            // return view('front/corporate');
        });
        
        Route::get('/sixteen', function () {
            $meta = 'Where young creative talents resolve real world requirements.';
            $title = 'SixClouds SIXTEEN | Where Great Creations Abound!';
            if(Config('constants.messages.CURRENT_DOMAIN')=='cn'){
                $alternate = "https://www.sixclouds.net";
                $canonical = "https://www.sixclouds.cn/sixteen";
                $alternatelang = "en-US";
            } else {
                $alternate = "https://www.sixclouds.cn";
                $canonical = "https://www.sixclouds.net/sixteen";
                $alternatelang = "zh-Hans";
            }
            return view('front/layouts/mp',['title' => $title,'meta' => $meta,'alternate'=>$alternate,'canonical'=>$canonical,'alternatelang'=>$alternatelang]);
            // return view('front/layouts/mp');
        });

        Route::get('/about-us', function () {
            $meta = 'SixClouds was founded with the vision to make knowledge and learning accessible and affordable to everyone.';
            $title = 'About Us | SixClouds';
            $robotsContent = "index,follow";
            if(Config('constants.messages.CURRENT_DOMAIN')=='cn'){
                $alternate = "https://www.sixclouds.net";
                $canonical = "https://www.sixclouds.cn/about-us";
                $alternatelang = "en-US";
            } else {
                $alternate = "https://www.sixclouds.cn";
                $canonical = "https://www.sixclouds.net/about-us";
                $alternatelang = "zh-Hans";
            }
            return view('front/about-us',['title' => $title,'meta' => $meta,'alternate'=>$alternate,'canonical'=>$canonical,'alternatelang'=>$alternatelang,'robotsContent'=>$robotsContent]);
            // return view('front/about-us');
        });

        Route::get('/about-us/the-company', function () {
            $meta = 'SixClouds was founded with the vision to make knowledge and learning accessible and affordable to everyone.';
            $title = 'About Us | SixClouds';
            $robotsContent = "noindex,nofollow";
            if(Config('constants.messages.CURRENT_DOMAIN')=='cn'){
                $alternate = "https://www.sixclouds.net";
                $canonical = "https://www.sixclouds.cn/about-us";
                $alternatelang = "en-US";
            } else {
                $alternate = "https://www.sixclouds.cn";
                $canonical = "https://www.sixclouds.net/about-us";
                $alternatelang = "zh-Hans";
            }
            return view('front/about-us',['title' => $title,'meta' => $meta,'alternate'=>$alternate,'canonical'=>$canonical,'alternatelang'=>$alternatelang,'robotsContent'=>$robotsContent]);
            // return view('front/about-us');
        });
        Route::get('/about-us/core-values', function () {
            $meta = 'SixClouds was founded with the vision to make knowledge and learning accessible and affordable to everyone.';
            $title = 'About Us | SixClouds';
            $robotsContent = "noindex,nofollow";
            if(Config('constants.messages.CURRENT_DOMAIN')=='cn'){
                $alternate = "https://www.sixclouds.net";
                $canonical = "https://www.sixclouds.cn/about-us";
                $alternatelang = "en-US";
            } else {
                $alternate = "https://www.sixclouds.cn";
                $canonical = "https://www.sixclouds.net/about-us";
                $alternatelang = "zh-Hans";
            }
            return view('front/about-us',['title' => $title,'meta' => $meta,'alternate'=>$alternate,'canonical'=>$canonical,'alternatelang'=>$alternatelang,'robotsContent'=>$robotsContent]);
            // return view('front/about-us');
        });
        Route::get('/about-us/our-team', function () {
            $meta = 'SixClouds was founded with the vision to make knowledge and learning accessible and affordable to everyone.';
            $title = 'About Us | SixClouds';
            $robotsContent = "noindex,nofollow";
            if(Config('constants.messages.CURRENT_DOMAIN')=='cn'){
                $alternate = "https://www.sixclouds.net";
                $canonical = "https://www.sixclouds.cn/about-us";
                $alternatelang = "en-US";
            } else {
                $alternate = "https://www.sixclouds.cn";
                $canonical = "https://www.sixclouds.net/about-us";
                $alternatelang = "zh-Hans";
            }
            return view('front/about-us',['title' => $title,'meta' => $meta,'alternate'=>$alternate,'canonical'=>$canonical,'alternatelang'=>$alternatelang,'robotsContent'=>$robotsContent]);
            // return view('front/about-us');
        });

        Route::get('/become-seller', function () {
            $meta = 'Start building your portfolio with real creative jobs today!';
            $title = 'Showcase Your Talents | SixClouds SIXTEEN';
            if(Config('constants.messages.CURRENT_DOMAIN')=='cn'){
                $alternate = "https://www.sixclouds.net";
                $canonical = "https://www.sixclouds.cn/become-seller";
                $alternatelang = "en-US";
            } else {
                $alternate = "https://www.sixclouds.cn";
                $canonical = "https://www.sixclouds.net/become-seller";
                $alternatelang = "zh-Hans";
            }
            return view('front/become-seller',['title' => $title,'meta' => $meta,'alternate'=>$alternate,'canonical'=>$canonical,'alternatelang'=>$alternatelang]);
            // return view('front/become-seller');
        });

        Route::get('/become-buyer', function () {
            $meta = 'Hire talents across different creative disciplines from SIXTEEN.';
            $title = 'Hire The Best Talents | SixClouds SIXTEEN';
            if(Config('constants.messages.CURRENT_DOMAIN')=='cn'){
                $alternate = "https://www.sixclouds.net";
                $canonical = "https://www.sixclouds.cn/become-buyer";
                $alternatelang = "en-US";
            } else {
                $alternate = "https://www.sixclouds.cn";
                $canonical = "https://www.sixclouds.net/become-buyer";
                $alternatelang = "zh-Hans";
            }
            return view('front/become-buyer',['title' => $title,'meta' => $meta,'alternate'=>$alternate,'canonical'=>$canonical,'alternatelang'=>$alternatelang]);
            // return view('front/become-buyer');
        });

        Route::get('/privacy-policy', function () {
            $meta = 'This Privacy Policy is meant to help you understand on how we may collect and process information about you.';
            $title = 'Privacy Policy | SixClouds';
            if(Config('constants.messages.CURRENT_DOMAIN')=='cn'){
                $alternate = "https://www.sixclouds.net";
                $canonical = "https://www.sixclouds.cn/privacy-policy";
                $alternatelang = "en-US";
            } else {
                $alternate = "https://www.sixclouds.cn";
                $canonical = "https://www.sixclouds.net/privacy-policy";
                $alternatelang = "zh-Hans";
            }
            return view('front/privacy-policy',['title' => $title,'meta' => $meta,'alternate'=>$alternate,'canonical'=>$canonical,'alternatelang'=>$alternatelang]);
            // return view('front/privacy-policy');
        });
        Route::get('/sixteen-terms-of-service', function () {
            $meta = 'The following terms and conditions is an agreement between you and SixClouds.';
            $title = 'Terms of Service | SixClouds SIXTEEN';
            if(Config('constants.messages.CURRENT_DOMAIN')=='cn'){
                $alternate = "https://www.sixclouds.net";
                $canonical = "https://www.sixclouds.cn/sixteen-terms-of-service";
                $alternatelang = "en-US";
            } else {
                $alternate = "https://www.sixclouds.cn";
                $canonical = "https://www.sixclouds.net/sixteen-terms-of-service";
                $alternatelang = "zh-Hans";
            }
            return view('front/terms-of-service',['title' => $title,'meta' => $meta,'alternate'=>$alternate,'canonical'=>$canonical,'alternatelang'=>$alternatelang]);
            // return view('front/terms-of-service');
        });
        Route::get('/terms-of-service', function () {
            $meta = 'The following terms and conditions is an agreement between you and SixClouds.';
            $title = 'Terms of Service | SixClouds SIXTEEN';
            if(Config('constants.messages.CURRENT_DOMAIN')=='cn'){
                $alternate = "https://www.sixclouds.net";
                $canonical = "https://www.sixclouds.cn/terms-of-service";
                $alternatelang = "en-US";
            } else {
                $alternate = "https://www.sixclouds.cn";
                $canonical = "https://www.sixclouds.net/terms-of-service";
                $alternatelang = "zh-Hans";
            }
            return view('front/terms-of-service',['title' => $title,'meta' => $meta,'alternate'=>$alternate,'canonical'=>$canonical,'alternatelang'=>$alternatelang]);
            // return view('front/terms-of-service');
        });

        Route::get('/sixteen-login', function () {
            if (Auth::check()) {
                if (Auth::user()->marketplace_current_role == 2) {
                    return redirect('/buyer-dashboard');
                } else {
                    return redirect('/seller-dashboard');
                }

            } else {
                $meta = 'Log in to SixClouds SIXTEEN and get immersed in a world of creativity.';
                $title = 'Sign In | SixClouds SIXTEEN';
                    if(Config('constants.messages.CURRENT_DOMAIN')=='cn'){
                    $alternate = "https://www.sixclouds.net";
                    $canonical = "https://www.sixclouds.cn/sixteen-login";
                    $alternatelang = "en-US";
                } else {
                    $alternate = "https://www.sixclouds.cn";
                    $canonical = "https://www.sixclouds.net/sixteen-login";
                    $alternatelang = "zh-Hans";
                }
                return view('front/layouts/mp',['title' => $title,'meta' => $meta,'alternate'=>$alternate,'canonical'=>$canonical,'alternatelang'=>$alternatelang]);
                // return view('front/layouts/mp',['title' => $title,'meta' => $meta]);
                // return view('front/layouts/mp');
            }
        });

        Route::get('/seller-signup/{email?}', function () {
            $meta = 'Start building your portfolio with real creative jobs today!';
            $title = 'Join as a Seller | SixClouds SIXTEEN';
            if(Config('constants.messages.CURRENT_DOMAIN')=='cn'){
                $alternate = "https://www.sixclouds.net";
                $canonical = "https://www.sixclouds.cn/buyer-sign-up";
                $alternatelang = "en-US";
            } else {
                $alternate = "https://www.sixclouds.cn";
                $canonical = "https://www.sixclouds.net/buyer-sign-up";
                $alternatelang = "zh-Hans";
            }
            return view('front/layouts/mp',['title' => $title,'meta' => $meta,'alternate'=>$alternate,'canonical'=>$canonical,'alternatelang'=>$alternatelang]);
            // return view('front/layouts/mp');
        });

        Route::get('/buyer-sign-up', function () {
            $meta = 'Hire talents across different creative disciplines from SIXTEEN.';
            $title = 'Join as a Buyer | SixClouds SIXTEEN';
            if(Config('constants.messages.CURRENT_DOMAIN')=='cn'){
                $alternate = "https://www.sixclouds.net";
                $canonical = "https://www.sixclouds.cn/buyer-sign-up";
                $alternatelang = "en-US";
            } else {
                $alternate = "https://www.sixclouds.cn";
                $canonical = "https://www.sixclouds.net/buyer-sign-up";
                $alternatelang = "zh-Hans";
            }
            return view('front/layouts/mp',['title' => $title,'meta' => $meta,'alternate'=>$alternate,'canonical'=>$canonical,'alternatelang'=>$alternatelang]);
            // return view('front/layouts/mp',['title' => $title,'meta' => $meta]);
            // return view('front/layouts/mp');
        });

        Route::get('/express-interest', function () {
            $meta = 'Our platform will be ready soon!';
            $title = 'Express Your Interest | SixClouds SIXTEEN';
            if(Config('constants.messages.CURRENT_DOMAIN')=='cn'){
                $alternate = "https://www.sixclouds.net";
                $canonical = "https://www.sixclouds.cn/express-interest";
                $alternatelang = "en-US";
            } else {
                $alternate = "https://www.sixclouds.cn";
                $canonical = "https://www.sixclouds.net/express-interest";
                $alternatelang = "zh-Hans";
            }
            return view('front/layouts/mp',['title' => $title,'meta' => $meta,'alternate'=>$alternate,'canonical'=>$canonical,'alternatelang'=>$alternatelang]);
            // return view('front/layouts/mp',['title' => $title,'meta' => $meta]);
            // return view('front/layouts/mp');
        });

        /* routes that does not need 'BuyerAuth' or 'SellerAuth' middleware : start */
        Route::get('/buyer-dashboard', function () {
            if(Auth::user()->marketplace_current_role!=2)
                return redirect('/seller-dashboard');
            else
                return view('front/layouts/mp');
        })->middleware('ManageAuth');

        Route::get('/seller-dashboard', function () {
            if(Auth::user()->marketplace_current_role!=1)
                return redirect('/buyer-dashboard');
            else
                return view('front/layouts/mp');
        })->middleware('ManageAuth');

        Route::get('/seller-profile/{slug}/{mp_project_id?}', function () {
            return view('front/layouts/mp');});

        Route::get('/gallery', function () {
            return view('front/layouts/mp');})->middleware('ManageAuth');

        Route::get('/seller-portfolio-item-details/{slug}', function () {
            return view('front/layouts/mp');})->middleware('ManageAuth');
        
        Route::get('/inspiration-bank-item-details/{slug}', function () {
                return view('front/layouts/mp');})->middleware('ManageAuth');    
        Route::get('/{urlfront}', function ($url) {
            return view('front/layouts/mp');
        })->where(['urlfront' => 'notifications',
        ])->middleware('ManageAuth');
        /* routes that does not need 'BuyerAuth' or 'SellerAuth' middleware : end */

        Route::get('/create-new-project/{slug?}/{whatAction?}/{cartId?}/{selectedDesigner?}', function () {
            return view('front/layouts/mp');})->middleware('ManageAuth','BuyerAuth');

        Route::get('/suggested-sellers/{slug}/{assigned_designer_id?}', function () {
            return view('front/layouts/mp');})->middleware('ManageAuth','BuyerAuth');

        Route::get('/browse-sellers/{slug?}/{assigned_designer_id?}', function () {
            return view('front/layouts/mp');})->middleware('ManageAuth','BuyerAuth');

        Route::get('/buyer-past-projects', function () {
            return view('front/layouts/mp');})->middleware('ManageAuth','BuyerAuth');

        Route::get('/buyer-project-info/{slug}', function () {
            return view('front/layouts/mp');})->middleware('ManageAuth','BuyerAuth');

        Route::get('/make-payment/{slug}', function () { ##for paypal##
            return view('front/layouts/mp');})->middleware('ManageAuth','BuyerAuth');

        Route::get('/buyer-past-project-info/{slug}', function () {
            return view('front/layouts/mp');})->middleware('ManageAuth','BuyerAuth');

        Route::get('/buyer-my-account', function () {
            return view('front/layouts/mp');})->middleware('ManageAuth','BuyerAuth');

        Route::get('/seller-my-account', function () {
            return view('front/layouts/mp');})->middleware('ManageAuth','SellerAuth');

        Route::get('/seller-my-uploads', function () {
            return view('front/layouts/mp');})->middleware('ManageAuth','SellerAuth');

        Route::get('/seller-portfolio', function () {
            return view('front/layouts/mp');})->middleware('ManageAuth','SellerAuth');

        Route::get('/seller-portfolio-item-edit/{slug}', function () {
            return view('front/layouts/mp');})->middleware('ManageAuth','SellerAuth');

        Route::get('/inspiration-bank-item-edit/{slug}', function () {
            return view('front/layouts/mp');})->middleware('ManageAuth','SellerAuth');    

        Route::get('/seller-project-info/{slug}', function () {
            return view('front/layouts/mp');})->middleware('ManageAuth','SellerAuth');

        Route::get('/seller-project-info-invitation/{slug}', function () {
            return view('front/layouts/mp');})->middleware('ManageAuth','SellerAuth');

        Route::get('/seller-past-projects', function () {
            return view('front/layouts/mp');})->middleware('ManageAuth','SellerAuth');

        Route::get('/seller-past-project-info/{slug}', function () {
            return view('front/layouts/mp');})->middleware('ManageAuth','SellerAuth');

        Route::get('/seller-inspiration-bank', function () {
            return view('front/layouts/mp');})->middleware('ManageAuth','SellerAuth');

        Route::get('/my-services', function () {
            return view('front/layouts/mp');})->middleware('ManageAuth','SellerAuth');
  
    /* ===================================== 
        Market place : end 
    ===================================== */


    /* ===================================== 
        IGNITE : start 
    ===================================== */
    Route::get('/ignite-buzz', function () {
        return view('front/layouts/mp');
    });
    Route::get('/ignite-maze', function () {
        return view('front/layouts/mp');
    });
    Route::get('/ignite-smile-international', function () {
        return view('front/layouts/mp');
    });
    Route::get('/ignite-smile-singapore', function () {
        return view('front/layouts/mp');
    });
    Route::get('/ignite-smile-philippines', function () {
        return view('front/layouts/mp');
    });
    Route::get('/ignite-mathematics', function () {
        return view('front/layouts/mp');
    });
    Route::get('/ignite', function () {
        if(Auth::guard('distributors')->check())
            return redirect('/subscribers');
        elseif(Auth::guard('distributor_team_members')->check())
            return redirect('/my-referral');
        else{
            $meta = 'Learning is easy! Our fun and exciting animated videos, interactive quizzes and worksheets help children to master the basics of knowledge effortlessly.';
            $title = 'SixClouds IGNITE';
            if(Config('constants.messages.CURRENT_DOMAIN')=='cn'){
                $alternate = "https://www.sixclouds.net";
                $canonical = "https://www.sixclouds.cn/ignite";
                $alternatelang = "en-US";
            } else {
                $alternate = "https://www.sixclouds.cn";
                $canonical = "https://www.sixclouds.net/ignite";
                $alternatelang = "zh-Hans";
            }
            return view('front/layouts/mp',['title' => $title,'meta' => $meta,'alternate'=>$alternate,'canonical'=>$canonical,'alternatelang'=>$alternatelang]);
            // return view('front/layouts/mp',['title' => $title,'meta' => $meta]);
        }
        // return view('front/layouts/mp');
    });
    
    Route::get('/ignite-signup/{slug?}/{mode?}', function () {
        return view('front/layouts/mp');});

    Route::get('/user-verify-otp/{phonecode?}/{contact?}/{email_address?}', function () {
        return view('front/layouts/mp');});

    /*Route::get('/ignite-login', function () {
        if(Auth::check()){
            if(Auth::user()->video_preference=='')
                return redirect('/select-categories');
            else
                return redirect('/ignite-categories');*/

    Route::get('/ignite-login/{encodeStr?}', function () {
        $uriPath = $_SERVER['REQUEST_URI']; 
        $uriParts = explode('/', $uriPath);
        $encodedStr = end($uriParts);
        $decodeStr = base64_decode($encodedStr);
        if(Auth::check()) {
            if($decodeStr == 'Direct Subscription') {
                return redirect('/my-subscription');
            } else {
                return redirect('/ignite-categories');
            }
        }
        elseif(Auth::guard('distributors')->check())
            return redirect('/subscribers');
        elseif(Auth::guard('distributor_team_members')->check())
            return redirect('/my-referral');
        else{
            $meta = 'Log in to SixClouds IGNITE and begin your learning journey with us.';
            $title = 'Sign In | SixClouds IGNITE';
            if(Config('constants.messages.CURRENT_DOMAIN')=='cn'){
                $alternate = "https://www.sixclouds.net";
                $canonical = "https://www.sixclouds.cn/ignite-login/".$encodedStr;
                $alternatelang = "en-US";
            } else {
                $alternate = "https://www.sixclouds.cn";
                $canonical = "https://www.sixclouds.net/ignite-login/".$encodedStr;
                $alternatelang = "zh-Hans";
            }
            return view('front/layouts/mp',['title' => $title,'meta' => $meta,'alternate'=>$alternate,'canonical'=>$canonical,'alternatelang'=>$alternatelang]);
            // return view('front/layouts/mp',['title' => $title,'meta' => $meta]);
        }
    });
    
    Route::get('/distributor', function () {
        if(Auth::check())
            return redirect('/select-categories');
        elseif(Auth::guard('distributors')->check())
            return redirect('/subscribers');
        elseif(Auth::guard('distributor_team_members')->check())
            return redirect('/my-referral');
        else
            return view('front/layouts/mp');
    });
    
    Route::get('/distributor/team-login', function () {
        if(Auth::check())
            return redirect('/select-categories');
        elseif(Auth::guard('distributors')->check())
            return redirect('/subscribers');
        elseif(Auth::guard('distributor_team_members')->check())
            return redirect('/my-referral');
        else
            return view('front/layouts/mp');
    });
    
    Route::get('/ignite-terms-of-service', function () {
        $meta = 'The following terms and conditions is an agreement between you and SixClouds.';
        $title = 'Terms of Service | SixClouds IGNITE';
        if(Config('constants.messages.CURRENT_DOMAIN')=='cn'){
            $alternate = "https://www.sixclouds.net";
            $canonical = "https://www.sixclouds.cn/ignite-terms-of-service";
            $alternatelang = "en-US";
        } else {
            $alternate = "https://www.sixclouds.cn";
            $canonical = "https://www.sixclouds.net/ignite-terms-of-service";
            $alternatelang = "zh-Hans";
        }
        return view('front/ignite-terms-of-service',['title' => $title,'meta' => $meta,'alternate'=>$alternate,'canonical'=>$canonical,'alternatelang'=>$alternatelang]);
        // return view('front/ignite-terms-of-service');
    });

    Route::get('/faq', function () {
        $meta = 'The following terms and conditions is an agreement between you and SixClouds.';
        $title = 'FAQ | SixClouds IGNITE';
        if(Config('constants.messages.CURRENT_DOMAIN')=='cn'){
            $alternate = "https://www.sixclouds.net";
            $canonical = "https://www.sixclouds.cn/faq";
            $alternatelang = "en-US";
        } else {
            $alternate = "https://www.sixclouds.cn";
            $canonical = "https://www.sixclouds.net/faq";
            $alternatelang = "zh-Hans";
        }
        return view('front/faq',['title' => $title,'meta' => $meta,'alternate'=>$alternate,'canonical'=>$canonical,'alternatelang'=>$alternatelang]);
        // return view('front/ignite-terms-of-service');
    });

    Route::group(['middleware' => ['DistributorAuth']], function () {
        Route::get('/subscribers', function () {
            return view('front/layouts/mp');});
        
        Route::get('/my-team', function () {
            return view('front/layouts/mp');});
        
        Route::get('/distributors-commissions', function () {
            return view('front/layouts/mp');});
        
        Route::get('/verify-distributor', function () {
            return view('front/layouts/mp');});
        
        Route::get('/distributor-my-account', function () {
            return view('front/layouts/mp');});

        Route::get('/ignite-subscriber-details/{slug}', function () {
            return view('front/layouts/mp');});

        Route::get('/team-detail/{slug}', function () {
            return view('front/layouts/mp');});

        Route::get('/my-codes', function () {
            return view('front/layouts/mp');});
    });

    Route::group(['middleware' => ['DistributorTeamAuth']], function () {
        Route::get('/my-referral', function () {
            return view('front/layouts/mp');});
        
        Route::get('/team-member-commissions', function () {
            return view('front/layouts/mp');});

        Route::get('/my-codes', function () {
            return view('front/layouts/mp');});
    });
        Route::get('/team-my-account', function () {
            if(Auth::guard('distributor_team_members')->check())
                if(Auth::guard('distributor_team_members')->user()->status==5||Auth::guard('distributor_team_members')->user()->status==1)
                    return view('front/layouts/mp');
                else
                    return redirect('/my-referral');    
            else
                return redirect('/distributor/team-login');
        });
        Route::get('/verify-phone-number', function () {
            if(Auth::guard('distributor_team_members')->check())
                if(Auth::guard('distributor_team_members')->user()->status==4)
                    return view('front/layouts/mp');
                else
                    return redirect('/my-referral');    
            else
                return redirect('/distributor/team-login');
        });

    Route::get('/my-account', function () {
        if(Auth::check())
            return view('front/layouts/mp');
        else
            return redirect('/ignite-login');
    });

    Route::group(['middleware' => ['IgniteAuth']], function () {

        Route::get('/make-payment-ignite/{slug}', function () { 
            ##for paypal##
            return view('front/layouts/mp');});

        // Route::get('/my-account', function () {
        //     return view('front/layouts/mp');});
        
        Route::get('/select-categories', function () {
            return view('front/layouts/mp');});

        Route::get('/ignite-categories/{subject_id?}', function () {
            return view('front/layouts/mp');});
        
        Route::get('/my-subscription/{slug?}', function () {
            return view('front/layouts/mp');});
        
        Route::get('/content-detail/{slug}', function () {
            return view('front/layouts/mp');});
        
        Route::get('/quiz/{slug}', function () {
            return view('front/layouts/mp');});
        
        Route::get('/score-card/{slug}', function () {
            return view('front/layouts/mp');});
        
        Route::get('/my-performance', function () {
            return view('front/layouts/mp');});

        Route::get('/ignite-payment/{token}', function () {
            return view('front/layouts/mp');});

        Route::get('/switch-plan/{subject_id}', function () {
            return view('front/layouts/mp');});

        Route::get('/subscription-plan/{subject_id}', function () {
            return view('front/layouts/mp');});

        Route::get('/subscription-payment/{subject_id}', function () {
            return view('front/layouts/mp');});
    });

    /* ===================================== 
        IGNITE : end 
    ===================================== */

    /* ===================================== 
        Sphere: start 
    ===================================== */
    /*Route::get('/sphere', function () {
        return view('front/layouts/mp');});
    });*/
    Route::get('/{url}', function ($url) {
        return view('front/layouts/mp');})->where(['url' => 'sphere|sphere-signup|sphere-messaging-teacher|teachers|sphere-transactions|sphere-history']);


    // Route::group(['middleware' => ['IgniteAuth']], function () {

    Route::get('/teacher-profile/{slug}', function () {
        return view('front/layouts/mp');
    });

    // });
    /* ===================================== 
        Sphere: end
    ===================================== */

    /* ===================================== 
        Customer Support: start 
    ===================================== */
    Route::get('/customer-support', function () {
        $title = "Customer Support | SixClouds";
        $meta="Do you still have questions? Our support agents are ready with the answers.";
        if(Config('constants.messages.CURRENT_DOMAIN')=='cn'){
            $alternate = "https://www.sixclouds.net";
            $canonical = "https://www.sixclouds.cn/customer-support";
            $alternatelang = "en-US";
        } else {
            $alternate = "https://www.sixclouds.cn";
            $canonical = "https://www.sixclouds.net/customer-support";
            $alternatelang = "zh-Hans";
        }
        return view('front/customer-support',['title' => $title,'meta' => $meta,'alternate'=>$alternate,'canonical'=>$canonical,'alternatelang'=>$alternatelang]);
        // return view('front/customer-support');
    });
    /* ===================================== 
        Customer Support : end 
    ===================================== */
/* ************************************ 
        Display Routes front : end 
************************************ */
/* ************************************ 
        Method Routes front : start 
************************************ */
    /* UserController.php method routes : start */
        Route::get('/signup', 'UserController@Signup');
        Route::get('/checkuseremail', 'UserController@Checkuseremail');
        Route::get('/checkuseremailexists', 'UserController@checkuseremailexists');
        Route::post('/Dosignup', 'UserController@Dosignup');
        Route::post('/Updateuserdata', 'UserController@Updateuserdata');

        Route::post('/Addlanguage', 'UserController@Addlanguage');
        Route::post('/Checkoldpassword', 'UserController@Checkoldpassword');
        Route::post('/Updateuserpassword', 'UserController@Updateuserpassword');
        Route::post('/Addsellerunavailability', 'UserController@Addsellerunavailability');
        Route::post('/Deleteunavailablity', 'UserController@Deleteunavailablity');

        Route::post('/Updatesellerdetails', 'UserController@Updatesellerdetails');
        Route::post('/Deletelanguage', 'UserController@Deletelanguage');
        Route::post('/Editbankdetails', 'UserController@Editbankdetails');
        Route::post('/Switchrole', 'UserController@Switchrole');
        Route::post('Getschools', 'UserController@Getschools');

        Route::post('Getlanguages', 'UserController@Getlanguages');
        Route::get('/Getcities', 'UserController@Getcities');
        Route::get('/Getloginuserdata', 'UserController@Getloginuserdata');
        Route::get('/Getuserpaymenthistory', 'UserController@Getuserpaymenthistory');
        Route::get('/Getcountrycitypro', 'UserController@Getcountrycitypro');

        Route::get('/Getcountry', 'UserController@Getcountry');
        Route::post('/Getstatesforcountry', 'UserController@Getstatesforcountry');
        Route::post('/Getcitiesforstate', 'UserController@Getcitiesforstate');
        Route::post('/checkuseremail', 'UserController@Checkuseremail');
        Route::post('/Gettickettype', 'UserController@Gettickettype');
        Route::post('/GetProducttype', 'UserController@GetProducttype');
        Route::post('/Getticketcategoriesforcustomersupport', 'UserController@Getticketcategoriesforcustomersupport');

        Route::post('/Createnewticket', 'UserController@Createnewticket'); 
        Route::post('Senduserotp', 'UserController@Senduserotp');
        Route::post('Verifyuserotp', 'UserController@Verifyotp');       
    /* UserController.php method routes : end */

    /* PRController.php method routes : start */
        Route::post('/Getcases', 'PRController@Getcases');
        Route::post('/Addcase', 'PRController@Addcase');
        Route::post('/Rejectcase', 'PRController@Rejectcase');
        Route::post('/Fileupload', 'PRController@Fileupload');
        Route::post('/Deletefile', 'PRController@Deletefile');

        Route::any('/Downloadcase', 'PRController@Download');
        Route::any('/Unlinkdownloadedfile', 'PRController@Unlinkdownloadedfile');
    /* PRController.php method routes : end */

    /* UserLoginController.php method routes : start */
        Route::get('/Logout', 'UserLoginController@Logout');
        Route::post('/forgotpassword', 'UserLoginController@Forgotpassword');
        Route::get('/Resetpassword', 'UserLoginController@Resetpassword');
        Route::post('/Doresetpassword', 'UserLoginController@Doresetpassword');
        Route::post('/Dosignin', 'UserLoginController@Dosignin');
    /* UserLoginController.php method routes : end */

    /* ELTController.php method routes : start */
        Route::post('/Geteltsections', 'ELTController@Geteltsections');
        Route::post('/GetVideodata', 'ELTController@GetVideodata');
        Route::post('/Addnote', 'ELTController@Addnote');
        Route::post('/Deletenote', 'ELTController@Deletenote');
        Route::post('/testThumbnail', 'ELTController@testThumbnail');
    /* ELTController.php method routes : end */

    /* MarketPlaceController.php method routes : start */
        Route::get('Activeprojects', 'MarketPlaceController@Activeprojects');
        Route::post('Returnfromcreditpayment', 'MarketPlaceController@Returnfromcreditpayment');
        Route::get('get-payment-details', 'MarketPlaceController@Returnalipayurldetails');
        Route::post('paypal-payment-details', 'MarketPlaceController@Paypalpaymentipndata');

         //temp
        Route::post('Escrowpaymentdata', 'MarketPlaceController@Escrowpaymentdata'); //temp
        Route::post('Paymentinfodata', 'MarketPlaceController@Paymentinfodata');
        Route::get('notify-urldetails', 'MarketPlaceController@Notifyalipayurldetails');

        Route::post('/Createnewproject', 'MarketPlaceController@Createnewproject');
        Route::post('/Createnewdesigner', 'MarketPlaceController@Createnewdesigner');
        Route::post('/Getdesigners', 'MarketPlaceController@Getdesigners');
        Route::post('/Designersuggestions', 'MarketPlaceController@Designersuggestions');
        Route::post('/Asssigndesigner', 'MarketPlaceController@Asssigndesigner');

        Route::post('/Updatebudgettimeline', 'MarketPlaceController@Updatebudgettimeline');
        Route::post('/Dofilter', 'MarketPlaceController@Dofilter');
        Route::post('/Projectinfo', 'MarketPlaceController@Projectinfo');
        Route::post('/Sendmessage', 'MarketPlaceController@Sendmessage');
        Route::post('/Actions', 'MarketPlaceController@Actions');

        Route::post('/Portfolio', 'MarketPlaceController@Portfolio');
        Route::post('/Editportfolio', 'MarketPlaceController@Editportfolio');
        Route::post('/Editinspirationbank', 'MarketPlaceController@Editinspirationbank');
        Route::post('/Portfoliocategory', 'MarketPlaceController@Portfoliocategory');
        Route::post('/Acceptrejectextension', 'MarketPlaceController@Acceptrejectextension');
        Route::post('/Searchdesigner', 'MarketPlaceController@Searchdesigner');

        Route::post('Getprojectdetail', 'MarketPlaceController@Getprojectdetail');
        Route::post('Getschools', 'MarketPlaceController@Getschools');
        Route::post('Getskills', 'MarketPlaceController@Getskills');
        Route::post('Adddesignerreview', 'MarketPlaceController@Adddesignerreview');
        Route::post('Getdesignerreview', 'MarketPlaceController@Getdesignerreview');

        Route::post('Buyerprojects', 'MarketPlaceController@Buyerprojects');
        Route::post('Openprojects', 'MarketPlaceController@Openprojects');
        Route::post('Gallerydetail', 'MarketPlaceController@Gallerydetail');
        Route::post('Getgallerylisting', 'MarketPlaceController@Getgallerylisting');
        Route::post('Getgallerypurchases', 'MarketPlaceController@Getgallerypurchases');

        Route::post('Designerprojectsinvitation', 'MarketPlaceController@Designerprojectsinvitation');
        Route::post('Designergalleryuploads', 'MarketPlaceController@Designergalleryuploads');
        Route::post('Designergallerynewupload', 'MarketPlaceController@Designergallerynewupload');
        Route::get('Designerinspirationbankuploads', 'MarketPlaceController@Designerinspirationbankuploads');
        Route::post('Designermyuploadscount', 'MarketPlaceController@Designermyuploadscount');

        Route::post('Designerinsbankupload', 'MarketPlaceController@Designerinsbankupload');
        Route::post('Designerprojectinfo', 'MarketPlaceController@Designerprojectinfo');
        Route::post('Designermarkprojectcomplete', 'MarketPlaceController@Designermarkprojectcomplete');
        Route::post('DesignerCancelProject', 'MarketPlaceController@DesignerCancelProject');
        Route::post('Designerinvitation', 'MarketPlaceController@Designerinvitation');

        Route::post('Designerinvitationresponse', 'MarketPlaceController@Designerinvitationresponse');
        Route::post('Designerpastprojects', 'MarketPlaceController@Designerpastprojects');
        Route::post('Designerpastprojectinfo', 'MarketPlaceController@Designerpastprojectinfo');
        Route::post('Uploadmessagefile', 'MarketPlaceController@Uploadmessagefile');
        Route::post('Unlinkmsgbrdremovedfile', 'MarketPlaceController@Unlinkmsgbrdremovedfile');

        Route::post('Designerinspirationbanklisting', 'MarketPlaceController@Designerinspirationbanklisting');
        Route::post('Designerinspirationbankcategorylistsing', 'MarketPlaceController@Designerinspirationbankcategorylistsing');
        Route::post('Addextensionforproject', 'MarketPlaceController@Addextensionforproject');
        Route::post('Designerprofileinfo', 'MarketPlaceController@Designerprofileinfo');
        Route::post('Designerworkhistoryandreviews', 'MarketPlaceController@Designerworkhistoryandreviews');
        Route::post('Getportfolio', 'MarketPlaceController@Getportfolio');
        Route::post('Portfolioinfo', 'MarketPlaceController@Portfolioinfo');
        Route::post('InspirationBankinfo', 'MarketPlaceController@InspirationBankinfo');
        Route::post('MakeUserFavourite', 'MarketPlaceController@MakeUserFavourite');
        Route::post('Makeprojectportfolio', 'MarketPlaceController@Makeprojectportfolio');
        Route::post('Portfolioimages', 'MarketPlaceController@Portfolioimages');
        Route::post('Getcompletedprojectsforportfolio', 'MarketPlaceController@Getcompletedprojectsforportfolio');

        Route::post('Deleteportfolio', 'MarketPlaceController@Deleteportfolio');
        Route::post('DeleteinspirationBank', 'MarketPlaceController@DeleteinspirationBank');
        Route::post('Deleteportfolioimage', 'MarketPlaceController@Deleteportfolioimage');
        Route::post('DeleteportfolioVideo', 'MarketPlaceController@DeleteportfolioVideo');
        Route::post('Deleteinspirationbankimage', 'MarketPlaceController@Deleteinspirationbankimage');
        Route::post('Unlinkmsgbrdremovedfile', 'MarketPlaceController@Unlinkmsgbrdremovedfile');
        Route::post('Acceptprojectcomplitionrequest', 'MarketPlaceController@Acceptprojectcomplitionrequest');
        Route::post('Addprojectcompletionrequestrejectreason', 'MarketPlaceController@Addprojectcompletionrequestrejectreason');

        Route::post('Sendinvitationfordesignersignup', 'MarketPlaceController@Sendinvitationfordesignersignup');
        Route::post('Decodeemail', 'MarketPlaceController@Decodeemail');
        Route::post('Getsentinvitationdetails', 'MarketPlaceController@Getsentinvitationdetails');
        Route::post('Addwithdrawrequest', 'MarketPlaceController@Addwithdrawrequest');
        Route::post('paymentStatusChange', 'MarketPlaceController@Paymentstatuschange');

        Route::post('Getprojectinfo', 'MarketPlaceController@Getprojectinfo');
        Route::post('Designercancelledprojects', 'MarketPlaceController@Designercancelledprojects');
        Route::post('Buyercancelledprojects', 'MarketPlaceController@Buyercancelledprojects');
        Route::post('Changecancelprojectstatus', 'MarketPlaceController@Changecancelprojectstatus');
        Route::post('Mutualcancelprojectlisting', 'MarketPlaceController@Mutualcancelprojectlisting');

        Route::post('Disputedprojectlisting', 'MarketPlaceController@Disputedprojectlisting');
        Route::post('Sellerdisputedprojectlisting', 'MarketPlaceController@Sellerdisputedprojectlisting');
        Route::post('Sellercanceldprojectrequestlisting', 'MarketPlaceController@Sellercanceldprojectrequestlisting');
        Route::post('Readmessages', 'MarketPlaceController@Readmessages');
        Route::post('Readnotifications', 'MarketPlaceController@Readnotifications');
        Route::post('Attentiongained', 'MarketPlaceController@Attentiongained');

        Route::post('Getoutstandingbalance', 'MarketPlaceController@Getoutstandingbalance');
        Route::post('Getbuyercredit', 'MarketPlaceController@Getbuyercredit');
        Route::post('Designerprojectinvitationrejected', 'MarketPlaceController@Designerprojectinvitationrejected');
        Route::post('Getserviceskills', 'MarketPlaceController@Getserviceskills');
        Route::post('Addservices', 'MarketPlaceController@Addservices');
        Route::post('Addserviceskills', 'MarketPlaceController@Addserviceskills');

        Route::post('Getexistingservices', 'MarketPlaceController@Getexistingservices');
        Route::post('Deleteservices', 'MarketPlaceController@Deleteservices');
        Route::post('Getnotifications', 'MarketPlaceController@Getnotifications');
        Route::post('Getunreadnotificationscount', 'MarketPlaceController@Getunreadnotificationscount');
        Route::post('Changecurrentlanguage', 'MarketPlaceController@Changecurrentlanguage');
        Route::post('Cancelwithoutseller', 'MarketPlaceController@Cancelwithoutseller');

        Route::post('DownloadId', 'MarketPlaceController@Download');
        Route::post('UnlinkdownloadedfileIdProof', 'MarketPlaceController@UnlinkdownloadedfileIdProof');
        Route::post('Downloadmpinspirationbank', 'MarketPlaceController@Downloadmpinspirationbank');
        Route::post('Designerinspirationbanklisting', 'MarketPlaceController@Designerinspirationbanklisting');
        Route::post('Designerinspirationbankcategorylistsing', 'MarketPlaceController@Designerinspirationbankcategorylistsing');

        Route::post('Getservices', 'MarketPlaceController@Getservices');
        Route::post('Storecart', 'MarketPlaceController@Storecart');
        Route::post('Getcartandservices', 'MarketPlaceController@Getcartandservices');
        Route::post('Removefromcart', 'MarketPlaceController@Removefromcart');
        Route::post('Addtocart', 'MarketPlaceController@Addtocart');

        Route::post('Getbankfields', 'MarketPlaceController@Getbankfields');
        Route::post('Getbankdetails', 'MarketPlaceController@Getbankdetails');
        Route::post('Getprojectextras', 'MarketPlaceController@Getprojectextras');
        Route::post('Addprojectextra', 'MarketPlaceController@Addprojectextra');
        Route::post('Makeextrapayment', 'MarketPlaceController@Makeextrapayment');

        Route::post('Createexpressinterestuser', 'MarketPlaceController@Createexpressinterestuser');
        Route::post('Showsellersonlandingpage', 'MarketPlaceController@Showsellersonlandingpage');
        Route::post('Myservicepageshown', 'MarketPlaceController@Myservicepageshown');
        Route::post('Checkemailexist', 'MarketPlaceController@Checkemailexist');
        Route::get('Registrationaccountverification', 'MarketPlaceController@Registrationaccountverification');
        Route::get('RegenerateVerificationLink', 'MarketPlaceController@RegenerateVerificationLink');
        /*widget route*/
        Route::any("mywidget", 'MarketPlaceController@Mywidget');
        Route::any("membership", 'MarketPlaceController@Mymembershipwidget');
        /*widget route*/
        
        
    /* MarketPlaceController.php method routes : end */

    /* IgniteController.php method routes : start */
        Route::any('Getvideoquizzworksheetdetails', 'IgniteController@Getvideoquizzworksheetdetails');
        Route::post('Addteammember', 'IgniteController@Addteammember');
        Route::any('GetallReferalData', 'IgniteController@GetallReferalData');
        Route::any('Getallteamdata', 'IgniteController@Getallteamdata');
        Route::post('/Dodistributorsignin', 'IgniteController@Dodistributorsignin');
        
        Route::post('Deleteteammember', 'IgniteController@Deleteteammember');
        Route::get('/Getloginuserdatadistributor', 'IgniteController@Getloginuserdatadistributor');
        Route::get('/getCategoriesWithLanguages', 'IgniteController@getCategoriesWithLanguages');
        Route::post('/CustomerSupport', 'IgniteController@CustomerSupport');
        Route::get('/Logoutdistributor', 'IgniteController@Logoutdistributor');
        Route::post('Updatedistributorninfo', 'IgniteController@Updatedistributorninfo');
        Route::post('Updatedistributorpassword', 'IgniteController@Updatedistributorpassword');

        Route::post('Getallcontent', 'IgniteController@Getallcontent');
        Route::post('DownloadWorksheet', 'IgniteController@DownloadWorksheet');
        Route::post('Contentdetail', 'IgniteController@Contentdetail');
        Route::post('CategoryExist', 'IgniteController@CategoryExist');
        Route::post('Getquizdetail', 'IgniteController@Getquizdetail');
        Route::post('Storeattempt', 'IgniteController@Storeattempt');
        Route::post('Endquiz', 'IgniteController@Endquiz');

        Route::post('Getquizresult', 'IgniteController@Getquizresult');
        Route::post('GetallPerformanceData', 'IgniteController@GetallPerformanceData');
        Route::get('Getbuzzdata', 'IgniteController@Getbuzzdata');
        Route::post('Ignitepayemnt', 'IgniteController@Ignitepayemnt');
        Route::post('Paymentinfodata', 'IgniteController@Paymentinfodata');

        Route::any('payment-process', 'IgniteController@Paymepaymentdata');
        
        Route::post('paypal-payment-details-ipn', 'IgniteController@Paypalpaymentipndata');
        Route::get('get-payment-details-ignite', 'IgniteController@Returnalipayurldetails');
        Route::get('Mysubscriptiondata', 'IgniteController@Mysubscriptiondata');
        Route::post('Videoseen', 'IgniteController@Videoseen');
        Route::any('ExportSubscribers/{export_type}', 'IgniteController@ExportSubscribers');
        
        Route::any('GetIgniteSubscriberDetailsData', 'IgniteController@GetIgniteSubscriberDetailsData');
        Route::any('GetSubscriberCurrentSubscription', 'IgniteController@GetSubscriberCurrentSubscription');
        Route::any('GetSubscriberHistorySubscription', 'IgniteController@GetSubscriberHistorySubscription');
        Route::any('ExportDistributorTeam/{export_type}', 'IgniteController@ExportDistributorTeam');
        Route::post('Teammemberdetail', 'IgniteController@Teammemberdetail');

        Route::post('Getteammembersubscribers', 'IgniteController@Getteammembersubscribers');
        Route::post('Sendotp', 'IgniteController@Sendotp');
        Route::post('Verifyotp', 'IgniteController@Verifyotp');
        Route::post('Getcommissions', 'IgniteController@Getcommissions');
        Route::post('Checkpromo', 'IgniteController@Checkpromo');

        Route::post('Gettypeandyear', 'IgniteController@Gettypeandyear');
        Route::post('Getteamforcommissions', 'IgniteController@Getteamforcommissions');
        Route::post('GetSubscriberTransaction', 'IgniteController@GetSubscriberTransaction');
        Route::post('Encryptdataforstripepayment', 'IgniteController@Encryptdataforstripepayment');
        Route::post('Stripepayemnt', 'IgniteController@Stripepayemnt');


        Route::get('Getsubjects', 'IgniteController@Getsubjects');
        Route::get('getCategories', 'IgniteController@getCategories');
        Route::post('Getcategoriesforsubject', 'IgniteController@Getcategoriesforsubject');
        Route::post('getCategoriesForSubjectFromSubjectId', 'IgniteController@getCategoriesForSubjectFromSubjectId');
        Route::post('Getsubcategories', 'IgniteController@Getsubcategories');
        Route::post('Getsubcategoriesforlandingpage', 'IgniteController@Getsubcategoriesforlandingpage');
        
        Route::post('selectLanguageForSubject', 'IgniteController@selectLanguageForSubject');
        Route::post('Savecategoryandlanguage', 'IgniteController@Savecategoryandlanguage');
        Route::post('Getzonelanguage', 'IgniteController@Getzonelanguage');
        Route::get('Getuserzone', 'IgniteController@Getuserzone');
        Route::get('GetUserTokenForTwinkle', 'IgniteController@GetUserTokenForTwinkle');

        Route::post('WantToPurchaseData', 'IgniteController@WantToPurchaseData');

        
        Route::post('Activeamember', 'IgniteController@Activeamember');
        Route::get('Getapkdownloadlink', 'IgniteController@Getapkdownloadlink');
        Route::get('GettaxrateDetails', 'IgniteController@GettaxrateDetails');
        Route::get('IgniteMysubscriptiondata', 'IgniteController@IgniteMysubscriptiondata');
        Route::get('IgniteHistorysubscriptiondata', 'IgniteController@IgniteHistorysubscriptiondata');
        Route::get('IgniteTransactionsnData', 'IgniteController@IgniteTransactionsnData');
        Route::post('SwitchPlan', 'IgniteController@SwitchPlan');
        Route::post('getSubjectCategory', 'IgniteController@getSubjectCategory');
        Route::post('CancelSubscription', 'IgniteController@CancelSubscription');
        Route::post('Applypromocode', 'IgniteController@Applypromocode');
        Route::post('Removepromocode', 'IgniteController@Removepromocode');
        Route::post('Getcategorydetails', 'IgniteController@Getcategorydetails');
        Route::post('SwitchhistoryDetails', 'IgniteController@SwitchhistoryDetails');
        Route::post('getPromocodedetails', 'IgniteController@getPromocodedetails');
        Route::post('Getpurchasecategorydetails', 'IgniteController@Getpurchasecategorydetails');
        Route::post('GetsubscriptionplanHistory', 'IgniteController@GetsubscriptionplanHistory');
        Route::post('Subscriptionpayemnt', 'IgniteController@Subscriptionpayemnt');
    /* IgniteController.php method routes : end */

    /* SphereController.php method routes : start */
        Route::post('Spheresignup', 'SphereController@Signup');
        Route::post('Signup', 'SphereController@Signup');
        Route::post('Getteacherslist', 'SphereController@Getteacherslist');
        Route::post('Getteacherchat', 'SphereController@Getteacherchat');
        Route::post('Sendmessage', 'SphereController@Sendmessage');
        Route::post('Makemessageread', 'SphereController@Makemessageread');
        
        Route::get('Getspheresubjects', 'SphereController@Getspheresubjects');
        Route::post('Getteacherdata', 'SphereController@Getteacherdata');
        Route::post('Getspheretransactions', 'SphereController@Getspheretransactions');
        Route::post('Getspherehistories', 'SphereController@Getspherehistories');
        Route::get('Getmycourses', 'SphereController@Getmycourses');

        
    /* SphereController.php method routes : end */
    
/* ************************************ 
        Method Routes front : end 
************************************ */
/* ************************************ 
        Other's : start 
************************************ */
    Route::any('/oss', 'OssController@index');
    Route::any('/part-upload', 'OssController@Partuploadfile');
    Route::any('/file-upload', 'OssController@Uploadfile');
    Route::any('/file-download', 'OssController@Downloadfile');
    Route::any('/UploadVideo', 'OssController@UploadVideo');
    Route::get('/mts-video-upload', function () {
        return view('front/layouts/mp');
    });
    Route::post('/Uploadvideodemo', 'MtsController@Uploadvideodemo');
    Route::any('/getvideos', 'MtsController@Getvideos');
/* ************************************ 
        Other's : end 
************************************ */
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
------------------------------------------------------------- ADMIN ROUTES -------------------------------------------------------------
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

    Route::group(['middleware' => ['AdminAuth']], function () {
        Route::get('/script-for-quiz-video-worksheet', 'ScriptForQuizzesVideosAndWorksheets@runScript');

        Route::get('/manage/dashboard', function () {
            return view('admin/layouts/master');
        });

        Route::get('/manage/ignite', function () {
            return view('admin/layouts/mp');
        });

        Route::get('/manage/pr/open-cases', function () {
            return view('admin/layouts/mp');
        });

        Route::get('/manage/pr/special-cases', function () {
            return view('admin/layouts/mp');
        });

        Route::get('/manage/pr/closed-cases', function () {
            return view('admin/layouts/mp');
        });

        Route::get('/manage/pr/all-accessor', function () {
            return view('admin/layouts/mp');
        });

        Route::get('/manage/pr/account-info/{id}', function () {
            return view('admin/layouts/mp');
        });

        Route::get('/manage/ignite/{url}', function ($url) {
            return view('admin/layouts/mp');})->where(['url' => 'dashboard|all-content|new-content|create-new-video|create-new-quiz|distributors|subscribers|pricing-plans|add-promo-codes|add-new-distributor|add-new-subscriber|create-new-worksheet|ignite-transactions|zone-manager|category-manager|change-password|ignite-taxrate']);    

        Route::get('/manage/ignite/quiz-info/{id}', function () {
                return view('admin/layouts/mp');
            }); 

        Route::get('/manage/ignite/edit-distributor/{id}', function () {
                return view('admin/layouts/mp');
            });

        Route::get('/manage/ignite/edit-quiz/{id}', function () {
                return view('admin/layouts/mp');
            });

        Route::get('/manage/ignite/edit-worksheet/{slug}', function () {
                return view('admin/layouts/mp');
            }); 

        Route::get('/manage/ignite/worksheet-info/{slug}', function () {
                return view('admin/layouts/mp');
            });         
            
        Route::get('/manage/ignite/video-details/{id}', function () {
                return view('admin/layouts/mp');
            }); 

        Route::get('/manage/ignite/edit-video/{id}', function () {
                return view('admin/layouts/mp');
            }); 

        Route::get('/manage/ignite/video-manager/{status?}/{category?}', function () {
                return view('admin/layouts/mp');
            }); 

        Route::get('/manage/ignite/quiz-manager/{status?}/{category?}', function () {
                return view('admin/layouts/mp');
            }); 

        Route::get('/manage/ignite/worksheet-manager/{status?}/{category?}', function () {
                return view('admin/layouts/mp');
            }); 
                
        Route::get('/manage/ignite/ignite-distributor-details/{id}', function () {
                return view('admin/layouts/mp');
            });

        Route::get('/manage/ignite/distributor-commissions/{slug}', function () {
                return view('admin/layouts/mp');
            });

        Route::get('/manage/ignite/ignite-subscriber-details/{id}', function () {
                return view('admin/layouts/mp');
            });     

        Route::get('/manage/transactions', function () {
            return view('admin/layouts/master');});

        Route::get('/manage/subscription-plans', function () {
            return view('admin/layouts/master');});

        Route::get('/manage/my-account', function () {
            return view('admin/layouts/master');});
        Route::get('/manage/admin-team', function () {
            return view('admin/layouts/master');});

        Route::get('/manage/sixteen/{url}', function ($url) {
            return view('admin/layouts/mp');
        })->where(['url' => 'sixteen-sellers|categories|sixteen-buyers|all-projects|dashboard|countries|transactions',
        ]);

        Route::get('/manage/sixteen/project-details/{id}', function () {
            return view('admin/layouts/mp');
        });
        
        Route::get('/manage/sixteen/seller-info/{id}', function () {
            return view('admin/layouts/mp');
        });

        Route::get('/manage/sixteen/buyer-info/{id}', function () {
            return view('admin/layouts/mp');
        });

        Route::get('/manage/sixteen/ticket-detail/{id}', function () {
            return view('admin/layouts/mp');
        });
        Route::get('/manage/ignite/ticket-detail/{id}', function () {
            return view('admin/layouts/mp');
        });
        Route::get('/manage/sixteen/tickets/{for}', function () {
            return view('admin/layouts/mp');
        });
        Route::get('/manage/ignite/edit-promo-codes/{slug}', function () {
            return view('admin/layouts/mp');
        });
        Route::get('/manage/ignite/promo-codes-details/{slug}', function () {
            return view('admin/layouts/mp');
        });
        Route::get('/manage/ignite/promo-codes/{status?}', function () {
            return view('admin/layouts/mp');
        });
        Route::get('/manage/ignite/tickets/{for}', function () {
            return view('admin/layouts/mp');
        });

        Route::post('/Getalljobsdata', 'AdminMarketPlaceController@Getalljobsdata');
        Route::post('/Getalldesignerdata', 'AdminMarketPlaceController@Getalldesignerdata');
        Route::post('Getallcategories', 'AdminMarketPlaceController@Getallcategories');
        Route::post('/Getallbuyerdata', 'AdminMarketPlaceController@Getallbuyerdata');
        Route::post('/Getadmincategories', 'AdminMarketPlaceController@Getadmincategories');
        Route::post('/Getprojectdetails', 'AdminMarketPlaceController@Getprojectdetails');
        Route::post('/Getprojectpaymentlogs', 'AdminMarketPlaceController@Getprojectpaymentlogs');
        Route::post('/Sendmailforratereview', 'AdminMarketPlaceController@Sendmailforratereview');
        Route::post('/Getmessagedata', 'AdminMarketPlaceController@Getmessagedata');
        Route::post('/Sendmessageadmin', 'AdminMarketPlaceController@Sendmessage');
        Route::post('/Uploadmessagefileadmin', 'AdminMarketPlaceController@Uploadmessagefileadmin');
        Route::post('/Unlinkmsgbrdremovedfileadmin', 'AdminMarketPlaceController@Unlinkmsgbrdremovedfile');
        Route::post('/Infavorofbuyer', 'AdminMarketPlaceController@Infavorofbuyer');
        Route::post('/Infavorofseller', 'AdminMarketPlaceController@Infavorofseller');
        Route::post('/Cancelprojectbyadmin', 'AdminMarketPlaceController@Cancelprojectbyadmin');                
        Route::any('/Exportdata/{Exportvalue}/{status}/{category}/{sort}/{search}/{price}/{timeleft}', 'AdminMarketPlaceController@Exportdata');//Added by ketan solanki for Exporting Data DATE: 3rd July 2018 -- New one with filter parameters       
        Route::any('/ExportBuyerData/{Exportvalue}/{sort}/{search}', 'AdminMarketPlaceController@ExportBuyerData');//Added by ketan solanki for Exporting Data DATE: 3rd July 2018        
        Route::any('/ExportSellerData/{Exportvalue}/{category}/{sort}/{search}/{proj_availability}/{rate}/{availability_long_term}/{budget}/{name}/{review}/{complete_project}/{earnings}', 'AdminMarketPlaceController@ExportSellerData');//Added by ketan solanki for Exporting Data DATE: 3rd July 2018 -- New ONe
        Route::any('/ExportCountryData/{Exportvalue}/{search}', 'AdminMarketPlaceController@ExportCountryData');//Added by ketan solanki for Exporting Data DATE: 5th July 2018        
        Route::any('/uploadCSVFileData', 'AdminMarketPlaceController@uploadCSVFileData');//Added by ketan solanki for Exporting Data DATE: 5th July 2018        
        Route::any('/Exportticketdata/{Exportvalue}/{tickettype}/{ticketcategory}/{search}/{startdate}/{enddate}', 'AdminMarketPlaceController@Exportticketdata');

        Route::any('/ExportTransactionData/{Exportvalue}/{status}/{amount}', 'AdminMarketPlaceController@ExportTransactionData');

        Route::post('/Addeditcategorydata', 'AdminMarketPlaceController@Addeditcategorydata');
        Route::post('/Enabledeleteskill', 'AdminMarketPlaceController@Enabledeleteskill');
        Route::post('/Getinspirationcategorydata', 'AdminMarketPlaceController@Getinspirationcategorydata');
        Route::post('/Addeditinscategorydata', 'AdminMarketPlaceController@Addeditinscategorydata');
        Route::post('/Getactiveprojectsbyskill', 'AdminMarketPlaceController@Getactiveprojectsbyskill');
        Route::post('/Getinspirationuploadsbycategory', 'AdminMarketPlaceController@Getinspirationuploadsbycategory');
        Route::post('/Enabledeleteinscategory', 'AdminMarketPlaceController@Enabledeleteinscategory');
        Route::post('/Getticketcategories', 'AdminMarketPlaceController@Getticketcategories');
        Route::post('/Gettickettypes', 'AdminMarketPlaceController@Gettickettypes');
        Route::post('/Getticketlisting', 'AdminMarketPlaceController@Getticketlisting');
        Route::post('/Getticketdetails', 'AdminMarketPlaceController@Getticketdetails');
        Route::post('/Sendmessageuser', 'AdminMarketPlaceController@Sendmessageuser');
        Route::post('/GetUserCountriesListing', 'AdminMarketPlaceController@GetUserCountriesListing');
        Route::post('/Getsellerdetail', 'AdminMarketPlaceController@Getsellerdetail');
        Route::post('/Getbuyerdetail', 'AdminMarketPlaceController@Getbuyerdetail');
        Route::post('/Getadminservices', 'AdminMarketPlaceController@Getadminservices');
        Route::post('/Designeradminworkhistoryandreviews', 'AdminMarketPlaceController@Designeradminworkhistoryandreviews');
        Route::post('/Getadminportfolio', 'AdminMarketPlaceController@Getadminportfolio');
        Route::post('/Markascompleted', 'AdminMarketPlaceController@Markascompleted');
        Route::post('GetBuyerProjects', 'AdminMarketPlaceController@GetBuyerProjects');
        Route::post('Gettransactionlist', 'AdminMarketPlaceController@Gettransactionlist');
        Route::post('Withdrawamountpaidbyadmin', 'AdminMarketPlaceController@Withdrawamountpaidbyadmin');
        
        Route::post('Assignticket', 'AdminMarketPlaceController@Assignticket');
        Route::post('Closeticket', 'AdminMarketPlaceController@Closeticket');


        Route::any('GetAllQuizData', 'AdminIgniteController@GetAllQuizData');
        Route::any('UpdateContentOrder', 'AdminIgniteController@UpdateContentOrder');
        Route::post('GetAllContentData', 'AdminIgniteController@GetAllContentData');
        Route::post('Getvideocategories', 'AdminIgniteController@Getvideocategories');
        Route::post('Uploadnewvideo', 'AdminIgniteController@Uploadnewvideo');
        Route::post('Editvideo', 'AdminIgniteController@Editvideo');
        
        Route::post('addNewDistributor', 'AdminIgniteController@AddNewDistributor');
        Route::post('CheckOutstandingCommisions', 'AdminIgniteController@CheckOutstandingCommisions');
        Route::post('suspendAccount', 'AdminIgniteController@suspendAccount');
        Route::post('teamAccountAction', 'AdminIgniteController@Teamaccountaction');
        Route::post('sendEmailToDistributor', 'AdminIgniteController@sendEmailToDistributor');
        Route::post('changeDistributorPassword', 'AdminIgniteController@changeDistributorPassword');
        
        Route::post('GetQuizDetails', 'AdminIgniteController@GetQuizDetails');
        Route::post('UnPublishQuiz', 'AdminIgniteController@UnPublishQuiz');
        Route::post('PublishQuiz', 'AdminIgniteController@PublishQuiz');
        Route::post('GetVideoDetails', 'AdminIgniteController@GetVideoDetails');
        Route::post('PublishVideo', 'AdminIgniteController@PublishVideo');
        
        Route::post('UnPublishVideo', 'AdminIgniteController@UnPublishVideo');
        Route::post('DeleteVideo', 'AdminIgniteController@DeleteVideo');
        Route::post('Deletequiz', 'AdminIgniteController@Deletequiz');
        Route::post('Deleteworksheet', 'AdminIgniteController@Deleteworksheet');
        Route::post('Loadprerequisites', 'AdminIgniteController@Loadprerequisites');
        Route::any('GetQuizListing', 'AdminIgniteController@GetQuizListing');
        
        Route::any('ExportToExcel', 'AdminIgniteController@ExportToExcel');
        
        Route::any('GetallDistributorData', 'AdminIgniteController@GetallDistributorData');
        Route::any('GetDistributorDetailsData', 'AdminIgniteController@GetDistributorDetailsData');
        Route::any('GetDistributorMembers', 'AdminIgniteController@GetDistributorMembers');
        Route::any('GetDistributorReferalls', 'AdminIgniteController@GetDistributorReferalls');
        Route::post('Uploadnewquiz', 'AdminIgniteController@Uploadnewquiz');
        
        Route::post('Addnewquestion', 'AdminIgniteController@Addnewquestion');
        Route::any('GetAllVideoData', 'AdminIgniteController@GetAllVideoData');
        Route::any('GetAllWorkSheetData', 'AdminIgniteController@GetAllWorkSheetData');
        Route::post('Addquiztitle', 'AdminIgniteController@Addquiztitle');
        Route::post('Quizpublishdraft', 'AdminIgniteController@Quizpublishdraft');
        
        Route::post('Uploadnewworksheet', 'AdminIgniteController@Uploadnewworksheet');
        Route::any('GetVideoListing', 'AdminIgniteController@GetVideoListing');
        Route::any('download-worksheet-file', 'AdminIgniteController@DownloadWorkSheetFile');
        Route::post('GetworkSheetDetails', 'AdminIgniteController@GetworkSheetDetails');
        Route::post('Editworksheet', 'AdminIgniteController@Editworksheet');
        
        Route::post('UnpublishWorkSheet', 'AdminIgniteController@UnpublishWorkSheet');
        Route::post('ChangeQuizStatus', 'AdminIgniteController@ChangeQuizStatus');
        Route::post('Editquestion', 'AdminIgniteController@Editquestion');
        Route::any('GetDistributorDetails', 'AdminIgniteController@GetDistributorDetails');
        Route::any('editDistributor', 'AdminIgniteController@editDistributor');
        
        Route::post('RegenerateQRCode', 'AdminIgniteController@RegenerateQRCode');
        Route::post('RegenerateReferalCode', 'AdminIgniteController@RegenerateReferalCode');
        Route::post('RegenerateUniqueLink', 'AdminIgniteController@RegenerateUniqueLink');
        Route::any('download-qrcode-file', 'AdminIgniteController@DownloadQrCodeFile');
        Route::any('GetallSubscriberData', 'AdminIgniteController@GetallSubscriberData');
        
        Route::any('GetSubscriberDetailsData', 'AdminIgniteController@GetSubscriberDetailsData');
        Route::any('GetCurrentSubscription', 'AdminIgniteController@GetCurrentSubscription');
        Route::any('GetHistorySubscription', 'AdminIgniteController@GetHistorySubscription');
        Route::post('addNewSubscriber', 'AdminIgniteController@addNewSubscriber');
        Route::get('generatePassword', 'AdminIgniteController@generatePassword');
        Route::post('Getcategoriesforsubscription', 'AdminIgniteController@Getcategoriesforsubscription');
        Route::post('getAllSubscription', 'AdminIgniteController@getAllSubscription');
        Route::post('AddSubscription', 'AdminIgniteController@AddSubscription');
        Route::post('removeSubscription','AdminIgniteController@removeSubscription');
        Route::post('UpdateQuizQuestionOrder', 'AdminIgniteController@UpdateQuizQuestionOrder');
        Route::post('Deletequestion', 'AdminIgniteController@Deletequestion');
        Route::post('Editsubscriberdetails', 'AdminIgniteController@Editsubscriberdetails');
        Route::post('Getallbuzzcategories', 'AdminIgniteController@Getallbuzzcategories');
        
        Route::post('Savecategory', 'AdminIgniteController@Savecategory');
        Route::post('Savepromo', 'AdminIgniteController@Savepromo');
        Route::post('Showteammemberinfo', 'AdminIgniteController@Showteammemberinfo');
        Route::post('Getalldistributors', 'AdminIgniteController@Getalldistributors');
        Route::post('Generatepromo', 'AdminIgniteController@Generatepromo');

        Route::post('Savepromocode', 'AdminIgniteController@Savepromocode');
        Route::post('GetAllPromoData', 'AdminIgniteController@GetAllPromoData');
        Route::any('ExportPromos', 'AdminIgniteController@ExportPromos');
        Route::post('Getpromodetaildata', 'AdminIgniteController@Getpromodetaildata');
        Route::post('Suspendpromo', 'AdminIgniteController@Suspendpromo');

        Route::post('GetPromoDetail', 'AdminIgniteController@GetPromoDetail');
        Route::post('Getdistributorcommissionsforadmin', 'AdminIgniteController@Getcommissions');
        Route::post('Gettypeandyearforadmin', 'AdminIgniteController@Gettypeandyear');
        Route::post('Getteamforcommissionsforadmin', 'AdminIgniteController@Getteamforcommissions');
        Route::post('Getignitetransactions', 'AdminIgniteController@Getignitetransactions');

        Route::post('removeSubscriptionOfSubscriber', 'AdminIgniteController@removeSubscriptionOfSubscriber');

        Route::any('ExportTransactions', 'AdminIgniteController@ExportTransactions');
        Route::post('Getcountryforzones', 'AdminIgniteController@Getcountryforzones');
        Route::get('Getlanguagesforzones', 'AdminIgniteController@Getlanguagesforzones');
        Route::post('Getzones', 'AdminIgniteController@Getzones');
        Route::post('Savenewzone', 'AdminIgniteController@Savenewzone');

        Route::post('Deletezone', 'AdminIgniteController@Deletezone');
        Route::post('Savenewcategory', 'AdminIgniteController@Savenewcategory');
        Route::post('Getcategories', 'AdminIgniteController@Getcategories');
        Route::get('addOrEditBundleCategoryId', 'AdminIgniteController@addOrEditBundleCategoryId');
        Route::post('Deletesubject', 'AdminIgniteController@Deletesubject');
        Route::post('Loadcategoriesforsubject', 'AdminIgniteController@Loadcategoriesforsubject');

        Route::post('Loadlanguagesforzones', 'AdminIgniteController@Loadlanguagesforzones');
        Route::post('Getsubcategoriesadmin', 'AdminIgniteController@Getsubcategories');
        Route::post('Updateadminpassword', 'AdminIgniteController@Updateadminpassword');

        Route::post('GetIgnitehistory', 'AdminIgniteController@GetIgnitehistory');
        Route::post('GetTaxrate', 'AdminIgniteController@GetTaxrate');
        Route::post('Getignitetaxrate', 'AdminIgniteController@Getignitetaxrate');
    });

    Route::get('/manage/pr/accessors-my-account', function () {
        return view('admin/layouts/mp');
    });

    Route::get('/manage/pr/my-jobs', function () {
        return view('admin/layouts/mp');
    });



/* ************************************ 
        Display Routes admin : start 
************************************ */

    

    Route::get('/manage', function () {
        return redirect('manage/login');
    });
    Route::get('/manage/123', function () {
        print_r("expression"); die;
    });

    Route::get('/manage/pr-login', function () {
        return view('admin/pr-login');
    });

    // Route::get('/manage/dashboard', function () {
    //     return view('admin/layouts/master');
    // })->middleware('ManageAuth:admins');

    // Route::get('/manage/ignite', function () {
    //     return view('admin/layouts/mp');
    // })->middleware('ManageAuth:admins');

    // Route::get('/manage/pr/open-cases', function () {
    //     return view('admin/layouts/mp');
    // })->middleware('ManageAuth:admins');

    // Route::get('/manage/pr/special-cases', function () {
    //     return view('admin/layouts/mp');
    // })->middleware('ManageAuth:admins');

    // Route::get('/manage/pr/closed-cases', function () {
    //     return view('admin/layouts/mp');
    // })->middleware('ManageAuth:admins');

    // Route::get('/manage/pr/all-accessor', function () {
    //     return view('admin/layouts/mp');
    // })->middleware('ManageAuth:admins');

    // Route::get('/manage/pr/account-info/{id}', function () {
    //     return view('admin/layouts/mp');
    // })->middleware('ManageAuth:admins');

    // Route::get('/manage/pr/accessors-my-account', function () {
    //     return view('admin/layouts/mp');
    // })->middleware('ManageAuth:admins');

    // Route::get('/manage/pr/my-jobs', function () {
    //     return view('admin/layouts/mp');
    // })->middleware('ManageAuth:admins');

    // Route::get('/manage/ignite/{url}', function ($url) {
    //     return view('admin/layouts/mp');})->where(['url' => 'dashboard|all-content|new-content|create-new-video|create-new-quiz|distributors|subscribers|add-new-distributor|add-new-subscriber|create-new-worksheet'])->middleware('ManageAuth:admins');    

    // Route::get('/manage/ignite/quiz-info/{id}', function () {
    //         return view('admin/layouts/mp');
    //     })->middleware('ManageAuth:admins'); 

    // Route::get('/manage/ignite/edit-distributor/{id}', function () {
    //         return view('admin/layouts/mp');
    //     })->middleware('ManageAuth:admins');

    // Route::get('/manage/ignite/edit-quiz/{id}', function () {
    //         return view('admin/layouts/mp');
    //     })->middleware('ManageAuth:admins');

    // Route::get('/manage/ignite/edit-worksheet/{slug}', function () {
    //         return view('admin/layouts/mp');
    //     })->middleware('ManageAuth:admins'); 

    // Route::get('/manage/ignite/worksheet-info/{slug}', function () {
    //         return view('admin/layouts/mp');
    //     })->middleware('ManageAuth:admins');         
        
    // Route::get('/manage/ignite/video-details/{id}', function () {
    //         return view('admin/layouts/mp');
    //     })->middleware('ManageAuth:admins'); 

    // Route::get('/manage/ignite/edit-video/{id}', function () {
    //         return view('admin/layouts/mp');
    //     })->middleware('ManageAuth:admins'); 

    // Route::get('/manage/ignite/video-manager/{status?}/{category?}', function () {
    //         return view('admin/layouts/mp');
    //     })->middleware('ManageAuth:admins'); 

    // Route::get('/manage/ignite/quiz-manager/{status?}/{category?}', function () {
    //         return view('admin/layouts/mp');
    //     })->middleware('ManageAuth:admins'); 

    // Route::get('/manage/ignite/worksheet-manager/{status?}/{category?}', function () {
    //         return view('admin/layouts/mp');
    //     })->middleware('ManageAuth:admins'); 
            
    // Route::get('/manage/ignite/ignite-distributor-details/{id}', function () {
    //         return view('admin/layouts/mp');
    //     })->middleware('ManageAuth:admins');

    // Route::get('/manage/ignite/ignite-subscriber-details/{id}', function () {
    //         return view('admin/layouts/mp');
    //     })->middleware('ManageAuth:admins');     

    // Route::get('/manage/transactions', function () {
    //     return view('admin/layouts/master');})->middleware('ManageAuth:admins');

    // Route::get('/manage/subscription-plans', function () {
    //     return view('admin/layouts/master');})->middleware('ManageAuth:admins');

    // Route::get('/manage/my-account', function () {
    //     return view('admin/layouts/master');})->middleware('ManageAuth:admins');
    // Route::get('/manage/admin-team', function () {
    //     return view('admin/layouts/master');})->middleware('ManageAuth:admins');
/* ************************************ 
        Display Routes admin : end 
************************************ */
/* ************************************ 
        Method Routes admin : start 
************************************ */

    
    /*AdminMarketPlaceController.php method routes : start */
        // Route::get('/manage/sixteen/{url}', function ($url) {
        //     return view('admin/layouts/mp');
        // })->where(['url' => 'sixteen-sellers|categories|sixteen-buyers|all-projects|dashboard|tickets|countries|transactions',
        // ])->middleware('ManageAuth:admins');

        // Route::get('/manage/sixteen/project-details/{id}', function () {
        //     return view('admin/layouts/mp');
        // })->middleware('ManageAuth:admins');
        
        // Route::get('/manage/sixteen/seller-info/{id}', function () {
        //     return view('admin/layouts/mp');
        // })->middleware('ManageAuth:admins');

        // Route::get('/manage/sixteen/buyer-info/{id}', function () {
        //     return view('admin/layouts/mp');
        // })->middleware('ManageAuth:admins');

        // Route::get('/manage/sixteen/ticket-detail/{id}', function () {
        //     return view('admin/layouts/mp');
        // })->middleware('ManageAuth:admins');

        // Route::post('/Getalljobsdata', 'AdminMarketPlaceController@Getalljobsdata');
        // Route::post('/Getalldesignerdata', 'AdminMarketPlaceController@Getalldesignerdata');
        // Route::post('Getallcategories', 'AdminMarketPlaceController@Getallcategories');
        // Route::post('/Getallbuyerdata', 'AdminMarketPlaceController@Getallbuyerdata');
        // Route::post('/Getadmincategories', 'AdminMarketPlaceController@Getadmincategories');
        // Route::post('/Getprojectdetails', 'AdminMarketPlaceController@Getprojectdetails');
        // Route::post('/Getprojectpaymentlogs', 'AdminMarketPlaceController@Getprojectpaymentlogs');
        // Route::post('/Sendmailforratereview', 'AdminMarketPlaceController@Sendmailforratereview');
        // Route::post('/Getmessagedata', 'AdminMarketPlaceController@Getmessagedata');
        // Route::post('/Sendmessageadmin', 'AdminMarketPlaceController@Sendmessage');
        // Route::post('/Uploadmessagefileadmin', 'AdminMarketPlaceController@Uploadmessagefileadmin');
        // Route::post('/Unlinkmsgbrdremovedfileadmin', 'AdminMarketPlaceController@Unlinkmsgbrdremovedfile');
        // Route::post('/Infavorofbuyer', 'AdminMarketPlaceController@Infavorofbuyer');
        // Route::post('/Infavorofseller', 'AdminMarketPlaceController@Infavorofseller');
        // Route::post('/Cancelprojectbyadmin', 'AdminMarketPlaceController@Cancelprojectbyadmin');                
        // Route::any('/Exportdata/{Exportvalue}/{status}/{category}/{sort}/{search}/{price}/{timeleft}', 'AdminMarketPlaceController@Exportdata');//Added by ketan solanki for Exporting Data DATE: 3rd July 2018 -- New one with filter parameters       
        // Route::any('/ExportBuyerData/{Exportvalue}/{sort}/{search}', 'AdminMarketPlaceController@ExportBuyerData');//Added by ketan solanki for Exporting Data DATE: 3rd July 2018        
        // Route::any('/ExportSellerData/{Exportvalue}/{category}/{sort}/{search}/{proj_availability}/{rate}/{availability_long_term}/{budget}/{name}/{review}/{complete_project}/{earnings}', 'AdminMarketPlaceController@ExportSellerData');//Added by ketan solanki for Exporting Data DATE: 3rd July 2018 -- New ONe
        // Route::any('/ExportCountryData/{Exportvalue}/{search}', 'AdminMarketPlaceController@ExportCountryData');//Added by ketan solanki for Exporting Data DATE: 5th July 2018        
        // Route::any('/uploadCSVFileData', 'AdminMarketPlaceController@uploadCSVFileData');//Added by ketan solanki for Exporting Data DATE: 5th July 2018        
        // Route::any('/Exportticketdata/{Exportvalue}/{tickettype}/{ticketcategory}/{search}/{startdate}/{enddate}', 'AdminMarketPlaceController@Exportticketdata');

        // Route::any('/ExportTransactionData/{Exportvalue}/{status}/{amount}', 'AdminMarketPlaceController@ExportTransactionData');

        // Route::post('/Addeditcategorydata', 'AdminMarketPlaceController@Addeditcategorydata');
        // Route::post('/Enabledeleteskill', 'AdminMarketPlaceController@Enabledeleteskill');
        // Route::post('/Getinspirationcategorydata', 'AdminMarketPlaceController@Getinspirationcategorydata');
        // Route::post('/Addeditinscategorydata', 'AdminMarketPlaceController@Addeditinscategorydata');
        // Route::post('/Getactiveprojectsbyskill', 'AdminMarketPlaceController@Getactiveprojectsbyskill');
        // Route::post('/Getinspirationuploadsbycategory', 'AdminMarketPlaceController@Getinspirationuploadsbycategory');
        // Route::post('/Enabledeleteinscategory', 'AdminMarketPlaceController@Enabledeleteinscategory');
        // Route::post('/Getticketcategories', 'AdminMarketPlaceController@Getticketcategories');
        // Route::post('/Gettickettypes', 'AdminMarketPlaceController@Gettickettypes');
        // Route::post('/Getticketlisting', 'AdminMarketPlaceController@Getticketlisting');
        // Route::post('/Getticketdetails', 'AdminMarketPlaceController@Getticketdetails');
        // Route::post('/Sendmessageuser', 'AdminMarketPlaceController@Sendmessageuser');
        // Route::post('/GetUserCountriesListing', 'AdminMarketPlaceController@GetUserCountriesListing');
        // Route::post('/Getsellerdetail', 'AdminMarketPlaceController@Getsellerdetail');
        // Route::post('/Getbuyerdetail', 'AdminMarketPlaceController@Getbuyerdetail');
        // Route::post('/Getadminservices', 'AdminMarketPlaceController@Getadminservices');
        // Route::post('/Designeradminworkhistoryandreviews', 'AdminMarketPlaceController@Designeradminworkhistoryandreviews');
        // Route::post('/Getadminportfolio', 'AdminMarketPlaceController@Getadminportfolio');
        // Route::post('/Markascompleted', 'AdminMarketPlaceController@Markascompleted');
        // Route::post('GetBuyerProjects', 'AdminMarketPlaceController@GetBuyerProjects');
        // Route::post('Gettransactionlist', 'AdminMarketPlaceController@Gettransactionlist');
        // Route::post('Withdrawamountpaidbyadmin', 'AdminMarketPlaceController@Withdrawamountpaidbyadmin');
        
    /* AdminMarketPlaceController.php method routes : end */
    

    /* AdminPrLoginController.php method routes : start */
        Route::get('/Checkpradminemailexists', 'AdminPrLoginController@Checkpradminemailexists');
        Route::get('Prlogout', 'AdminPrLoginController@Prlogout');
        Route::get('/Adminprforgotpassword', 'AdminPrLoginController@Adminprforgotpassword');
        Route::get('/AdminprResetpassword', 'AdminPrLoginController@Adminprresetpassword');
        
        Route::post('Doadminprresetpassword', 'AdminPrLoginController@Doadminprresetpassword');
        
        Route::middleware('ManageAuth')->post('Pruserdata', 'AdminPrLoginController@Pruserdata');
    /* AdminPrLoginController.php method routes : end */

    /* AdminSubscriptionController.php method routes : start */
        Route::middleware('ManageAuth')->post('Getsubscriptions', 'AdminSubscriptionController@Getsubscriptions');
        Route::middleware('ManageAuth')->post('Getpayments', 'AdminSubscriptionController@Getpayments');
        Route::middleware('ManageAuth')->post('GetSubscriptionusers', 'AdminSubscriptionController@GetSubscriptionusers');
    /* AdminSubscriptionController.php method routes : end */

    /* AdminPRController.php method routes : start */
        Route::middleware('ManageAuth:admins')->post('/Getadmincases', 'AdminPRController@Getcases');
        Route::middleware('ManageAuth:admins')->post('/Getaccessors', 'AdminPRController@Getaccessors');
        Route::middleware('ManageAuth:admins')->post('/Assignaccessor', 'AdminPRController@Assignaccessor');
        Route::middleware('ManageAuth:admins')->post('/Addaccessor', 'AdminPRController@Addaccessor');
        Route::middleware('ManageAuth:admins')->post('/Getaccessorinfo', 'AdminPRController@Getaccessorinfo');

        Route::middleware('ManageAuth:pr-admins')->post('/Updatepassword', 'AdminPRController@Updatepassword');
        Route::middleware('ManageAuth:pr-admins')->post('/Myjobcases', 'AdminPRController@Myjobcases');
        Route::middleware('ManageAuth:pr-admins')->post('/Accessorcompletedjobs', 'AdminPRController@Accessorcompletedjobs');
        Route::middleware('ManageAuth:pr-admins')->post('/Myteaminfo', 'AdminPRController@Myteaminfo');
        Route::middleware('ManageAuth:pr-admins')->post('/Helpme', 'AdminPRController@Helpme');

        Route::middleware('ManageAuth:admins')->post('/Download', 'AdminPRController@Download');
        Route::middleware('ManageAuth:admins')->any('/Unlinkdownloadedfile', 'AdminPRController@Unlinkdownloadedfile');
        Route::middleware('ManageAuth:admins')->post('/Acceptorrejectcase', 'AdminPRController@Acceptorrejectcase');
        Route::middleware('ManageAuth:admins')->post('/Accessedfile', 'AdminPRController@Accessedfile');
        Route::middleware('ManageAuth:admins')->post('/Cancelcase', 'AdminPRController@Cancelcase');
    /* AdminPRController.php method routes : end */

    /* AdminLoginController.php method routes : start */
        Route::get('logout', 'AdminLoginController@Logout');
        Route::middleware('ManageAuth')->post('Addadmin', 'AdminLoginController@Addadmin');
        Route::middleware('ManageAuth')->post('Deleteadmin', 'AdminLoginController@Deleteadmin');
        Route::middleware('ManageAuth')->post('Changepassword', 'AdminLoginController@Changepassword');
        Route::get('/Checkadminemailexists', 'AdminLoginController@Checkadminemailexists');

        Route::get('/Adminforgotpassword', 'AdminLoginController@Adminforgotpassword');
        Route::get('/AdminResetpassword', 'AdminLoginController@AdminResetpassword');
        Route::post('Doadminresetpassword', 'AdminLoginController@Doadminresetpassword');
        Route::post('Userdata', 'AdminLoginController@Userdata');
        // Route::middleware('ManageAuth:admins')->post('Userdata', 'AdminLoginController@Userdata');
        Route::middleware('ManageAuth')->post('Editadminprofile', 'AdminLoginController@Editadminprofile');

        Route::middleware('ManageAuth')->post('Adminlisting', 'AdminLoginController@Adminlisting');
        Route::group(['prefix' => 'manage'], function () {
            Route::resource('login', 'AdminLoginController');
            Route::post('/Dologin', 'AdminLoginController@Dologin');
            Route::post('/Doprlogin', 'AdminPrLoginController@Doprlogin');
            //    Route::get('resetpassword', 'AdminLoginController@Resetpassword');
            //    Route::post('doresetpassword', 'DirectoryController@Doresetpassword');
        });
        Route::group(['prefix' => 'manage'], function() {
            //  login bypass for the below listed controllers
            //  Route::resource('login', 'AdminLoginController');
            //  Route::post('forgotpassword', 'AdminLoginController@Forgotpassword');
            //  Route::get('resetpassword', 'AdminLoginController@Resetpassword');
            //  Route::post('doresetpassword', 'DirectoryController@Doresetpassword');
        });
    /* AdminLoginController.php method routes : end */

    /* AdminELTController.php method routes : start */
        Route::middleware('ManageAuth')->post('Getreportingofficers', 'AdminELTController@Getreportingofficers');
        Route::middleware('ManageAuth')->post('Getadmineltsections', 'AdminELTController@Getadmineltsections');
        Route::middleware('ManageAuth')->post('Addnewsection', 'AdminELTController@Addnewsection');
        Route::middleware('ManageAuth')->post('Getallvideoposition', 'AdminELTController@Getallvideoposition');
        Route::middleware('ManageAuth')->post('Addnewvideo', 'AdminELTController@Addnewvideo');
    /* AdminELTController.php method routes : end */
/* ************************************ 
        Method Routes admin : end 
************************************ */
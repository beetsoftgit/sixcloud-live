<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('api/login', 'UserController@login'); // to login user
## Authentication related calls
Route::group(['namespace' => 'api'], function () {
    Route::post('/login', 'UserController@login'); // to login user
});
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$api = $app->make(Dingo\Api\Routing\Router::class);
$api->version('v1',['prefix' => 'v1'], function ($api) {
	$api->post('/register', ['uses' => 'App\Http\Controllers\UserController@postCreateuser', 'as' => 'api.users']);
	$api->post('/login', ['uses' => 'App\Http\Controllers\UserController@postLogin', 'as' => 'api.users']);
	$api->get('/countries', ['uses' => 'App\Http\Controllers\UserController@allCountries', 'as' => 'api.countries']);
    $api->get('/countries/{id}/states', ['uses' => 'App\Http\Controllers\UserController@allStates', 'as' => 'api.states']);
    $api->get('/countries/{country_id}/states/{state_id}/cities', ['uses' => 'App\Http\Controllers\UserController@allCities', 'as' => 'api.cities']);
    $api->get('/sendotp', ['uses' => 'App\Http\Controllers\IgniteappController@Sendsmsforverfication', 'as' => 'api.sendotp']);
    $api->post('/storeLogFile', ['uses' => 'App\Http\Controllers\IgniteappController@storeLogFile', 'as' => 'api.storeLogFile']);
    $api->post('/payerid', ['uses' => 'App\Http\Controllers\IgniteappController@getPayeridwechat', 'as' => 'api.getPayeridwechat']); //temperary
    $api->post('/orderstatus', ['uses' => 'App\Http\Controllers\IgniteappController@getPaymentOrderStatus', 'as' => 'api.orderstatus']);
    $api->post('/forgotPassword', ['uses' => 'App\Http\Controllers\UserController@forgotPassword', 'as' => 'api.forgotPassword']);
    
    $api->post('/Verifyotp', ['uses' => 'App\Http\Controllers\UserController@Verifyotp', 'as' => 'api.Verifyotp']);
    $api->post('/Resendotp', ['uses' => 'App\Http\Controllers\UserController@Resendotp', 'as' => 'api.Resendotp']);
    $api->get('/versionUpdate', ['uses' => 'App\Http\Controllers\IgniteappController@versionUpdate', 'as' => 'api.versionUpdate']);
    $api->get('/getAllStores', ['uses' => 'App\Http\Controllers\v3\IgniteappController@getAllStores', 'as' => 'api.getAllStores']);
    //$api->post('/stripPay', ['uses' => 'App\Http\Controllers\IgniteappController@stripIntegrate', 'as' => 'api.stripIntegrate']);
    
    $api->group(['middleware' => 'api.auth'], function ($api) {        
        $api->get('/all-users', ['uses' => 'App\Http\Controllers\UserController@allUsers']);
        $api->get('/logout', ['uses' => 'App\Http\Controllers\UserController@postLogout', 'as' => 'api.users']);
        $api->post('/users/me', ['uses' => 'App\Http\Controllers\UserController@Updateuserprofile', 'as' => 'api.updateProfile']);
        $api->patch('/users/me/password', ['uses' => 'App\Http\Controllers\UserController@Changepassword', 'as' => 'api.changePassword']);
        $api->patch('/refresh', ['uses' => 'App\Http\Controllers\UserController@patchRefresh','as' => 'api.refresh']);
        $api->get('/contents/{content_id}/type/{content_type}', ['uses' => 'App\Http\Controllers\IgniteappController@contentDetail', 'as' => 'api.contentDetail']);
        $api->get('/categories', ['uses' => 'App\Http\Controllers\IgniteappController@allCategories', 'as' => 'api.categories']);
        $api->get('/categories/{category_id}', ['uses' => 'App\Http\Controllers\IgniteappController@allContent', 'as' => 'api.contents']);
        $api->post('/pausevideo', ['uses' => 'App\Http\Controllers\IgniteappController@pauseVideo', 'as' => 'api.pausevideo']);
        
        // $api->get('/videos/{video_url_id}/downloaded/{imei?}', ['uses' => 'App\Http\Controllers\IgniteappController@videoDownloaded', 'as' => 'api.videoDownloaded']);
        $api->get('/videos/{video_url_id}/downloaded', ['uses' => 'App\Http\Controllers\IgniteappController@videoDownloaded', 'as' => 'api.videoDownloaded']);
        $api->get('/worksheets/{worksheet_id}/downloaded', ['uses' => 'App\Http\Controllers\IgniteappController@worksheetDownloaded', 'as' => 'api.worksheetDownloaded']);
        $api->post('/endQuiz', ['uses' => 'App\Http\Controllers\IgniteappController@endQuiz', 'as' => 'api.endQuiz']);
        $api->post('/flagContent', ['uses' => 'App\Http\Controllers\IgniteappController@flagContent', 'as' => 'api.flagContent']);
        $api->get('/getAllPerformanceData/{page}', ['uses' => 'App\Http\Controllers\IgniteappController@getAllPerformanceData', 'as' => 'api.getAllPerformanceData']);
        $api->post('/storePayment', ['uses' => 'App\Http\Controllers\IgniteappController@storePayment', 'as' => 'api.storePayment']);
        //$api->get('/payerid/{amount_tobe_paid}', ['uses' => 'App\Http\Controllers\IgniteappController@getPayeridwechat', 'as' => 'api.getPayeridwechat']);
        $api->get('/generateClientToken', ['uses' => 'App\Http\Controllers\IgniteappController@generateClientToken', 'as' => 'api.generateClientToken']);
        $api->post('/makePaymentThroughNonce', ['uses' => 'App\Http\Controllers\IgniteappController@makePaymentThroughNonce', 'as' => 'api.makePaymentThroughNonce']);
        /*$api->get('/getDownloadedVideoList/{imei?}', ['uses' => 'App\Http\Controllers\IgniteappController@getDownloadedVideoList', 'as' => 'api.getDownloadedVideoList']);
        $api->get('/deletedVideos/{video_url_id}/deleted/{imei?}', ['uses' => 'App\Http\Controllers\IgniteappController@getDownloadedDeletedVideoList', 'as' => 'api.getDownloadedDeletedVideoList']);*/
        $api->get('/getDownloadedVideoList', ['uses' => 'App\Http\Controllers\IgniteappController@getDownloadedVideoList', 'as' => 'api.getDownloadedVideoList']);
        $api->get('/deletedVideos/{video_url_id}/deleted', ['uses' => 'App\Http\Controllers\IgniteappController@getDownloadedDeletedVideoList', 'as' => 'api.getDownloadedDeletedVideoList']);
        $api->post('/stripPay', ['uses' => 'App\Http\Controllers\IgniteappController@stripIntegrate', 'as' => 'api.stripIntegrate']);
        $api->post('/syncOfflineData', ['uses' => 'App\Http\Controllers\IgniteappController@syncOfflineData', 'as' => 'api.syncOfflineData']);
        $api->post('/CheckPromoCode', ['uses' => 'App\Http\Controllers\IgniteappController@CheckPromoCode', 'as' => 'api.CheckPromoCode']);
    });
});


$api->version('v1',['prefix' => 'v2'], function ($api) {
    $api->post('/register', ['uses' => 'App\Http\Controllers\v2\UserController@postCreateuser', 'as' => 'api.users']);
    $api->post('/login', ['uses' => 'App\Http\Controllers\v2\UserController@postLogin', 'as' => 'api.users']);
    $api->get('/countries', ['uses' => 'App\Http\Controllers\v2\UserController@allCountries', 'as' => 'api.countries']);
    $api->get('/countries/{id}/states', ['uses' => 'App\Http\Controllers\v2\UserController@allStates', 'as' => 'api.states']);
    $api->get('/countries/{country_id}/states/{state_id}/cities', ['uses' => 'App\Http\Controllers\v2\UserController@allCities', 'as' => 'api.cities']);
    $api->post('/storeLogFile', ['uses' => 'App\Http\Controllers\v2\IgniteappController@storeLogFile', 'as' => 'api.storeLogFile']);
    $api->post('/forgotPassword', ['uses' => 'App\Http\Controllers\v2\UserController@forgotPassword', 'as' => 'api.forgotPassword']);
    $api->post('/verifyOtp', ['uses' => 'App\Http\Controllers\v2\UserController@verifyOtp', 'as' => 'api.verifyOtp']);
    $api->post('/resendOtp', ['uses' => 'App\Http\Controllers\v2\UserController@resendOtp', 'as' => 'api.resendOtp']);
    $api->get('/versionUpdate', ['uses' => 'App\Http\Controllers\v2\IgniteappController@versionUpdate', 'as' => 'api.versionUpdate']);
    $api->get('/getAllStores', ['uses' => 'App\Http\Controllers\v3\IgniteappController@getAllStores', 'as' => 'api.getAllStores']);
    /* ##Not used
        $api->get('/sendotp', ['uses' => 'App\Http\Controllers\v2\IgniteappController@Sendsmsforverfication', 'as' => 'api.sendotp']); 
    */
    $api->group(['middleware' => 'api.auth'], function ($api) {
        $api->get('/logout', ['uses' => 'App\Http\Controllers\v2\UserController@postLogout', 'as' => 'api.users']);
        $api->post('/users/me', ['uses' => 'App\Http\Controllers\v2\UserController@Updateuserprofile', 'as' => 'api.updateProfile']);
        $api->patch('/users/me/password', ['uses' => 'App\Http\Controllers\v2\UserController@Changepassword', 'as' => 'api.changePassword']);
        $api->get('/contents/{content_id}/type/{content_type}', ['uses' => 'App\Http\Controllers\v2\IgniteappController@contentDetail', 'as' => 'api.contentDetail']);
        $api->get('/categories', ['uses' => 'App\Http\Controllers\v2\IgniteappController@allCategories', 'as' => 'api.categories']);
        $api->get('/categories/{category_id}', ['uses' => 'App\Http\Controllers\v2\IgniteappController@allContent', 'as' => 'api.contents']);
        $api->post('/pauseVideo', ['uses' => 'App\Http\Controllers\v2\IgniteappController@pauseVideo', 'as' => 'api.pauseVideo']);
        $api->get('/videos/{video_url_id}/downloaded', ['uses' => 'App\Http\Controllers\v2\IgniteappController@videoDownloaded', 'as' => 'api.videoDownloaded']);
        $api->get('/worksheets/{worksheet_id}/downloaded', ['uses' => 'App\Http\Controllers\v2\IgniteappController@worksheetDownloaded', 'as' => 'api.worksheetDownloaded']);
        $api->post('/endQuiz', ['uses' => 'App\Http\Controllers\v2\IgniteappController@endQuiz', 'as' => 'api.endQuiz']);
        $api->get('/getAllPerformanceData/{page}', ['uses' => 'App\Http\Controllers\v2\IgniteappController@getAllPerformanceData', 'as' => 'api.getAllPerformanceData']);
        $api->post('/payerId', ['uses' => 'App\Http\Controllers\v2\IgniteappController@getPayeridWechat', 'as' => 'api.payerId']);
        $api->post('/orderStatus', ['uses' => 'App\Http\Controllers\v2\IgniteappController@getPaymentOrderStatus', 'as' => 'api.orderStatus']);
        $api->get('/deletedVideos/{video_url_id}/deleted', ['uses' => 'App\Http\Controllers\v2\IgniteappController@getDownloadedDeletedVideoList', 'as' => 'api.getDownloadedDeletedVideoList']);
        $api->post('/stripPay', ['uses' => 'App\Http\Controllers\v2\IgniteappController@stripIntegrate', 'as' => 'api.stripIntegrate']);
        $api->post('/syncOfflineData', ['uses' => 'App\Http\Controllers\v2\IgniteappController@syncOfflineData', 'as' => 'api.syncOfflineData']);
        $api->post('/checkPromoCode', ['uses' => 'App\Http\Controllers\v2\IgniteappController@checkPromoCode', 'as' => 'api.checkPromoCode']);
        $api->post('/applePurchase', ['uses' => 'App\Http\Controllers\v2\IgniteappController@applePurchase', 'as' => 'api.applePurchase']);
        /* ##Not used
            $api->get('/generateClientToken', ['uses' => 'App\Http\Controllers\v2\IgniteappController@generateClientToken', 'as' => 'api.generateClientToken']);
            $api->post('/makePaymentThroughNonce', ['uses' => 'App\Http\Controllers\v2\IgniteappController@makePaymentThroughNonce', 'as' => 'api.makePaymentThroughNonce']);
            $api->get('/getDownloadedVideoList', ['uses' => 'App\Http\Controllers\v2\IgniteappController@getDownloadedVideoList', 'as' => 'api.getDownloadedVideoList']);
            $api->post('/storePayment', ['uses' => 'App\Http\Controllers\v2\IgniteappController@storePayment', 'as' => 'api.storePayment']);
            $api->post('/flagContent', ['uses' => 'App\Http\Controllers\v2\IgniteappController@flagContent', 'as' => 'api.flagContent']);
            $api->patch('/refresh', ['uses' => 'App\Http\Controllers\v2\UserController@patchRefresh','as' => 'api.refresh']);
            $api->get('/all-users', ['uses' => 'App\Http\Controllers\v2\UserController@allUsers']);
        */
    });
});

$api->version('v1',['prefix' => 'v3'], function ($api) {
    $api->post('/register', ['uses' => 'App\Http\Controllers\v3\UserController@postCreateuser', 'as' => 'api.users']);
    $api->post('/guestregister', ['uses' => 'App\Http\Controllers\v3\UserController@postCreateguestuser', 'as' => 'api.guestusers']);
        $api->post('/login', ['uses' => 'App\Http\Controllers\v3\UserController@postLogin', 'as' => 'api.users']);
        $api->get('/countries', ['uses' => 'App\Http\Controllers\v3\UserController@allCountries', 'as' => 'api.countries']);
        $api->get('/countries/{id}/states', ['uses' => 'App\Http\Controllers\v3\UserController@allStates', 'as' => 'api.states']);
        $api->get('/countries/{country_id}/states/{state_id}/cities', ['uses' => 'App\Http\Controllers\v3\UserController@allCities', 'as' => 'api.cities']);
        $api->post('/storeLogFile', ['uses' => 'App\Http\Controllers\v3\IgniteappController@storeLogFile', 'as' => 'api.storeLogFile']);
        $api->post('/forgotPassword', ['uses' => 'App\Http\Controllers\v3\UserController@forgotPassword', 'as' => 'api.forgotPassword']);
        $api->post('/verifyOtp', ['uses' => 'App\Http\Controllers\v3\UserController@verifyOtp', 'as' => 'api.verifyOtp']);
        $api->post('/resendOtp', ['uses' => 'App\Http\Controllers\v3\UserController@resendOtp', 'as' => 'api.resendOtp']);
        $api->get('/versionUpdate', ['uses' => 'App\Http\Controllers\v3\IgniteappController@versionUpdate', 'as' => 'api.versionUpdate']);
        $api->get('/getAllStores', ['uses' => 'App\Http\Controllers\v3\IgniteappController@getAllStores', 'as' => 'api.getAllStores']);
        $api->post('/pushNotification', ['uses' => 'App\Http\Controllers\v3\UserController@pushNotification', 'as' => 'api.pushNotification']);
        $api->get('/zones', ['uses' => 'App\Http\Controllers\v3\UserController@zoneList', 'as' => 'api.zoneList']);
        $api->get('/commonConfiguration', ['uses' => 'App\Http\Controllers\v3\UserController@commonConfiguration', 'as' => 'api.commonConfiguration']);

    $api->group(['middleware' => 'api.auth'], function ($api) {
        $api->get('/getAllPerformanceData/{page}', ['uses' => 'App\Http\Controllers\v3\IgniteappController@getAllPerformanceData', 'as' => 'api.getAllPerformanceData']);
        $api->get('/Getsubjects', ['uses' => 'App\Http\Controllers\v3\IgniteappController@Getsubjects', 'as' => 'api.Getsubjects']);
        $api->post('/Getcategoriesforsubject', ['uses' => 'App\Http\Controllers\v3\IgniteappController@Getcategoriesforsubject', 'as' => 'api.Getcategoriesforsubject']);
        $api->post('/Getsubcategories', ['uses' => 'App\Http\Controllers\v3\IgniteappController@Getsubcategories', 'as' => 'api.Getsubcategories']);
        $api->get('/getCategoriesWithContents', ['uses' => 'App\Http\Controllers\v3\IgniteappController@getCategoriesWithContents', 'as' => 'api.getCategoriesWithContents']);
        $api->get('/mySubscriptionData', ['uses' => 'App\Http\Controllers\v3\IgniteappController@mySubscriptionData', 'as' => 'api.mySubscriptionData']);
        $api->get('/logout', ['uses' => 'App\Http\Controllers\v3\UserController@postLogout', 'as' => 'api.users']);
            $api->post('/users/me', ['uses' => 'App\Http\Controllers\v3\UserController@Updateuserprofile', 'as' => 'api.updateProfile']);
            $api->post('/users/guest', ['uses' => 'App\Http\Controllers\v3\UserController@Updateguestuserprofile', 'as' => 'api.updateguestProfile']);
            $api->patch('/users/me/password', ['uses' => 'App\Http\Controllers\v3\UserController@Changepassword', 'as' => 'api.changePassword']);
            $api->get('/contents/{content_id}/type/{content_type}', ['uses' => 'App\Http\Controllers\v3\IgniteappController@contentDetail', 'as' => 'api.contentDetail']);
            $api->get('/categories', ['uses' => 'App\Http\Controllers\v3\IgniteappController@allCategories', 'as' => 'api.categories']);
            $api->get('/categories/{category_id}', ['uses' => 'App\Http\Controllers\v3\IgniteappController@allContent', 'as' => 'api.contents']);
            $api->post('/pauseVideo', ['uses' => 'App\Http\Controllers\v3\IgniteappController@pauseVideo', 'as' => 'api.pauseVideo']);

            $api->post('/checkVideoViewHistory', ['uses' => 'App\Http\Controllers\v3\IgniteappController@checkVideoViewHistory', 'as' => 'api.checkVideoViewHistory']);

            $api->post('/updateVideoViewDetails', ['uses' => 'App\Http\Controllers\v3\IgniteappController@updateVideoViewDetails', 'as' => 'api.updateVideoViewDetails']);

            $api->get('/videos/{video_url_id}/downloaded', ['uses' => 'App\Http\Controllers\v3\IgniteappController@videoDownloaded', 'as' => 'api.videoDownloaded']);
            $api->get('/worksheets/{worksheet_id}/downloaded', ['uses' => 'App\Http\Controllers\v3\IgniteappController@worksheetDownloaded', 'as' => 'api.worksheetDownloaded']);
            $api->post('/endQuiz', ['uses' => 'App\Http\Controllers\v3\IgniteappController@endQuiz', 'as' => 'api.endQuiz']);
            $api->post('/payerId', ['uses' => 'App\Http\Controllers\v3\IgniteappController@getPayeridWechat', 'as' => 'api.payerId']);
            $api->post('/orderStatus', ['uses' => 'App\Http\Controllers\v3\IgniteappController@getPaymentOrderStatus', 'as' => 'api.orderStatus']);
            $api->get('/deletedVideos/{video_url_id}/deleted', ['uses' => 'App\Http\Controllers\v3\IgniteappController@getDownloadedDeletedVideoList', 'as' => 'api.getDownloadedDeletedVideoList']);
            $api->post('/stripPay', ['uses' => 'App\Http\Controllers\v3\IgniteappController@stripIntegrate', 'as' => 'api.stripIntegrate']);
            $api->post('/syncOfflineData', ['uses' => 'App\Http\Controllers\v3\IgniteappController@syncOfflineData', 'as' => 'api.syncOfflineData']);
            $api->post('/checkPromoCode', ['uses' => 'App\Http\Controllers\v3\IgniteappController@checkPromoCode', 'as' => 'api.checkPromoCode']);
            $api->post('/applePurchase', ['uses' => 'App\Http\Controllers\v3\IgniteappController@applePurchase', 'as' => 'api.applePurchase']);
            $api->get('/userSubscription', ['uses' => 'App\Http\Controllers\v3\IgniteappController@userSubscription', 'as' => 'api.userSubscription']);
            $api->post('/saveLanguage', ['uses' => 'App\Http\Controllers\v3\IgniteappController@saveLanguage', 'as' => 'api.saveLanguage']);
            $api->get('/removeLanguage', ['uses' => 'App\Http\Controllers\v3\IgniteappController@removeLanguage', 'as' => 'api.removeLanguage']);
            $api->get('/guestCountries', ['uses' => 'App\Http\Controllers\v3\IgniteappController@guestCountries', 'as' => 'api.guestcountries']);
            /* ##Not used
                $api->get('/generateClientToken', ['uses' => 'App\Http\Controllers\v3\IgniteappController@generateClientToken', 'as' => 'api.generateClientToken']);
                $api->post('/makePaymentThroughNonce', ['uses' => 'App\Http\Controllers\v3\IgniteappController@makePaymentThroughNonce', 'as' => 'api.makePaymentThroughNonce']);
                $api->get('/getDownloadedVideoList', ['uses' => 'App\Http\Controllers\v3\IgniteappController@getDownloadedVideoList', 'as' => 'api.getDownloadedVideoList']);
                $api->post('/storePayment', ['uses' => 'App\Http\Controllers\v3\IgniteappController@storePayment', 'as' => 'api.storePayment']);
                $api->post('/flagContent', ['uses' => 'App\Http\Controllers\v3\IgniteappController@flagContent', 'as' => 'api.flagContent']);
                $api->patch('/refresh', ['uses' => 'App\Http\Controllers\v3\UserController@patchRefresh','as' => 'api.refresh']);
                $api->get('/all-users', ['uses' => 'App\Http\Controllers\v3\UserController@allUsers']);
            */
        /* ##Not used
            $api->get('/sendotp', ['uses' => 'App\Http\Controllers\v3\IgniteappController@Sendsmsforverfication', 'as' => 'api.sendotp']); 
        */
    });
});
<?php

//###############################################################
//File Name : Distributors.php
//Author : Ketan Solanki <ketan@creolestudios.com>
//Purpose : Model file for the table `distributors`
//Date : 18th July 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class Distributors extends Model
{
    protected $table = 'distributors';
    public $rules    = array(
        'company_name'  => 'required',
        'email_address' => 'required',        
        'locality'      => 'required',
        'country_id'    => 'required',
        'state_id'      => 'required',
        'city_id'       => 'required',
        'contact'       => 'required',
    );
    public function distributor_team_members(){
        return $this->hasMany('App\DistributorsTeamMembers', 'distributor_id');
    }
    public function distributor_referrals(){
        return $this->hasManyThrough('App\DistributorsReferrals', 'App\DistributorsTeamMembers', 'distributor_id', 'distributor_team_member_id', 'id', 'id');
    }
    public function country(){
        return $this->belongsTo('App\Country', 'country_id')->select('id', 'name');
    }
    public function state(){
        return $this->belongsTo('App\State', 'state_id')->select('id', 'name');
    }
    public function city(){
        return $this->belongsTo('App\City', 'city_id')->select('id', 'name');
    }
}
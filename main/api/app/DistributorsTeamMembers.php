<?php

//###############################################################
//File Name : DistributorsTeamMembers.php
//Author : Ketan Solanki <ketan@creolestudios.com>
//Purpose : Model file for the table `distributor_team_members`
//Date : 19th July 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class DistributorsTeamMembers extends Model
{
    protected $table = 'distributor_team_members';

    protected $hidden = [
        'password', 'remember_token', 'reset_token'
    ];
    public $rules    = array(
        'first_name'     => 'required',
        'last_name'      => 'required',
        'email_address'  => 'required|email',
        'password'       => 'required',
        'distributor_id' => 'required',
    );

    public function distributor_referrals()
    {
        return $this->hasMany('App\DistributorsReferrals', 'distributor_team_member_id');
    }
    public function distributor_referrals_text()
    {
        return $this->hasMany('App\DistributorsReferrals', 'distributor_team_member_id')->where('referal_through',1);
    }
    public function distributor_referrals_barcode()
    {
        return $this->hasMany('App\DistributorsReferrals', 'distributor_team_member_id')->where('referal_through',2);
    }
    public function distributor_referrals_count()
    {
        return $this->hasMany('App\DistributorsReferrals', 'distributor_team_member_id');
    }
    public function distributor()
    {
        return $this->belongsTo('App\Distributors', 'distributor_id');
    }
    public function country(){
        return $this->belongsTo('App\Country', 'country_id')->select('id', 'name');
    }
    public function state(){
        return $this->belongsTo('App\State', 'state_id')->select('id', 'name');
    }
    public function city(){
        return $this->belongsTo('App\City', 'city_id')->select('id', 'name');
    }
    
}

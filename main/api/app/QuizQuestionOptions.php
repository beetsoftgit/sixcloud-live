<?php

//###############################################################
//File Name : QuizQuestionOptions.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : Related to quiz question options
//Date : 23rd Jul 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuizQuestionOptions extends Model
{
    protected $table = 'quiz_question_options';
    public $rules = array(
        'quiz_question_id' => 'required',
        'options_value'    => 'required',
        'options_type'     => 'required',
        'correct_answer'   => 'required',
    ); 
    public function quiz()
    {
        return $this->belongsTo('App\Quizzes', 'quiz_id');
    } 
    public function quiz_questions()
    {
        return $this->belongsTo('App\QuizQuestions', 'quiz_question_id');
    }    
}

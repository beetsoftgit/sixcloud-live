<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MultipleStore extends Model
{
    protected $table = 'multiple_store';
}

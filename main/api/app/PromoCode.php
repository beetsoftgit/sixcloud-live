<?php

//###############################################################
//File Name : PromoCode.php
//Author : Ketan Solanki <ketan@creolestudios.com>
//Purpose : Model file for the table `distributors`
//Date : 18th July 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class PromoCode extends Model
{
    protected $table = 'promo_codes';
    public $rules    = array(
        'promo_code'        => 'required',
        'remarks'           => 'required',
        'video_category_id' => 'required',
        'discount_type'     => 'required',
        'discount_of'       => 'required',
        'promo_type'        => 'required',
        'redemption_type'   => 'required',
    );
    public function category_detail(){
        return $this->hasOne('App\VideoCategory', 'id','video_category_id')->select('id','category_name','category_price');
    }
    public function distributor_detail(){
        return $this->hasOne('App\Distributors', 'id','distributor_id')->select('id','company_name');
    }
    public function category_details(){
        return $this->hasMany('App\VideoCategory', 'id','categories')->select('id','category_name');
    }
}
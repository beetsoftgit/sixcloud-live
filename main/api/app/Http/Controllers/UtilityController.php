<?php

/**
 * Short description for file
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
/*
 * Place includes controller for project module.
 */
/**
Pre-Load all necessary library & name space
 */

namespace App\Http\Controllers;

use Excel;
use File;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Schema;
use Validator;

// Load models

class UtilityController extends BaseController
{
    public function __construct()
    {
        //
    }

    //###############################################################
    //Function Name : Sendexceptionmail
    //Author : Six clouds
    //Purpose : To send the mail when any Exception is catched
    //In Params : Exception object only. (From controller)
    //Return : 'true' when email will be sent successfully.
    //###############################################################
    public static function Sendexceptionmail($object)
    {
        $viewFile = Config('constants.exception_mail.TEMPLATE');
        $fromEmail = Config('constants.exception_mail.FROM');
        $fromEmailName = Config('constants.exception_mail.APP');
        $recepients = Config('constants.exception_mail.TO');
        $mailSubject = Config('constants.exception_mail.SUBJECT');

        $dataArray = array();
        $dataArray['Error'] = $object->getMessage();
        $dataArray['File'] = $object->getFile();
        $dataArray['Line'] = $object->getLine();
        $dataArray['Source'] = isset($_SERVER['SYSTEM_NAME']) ? $_SERVER['SYSTEM_NAME'] : '';

        //Check Condition if not run from local server
        // if (strpos(url('/'), 'localhost') == true) {
            Mail::send($viewFile, $dataArray, function ($message) use ($dataArray, $viewFile, $fromEmail, $fromEmailName, $recepients, $mailSubject) {
                $message->from($fromEmail, $fromEmailName);
                $message->to($recepients);
                $message->subject($mailSubject);
            });
        // }
        return true;
    }

    //###############################################################
    //Function Name : Imageexist
    //Author : ketan Solanki <ketan@creolestudios.com>
    //Purpose : TO Check whether the image exists or not
    //In Params : File name and physical path and Url path and placeholder image path
    //Return : If Image exists then full path else placeholder image path
    //###############################################################
    public static function Imageexist($imageName, $path, $urlPath, $placeHolderPath = "")
    {
        $fileName = $urlPath . $imageName;
        $physicalFileName = $path . $imageName;

        // print_r($physicalFileName);die;
        if (file_exists($physicalFileName) && $imageName != '') {
            $fileName = $fileName;
        } else {
            $fileName = $placeHolderPath;
        }
        return $fileName;
    }

    //###############################################################
    //Function Name : Fileexist
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : TO Check whether the file exists or not
    //In Params : File name and physical path and Url path
    //Return : If File exists then full path else empty string
    //###############################################################
    public static function Fileexist($fileeName, $path, $urlPath)
    {
        $fileName = $urlPath . $fileeName;
        $physicalFileName = $path . $fileeName;
        if (file_exists($physicalFileName) && $fileeName != '') {
            $fileName = $fileName;
        } else {
            $fileName = "";
        }
        return $fileName;
    }

    //###############################################################
    //Function Name : Getmessage
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose :Common fuc to get contant message of messages array
    //In Params : Element of messages array
    //Return :boolean
    //Date : 7th December 2017
    //###############################################################
    public static function Getmessage($message)
    {
        return Config('constants.messages.' . $message);
    }

    //###############################################################
    //Function Name : Getpath
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose :Common function to get contant path of messages array
    //In Params : Element of path array
    //Return :boolean
    //Date : 11th January 2018
    //###############################################################
    public static function Getpath($path)
    {
        return Config('constants.paths.' . $path);
    }

    //###############################################################
    //Function Name : Setreturnvariables
    //Author : Six clouds
    //Purpose : To initialize the return variables
    //In Params : N/A
    //Return : Array of return response
    //###############################################################
    public static function Setreturnvariables()
    {        
        $returnData = array();        
        $returnData['status'] = Config('constants.standard_response_values.FAILURE');
        $returnData['message'] = Config('constants.messages.GENERAL_ERROR');
        $returnData['code'] = Config('constants.standard_response_values.STATUS_CODE_100');
        $returnData['data'] = array();

        return $returnData;
    }

    //###############################################################
    //Function Name : Generateresponse
    //Author : Six clouds
    //Purpose : To initialize the return variables
    //In Params : N/A
    //Return : Array of return response
    //###############################################################
    public static function Generateresponse($type, $message, $statusCode, $responseData = array())
    {
        
        $returnData = array();

        $returnData['status'] = ($type) ? Config('constants.standard_response_values.SUCCESS') : Config('constants.standard_response_values.FAILURE');

        if (Config('constants.messages.' . $message) != '') {
            $returnData['message'] = Config('constants.messages.' . $message);
        } else {
            $returnData['message'] = $message;
        }
        $returnData['code'] = $statusCode;
        $returnData['data'] = ($responseData != '') ? $responseData : array();

        return $returnData;
    }

    //###############################################################
    //Function Name : Setvalidationresponse
    //Author : Six clouds
    //Purpose : To initialize the return variables when validation exception occurs
    //In Params : N/A
    //Return : Array of return response
    //###############################################################
    public static function Setvalidationresponse($type)
    {
        $error = $type->original;
        foreach ($error as $k => $v) {
            $error[$k] = $v[0]->classes;
        }
        return $error;
    }

    //###############################################################
    //Function Name : ValidationRules
    //Author : Ashish Patel(ashish@creolestudios.com)
    //Purpose : to validate request fields
    //In Params : N/A
    //Return : Array of return response
    //###############################################################

    public static function ValidationRules($requests, $modelClassName, $unsets = '')
    {
        try {
            //  assigned default response
            $returnData = UtilityController::Setreturnvariables();
            if (!empty($modelClassName)) {
                if (empty($requests)) {
                    return UtilityController::Generateresponse(0, 'EMPTY_REQUEST', Response::HTTP_BAD_REQUEST, '');

                }
                $modelClassName = 'App\\' . $modelClassName; //  To make model object using string variable, use full reference of model class
                $$modelClassName = new $modelClassName;
                if ($$modelClassName != '') {
                    $rules = $$modelClassName->rules; // get rules from spacific model
                    if (empty($rules)) {
                        return UtilityController::Generateresponse(0, 'NO_RULES', Response::HTTP_BAD_REQUEST, '');
                    }
                    if (!empty($unsets) && is_array($unsets)) {
                        // if $unset is not empty than unset those fields from rule
                        /* unset rules which are not in use */
                        foreach ($unsets as $key => $value) {
                            unset($rules[$value]);
                        }
                    }
                    // validate request
                    $validator = Validator::make($requests, $rules);
                    if ($validator->fails()) {
                        $messages = implode("\n", $validator->messages()->all());
                        $returnData = UtilityController::Generateresponse(0, $messages, Response::HTTP_BAD_REQUEST, '');
                    } else {
                        $returnData = UtilityController::Generateresponse(1, 'EMPTY_MESSAGE', 200, '');
                    }
                }
            }

            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Makemodelobject
    //Author : Six clouds
    //Purpose : Generic function to create model object to save()
    //Return : model object
    //###############################################################
    public static function Makemodelobject($data, $modelClassName, $primaryKey = 'id', $idToEdit = '')
    {
        try {
            // print_r($data); echo "<br>";
            //  die;
            if (!empty($data) && $modelClassName != '') {
                $modelClassName = 'App\\' . $modelClassName; //  To make model object using string variable, use full reference of model class

                if ($idToEdit == '') {
                    //  If id is passed to edit the record. (Update function)
                    $$modelClassName = new $modelClassName;
                } else {
                    $$modelClassName = $modelClassName::find($idToEdit);
                }

                if ($$modelClassName != '') {
                    //  If object created then proceed.
                    // Getting all columns of the table.
                    $columns = Schema::getColumnListing($$modelClassName->getTable());

                    //  Looping through all the table columns to check all input values in $data variable.
                    foreach ($columns as $k => $v) {
                        if (isset($data[$v])) {
                            $$modelClassName->$v = $data[$v];
                        }

                    }

                    if ($$modelClassName->save()) {
                        return $$modelClassName;
                    } else {
                        return false;
                    }
                } else {
                    //  Else return false to controller.
                    return false;
                }
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : MakeArrayFromInput
    //Author : Six clouds
    //Purpose : Generic function to create array from input parameters
    //Return : model object
    //###############################################################
    public static function MakeObjectFromArray($data, $modelClassName)
    {
        try {
            if (isset($data) && !empty($data)) {
                $modelClassName = 'App\\' . $modelClassName; //  To make model object using string variable, use full reference of model class
                //define model with reference
                $$modelClassName = new $modelClassName;
                if ($$modelClassName != '') {
                    //  If object created then proceed.
                    // Getting all columns of the table.
                    $columns = Schema::getColumnListing($$modelClassName->getTable());
                    //  Looping through all the table columns to check all input values in $data variable.
                    foreach ($columns as $k => $v) {
                        if (isset($data[$v])) {
                            $$modelClassName->$v = $data[$v];
                        }

                    }
                    return $$modelClassName;
                } else {
                    //  Else return false to controller.
                    return false;
                }
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
            return $returnData;
        }
    }

    //###############################################################
    //Function Name : Changedateformat
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : To change the system date format to user friendly
    //In Params : Date which is to be converted
    //Return : Converted date
    //Date : 07th Dec 2017
    //###############################################################
    public static function Changedateformat($date, $format = "d M, H:i a")
    {
        $ConvertedDate = date($format, strtotime($date));
        return $ConvertedDate;
    }

    //###############################################################
    //Function Name : ELTVideoExists
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : TO Check whether the video exists or not
    //In Params : File name and physical path and Url path and placeholder image path
    //Return : If Image exists then full path else placeholder image path
    //###############################################################
    public static function ELTVideoExists($imageName, $path, $urlPath)
    {
        $fileName = url('') . $urlPath . $imageName;
        $physicalFileName = public_path('') . $path . $imageName;
        if (file_exists($physicalFileName) && $imageName != '') {
            $fileName = $fileName;
        } else {
            $fileName = '';
        }
        return $fileName;
    }

    //###############################################################
    //Function Name : ELTVideoExists
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : TO Check whether the video exists or not
    //In Params : File name and physical path and Url path and placeholder image path
    //Return : If Image exists then full path else placeholder image path
    //###############################################################
    public static function PaymentStatus($stautsNumber)
    {
        $statusValue = '';
        switch ($stautsNumber) {
            case 1:
                $statusValue = 'Successful';
                break;
            case 3:
                $statusValue = 'Failed';
                break;
        }
        return $statusValue;
    }
    //###############################################################
    //Function Name : getSignedUrlForGettingObject
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get Video Signed URL
    //In Params : OSS client object,bucket and file
    //Return : Signed URL
    //###############################################################
    public static function getSignedUrlForGettingObject($ossClient, $bucket, $file)
    {
        $object = $file;
        $timeout = 3600; //  The URL is valid for 3600 seconds
        try {
            $signedUrl = $ossClient->signUrl($bucket, $object, $timeout);
        } catch (OssException $e) {
            printf(__FUNCTION__ . ": FAILED\n");
            printf($e->getMessage() . "\n");
            return;
        }
        return $signedUrl;
    }
    //###############################################################
    //Function Name : slugify
    //Author : Komal Kapadi <komal@creolestudios.com>
    //Purpose : To get slug
    //In Params : Text for slug
    //Return : Slug
    //###############################################################
    public static function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

    //###############################################################
    //Function Name : Savearrayobject
    //Author : Ashish Patel <ashish@creolestudios.com>
    //Purpose : Save array and get objects in response
    //In Params : void
    //Date : 29th January 2018
    //Return: model object
    //###############################################################

    public static function Savearrayobject($data = array(), $modelClassName)
    {
        try {
            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    $idToEdit = isset($value['id']) && $value['id'] ? $value['id'] : '';
                    $returnData = UtilityController::Makemodelobject($value, $modelClassName, 'id', $idToEdit);
                }
            }

            return $returnData;
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            //Code to send mail ends here
            return response()->json(UtilityController::Setreturnvariables());
        }

    }

    //###############################################################
    //Function Name  : ExportData
    //Author         : Ketan Solanki <ketan@creolestudios.com>
    //Purpose        : To download the Excell file
    //In Params      : Data Array
    //Date           : 27th June 2018
    //###############################################################

    public static function ExportData($dataArray, $fileName, $title, $description, $type)
    {
        try {
            Excel::create($fileName, function ($excel) use ($fileName, $dataArray, $title, $description, $type) {
                $excel->setTitle($title);
                $excel->setCreator('Six-Clouds');
                $excel->setDescription($description);
                $excel->sheet('sheet1', function ($sheet) use ($dataArray) {
                    $sheet->fromArray($dataArray, null, 'A1', false, false);
                });
            })->download($type);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
    //###############################################################
    //Function Name  : GenerateRunningNumber
    //Author         : Ketan Solanki <ketan@creolestudios.com>
    //Purpose        : To generate the Running Number
    //In Params      : Data Array
    //Date           : 24th July 2018
    //###############################################################

    public static function GenerateRunningNumber($modelName,$columnName,$prefix = '')
    {
        try {             
            $modelClassName = 'App\\' . $modelName;
            $lastRecord =   $modelClassName::orderBy('id', 'desc')->first();                     
            if (!empty($lastRecord)) {
                $str = $lastRecord[$columnName];
            } else {
                $str = "";
            }             
            if ($str == '') {
                $num = sprintf("%04d", 1);
                $char = 'A';
                $projectRunningNumber = $prefix.date('y') . date('m') . $num . $char;
            } else {
                $num = substr($str, -5, -1);
                $num = $num + 1;
                $num = sprintf("%04d", $num);
                $char = substr($str, -1);
                if ($num > 9999) {
                    $num = sprintf("%04d", 1);
                    $char++;
                }
                $projectRunningNumber = $prefix.date('y') . date('m') . $num . $char;
            }
            return $projectRunningNumber;
        } catch (Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            return $returnData;
        }
    }
}

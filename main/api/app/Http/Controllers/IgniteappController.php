<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\App as App;
use App\User;
use App\Country;
use App\State;
use App\City;
use App\VideoCategory;
use App\MarketPlacePayment;
use App\VersionControl;
use App\DistributorsReferrals;
use App\PromoCode;
use App\Distributors;
use App\DistributorsTeamMembers;
use App\IgniteCommissions;


use App\IgnitePrerequisite;
use App\Quizzes;
use App\VideoLanguageUrl;
use App\VideoViews;
use App\Videos;
use App\QuizQuestionOptions;
use App\QuizQuestions;
use App\Worksheets;
use App\QuizAttempts;
use App\FlagContents;
use App\WorksheetDownloads;
use App\AndroidLogFile;
use DB;
// use \Barryvdh\DomPDF\PDF;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\carbon;
use App\DeviceTokens;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\UtilityController;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;


##for sms gatway start
use Qcloud\Sms\SmsSingleSender;
use Qcloud\Sms\SmsMultiSender;
use Qcloud\Sms\SmsVoiceVerifyCodeSender;
use Qcloud\Sms\SmsVoicePromptSender;
use Qcloud\Sms\SmsStatusPuller;
use Qcloud\Sms\SmsMobileStatusPuller;

use Qcloud\Sms\VoiceFileUploader;
use Qcloud\Sms\FileVoiceSender;
use Qcloud\Sms\TtsVoiceSender;
use Braintree_Gateway;
##for sms gatway end

##strip
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;

use URL;


class IgniteappController extends Controller
{    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
         
    }   
    
    //###############################################################
    //Function Name: allCities
    //Author:        Nivedita <nivedita@creolestudios.com>
    //Purpose:       To get all cities list
    //In Params:     state id
    //Return:        json
    //Date:          23rd August 2018
    //###############################################################
    public function allCategories(Request $request){
        $inputData = $request->all();
        $userAuth = JWTAuth::parseToken()->authenticate();
        $userId = $userAuth->id;
        $paymentsDoneFor = MarketPlacePayment::select('video_category_id')->where('paid_by',$userId)->where('paid_for',3)->where('subscription_end_date','>=',Carbon::now())->where('payment_status',1)->pluck('video_category_id');
        if(!empty($paymentsDoneFor)){
            $paidCategoryId = '';
            foreach ($paymentsDoneFor as $key => $value)
                $paidCategoryId = $paidCategoryId.','.$value;
            $paidCategoryId = array_unique(array_filter(explode(',', $paidCategoryId)));
        }
        if($request->server('HTTP_ACCEPT_LANGUAGE')=='en')
            $data = VideoCategory::select('id','category_name','category_price','total_video_count','total_video_duration','status','promotions','promotional_price', DB::Raw('description AS description'))->get();
        else
            $data = VideoCategory::select('id','category_name','category_price','total_video_count','total_video_duration','status','promotions','promotional_price', DB::Raw('description_chi AS description'))->get();
        foreach ($data as $key => $value) {
            $value['purchaseStatus'] = (in_array($value['id'], $paidCategoryId)?$purchaseStatus = 1:0);
        }
        return response()->json($data);
    }


    //###############################################################
    //Function Name: allContent
    //Author:        Nivedita <nivedita@creolestudios.com>
    //Purpose:       To get all content(videos,quizes and worksheet) list
    //In Params:     category id
    //Return:        json
    //Date:          24th August 2018
    //###############################################################
    public function allContent(Request $request, $category_id = ''){           
        try {
            $categoryId = 0;
            if(isset($category_id) && is_numeric($category_id) && $category_id > 0){
                $categoryId = $category_id;
                $userAuth = JWTAuth::parseToken()->authenticate();
                $userId = $userAuth->id;
                
                $paymentsDoneFor = MarketPlacePayment::select('video_category_id')->where('paid_by',$userId)->where('paid_for',3)->where('subscription_end_date','>=',Carbon::now())->where('payment_status',1)->pluck('video_category_id');
                if(!empty($paymentsDoneFor)){
                    $paidCategoryId = '';
                    foreach ($paymentsDoneFor as $key => $value)
                        $paidCategoryId = $paidCategoryId.','.$value;
                    $paidCategoryId = array_unique(array_filter(explode(',', $paidCategoryId)));
                }
                //$videoStatus = (in_array($categoryId, $paidCategoryId)?$videoStatus = array(1,2):array(2));
                $videoStatus = (in_array($categoryId, $paidCategoryId)?$videoStatus = array(1,2):array(1,2));

                ##1=>purchased the category, 2-=> not purchased
                $purchaseStatus = (in_array($categoryId, $paidCategoryId)?$purchaseStatus = 1:2);
                
                $allContentData = VideoCategory::select("id", "category_name")
                    ->with([
                        'videos' => function ($query) use($userId,$videoStatus) {
                            $query->select("id","title", "description","video_status", "video_category_id", "video_ordering as Order", "slug", DB::Raw('IF(slug != "", "1", "1") AS content_type'), 'thumbnail_160_90', 'thumbnail_128_72','thumbnail_web')
                            ->with([
                                    'video_url' =>function($query) use($userId,$videoStatus){
                                        $query->select('id','video_id','video_duration','video_language','video_oss_url', 'video_oss_url_mp4')->with([
                                            'views_detail' => function($query) use($userId){
                                                $query->select('id','video_id','video_url_id','video_viewed_duration AS resume_from','video_viewing_app_status AS view_status', 'video_app_download','video_viewing_app_status_permanent')->where('user_id',$userId);
                                            }
                                        ]);
                                    }
                                ])
                            ->where("status",1)
                            ->whereIn('video_status',$videoStatus)
                            ->get();
                        }, 'quizzes' => function ($query) use($videoStatus,$userId){
                            $query->select("id","title", "description","quiz_status", "category_id", "quiz_ordering as Order", "slug", DB::Raw('IF(slug != "", "2", "2") AS content_type'))->with([
                                'quiz_attempt' => function($query) use($userId){
                                    $query->where('user_id',$userId)->where('quiz_completed_status',1)->exists();
                                }
                            ])
                            ->where("quiz_published_status",2)
                            ->where("status",1)
                            ->withCount('quizquestions')
                            ->whereIn('quiz_status',$videoStatus)
                            ->get();
                        },'worksheets' => function ($query) use($userId){
                            $query->select("id","title","description", "worksheet_status", "worksheet_category_id", "worksheet_ordering as Order", "slug", DB::Raw('IF(slug != "", "3", "3") AS content_type'), 'file_name','original_file_name')->with([
                                        'worksheet_downloads' => function($query) use($userId){
                                            $query->select('*')->where('user_id',$userId);
                                        },
                                        'category' => function($query) {
                                            $query->select('id','category_name');   
                                        }
                                    ])
                            ->where("status",1)
                            ->get();
                        },
                    ])
                    ->where('id',$categoryId)
                    ->first();
                if (!empty($allContentData)) {
                    $allContentData->videos = $allContentData->videos->map(function($value, $key){
                        if(!empty($value->video_url)){

                            $value->video_url->video_duration = explode(":", $value->video_url->video_duration);
                            $value->video_url->video_duration = ($value->video_url->video_duration[0]==00)?$value->video_url->video_duration[1].':'.$value->video_url->video_duration[2]:$value->video_url->video_duration[0].':'.$value->video_url->video_duration[1].':'.$value->video_url->video_duration[2];
                        }
                        return $value;
                    });
                    $globalVariable = UserController::Globalvariables();
                    $allContentData->videos = $allContentData->videos->map(function($value, $key) use($globalVariable){
                        if($globalVariable['CURRENT_DOMAIN']=='net'){
                            $value->thumbnail_160_90 = isset($value->thumbnail_160_90)&&$value->thumbnail_160_90!=''?UtilityController::Getpath('VIDEO_THUMBNAIL_MOBILE_160_90_CDN').$value->thumbnail_160_90:UtilityController::getMessage('NO_IMAGE_URL');
                            $value->thumbnail_128_72 = isset($value->thumbnail_128_72)&&$value->thumbnail_128_72!=''?UtilityController::Getpath('VIDEO_THUMBNAIL_MOBILE_128_72_CDN').$value->thumbnail_128_72:UtilityController::getMessage('NO_IMAGE_URL');
                        } else {
                            $value->thumbnail_160_90 = isset($value->thumbnail_160_90)&&$value->thumbnail_160_90!=''?UtilityController::Getpath('VIDEO_THUMBNAIL_MOBILE_160_90_URL').$value->thumbnail_160_90:UtilityController::getMessage('NO_IMAGE_URL');
                            $value->thumbnail_128_72 = isset($value->thumbnail_128_72)&&$value->thumbnail_128_72!=''?UtilityController::Getpath('VIDEO_THUMBNAIL_MOBILE_128_72_URL').$value->thumbnail_128_72:UtilityController::getMessage('NO_IMAGE_URL');
                        }
                        return $value;
                    });
                    $allContentData->worksheets = $allContentData->worksheets->map(function($value, $key) use($globalVariable){
                        $value->original_file_name = $value->category->category_name.' - '.$value->title;
                        unset($value->category);
                        return $value;
                    });
                    $allContentData->purchaseStatus = $purchaseStatus;
                    $allContentData->quizzes_count    = count($allContentData->quizzes);
                    $allContentData->videos_count     = count($allContentData->videos);
                    $allContentData->worksheets_count = count($allContentData->worksheets);
                    $mergedArray = (object) array_merge($allContentData->videos->toArray(), $allContentData->quizzes->toArray(), $allContentData->worksheets->toArray());
                    $sortedArray = collect($mergedArray)->sortBy('Order')->values(); //To sort all the array based on the Order specified
                    $allContentData->allData = $sortedArray;                    
                    unset($allContentData->quizzes);
                    unset($allContentData->videos);
                    unset($allContentData->worksheets);
                    $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', Response::HTTP_OK, $allContentData);
                } else {
                    $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
                }
            }else {
                $returnData = UtilityController::Generateresponse(0, 'GENERAL_ERROR', Response::HTTP_BAD_REQUEST, '', '');
            }
        } 
        catch (Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
            //return $returnData;
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: contentDetail
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To get content detail (videos,quizes and worksheet)
    //In Params:     content id, content type
    //Return:        json
    //Date:          28th August 2018
    //###############################################################
    public function contentDetail(Request $request, $content_id ='', $content_type = ''){       
        try {
           
            if(isset($content_id) && is_numeric($content_id) && $content_id > 0){
                $id = $content_id;
                if(isset($content_type) && ($content_type=='1'||$content_type=='2'||$content_type=='3')){
                    $contentType = $content_type;
                }
                $userAuth = JWTAuth::parseToken()->authenticate();
                $userId = $userAuth->id;
                if($contentType=="1"){
                    repeat:
                    $detailData = Videos::select('id','title','description','thumbnail_160_90','thumbnail_128_72')
                        ->with([
                            'video_urls' => function ($query) use($userId) {
                                $query->select("id", "video_id","video_oss_url", "video_oss_url_mp4", "video_duration", "video_language AS language")->with([
                                        'views_detail' => function($query) use($userId){
                                            $query->select('id','video_id','video_url_id','video_viewed_duration AS resume_from','video_app_download','video_viewing_app_status AS view_status','video_viewing_app_status_permanent')->where('user_id',$userId);
                                        }
                                    ])->get();
                            }
                        ])
                        ->where('id',$id)              
                        ->first();
                    $globalVariable = UserController::Globalvariables();
                    if($globalVariable['CURRENT_DOMAIN']=='net'){
                        $detailData->thumbnail_160_90 = isset($detailData->thumbnail_160_90)&&$detailData->thumbnail_160_90!=''?UtilityController::Getpath('VIDEO_THUMBNAIL_MOBILE_160_90_CDN').$detailData->thumbnail_160_90:UtilityController::getMessage('NO_IMAGE_URL');
                        $detailData->thumbnail_128_72 = isset($detailData->thumbnail_128_72)&&$detailData->thumbnail_128_72!=''?UtilityController::Getpath('VIDEO_THUMBNAIL_MOBILE_128_72_CDN').$detailData->thumbnail_128_72:UtilityController::getMessage('NO_IMAGE_URL');
                    } else {
                        $detailData->thumbnail_160_90 = isset($detailData->thumbnail_160_90)&&$detailData->thumbnail_160_90!=''?UtilityController::Getpath('VIDEO_THUMBNAIL_MOBILE_160_90_URL').$detailData->thumbnail_160_90:UtilityController::getMessage('NO_IMAGE_URL');
                        $detailData->thumbnail_128_72 = isset($detailData->thumbnail_128_72)&&$detailData->thumbnail_128_72!=''?UtilityController::Getpath('VIDEO_THUMBNAIL_MOBILE_128_72_URL').$detailData->thumbnail_128_72:UtilityController::getMessage('NO_IMAGE_URL');
                    }
                    
                    $userAuth = JWTAuth::parseToken()->authenticate();
                    $userId = $userAuth->id;
                    /*change this code from static 0 to dynamic*/
                    $videoUrlId = $detailData->video_urls->toArray();
                    $videoUrlId = $videoUrlId[0]['id']; 
                    /*change this code from static 0 to dynamic*/
                    $videoView = VideoViews::where('user_id',$userId)->where('video_id',$id)->where('video_url_id',$videoUrlId)->exists();
                    
                    if(!$videoView){
                        $insertVideoView['video_id']     = $id;
                        $insertVideoView['video_url_id'] = $videoUrlId;
                        $insertVideoView['user_id']      = $userId;
                        $insertVideoViewResult           = UtilityController::Makemodelobject($insertVideoView,'VideoViews');
                        goto repeat;
                    }
                }
                if($contentType=="2"){
                    
                    $detailData = Quizzes::select('id','title','description','quiz_status','attempts_per_user')->with('quiz_attempt')
                        ->with([
                            'quizquestions' => function ($query) {
                                $query->select("id", "quiz_id","question_title", "question_image", "question_audio")->with([
                                    'quizquestionsoptions' => function ($query) {
                                        $query->select("id", "quiz_question_id","options_value AS answer", "options_type AS answer_type", DB::Raw('IF(correct_answer = 1, 1, 0) AS correct_answer'))->get();
                                    }
                                ])->withCount('quizquestionscorrectoptions')->orderBy('question_order','ASC')->get();
                            }
                        ])
                        ->where('id',$id)              
                        ->first();
                    $detailData->quizquestions = $detailData->quizquestions->map(function($value, $key) {
                        if(isset($value->question_image) && $value->question_image != '')
                            $value->question_image = UtilityController::Getpath('QUIZ_OPTION_UPLOAD_URL').$value->question_image;
                        /*else
                            $value->question_image = UtilityController::Getpath('NO_IMAGE_URL');*/
                        if(isset($value->question_audio) && $value->question_audio != '')
                            $value->question_audio = UtilityController::Getpath('QUIZ_OPTION_UPLOAD_URL').$value->question_audio;
                        /*else
                            $value->question_audio = UtilityController::Getpath('NO_IMAGE_URL');*/
                        $value->quizquestionsoptions = $value->quizquestionsoptions->map(function($valueInner, $keyInner){
                            if($valueInner->answer_type==2||$valueInner->answer_type==3){
                                if(isset($valueInner->answer) && $valueInner->answer != '')
                                    $valueInner->answer = UtilityController::Getpath('QUIZ_OPTION_UPLOAD_URL').$valueInner->answer;
                                /*else
                                    $valueInner->answer = UtilityController::Getpath('NO_IMAGE_URL');*/
                            }
                            return $valueInner;
                        });
                        return $value;
                    });
                    $alreadyAttempted = QuizAttempts::select('no_of_attempt')->where('quiz_id',$id)->where('user_id',$userId)->where('quiz_completed_status',1)->orderBy('id','DESC')->first();
                    if(!empty($alreadyAttempted))
                        $detailData->no_of_attempt = $alreadyAttempted->no_of_attempt+1;
                    else
                        $detailData->no_of_attempt = 1;
                }
                if($contentType=="3"){
                    $detailData = Worksheets::select('id','worksheet_category_id','title','description', 'worksheet_status','file_name','original_file_name')
                        ->with([
                            'worksheet_downloads' => function($query) use($userId){
                                $query->select('*')->where('user_id',$userId);
                            },
                            'category' => function($query) {
                                $query->select('id','category_name');   
                            }
                        ])
                        ->where('id',$id)
                        ->first();
                    $detailData->original_file_name = $detailData->category->category_name.' - '.$detailData->title;
                    if(isset($detailData->file_name))
                        $detailData->file_name = UtilityController::Getpath('WORKSHEET_FILE_UPLOAD_URL').$detailData->file_name;
                    else
                        $detailData->file_name = UtilityController::Getpath('NO_IMAGE_URL');
                    unset($detailData->category);
                }
                if(!empty($detailData)) {
                    $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 200, $detailData);
                } else {
                    $returnData = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');
                }
            }else {
                $returnData = UtilityController::Generateresponse(0, 'GENERAL_ERROR', Response::HTTP_BAD_REQUEST, '', '');
            }
        } catch (Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: pauseVideo
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To store the duration at which the video was paused and from where to start again
    //In Params:     video id(video_language_url's table id), duration
    //Return:        json
    //Date:          28th August 2018
    //###############################################################
    public function pauseVideo(Request $request){       
        try {
            $Input = $request->all();
            $id = 0;
            $userAuth = JWTAuth::parseToken()->authenticate();
            $userId = $userAuth->id;
            if(isset($Input['id']) && is_numeric($Input['id']) && $Input['id'] > 0){
                $id = $Input['id'];
            }
            $data    = VideoLanguageUrl::where('id',$id)->first();
            $dataGet = VideoLanguageUrl::where('id',$id)->first();

            if($Input['duration']<$data['video_duration']&&$Input['duration']!='00:00:00'&&$Input['video_status']==0){
                $ifResumeExists = VideoViews::where('user_id',$userId)->where('video_url_id',$id)->orderBy('id','DESC')->first();
                if($ifResumeExists['video_viewing_app_status']==2){
                    $resume['video_viewing_app_status'] = 2; ##resume video
                    $resume['video_viewed_duration']    = $Input['duration'];
                    $resumeResult                       = VideoViews::where('user_id',$userId)->where('video_url_id',$id)->orderBy('id','DESC')->limit(1)->update($resume);
                } else {
                    $resume['video_id']                           = $dataGet['video_id'];
                    $resume['video_url_id']                       = $id;
                    $resume['user_id']                            = $userId;
                    $resume['video_viewed_duration']              = $Input['duration'];
                    $resume['video_viewing_app_status']           = 2; ##resume video
                    $resume['video_viewing_app_status_permanent'] = $dataGet['video_viewing_app_status_permanent'];
                    $resumeResult                                 = UtilityController::Makemodelobject($resume,'VideoViews');
                }
            }
            if($Input['duration']!='00:00:00'&&$Input['video_status']==1){
                $resume['video_id']                           = $dataGet['video_id'];
                $resume['video_url_id']                       = $id;
                $resume['user_id']                            = $userId;
                $resume['video_viewed_duration']              = '00:00:00';
                $resume['video_viewing_app_status']           = 1; ##watched completely
                $resume['video_viewing_app_status_permanent'] = 1;
                $resumeResult                                 = UtilityController::Makemodelobject($resume,'VideoViews');
            }
            if(!empty($resumeResult)) {
                $returnData = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 200, $resume['video_viewing_app_status']);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 400, '');
            }
        } catch (Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: videoDownloaded
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To change the video status from not-downloaded to downloaded
    //In Params:     video id(video_language_url's table id)
    //Return:        json
    //Date:          28th August 2018
    //###############################################################
    public function videoDownloaded(Request $request, $video_url_id){
        try {
            // $Input = $request->all();
            // $id = 0;
            if(isset($video_url_id) && is_numeric($video_url_id) && $video_url_id > 0){
                $id = $video_url_id;
                $userAuth = JWTAuth::parseToken()->authenticate();
                $userId = $userAuth->id;
                $download['video_app_download'] = 1;
                $download['video_deleted_app_status'] = 0;
                //$download['imei'] = $imei;
                $downloadResult = VideoViews::where('user_id',$userId)->where('video_url_id',$id)->update($download);
                if(!empty($downloadResult)) {
                    $returnData = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 200, $download['video_app_download']);
                } else {
                    $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 400, '');
                }
            }else {
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 400, '');
            }
        } catch (Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: endQuiz
    //Author:        Nivedita <nivedita@creolestudios.com>
    //Purpose:       To store the data when ends the quiz from app
    //In Params:     
    //Return:        json
    //Date:          5th Sept 2018
    //###############################################################
    public function endQuiz(Request $request){
        try {
            $Input    = $request->all();
            $userAuth = JWTAuth::parseToken()->authenticate();
            $userId   = $userAuth->id;
            
            if(isset($Input['quizattempts']) && count($Input['quizattempts']) > 0){
                
                $totalCount = count($Input['quizattempts'])-1;
                foreach ($Input['quizattempts'] as $key => $value) {
                    $Input['quizattempts'][$key]['user_id'] = $userId;
                    $Input['quizattempts'][$key]['score_per_attempt']             = $Input['quizattempts'][$totalCount]['score_per_attempt'];
                }
                $returnData['quiz_ended'] = 1;
                $returnData = UtilityController::Savearrayobject($Input['quizattempts'],'QuizAttempts');
                
            }
            if(!empty($returnData)) {
                $returnData = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 200, $returnData['quiz_ended']);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 400, '');
            }
        } catch (Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getMessage().': '.$e->getLine(), '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: flagContent
    //Author:        Nivedita <nivedita@creolestudios.com>
    //Purpose:       To flag the content from app
    //In Params:     
    //Return:        json
    //Date:          7th Sept 2018
    //###############################################################
    public function flagContent(Request $request){
        try {
            $input            = $request->all();
            $userAuth         = JWTAuth::parseToken()->authenticate();
            $input['user_id'] = $userAuth->id;
           
            if(isset($input)){
                $rule = array(
                'reason'    => 'required'
                );
                $validator = \Validator::make($input, $rule); // Execute validation.                                    
                if ($validator->fails()) {
                    $returnData = UtilityController::Generateresponse(0, 'VALIDATION_ERROR', Response::HTTP_BAD_REQUEST, $validator->messages()); 
                    $returnData['content_flaged'] = 1;               
                } else {
                    $returnData = UtilityController::Makemodelobject($input,'FlagContents','');
                }
                
            }
            if(!empty($returnData)) {
                $returnData = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 200, $returnData['content_flaged']);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 400, '');
            }
        } catch (Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getMessage().': '.$e->getLine(), '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: GetallPerformanceData
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To get subscribers's performace page data
    //In Params:     
    //Return:        json
    //Date:          10th Sept 2018
    //###############################################################
    public function getAllPerformanceData(Request $request, $page){     
        try {
                $userAuth              = JWTAuth::parseToken()->authenticate();
                $userId                = $userAuth->id;
                $data['quizCount']     = QuizAttempts::where('user_id',$userId)->where('quiz_completed_status',1)->distinct()->groupBy('quiz_id')->pluck('quiz_id');
                $data['attemptedQuiz'] = Quizzes::select('id','category_id','title','slug')->withCount('quizquestions','total_questions')->with([
                    'quiz_attempt'=>function($query) use($userId){
                        $query->select('user_id','quiz_id','created_at')->where('user_id',$userId)->orderBy('created_at','Desc');
                    },
                    'quiz_attempt_highest'=>function($query) use($userId){
                        $query->select('quiz_id','no_of_attempt','score_per_attempt')
                        ->where('user_id',$userId)
                        ->orderBy('created_at','Desc');
                    }
                ])->whereIn('id',$data['quizCount'])->orderBy('id','Desc')->get();

                $data['videoCount']          = VideoViews::where('user_id',$userId)->where('video_viewing_app_status',1)->distinct()->groupBy('video_id')->get()->count();
                /*$data['viewedVideos']        = VideoViews::select('id','video_id','video_url_id','user_id','updated_at')->with('video_url')->with([
                    'video_details'=>function($query){
                        $query->select('id','video_category_id','title','slug','thumbnail_128_72');
                    }
                ])->where('user_id',$userId)->where('video_viewing_app_status',1)->distinct()->groupBy('video_id')->orderBy('updated_at','Desc')->get();*/
                $limit = 10;
                $data['viewedVideos']        = VideoViews::select('id','video_id','video_url_id','user_id','updated_at')->with('video_url')->with([
                    'video_details'=>function($query){
                        $query->select('id','video_category_id','title','slug','thumbnail_128_72');
                    }
                ])->where('user_id',$userId)->where('video_viewing_app_status',1)->orderBy('updated_at','Desc')
                ->offset($page * $limit)
                ->take($limit)
                ->get();
                /* optimize this : start */
                $page = $page+1;
                $viewedVideosNext        = VideoViews::select('id','video_id','video_url_id','user_id','updated_at')->with('video_url')->with([
                    'video_details'=>function($query){
                        $query->select('id','video_category_id','title','slug','thumbnail_128_72');
                    }
                ])->where('user_id',$userId)->where('video_viewing_app_status',1)->orderBy('updated_at','Desc')
                ->offset($page * $limit)
                ->take($limit)
                ->get();
                /* optimize this : end */
                if($viewedVideosNext->count()==0)
                    $data['moreVideos'] = 0;
                else
                    $data['moreVideos'] = 1;
                $data['totalVideoQuizCount'] = $data['videoCount'] + $data['quizCount']->count();
                $data['quizCount']           = $data['quizCount']->count();
                $data['quizResultArray'] = 0;
                $data['attemptedQuiz']       = $data['attemptedQuiz']->map(function($value, $key) use($data){
                    if(Carbon::now()->diffInHours($value->quiz_attempt->created_at) > 24){  
                        $value->attempted_at = Carbon::now()->diffInHours($value->quiz_attempt->created_at);
                        $value->attempted_at = Carbon::now()->subHours($value->attempted_at)->diffForHumans();
                    } else {
                        $value->attempted_at = Carbon::now()->diffInMinutes($value->quiz_attempt->created_at);
                        $value->attempted_at = Carbon::now()->subMinutes($value->attempted_at)->diffForHumans();
                    }
                    $value->correct_answers_count = 0;
                    $value->total_attempts_count = 0;
                    $value->forSort = $value->quiz_attempt->created_at;
                    $value->quiz_attempt_highest = $value->quiz_attempt_highest->toArray();
                    foreach ($value->quiz_attempt_highest as $keyInner => $valueInner) {
                        if($valueInner['score_per_attempt']>$value->correct_answers_count)
                            $value->correct_answers_count = $valueInner['score_per_attempt'];
                        if($valueInner['no_of_attempt']>$value->total_attempts_count)
                            $value->total_attempts_count = $valueInner['no_of_attempt'];
                    }
                    unset($value->quiz_attempt_highest);
                    $quizResult = ($value->correct_answers_count/$value->quizquestions_count)*100;
                    $value['quizResultArray'] = $quizResult;
                    return $value;
                });
                $data['attemptedQuiz'] = $data['attemptedQuiz']->sortByDesc('forSort')->values();
                $data['attemptedQuiz']       = $data['attemptedQuiz']->map(function($value, $key) use($data){
                    unset($value->forSort);
                    return $value;
                });
                $averagePercentage = $data['attemptedQuiz'];
                $data['quizResultArray'] = $averagePercentage->map(function($value, $key) use($data){
                    $data['quizResultArray'] += $value->quizResultArray;
                    return $data['quizResultArray'];
                });
                
                if(array_sum($data['quizResultArray']->toArray())>0)
                    $data['averageQuizPerformance'] = array_sum($data['quizResultArray']->toArray())/$data['quizCount'];
                else
                    $data['averageQuizPerformance'] = 0;
                unset($data['quizResultArray']);
                $globalVariable = UserController::Globalvariables();
                $data['viewedVideos'] = $data['viewedVideos']->map(function($value, $key) use($globalVariable){
                    if(Carbon::now()->diffInHours($value->updated_at) > 24){  
                        $value->viewed_at = Carbon::now()->diffInHours($value->updated_at);
                        $value->viewed_at = Carbon::now()->subHours($value->viewed_at)->diffForHumans();
                    } else {
                        $value->viewed_at = Carbon::now()->diffInMinutes($value->updated_at);
                        $value->viewed_at = Carbon::now()->subMinutes($value->viewed_at)->diffForHumans();
                    }
                    if($globalVariable['CURRENT_DOMAIN']=='net')
                        $value->thumbnail_128_72 = (isset($value->video_details->thumbnail_128_72)&&$value->video_details->thumbnail_128_72!='')?UtilityController::Getpath('VIDEO_THUMBNAIL_MOBILE_128_72_CDN').$value->video_details->thumbnail_128_72:UtilityController::getMessage('NO_IMAGE_URL');
                    else
                        $value->thumbnail_128_72 = (isset($value->video_details->thumbnail_128_72)&&$value->video_details->thumbnail_128_72!='')?UtilityController::Getpath('VIDEO_THUMBNAIL_MOBILE_128_72_URL').$value->video_details->thumbnail_128_72:UtilityController::getMessage('NO_IMAGE_URL');
                    return $value;
                });
                $data['viewedVideos'] = $data['viewedVideos']->map(function($value, $key){
                    $value->video_details->thumbnail_128_72 = $value->thumbnail_128_72;
                    unset($value->thumbnail_128_72);
                    return $value;
                });
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $data);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getFile().'-:-'.$e->getLine().'-:-'.$e->getMessage(), '', '');
        }            
        return response($returnData);
    }

    //###############################################################

    //Function Name: Sendsmsforverfication
    //Author:        Nivedita <nivedita@creolestudios.com>
    //Purpose:       to send the otp for phone verification
    //In Params:     
    //Return:        json
    //Date:          11th Sept 2018
    //###############################################################
    /*public function Sendsmsforverfication(Request $request){

        try {
            $Input = $request->all();
            if(isset($Input['phonecode']) && isset($Input['email']) && isset($Input['phone'])){
                ## SMS application SDK AppID
                $appid = UtilityController::Getmessage('APP_ID_SMS'); // Starting with 1400
                ## SMS application SDK AppKey
                $appkey = UtilityController::Getmessage('APP_KEY_SMS');
                ## SMS template ID, you need to apply in the SMS application
                $templateId = UtilityController::Getmessage('APP_TEMPLATE_ID_SMS');
                ## signature
                $smsSign = "重庆六云教育";
                ## Mobile number that needs to send a text message
                $phoneNumbers = ["18883708501"];


                // Single text message
                $ssender    = new SmsSingleSender($appid, $appkey);
                $params     = ["5678", "1"];
                $returnData = $ssender->sendWithParam("86", $phoneNumbers[0], $templateId,
                    $params, $smsSign, "", "");  // When the signature parameter is not provided or is empty, the SMS will be sent with the default signature.
            }else {
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 400, '');
            }
        } catch (Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }*/
    
    //###############################################################
    //Function Name: storePayment
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To store the payment details
    //In Params:     payment details data
    //Return:        json
    //Date:          12th Sept, 2018 
    //###############################################################
    public function storePayment(Request $request){     
        try {
            $Input             = Input::all();
            $userAuth = JWTAuth::parseToken()->authenticate();
            $userId   = $userAuth->id;

            $Input['paid_by']  = $userId;
            $Input['paid_for'] = 3;
            $Input['system_transaction_number'] = UtilityController::GenerateRunningNumber('MarketPlacePayment','system_transaction_number',"VL");
            \DB::beginTransaction();
                $paymentResult = UtilityController::Makemodelobject($Input,'MarketPlacePayment');
                if(!empty($paymentResult)){
                    $Input['video_category_id'] = explode(',', $Input['video_category_id']);
                    $category_name = VideoCategory::whereIn('id',[2,3])->get()->pluck('category_name');
                    // $category_name = $category_name['category_name'];
                    // print_r($category_name->toArray()); die;
                    $info['generateReceipt'] = self::Receiptattachement($paymentResult['payment_amount'],$userAuth->email_address,$userAuth->first_name,$userAuth->last_name,$paymentResult['subscription_end_date'],$paymentResult['system_transaction_number'],$category_name);
                    $info['email_address'] = $userAuth->email_address;
                    $info['current_language'] = 1;
                    
                    $info['subject']=$info['current_language']==1?'Thank you for your payment. Enclosed is your payment receipt.':'谢谢您的付款。 附件的是您的项付款收据。';

                    $info['content']=$info['current_language']==1?'Hello <strong>'.$userAuth->first_name.' '.$userAuth->last_name.',
                        </strong><br/><br/>
                        Thank you for your payment. Enclosed is your payment receipt.<br/><br/>':'你好 <strong>'.$userAuth->first_name.' '.$userAuth->last_name.'</strong>,
                        <br/><br/>
                        谢谢您的付款。随函附上您的付款收据。<br/><br/><br/>';

                    $info['footer_content']=$info['current_language']==1?'From,<br /> SixClouds':'六云 ';

                    $info['footer']=$info['current_language']==1?'SixClouds':'六云';
                    Mail::send('emails.email_template', $info, function ($message) use ($info) {
                        $message->from('noreply@sixclouds.cn', 'SixClouds');
                        $message->attach($info['generateReceipt']);
                        $message->to($info['email_address'])->subject($info['subject']);
                    });
                    $returnData = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, 1);
                }
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getMessage(), '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name : Receiptattachement
    //Author : Senil Shah <senil@creolestudios.com>
    //Purpose : generate invoice of the payment
    //In Params : Void
    //Return :
    //Date : 12th Sept 2018
    //###############################################################
    public function Receiptattachement($totalAmount, $email_address, $first_name,$last_name, $hasDistributor,$date, $tradeNo,$projectTitle='',$serviceExtra='',$current_language=1)
    {
        $current_domain = UtilityController::Getmessage('CURRENT_DOMAIN'); 
        if($current_domain=='cn'){
            $priceUnit='¥';
        }else{
            $priceUnit='S$';
        }
        if($current_language==1){
            $title='Your Receipt From SixClouds';
            $invoice='Invoice';
            $date_label='Date';
            $item='Item';
            $price='Price';
            $name=$first_name.' '.$last_name;
        }else{
            $title='收据';
            $invoice='发票';
            $date_label='日期';
            $item='项目';
            $price='价格';
            $name=$first_name.$last_name;
        }
        $date=UtilityController::Changedateformat($date, 'd/m/Y');

        $html='<div class="container" style="font-size:13px; ">
                    <table style="width:100%">
                        <tr>
                            <td rowspan="6"><img src="' . url('/') . '/resources/assets/images/logo-blue.png" height="60" /></td>
                            <td style="text-align:right"><strong>SixClouds Pte. Ltd.</strong></td>
                        </tr>
                        <tr>
                            <td style="text-align:right">Company Registration No:</td>
                        </tr>
                        <tr>
                            <td style="text-align:right">201720404G</td>
                        </tr>
                        <tr>
                            <td style="text-align:right">6001 Beach Road</td>
                        </tr>
                        <tr>
                            <td style="text-align:right">#09-09 Golden Mile Tower</td>
                        </tr>
                        <tr>
                            <td style="text-align:right">Singapore 199589</td>
                        </tr>
                    </table>
                    <br/>
                    <br/>
                <div class="row" style="font-size:16px; ">
                    <div class="col-md-12" align="center">
                        '.$title.'
                    </div>
                </div>
                <hr>
                <br/>
                <div class="row" style="font-size:16px;">
                    <table style="width:100%">
                        <tr>
                            <td><b> '. $name .' </b></td>
                            <td style="text-align:right"><b> '.$invoice.' : '.$tradeNo.' </b></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="text-align:right">'.$date_label.' : '. $date .'</td>
                        </tr>
                    </table>
                </div>
                <br>
                <div class="row">
                    <div class="table">
                        <table  width="100%" cellspacing="0" style="border-collapse: separate;border-spacing: 0px 5px !important; ">
                            <tr  class="col-md-12">
                                <th class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:16px; text-align:left;"><b>'.$item.'</b></th>
                                <th class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:16px; text-align:left;"></th>
                                <th class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:16px; text-align:right;"><b>'.$price.'</b></th>
                            </tr>
                            <tr  class="col-md-12">
                                <th class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:16px; text-align:left;"><b>IGNITE</b></th>
                                <th class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:16px; text-align:left;"></th>
                                <th class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:16px; text-align:right;"><b></b></th>
                            </tr>
                        
                            <tbody class="row" style="margin-top:5px;">';
                            $subTotal = 0;
                            foreach ($projectTitle['categories'] as $key => $value) {
                                $html.='<tr  class="col-md-12">
                                    <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:13px; text-align:left;"><b> '.$value['category_name'].' </b></td>
                                    <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:13px; text-align:left;"></td>
                                    <td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:13px; text-align:right;"><b> '.$priceUnit.' '.$value['category_price'].' </b></td>
                                </tr>';
                                $subTotal += $value['category_price'];
                            }
                            if($projectTitle['isDiscount']==1){
                                $discountOf = intval($subTotal)-intval($totalAmount);
                                $html.= '<tr><td class="col-md-4" style="background-color: #f5f5f5;text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> Sub-Total : '.$priceUnit.' '. $subTotal.' </td></tr>';
                                $html.= '<tr><td class="col-md-4" style="background-color: #f5f5f5;text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> Code Applied : '. $projectTitle['promoCode'].' </td></tr>';
                                $html.= '<tr><td class="col-md-4" style="background-color: #f5f5f5;text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> Discount : '.$priceUnit.' '. $discountOf .' </td></tr>';
                            }
                            $html.='<tr><td class="col-md-4" style="background-color: #f5f5f5;text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; text-align:center;"></td><td class="col-md-4" style="background-color: #f5f5f5; color:#000; padding:5px; font-size:15px; text-align:right;"><b> Total : '.$priceUnit.' '. $totalAmount.' </td></tr>
                            </tbody> 
                        </table>   
                    </div>
                </div>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>';
                if(!empty($hasDistributor)){
                        $html.= '<div class="row">
                                    <table style="width:100%">
                                        <tr>
                                            <td>'.$hasDistributor->distributor->company_name.' is an authorised distributor of SixClouds Pte. Ltd.</td>
                                            <td style="text-align:right">Fees paid are non-refundable.</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td style="text-align:right">Please refer to our Terms of Service.</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>';
                } else {
                    $html.= '<div class="row">
                                <table style="width:100%">
                                    <tr>
                                        <td></td>
                                        <td style="text-align:right">Fees paid are non-refundable.</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td style="text-align:right">Please refer to our Terms of Service.</td>
                                    </tr>
                                </table>
                            </div>
                        </div>';
                }
        $wholePath = substr(URL::to('/'), 0, -3);
        $pathToStore = explode('/', $wholePath);
        if($pathToStore[3]=='beta')
            $pathToStore = '/var/www/html/beta/public/uploads/MP_Attachments/receiptAttachments/';
        else if(in_array('localhost', $pathToStore))
            $pathToStore = '/var/www/html/sixclouds-web/public/uploads/MP_Attachments/receiptAttachments/';
        else
            $pathToStore = '/var/www/html/public/uploads/MP_Attachments/receiptAttachments/';
        
        $filename   = "payment_receipt_" . $tradeNo . ".pdf";
        try{
            $pdf = App::make('dompdf.wrapper');
            $pdf = PDF::loadHtml($html);
        }  catch (\Exception $e) {
            // $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            print_r($e->getMessage()); die;
        }  
        $finalPath = $pathToStore . $filename;
        $pdf->save($finalPath);
        return $finalPath;
    }

    //###############################################################
    //Function Name : Mysubscriptiondata
    //Author : Nivedita<nivedita@creolestudios.com>
    //Purpose : Return the data of the payments made
    //In Params : Void
    //Return :
    //Date : 18th Sept 2018
    //###############################################################
    public function Mysubscriptiondata(){

        try {
            
            $userAuth   = JWTAuth::parseToken()->authenticate();
            $userId     = $userAuth->id;
            $todaysDate = Carbon::now();
            $result     = MarketPlacePayment::select('*')->where('paid_by', $userId)->where('paid_for', 3)->where('payment_status', 1)->where('subscription_end_date', '>', $todaysDate)->get()->toArray();
                           
            if($result){
                foreach ($result as $key => $value) {
                    $date                       = Carbon::parse($value['created_at'])->timezone(Auth::user()->timezone);
                    $result[$key]['created_at'] = UtilityController::Changedateformat($date, 'd F, Y');
                    $result[$key]['paymentstatus'] = ($value['payment_status'] == 1 ? 'Successful' :  'Failed');
                }
                $responseArray = UtilityController::Generateresponse(true,'GENERAL_SUCCESS',1,$result);
            }
            else{
                $responseArray = UtilityController::Generateresponse(false, 'NO_DATA', 400, '');   
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $responseArray     = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response()->json($responseArray);
        
    }

    //###############################################################
    //Function Name : worksheetDownloaded
    //Author : Nivedita<nivedita@creolestudios.com>
    //Purpose : to show that worksheet is downloaded or not
    //In Params : Void
    //Return :
    //Date : 20th Sept 2018
    //###############################################################
    public function worksheetDownloaded(Request $request, $worksheet_id){
        try {
            if(isset($worksheet_id) && is_numeric($worksheet_id) && $worksheet_id > 0){
                $userAuth                    = JWTAuth::parseToken()->authenticate();
                $Input['worksheet_id']       = $worksheet_id;
                $Input['user_id']            = $userAuth->id;
                $Input['worksheet_download'] = 1;

                WorksheetDownloads::where('worksheet_id',$Input['worksheet_id'])->where('user_id',$Input['user_id'])->forceDelete();
                $downloadResult                  = UtilityController::Makemodelobject($Input,'WorksheetDownloads');
                if(!empty($downloadResult)) {
                    $returnData = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 200, $downloadResult['worksheet_app_download']);
                } else {
                    $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 400, '');
                }
            }else {
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 400, '');
            }
        } catch (Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name : getPayeridwechat
    //Author : Nivedita<nivedita@creolestudios.com>
    //Purpose : Get the payer Id from wechat by sending 
    //In Params : Void
    //Return :
    //Date : 24th Sept 2018
    public function getPayeridwechat(Request $request){
        try {

            $Input             = Input::all();
            $language = $request->server('HTTP_ACCEPT_LANGUAGE');
            $deviceType = $request->server('HTTP_DEVICE_TYPE');
            $version = $request->server('HTTP_VERSION');

            if(!array_key_exists('category_id', $Input)){
                throw new \Exception(UtilityController::getMessage("CATEGORY_REQUIRED"));
                
            }
            if(!array_key_exists('access_platform', $Input)){
                throw new \Exception(UtilityController::getMessage("ACCESS_PLATFORM_REQUIRED"));
            }
            

            $userAuth = JWTAuth::parseToken()->authenticate();
            $userId   = $userAuth->id;
            $Input['category_id'] = explode(',', $Input['category_id']);
            $paymentFor = VideoCategory::whereIn('id',$Input['category_id'])->get();
            $totalPaymentAmout = 0.00;
            if(array_key_exists('promo_code', $Input)&&$Input['promo_code']){
                $promoData['category_id'] = implode(',',$Input['category_id']);
                $promoData['promo_code'] = $Input['promo_code'];
                $promoData['promo'] = $Input['promo_code'];
                $promoData['for_category'] = $Input['category_id'];
                $totalPaymentAmout = self::checkPromo($promoData,$userId,$language); 
                $totalPaymentAmout = $totalPaymentAmout['data'];
            } else {
                foreach ($paymentFor as $key => $value) {
                    $totalPaymentAmout += $value['category_price'];
                }
            }
            $totalPaymentAmout = $totalPaymentAmout*100;
            $catgoryNameArray = implode(', ', VideoCategory::whereIn('id',$Input['category_id'])->get()->pluck('category_name')->toArray());
            $outTradeNo                     = mt_rand(0, 999999999);
            $url                            = "https://api.mch.weixin.qq.com/pay/unifiedorder";
            $parameters["appid"]            = "wx2ca46cca03c914f8";
            $parameters["mch_id"]           = "1513927351";
            $parameters["nonce_str"]        = $this->createNoncestr(32);
            $parameters["out_trade_no"]     = $outTradeNo;
            $parameters["total_fee"]        = $totalPaymentAmout;
            // $parameters["total_fee"]        = 2;
            $parameters["spbill_create_ip"] = "125.86.83.126";
            $parameters["notify_url"]       = "https://sixclouds.net";
            $parameters["trade_type"]       = "APP";
            //$parameters["body"]             = "App payment test".$outTradeNo;
            $parameters["body"]             = $catgoryNameArray;
            $parameters["fee_type"]         = "CNY";
            $parameters["sign"]             = $this->genSignature($parameters, "8bHENGiTFqJ5CrXmmMZoP2e0oVROJym0");

            $xml = $this->arrayToXml($parameters);

            $result_xml = $this->postXmlCurl($xml, $url);
            $result = $this->xmlToArray($result_xml);
            if(!empty($result)) {
                $returnData = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', Response::HTTP_OK, $result);
                $returnData['data']['order_id'] = $outTradeNo;
                //\DB::commit();
            } else {
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', Response::HTTP_NO_CONTENT, '');
            }
            //echo json_encode($result);
        }catch (Exception $e) {
            //\DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    public function getPaymentOrderStatus(Request $request){
        try{
            $Input             = Input::all();
            if(!array_key_exists('order_id', $Input)){
                throw new \Exception(UtilityController::getMessage("ORDERID_REQUIRED"));
            }
            $url                            = "https://api.mch.weixin.qq.com/pay/orderquery";
            $parameters["appid"]            = "wx2ca46cca03c914f8";
            $parameters["mch_id"]           = "1513927351";
            $parameters["out_trade_no"]     = $Input['order_id'];
            //$parameters["out_trade_no"]     = '939137993';
            $parameters["nonce_str"]        = $this->createNoncestr(32);
            $parameters["sign"]             = $this->genSignature($parameters, "8bHENGiTFqJ5CrXmmMZoP2e0oVROJym0");

            $xml = $this->arrayToXml($parameters);
            $result_xml = $this->postXmlCurl($xml, $url);
            $result     = $this->xmlToArray($result_xml);

            $userAuth   = JWTAuth::parseToken()->authenticate();
            $userId     = $userAuth->id;
            $userEmail  = $userAuth->email_address;

            if(!empty($result)) {
                
                if($result['trade_state']=='SUCCESS'){
                    \DB::beginTransaction();
                        $category_id = explode(',', $Input['category_id']);
                        $catgoryNameArray = implode(', ', VideoCategory::whereIn('id',$category_id)->get()->pluck('category_name')->toArray());
                        if(array_key_exists('promo_code', $Input) && isset($Input['promo_code'])){
                            $promoData = PromoCode::where('promo_code',$Input['promo_code'])->first();
                            $Input['discount_type'] = $promoData['discount_type'];
                            $Input['discount_of']   = $promoData['discount_of'];
                            
                            if($promoData['code_for']==0){
                                $stringToIntArray = array_map('intval', $category_id);
                                $Input['promo_code_id'] = PromoCode::select(DB::RAW('GROUP_CONCAT(id) AS ids'))->where('promo_code',$Input['promo_code'])->whereIn('video_category_id',$stringToIntArray)->first()->toArray();
                                $Input['promo_code_id'] = $Input['promo_code_id']['ids'];
                            } else {
                                $addToPayment['distributor_id']             = $promoData['distributor_id'];
                                $addToPayment['distributer_team_member_id'] = $promoData['distributor_team_member_id'];
                            }
                        }

                        $payment['payment_method']            = 4 ;##for wechat;
                        $payment['paid_by']                   = $userId;
                        // $payment['video_category_id']      = $paymentFor['id'];
                        $payment['video_category_id']         = $Input['category_id'];
                        $payment['video_category_name']       = $catgoryNameArray;
                        $payment['subscription_end_date']     = Carbon::now()->addYears(1);
                        $payment['remarks']                   = 'New subscription';
                        $payment['access_platform']           = $Input['access_platform'];
                        $payment['transaction_reference']     = $result['transaction_id'];
                        $payment['paid_for']                  = 3;
                        $payment['payment_amount']            = $Input['paid_amount'];
                        if(array_key_exists('promo_code', $Input) && isset($Input['promo_code'])){   
                            $payment['promo_code']                = $Input['promo_code'];
                            $payment['discount_type']             = $Input['discount_type'];
                            $payment['discount_of']               = $Input['discount_of'];
                            if($promoData['code_for']==0&&isset($Input['promo_code_id']))
                                $payment['promo_code_id']             = $Input['promo_code_id'];
                        }
                        $payment['payment_status']            = 1;
                        $payment['slug']                      = $userAuth->slug;
                        $payment['system_transaction_number'] = UtilityController::GenerateRunningNumber('MarketPlacePayment','system_transaction_number',"VL");
                        if(!empty($addToPayment)){
                            $payment['distributor_id'] = $promoData['distributor_id'];
                            $payment['distributer_team_member_id'] = $promoData['distributor_team_member_id'];
                        }
                       
                        $paymentResult                        = UtilityController::Makemodelobject($payment,'MarketPlacePayment');
                        if(!empty($paymentResult)){
                            if($paymentResult['promo_code']!=''){

                                if($promoData['code_for']==1||$promoData['code_for']==2){
                                    $checkIfdistributorExists = DistributorsReferrals::where('user_id',$paymentResult['paid_by'])->first();
                                    if(empty($checkIfdistributorExists)){
                                        if($promoData['code_for']==2)
                                            $addReferal['distributor_team_member_id'] = $promoData['distributor_team_member_id'];
                                        $addReferal['distributors_id'] = $promoData['distributor_id'];
                                        $addReferal['user_id'] = $paymentResult['paid_by'];
                                        $addReferal['referal_through'] = $promoData['code_for']==2?4:1;
                                        $addReferalResult = UtilityController::Makemodelobject($addReferal,'DistributorsReferrals');
                                    }
                                }

                                if($promoData['code_for']==0){
                                    /*$promoCountLimitedInc  = PromoCode::whereIn('id',explode(',', $paymentResult['promo_code_id']))->where('redemption_type',0)->increment('redeemed_count');*/
                                    $promoCount = PromoCode::whereIn('id',explode(',', $paymentResult['promo_code_id']))->increment('redeemed_count');
                                } else {
                                    $promoCount = PromoCode::where('promo_code',$Input['promo_code'])->increment('redeemed_count');
                                }
                            }
                            $hasDistributor = DistributorsReferrals::with('distributor','salesperson')->where('user_id',$userId)->orderBy('id','DESC')->first();
                            if(!empty($hasDistributor)){
                                $addDistributortoPayment['distributor_id'] = $hasDistributor['distributors_id'];
                                if($paymentResult['distributer_team_member_id']=='')
                                    $addDistributortoPayment['distributer_team_member_id'] = $hasDistributor['distributor_team_member_id'];
                                $paymentResultDistributor = UtilityController::Makemodelobject($addDistributortoPayment,'MarketPlacePayment','',$paymentResult['id']);
                                
                                $commissions_category = $category_id;
                                // $ifAlreadyExists = IgniteCommissions::where('distributor_id',$hasDistributor['distributors_id'])->where('subscription_month',Carbon::now()->format('m'))->where('subscription_year',Carbon::now()->format('Y'))->whereIn('video_category_id',$commissions_category)
                                $distributorNewCommission   = $hasDistributor['distributor']['commission_percentage'];
                                $distributorRenewCommission = $hasDistributor['distributor']['renewal_commission_percentage'];
                                $distributorCommissionAmount = ($paymentResult['payment_amount']*$distributorNewCommission)/100;
                                if($paymentResult['distributer_team_member_id']!=''){
                                    $teamMemberInfo = DistributorsTeamMembers::where('id',$paymentResult['distributer_team_member_id'])->first();
                                    $teamMemberNewCommission    = $teamMemberInfo['commission_percentage'];
                                    $teamMemberRenewCommission  = $teamMemberInfo['renewal_commission_percentage'];
                                    $TeamMemberCommissionAmount = ($distributorCommissionAmount*$teamMemberNewCommission)/100;
                                } else {
                                    if(!empty($hasDistributor['salesperson'])){
                                        $teamMemberNewCommission    = $hasDistributor['salesperson']['commission_percentage'];
                                        $teamMemberRenewCommission  = $hasDistributor['salesperson']['renewal_commission_percentage'];
                                        $TeamMemberCommissionAmount = ($distributorCommissionAmount*$teamMemberNewCommission)/100;
                                    }
                                }
                                
                                foreach ($commissions_category as $key => $value) {
                                    if($paymentResult['distributor_id']!=''){
                                        $addCommissions[$key]['distributor_id']             = $paymentResult['distributor_id'];
                                        $addCommissions[$key]['distributer_team_member_id'] = $paymentResult['distributer_team_member_id'];
                                    } else {
                                        $addCommissions[$key]['distributor_id']             = $hasDistributor['distributors_id'];
                                        $addCommissions[$key]['distributer_team_member_id'] = $hasDistributor['distributor_team_member_id'];   
                                    }
                                    $addCommissions[$key]['distributor_commission_amount'] = $distributorCommissionAmount;
                                    if($paymentResult['distributer_team_member_id']!='')
                                        $addCommissions[$key]['team_member_commission_amount'] = $TeamMemberCommissionAmount;
                                    $addCommissions[$key]['video_category_id']          = $value;
                                    $addCommissions[$key]['subscription_month']         = Carbon::now()->format('m');
                                    $addCommissions[$key]['subscription_year']          = Carbon::now()->format('Y');
                                    $addCommissions[$key]['subscribers_count']          = 1;
                                    $addCommissions[$key]['subscription_type']          = 1;
                                    $addCommissions[$key]['created_at']                 = Carbon::now();
                                    $addCommissions[$key]['updated_at']                 = Carbon::now();
                                }
                                IgniteCommissions::insert($addCommissions);
                            }
                            // $Input['category_id'] = explode(',', $Input['category_id']);
                            /*$category_name['categories'] = VideoCategory::select('category_name','category_price')->whereIn('id',$Input['category_id'])->get()->toArray();
                            if($paymentResult['promo_code']!=''){
                                $category_name['isDiscount'] = 1;
                                $category_name['promoCode'] = $paymentResult['promo_code'];
                                // $category_name['discountOf'] = 1;
                            } else
                                $category_name['isDiscount'] = 0;
                            // print_r($category_name->toArray()); die;
                            $info['generateReceipt'] = self::Receiptattachement($paymentResult['payment_amount'],$userAuth->email_address,$userAuth->first_name,$userAuth->last_name,$hasDistributor,Carbon::now(),$paymentResult['system_transaction_number'],$category_name);
                            $info['email_address'] = $userAuth->email_address;
                            $info['current_language'] = 1;
                            
                            $info['subject']=$info['current_language']==1?'Thank you for your payment. Enclosed is your payment receipt.':'谢谢您的付款。 附件的是您的项付款收据。';

                            $info['content']=$info['current_language']==1?'Hello <strong>'.$userAuth->first_name.' '.$userAuth->last_name.',
                                </strong><br/><br/>
                                Thank you for your payment. Enclosed is your payment receipt.<br/><br/>':'你好 <strong>'.$userAuth->first_name.' '.$userAuth->last_name.'</strong>,
                                <br/><br/>
                                谢谢您的付款。随函附上您的付款收据。<br/><br/><br/>';

                            $info['footer_content']=$info['current_language']==1?'From,<br /> SixClouds':'六云';

                            $info['footer']=$info['current_language']==1?'SixClouds':'六云';
                            Mail::send('emails.email_template', $info, function ($message) use ($info) {
                                $message->from('noreply@sixclouds.cn', 'SixClouds');
                                $message->attach($info['generateReceipt']);
                                $message->to($info['email_address'])->subject($info['subject']);
                            });*/
                            $returnData = UtilityController::Generateresponse(true, 'PAYMENT_SUCCESS', Response::HTTP_OK, '1');
                        }
                    \DB::commit();
                } else if($result['trade_state']== 'NOTPAY'){   
                    $returnData = UtilityController::Generateresponse(false, 'NOT_PAID', Response::HTTP_OK, '0');
                }else {
                    $returnData = UtilityController::Generateresponse(false, 'PAYMENT_FAILED', 0, '0');
                }
            } else {
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', Response::HTTP_NO_CONTENT, '');
            }
            
        }catch (Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    public function xmlToArray($xml)
    {
        $array_data = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
            return $array_data;
    }

    public function postXmlCurl($xml, $url, $second = 30)
    {
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "xmlRequest=" . $xml);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
        $response = curl_exec($ch);
        //curl_close($ch);

        if ($response)
        {
            curl_close($ch);
            return $response;
        }
        else
        {
            curl_close($ch);
            return false;
        }
    }

    public function arrayToXml($arr)
    {
        $xml = "<xml>";
            foreach ($arr as $key => $val)

            {
                if (is_numeric($val))
                {
                    $xml .= "<" . $key . ">" . $val . "</" . $key . ">";

                }
                else
                    $xml .= "<" . $key . "><![CDATA[" . $val . "]]></" . $key . ">";
            }
            $xml .= "</xml>";
            return $xml;
    }

    public function createNoncestr($length = 32)
    {
        $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        $str = "";
        for ($i = 0; $i < $length; $i++)
        {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }
        return $str;
    }

    public function genSignature($obj, $key)
    {
        foreach ($obj as $k => $v)
        {
            $parameters[$k] = $v;
        }
        
        ksort($parameters);
        $String = $this->formatBizQueryParaMap($parameters, false);
        $String = $String . "&key=" . $key;
        $String = md5($String);
        $result_ = strtoupper($String);
        return $result_;
    }

    public function formatBizQueryParaMap($paraMap, $urlencode)
    {
        $buff = "";
        ksort($paraMap);
        foreach ($paraMap as $k => $v)
        {
            if ($urlencode)
            {
                $v = urlencode($v);
            }
            //$buff .= strtolower($k) . "=" . $v . "&";
            $buff .= $k . "=" . $v . "&";
        }
        $reqPar = '';
        if (strlen($buff) > 0)
        {
            $reqPar = substr($buff, 0, strlen($buff) - 1);
        }
        return $reqPar;
    }

    //###############################################################

    //###############################################################
    //Function Name: storeLogFile
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to store the log file sent, for debugging
    //In Params:     log file, id(optional), userId(optional)
    //Return:        json
    //Date:          28th Sept, 2018
    //###############################################################
    public function storeLogFile(Request $request){
        try {
            $Input            = Input::all();
            if(!array_key_exists('file_name', $Input)) {
                $returnData = UtilityController::Generateresponse(false, 'NO_FILE_UPLOADED', 0, '');
                return response($returnData);
            }
            if(array_key_exists('take_user_id', $Input)) {
                $userAuth         = JWTAuth::parseToken()->authenticate();
                $Input['user_id'] = $userAuth->id;
            }
            \DB::beginTransaction();
                $fileName = date("dmYHis").rand();
                $Input['file_name']->move(public_path('') . UtilityController::Getpath('LOG_FILE_PATH'), $fileName);
                $path = public_path('').UtilityController::Getpath('LOG_FILE_PATH').$fileName;
                $Input['file_name'] = $fileName;
                if(array_key_exists('id', $Input)){
                    $existingFile = AndroidLogFile::select('file_name')->where('id',$Input['id'])->first();
                    unlink(public_path('').UtilityController::Getpath('LOG_FILE_PATH').$existingFile['file_name']);
                    $fileStoreResult = UtilityController::Makemodelobject($Input,'AndroidLogFile','',$Input['id']);
                } else {
                    $fileStoreResult = UtilityController::Makemodelobject($Input,'AndroidLogFile');
                }
                if(!empty($fileStoreResult)){
                    $returnData = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 1, $fileStoreResult['id']);
                }
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getMessage().'-:-'.$e->getLine(), '', '');
        }            
        return response($returnData);
    }
    //###############################################################
    //Function Name: generateClientToken
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to generate client token for app paypal payment
    //In Params:     
    //Return:        json
    //Date:          3rd Oct, 2018
    //###############################################################
    public function generateClientToken(Request $request){     
        try {
            $gateway = new Braintree_Gateway([
                'environment' => 'sandbox',
                'merchantId'  => 'rhpbbf7529753rky',
                'publicKey'   => 'f72nxw2txtr5qt42',
                'privateKey'  => '44cef2c33ecfe9e56da0c32c070d6b4c'
            ]);
            $clientToken = $gateway->clientToken()->generate();
            $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $clientToken);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: makePaymentThroughNonce
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To do payment through paypal by accepting nonce and store them into database
    //In Params:     nonce, payment detail data
    //Return:        json
    //Date:          3rd Oct, 2018
    //###############################################################
    public function makePaymentThroughNonce(Request $request){     
        try {
            $Input    = Input::all();
            if(!array_key_exists('nonce', $Input)){
                throw new \Exception(UtilityController::getMessage("NONCE_REQUIRED"));
                
            }
            if(!array_key_exists('category_id', $Input)){
                throw new \Exception(UtilityController::getMessage("CATEGORY_REQUIRED"));
                
            }
            if(!array_key_exists('access_platform', $Input)){
                throw new \Exception(UtilityController::getMessage("ACCESS_PLATFORM_REQUIRED"));
                
            }
            $userAuth = JWTAuth::parseToken()->authenticate();
            $userId   = $userAuth->id;
            $paymentFor = VideoCategory::where('id',$Input['category_id'])->first();
            $gateway  = new Braintree_Gateway([
                'environment' => 'sandbox',
                'merchantId'  => 'rhpbbf7529753rky',
                'publicKey'   => 'f72nxw2txtr5qt42',
                'privateKey'  => '44cef2c33ecfe9e56da0c32c070d6b4c'
            ]);
            $result = $gateway->transaction()->sale([
              'amount' => $paymentFor['category_price'],
              'paymentMethodNonce' => $Input['nonce'],
              'options' => [
                'submitForSettlement' => True
              ]
            ]);
                // print_r($result); die;
            if($result->success==1){
                \DB::beginTransaction();
                    $payment['payment_method'] = 2;
                    $payment['paid_by'] = $userId;
                    $payment['video_category_id'] = $paymentFor['id'];
                    $payment['subscription_end_date'] = Carbon::now()->addYears(1);
                    $payment['remarks'] = 'New subscription';
                    $payment['access_platform'] = $Input['access_platform'];
                    $payment['transaction_reference'] = $result->transaction->id;
                    $payment['paid_for'] = 3;
                    $payment['payment_amount'] = $paymentFor['category_price'];
                    $payment['payment_status'] = $result->success;
                    $payment['slug'] = $userAuth->slug;
                    $payment['system_transaction_number'] = UtilityController::GenerateRunningNumber('MarketPlacePayment','system_transaction_number',"VL");
                    $paymentResult = UtilityController::Makemodelobject($payment,'MarketPlacePayment');
                    if(!empty($paymentResult)){
                        $Input['category_id'] = explode(',', $Input['category_id']);
                        $category_name['categories'] = VideoCategory::whereIn('id',$Input['category_id'])->get()->pluck('category_name');
                        if($paymentResult['promo_code']!=''){
                            $category_name['isDiscount'] = 1;
                            $category_name['promoCode'] = $paymentResult['promo_code'];
                            // $category_name['discountOf'] = 1;
                        } else
                            $category_name['isDiscount'] = 0;
                        // $category_name = $category_name['category_name'];
                        // print_r($category_name->toArray()); die;
                        $info['generateReceipt'] = self::Receiptattachement($paymentResult['payment_amount'],$userAuth->email_address,$userAuth->first_name,$userAuth->last_name,$paymentResult['subscription_end_date'],$paymentResult['system_transaction_number'],$category_name);
                        $info['email_address'] = $userAuth->email_address;
                        $info['current_language'] = 1;
                        
                        $info['subject']=$info['current_language']==1?'Thank you for your payment. Enclosed is your payment receipt.':'谢谢您的付款。 附件的是您的项付款收据。';

                        $info['content']=$info['current_language']==1?'Hello <strong>'.$userAuth->first_name.' '.$userAuth->last_name.',
                            </strong><br/><br/>
                            Thank you for your payment. Enclosed is your payment receipt.<br/><br/>':'你好 <strong>'.$userAuth->first_name.' '.$userAuth->last_name.'</strong>,
                            <br/><br/>
                            谢谢您的付款。随函附上您的付款收据。<br/><br/><br/>';

                        $info['footer_content']=$info['current_language']==1?'From,<br /> SixClouds':'六云';

                        $info['footer']=$info['current_language']==1?'SixClouds':'六云';
                        Mail::send('emails.email_template', $info, function ($message) use ($info) {
                            $message->from('noreply@sixclouds.cn', 'SixClouds');
                            $message->attach($info['generateReceipt']);
                            $message->to($info['email_address'])->subject($info['subject']);
                        });
                        $returnData = UtilityController::Generateresponse(true, 'PAYMENT_SUCCESS', 1, '1');
                    }
                \DB::commit();
            } else {
                $returnData = UtilityController::Generateresponse(false, 'PAYMENT_FAILED', 0, '0');
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getMessage().' -:- '.$e->getLine(), '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: getDownloadedVideoList
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To get the device specific downloaded video list
    //In Params:     Auth token, imei number
    //Return:        json
    //Date:          5th Oct, 2018
    //###############################################################
    public function getDownloadedVideoList(Request $request, $imei=''){     
        try {
            $userAuth = JWTAuth::parseToken()->authenticate();
            $userId   = $userAuth->id;
            // $videoListUrlIds = VideoViews::where('user_id',$userId)->where('video_deleted_app_status',0)->where('imei',$imei)->where('video_app_download',1)->pluck('id')->toArray();
            // $videoListIds = VideoViews::where('user_id',$userId)->where('video_deleted_app_status',0)->where('imei',$imei)->where('video_app_download',1)->pluck('video_id')->toArray();
            $videoListUrlIds = VideoViews::where('user_id',$userId)->where('video_deleted_app_status',0)->where('video_app_download',1)->pluck('id')->toArray();
            $videoListIds = VideoViews::where('user_id',$userId)->where('video_deleted_app_status',0)->where('video_app_download',1)->pluck('video_id')->toArray();
            $videoListIds = array_unique($videoListIds);
            
            $videoList = Videos::with([
                'video_url' =>function($query) use($videoListUrlIds){
                    $query->select('id','video_id','video_duration','video_language','video_oss_url')->with([
                        'views_detail' => function($query) use($videoListUrlIds){
                            $query->select('id','video_id','video_url_id','video_viewed_duration AS resume_from','video_viewing_app_status AS view_status');
                        }
                    ]);
                }
            ])->whereIn('id',$videoListIds)->get();
            $globalVariable = UserController::Globalvariables();
            $videoList = $videoList->map(function($value, $key) use($globalVariable){
                if($globalVariable['CURRENT_DOMAIN']=='net'){
                    $value->thumbnail_160_90 = isset($value->thumbnail_160_90)&&$value->thumbnail_160_90!=''?UtilityController::Getpath('VIDEO_THUMBNAIL_MOBILE_160_90_CDN').$value->thumbnail_160_90:UtilityController::getMessage('NO_IMAGE_URL');
                    $value->thumbnail_128_72 = isset($value->thumbnail_128_72)&&$value->thumbnail_128_72!=''?UtilityController::Getpath('VIDEO_THUMBNAIL_MOBILE_128_72_CDN').$value->thumbnail_128_72:UtilityController::getMessage('NO_IMAGE_URL');
                } else {
                    $value->thumbnail_160_90 = isset($value->thumbnail_160_90)&&$value->thumbnail_160_90!=''?UtilityController::Getpath('VIDEO_THUMBNAIL_MOBILE_160_90_URL').$value->thumbnail_160_90:UtilityController::getMessage('NO_IMAGE_URL');
                    $value->thumbnail_128_72 = isset($value->thumbnail_128_72)&&$value->thumbnail_128_72!=''?UtilityController::Getpath('VIDEO_THUMBNAIL_MOBILE_128_72_URL').$value->thumbnail_128_72:UtilityController::getMessage('NO_IMAGE_URL');
                }
                return $value;
            });
            $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1, $videoList);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getMessage(), '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: getDownloadedDeletedVideoList
    //Author:        Nivedita Mitra <nivedita@creolestudios.com>
    //Purpose:       To get the device specific downloaded video list that are deleted
    //In Params:     Auth token, imei number, video url id
    //Return:        json
    //Date:          8th Oct, 2018
    //###############################################################
    public function getDownloadedDeletedVideoList(Request $request, $video_url_id, $imei=''){     
        try {
            $userAuth = JWTAuth::parseToken()->authenticate();
            $userId   = $userAuth->id;

            $delete['video_deleted_app_status'] = 1;
            // $deleteResult                       = VideoViews::where('user_id',$userId)->where('video_url_id',$video_url_id)->where('imei',$imei)->update($delete);
            $deleteResult                       = VideoViews::where('user_id',$userId)->where('video_url_id',$video_url_id)->update($delete);
            if(!empty($deleteResult)) {
                $returnData = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', Response::HTTP_OK, $delete['video_deleted_app_status']);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', Response::HTTP_BAD_REQUEST, '');
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getMessage(), '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: stripIntegrate
    //Author:        Nivedita Mitra <nivedita@creolestudios.com>
    //Purpose:       Payment through strip gateway
    //In Params:     Source token, category id, access platform, development=> 1: sandbox 0: live
    //Return:        json
    //Date:          8th Oct, 2018
    //###############################################################
    public function stripIntegrate(Request $request){
        
        $Input    = Input::all();
        try {
            $Input    = Input::all();
            if(!array_key_exists('source_token', $Input)){
                throw new \Exception(UtilityController::getMessage("SOURCE_TOKEN_REQUIRED"));
                
            }
            if(!array_key_exists('category_id', $Input)){
                throw new \Exception(UtilityController::getMessage("CATEGORY_REQUIRED"));
                
            }
            if(!array_key_exists('access_platform', $Input)){
                throw new \Exception(UtilityController::getMessage("ACCESS_PLATFORM_REQUIRED"));
            }
            if(!array_key_exists('price_type', $Input)){
                throw new \Exception(UtilityController::getMessage("PRICE_TYPE_REQUIRED"));
            }
            $userAuth = JWTAuth::parseToken()->authenticate();
            $userId   = $userAuth->id;
            $userEmail   = $userAuth->email_address;
            $Input['category_id'] = explode(',', $Input['category_id']);
            $totalPaymentAmout = 0.00;
            if(array_key_exists('promo_code', $Input)&&$Input['promo_code']){
                $promoData['category_id'] = implode(',',$Input['category_id']);
                $promoData['promo_code'] = $Input['promo_code'];
                $promoData['promo'] = $Input['promo_code'];
                $promoData['for_category'] = $Input['category_id'];
                $totalPaymentAmout = self::checkPromo($promoData,$userId,$Input['price_type']);
                $totalPaymentAmout = $totalPaymentAmout['data'];
            } else {
                $paymentFor = VideoCategory::whereIn('id',$Input['category_id'])->get();
                foreach ($paymentFor as $key => $value) {
                    if($Input['price_type'] == 1) {
                        $totalPaymentAmout += $value['category_price'];
                    } else if($Input['price_type'] == 1) {
                        $totalPaymentAmout += $value['category_price_sgd'];
                    }
                }
            }
            $catgoryNameArray = implode(', ', VideoCategory::whereIn('id',$Input['category_id'])->get()->pluck('category_name')->toArray());
            if(array_key_exists('development', $Input) && $Input['development']==1)
                Stripe::setApiKey(env('STRIPE_SANDBOX'));
            else
                Stripe::setApiKey(env('STRIPE_LIVE'));
            $customer = Customer::create(array(
                'email'  => $userEmail,
                'source' => $Input['source_token']
            ));
            
            if($Input['price_type'] == 1) {
                $currency = 'usd';
            } else if($Input['price_type'] == 2) {
                $currency = 'sgd';
            }

            $charge = Charge::create(array(
                'customer' => $customer->id,
                // 'amount'   => $paymentFor['category_price'],
                'amount'   => $totalPaymentAmout*100,
                'currency' => $currency
            ));
            // echo("<pre>");print_r($charge);echo("</pre>");die;
                // print_r($result); die;
            if($charge->paid==1){
                \DB::beginTransaction();                    
                    if(array_key_exists('promo_code', $Input) && isset($Input['promo_code'])){
                        $promoData = PromoCode::where('promo_code',$Input['promo_code'])->first();
                        $Input['discount_type'] = $promoData['discount_type'];
                        $Input['discount_of']   = $promoData['discount_of'];
                        if($promoData['code_for']==0){
                            $stringToIntArray = array_map('intval', $Input['category_id']);
                            $Input['promo_code_id'] = PromoCode::select(DB::RAW('GROUP_CONCAT(id) AS ids'))->where('promo_code',$Input['promo_code'])->whereIn('video_category_id',$stringToIntArray)->first()->toArray();
                            $Input['promo_code_id'] = $Input['promo_code_id']['ids'];
                        } else {
                            $addToPayment['distributor_id'] = $promoData['distributor_id'];
                            $addToPayment['distributer_team_member_id'] = $promoData['distributor_team_member_id'];
                        }
                    }
                    $payment['payment_method']            = 5; //strip
                    $payment['paid_by']                   = $userId;
                    // $payment['video_category_id']      = $paymentFor['id'];
                    $payment['video_category_id']         = implode(',', $Input['category_id']);
                    $payment['video_category_name']       = $catgoryNameArray;
                    $payment['subscription_end_date']     = Carbon::now()->addYears(1);
                    $payment['remarks']                   = 'New subscription';
                    $payment['access_platform']           = $Input['access_platform'];
                    $payment['transaction_reference']     = $charge->balance_transaction;
                    $payment['paid_for']                  = 3;
                    // $payment['payment_amount']         = $paymentFor['category_price'];
                    $payment['payment_amount']            = $totalPaymentAmout;
                    if(array_key_exists('promo_code', $Input) && isset($Input['promo_code'])){   
                        $payment['promo_code']                = $Input['promo_code'];
                        $payment['discount_type']             = $Input['discount_type'];
                        $payment['discount_of']               = $Input['discount_of'];
                        if($promoData['code_for']==0&&isset($Input['promo_code_id']))
                            $payment['promo_code_id']             = $Input['promo_code_id'];
                    }
                    $payment['payment_status']            = 1;
                    $payment['slug']                      = $userAuth->slug;
                    $payment['system_transaction_number'] = UtilityController::GenerateRunningNumber('MarketPlacePayment','system_transaction_number',"VL");
                    if(!empty($addToPayment)){
                        $payment['distributor_id'] = $promoData['distributor_id'];
                        $payment['distributer_team_member_id'] = $promoData['distributor_team_member_id'];
                    }
                    $paymentResult                        = UtilityController::Makemodelobject($payment,'MarketPlacePayment');
                    if(!empty($paymentResult)){

                        if($paymentResult['promo_code']!=''){
                            $promoData = PromoCode::where('promo_code',$paymentResult['promo_code'])->first();
            
                            if($promoData['code_for']==1||$promoData['code_for']==2){
                                $checkIfdistributorExists = DistributorsReferrals::where('user_id',$paymentResult['paid_by'])->first();
                                if(empty($checkIfdistributorExists)){
                                    if($promoData['code_for']==2)
                                        $addReferal['distributor_team_member_id'] = $promoData['distributor_team_member_id'];
                                    $addReferal['distributors_id'] = $promoData['distributor_id'];
                                    $addReferal['user_id'] = $paymentResult['paid_by'];
                                    $addReferal['referal_through'] = $promoData['code_for']==2?4:1;
                                    $addReferalResult = UtilityController::Makemodelobject($addReferal,'DistributorsReferrals');
                                }
                            }
                            if($promoData['code_for']==0){
                                /*$promoCountLimitedInc  = PromoCode::whereIn('id',explode(',', $paymentResult['promo_code_id']))->where('redemption_type',0)->increment('redeemed_count');*/
                                $promoCount = PromoCode::whereIn('id',explode(',', $paymentResult['promo_code_id']))->increment('redeemed_count');
                            } else {
                                $promoCount = PromoCode::where('promo_code',$Input['promo_code'])->increment('redeemed_count');
                            }
                        }
                        $hasDistributor = DistributorsReferrals::with('distributor','salesperson')->where('user_id',$userId)->orderBy('id','DESC')->first();
                        if(!empty($hasDistributor)){
                            $addDistributortoPayment['distributor_id'] = $hasDistributor['distributors_id'];
                            if($paymentResult['distributer_team_member_id']=='')
                                $addDistributortoPayment['distributer_team_member_id'] = $hasDistributor['distributor_team_member_id'];
                            $paymentResultDistributor = UtilityController::Makemodelobject($addDistributortoPayment,'MarketPlacePayment','',$paymentResult['id']);
                            
                            $commissions_category = $Input['category_id'];
                            // $ifAlreadyExists = IgniteCommissions::where('distributor_id',$hasDistributor['distributors_id'])->where('subscription_month',Carbon::now()->format('m'))->where('subscription_year',Carbon::now()->format('Y'))->whereIn('video_category_id',$commissions_category)
                            $distributorNewCommission   = $hasDistributor['distributor']['commission_percentage'];
                            $distributorRenewCommission = $hasDistributor['distributor']['renewal_commission_percentage'];
                            $distributorCommissionAmount = ($paymentResult['payment_amount']*$distributorNewCommission)/100;
                            if($paymentResult['distributer_team_member_id']!='') {
                                $teamMemberInfo = DistributorsTeamMembers::where('id',$paymentResult['distributer_team_member_id'])->first();
                                $teamMemberNewCommission    = $teamMemberInfo['commission_percentage'];
                                $teamMemberRenewCommission  = $teamMemberInfo['renewal_commission_percentage'];
                                $TeamMemberCommissionAmount = ($distributorCommissionAmount*$teamMemberNewCommission)/100;
                            } else {
                                if(!empty($hasDistributor['salesperson'])){
                                    $teamMemberNewCommission    = $hasDistributor['salesperson']['commission_percentage'];
                                    $teamMemberRenewCommission  = $hasDistributor['salesperson']['renewal_commission_percentage'];
                                    $TeamMemberCommissionAmount = ($distributorCommissionAmount*$teamMemberNewCommission)/100;
                                }
                            }
                            foreach ($commissions_category as $key => $value) {
                                if($paymentResult['distributor_id']!=''){
                                        $addCommissions[$key]['distributor_id']             = $paymentResult['distributor_id'];
                                        $addCommissions[$key]['distributer_team_member_id'] = $paymentResult['distributer_team_member_id'];
                                    } else {
                                        $addCommissions[$key]['distributor_id']             = $hasDistributor['distributors_id'];
                                        $addCommissions[$key]['distributer_team_member_id'] = $hasDistributor['distributor_team_member_id'];   
                                    }
                                    $addCommissions[$key]['distributor_commission_amount'] = $distributorCommissionAmount;
                                    if($paymentResult['distributer_team_member_id']!='')
                                        $addCommissions[$key]['team_member_commission_amount'] = $TeamMemberCommissionAmount;
                                    $addCommissions[$key]['video_category_id']          = $value;
                                    $addCommissions[$key]['subscription_month']         = Carbon::now()->format('m');
                                    $addCommissions[$key]['subscription_year']          = Carbon::now()->format('Y');
                                    $addCommissions[$key]['subscribers_count']          = 1;
                                    $addCommissions[$key]['subscription_type']          = 1;
                                    $addCommissions[$key]['created_at']                 = Carbon::now();
                                    $addCommissions[$key]['updated_at']                 = Carbon::now();
                            }
                            IgniteCommissions::insert($addCommissions);
                        }
                        // $Input['category_id'] = explode(',', $Input['category_id']);
                        $category_name['categories'] = VideoCategory::select('category_name','category_price')->whereIn('id',$Input['category_id'])->get()->toArray();
                        if($paymentResult['promo_code']!=''){
                            $category_name['isDiscount'] = 1;
                            $category_name['promoCode'] = $paymentResult['promo_code'];
                            // $category_name['discountOf'] = 1;
                        } else
                            $category_name['isDiscount'] = 0;
                        // print_r($category_name->toArray()); die;
                        // $info['generateReceipt'] = self::Receiptattachement($paymentResult['payment_amount'],$userAuth->email_address,$userAuth->first_name,$userAuth->last_name,$hasDistributor,$paymentResult['subscription_end_date'],$paymentResult['system_transaction_number'],$category_name);
                        $info['generateReceipt'] = self::Receiptattachement($paymentResult['payment_amount'],$userAuth->email_address,$userAuth->first_name,$userAuth->last_name,$hasDistributor,Carbon::now(),$paymentResult['system_transaction_number'],$category_name);
                        $info['email_address'] = $userAuth->email_address;
                        $info['current_language'] = 1;
                        
                        $info['subject']=$info['current_language']==1?'Thank you for your payment. Enclosed is your payment receipt.':'谢谢您的付款。 附件的是您的项付款收据。';

                        $info['content']=$info['current_language']==1?'Hello <strong>'.$userAuth->first_name.' '.$userAuth->last_name.',
                            </strong><br/><br/>
                            Thank you for your payment. Enclosed is your payment receipt.<br/><br/>':'你好 <strong>'.$userAuth->first_name.' '.$userAuth->last_name.'</strong>,
                            <br/><br/>
                            谢谢您的付款。随函附上您的付款收据。<br/><br/><br/>';

                        $info['footer_content']=$info['current_language']==1?'From,<br /> SixClouds':'六云';

                        $info['footer']=$info['current_language']==1?'SixClouds':'六云';
                        Mail::send('emails.email_template', $info, function ($message) use ($info) {
                            $message->from('noreply@sixclouds.cn', 'SixClouds');
                            $message->attach($info['generateReceipt']);
                            $message->to($info['email_address'])->subject($info['subject']);
                        });
                        $returnData = UtilityController::Generateresponse(true, 'PAYMENT_SUCCESS', 1, '1');
                    }
                \DB::commit();
            } else {
                $returnData = UtilityController::Generateresponse(false, 'PAYMENT_FAILED', 0, '0');
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getMessage().' -:- '.$e->getLine(), '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: syncOfflineData
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to update the details of downloaded content when subscriber goes offline
    //In Params:     array of details
    //Return:        json
    //Date:          16th Oct, 2018
    //###############################################################
    public function syncOfflineData(Request $request){
        try {
            $Input = Input::all();
            $userAuth = JWTAuth::parseToken()->authenticate();
            $userId   = $userAuth->id;
            \DB::beginTransaction();
                $saveSuccess = 0;
                foreach ($Input as $key => $value) {
                    $syncData['video_url_id'] = $value['video_url_id'];
                    $syncData['video_viewed_duration'] = $value['video_status']==1?'00:00:00':$value['duration'];
                    $syncData['video_viewing_app_status'] = $value['video_status'];
                    $returnData = VideoViews::where('user_id',$userId)->where('video_url_id',$value['video_url_id'])->update($syncData);
                    if($returnData){
                        $saveSuccess++;
                    }
                }
                if($saveSuccess == count($Input)){
                    $returnData = UtilityController::Generateresponse(true, 'GENERAL_SUCCESS', 200);
                    \DB::commit();
                } else {
                    throw new \Exception(UtilityController::getMessage('GENERAL_ERROR'));
                }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }
        return response($returnData);
    }

    //###############################################################
    //Function Name: versionUpdate
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To check users version and let them know to update accordingly
    //In Params:     headers value
    //Return:        json
    //Date:          31st Oct, 2018
    //###############################################################
    public function versionUpdate(Request $request){     
        try {
            $language = $request->server('HTTP_ACCEPT_LANGUAGE');
            $deviceType = $request->server('HTTP_DEVICE_TYPE');
            $version = $request->server('HTTP_VERSION');
            print_r($version);die;
            $data = ($language=='en'?VersionControl::select('id','status','link','play_store_link', DB::Raw('message_english AS message'))->where('version',$version)->where('device_type',$deviceType)->first():VersionControl::select('id','status','link','play_store_link', DB::Raw('message_chinese AS message'))->where('version',$version)->where('device_type',$deviceType)->first());
            if(!empty($data)){
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', Response::HTTP_OK, $data);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', Response::HTTP_BAD_REQUEST);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: CheckPromoCode
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       To check users version and let them know to update accordingly
    //In Params:     headers value
    //Return:        json
    //Date:          31st Oct, 2018
    //###############################################################
    public function CheckPromoCode(Request $request){     
        try {
            $language = $request->server('HTTP_ACCEPT_LANGUAGE');
            $deviceType = $request->server('HTTP_DEVICE_TYPE');
            $version = $request->server('HTTP_VERSION');
            $Input = Input::all();
            $Input['promo'] = $Input['promo_code'];
            $Input['for_category'] = explode(',', $Input['category_id']);
            $user = JWTAuth::parseToken()->authenticate();
            $returnData = self::CheckPromo($Input,$user->id,$language,$deviceType,$version);
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getMessage().'-'.$e->getLine(), '', '');
        }            
        return response($returnData);
    }

    //###############################################################
    //Function Name: checkPromo
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to verify promo code entered by user
    //In Params:     promo code, user id, language, device type, app version
    //Return:        json
    //Date:          4th Dec, 2018
    //###############################################################
    public function checkPromo($Input,$userId,$language='en',$deviceType='A',$version=''){     
        try {
            $whatPromo = PromoCode::where('promo_code',$Input['promo'])->where('status',1)->first();
            if(empty($whatPromo)){
                $returnMessage = $language=='en'?'PROMO_NOT_APPLICABLE':($language=='chi'?'PROMO_NOT_APPLICABLE_CHINESE':'PROMO_NOT_APPLICABLE_RU');
                //$returnMessage = $language=='en'?'PROMO_NOT_APPLICABLE':'PROMO_NOT_APPLICABLE_CHINESE';
                $returnData = UtilityController::Generateresponse(false, $returnMessage, Response::HTTP_BAD_REQUEST);
            } elseif ($whatPromo['code_for']==1||$whatPromo['code_for']==2) {
                $returnData = self::Checkdistributorcode($Input,$userId,$language);
            } else {
                $checkIfdistributorExists = DistributorsReferrals::select('distributors_id')->where('user_id',$userId)->first();
                $checkExists = (!empty($checkIfdistributorExists)?PromoCode::where(function ($query) use($checkIfdistributorExists){
                        $query->where('distributor_id',$checkIfdistributorExists['distributors_id'])->orWhereNull('distributor_id');
                    }):PromoCode::whereNull('distributor_id'));

                $checkExists = $checkExists->where('promo_code',$Input['promo'])->where('to_date','>=',Carbon::now())->where('status',1)->exists();
                if($checkExists){
                    $finalPrice = 0;
                    if(PromoCode::where('promo_code',$Input['promo'])->where('promo_type',1)->count()==1){
                        $promoData = PromoCode::where('promo_code',$Input['promo'])->first()->toArray();
                        $promoData['video_category_id'] = array_values(explode(',', $promoData['video_category_id']));
                        $Input['for_category'] = array_values($Input['for_category']);
                        $arraysAreEqual = ($Input['for_category'] == $promoData['video_category_id']);
                        if($arraysAreEqual){
                            if($Input['price_type'] == 1) {
                                $price = array_sum(VideoCategory::whereIn('id',$Input['for_category'])->get()->pluck('category_price')->toArray());
                            } else if($Input['price_type'] == 2) {
                                $price = array_sum(VideoCategory::whereIn('id',$Input['for_category'])->get()->pluck('category_price_sgd')->toArray());
                            }
                            if($promoData['discount_type']==0){
                                $finalPrice = (($price*$promoData['discount_of'])/100);
                                $finalPrice = $price - $finalPrice;
                            } else {
                                $finalPrice = $price - $promoData['discount_of'];
                            }
                            $finalPrice = round($finalPrice,2);
                            $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1,$finalPrice);
                        } else {
                            $arraysAreEqual = array_diff($promoData['video_category_id'], $Input['for_category']);
                            $alsoBuy = implode(', ', VideoCategory::whereIn('id',$arraysAreEqual)->get()->pluck('category_name')->toArray());
                            $returnMessage = $language=='en'?'This Promo Code is only applicable if you purchase '.$alsoBuy:'此优惠码只限于认购'.$alsoBuy;
                            $returnData = UtilityController::Generateresponse(false, $returnMessage, Response::HTTP_BAD_REQUEST);
                        }   
                    } else {
                        $promoData = PromoCode::with('category_detail')->where('promo_code',$Input['promo'])->whereIn('video_category_id',$Input['for_category'])->get()->toArray();
                        if(!empty($promoData) && count($Input['for_category'])==count($promoData)){
                            foreach ($promoData as $key => $value)
                                if($Input['price_type'] == 1) {
                                    $finalPrice += ($value['discount_type']==0?$value['category_detail']['category_price'] - (($value['category_detail']['category_price']*$value['discount_of'])/100):$value['category_detail']['category_price'] - $value['discount_of']);
                                } else if($Input['price_type'] == 2) {
                                    $finalPrice += ($value['discount_type']==0?$value['category_detail']['category_price_sgd'] - (($value['category_detail']['category_price_sgd']*$value['discount_of'])/100):$value['category_detail']['category_price_sgd'] - $value['discount_of']);
                                }
                            $finalPrice = round($finalPrice,2);
                            $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1,$finalPrice);
                        } else {
                            $returnMessage = $language=='en'?'PROMO_NOT_APPLICABLE':($language=='chi'?'PROMO_NOT_APPLICABLE_CHINESE':'PROMO_NOT_APPLICABLE_RU');
                            //$returnMessage = $language=='en'?'PROMO_NOT_APPLICABLE':'PROMO_NOT_APPLICABLE_CHINESE';
                            $returnData = UtilityController::Generateresponse(false, $returnMessage, Response::HTTP_BAD_REQUEST);
                        }
                    }
                } else {
                    $returnMessage = $language=='en'?'PROMO_NOT_APPLICABLE':($language=='chi'?'PROMO_NOT_APPLICABLE_CHINESE':'PROMO_NOT_APPLICABLE_RU');
                    //$returnMessage = $language=='en'?'PROMO_NOT_APPLICABLE':'PROMO_NOT_APPLICABLE_CHINESE';
                    $returnData = UtilityController::Generateresponse(false, $returnMessage, Response::HTTP_BAD_REQUEST);
                }
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return $returnData;
    }

    //###############################################################
    //Function Name: Checkdistributorcode
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       if user enters distributor referal code while availing promo code, then navigate user accordingly
    //In Params:     Input parameter for CkeckPromo method
    //Return:        json
    //Date:          6th Dec, 2018
    //###############################################################
    public function Checkdistributorcode($Input,$userId,$language='en'){     
        try {
            \DB::beginTransaction();
                $promoData = PromoCode::where('promo_code',$Input['promo'])->where('status',1)->first()->toArray();
                $checkIfdistributorExists = DistributorsReferrals::where('user_id',$userId)->first();
                if(!empty($checkIfdistributorExists)){
                    $distributorDetail = $promoData['code_for']==1?Distributors::where('referal_code',$Input['promo'])->first():DistributorsTeamMembers::where('referal_code',$Input['promo'])->first();
                    if(!($userId==$checkIfdistributorExists['user_id'] && $promoData['distributor_id']==$checkIfdistributorExists['distributors_id'])||$distributorDetail['status']!=1){
                        $returnMessage = $language=='en'?'PROMO_NOT_APPLICABLE':($language=='chi'?'PROMO_NOT_APPLICABLE_CHINESE':'PROMO_NOT_APPLICABLE_RU');
                        //$returnMessage = $language=='en'?'PROMO_NOT_APPLICABLE':'PROMO_NOT_APPLICABLE_CHINESE';
                        $returnData = UtilityController::Generateresponse(false, $returnMessage, Response::HTTP_BAD_REQUEST);
                        return $returnData;
                    }
                    if($checkIfdistributorExists['distributor_team_member_id']!='' && $promoData['distributor_team_member_id']!=$checkIfdistributorExists['distributor_team_member_id']){
                        $returnMessage = $Input['language']==1?'PROMO_NOT_APPLICABLE':($Input['language']==2?'PROMO_NOT_APPLICABLE_CHINESE':'PROMO_NOT_APPLICABLE_RU');
                        //$returnMessage = $Input['language']==1?'PROMO_NOT_APPLICABLE':'PROMO_NOT_APPLICABLE_CHINESE';
                        $returnData = UtilityController::Generateresponse(false, $returnMessage, Response::HTTP_BAD_REQUEST);
                        return $returnData;
                    }
                }
                $Input['for_category'] = array_values($Input['for_category']);
                $price = array_sum(VideoCategory::whereIn('id',$Input['for_category'])->get()->pluck('category_price')->toArray());
                if($promoData['discount_type']==0){
                    $finalPrice = (($price*$promoData['discount_of'])/100);
                    $finalPrice = $price - $finalPrice;
                } else {
                    $finalPrice = $price - $promoData['discount_of'];
                }
                $finalPrice = round($finalPrice,2);
                $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', 1,$finalPrice);
            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', '', '');
        }            
        return $returnData;
    }
}
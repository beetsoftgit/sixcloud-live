<?php

namespace App\Http\Controllers\v2;

use App\User;
use App\Country;
use App\State;
use App\City;
use App\DeviceTokens;
use App\UserEmailLog;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\UtilityController;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Mail;
use URL;
use Illuminate\Support\Facades\Input;

use Qcloud\Sms\SmsSingleSender;
use Qcloud\Sms\SmsMultiSender;
use Qcloud\Sms\SmsVoiceVerifyCodeSender;
use Qcloud\Sms\SmsVoicePromptSender;
use Qcloud\Sms\SmsStatusPuller;
use Qcloud\Sms\SmsMobileStatusPuller;

use Qcloud\Sms\VoiceFileUploader;
use Qcloud\Sms\FileVoiceSender;
use Qcloud\Sms\TtsVoiceSender;


class UserController extends Controller
{    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    } 

    //###############################################################
    //Function Name: Globalvariables
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to set and return global variables
    //In Params:     void
    //Return:        json
    //Date:          25th Oct, 2018
    //###############################################################
    public static function Globalvariables(){     
        if(strpos(url('/'), 'sixclouds.net/api') !== false || strpos(url('/'), 'sixclouds.net/beta/api') !== false || strpos(url('/'), 'localhost') !== false || strpos(url('/'), '192.168.1.') !== false){
            $globalVariable['CURRENT_DOMAIN']      = 'net';
            $globalVariable['APP_ID_SMS']          = 1400153255;
            $globalVariable['APP_KEY_SMS']         = '805ec6a673101f6026acb5296997c7d6';
            $globalVariable['APP_TEMPLATE_ID_SMS'] = 216554;
            $globalVariable['SMS_SIGN']            = 'SixClouds';
        } elseif(strpos(url('/'), 'sixclouds.cn/api') !== false || strpos(url('/'), 'sixclouds.cn/beta/api') !== false) {
            $globalVariable['CURRENT_DOMAIN']      = 'cn';
            $globalVariable['APP_ID_SMS']          = 1400137460;
            $globalVariable['APP_KEY_SMS']         = 'f91566bd6100b84e6af5df28a57bcae4';
            $globalVariable['APP_TEMPLATE_ID_SMS'] = 229997;
            $globalVariable['SMS_SIGN']            = '重庆六云';
        }
        return $globalVariable;
        // $GLOBALS['variable'] = something;
    }

    //###############################################################
    //Function Name: allUsers
    //Author:        Ketan Solanki <ketan@creolestudios.com>
    //Purpose:       To get all users
    //In Params:     Void
    //Return:        json
    //Date:          7th August 2018
    //###############################################################
    public function allUsers(){            

        $data = User::all()->toArray();            
        return response()->json($data);
    }

    //###############################################################
    //Function Name: allCountries
    //Author:        Nivedita <nivedita@creolestudios.com>
    //Purpose:       To get all countries list
    //In Params:     Void
    //Return:        json
    //Date:          23rd August 2018
    //###############################################################
    public function allCountries(){            
        try{
            $data = Country::select('id','sortname','name','phonecode')->whereIn('others',array(2,3))->where('status',1)->orderBy('name')->get();
            if(!empty($data)){
                $returnData = UtilityController::Generateresponse(true, "GOT_DATA", Response::HTTP_OK, $data);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'GENERAL_NO_DATA', Response::HTTP_NO_CONTENT);
            }
        } catch (\Exception $e) {                                             
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
        return response($returnData,$returnData['code']);
    }

    //###############################################################
    //Function Name: allStates
    //Author:        Nivedita <nivedita@creolestudios.com>
    //Purpose:       To get all states list
    //In Params:     country id
    //Return:        json
    //Date:          23rd August 2018
    //###############################################################
    public function allStates(Request $request, $id = ''){            
        try {
            if ($id != '' && is_numeric($id)) {
                $states = State::select('id', 'name')->where('country_id', $id)->orderBy('name')->get();
                if($states->count()>0)
                    $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', Response::HTTP_OK, $states);
                else
                    $returnData = UtilityController::Generateresponse(false, 'GENERAL_NO_DATA', Response::HTTP_NO_CONTENT);
            } else {
                $returnData = UtilityController::Generateresponse(false, 'INVALID_COUNTRY_ID', Response::HTTP_EXPECTATION_FAILED, '');
            }
        } catch (\Exception $e) {                                             
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');            
        }
        return response($returnData,$returnData['code']);
        
    }

    //###############################################################
    //Function Name: allCities
    //Author:        Nivedita <nivedita@creolestudios.com>
    //Purpose:       To get all cities list
    //In Params:     state id
    //Return:        json
    //Date:          23rd August 2018
    //###############################################################
    public function allCities(Request $request, $country_id = '', $state_id = ''){     
        try {
            if ($state_id != '' && is_numeric($state_id)) {
                $cities = City::select('id', 'name')->where('state_id', $state_id)->orderBy('name')->get();
                if($cities->count()>0)
                    $returnData = UtilityController::Generateresponse(true, 'GOT_DATA', Response::HTTP_OK, $cities);
                else
                    $returnData = UtilityController::Generateresponse(false, 'GENERAL_NO_DATA', Response::HTTP_NO_CONTENT);
            }
            else {
                $returnData = UtilityController::Generateresponse(0, 'INVALID_STATE_ID', Response::HTTP_EXPECTATION_FAILED, '');
            }
        }catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response($returnData,$returnData['code']);
    } 

    //###############################################################
    //Function Name: postCreateuser
    //Author:        Ketan Solanki <ketan@creolestudios.com>
    //Purpose:       To create new user
    //In Params:     user create parameters
    //Return:        Newly generated user parameters.
    //###############################################################
    public function postCreateuser(Request $request) {
        try {
            $wholePath = substr(URL::to('/'), 0, -3);
            $pathToStore = explode('/', $wholePath);
            if($pathToStore[3]=='beta')
                $pathToStore = '/var/www/html/beta/public/uploads/user_profile/';
            else
                $pathToStore = '/var/www/html/public/uploads/user_profile/';
            \DB::beginTransaction();
            $returnData = UtilityController::Setreturnvariables();  // Global declaration of response and input parameters.
            $inputData = $request->all();
            ##checking if user selects other option in country list
            if($inputData['country_id']=='other'){
                $existingCountry = Country::where('name',$inputData['country_text'])->first();
                if(empty($existingCountry)){
                    $country['name']         = $inputData['country_text'];
                    $country['phonecode']    = $inputData['phonecode'];
                    $country['others']       = 1;
                    $country['status']       = 2;
                    $countryResult           = UtilityController::Makemodelobject($country,'Country','');
                    $inputData['country_id'] = $countryResult['id'];
                } else {
                    $inputData['country_id'] = $existingCountry['id'];
                    $inputData['phonecode']  = $existingCountry['phonecode']; 
                }
                $state['name']             = $inputData['state_text'];
                $state['country_id']       = $inputData['country_id'];
                $stateResult               = UtilityController::Makemodelobject($state,'State','');
                $inputData['provience_id'] = $stateResult['id'];

                $city['name']         = $inputData['city_text'];
                $city['state_id']     = $stateResult['id'];
                $cityResult           = UtilityController::Makemodelobject($city,'City','');
                $inputData['city_id'] = $cityResult['id'];
               
            }
            ## Defining validation rules
            $rule = array(
                'first_name'    => 'required',
                'last_name'     => 'required',
                'email_address' => 'required|email|unique:users,email_address',
                'country_id'    => 'required',
                'contact'       => 'required|numeric|unique:users,contact',
                'dob'           => 'required|date',
                'time_zone'     => 'required',
                'password'      => 'required|min:8',
                'token'         => 'required',
                'os_type'       => 'required',
            );
            $validator = \Validator::make($inputData, $rule); // Execute validation.                                    
            if ($validator->fails()) {
                $validations = $validator->messages()->toArray();
                    if(array_key_exists('email_address', $validations)){
                        $returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'EMAIL_ALREADY_EXISTS':($request->server('HTTP_ACCEPT_LANGUAGE')=='chi'?'EMAIL_ALREADY_EXISTS_CHINESE':'EMAIL_ALREADY_EXISTS_RU'));
                        //$returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'EMAIL_ALREADY_EXISTS':'EMAIL_ALREADY_EXISTS_CHINESE');
                        $returnData = UtilityController::Generateresponse(0, $returnMessage, Response::HTTP_CONFLICT);
                    } elseif (array_key_exists('contact', $validations)) {
                        $returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'CONTACT_ALREADY_EXISTS':'CONTACT_ALREADY_EXISTS_CHINESE');
                        $returnData = UtilityController::Generateresponse(0, $returnMessage, Response::HTTP_CONFLICT);
                    } else {
                        $returnData = UtilityController::Generateresponse(0, 'VALIDATION_ERROR', Response::HTTP_CONFLICT, $validator->messages());
                    }
            } else {
                //Appending other details required in the table
                $inputData['nationality_id'] = null;
                $inputData['password']       = Hash::make($inputData['password']);
                $inputData['school_id']      = 0;
                $inputData['display_name']   = $inputData['first_name']." ".$inputData['last_name'];                                
                if (!empty($inputData['image']) && !is_string($inputData['image'])) {                                                                        
                    if ($inputData['image']->getClientSize() <= UtilityController::Getmessage('FILE_SIZE_2')) {                        
                        $extension = File::extension($inputData['image']->getClientOriginalName());                        
                        if (in_array($extension, array('jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG'))) {
                            $file            = $inputData['image']->getMimeType();
                            $imageName       = date("dmYHis") . rand();
                            // $DestinationPath = public_path() . Config('constants.paths.USER_PROFILE_PATH');
                            $DestinationPath = $pathToStore;
                            $inputData['image']->move($DestinationPath, $imageName);                            
                            $inputData['image']  = $imageName;
                        } else {
                            $returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'ONLY_JPG_PNG_JPEG':'ONLY_JPG_PNG_JPEG_CHINESE');
                            $returnData = UtilityController::Generateresponse(0, $returnMessage, Response::HTTP_UNSUPPORTED_MEDIA_TYPE);
                            return response($returnData,$returnData['code']);                           
                        }
                    } else {
                            $returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'PROFILE_PHOTO_SIZE_LARGE':'PROFILE_PHOTO_SIZE_LARGE_CHINESE');
                            $returnData = UtilityController::Generateresponse(0, $returnMessage, Response::HTTP_REQUEST_ENTITY_TOO_LARGE);
                            return response($returnData,$returnData['code']);
                    }
                } else {
                    $inputData['image'] = 'NULL';
                }
                /*$phonecode  = 0;//Inititalize Phone code with 0
                $countryData = Country::find($inputData['country_id'])->toArray();
                if(!empty($countryData)){
                    $phonecode  = $countryData['phonecode'];
                }
                $inputData['phonecode']   = $phonecode;*/
                $inputData['student_id']  = "";
                $inputData['user_number'] = UtilityController::GenerateRunningNumber('User','user_number',"U");
                $inputData['timezone']    = $inputData['time_zone'];
                $inputData['is_ignite']   = 1;
                if($inputData['os_type'] == 2)
                    $osType = 'android';
                elseif($inputData['os_type'] == 1)
                    $osType = 'IOS';
                $inputData['platform']    = $osType;
                //$inputData['status']      = 1;           
                     
                $modelObj = UtilityController::Makemodelobject($inputData, 'User', '');
                if ($modelObj) {
                    $slug['slug']                    = str_slug($inputData['display_name'], '-');
                    $slug['slug']                    = $slug['slug'] . ':' . base64_encode($modelObj->id);
                    $result                          = UtilityController::Makemodelobject($slug, 'User', '', $modelObj->id);
                    $userData['user_details']        = User::find($modelObj->id)->toArray();
                    if($userData['user_details']['image'] != '' || $userData['user_details']['image'] != 'null' || $userData['user_details']['image'] != 'NULL'){
                            $userData['user_details']['image'] = url_path()."/uploads/user_profile/".$userData['user_details']['image'];
                    }
                    $userData['authorization_token'] = JWTAuth::attempt($request->only('email_address', 'password'));//To genegerate the Authorization Token
                    $deviceTokenData['user_id']      = $modelObj->id;
                    /*$deviceTokenData['device_id']    = $inputData['device_id'];*/
                    $deviceTokenData['token']        = $inputData['token'];
                    $deviceTokenData['os_type']      = $inputData['os_type'];
                    $modelObjDeviceTokens            = UtilityController::Makemodelobject($deviceTokenData, 'DeviceTokens', '');  
                    $globals = self::Globalvariables();
                    $current_domain = $globals['CURRENT_DOMAIN'];
                    if($current_domain=='net') {
                        /*Code to Send Mail to the User after Successfull Registartion : Start*/
                        $registerEmailToken['account_verification_token'] = rand().base64_encode($result['email_address']).date('dmYHis').rand();
                        $registerEmailToken['email']                      = base64_encode($result['email_address']);
                        $registerEmailToken['current_language']           = 1;
                        $resultEmailToken                                 = UtilityController::Makemodelobject($registerEmailToken,'User','',$result['id']);
                        $verificationURL                                  = substr(URL::to('/'), 0, -3).'Registrationaccountverification?account_verification_token='.$registerEmailToken['account_verification_token'].'&email='.$registerEmailToken['email'].'&language='.$registerEmailToken['current_language'];
                        $registerEmail['user_name']                       = $result['first_name'].' '.$result['last_name'];
                        $registerEmail['registerLink']                    = $verificationURL;
                        $registerEmail['email_address']                   = $result['email_address'];
                        $registerEmail['current_language']                = 1;
                        
                        // $registerEmail['subject']='SixClouds : Account Verification';
                        $registerEmail['subject']=$registerEmail['current_language']==1?'SixClouds : Account Verification':'六云 ： 帐户验证';
                        $supportEmail = 'support@sixclouds.net';
                        /*$registerEmail['content']='
                            Your account has been created. Please click on the link below to complete your registration process. 
                            <br><br>'.
                            $registerEmail['registerLink']
                            .'<br><br>
                            If the link does not work, copy the link to the browser address bar.
                            <br><br>
                            If you have any queries, please contact '.$supportEmail;*/

                        $registerEmail['content']=$registerEmail['current_language']==1?'
                        Your account has been created. Please click on the button below to complete your registration process. 
                        <br><br><a href="'.$registerEmail['registerLink'].'" class="" style="color: #fff;
                                               background-color: #009bdd;
                                               border-color: #bd383e;
                                               text-decoration: none;display: inline-block;
                                               padding: 5px 10px;
                                               margin-bottom: 0;
                                               margin-top: 3px;
                                               font-size: 14px;
                                               font-weight: 400;
                                               line-height: 1.42857143;
                                               text-align: center;
                                               white-space: nowrap;
                                               vertical-align: middle;
                                               -ms-touch-action: manipulation;
                                               touch-action: manipulation;
                                               cursor: pointer;
                                               -webkit-user-select: none;
                                               -moz-user-select: none;
                                               -ms-user-select: none;
                                               user-select: none;
                                               background-image: none;
                                               border: 1px solid transparent;
                                               border-radius: 4px;">Click Here</a><br><br>
                        If the link does not work, copy the below link to the browser address bar.
                        <br><br>'.$registerEmail['registerLink'].'<br><br>
                        If you have any queries, please contact '.$supportEmail:'

                        您的帐号已经建立。 请点击以下链接完成注册。
                        <br><br><a href="'.$registerEmail['registerLink'].'" class="" style="color: #fff;
                                               background-color: #009bdd;
                                               border-color: #bd383e;
                                               text-decoration: none;display: inline-block;
                                               padding: 5px 10px;
                                               margin-bottom: 0;
                                               margin-top: 3px;
                                               font-size: 14px;
                                               font-weight: 400;
                                               line-height: 1.42857143;
                                               text-align: center;
                                               white-space: nowrap;
                                               vertical-align: middle;
                                               -ms-touch-action: manipulation;
                                               touch-action: manipulation;
                                               cursor: pointer;
                                               -webkit-user-select: none;
                                               -moz-user-select: none;
                                               -ms-user-select: none;
                                               user-select: none;
                                               background-image: none;
                                               border: 1px solid transparent;
                                               border-radius: 4px;">请点击这里</a><br><br>
                        如果链接不起作用，请将链接复制到浏览器地址栏。
                        <br><br>'.$registerEmail['registerLink'].'<br><br>
                        如果您有任何疑问，请联系 '.$supportEmail;

                        $registerEmail['footer_content']=$registerEmail['current_language']==1?'From,<br /> SixClouds':'六云 ';

                        $registerEmail['footer']=$registerEmail['current_language']==1?'SixClouds':'六云';
                        Mail::send('emails.email_template', $registerEmail, function ($message) use ($registerEmail) {
                            $message->from('noreply@sixclouds.cn', 'SixClouds');
                            $message->to($registerEmail['email_address'])->subject($registerEmail['subject']);

                        });
                        //$returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'USER_CREATED':'USER_CREATED_CHINESE');
                        $returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'USER_CREATED':($request->server('HTTP_ACCEPT_LANGUAGE')=='chi'?'USER_CREATED_CHINESE':'USER_CREATED_RU'));
                        $returnData = UtilityController::Generateresponse(1, $returnMessage, Response::HTTP_CREATED, $userData);
                        /*Code to Send Mail to the User after Successfull Registartion : End*/
                    } else {
                        /* Send OTP for phone number verification : start */
                            $otpInput['phonecode']  = $modelObj->phonecode;
                            $otpInput['contact']  = $modelObj->contact;
                            $otpInput['email_address'] = $modelObj->email_address;
                            $otpReturn = self::Sendotp($otpInput);
                            if($otpReturn['status']==0){
                                throw new \Exception($otpReturn['message']);
                            } elseif($otpReturn['status']==1){
                                $otp['otp'] = $otpReturn['otp'];
                                $storeOtp = UtilityController::Makemodelobject($otp, 'User', '', $modelObj->id);
                                // $userData['user_details']['otp'] = $otpReturn['otp'];
                                $returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'USER_CREATED_CN':'USER_CREATED_CN_CHINESE');
                                $returnData = UtilityController::Generateresponse(1, $returnMessage, Response::HTTP_CREATED, $userData);
                            }
                        /* Send OTP for phone number verification : end */
                    }
                    // $returnData = UtilityController::Generateresponse(1, 'USER_CREATED', Response::HTTP_OK, $userData);
                    \DB::commit();
                } else {
                    $returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'GENERAL_ERROR':'GENERAL_ERROR_CHINESE');
                    $returnData = UtilityController::Generateresponse(false, $returnMessage, Response::HTTP_CONFLICT, '');
                }
            }            
        } catch (\Exception $e) { 
            \DB::rollback();                                               
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');            
        }
        return response($returnData,$returnData['code']);
    }

    //###############################################################
    //Function Name: Sendotp
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to send the otp for phone verification
    //In Params:     country_code, phone_number
    //Return:        json
    //Date:          10th Oct 2018
    //###############################################################
    public function Sendotp($Input) {
            if(strpos(url('/'), 'sixclouds.net/api') !== false || strpos(url('/'), 'sixclouds.net/beta/api') !== false ||  strpos(url('/'), 'localhost') !== false){
                $CURRENT_DOMAIN      = 'net';
                $APP_ID_SMS          = 1400153255;
                $APP_KEY_SMS         = '805ec6a673101f6026acb5296997c7d6';
                $APP_TEMPLATE_ID_SMS = 220607;
                $SMS_SIGN            = 'SixClouds';
            }elseif(strpos(url('/'), 'sixclouds.cn/api') !== false){
                $CURRENT_DOMAIN      = 'cn';
                $APP_ID_SMS          = 1400137460;
                $APP_KEY_SMS         = 'f91566bd6100b84e6af5df28a57bcae4';
                $APP_TEMPLATE_ID_SMS = 191050;
                $SMS_SIGN            = '重庆六云教育';
            }
            // $Input = Input::all();
            if(!array_key_exists('phonecode', $Input)){
                $returnData['status'] = 0;
                $returnData['message'] = UtilityController::getMessage('COUNTRY_CODE_REQUIRED');
                return $returnData;
                // throw new \Exception(UtilityController::getMessage('COUNTRY_CODE_REQUIRED'));
            }
            if(!array_key_exists('contact', $Input)){
                $returnData['status'] = 0;
                $returnData['message'] = UtilityController::getMessage('PHONE_NUMBER_REQUIRED');
                return $returnData;
                // throw new \Exception(UtilityController::getMessage('PHONE_NUMBER_REQUIRED'));
            }
            if(!array_key_exists('email_address', $Input)){
                $returnData['status'] = 0;
                $returnData['message'] = UtilityController::getMessage('EMAIL_ADDRESS_REQUIRED');
                return $returnData;
                // throw new \Exception(UtilityController::getMessage('PHONE_NUMBER_REQUIRED'));
            }
            if(isset($Input['contact']) && isset($Input['phonecode'])) {
                    ## SMS application SDK AppID
                    $appid = $APP_ID_SMS; // Starting with 1400
                    ## SMS application SDK AppKey
                    $appkey = $APP_KEY_SMS;
                    ## SMS template ID, you need to apply in the SMS application
                    $templateId = $APP_TEMPLATE_ID_SMS;
                    ## signature
                    $smsSign = $SMS_SIGN;
                    ## Mobile number that needs to send a text message
                    // $phoneNumbers = ["18883708501"];
                    $phoneNumber = $Input['contact'];
                    $countryCode  = $Input['phonecode'];
                    $ssender   = new SmsSingleSender($appid, $appkey);
                    $randomOtp = rand(1000,9999);
                    $params    = [$randomOtp, "5"];
                    $returnData = $ssender->sendWithParam($countryCode, $phoneNumber, $templateId, $params, $smsSign, "", "");
                    $returnData = json_decode($returnData,true);
                    if($returnData['errmsg']=='OK'||$returnData['errmsg']=='ok') {
                            $returnData['status'] = 1;
                            $returnData['message'] = UtilityController::getMessage($Input['language']=='en'?'OTP_SENT':'OTP_SENT_CHINESE');
                            $returnData['otp'] = $randomOtp;
                    } else {
                        $returnData['status'] = 0;
                        $returnData['message'] = UtilityController::getMessage($Input['language']=='en'?'INVALID_PHONE_NUMBER':'INVALID_PHONE_NUMBER_CHINESE');
                        // $returnData = UtilityController::Generateresponse(false, 'INVALID_PHONE_NUMBER', 0);
                    }
            } else {
                $returnData['status'] = 0;
                $returnData['message'] = UtilityController::getMessage('GENERAL_ERROR');
                // $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', 400, '');
            }            
        return $returnData;
    }

    //###############################################################
    //Function Name: resendOtp
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to resend the otp
    //In Params:     country code, phone number
    //Return:        json
    //Date:          25th Oct, 2018
    //###############################################################
    public function resendOtp(Request $request){
        try {
            \DB::beginTransaction();
            $Input = Input::all();
            $language   = $request->server('HTTP_ACCEPT_LANGUAGE');
            $deviceType = $request->server('HTTP_DEVICE_TYPE');
            $version    = $request->server('HTTP_VERSION');
            if(!array_key_exists('phonecode', $Input)){
                throw new \Exception(UtilityController::getMessage($language=='en'?'COUNTRY_CODE_REQUIRED':'COUNTRY_CODE_REQUIRED_CHINESE'));
            }
            if(!array_key_exists('contact', $Input)){
                throw new \Exception(UtilityController::getMessage($language=='en'?'PHONE_NUMBER_REQUIRED':'PHONE_NUMBER_REQUIRED_CHINESE'));
            }
            if(!array_key_exists('email_address', $Input)){
                throw new \Exception(UtilityController::getMessage($language=='en'?'EMAIL_ADDRESS_REQUIRED':'EMAIL_ADDRESS_REQUIRED_CHINESE'));
            }
            if(array_key_exists('contact_old', $Input)&&array_key_exists('phonecode_old', $Input))
                $UserExists = User::where('email_address',$Input['email_address'])->where('contact',$Input['contact_old'])->where('phonecode',$Input['phonecode_old'])->first();
            else
                $UserExists = User::where('email_address',$Input['email_address'])->where('contact',$Input['contact'])->where('phonecode',$Input['phonecode'])->first();
            if(!empty($UserExists)){
                $otpInput['phonecode']     = $Input['phonecode'];
                $otpInput['contact']       = $Input['contact'];
                $otpInput['email_address'] = $Input['email_address'];
                $otpInput['language']      = $language;
                $otpReturn                 = self::Sendotp($otpInput);
                if($otpReturn['status']==0){
                    throw new \Exception($otpReturn['message']);
                } elseif($otpReturn['status']==1){
                    $otpStore['otp'] = $otpReturn['otp'];
                    $storeOtp = UtilityController::Makemodelobject($otpStore, 'User', '', $UserExists['id']);
                    $returnMessage = $language=='en'?'OTP_SENT':'OTP_SENT_CHINESE';
                    $returnData = UtilityController::Generateresponse(true, $returnMessage, Response::HTTP_OK);
                    \DB::commit();
                }
            } else {
                $returnMessage = $language=='en'?'NO_USER':'NO_USER_CHINESE';
                $returnData = UtilityController::Generateresponse(false, $returnMessage, Response::HTTP_NOT_FOUND);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getMessage(), Response::HTTP_BAD_REQUEST);
        }            
        return response($returnData,$returnData['code']);
    }

    //###############################################################
    //Function Name: verifyOtp
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       to verify the otp sent to user
    //In Params:     country code, phone number, otp
    //Return:        json
    //Date:          10th Oct, 2018
    //###############################################################
    public function verifyOtp(Request $request){     
        try {
            $Input = Input::all();
            $language   = $request->server('HTTP_ACCEPT_LANGUAGE');
            $deviceType = $request->server('HTTP_DEVICE_TYPE');
            $version    = $request->server('HTTP_VERSION');
            if(!array_key_exists('phonecode', $Input)){
                throw new \Exception(UtilityController::getMessage($language=='en'?'COUNTRY_CODE_REQUIRED':'COUNTRY_CODE_REQUIRED_CHINESE'));
            }
            if(!array_key_exists('contact', $Input)){
                throw new \Exception(UtilityController::getMessage($language=='en'?'PHONE_NUMBER_REQUIRED':'PHONE_NUMBER_REQUIRED_CHINESE'));
            }
            if(!array_key_exists('email_address', $Input)){
                throw new \Exception(UtilityController::getMessage($language=='en'?'EMAIL_ADDRESS_REQUIRED':'EMAIL_ADDRESS_REQUIRED_CHINESE'));
            }
            if(!array_key_exists('otp', $Input)){
                throw new \Exception(UtilityController::getMessage($language=='en'?'OTP_REQUIRED':'OTP_REQUIRED_CHINESE'));
            }
            if(array_key_exists('contact_old', $Input)&&array_key_exists('phonecode_old', $Input))
                $correctOtp = User::where('email_address',$Input['email_address'])->where('contact',$Input['contact_old'])->where('phonecode',$Input['phonecode_old'])->where('otp',$Input['otp'])->exists();
            else
                $correctOtp = User::where('email_address',$Input['email_address'])->where('contact',$Input['contact'])->where('phonecode',$Input['phonecode'])->where('otp',$Input['otp'])->exists();
            if($correctOtp){
                \DB::beginTransaction();
                    if(array_key_exists('contact_old', $Input)&&array_key_exists('phonecode_old', $Input))
                        $memberVerified = User::where('email_address',$Input['email_address'])->where('contact',$Input['contact_old'])->where('phonecode',$Input['phonecode_old'])->where('otp',$Input['otp'])->update(['status'=>1,'otp'=>'']);
                    else
                        $memberVerified = User::where('email_address',$Input['email_address'])->where('contact',$Input['contact'])->where('phonecode',$Input['phonecode'])->where('otp',$Input['otp'])->update(['status'=>1,'otp'=>'']);
                    if($memberVerified){
                        $returnData = UtilityController::Generateresponse(true, 'VERIFIED', Response::HTTP_OK);
                    } else {
                        $returnData = UtilityController::Generateresponse(false, 'GENERAL_ERROR', Response::HTTP_UNAUTHORIZED);
                    }
                \DB::commit();
            } else {
                $returnMessage = $language=='en'?'INVALID_OTP':'INVALID_OTP_CHINESE';
                $returnData = UtilityController::Generateresponse(false, $returnMessage, Response::HTTP_UNAUTHORIZED);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getMessage(), Response::HTTP_BAD_REQUEST);
        }            
        return response($returnData,$returnData['code']);
    }

    //###############################################################
    //Function Name: postLogin
    //Author:        Ketan Solanki <ketan@creolestudios.com>
    //Purpose:       User authentication and JWtoken generation.
    //In Params:     Email Address & Password
    //Return:        Newly generated user parameters.
    //###############################################################
    public function postLogin(Request $request) {
        try {
            $globals = self::Globalvariables();
            $current_domain = $globals['CURRENT_DOMAIN'];
            \DB::beginTransaction();
            $returnData = UtilityController::Setreturnvariables();
            $inputData = $request->all();
            if(!array_key_exists('email_address', $inputData)&&$current_domain=='cn')
                throw new \Exception(UtilityController::getMessage('EMAIL_CONTACT_REQUIRED'));
            if(!array_key_exists('email_address', $inputData)&&$current_domain=='net')
                throw new \Exception(UtilityController::getMessage('EMAIL_ADDRESS_REQUIRED'));
            $rule = array(
                // 'email_address' => 'required|email',
                'password'      => 'required|min:8',
                /*'device_id'     => 'required',*/
                'token'         => 'required',
                'os_type'       => 'required',
            );            
            $validator = \Validator::make($inputData, $rule);
            if ($validator->fails()) {
                $returnData = UtilityController::Generateresponse(0, 'VALIDATION_ERROR', Response::HTTP_CONFLICT, $validator->messages());
            } else {
                if($current_domain=='net')
                    $token = JWTAuth::attempt($request->only('email_address', 'password'));

                if($current_domain=='cn'){
                    if(is_numeric($inputData['email_address'])){
                        $contactCheck['contact'] = $inputData['email_address'];
                        $contactCheck['password'] = $inputData['password'];
                        $token = JWTAuth::attempt($contactCheck);
                    }
                    else
                        $token = JWTAuth::attempt($request->only('email_address', 'password'));
                }

                if (!$token) {
                    //TOKEN_GENERATION_FAILED
                    //$returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'INVALID_CREDENTIALS':'INVALID_CREDENTIALS_CHINESE');
                    $returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'INVALID_CREDENTIALS':($request->server('HTTP_ACCEPT_LANGUAGE')=='chi'?'INVALID_CREDENTIALS_CHINESE':'INVALID_CREDENTIALS_RU'));
                    $returnData = UtilityController::Generateresponse(0, $returnMessage, Response::HTTP_UNAUTHORIZED);
                } else {
                    //TOKEN_GENERATED
                    $user = $request->user(); //  Getting user object
                    //if user is in-active then display error for the same
                    if (isset($user) && $user->status == 1) {
                        $responseData['authorization_token'] = $token;
                        $responseData['user']                = $user;
                        if($responseData['user']['image']   != '' || $responseData['user']['image'] != 'null' || $responseData['user']['image'] != 'NULL') {
                            $responseData['user']['image'] = "/public/uploads/user_profile/".$responseData['user']['image'];
                        }
                        
                        $getDeviceLoginResults = DeviceTokens::where("user_id",$user->id)->first();
                        // print_r($getDeviceLoginResults['token']); die;
                        if(!empty($getDeviceLoginResults) && ($getDeviceLoginResults['token'] != $inputData['token'])){
                            ##send push notification here and replace the data with new token;
                            ##Payload data you want to send to devices
                            $returnMessageLogOut = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'You have been logged out because you have logged in on another device.':'您已被注销，因为您已登录另一个设备上。');
                            $notifyData = array('message' => $returnMessageLogOut);
                            ##The recipient device tokens
                            $to = $getDeviceLoginResults['token'];
                            if($current_domain=='cn'){
                                ##Send it with Pushy
                                $this->sendPushNotification($notifyData, $to, $options=array());
                            } else {
                                $fields = 
                                [
                                    'to'   => $to,
                                    'data' => $notifyData
                                ];
                                $headers = 
                                [
                                   'Authorization: key=' . 'AAAA7AOKDos:APA91bGw0JKloe1PVNDA7boQ1RCFC6u2gUDzYKJSWtJERnNAyPvsyYwOrsZgiAOMxOYBjSM3rGf5BKGDUiz809VyukTfm2qheb8x7-fNZ3_byWV1elFzgrIPNaB476W9IKtnngW8tMis',
                                   'Content-Type: application/json'
                                ];
                                $ch = curl_init();
                                curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
                                curl_setopt( $ch,CURLOPT_POST, true );
                                curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                                curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
                                curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                                curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                $result = curl_exec($ch);
                                if ($result === FALSE) {
                                    throw new \Exception('Oops! FCM Send Error: ' . curl_error($ch));
                                }
                                curl_close( $ch );
                            }
                            ##END
                            $deleteDeviceTokensData = DeviceTokens::where("user_id",$user->id)->delete();
                        }
                        //Code to insert data into devicetokens tables
                        $deviceTokenData['user_id']             = $user->id;
                        $deviceTokenData['token']               = $inputData['token'];
                        $deviceTokenData['os_type']             = $inputData['os_type'];
                        $modelObjDeviceTokens                   = UtilityController::Makemodelobject($deviceTokenData, 'DeviceTokens', '');
                        $returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'TOKEN_GENERATED':'TOKEN_GENERATED_CHINESE');
                        $returnData                             = UtilityController::Generateresponse(1, $returnMessage, Response::HTTP_OK, $responseData);
                    } else {
                        if(isset($user) && $current_domain=='cn') {
                            $otpInput['phonecode']  = $user->phonecode;
                            $otpInput['contact']  = $user->contact;
                            $otpInput['email_address'] = $user->email_address;
                            $otpReturn = self::Sendotp($otpInput);
                            if($otpReturn['status']==0){
                                throw new \Exception($otpReturn['message']);
                            } elseif($otpReturn['status']==1){
                                $otp['otp'] = $otpReturn['otp'];
                                $storeOtp = UtilityController::Makemodelobject($otp, 'User', '', $user->id);
                                $responseData['authorization_token'] = $token;
                                $responseData['user']                = $user;
                                $returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'UNVERIFIED_OTP':'UNVERIFIED_OTP_CHINESE');
                                $returnData = UtilityController::Generateresponse(0, $returnMessage, Response::HTTP_FORBIDDEN,$responseData);
                            }
                        } elseif (isset($user) && $current_domain=='net' && $user->status == 0) {
                            $returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'UNVERIFIED_EMAIL':'UNVERIFIED_EMAIL_CHINESE');
                            $returnData = UtilityController::Generateresponse(0, $returnMessage, Response::HTTP_FORBIDDEN,$user);
                        } else {
                            //user is in-Active
                            $returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'USER_ACCOUNT_LOCKED':'USER_ACCOUNT_LOCKED_CHINESE');
                            $returnData = UtilityController::Generateresponse(0, $returnMessage, Response::HTTP_FORBIDDEN);
                        }
                    }
                }
                \DB::commit();
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(0, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        // All good so return the token
        return response($returnData,$returnData['code']);
    } 

    //###############################################################
    //Function Name: postLogout
    //Author:        Ketan Solanki <ketan@creolestudios.com>
    //Purpose:       To logout the User from the app and delete its tokens
    //In Params:     Authorization Token
    //Return:        Success/Error Message
    //###############################################################
    public function postLogout(Request $request) {
        try {
            $returnData = UtilityController::Setreturnvariables();
            $inputData = $request->all();  
            if(!array_key_exists('token', $inputData))
                throw new \Exception(UtilityController::getMessage('TOKEN_REQUIRED'));
            if(!array_key_exists('os_type', $inputData))
                throw new \Exception(UtilityController::getMessage('OS_TYPE_REQUIRED'));
            $token = JWTAuth::parseToken();
            $userAuth = JWTAuth::parseToken()->authenticate();
            $userID = $userAuth->id;
            $deleteDeviceTokensData = DeviceTokens::where("user_id",$userID)->where("token",$inputData['token'])->where("os_type",$inputData['os_type'])->delete();
            //Black list the token
            if($token->invalidate()){   
                $returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'USER_LOGGED_OUT':'USER_LOGGED_OUT_CHINESE');
                $returnData = UtilityController::Generateresponse(true, $returnMessage, Response::HTTP_OK);
            } else {
                $returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'USER_LOGGED_OUT_ERROR':'USER_LOGGED_OUT_ERROR_CHINESE');
                $returnData = UtilityController::Generateresponse(true, $returnMessage, Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        } catch (\Exception $e) {
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getMessage(), Response::HTTP_BAD_REQUEST);         
        }
        return response($returnData,$returnData['code']);        
    }

    //###############################################################
    //Function Name: Changepassword
    //Author:        Nivedita <nivedita@creolestudios.com>
    //Purpose:       To change user's password
    //In Params:     user password's parameters
    //Return:        updated password.
    //###############################################################
    public function Changepassword(Request $request) {
        try {        
            \DB::beginTransaction();

            $inputData = $request->all();
                $rule = array(
                'current_password' => 'required',
                'password'         => 'required|min:8',
                'confirm_password' => 'required|min:8',
            );
            $validator = \Validator::make($inputData, $rule); // Execute validation.
            if ($validator->fails()) {
                $validator = $validator->messages()->toArray();
                if(array_key_exists('confirm_password', $validator)){
                    $returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'CONFIRM_PASSWORD_REQUIRED':'CONFIRM_PASSWORD_REQUIRED_CHINESE');
                } elseif (array_key_exists('password', $validator)) {
                    //$returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'NEW_PASSWORD_REQUIRED':'NEW_PASSWORD_REQUIRED_CHINESE');
                    $returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'NEW_PASSWORD_REQUIRED':($request->server('HTTP_ACCEPT_LANGUAGE')=='chi'?'NEW_PASSWORD_REQUIRED_CHINESE':'NEW_PASSWORD_REQUIRED_RU'));
                } else {
                    $returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'CURRENT_PASSWORD_REQUIRED':'CURRENT_PASSWORD_REQUIRED_CHINESE');
                }
                $returnData = UtilityController::Generateresponse(0, 'VALIDATION_ERROR', Response::HTTP_BAD_REQUEST, UtilityController::getMessage($returnMessage));
            } else {

                $userAuth = JWTAuth::authenticate();
                $userID   = $userAuth->id; 
                $getUser  = User::where('id',$userID)->first();
                if(Hash::check($inputData['current_password'],$getUser['password'])) {
                    if($inputData['password']==$inputData['confirm_password']){
                        $result     = User::where('id',$userID)->update(['password'=>Hash::make($inputData['password'])]);
                        $returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'PASSWORD_CHANGED':'PASSWORD_CHANGED_CHINESE');
                        $returnData = UtilityController::Generateresponse(true, $returnMessage, Response::HTTP_OK,$result);
                        \DB::commit();
                    } else {
                        $returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'PASSWORD_AND_CONFIRM_PASSWORD_NOT_SAME':'PASSWORD_AND_CONFIRM_PASSWORD_NOT_SAME_CHINESE');
                        $returnData = UtilityController::Generateresponse(0, $returnMessage, Response::HTTP_EXPECTATION_FAILED);
                    }
                } else {
                    $returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'PASSWORD_OLD_INCORRECT':($request->server('HTTP_ACCEPT_LANGUAGE')=='chi'?'PASSWORD_OLD_INCORRECT_CHINESE':'PASSWORD_OLD_INCORRECT_RU'));
                    //$returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'PASSWORD_OLD_INCORRECT':'PASSWORD_OLD_INCORRECT_CHINESE');
                    $returnData = UtilityController::Generateresponse(0, $returnMessage, Response::HTTP_UNAUTHORIZED);
                }
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, $e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
        return response()->json($returnData,$returnData['code']);

    }

    //###############################################################
    //Function Name: Updateuserprofile
    //Author:        Nivedita <nivedita@creolestudios.com>
    //Purpose:       To update user profile
    //In Params:     user update profile parameters
    //Return:        updated profile.
    //###############################################################
    public function Updateuserprofile(Request $request) {
        try {
            $wholePath = substr(URL::to('/'), 0, -3);
            $pathToStore = explode('/', $wholePath);
            if($pathToStore[3]=='beta')
                $pathToStore = '/var/www/html/beta/public/uploads/user_profile/';
            else
                $pathToStore = '/var/www/html/public/uploads/user_profile/';
            \DB::beginTransaction();
            $input = $request->all();
            if(isset($input['country_id'])){
                if($input['country_id']=='other'){
                    if(empty($existingCountry)){   
                        $country['name']      = $input['country_text'];
                        $country['phonecode'] = $input['phonecode'];
                        $country['others']    = 1;
                        $country['status']    = 2;
                        $countryResult        = UtilityController::Makemodelobject($country,'Country');
                        $input['country_id']  = $countryResult['id'];
                    } else {
                        $input['country_id'] = $existingCountry['id'];
                    }
                    
                    $state['name']         = $input['state_text'];
                    $state['country_id']   = $input['country_id'];
                    $stateResult           = UtilityController::Makemodelobject($state,'State');
                    $input['provience_id'] = $stateResult['id'];

                    $city['name']     = $input['city_text'];
                    $city['state_id'] = $stateResult['id'];
                    $cityResult       = UtilityController::Makemodelobject($city,'City');
                    $input['city_id'] = $cityResult['id'];
                }
                if(isset($input['country_text']))
                    $existingCountry = Country::where('name',$input['country_text'])->first();
            }
            
            $userAuth     = JWTAuth::parseToken()->authenticate();
            $userID       = $userAuth->id; 
            if($userID){
                if(isset($input['email_address'])){

                    $UserExists             = User::where('email_address', $input['email_address'])->where('status', 1)->where('id',' != ', $userID)->first();
                    if(isset($input['old_email'])){
                        ##if changes email address then maintain log of that
                            if($input['old_email']!=$input['email_address']){
                                $dataUpdate['user_id']           = $userID;
                                $dataUpdate['old_email_address'] = $input['old_email'];
                                $dataUpdate['new_email_address'] = $input['email_address'];
                                $result                          = UtilityController::Makemodelobject($dataUpdate, 'UserEmailLog');
                            }
                    }else {
                        throw new \Exception(UtilityController::Getmessage($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'SEND_OLD_EMAIL':'SEND_OLD_EMAIL_CHINESE'));
                    }
                }
            }
            if (!empty($UserExists)) {
                $returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'EMAIL_ALREADY_EXISTS':($request->server('HTTP_ACCEPT_LANGUAGE')=='chi'?'EMAIL_ALREADY_EXISTS_CHINESE':'EMAIL_ALREADY_EXISTS_RU'));
                //$returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'EMAIL_ALREADY_EXISTS':'EMAIL_ALREADY_EXISTS_CHINESE');
                $returnData        = UtilityController::Generateresponse(false, $returnMessage, Response::HTTP_CONFLICT, '');   
            } else {
                if (!empty($input['image']) && !is_string($input['image'])) {
                    if ($input['image']->getClientSize() <= UtilityController::Getmessage('FILE_SIZE_2')) {
                        $extension = File::extension($input['image']->getClientOriginalName());                        
                        if (in_array($extension, array('jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG'))) {
                            $file            = $input['image']->getMimeType();
                            $imageName       = date("dmYHis") . rand();
                            // $DestinationPath = public_path() . Config('constants.paths.USER_PROFILE_PATH');
                            $DestinationPath = $pathToStore;
                            $input['image']->move($DestinationPath, $imageName);
                            $input['image']  = $imageName;
                        } else {
                            $returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'ONLY_JPG_PNG_JPEG':'ONLY_JPG_PNG_JPEG_CHINESE');
                            $returnData = UtilityController::Generateresponse(0, $returnMessage, Response::HTTP_UNSUPPORTED_MEDIA_TYPE, '');
                            return response($returnData, $returnData['code']);
                        }
                    }
                    else {
                        $returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'PROFILE_PHOTO_SIZE_LARGE':'PROFILE_PHOTO_SIZE_LARGE_CHINESE');
                        $returnData = UtilityController::Generateresponse(0, $returnMessage, Response::HTTP_REQUEST_ENTITY_TOO_LARGE, '');
                        $returnData['message'] = $input['image']->getClientOriginalName().$returnData['message'];
                        return response($returnData, $returnData['code']);
                    }
                }
                $input['display_name'] = $input['first_name'] .' '. $input['last_name'];
                $result = UtilityController::Makemodelobject($input, 'User', '', $userID); 
                $loginData = User::where('id', $userID)->first()->toArray();
                $loginData['profile_image'] = $loginData['image'];
                if(isset($loginData['image'])) {
                    // $loginData['image'] = url_path()."/uploads/user_profile/".$loginData['image'];
                    $loginData['image'] = "/public/uploads/user_profile/".$loginData['image'];
                }
                $returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'PROFILE_UPDATED_SUCCESSFULLY':'PROFILE_UPDATED_SUCCESSFULLY_CHINESE');
                $returnData = UtilityController::Generateresponse(true, $returnMessage, Response::HTTP_OK, $loginData);
                \DB::commit();
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData        = UtilityController::Generateresponse(false, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }
        return response($returnData, $returnData['code']);
    }

    //###############################################################
    //Function Name : patchRefresh
    //Author : Shootsta
    //Purpose : To refresh the token
    //Return : Refresh token
    //###############################################################
    public function patchRefresh() {
        $token         = JWTAuth::parseToken();
        $newToken      = $token->refresh();
        $returnMessage = 'token_refreshed';
        $returnData    = UtilityController::Generateresponse(1, $returnMessage, Response::HTTP_OK, $newToken);
        return response($returnData);
    }


    //###############################################################
    //Function Name : sendPushNotification
    //Author : Nivedita <nivedita@creolestudios.com>
    //Purpose : To send push notification to mobile devices
    //Return : 
    //###############################################################
    public function sendPushNotification($data, $to, $options) {
        // Insert your Secret API Key here
        $globalVariable = self::Globalvariables();
        $apiKey = $globalVariable['CURRENT_DOMAIN']=='net'?'ec10e3c65047325d0f2d226973df9c97060713954e072a80e91d239b0238dc6c':'69d4492b5cce7230531a725dcf611e484c3f39066a3eae19a0b0dce45d341145';

        // Default post data to provided options or empty array
        $post = $options ?: array();

        // Set notification payload and recipients
        $post['to'] = $to;
        $post['data'] = $data;

        // Set Content-Type header since we're sending JSON
        $headers = array(
            'Content-Type: application/json'
        );

        // Initialize curl handle
        $ch = curl_init();

        // Set URL to Pushy endpoint
        curl_setopt($ch, CURLOPT_URL, 'https://api.pushy.me/push?api_key=' . $apiKey);

        // Set request method to POST
        curl_setopt($ch, CURLOPT_POST, true);

        // Set our custom headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // Get the response back as string instead of printing it
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Set post data as JSON
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post, JSON_UNESCAPED_UNICODE));

        // Actually send the push
        $result = curl_exec($ch);
        // Display errors
        if (curl_errno($ch)) {
            echo curl_error($ch);
        }

        // Close curl handle
        curl_close($ch);

        // Debug API response
        //echo $result;
    }

    //###############################################################
    //Function Name: forgotPassword
    //Author:        Senil Shah <senil@creolestudios.com>
    //Purpose:       Send reset password link to subscriber
    //In Params:     Subscriber email address
    //Return:        json
    //Date:          17th Oct, 2018
    //###############################################################
    public function forgotPassword(Request $request){     
        try {
            $Input = Input::all();
            // print_r($Input); die;
            $Input['email_address'] = trim($Input['email_address']);
            if(array_key_exists('email_address', $Input) && $Input['email_address']!='' && isset($Input['email_address'])) {
                \DB::beginTransaction();
                $userEmail = $Input['email_address'];
                $forgetToken = str_random(25);
                $language = 1;
                if(User::where('email_address', $userEmail)->exists()){
                    $userObject = User::where('email_address', $userEmail)->first();
                    if($userObject['status']==3){
                        $returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'ACCOUNT_SUSPENDED_BY_ADMIN':'ACCOUNT_SUSPENDED_BY_ADMIN_CHINESE');
                        $returnData = UtilityController::Generateresponse(false, $returnMessage, Response::HTTP_FORBIDDEN, '');
                    } else {
                        $resetURL = substr(URL::to('/'), 0, -3).'Resetpassword?reset_token='.$forgetToken.'&email='.base64_encode($userEmail).'&language='.$language;
                        $userObject->reset_token = $forgetToken;
                        if ($userObject->save()) {
                            $data['username'] = $userObject->first_name . ' ' . $userObject->last_name;
                            $data['link'] = $resetURL;
                            $data['email'] = $userEmail;
                            if($userObject->current_language!='')
                                $data['current_language']=$userObject->current_language;
                            else
                                $data['current_language']= 1;
                            $data['subject']=$data['current_language']==1?'Your request to reset your password':'您重置密码的请求';

                            $mailResult = Mail::send('emails.forgotpassword', $data, function($message) use ($data) {
                                        $message->from('noreply@sixclouds.cn', 'SixClouds');
                                        $message->to($data['email'])->subject($data['subject']);
                                    });
                            $returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'FORGOT_PASSWORD_SUCCESS':($request->server('HTTP_ACCEPT_LANGUAGE')=='chi'?'FORGOT_PASSWORD_SUCCESS_CHINESE':'FORGOT_PASSWORD_SUCCESS_RU'));
                            //$returnMessage=$request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'FORGOT_PASSWORD_SUCCESS':'FORGOT_PASSWORD_SUCCESS_CHINESE';
                            $returnData = UtilityController::Generateresponse(true, $returnMessage, Response::HTTP_OK, '');
                            \DB::commit();
                        } else {
                            $returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'GENERAL_ERROR':'GENERAL_ERROR_CHINESE');
                            $returnData = UtilityController::Generateresponse(false, $returnMessage, Response::HTTP_CONFLICT, '');
                        }
                    }
                } else {
                    //$returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'EMAIL_NOT_EXISTS':'EMAIL_NOT_EXISTS_CHINESE');
                    $returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'EMAIL_NOT_EXISTS':($request->server('HTTP_ACCEPT_LANGUAGE')=='chi'?'EMAIL_NOT_EXISTS_CHINESE':'EMAIL_NOT_EXISTS_RU'));
                    $returnData = UtilityController::Generateresponse(false, $returnMessage, Response::HTTP_UNPROCESSABLE_ENTITY, '');
                }
            } else {
                $returnMessage = ($request->server('HTTP_ACCEPT_LANGUAGE')=='en'?'EMAIL_ADDRESS_REQUIRED':'EMAIL_ADDRESS_REQUIRED_CHINESE');
                $returnData = UtilityController::Generateresponse(false, $returnMessage, Response::HTTP_BAD_REQUEST, '');
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $sendExceptionMail = UtilityController::Sendexceptionmail($e);
            $returnData = UtilityController::Generateresponse(false, $e->getMessage(), Response::HTTP_BAD_REQUEST, '');
        }            
        return response($returnData,$returnData['code']);
    }
}
<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\UtilityController;

class CORSMiddleware {

	/**
	 * Handle an incoming request.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Closure                 $next
	 *
	 * @return mixed
	 */
	//###############################################################
	//Function Name : Makeinputcaseinsensitive
	//Author : Shootsta
	//Purpose : Generic function to convert all input parameter keys into lowercase.
	//Return : small case input parameter keys
	//###############################################################
	public static function Makeinputcaseinsensitive($request) {
		if (!empty($request->input())) {
			foreach ($request->input() as $key => $val) {
				$request[strtolower($key)] = $request[$key];
			}
		}
	}

	public function handle($request, Closure $next) {
		// Get all request parameters in lowercase to make all requests case insensitive.
		$this->Makeinputcaseinsensitive($request);
		//check if user is not locked or not
		$this->InterceptLockedUser($request);
		// TODO: Should check whether route has been registered
		if ($this->isPreflightRequest($request)) {
			$response = $this->createEmptyResponse();
		} else {
			$response = $next($request);
		}

		return $this->addCorsHeaders($request, $response);
	}

	/**
	 * Determine if request is a preflight request.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return bool
	 */
	protected function isPreflightRequest($request) {
		return $request->isMethod('OPTIONS');
	}

	/**
	 * Create empty response for preflight request.
	 *
	 * @return \Illuminate\Http\Response
	 */
	protected function createEmptyResponse() {
		return new Response(null, 204);
	}

	/**
	 * Add CORS headers.
	 *
	 * @param \Illuminate\Http\Request  $request
	 * @param \Illuminate\Http\Response $response
	 */
	protected function addCorsHeaders($request, $response) {
		foreach ([
		'Access-Control-Allow-Origin' => '*',
		'Access-Control-Max-Age' => (60 * 60 * 24),
		'Access-Control-Allow-Headers' => $request->header('Access-Control-Request-Headers'),
		'Access-Control-Allow-Methods' => $request->header('Access-Control-Request-Methods')
		? : 'GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS',
		'Access-Control-Allow-Credentials' => 'true',
		] as $header => $value) {
			$response->header($header, $value);
		}
		return $response;
	}

	private function InterceptLockedUser($request) {
		//parse jwt token to get user is locked or not
		$currentToken = JWTAuth::getToken();		
		if (isset($currentToken) && !empty($currentToken)) {
			$parsedToken = JWTAuth::parseToken();			 			 
			$lockedStatus = $parsedToken->toUser()->status;			
			//if locked status is 1 then invalidate the token and restrict user from accesing any secure API
			if ($lockedStatus != 1) {
				$parsedToken->invalidate();
			}
		}
	}

}

<?php

//###############################################################
//File Name : WorksheetDownloads.php
//Author : Nivedita Mitra <nivedita@creolestudios.com>
//Purpose : related to downloads of worksheets
//Date : 21th Sept 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorksheetDownloads extends Model
{
    protected $table = 'worksheet_downloads';
}

<?php

//###############################################################
//File Name : QuizAttempts.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : related to attempting questions of quiz
//Date : 6th Sept 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\VideoCategory;

class QuizAttempts extends Model
{
    protected $table = 'quiz_attempts';
    public $rules    = array(
        'user_id'               => 'required',
        'quiz_id'               => 'required',
        'question_id'           => 'required',
        'attempted_answer'      => 'required',
        'platform'              => 'required',
        'quiz_completed_status' => 'required',
    );
    public function quiz_details()
    {
        return $this->belongsTo('App\Quizzes', 'quiz_id','id');
    }
    public function users()
    {
       return $this->hasMany('App\User', 'user_id');
    }
}

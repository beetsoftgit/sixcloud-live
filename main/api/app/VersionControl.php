<?php

//###############################################################
//File Name : VersionControl.php
//Author : senil Shah <senil@creolestudios.com>
//Purpose : related to version update for mobile devices
//Date : 31st Oct, 2018
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

class VersionControl extends Model {

    protected $table = 'version_control';
}

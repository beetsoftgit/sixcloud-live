<?php
/*
##
## @File Name   Bank.php
## @category    Model File for database table named "amenities"
## @Author      Hardika satasiya<hardika@creolestudios.com>
## @since       File available since Release 1.0.0
## @copyright   2017 Homex
##
 */
namespace App;

use Illuminate\Database\Eloquent\Model;

class DeviceTokens extends Model
{
    //Define the table name
    protected $table = 'devicetokens';
    //Define Relationship here
    protected $fillable = [
        'user_id',
        'device_id',
        'token',
        'os_type',
    ];
    public $rules = [
        'user_id'   => 'required | numeric',
        'device_id' => 'required',
        'token'     => 'required',
        'os_type'   => 'required',
    ];
    public function users()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}

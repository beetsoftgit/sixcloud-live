<?php

//###############################################################
//File Name : Countries.php
//Author : Komal Kapadi <komal@creolestudios.com>
//Purpose : to get list of countries
//Date : 5th Dec 2017
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\VideoCategory;

class Quizzes extends Model
{
    protected $table = 'quizzes';
    public function category()
    {
        return $this->belongsTo('App\VideoCategory', 'category_id');
    }

    public function categoryWithOnlyName()
    {
        return $this->belongsTo('App\VideoCategory', 'category_id')->select('id','category_name');
    }
    public function subcategoryWithOnlyName() {
        return $this->belongsTo('App\VideoCategory', 'subcategory_id')->select('id','subcategory_name');
    }

    public function quizquestions() {
        return $this->hasMany('App\QuizQuestions', 'quiz_id')->where('question_status',1);
    }

    public function total_questions() {
        return $this->hasMany('App\QuizAttempts', 'quiz_id');
    }

    public function correct_answers() {
        return $this->hasMany('App\QuizAttempts', 'quiz_id')->where('is_correct',1);
    }

    public function quiz_attempt() {
        return $this->hasOne('App\QuizAttempts', 'quiz_id');
    }
    public function quiz_attempt_highest() {
        return $this->hasMany('App\QuizAttempts', 'quiz_id');
    }
}

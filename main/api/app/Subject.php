<?php

//###############################################################
//File Name : Subject.php
//Author : Senil Shah <senil@creolestudios.com>
//Purpose : Subjects model
//Date : 30th Jan, 2019
//###############################################################

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Http\Controllers\UtilityController;

use DB;
use URL;

class Subject extends Model
{
    protected $table  = 'subjects';
    //Added By ketan Solanki for coiuntry list with States and Cities
    public function categories() {
        return $this->hasMany('App\VideoCategory', 'subject_id');
    }

    public function categories_only() {
        return $this->hasMany('App\VideoCategory', 'subject_id')->select('id','subject_id','category_name','description','description_chi','category_price','category_price_sgd', DB::raw("'$' as currency_usd"), DB::raw("'S$' as currency_sgd"),'status')->where('status',1)->whereNull('parent_id');
    }

    public function getSubjectImageAttribute($subject_image)
    {
    	$url = preg_replace("/\/[a][p][i]$/", "", URL::to('/'));
    	if($subject_image != null) {
    		return ($url.UtilityController::Getpath('SUBJECT_IMAGE_URL').$subject_image);
    	} else {
    		return ($url.UtilityController::Getpath('SUBJECT_IMAGE_URL').'maze.png');
    	}
    }

}

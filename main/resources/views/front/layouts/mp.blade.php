<!DOCTYPE html>
<html ng-app="sixcloudApp" class="[[eng?'chi-font':'']]">
<head>
<meta charset="utf-8">
<meta name="304Fix" content="safari-fix">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
@if(isset($title)&&isset($meta))
  <!--for .cn-->
  <meta ng-if="current_domain=='cn'" name="google-site-verification" content="OcrZncefagNshLgnp-9lZX38U00cfjh0-xs1MukpY7o"/>
  <!--end-->
  <title>{{$title}}</title>
  <meta name="description" content="{{$meta}}">
  <meta name="robots" content="index,follow" />
@else
  <title>SixClouds Web</title>
  <meta name="description" content="">
@endif
@if(isset($title)&&isset($meta))
<link rel="alternate" href="{{$alternate}}" hreflang="{{$alternatelang}}" />
<link rel="canonical" href="{{$canonical}}" />
@else
@endif
<meta name="google-site-verification" content="F0XMjfssK8hcmTCfqJJcQFq80vAe8lHHegJBFA84XAQ" />
<meta name="msvalidate.01" content="5789F571110EF5249F1B42276844821B" />
<meta name="p:domain_verify" content="8475f8bbc093c663df8ead38f223dfac"/>
<meta name="csrf-token" content="{{ csrf_token() }}">


<!-- Google Analytics -->
  <script>
  window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
  ga('create', 'UA-149665763-1');
  ga('send', 'pageview');
  </script>
  <script async src='https://www.google-analytics.com/analytics.js'></script>
  <!-- End Google Analytics -->


<!-- Global site tag (gtag.js) - Google Analytics -->

<!--this is for .net-->
<script ng-if="current_domain!='cn'" async src="https://www.googletagmanager.com/gtag/js?id=UA-127371237-1"></script>
<script ng-if="current_domain!='cn'">
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-127371237-1');
</script>
<!--this is for .net-->

<!--this is for .cn-->
<script ng-if="current_domain=='cn'" async src="https://www.googletagmanager.com/gtag/js?id=UA-127371237-2"></script>
<script ng-if="current_domain=='cn'">
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-127371237-2');
</script>
<!--this is for .cn-->
@if(config('constants.messages.CURRENT_DOMAIN') == 'cn')
  {{ HTML::style('resources/assets/css/front/mp/animations.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/swiper.min.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/custom-font.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/font-awesome.min.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/jquery.FlowupLabels.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/bootstrap-datetimepicker.min.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/asRange.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/common/sweet-alert.min.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/common/star-rating.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/common/uploadfile.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/common/jasny-bootstrap.min.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/bootstrap.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/owl.carousel.css',array('rel'=>'stylesheet')) }}
  <link rel="stylesheet" media="all" type="text/css" href="{{ url('/')}}/resources/assets/css/front/mp/style.css" ng-if="module =='mp'">
  <link rel="stylesheet" media="all" type="text/css" href="{{ url('/')}}/resources/assets/css/front/mp/responsive.css" ng-if="module =='mp'">
  <link rel="stylesheet" media="all" type="text/css" href="{{ url('/')}}/resources/assets/css/front/ignite/audioplayer.css" ng-if="module =='ignite'">
  <link rel="stylesheet" media="all" type="text/css" href="{{ url('/')}}/resources/assets/css/front/ignite/dob-ui.css" ng-if="module =='ignite'">
  <link rel="stylesheet" media="all" type="text/css" href="{{ url('/')}}/resources/assets/css/front/ignite/responsive.css" ng-if="module =='ignite'">
  <link rel="stylesheet" media="all" type="text/css" href="{{ url('/')}}/resources/assets/css/front/ignite/style.css" ng-if="module =='ignite'">
  <link rel="stylesheet" media="all" type="text/css" href="{{ url('/')}}/resources/assets/css/front/pr/responsive.css" ng-if="module =='pr'">
  <link rel="stylesheet" media="all" type="text/css" href="{{ url('/')}}/resources/assets/css/front/pr/style.css" ng-if="module =='pr'">
  <link rel="stylesheet" media="all" type="text/css" href="{{ url('/')}}/resources/assets/css/front/pr/dob-ui.css" ng-if="module =='pr'">
  <link rel="stylesheet" media="all" type="text/css" href="{{ url('/')}}/resources/assets/css/front/pr/icomoon.css" ng-if="module =='pr'">

  <link rel="stylesheet" media="all" type="text/css" href="{{ url('/')}}/resources/assets/css/front/sphere/responsive.css" ng-if="module =='sphere'">
  <link rel="stylesheet" media="all" type="text/css" href="{{ url('/')}}/resources/assets/css/front/sphere/style.css" ng-if="module =='sphere'">

  {{ HTML::style('resources/assets/css/common/ng-img-crop.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/six-clouds.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/common/angular-page-loader.css',array('rel'=>'stylesheet')) }}
@else
  {{ HTML::style('resources/assets/css/front/mp/animations.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/swiper.min.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/custom-font.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/font-awesome.min.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/jquery.FlowupLabels.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/bootstrap-datetimepicker.min.css',array('rel'=>'stylesheet')) }}
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
  {{ HTML::style('resources/assets/css/front/mp/asRange.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/common/sweet-alert.min.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/common/star-rating.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/common/uploadfile.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/common/jasny-bootstrap.min.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/bootstrap.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/owl.carousel.css',array('rel'=>'stylesheet')) }}
  <link rel="stylesheet" media="all" type="text/css" href="{{ url('/')}}/resources/assets/css/front/mp/style.css" ng-if="module =='mp'">
  <link rel="stylesheet" media="all" type="text/css" href="{{ url('/')}}/resources/assets/css/front/mp/responsive.css" ng-if="module =='mp'">
  <link rel="stylesheet" media="all" type="text/css" href="{{ url('/')}}/resources/assets/css/front/ignite/audioplayer.css" ng-if="module =='ignite'">
  <link rel="stylesheet" media="all" type="text/css" href="{{ url('/')}}/resources/assets/css/front/ignite/dob-ui.css" ng-if="module =='ignite'">
  <link rel="stylesheet" media="all" type="text/css" href="{{ url('/')}}/resources/assets/css/front/ignite/responsive.css" ng-if="module =='ignite'">
  <link rel="stylesheet" media="all" type="text/css" href="{{ url('/')}}/resources/assets/css/front/ignite/style.css" ng-if="module =='ignite'">
  <link rel="stylesheet" media="all" type="text/css" href="{{ url('/')}}/resources/assets/css/front/pr/responsive.css" ng-if="module =='pr'">
  <link rel="stylesheet" media="all" type="text/css" href="{{ url('/')}}/resources/assets/css/front/pr/style.css" ng-if="module =='pr'">
  <link rel="stylesheet" media="all" type="text/css" href="{{ url('/')}}/resources/assets/css/front/pr/dob-ui.css" ng-if="module =='pr'">
  <link rel="stylesheet" media="all" type="text/css" href="{{ url('/')}}/resources/assets/css/front/pr/icomoon.css" ng-if="module =='pr'">

  <link rel="stylesheet" media="all" type="text/css" href="{{ url('/')}}/resources/assets/css/front/sphere/responsive.css" ng-if="module =='sphere'">
  <link rel="stylesheet" media="all" type="text/css" href="{{ url('/')}}/resources/assets/css/front/sphere/style.css" ng-if="module =='sphere'">

  <!-- <link rel="stylesheet" media="all" type="text/css" href="https://cdn.sixclouds.net/assets/css/front/sphere/responsive.css" ng-if="module =='sphere'">
  <link rel="stylesheet" media="all" type="text/css" href="https://cdn.sixclouds.net/assets/css/front/sphere/style.css" ng-if="module =='sphere'"> -->

  {{ HTML::style('resources/assets/css/common/ng-img-crop.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/six-clouds.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/common/angular-page-loader.css',array('rel'=>'stylesheet')) }}
@endif

<link rel="stylesheet" media="all" type="text/css" href="{{ url('/')}}/resources/assets/css/front/mp/mp_custom.css" ng-if="module =='mp'">
<link rel="stylesheet" media="all" type="text/css" href="{{ url('/')}}/resources/assets/css/front/ignite/ignite_custom.css" ng-if="module =='ignite'">
<link rel="stylesheet" media="all" type="text/css" href="{{ url('/')}}/resources/assets/css/front/pr/pr_custom.css" ng-if="module =='pr'">
<link rel="stylesheet" media="all" type="text/css" href="{{ url('/')}}/resources/assets/css/front/sphere/sphere_custom.css" ng-if="module =='sphere'">
<!-- <link rel="stylesheet" media="all" type="text/css" href="https://g.alicdn.com/de/prismplayer/2.7.2/skins/default/aliplayer-min.css" ng-if="module =='ignite'"> -->

{{ HTML::script('resources/assets/js/common/jquery.min.js') }}
{{ HTML::script('resources/assets/js/common/angular.min.js') }}
{{ HTML::script('resources/assets/js/common/angular-route.js') }}
{{ HTML::script('resources/assets/js/common/angular-sanitize.min.js') }}
{{ HTML::script('resources/assets/js/common/angular-page-loader.min.js') }}
<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="https://rawgit.com/desandro/imagesloaded/master/imagesloaded.pkgd.min.js"></script>
<!-- <script src="assets/js/angular-masonry-directive.js"></script> -->
<link rel="stylesheet" href="https://cdn.rawgit.com/codekraft-studio/angular-page-loader/master/dist/angular-page-loader.css">
<link rel="shortcut icon" type="image/x-icon" href="resources/assets/images/sc108x108.png">
        <base href="<?=url('/') . '/'?>">
        <style>
            [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
                display: none !important;
            }
        </style>
</head>
<body ng-controller="MainController" ng-cloak ng-class="isLoading?'custom-loader':'class2'">
      <!-- <page-loader bg-color="#000" flag="isLoading">
        <img class="custom-img-angular-loader" width="150px" src="resources/assets/images/SixClouds2.gif"/>
      </page-loader> -->
      <page-loader></page-loader>
<!-- <form id='twinkleRedirectForm' class="hide" enctype='application/json' method='POST' action ='https://sixclouds.smartjen.com/services/access' target="iFrame">
    <input type='text' name='token' ng-model=twinkleRedirectToken >
</form> -->
<div class="body-div">
    <!--Header section Ignite-->
    <header class="header ignite-header" ng-if="module == 'ignite'">
      <div class="container">
        <div class="row">
          <div class="col-sm-4 pull-right pull-none767" ng-if="loginDistributorData == '' || loginDistributorData == undefined">
            <div class="top-right-language language-ignite" ng-controller="language as ctrl"> 
              <!-- <a href="#" class="btn btn-medium-pink">中文</a> 
              <a href="#" class="btn btn-medium-pink">English</a> -->
              <!-- <a href="javascript:;" id="eng" ng-if="eng == true" ng-click="changeLang('en')" class="btn btn-medium-pink login-btn lang_button">[['lbl_lang_chng'|translate]]</a>
              <a href="javascript:;" id="cn" ng-if="!eng" ng-click="changeLang('chi')" class="btn btn-medium-pink login-btn lang_button">[['lbl_lang_chng'|translate]]</a> -->
              <select ng-model="changeLangs" ng-change="changeLang(changeLangs)" class="form-control">
                <option value="chi" ng-selected="changeLangs=='chi'">中文</option>
                <option value="en" ng-selected="changeLangs=='en'">English</option>
                <option value="ru" ng-selected="changeLangs=='ru'">Русский</option>
              </select>
              <a href="ignite-login" class="btn btn-medium-pink btn-light-blue uppercase login-btn">[['lbl_login'|translate]]</a>
            </div>
          </div>
          <div class="col-xs-4 pull-right">
            <div class="dropdown head-right-dropdown ignite-dropdown" ng-controller="language as ctrl" ng-if="loginDistributorData != ''">
              <!-- <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> -->
                <!-- <div class="uploaded-img upload-profile-img distributor_profile_image img_top_custom" id="previewImage" style="background-image:url(public/uploads/user_profile/[[loginDistributorData.image]]); width: 50px; height: 50px"></div> -->
              <table cellpadding="0" cellspacing="0" width="100%" data-toggle="dropdown" class="custom_cursor_pointer">
                <tbody>
                  <tr>
                    <td width="50">
                      <span class="">
                      <!-- <img class="img_top_custom" ng-if="loginDistributorData.userType!='ignite_user'" alt="" ng-src="public/uploads/distributor_profile/[[loginDistributorData.image]]" width="53" height="53"> -->
                      <div ng-if="loginDistributorData.userType!='ignite_user'" class="custom_profile_header uploaded-img upload-profile-img distributor_profile_image" style="background-image:url(public/uploads/distributor_profile/[[loginDistributorData.image]]);"></div>
                      <div ng-if="loginDistributorData.userType=='ignite_user'" class="custom_profile_header uploaded-img upload-profile-img distributor_profile_image" style="background-image:url(public/uploads/user_profile/[[loginDistributorData.image]]);"></div>
                      <!-- <img class="img_top_custom" ng-if="loginDistributorData.userType=='ignite_user'" alt="" ng-src="public/uploads/user_profile/[[loginDistributorData.image]]" width="53" height="53"> -->
                      </span>
                    </td>
                    <td class="custom_padding_10 ignite-user" ng-if="loginDistributorData.userType!='distributor'">[[loginDistributorData.first_name]] [[loginDistributorData.last_name]]<span class="caret"></span></td>
                    <td class="custom_padding_10 ignite-user" ng-if="loginDistributorData.userType=='distributor'">[[loginDistributorData.company_name]]<span class="caret"></span></td>
                  </tr>
                </tbody>
              </table>
              <!-- </button> -->

              <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dLabel">

                  <!-- For Home Option in Dropdown -->

                  <li ng-if="loginDistributorData.userType=='ignite_user' && loginDistributorData.video_preference!=null && loginDistributorData.video_preference!='' ">
                    <a tabindex="-1" href="ignite-categories">[['lbl_home'|translate]]</a>
                  </li>

                  <li class="dropdown-submenu dropdown-menu-right" ng-if="loginDistributorData.userType=='ignite_user' && loginDistributorData.video_preference!=null && loginDistributorData.video_preference!='' ">
                    <a tabindex="-1" href="#">[['lbl_display_language_dropdown'|translate]]</a>
                    <ul class="dropdown-menu dropdown-menu-outer language-selection" style="margin-top: 38px;">
                      <li><a href="#" ng-class="{'selected':transLang=='en'}" ng-click="changeLanguage('en')">English</a></li>
                      <li><a href="#" ng-class="{'selected':transLang=='chi'}" ng-click="changeLanguage('chi')">中文</a></li>
                      <li><a href="#" ng-class="{'selected':transLang=='ru'}" ng-click="changeLanguage('ru')">Русский</a></li>
                    </ul>
                  </li>
                  <li ng-if="loginDistributorData.userType=='ignite_user' && loginDistributorData.video_preference==null">
                    <a ng-class="{disabled: loginDistributorData.country_id==0||loginDistributorData.city_id==0||loginDistributorData.provience_id==0||loginDistributorData.dob==null}" href="select-categories">[['lbl_change_subject'|translate]]</a>
                  </li>

                  <li class="dropdown-submenu dropdown-menu-right" ng-if="(INTL && CIS) && loginDistributorData.userType=='ignite_user' && loginDistributorData.video_preference!=null && loginDistributorData.video_preference!='' ">
                    <a tabindex="-1" href="#">[['lbl_change_subtitle_language'|translate]]</a>
                    <ul class="dropdown-menu dropdown-menu-outer" style="margin-top: 76px;">
                      <li class="dropdown-submenu dropdown-submenu-inner" ng-repeat="category in categoryLanguages">
                        <a href="#">[[ category.subject_display_name ]]</a>
                        <ul class="dropdown-menu" style="margin-top: [[$index*35]]px;">
                            <li ng-repeat="language in category.categories">
                              <a href="#" ng-click="Seevideos(language.language_name, language.subject_id)">
                                [[ language.language_display_name | translateSubject ]]
                              </a>
                            </li>
                        </ul>
                      </li>
                    </ul>
                  </li>

                  <li ng-if="loginDistributorData.userType=='distributor'">
                    <a href="subscribers">[['lbl_home'|translate]]</a>
                  </li>
                  <li ng-if="loginDistributorData.userType=='distributor_team_member'">
                    <a ng-class="{disabled: loginDistributorData.status==4||loginDistributorData.status==5}" href="my-referral">[['lbl_home'|translate]]</a>
                  </li>
                  <li  ng-if="loginDistributorData.userType=='ignite_user'"><a ng-class="{disabled: loginDistributorData.country_id==0||loginDistributorData.city_id==0||loginDistributorData.provience_id==0||loginDistributorData.dob==null}" href="my-performance">[['lbl_my_performance'|translate]]</a></li>
                  
                  <li ng-if="userZone.is_subscription_allowed && loginDistributorData.userType=='ignite_user' && loginDistributorData.is_subscription_allowed==1"><a ng-class="{disabled: loginDistributorData.country_id==0||loginDistributorData.city_id==0||loginDistributorData.provience_id==0||loginDistributorData.dob==null}" href="my-subscription">[['lbl_my_subscription'|translate]]</a></li>

                  <li ng-if="loginDistributorData.userType=='distributor'"><a href="distributor-my-account">[['lbl_my_account'|translate]]</a></li>
                  <li ng-if="loginDistributorData.userType=='distributor_team_member'"><a  ng-class="{disabled: loginDistributorData.status==4}" href="team-my-account">[['lbl_my_account'|translate]]</a></li>
                  <li ng-if="loginDistributorData.userType=='ignite_user'"><a href="my-account">[['lbl_my_account'|translate]]</a></li>
                  <!-- <li><a href="#/my-performance">Notifications <span class="alert-span">5</span></a> </li>
                  <li><a href="#/">Sixteen</a></li>
                  <li><a href="#/">Proof Reading</a></li>
                  <li><a href="#/">Digital Library</a></li> -->
                  <li><a ng-click="Logoutdistributor()" href="javascript:;">[['lbl_log_out'|translate]]</a></li>
              </ul>
            </div>
          </div>
          <div class="col-xs-12 col-sm-8 tab-ignite">
            <div ng-if="transLang == 'en'" class="logo"> <a href="https://www.sixclouds.net/"><img src="resources/assets/images/Corporate_Alternate_Color-1.png"></a> </div>
            <div ng-if="transLang == 'chi'" class="logo"> <a href="https://www.sixclouds.net/zh/%e9%a6%96%e9%a1%b5/"><img src="resources/assets/images/Corporate_Alternate_Color-1.png"></a> </div>
            <div ng-if="transLang == 'ru'" class="logo"> <a href="https://www.sixclouds.net/ru/%d0%b4%d0%be%d0%bc%d0%be%d0%b9/"><img src="resources/assets/images/Corporate_Alternate_Color-1.png"></a> </div>
            <div class="tab-section" ng-if="loginDistributorData.userType=='ignite_user'">
              
                <a href="https://www.sixclouds.net/" ng-if="transLang == 'en'" id="home" ng-class="{active: ignitePage=='Home'}" ng-click="switchTab('Home')" class="btn uppercase">[['lbl_home'|translate]]</a>
                <a href="https://www.sixclouds.net/zh/%e9%a6%96%e9%a1%b5/" ng-if="transLang == 'chi'" id="home" ng-class="{active: ignitePage=='Home'}" ng-click="switchTab('Home')" class="btn uppercase">[['lbl_home'|translate]]</a>
                <a href="https://www.sixclouds.net/ru/%d0%b4%d0%be%d0%bc%d0%be%d0%b9/" ng-if="transLang == 'ru'" id="home" ng-class="{active: ignitePage=='Home'}" ng-click="switchTab('Home')" class="btn uppercase">[['lbl_home'|translate]]</a>

                <a href="#" ng-if="Eng == 'English'" id="english" ng-class="{active: igniteId=='English'}" ng-click="switchCategory(loginDistributorData, subengId )" class="btn uppercase">[['lbl_lang_eng'|translate]]</a>
              
                <a href="#" ng-if="Math == 'Mathematics'" id="mathematics" ng-class="{active: igniteId=='Mathematics'}" ng-click="switchCategory(loginDistributorData, submathId )" class="btn uppercase">[['lbl_mathematics'|translate]]</a>

                <!-- un-comment the below line to view twinkle related changes -->
                <!-- <a href="#" ng-click="twinkleRedirect()" id="Twinkle" class="btn uppercase">[['lbl_twinkle'|translate]]</a> -->

                <!-- <a href="#" ng-if="isBUZZ||isBUZZRU" ng-class="{active: ignitePage=='English'}" ng-click="switchCategory(loginDistributorData,'BUZZ')" class="btn uppercase">[['lbl_lang_eng'|translate]]</a>
              
                <a href="#" ng-class="{active: ignitePage=='Mathematics' || Mathematics=='Mathematics'}" ng-click="switchCategory(loginDistributorData,'MAZE')" class="btn uppercase">[['lbl_mathematics'|translate]]</a> -->

            </div>
            <div class="tab-section" ng-if="loginDistributorData == '' || loginDistributorData == undefined">

              <a href="https://www.sixclouds.net/" ng-if="transLang == 'en'" ng-class="{active: ignitePage=='Home'}" ng-click="switchTab('Home')" class="btn uppercase">[['lbl_home'|translate]]</a>
              <a href="https://www.sixclouds.net/zh/%e9%a6%96%e9%a1%b5/" ng-if="transLang == 'chi'" ng-class="{active: ignitePage=='Home'}" ng-click="switchTab('Home')" class="btn uppercase">[['lbl_home'|translate]]</a>
              <a href="https://www.sixclouds.net/ru/%d0%b4%d0%be%d0%bc%d0%be%d0%b9/" ng-if="transLang == 'ru'" ng-class="{active: ignitePage=='Home'}" ng-click="switchTab('Home')" class="btn uppercase">[['lbl_home'|translate]]</a>

              <a href="https://www.sixclouds.net/english/" ng-if="transLang == 'en'" ng-class="{active: ignitePage=='English'}" ng-click="switchTab('English')" class="btn uppercase">[['lbl_lang_eng'|translate]]</a>
              <a href="https://www.sixclouds.net/zh/%e8%8b%b1%e8%af%ad/" ng-if="transLang == 'chi'" ng-class="{active: ignitePage=='English'}" ng-click="switchTab('English')" class="btn uppercase">[['lbl_lang_eng'|translate]]</a>
              <a href="https://www.sixclouds.net/ru/%d0%b0%d0%bd%d0%b3%d0%bb%d0%b8%d0%b9%d1%81%d0%ba%d0%b8%d0%b9/" ng-if="transLang == 'ru'" ng-class="{active: ignitePage=='English'}" ng-click="switchTab('English')" class="btn uppercase">[['lbl_lang_eng'|translate]]</a>

              <a href="https://www.sixclouds.net/maths/" ng-if="transLang == 'en'" ng-class="{active: ignitePage=='Mathematics' || Mathematics=='Mathematics'}" ng-click="switchTab('Mathematics')" class="btn uppercase">[['lbl_mathematics'|translate]]</a>
              <a href="https://www.sixclouds.net/zh/%e6%95%b0%e5%ad%a6/" ng-if="transLang == 'chi'" ng-class="{active: ignitePage=='Mathematics' || Mathematics=='Mathematics'}" ng-click="switchTab('Mathematics')" class="btn uppercase">[['lbl_mathematics'|translate]]</a>
              <a href="https://www.sixclouds.net/ru/%d0%bc%d0%b0%d1%82%d0%b5%d0%bc%d0%b0%d1%82%d0%b8%d0%ba%d0%b0/" ng-if="transLang == 'ru'" ng-class="{active: ignitePage=='Mathematics' || Mathematics=='Mathematics'}" ng-click="switchTab('Mathematics')" class="btn uppercase">[['lbl_mathematics'|translate]]</a>

            </div>
          </div>
          
        </div>
      </div>
    </header>
    <div ng-if="loginDistributorData!='' && loginDistributorData.userType=='ignite_user' && subheader == 'subheader'" class="ignite-subheader ignite-header ignite-subheader-white && (categoryLanguages.static!='' || categoryLanguages.staticget!='')">
        <div class="container">
          <div class="tab-section" ng-if="igniteId=='Mathematics'">
              <a href="#" ng-class="{active: ignitePageId == subject.id}" data-ng-repeat="subject in categoryLanguages.static"  ng-click="switchCategory(loginDistributorData,subject.id)" class="btn uppercase">[[subject.subject_name]]</a>
              <!-- <a ng-if="isSMILE" href="#" ng-click="switchCategory(loginDistributorData,'SMILE (INTERNATIONAL)')" ng-class="{active: ignitePage=='Smile'}" class="btn uppercase">[['lbl_ignite_smile'|translate]]</a>
              <a ng-if="isSMILESG" href="#" ng-click="switchCategory(loginDistributorData,'SMILE (SG)')" ng-class="{active: ignitePage=='SmileSG'}" class="btn uppercase">[['lbl_ignite_smile_sg'|translate]]</a> -->
            </div>
            <div class="tab-section" ng-if="igniteId=='English'">
              <a href="#" ng-class="{active: ignitePageId == subject.id}" data-ng-repeat="subject in categoryLanguages.staticget"  ng-click="switchCategory(loginDistributorData,subject.id)" class="btn uppercase">[[subject.subject_name]]</a>
              <!-- <a ng-if="isSMILE" href="#" ng-click="switchCategory(loginDistributorData,'SMILE (INTERNATIONAL)')" ng-class="{active: ignitePage=='Smile'}" class="btn uppercase">[['lbl_ignite_smile'|translate]]</a>
              <a ng-if="isSMILESG" href="#" ng-click="switchCategory(loginDistributorData,'SMILE (SG)')" ng-class="{active: ignitePage=='SmileSG'}" class="btn uppercase">[['lbl_ignite_smile_sg'|translate]]</a> -->
            </div>
        </div>
    </div>
    <!--Header section Ignite end-->

    <!-- Header section Sphere : start -->
    <header class="header" ng-if="module == 'sphere'">
      <div class="container">
        <div class="row">
          <div class="col-sm-4 col-xs-5">
            <div class="logo"> <a href="sphere"><img src="resources/assets/images/sphere/logo.png"></a> </div>
          </div>
      <div class="col-sm-4">
        <h2 class="header-mid-text hidden-xs">Sphere</h2>
      </div>
          <div class="col-sm-4 col-xs-7">
          
            <div class="dropdown head-right-dropdown">
              <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <table cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                  <tr>
                    <td width="50">
                      <!-- <img alt="" src="resources/assets/images/sphere/sm-photo2.png" width="53" height="53"> -->
                      <div class="custom_profile_header uploaded-img upload-profile-img profile_image" style="background-image:url(resources/assets/images/sphere/sm-photo2.png);"> 
                      </div>
                    </td>
                    <td>John Doe <span class="caret"></span></td>
                  </tr>
                </tbody>
              </table>
              </button>
              <ul class="dropdown-menu dropdown-menu-form" aria-labelledby="dLabel">
                <!-- <li class="language"> <span class="china">中文</span> <span class="switch-btn">
                  <label class="switch">
                    <input type="checkbox" checked>
                    <span class="slider round"></span> </label>
                  </span> <span class="en active">English</span> </li> -->
                <li class="language"> 
                  <span class="china [[whatLanguageSelect==chi?'active':'']]">中文 [[whatLanguageSelect]]</span> 
                  <span class="switch-btn">
                    <label class="switch">
                    <!-- <input type="checkbox" checked id="chk_lang" ng-model=selectedLang> -->
                    <input type="checkbox" ng-checked="selectedLang" id="chk_lang" ng-model=selectedLang>
                    <span class="slider round"></span> </label>
                  </span> 
                  <span class="en [[whatLanguageSelect==en?'active':'']]">English</span> 
                </li>
                 
                <li><a href="#/my-performance">Notifications <span class="alert-span">5</span></a> </li>
                <li><a href="#/">Sixteen</a></li>
                <li><a href="#/">Proof Reading</a></li>
                <li><a href="#/">Digital Library</a></li>
                <li><a href="login.html">Log Out</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </header>
    <!-- Header section Sphere : end -->
   
    <!--Header section Started mp-->
    <header ng-show="module=='mp'" ng-class="!loginUserData?'animatedParent animateOnce Bmargin sixteen-header':'animatedParent animateOnce sixteen-header'" data-sequence='500'>
      <div class="container">
        <div class="row animated fadeIn" data-id='2'>
          <div ng-if="loginUserData == '' && module == 'mp'" class="mp_without_login col-sm-4 col-md-3 col-lg-2">
            <div class="logo"> <a href="sixteen"><img src="resources/assets/images/logo.png" width="173" style="height: auto;"></a></div>
          </div>
          <div ng-if="loginUserData == '' && module == 'mp'" class="col-sm-8 col-md-9 col-lg-10 mp_without_login" ng-controller="language as ctrl">
           <div class="header-mid-btn">
              <!-- <a href="javascript:;" id="eng" ng-if="eng == true" ng-click="changeLang('en')" class="btn btn-light-purple login-btn lang_button">[['lbl_lang_chng'|translate]]</a>
              <a href="javascript:;" id="cn" ng-if="!eng" ng-click="changeLang('chi')" class="btn btn-light-purple login-btn lang_button">[['lbl_lang_chng'|translate]]</a> -->
              <select ng-model="changeLangs" ng-change="changeLang(changeLangs)" class="btn btn-light-purple login-btn lang_button">
                <option value="chi">中文</option>
                <option value="en">English</option>
                <option value="ru">Русский</option>
              </select>
              <a href="sixteen-login" class="btn btn-purple uppercase login-btn">[['lbl_login'|translate]]</a>
            </div>
          </div>
          <!--  After Login Start-->
          <div ng-if="loginUserData != '' && module != 'ignite'" class="mp_with_login col-sm-4 col-md-3 col-lg-2">
            <div class="logo"> <a style="height: auto;" ng-href="[[loginUserData.is_buyer==1?'buyer-dashboard':'seller-dashboard']]" ><img src="resources/assets/images/logo.png" width="173"></a></div>
          </div>
          <div ng-if="loginUserData != '' && module != 'ignite'" class="mp_with_login col-sm-4 col-md-5 col-lg-7 mob-hide">
            <div class="header-mid-text">
              [['lbl_marketplace'|translate]]
            </div>
          </div>
          <div ng-if="loginUserData != '' && module != 'ignite'" class="mp_with_login col-sm-4 col-md-4 col-lg-3">
            <div class="header-profile-dropdown">
              <div class="dropdown show">
                <a class="dropdown-toggle" href="javascript:;" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                   <table cellpadding="0" cellspacing="0">
                     <tr>
                       <td class="left-img header-profile-image">
                         <!-- <span>5</span> -->
                         <p class="profile-img">
                         <img  ng-src="[[loginUserData.image]]" width="44" height="44"></p>
                       </td>
                       <td>
                         <strong class="welcome_text">John Doe </strong>
                          
                       </td>
                     </tr>
                   </table>
                </a>
                <div class="dropdown-menu dropdown-menu-form" role="menu" ng-controller="language as ctrl" >
                 <ul>
                   <li class="switch-li">
                     <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td  width="35%" class="[[whatLanguageSelect==chi?'active':'']]">中文</td>
                              <td  width="30%" class="text-center">
                                <label class="switch">
                                  <input type="checkbox" ng-checked="selectedLang" id="chk_lang" ng-model=selectedLang>
                                  <span class="slider round"></span>
                                </label>
                              </td>
                              <td  width="35%" align="right" class="[[whatLanguageSelect==en?'active':'']]">English</td>
                          </tr>
                      </table>
                   </li>
                   <li class="switch-li" ng-if="loginUserData.is_buyer==1&&loginUserData.is_designer==1">
                     <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td width="35%" class="[[!selectedRole?'active':'']]">Buyer</td>
                              <td  width="30%" class="text-center">
                              <label class="switch">
                                <input type="checkbox" id="role_switch" ng-model="selectedRole">
                                <span class="slider round"></span>
                              </label>
                              </td>
                              <td  width="35%" class="[[selectedRole?'active':'']]" align="right" >Seller</td>
                          </tr>
                      </table>
                   </li>
                   <!-- <li><a href="javascript:;">[['lbl_video_learning'|translate]]</a></li>
                   <li><a href="javascript:;">[['lbl_pr'|translate]]</a></li>
                   <li><a href="javascript:;">[['lbl_marketplace'|translate]]</a></li>
                   <li><a href="javascript:;">[['lbl_dl'|translate]]</a></li> -->
                   <li ng-if="loginUserData.marketplace_current_role == 1"><a href="seller-my-account">[['lbl_my_account'|translate]]</a></li>
                   <li ng-if="loginUserData.marketplace_current_role == 2"><a href="buyer-my-account">[['lbl_my_account'|translate]]</a></li>
                   <li><a href="notifications">[['lbl_notifications'|translate]] <span id="notification_number" class="alert-span">5</span></a></li>
                   <li><a class="logout_link" href="javascript:;">[['lbl_log_out'|translate]]</a></li>
                 </ul>
                </div>
              </div>
            </div>
          </div>
          <!--  After Login End-->
        </div>
      </div>
    </header>
    <!--Header section End MP-->
    <!--Header section Started PR-->
    <header class="header" ng-show="module=='pr'">
      <div class="container">
        <div class="row">
          <div class="col-sm-4 col-xs-5">
            <div class="logo"> <a href="#"><img src="resources/assets/images/logo_pr.svg"></a> </div>
          </div>
          <div class="col-sm-4 hidden-xs">
            <h2 class="h-mid">PROOF READING </h2>
          </div>
          <div class="col-sm-4 col-xs-7">
            <div class="dropdown head-right-dropdown">
              <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <table cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                  <tr>
                    <td width="50"><img alt="" src="resources/assets/images/sm-photo2.png" width="53" height="53"></td>
                    <td>Shi Sung <span class="caret"></span></td>
                  </tr>
                </tbody>
              </table>
              </button>
              <ul class="dropdown-menu dropdown-menu-form" aria-labelledby="dLabel">
                <li class="language"> <span class="china [[whatLanguageSelect==chi?'active':'']]">中文</span> <span class="switch-btn">
                  <label class="switch">
                    <input type="checkbox" ng-checked="selectedLang" id="chk_lang" ng-model=selectedLang>
                    <span class="slider round"></span> </label>
                  </span> <span class="en [[whatLanguageSelect==en?'active':'']]">English</span> </li>
                 
                <li><a href="#/my-performance">Notifications <span class="alert-span">5</span></a> </li>
                <li><a href="#/">Sixteen</a></li>
                <li><a href="#/">Proof Reading</a></li>
                <li><a href="#/">Digital Library</a></li>
                <li><a href="login.html">Log Out</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </header>
    <!--Header section End PR-->
    <section class="second-header-menu" ng-if="module == 'ignite' && loginDistributorData != '' && loginDistributorData.userType!='ignite_user'">
      <div class="container" ng-if="loginDistributorData.userType=='distributor'&&loginDistributorData.status=='1'">
        <div class="row">
          <div class="col-sm-12">
            <ul class="menu-sm ignite-menu" id="sub-menu-item">
              <li id="all-referral">
                  <a href="subscribers">[['lbl_subscribers'|translate]]</a>
              </li>
              <li id="my-team">
                  <a href="my-team">[['lbl_sales_team'|translate]]</a>
              </li>
              <li id="commissions">
                  <a href="distributors-commissions">[['lbl_commissions'|translate]]</a>
              </li>
              <li id="my-codes">
                  <a href="my-codes">[['lbl_my_codes'|translate]]</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="container" ng-if="loginDistributorData.userType=='distributor_team_member'">
        <div class="row">
          <div class="col-sm-12">
            <ul class="menu-sm ignite-menu" id="sub-menu-item">
              <li id="my-referral">
                  <a ng-if="loginDistributorData.status==1" href="subscribers">[['lbl_subscribers'|translate]]</a>
                  <a ng-if="loginDistributorData.status==4||loginDistributorData.status==5" >[['lbl_subscribers'|translate]]</a>
              </li>
              <li id="commissions">
                  <a ng-if="loginDistributorData.status==1" href="team-member-commissions">[['lbl_commissions'|translate]]</a>
                  <a ng-if="loginDistributorData.status==4||loginDistributorData.status==5">[['lbl_commissions'|translate]]</a>
              </li>
              <li id="my-codes">
                  <a href="my-codes">[['lbl_my_codes'|translate]]</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>
    <section class="after-login-new-head" ng-if="loginUserData.marketplace_current_role==2 && module == 'mp'">
      <div class="container">
        <div class="row">
          <div class="col-sm-9 col-lg-8">
            <ul class="menu-sm buyer-dashboard-menu" id="buyer-dashboard-menu">
              <li id="dashboard">
                  <a href="buyer-dashboard">[['lbl_dashboard'|translate]]</a>
              </li>
              <li id="browse_designers">
                  <a href="browse-sellers">[['lbl_browse_designers'|translate]]</a>
              </li>
              <!-- <li id="gallery">
                  <a href="gallery">[['lbl_gallery'|translate]]</a>
              </li> -->
              <li id="past_projects">
                  <a href="buyer-past-projects">[['lbl_past_projects'|translate]]</a>
              </li>
            </ul>
          </div>
          <div  class="col-sm-3 col-lg-2">
             <div class="outstanding-balance" >[['lbl_credit'|translate]]: <strong > <span ng-if="current_domain=='cn'" class="left-yen">¥</span>
              <span ng-if="current_domain=='net'" class="left-yen">
                  S$
              </span><span id="credit"> [[credit]]</span></strong></div>
          </div>

          <div  class="col-sm-offset-9 col-sm-3 col-lg-offset-0  col-lg-2">
            <a href="create-new-project" class="btn btn-purple new-project-btn">[['lbl_new_project'|translate]]</a>
          </div>
        </div>
      </div>
    </section>
    <section class="after-login-new-head" ng-if="loginUserData.marketplace_current_role==1 && module == 'mp'">
      <div class="container">
        <div class="row">
          <div class="col-sm-9 col-lg-8">
            <ul class="menu-sm designer-dashboard-menu" id="sub-menu-item">
              <li id="designer_dashboard">
                  <a href="seller-dashboard">[['lbl_dashboard'|translate]]</a>
              </li>
              <li id="designer_portfolio">
                  <a href="seller-portfolio">[['lbl_my_portfolio'|translate]]</a>
              </li>

              <!-- <li id="designer_myuploads">
                  <a href="seller-my-uploads">[['lbl_my_uploads'|translate]]</a>
              </li> -->
              <li id="designer_myuploads">
                  <a href="seller-my-uploads">[['lbl_inspiration_bank'|translate]]</a>
              </li>
              <!-- <li>
                  <a href="gallery">[['lbl_profile'|translate]]</a>
              </li> -->
              <li id="designer_past_project">
                  <a href="seller-past-projects">[['lbl_past_projects'|translate]]</a>
              </li>
               <li id="my_services">
                  <a href="my-services">[['lbl_my_services'|translate]]</a>
              </li>
            </ul>
          </div>
          <div  class="col-sm-3 col-lg-2">
             <div class="outstanding-balance">[['lbl_outstanding_balance'|translate]]: <strong><span class="yen" ng-if="current_domain=='cn'">&yen;</span><span class="yen" ng-if="current_domain=='net'">S$ </span> [[outstandingbalance]]</strong></div>
          </div>
          <div  class="col-sm-offset-9 col-sm-3 col-lg-offset-0  col-lg-2"  ng-if="[[outstandingbalance]] != 0">
            <a href="#/new-project" class="btn btn-purple new-project-btn">[['lbl_withdraw_header'|translate]]</a>
          </div>
        </div>
      </div>
    </section>
  <div id="main">
    <!-- <div>
      <form ng-if="module == 'ignite'">
        <button type="button" class="btn tablinks btn-medium-pink" ng-click="goBack()" style="position: relative; margin-left: 1%; margin-bottom: 1%;"><i class="fa fa-arrow-left"></i></button>
      </form>
    </div> -->
    <div id="content">
      <div ng-view></div>
    </div>
    <div ng-if="showiframe">
      <!-- <iframe id="iFrame" name="iFrame" src='https://sixclouds.smartjen.com/services/access' style="border:1px solid #9a9a9a;" width='100%' height="800px" allowfullscreen>
      </iframe> -->
      <iframe name='proprofs' id='proprofs' width='100%' height='800px' frameborder=0 marginwidth=0 marginheight=0  style="border:1px solid #9a9a9a;" ></iframe>
    </div>
  </div>

  <!--Footer Start mp-->
  <footer ng-if="module =='mp'" class="animatedParent animateOnce">
    <div class="footer-bg-repeat animated fadeIn slowest"></div>
    <div class="main-footer animated fadeIn slowest">
      <div class="container">
        <div class="row">
          <div class="col-sm-4">
            <div class="h-div bs-copy-right">
              <div class="footer-logo"> <a href="sixteen"><img src="resources/assets/images/logo.png"></a> </div>
              <p><a target="_blank" href="<?=url('/')?>">www.sixclouds.[[ @if(strpos(url('/'), 'sixclouds.cn') !== false ||  strpos(url('/'), 'localhost') !== false) ]]cn[[ @else ]]net[[ @endif ]]</a>
              <div class="bottom-div"> <span class="copy">&copy;</span>[['lbl_allrights'|translate]]</div>
              <p>[[ @if(strpos(url('/'), 'sixclouds.cn') !== false ||  strpos(url('/'), 'localhost') !== false) ]] ICP号为：渝ICP备18001226号 [[  @endif ]]</p>
            </div>
          </div>
          <div class="col-sm-7">
            <table cellpadding="0" cellspacing="0" width="100%">
              <tr>
                <td width="50%" valign="top"><div class="h-div">
                    <h3>[['lbl_about_us'|translate]]</h3>
                    <ul ng-class="{ru_text: transLang == 'ru'}">
                      <li><a target="_blank" href="<?=url('/') . '/about-us/the-company'?>">[['lbl_the_company'|translate]]</a></li>
                      <li><a target="_blank" href="<?=url('/') . '/about-us/core-values'?>">[['lbl_core_values'|translate]]</a></li>
                      <li><a target="_blank" href="<?=url('/') . '/about-us/our-team'?>">[['lbl_our_team'|translate]]</a></li>
                      <!-- <li><a href="javascript:;">Ignite </a></li>
                      <li><a href="javascript:;">Sixteen </a></li>
                      <li><a href="javascript:;">Discover </a></li>
                      <li><a href="javascript:;">Proof Reading</a></li> -->
                    </ul>
                  </div>
                </td>
                <td valign="top"><div class="h-div">
                    <h3>[['lbl_quick_links'|translate]]</h3>
                    <ul ng-class="{ru_text: transLang == 'ru'}">
                      <li><a target="_blank" href="<?=url('/') . '/become-seller'?>">[["lbl_become_seller" | translate]]</a></li>
                      <li><a target="_blank" href="<?=url('/') . '/become-buyer'?>">[["lbl_become_buyer" | translate]]</a></li>
                      <li><a target="_blank" href="<?=url('/') . '/customer-support'?>">[["lbl_customer_support" | translate]] </a></li>
                      <li><a target="_blank" href="<?=url('/') . '/sixteen-terms-of-service'?>">[["lbl_terms_service" | translate]]  </a></li>
                      <li><a target="_blank" href="<?=url('/') . '/privacy-policy'?>">[["lbl_privacy_policy" | translate]]</a></li>
                    </ul>
                  </div>
                   </td>
              </tr>
            </table>
          </div>

        </div>
      </div>
    </div>
  </footer>
  <!--Footer Start ingite-->
  <!-- <footer ng-if="module =='ignite'"> -->
    <!-- <div class="footer-bg-repeat "></div>
    <div class="main-footer">
      <div class="container">
        <div class="row"> -->
          <!-- <div class="col-sm-5">
            <div class="h-div">
              <div class="footer-logo"> <a href="ignite"><img src="resources/assets/images/logo.svg" width="173" height="70"></a> </div>
              <p class="l-link"><a href="#">sixclouds.com</a></p> 
              <div class="bottom-div"> <span class="copy">&copy;</span>All rights reserved </div>
            </div>
            
          </div> -->
          <!-- <div class="col-sm-5">
            <div class="h-div bs-copy-right">
              <div class="footer-logo"> <a href="ignite"><img src="resources/assets/images/logo.svg"></a> </div>
              <p class="l-link"><a target="_blank" href="<?=url('/')?>">www.sixclouds.[[ @if(strpos(url('/'), 'sixclouds.cn') !== false ||  strpos(url('/'), 'localhost') !== false) ]]cn[[ @else ]]net[[ @endif ]]</a>
              <div class="bottom-div"> <span class="copy">&copy;</span>[['lbl_allrights'|translate]]</div>
              <p>[[ @if(strpos(url('/'), 'sixclouds.cn') !== false ||  strpos(url('/'), 'localhost') !== false) ]] ICP号为：渝ICP备18001226号 [[  @endif ]]</p>
            </div>
          </div>
          <div class="col-sm-7">
            <table cellpadding="0" cellspacing="0" width="100%">
              <tr>
                <td width="50%" valign="top"><div class="h-div">
                    <h3>[['lbl_about_us'|translate]]</h3>
                    <ul ng-class="{ru_text: transLang == 'ru'}">
                      <li><a target="_blank" href="<?=url('/') . '/about-us/the-company'?>">[['lbl_the_company'|translate]]</a></li>
                      <li><a target="_blank" href="<?=url('/') . '/about-us/core-values'?>">[['lbl_core_values'|translate]]</a></li>
                      <li><a target="_blank" href="<?=url('/') . '/about-us/our-team'?>">[['lbl_our_team'|translate]]</a></li> 
                    </ul>
                  </div>
                  </td>
                <td valign="top"><div class="h-div">
                    <h3>[['lbl_quick_links'|translate]]</h3>
                    <ul ng-class="{ru_text: transLang == 'ru'}">
                      <li><a target="_blank" href="<?=url('/') . '/customer-support'?>">[["lbl_customer_support" | translate]] </a></li>
                      <li><a target="_blank" href="<?=url('/') . '/ignite-terms-of-service'?>">[["lbl_terms_service" | translate]]  </a></li>
                      <li><a target="_blank" href="<?=url('/') . '/faq'?>">[["lbl_faq" | translate]]</a></li>
                      <li><a target="_blank" href="<?=url('/') . '/privacy-policy'?>">[["lbl_privacy_policy" | translate]]</a></li>
                      <li ng-if="loginDistributorData.userType!='ignite_user'"><a href="<?=url('/') . '/distributor'?>">[["lbl_ignite_distributor_access" | translate]]</a></li>
                    </ul>
                  </div>
                  </td>
              </tr>
            </table>
          </div>
          
        </div>
      </div>
    </div> -->
  <!-- </footer> -->
  <!-- footer custom ignite start-->
  <footer class="ignite-footer" ng-if="module =='ignite'">
    <div class="container">
      <div class="row">
        <div class="col-md-6 ignite-footer-logo">
          <img src="resources/assets/images/Corporate_Color.png" alt="">
        </div>
        <div class="col-md-6 ignite-footer-right text-right">
          <p>[["lbl_follow_us" | translate]]</p>
          <div class="social-icon-wrapper">
            <a href="https://www.facebook.com/SixCloudsSG/"><i class="fa fa-facebook"></i></a>
          </div>
          <p>[["lbl_available_at" | translate]]</p>
          <div class="apk-images">
            <a ng-if="transLang == 'en'" href="https://apps.apple.com/sg/app/sixclouds/id1451386328"><img src="resources/assets/images/app-store.png" alt="app-store"></a>
            <a ng-if="transLang == 'chi'" href="https://apps.apple.com/sg/app/sixclouds/id1451386328"><img src="resources/assets/images/Chi_App-Store.png" alt="app-store"></a>
            <a ng-if="transLang == 'ru'" href="https://apps.apple.com/sg/app/sixclouds/id1451386328"><img src="resources/assets/images/icon_App-Store-ln-ru.png" alt="app-store"></a>
            <a href="https://sixclouds.net/main/apk/SixClouds_v1.22.apk"><img src="resources/assets/images/android.png" alt="play-store"></a>
            <a ng-if="transLang == 'en'" href="https://play.google.com/store/apps/details?id=com.sixcloudsall"><img src="resources/assets/images/play-store-btn.png" alt="play-store"></a>
            <a ng-if="transLang == 'chi'" href="https://play.google.com/store/apps/details?id=com.sixcloudsall"><img src="resources/assets/images/Chi_Google-Play.png" alt="play-store"></a>
            <a ng-if="transLang == 'ru'" href="https://play.google.com/store/apps/details?id=com.sixcloudsall"><img src="resources/assets/images/icon_Google-Play-ln-ru.png" alt="play-store"></a>
          </div>
        </div>
      </div>
      <div class="row policy"> 
        <div class="col-md-6 terms">
          <a ng-if="transLang == 'en'" href="https://www.sixclouds.net/terms-of-service/">[["lbl_terms_service" | translate]]</a><a ng-if="transLang == 'chi'" href="https://www.sixclouds.net/zh/%e6%9c%8d%e5%8a%a1%e6%9d%a1%e6%ac%be/">[["lbl_terms_service" | translate]]</a><a ng-if="transLang == 'ru'" href="https://www.sixclouds.net/ru/%d1%83%d1%81%d0%bb%d0%be%d0%b2%d0%b8%d1%8f-%d1%83%d1%81%d0%bb%d1%83%d0%b3%d0%b8/">[["lbl_terms_service" | translate]]</a> |
          <a ng-if="transLang == 'en'" href="https://www.sixclouds.net/privacy-policy/">[["lbl_privacy_policy" | translate]]</a><a ng-if="transLang == 'chi'" href="https://www.sixclouds.net/zh/%e9%9a%90%e7%a7%81%e6%94%bf%e7%ad%96/">[["lbl_privacy_policy" | translate]]</a><a ng-if="transLang == 'ru'" href="https://www.sixclouds.net/ru/%d0%ba%d0%be%d0%bd%d1%84%d0%b8%d0%b4%d0%b5%d0%bd%d1%86%d0%b8%d0%b0%d0%bb%d1%8c%d0%bd%d0%be%d1%81%d1%82%d1%8c-%d0%b8-%d0%b1%d0%b5%d0%b7%d0%be%d0%bf%d0%b0%d1%81%d0%bd%d0%be%d1%81%d1%82%d1%8c/">[["lbl_privacy_policy" | translate]]</a>
        </div>
        <div class="col-md-6 copyright text-right">
        <p>© 2021 SixClouds. [["lbl_all_rights_reserved" | translate]]</p>
        </div>

      </div>
    </div>
  </footer>
  <!-- footer custom ignite end-->
  <!--Footer Start sphere-->
  <footer ng-if="module =='sphere'">
    <div class="footer-bg-repeat "></div>
    <div class="main-footer">
      <div class="container">
        <div class="row">
          <!-- <div class="col-sm-5">
            <div class="h-div">
              <div class="footer-logo"> <a href="ignite"><img src="resources/assets/images/logo.svg" width="173" height="70"></a> </div>
              <p class="l-link"><a href="#">sixclouds.com</a></p> 
              <div class="bottom-div"> <span class="copy">&copy;</span>All rights reserved </div>
            </div>
            
          </div> -->
          <div class="col-sm-5">
            <div class="h-div bs-copy-right">
              <div class="footer-logo"> <a href="sphere"><img src="resources/assets/images/sphere/logo.png"></a> </div>
              <p class="l-link"><a target="_blank" href="<?=url('/')?>">www.sixclouds.[[ @if(strpos(url('/'), 'sixclouds.cn') !== false ||  strpos(url('/'), 'localhost') !== false) ]]cn[[ @else ]]net[[ @endif ]]</a>
              <div class="bottom-div"> <span class="copy">&copy;</span>[['lbl_allrights'|translate]]</div>
              <p>[[ @if(strpos(url('/'), 'sixclouds.cn') !== false ||  strpos(url('/'), 'localhost') !== false) ]] ICP号为：渝ICP备18001226号 [[  @endif ]]</p>
            </div>
          </div>
          <div class="col-sm-7">
            <table cellpadding="0" cellspacing="0" width="100%">
              <tr>
                <td width="50%" valign="top"><div class="h-div">
                    <h3>[['lbl_about_us'|translate]]</h3>
                    <ul ng-class="{ru_text: transLang == 'ru'}">
                      <li><a target="_blank" href="<?=url('/') . '/about-us/the-company'?>">[['lbl_the_company'|translate]]</a></li>
                      <li><a target="_blank" href="<?=url('/') . '/about-us/core-values'?>">[['lbl_core_values'|translate]]</a></li>
                      <li><a target="_blank" href="<?=url('/') . '/about-us/our-team'?>">[['lbl_our_team'|translate]]</a></li> 
                    </ul>
                  </div>
                  </td>
                <td valign="top"><div class="h-div">
                    <h3>[['lbl_quick_links'|translate]]</h3>
                    <ul ng-class="{ru_text: transLang == 'ru'}">
                      <li><a target="_blank" href="<?=url('/') . '/customer-support'?>">[["lbl_customer_support" | translate]] </a></li>
                      <li><a target="_blank" href="<?=url('/') . '/terms-of-service'?>">[["lbl_terms_service" | translate]]  </a></li>
                      <li><a target="_blank" href="<?=url('/') . '/faq'?>">[["lbl_faq" | translate]]</a></li>
                      <li><a target="_blank" href="<?=url('/') . '/privacy-policy'?>">[["lbl_privacy_policy" | translate]]</a></li>
                    </ul>
                  </div>
                  </td>
              </tr>
            </table>
          </div>
          
        </div>
      </div>
    </div>
  </footer>
  <!--Footer Start pr-->
  <footer ng-if="module =='pr'">
    <div class="footer-bg-repeat "></div>
    <div class="main-footer">
      <div class="container">
        <div class="row">
          <!-- <div class="col-sm-5">
            <div class="h-div">
              <div class="footer-logo"> <a href="ignite"><img src="resources/assets/images/logo.svg" width="173" height="70"></a> </div>
              <p class="l-link"><a href="#">sixclouds.com</a></p> 
              <div class="bottom-div"> <span class="copy">&copy;</span>All rights reserved </div>
            </div>
            
          </div> -->
          <div class="col-sm-5">
            <div class="h-div bs-copy-right">
              <div class="footer-logo"> <a href="ignite"><img src="resources/assets/images/logo_pr.svg"></a> </div>
              <p class="l-link"><a href="<?=url('/')?>">www.sixclouds.[[ @if(strpos(url('/'), 'sixclouds.cn') !== false ||  strpos(url('/'), 'localhost') !== false) ]]cn[[ @else ]]net[[ @endif ]]</a>
              <div class="bottom-div"> <span class="copy">&copy;</span>[['lbl_allrights'|translate]]</div>
              <p>[[ @if(strpos(url('/'), 'sixclouds.cn') !== false ||  strpos(url('/'), 'localhost') !== false) ]] ICP号为：渝ICP备18001226号 [[  @endif ]]</p>
            </div>
          </div>
          <div class="col-sm-7">
            <table cellpadding="0" cellspacing="0" width="100%">
              <tr>
                <td width="50%" valign="top"><div class="h-div">
                    <h3>[['lbl_about_us'|translate]]</h3>
                    <ul ng-class="{ru_text: transLang == 'ru'}">
                      <li><a target="_blank" href="<?=url('/') . '/about-us'?>">[['lbl_about_us'|translate]]</a></li> 
                    </ul>
                  </div>
                  </td>
                <td valign="top"><div class="h-div">
                    <h3>[['lbl_quick_links'|translate]]</h3>
                    <ul ng-class="{ru_text: transLang == 'ru'}">
                      <li><a target="_blank" href="<?=url('/') . '/customer-support'?>">[["lbl_customer_support" | translate]] </a></li>
                      <li><a target="_blank" href="<?=url('/') . '/ignite-terms-of-service'?>">[["lbl_terms_service" | translate]]  </a></li>
                      <li><a target="_blank" href="<?=url('/') . '/privacy-policy'?>">[["lbl_privacy_policy" | translate]]</a></li>
                    </ul>
                  </div>
                  </td>
              </tr>
            </table>
          </div>
          
        </div>
      </div>
    </div>
  </footer>
</div>
    <script>
        // set MODE for enable and disable console.log
        var project_mode = 'development'; //'production';
        var BASEURL = "<?=url('/') . '/'?>";
        var MAX_SIZE = "<?=config('constants.messages.MAX_UPLOAD_FILE_SIZE')?>";
        var protocol = "<?= empty($_SERVER['HTTPS']) ? 'http' : 'https'?>";
        var domain = "<?=$_SERVER['SERVER_NAME']?>";
        var completeUrl = protocol+'://'+domain+'/';
        var VIDEO_PLACEHOLDER = "<?=url('/') . config('constants.path.ELT_VIDOE_PLACEHOLDER')?>";
        var current_domain = "<?=config('constants.messages.CURRENT_DOMAIN')?>";
    </script>
    <!-- Core files -->
    {{ HTML::script('resources/assets/js/front/mp/popper.min.js') }}
    {{ HTML::script('resources/assets/js/front/mp/bootstrap.js') }}
    {{ HTML::script('resources/assets/js/front/mp/swiper.min.js') }}
    {{ HTML::script('resources/assets/js/front/mp/dob-ui.js') }}
    {{ HTML::script('resources/assets/js/front/mp/css3-animate-it.js') }}
    {{ HTML::script('resources/assets/js/front/mp/jquery.FlowupLabels.js') }}
    {{ HTML::script('resources/assets/js/common/init.js') }}
    {{ HTML::script('resources/assets/js/common/ng-img-crop.js') }}
    {{ HTML::script('resources/assets/js/common/classes/crop-area-circle.js') }}
    {{ HTML::script('resources/assets/js/common/classes/crop-area-square.js') }}
    {{ HTML::script('resources/assets/js/common/classes/crop-area.js') }}
    {{ HTML::script('resources/assets/js/common/classes/crop-canvas.js') }}
    {{ HTML::script('resources/assets/js/common/classes/crop-exif.js') }}
    {{ HTML::script('resources/assets/js/common/classes/crop-host.js') }}
    {{ HTML::script('resources/assets/js/common/classes/crop-pubsub.js') }}

    {{ HTML::script('resources/assets/js/front/mp/moment.js') }}
    {{ HTML::script('resources/assets/js/front/mp/moment-cn.js') }}
   
    {{ HTML::script('resources/assets/js/front/mp/bootstrap-datetimepicker.js') }}
    {{ HTML::script('resources/assets/js/front/mp/bootstrap-datepicker.js') }}
    {{ HTML::script('resources/assets/js/front/mp/jquery-asRange.js') }}

    {{ HTML::script('resources/assets/js/common/file-progress/vendors/jquery.ui.widget.js') }}
    {{ HTML::script('resources/assets/js/common/file-progress/load-image.all.min.js') }}
    {{ HTML::script('resources/assets/js/common/file-progress/canvas-to-blob.min.js') }}
    {{ HTML::script('resources/assets/js/common/file-progress/jquery.iframe-transport.js') }}
    {{ HTML::script('resources/assets/js/common/file-progress/jquery.fileupload.js') }}
    {{ HTML::script('resources/assets/js/common/file-progress/jquery.fileupload-process.js') }}
    {{ HTML::script('resources/assets/js/common/file-progress/jquery.fileupload-image.js') }}
    {{ HTML::script('resources/assets/js/common/file-progress/jquery.fileupload-video.js') }}
    {{ HTML::script('resources/assets/js/common/file-progress/jquery.fileupload-audio.js') }}
    {{ HTML::script('resources/assets/js/common/file-progress/jquery.fileupload-validate.js') }}
    {{ HTML::script('resources/assets/js/common/highcharts.js') }}
    {{ HTML::script('resources/assets/js/common/highcharts-more.js') }}
    {{ HTML::script('resources/assets/js/common/exporting.js') }}
    {{ HTML::script('resources/assets/js/common/export-data.js') }}
    <!-- {{ HTML::script('resources/assets/js/common/aliplayer-min.js') }} -->


    {{ HTML::script('resources/assets/js/front/customangular.js') }}
    {{ HTML::script('resources/assets/js/common/custom_alert.js') }}
    {{ HTML::script('resources/assets/js/common/jquery.validate.min.js') }}
    {{ HTML::script('resources/assets/js/common/sweetalert.min.js') }}
    {{ HTML::script('resources/assets/js/common/underscore.js') }}
    {{ HTML::script('resources/assets/js/front/basic.js') }}
    {{ HTML::script('resources/assets/js/common/js.cookie.min.js') }}
    {{ HTML::script('resources/assets/js/common/lang-label.js') }}
    {{ HTML::script('resources/assets/js/common/angular-translate.min.js') }}
    {{ HTML::script('resources/assets/js/common/star-rating.js') }}
    {{ HTML::script('resources/assets/js/common/uploadfile.min.js') }}
    {{ HTML::script('resources/assets/js/common/moment-timezone.js') }}
    {{ HTML::script('resources/assets/js/common/moment-timezone-with-data.js') }}
    {{ HTML::script('resources/assets/js/common/jasny-bootstrap.min.js') }}
    {{ HTML::script('resources/assets/js/front/core/wow.min.js') }}
    
    {{ HTML::script('resources/assets/js/front/mp/ckeditor.js') }}
    {{ HTML::script('resources/assets/js/front/mp/ckeditorconfig.js') }}

    {{ HTML::script('resources/assets/js/front/controllers/elt/ELTvideouploadController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/MainController.js') }}
    <!-- Controller files of Sixteen -->
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPHomeController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/ignite/IgniteHomeController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPDesignerSignupController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPBuyerSignUpController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPCreateNewProjectController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPSuggestedDesignerController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPLoginController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPBuyerDashboardController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPDesignerDashboardController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPBrowseDesignersController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPBuyerPastProjectsController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPBuyerGalleryController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPBuyerProjectInfoActiveController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPDesignerMyUploadsController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPDesignerProjectInfoController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPBuyerProjectInfoPastController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPDesignerPortfolioController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPDesignerPastProjectsController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPDesignerPastProjectInfoController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPDesignerInspirationBankController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPDesignerPortfolioItemDetailsController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPDesignerInspirationBankItemDetailsController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPDesignerPortfolioItemEditController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPDesignerInspirationBankEditController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPDesignerProjectInfoInvitationController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPDesignerInspirationBankController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPBuyerGalleryProductInfoController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPNotificationsController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/designerProfileController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPDesignerMyAccountController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPSellerMyServicesController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MakePaymentController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/mp/MPExpressInterestController.js') }}
    <!--controller files of ignite-->
    {{ HTML::script('resources/assets/js/front/controllers/ignite/IgniteLoginController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/ignite/IgniteAllReferralController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/ignite/IgniteMyTeamController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/ignite/IgniteMyAccountController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/ignite/IgniteSignUpController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/ignite/IgniteSelectCategoriesController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/ignite/IgniteCategoriesController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/ignite/IgniteVerifyDistributorController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/ignite/IgniteMySubscriptionController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/ignite/IgniteCommissionsController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/ignite/IgniteHomeController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/ignite/IgniteContentDetailController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/ignite/IgniteQuizController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/ignite/IgniteQuizResultController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/ignite/IgnitePerformancesController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/ignite/MakePaymentIgniteController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/ignite/IgniteSubscriberDetailsController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/ignite/IgniteMyCodesController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/ignite/IgniteTeamDetailController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/ignite/IgniteVerifyPhoneNumberController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/ignite/IgniteUserVerifyOtpController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/ignite/IgniteStripePaymentController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/ignite/IgniteSwitchPlanController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/ignite/IgniteSubscriptionPlanController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/ignite/IgniteSubscriptionPaymentController.js') }}
    <!--controller files of ignite-->
    <!-- controller files of pr -->
    {{ HTML::script('resources/assets/js/front/controllers/pr/PRHomeController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/pr/PRMyCasesController.js') }}
    <!-- controller files of pr -->

    <!-- controller files of sphere : start -->
    {{ HTML::script('resources/assets/js/front/controllers/sphere/SphereLandingPageController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/sphere/SphereSignupController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/sphere/SphereMessageController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/sphere/SphereTeacherController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/sphere/SphereTeacherProfileController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/sphere/SphereTransactionsController.js') }}
    {{ HTML::script('resources/assets/js/front/controllers/sphere/SpherehistoryController.js') }}
    <!-- controller files of sphere : end -->

    <!-- factory -->
    {{ HTML::script('resources/assets/js/front/factory/userfactory.js') }}
    {{ HTML::script('resources/assets/js/front/factory/prfactory.js') }}
    {{ HTML::script('resources/assets/js/front/factory/mpfactory.js') }}
    {{ HTML::script('resources/assets/js/front/factory/ignitefactory.js') }}
    {{ HTML::script('resources/assets/js/front/factory/spherefactory.js') }}
    {{ HTML::script('resources/assets/js/admin/hls.js') }}
    {{ HTML::script('resources/assets/js/front/core/owl.carousel.js') }}
    {{ HTML::script('resources/assets/js/front/custom.js') }}
    <!-- <script type="text/javascript">
      $(window).on('load', function(){
             $(".loader").fadeOut('slow');
        })
    </script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-sanitize/1.6.9/angular-sanitize.min.js">
    </script>
    <script src="https://g.alicdn.com/de/prismplayer/2.7.2/aliplayer-min.js"></script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="assets/js/html5shiv.js"></script>
  <script src="assets/js/respond.min.js"></script>
<![endif]-->
<!--[if lte IE 9]>
      <link href='assets/css/animations-ie-fix.css' rel='stylesheet'>
<![endif]-->

<div ng-if="module =='mp'" class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Promotional video coming soon.</h5>
      </div>
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
      </div>
    </div>
  </div>
</div>
<div ng-if="module =='mp'" class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>Search would be working soon.</h5>
      </div>
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
      </div>
    </div>
  </div>
</div>

<div ng-if="module =='mp'" class="modal fade" id="joinNowModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h5>The sign ups would begin soon</h5>
      </div>
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
      </div>
    </div>
  </div>
</div>

</body>
</html>
</body>
</html>
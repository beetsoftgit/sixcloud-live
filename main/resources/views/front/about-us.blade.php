<!DOCTYPE html>
<html ng-app="sixcloudAboutApp">
<head>
  <title>{{$title}}</title>
  <meta name="description" content="{{$meta}}">
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1" name="viewport"/>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="robots" content="{{$robotsContent}}" />

  <link rel="alternate" href="{{$alternate}}" hreflang="{{$alternatelang}}" />
  <link rel="canonical" href="{{$canonical}}" />
  <meta name="google-site-verification" content="F0XMjfssK8hcmTCfqJJcQFq80vAe8lHHegJBFA84XAQ" />
  <meta name="msvalidate.01" content="5789F571110EF5249F1B42276844821B" />
  <meta name="p:domain_verify" content="8475f8bbc093c663df8ead38f223dfac"/>

  <!-- Google Analytics -->
  <script>
  window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
  ga('create', 'UA-149665763-1');
  ga('send', 'pageview');
  </script>
  <script async src='https://www.google-analytics.com/analytics.js'></script>
  <!-- End Google Analytics -->


  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127371237-1"></script>
  <script>
   window.dataLayer = window.dataLayer || [];
   function gtag(){dataLayer.push(arguments);}
   gtag('js', new Date());

   gtag('config', 'UA-127371237-1');
  </script>

  {{ HTML::script('resources/assets/js/common/jquery.min.js') }}
  <link rel="shortcut icon" type="image/x-icon" href="resources/assets/images/favicon.ico">
  <!--Animation Style-->
  {{ HTML::style('resources/assets/css/front/mp/animations.css',array('rel'=>'stylesheet')) }}

  <!--Slider Style-->
  {{ HTML::style('resources/assets/css/front/mp/swiper.min.css',array('rel'=>'stylesheet')) }}

  <!--Fonts Style-->
  {{ HTML::style('resources/assets/css/front/mp/custom-font.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/font-awesome.min.css',array('rel'=>'stylesheet')) }}


  <!--jquery.FlowupLabels Style-->
  {{ HTML::style('resources/assets/css/front/mp/jquery.FlowupLabels.css',array('rel'=>'stylesheet')) }}

  <!--bootstrap-datetimepicker Style-->
  {{ HTML::style('resources/assets/css/front/mp/bootstrap-datetimepicker.min.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/asRange.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/common/star-rating.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/bootstrap.css',array('rel'=>'stylesheet')) }}

  <!--Custom Style-->
  {{ HTML::style('resources/assets/css/front/mp/style.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/mp_custom.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/responsive.css',array('rel'=>'stylesheet')) }}
  {{ HTML::script('resources/assets/js/common/angular.min.js') }}
  {{ HTML::script('resources/assets/js/common/angular-route.js') }}
  {{ HTML::script('resources/assets/js/common/angular-translate.min.js') }}
  {{ HTML::script('resources/assets/js/common/js.cookie.min.js') }}

  <!--{{ HTML::script('resources/assets/js/common/angular-sanitize.min.js') }} -->

    <!--[if lte IE 9]>
          <link href='assets/css/animations-ie-fix.css' rel='stylesheet'>
    <![endif]-->
  <!-- <script src="script.js"></script> -->
</head>
<body ng-controller="AboutController">
<div class="body-div">
  <!--Header section Started-->
  <header class="header-design2" data-spy="affix" data-offset-top="60">
    <div class="container">
      <nav class="navbar navbar-custom">
       <div class="container-fluid">
         <!-- Brand and toggle get grouped for better mobile display -->
         <div class="navbar-header">
           <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
             <span class="sr-only">Toggle navigation</span>
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
           </button>
           <a class="navbar-brand" href="<?=url('/')?>"><img style="height: auto;" ng-src="[[image_path]]/logo-new-white.png" width="170"  class="white-logo"><img style="height: auto;" ng-src="[[image_path]]/logo-blue.png" width="170"  class="blue-logo"></a>
         </div>

         <!-- Collect the nav links, forms, and other content for toggling -->
         <div class="collapse navbar-collapse hMenu" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav abc">
             <!-- <li class="active"><a href="javascript:;" id="TheCompany_click" data-name="TheCompany">[["lbl_the_company" | translate]]</a></li> -->
             <li class="active"><a href="<?=url('/') . '/about-us/the-company'?>" id="the-company_click" data-name="the-company">[["lbl_the_company" | translate]]</a></li>
             <li><a href="javascript:;" id="Vision_click" data-name="Vision">[["lbl_vision" | translate]]</a></li>
             <li><a href="javascript:;" id="misson_click" data-name="misson">[["lbl_mission" | translate]]</a></li>
             <!-- <li><a href="javascript:;" id="CoreValues_click" data-name="CoreValues">[["lbl_core_values" | translate]]</a></li> -->
             <li><a href="<?=url('/') . '/about-us/core-values'?>" id="core-values_click" data-name="core-values">[["lbl_core_values" | translate]]</a></li>
             <!-- <li><a href="javascript:;" id="OurTeam_click" data-name="OurTeam">[["lbl_our_team" | translate]]</a></li> -->
             <li><a href="<?=url('/') . '/about-us/our-team'?>" id="our-team_click" data-name="our-team">[["lbl_our_team" | translate]]</a></li>
            </ul>
            <!-- <ul class=" before-login nav navbar-right header-mid-btn about-us-btn">
              <li class="become-a-buyer"><a href="javascript:;" ng-if="eng == true" ng-click="changeLang('en')" class="btn about_language_custom uppercase login-btn">[['lbl_lang_chng'|translate]]</a>
              <a href="javascript:;" ng-if="!eng" ng-click="changeLang('chi')" class="btn about_language_custom uppercase login-btn">[['lbl_lang_chng'|translate]]</a>
              </li>
            </ul> -->
            <!-- <select ng-model="changeLangs" ng-change="changeLang(changeLangs)" class="before-login nav navbar-right header-mid-btn about-us-btn">
              <option value="chi">中文</option>
              <option value="en">English</option>
              <option value="ru">RU</option>
            </select> -->
            <select ng-model="changeLangs" ng-change="changeLang(changeLangs)" class="before-login nav navbar-right header-mid-btn about-us-btn">
                <option value="chi" ng-selected="changeLangs=='chi'">中文</option>
                <option value="en" ng-selected="changeLangs=='en'">English</option>
                <option value="ru" ng-selected="changeLangs=='ru'">Русский</option>
              </select>
             <!-- <ul class="nav navbar-nav navbar-right">
             <li class="become-a-buyer"><a href="#">English</a></li>
           </ul>  -->
         </div><!-- /.navbar-collapse -->
       </div><!-- /.container-fluid -->
     </nav>

    </div>
  </header>
  <!--Header section End-->

  <div id="main">
    <section class="page-head">
      <div style="background-image:url([[image_path]]/about-img.jpg);" class="img-div"></div>
      <div class="container">
        <h1>[["lbl_about_us" | translate]]</h1>
      </div>
    </section>
    <section class="the-company-section" id="the-company">
      <div class="container">
        <div class="inner-padding">
          <h2>[["lbl_the_company" | translate]]</h2>

          <p>[["lbl_company_text_1" | translate]]</p>

          <p>[["lbl_company_text_2" | translate]]</p>
        </div>
      </div>
    </section>
    <section style="background-image:url([[image_path]]/m-v-bg.jpg)" class="misson-vision-section" >
      <div class="container">
       
       <div class="row" id="Vision">
         <div class="col-md-6">
          <div class="box-div yellow-bg">
              <div class="inner-box">
                <h3>[["lbl_vision" | translate]]</h3>
                [["lbl_vision_text" | translate]]
              </div>
           </div>
         </div>
         <div class="col-md-6">
         </div>
       </div>
       <div class="row" id="misson">
         <div class="col-md-6">
         </div>
         <div class="col-md-6">
          <div class="box-div">
              <div class="inner-box">
                <h3>[["lbl_mission" | translate]]</h3>
                [["lbl_mission_text" | translate]]
              </div>
           </div>
         </div>
       </div>
      </div>
    </section>
    <section class="core-values-section" id="core-values">
      <div class="heading-div">
       <div class="container">
        <h3>[["lbl_core_values" | translate]]</h3>
        </div>
      </div>
      <div class="container top-m">
          <div class="inner-padd">
              <div class="row">
                  <div class="col-sm-6 col-md-4">
                      <div class="media">
                        <div class="pull-left">
                          <a href="#">
                            <img class="media-object" ng-src="[[image_path]]/cv-icon01.png" alt="...">
                          </a>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading">[["lbl_confidence_building" | translate]]</h4>
                          [["lbl_confidence_building_text" | translate]]
                        </div>
                      </div>
                  </div>
                  <div class="col-sm-6 col-md-4">
                      <div class="media">
                          <div class="pull-left">
                            <a href="#">
                              <img class="media-object" ng-src="[[image_path]]/cv-icon02.png" alt="...">
                            </a>
                          </div>
                          <div class="media-body">
                            <h4 class="media-heading">[["lbl_quality_affordability" | translate]]</h4>
                            [["lbl_quality_affordability_text" | translate]]
                          </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                          <div class="media">
                            <div class="pull-left">
                              <a href="#">
                                <img class="media-object" ng-src="[[image_path]]/cv-icon03.png" alt="...">
                              </a>
                            </div>
                            <div class="media-body">
                              <h4 class="media-heading">[["lbl_constant_positive_evolution" | translate]]</h4>
                              [["lbl_constant_positive_evolution_text" | translate]]
                            </div>
                          </div>
                      </div>
                      <div class="col-sm-6 col-md-4">
                          <div class="media">
                              <div class="pull-left">
                                <a href="#">
                                  <img class="media-object" ng-src="[[image_path]]/cv-icon04.png" alt="...">
                                </a>
                              </div>
                              <div class="media-body">
                                <h4 class="media-heading">[["lbl_trust" | translate]]</h4>
                              [["lbl_trust_text" | translate]]
                              </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                              <div class="media">
                                <div class="pull-left">
                                  <a href="#">
                                    <img class="media-object" ng-src="[[image_path]]/cv-icon05.png" alt="...">
                                  </a>
                                </div>
                                <div class="media-body">
                                  <h4 class="media-heading">[["lbl_tansparency" | translate]]</h4>
                                  [["lbl_tansparency_text" | translate]]
                                </div>
                              </div>
                          </div>
                          <div class="col-sm-6 col-md-4">
                              <div class="media">
                                  <div class="pull-left">
                                    <a href="#">
                                      <img class="media-object" ng-src="[[image_path]]/cv-icon06.png" alt="...">
                                    </a>
                                  </div>
                                  <div class="media-body">
                                    <h4 class="media-heading">[["lbl_social_responsibility" | translate]]</h4>
                                    [["lbl_social_responsibility_text" | translate]]
                                  </div>
                                </div>
                            </div>

              </div>
          </div>
      </div>
    </section>
    <section class="our-team-section" id="our-team">
      <div class="container">
            <div class="inner-padd">
             <h3>[['lbl_our_team'|translate]]</h3>
              <div class="white-block">
                  <h4 class="uppercase">RICHARD WOON</h4>
                    <p>[['lbl_richart_text'|translate]]</p>                 
                </div>
                <div class="white-block">
                  <h4 class="uppercase">Benson Lek</h4>
                    <p>[['lbl_benson_text'|translate]]</p>                 
                </div>
                <div class="white-block">
                  <h4 class="uppercase">Terrence Chee</h4>
                    <p>[['lbl_terrence_text'|translate]]</p>                 
                </div>
                <div class="white-block">
                  <h4 class="uppercase">Calina Chew</h4>
                    <p>[['lbl_calina_text'|translate]]</p>                 
                </div>
            </div>
            <span class="lb-circle"></span>
        </div>
    </section>

  </div>

  <!--Footer Start-->
  <footer class="animatedParent animateOnce blue-footer">
    <div class="footer-bg-repeat animated fadeIn slowest"></div>
    <div class="main-footer animated fadeIn slowest">
      <div class="container">
        <div class="row">
          <div class="col-sm-4">
            <div class="h-div">
              <div class="footer-logo"> <a href="<?=url('/')?>"><img ng-src="[[image_path]]/logo-blue.png"></a> </div>
              <p class="l-link"><a target="_blank" href="<?=url('/') ?>">www.sixclouds.[[ @if(strpos(url('/'), 'sixclouds.cn') !== false ||  strpos(url('/'), 'localhost') !== false) ]]cn[[ @else ]]net[[ @endif ]]</a></p>
              <div class="bottom-div"> <span class="copy">&copy;</span>[['lbl_all_rights_reserved'|translate]] </div>
              <p>[[ @if(strpos(url('/'), 'sixclouds.cn') !== false ||  strpos(url('/'), 'localhost') !== false) ]] ICP号为：渝ICP备18001226号 [[  @endif ]]</p>
            </div>

          </div>
          <div class="col-sm-7">
            <table cellpadding="0" cellspacing="0" width="100%">
              <tr>
                <td width="50%" valign="top"><div class="h-div" ng-class="{ru_text:changeLangs=='ru'}">
                    <h3>[["lbl_about_us" | translate]]</h3>
                    <ul>
                      <li><a href="<?=url('/') . '/about-us/the-company'?>">[['lbl_the_company'|translate]]</a></li>
                      <li><a href="<?=url('/') . '/about-us/core-values'?>">[['lbl_core_values'|translate]]</a></li>
                      <li><a href="<?=url('/') . '/about-us/our-team'?>">[['lbl_our_team'|translate]]</a></li>
                      <!-- <li><a href="javascript:;">[["lbl_ignite" | translate]] </a></li>
                      <li><a href="javascript:;">[["lbl_sixteen" | translate]] </a></li>
                      <li><a href="javascript:;">[["lbl_discover" | translate]] </a></li>
                      <li><a href="javascript:;">[["lbl_proof_reading" | translate]] </a></li> -->
                    </ul>
                  </div>
                </td>
                <td valign="top"><div class="h-div" ng-class="{ru_text:changeLangs=='ru'}">
                    <h3>[["lbl_quick_links" | translate]]</h3>
                    <ul>
                      <li><a target="_blank" href="<?=url('/') . '/customer-support'?>">[["lbl_customer_support" | translate]] </a></li>
                      <li><a target="_blank" href="<?=url('/') . '/privacy-policy'?>">[["lbl_privacy_policy" | translate]]</a></li>
                    </ul>
                  </div>
                   </td>
              </tr>
            </table>
          </div>

        </div>
      </div>
    </div>
  </footer>
</div>
<script>
    var BASEURL = "<?=url('/') . '/'?>";
</script>
{{ HTML::script('resources/assets/js/front/mp/popper.min.js') }}
{{ HTML::script('resources/assets/js/front/mp/bootstrap.js') }}
{{ HTML::script('resources/assets/js/front/mp/swiper.min.js') }}
{{ HTML::script('resources/assets/js/front/mp/css3-animate-it.js') }}
{{ HTML::script('resources/assets/js/front/mp/jquery.FlowupLabels.js') }}
{{ HTML::script('resources/assets/js/common/star-rating.js') }}

  <!--bootstrap-datetimepicker Start-->
{{ HTML::script('resources/assets/js/front/mp/moment.js') }}
{{ HTML::script('resources/assets/js/front/mp/bootstrap-datetimepicker.js') }}
{{ HTML::script('resources/assets/js/front/mp/jquery-asRange.js') }}
{{ HTML::script('resources/assets/js/common/lang-label.js') }}
{{ HTML::script('resources/assets/js/front/controllers/AboutController.js') }}

<!-- <script type="text/javascript" src="assets/js/script.js"></script> -->
<script type="text/javascript">
  $('body').css('background','#fff');
    $(window).load(function() {
     if($(window).width() > 767) {

     } else {
       $(function(){
           var navMain = $(".hMenu");
           navMain.on("click", "a", null, function () {
               navMain.collapse('hide');
           });
       });
     }
  });

    $('.hMenu a').click(function() {
      $('.hMenu li').removeClass('active');
          var datanameC = $(this).attr('data-name');
          var headerHei = $('header').height();
         $(this).parent('li').addClass('active');
          $('html, body').animate({
              scrollTop: $("#"+datanameC).offset().top-headerHei
            }, 1000);
    });
</script>

</body>
</html>
</body>
</html>

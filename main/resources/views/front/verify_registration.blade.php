<!DOCTYPE html>
<head>
    <title>SixClouds</title>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    {{ HTML::style('resources/assets/css/front/bootstrap.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/front/font-awesome.min.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/front/animate.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/front/style.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/plugins/owl-craousel/owl.carousel.min.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/plugins/boostrap-datepicker/css/daterangepicker.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/front/custom.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/front/six-clouds.css',array('rel'=>'stylesheet')) }}
    {{ HTML::script('resources/assets/js/common/jquery-3.2.1.min.js') }}
    <link rel="shortcut icon" href="<?php echo Config('constants.path.ASSETS_IMAGE') . 'logo.png' ?>"/>
    <base href="<?= url('/') . '/' ?>">
</head>
<body class="margin-zero">
    <div class="user-signup-page">
        <div class="user-content">
            <div class="container">
                <div class="logo text-center">
                    <img src="resources/assets/images/Corporate_Color.png">
                </div>
                <div class="options text-center">
                    <div class="row">
                        @if($current_language==1)
                            <div class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6">
                                <h2 class="title text-black margin-bt30">Thank you</h2><br>
                                <h2 class="title text-black margin-bt30">You can now login with your credentials.</h2>
                            </div>
                        @elseif($current_language==2)
                            <div class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6">
                                <h2 class="title text-black margin-bt30">谢谢您</h2><br>
                                <h2 class="title text-black margin-bt30">您现在可以登录。</h2>
                            </div>
                        @else
                            <div class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6">
                                <h2 class="title text-black margin-bt30">Благодарим Вас</h2><br>
                                <h2 class="title text-black margin-bt30">You can now login with your credentials.</h2>
                            </div>
                        @endif    
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{ HTML::script('resources/assets/js/common/sweetalert.min.js') }}
    {{ HTML::script('resources/assets/js/common/custom_alert.js') }}
    {{ HTML::script('resources/assets/js/front/core/bootstrap.min.js') }}
    {{ HTML::script('resources/assets/js/common/jquery.validate.min.js') }}
    <script>
        // set MODE for enable and disable console.log
        var project_mode = 'development'; //'production';
        var BASEURL = "<?= url('/') . '/' ?>";
    </script>
</body>
</html>
</body>
</html>
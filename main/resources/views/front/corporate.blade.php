<!DOCTYPE html>
<html ng-app="sixcloudCorporateApp">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1" name="viewport"/>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>{{$title}}</title>
  <meta name="description" content="{{$meta}}">
  <link rel="alternate" href="{{$alternate}}" hreflang="{{$alternatelang}}" />
  <link rel="canonical" href="{{$canonical}}" />
  <meta name="google-site-verification" content="F0XMjfssK8hcmTCfqJJcQFq80vAe8lHHegJBFA84XAQ" />
  <meta name="msvalidate.01" content="5789F571110EF5249F1B42276844821B" />
  <meta name="p:domain_verify" content="8475f8bbc093c663df8ead38f223dfac"/>

  <!-- Google Analytics -->
  <script>
  window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
  ga('create', 'UA-149665763-1');
  ga('send', 'pageview');
  </script>
  <script async src='https://www.google-analytics.com/analytics.js'></script>
  <!-- End Google Analytics -->

  <!-- Google Analytics -->
  <script>
  window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
  ga('create', 'UA-145370450-1');
  ga('send', 'pageview');
  </script>
  <script async src='https://www.google-analytics.com/analytics.js'></script>
  <!-- End Google Analytics -->



  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127371237-1"></script>
  <script>
   window.dataLayer = window.dataLayer || [];
   function gtag(){dataLayer.push(arguments);}
   gtag('js', new Date());

   gtag('config', 'UA-127371237-1');
  </script>
  {{ HTML::script('resources/assets/js/common/jquery.min.js') }}
  <link rel="shortcut icon" type="image/x-icon" href="resources/assets/images/favicon.ico">
  <!--Animation Style-->
  {{ HTML::style('resources/assets/css/front/mp/animations.css',array('rel'=>'stylesheet')) }}

  <!--Slider Style-->
  {{ HTML::style('resources/assets/css/front/mp/swiper.min.css',array('rel'=>'stylesheet')) }}

  <!--Fonts Style-->
  {{ HTML::style('resources/assets/css/front/mp/custom-font.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/font-awesome.min.css',array('rel'=>'stylesheet')) }}


  <!--jquery.FlowupLabels Style-->
  {{ HTML::style('resources/assets/css/front/mp/jquery.FlowupLabels.css',array('rel'=>'stylesheet')) }}

  <!--bootstrap-datetimepicker Style-->
  {{ HTML::style('resources/assets/css/front/mp/bootstrap-datetimepicker.min.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/asRange.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/common/star-rating.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/bootstrap.css',array('rel'=>'stylesheet')) }}

  <!--Custom Style-->
  {{ HTML::style('resources/assets/css/front/corporate/style.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/corporate/corporate_custom.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/corporate/responsive.css',array('rel'=>'stylesheet')) }}

  {{ HTML::script('resources/assets/js/common/angular.min.js') }}
  {{ HTML::script('resources/assets/js/common/angular-route.js') }}
  {{ HTML::script('resources/assets/js/common/angular-translate.min.js') }}
  {{ HTML::script('resources/assets/js/common/js.cookie.min.js') }}

  <!--{{ HTML::script('resources/assets/js/common/angular-sanitize.min.js') }} -->

    <!--[if lte IE 9]>
          <link href='assets/css/animations-ie-fix.css' rel='stylesheet'>
    <![endif]-->
  <!-- <script src="script.js"></script> -->
</head>
<body ng-controller="CorporateController">
<div class="body-div">
  <header class="header-design2 affix-top" data-spy="affix" data-offset-top="60">
    <div class="container">
      <nav class="navbar navbar-custom">
       <div class="container-fluid">
         <!-- Brand and toggle get grouped for better mobile display -->
         <div class="navbar-header">
           <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
             <span class="sr-only">Toggle navigation</span>
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
           </button>
           <a class="navbar-brand" href=""><img style="height: auto;" src="resources/assets/images/corporate/footer-logo.svg" width="170" class="white-logo"><img style="height: auto;" src="resources/assets/images/corporate/logo-blue.png" width="170" class="blue-logo"></a>
         </div>

         <!-- Collect the nav links, forms, and other content for toggling -->
         <div class="collapse navbar-collapse hMenu" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
             <li><a href="<?=url('/') . '/about-us'?>" data-name="TheCompany" class="ng-binding">[['lbl_about_us'|translate]]</a></li>
             <!-- <li><a href="javascript:;" data-name="misson" class="ng-binding">任务</a></li> -->
            </ul>
            <select ng-model="changeLangs" ng-change="changeLang(changeLangs)" class="before-login nav navbar-right header-mid-btn about-us-btn">
              <option value="chi" ng-selected="changeLangs=='chi'">中文</option>
              <option value="en" ng-selected="changeLangs=='en'">English</option>
              <option value="ru" ng-selected="changeLangs=='ru'">Русский</option>
            </select>
            <!-- <ul class=" before-login nav navbar-right header-mid-btn about-us-btn">
              <li class="become-a-buyer"><a href="javascript:;" ng-if="eng == true" ng-click="changeLang('en')" class="btn  uppercase login-btn">[['lbl_lang_chng'|translate]]</a>
              <a href="javascript:;" ng-if="!eng" ng-click="changeLang('chi')" class="btn uppercase login-btn">[['lbl_lang_chng'|translate]]</a>
              </li>
            </ul> -->
             <!-- <ul class="nav navbar-nav navbar-right">
             <li class="become-a-buyer"><a href="#">English</a></li>
           </ul>  -->
         </div><!-- /.navbar-collapse -->
       </div><!-- /.container-fluid -->
     </nav>

    </div>
  </header>
  <section style="background-image:url(resources/assets/images/corporate/blue-body-bg.png)" class="header-banner-section">
    <div class="container">
      <div class="logo"> <img src="resources/assets/images/corporate/Futuristic-logo.svg"> </div>
    </div>
    <img src="resources/assets/images/corporate/banner-img.png" width="100%"> 
  </section>
  <article class="futuristic-home-page">
    <section class="the-company-section">
      <div class="container">
        <h2>[['lbl_the_company'|translate]]</h2>
        <div class="inner-white-bg">
          <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
              <div class="row">
                <div class="col-sm-5">
                  <p><img src="resources/assets/images/corporate/vision.png" width="150"></p>
                  <h3>[['lbl_vision'|translate]]</h3>
                  <p>[['lbl_vision_text'|translate]]</p>
                </div>
                <div class="col-sm-2"> </div>
                <div class="col-sm-5">
                  <p><img src="resources/assets/images/corporate/mission.png" width="150"></p>
                  <h3>[['lbl_mission'|translate]]</h3>
                  <p>[['lbl_mission_text'|translate]]</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="our-product-section bg-pink">
      <div class="container bg-white">
        <h2>[['lbl_our_products'|translate]]</h2>
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2">
            <!-- <div class="row light-indigo">
              <div class="col-sm-5">
                <p><img src="resources/assets/images/corporate/f-img01.png" width="698"></p>
              </div>
              <div class="col-sm-7">
                <div class="right-content">
                  <h3><a class="sixteen_text" href="<?=url('/') . '/sixteen'?>">[['lbl_sixteen'|translate]]</a></h3>
                  <p>[['lbl_ignite_text_1'|translate]]</p>
                  <p>[['lbl_ignite_text_2'|translate]]</p>
                  <p>[['lbl_ignite_text_3'|translate]]</p>
                  <p>[['lbl_ignite_text_4'|translate]]</p>
                </div>
              </div>
            </div> -->
            <div class="row medium-pink">
              <div class="col-sm-12 pull-left">
                <div class="text-center">
                  <h3><a class="ignite_text" href="<?=url('/') . '/ignite'?>">[['lbl_ignite'|translate]]</a></h3>
                  <!-- <h4>Lorem Ipsum is simply dummy</h4> -->
                  <p>[['lbl_sixteen_text'|translate]]</p>
                  <p class="margin_top_10">[['lbl_sixteen_text1'|translate]]</p>
                  <p class="margin_top_10 pro-label">[['lbl_sixteen_text2'|translate]]</p>
                  <ul class="margin_top_10">
                    <li>[['lbl_sixteen_li1'|translate]]</li>
                    <li>[['lbl_sixteen_li2'|translate]]</li>
                    <li>[['lbl_sixteen_li3'|translate]]</li>
                  </ul>
                  <div>
                    <a class="common-btn" href="ignite">[['lbl_learn_more'|translate]]</a>
                  </div>
                  <div class="programme-section">
                    <h3><a class="ignite_text" href="ignite-mathematics">[['lbl_mathematics_programme'|translate]]</a></h3>
                    <div class="d-flex">
                      <div class="box">
                        <div class="buzz-english-div">
                          <div class="buzz-english-image">
                            <img src="resources/assets/images/smile-sg-button.png">
                          </div>
                          <div class="buzz-english-description">
                            <p class="ng-binding">[['lbl_smile_sg'|translate]]</p>
                            <a class="common-btn ng-binding" href="ignite-smile-singapore">[['lbl_learn_more'|translate]]</a>
                          </div>
                        </div>
                      </div>
                      <div class="box">
                        <div class="buzz-english-div">
                          <div class="buzz-english-image">
                            <img src="resources/assets/images/smile-button.png">
                          </div>
                          <div class="buzz-english-description">
                            <p class="ng-binding">[['lbl_smile_intl'|translate]]</p>
                            <a class="common-btn ng-binding" href="ignite-smile-international">[['lbl_learn_more'|translate]]</a>
                          </div>
                        </div>
                      </div>
                      <div class="box">
                        <div class="buzz-english-div">
                          <div class="buzz-english-image">
                            <img src="resources/assets/images/maze-button.png">
                          </div>
                          <div class="buzz-english-description">
                            <p class="ng-binding">[['lbl_maze'|translate]]</p>
                            <a class="common-btn ng-binding" href="ignite-maze">[['lbl_learn_more'|translate]]</a>
                          </div>
                        </div>
                      </div>
                      <div class="box">
                        <div class="buzz-english-div">
                          <div class="buzz-english-image">
                            <img src="resources/assets/images/smile_phil_button_draft.png">
                          </div>
                          <div class="buzz-english-description">
                            <p class="ng-binding">[['lbl_smile_ph'|translate]]</p>
                            <a class="common-btn ng-binding" href="ignite-smile-philippines">[['lbl_learn_more'|translate]]</a>
                          </div>
                        </div>
                      </div>
                      </div>
                  </div>
                    <div class="programme-section english-pro">
                      <h3><a class="ignite_text" href="ignite-buzz">[['lbl_english_programme'|translate]]</a></h3>
                      <div class="d-flex">
                        <div class="box">
                          <div class="buzz-english-div">
                            <div class="buzz-english-image">
                              <img src="resources/assets/images/buzz-button.png">
                            </div>
                            <div class="buzz-english-description">
                              <p class="ng-binding">[['lbl_buzz_description'|translate]]</p>
                              <a class="common-btn ng-binding" href="ignite-buzz">[['lbl_learn_more'|translate]]</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="our-product-section">
      <div class="container">
        <h2>[['lbl_our_partners'|translate]]</h2>
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2">
            <div class="row light-indigo our-partners">
              <div class="col-sm-4">
                <p><img src="resources/assets/images/corporate/IMG-20181031-WA0019.jpg" width="698"></p>
              </div>
              <div class="col-sm-4">
                <p><img src="resources/assets/images/corporate/uz_presidential_logo.jpg" width="598" class="img"></p>
              </div>
              <div class="col-sm-4">
                <p><img src="resources/assets/images/corporate/IMG-20181031-WA0020.jpg" width="698"></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </article>

  <!--Footer Start-->
  <footer class="animatedParent animateOnce blue-footer">
    <div class="footer-bg-repeat animated fadeIn slowest"></div>
    <div class="main-footer animated fadeIn slowest">
      <div class="container">
        <div class="row">
          <div class="col-sm-5">
            <div class="h-div">
              <div class="footer-logo"> <a href=""><img src="resources/assets/images/logo-blue.png" width="173" height="70"></a> </div>
              <p class="l-link"><a href="<?=url('/') ?>">www.sixclouds.[[ @if(strpos(url('/'), 'sixclouds.cn') !== false ||  strpos(url('/'), 'localhost') !== false) ]]cn[[ @else ]]net[[ @endif ]]</a></p>
              <div class="bottom-div"> <span class="copy">&copy;</span>[['lbl_all_rights_reserved'|translate]] </div>
              <p>[[ @if(strpos(url('/'), 'sixclouds.cn') !== false ||  strpos(url('/'), 'localhost') !== false) ]] ICP号为：渝ICP备18001226号 [[  @endif ]]</p>
            </div>
          </div>
          <div class="col-sm-7">
            <table cellpadding="0" cellspacing="0" width="100%">
              <tr>
                <td width="50%" valign="top"><div class="h-div" ng-class="{ru_text:changeLangs=='ru'}">
                    <h3>[['lbl_about_us'|translate]]</h3>
                    <ul>
                      <li><a href="<?=url('/') . '/about-us/the-company'?>">[['lbl_the_company'|translate]]</a></li>
                      <li><a href="<?=url('/') . '/about-us/core-values'?>">[['lbl_core_values'|translate]]</a></li>
                      <li><a href="<?=url('/') . '/about-us/our-team'?>">[['lbl_our_team'|translate]]</a></li>
                    </ul>
                  </div>
                </td>
                <td valign="top"><div class="h-div" ng-class="{ru_text:changeLangs=='ru'}">
                    <h3>[['lbl_quick_links'|translate]]</h3>
                    <ul>
                      <li><a target="_blank" href="<?=url('/') . '/customer-support'?>">[['lbl_customer_support'|translate]] </a></li>
                      <!-- <li><a href="javascript:;">[['lbl_terms_service'|translate]]</a></li> -->
                      <li><a target="_blank" href="<?=url('/') . '/privacy-policy'?>">[['lbl_privacy_policy'|translate]]</a></li>
                    </ul>
                  </div></td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  </footer>
</div>
<script>
    var BASEURL = "<?=url('/') . '/'?>";
</script>
{{ HTML::script('resources/assets/js/front/mp/popper.min.js') }}
{{ HTML::script('resources/assets/js/front/mp/bootstrap.js') }}
{{ HTML::script('resources/assets/js/front/mp/swiper.min.js') }}
{{ HTML::script('resources/assets/js/front/mp/css3-animate-it.js') }}
{{ HTML::script('resources/assets/js/front/mp/jquery.FlowupLabels.js') }}
{{ HTML::script('resources/assets/js/common/star-rating.js') }}

  <!--bootstrap-datetimepicker Start-->
{{ HTML::script('resources/assets/js/front/mp/moment.js') }}
{{ HTML::script('resources/assets/js/front/mp/bootstrap-datetimepicker.js') }}
{{ HTML::script('resources/assets/js/front/mp/jquery-asRange.js') }}

{{ HTML::script('resources/assets/js/front/controllers/CorporateController.js') }}

<!-- <script type="text/javascript" src="assets/js/script.js"></script> -->
<script type="text/javascript">
  $('body').css('background','#fff');
    $(window).load(function() {
     if($(window).width() > 767) {

     } else {
       $(function(){
           var navMain = $(".hMenu");
           navMain.on("click", "a", null, function () {
               navMain.collapse('hide');
           });
       });
     }
  });

    $('.hMenu a').click(function() {
      $('.hMenu li').removeClass('active');
          var datanameC = $(this).attr('data-name');
          var headerHei = $('header').height();
         $(this).parent('li').addClass('active');
          $('html, body').animate({
              scrollTop: $("#"+datanameC).offset().top-headerHei
            }, 1000);
    });
</script>

</body>
</html>
</body>
</html>

<!DOCTYPE html>
<html  ng-app="sixcloudsellerApp">
<head>
  <title>{{$title}}</title>
  <meta name="description" content="{{$meta}}">
  <meta name="robots" content="index,follow" />
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1" name="viewport"/>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="alternate" href="{{$alternate}}" hreflang="{{$alternatelang}}" />
  <link rel="canonical" href="{{$canonical}}" />
  <meta name="google-site-verification" content="F0XMjfssK8hcmTCfqJJcQFq80vAe8lHHegJBFA84XAQ" />
  <meta name="msvalidate.01" content="5789F571110EF5249F1B42276844821B" />
  <meta name="p:domain_verify" content="8475f8bbc093c663df8ead38f223dfac"/>

  <!-- Google Analytics -->
  <script>
  window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
  ga('create', 'UA-149665763-1');
  ga('send', 'pageview');
  </script>
  <script async src='https://www.google-analytics.com/analytics.js'></script>
  <!-- End Google Analytics -->

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127371237-1"></script>
  <script>
   window.dataLayer = window.dataLayer || [];
   function gtag(){dataLayer.push(arguments);}
   gtag('js', new Date());

   gtag('config', 'UA-127371237-1');
  </script>
  
  {{ HTML::script('resources/assets/js/common/jquery.min.js') }}
  <link rel="shortcut icon" type="image/x-icon" href="resources/assets/images/favicon.ico">
  <!--Animation Style-->
  {{ HTML::style('resources/assets/css/front/mp/animations.css',array('rel'=>'stylesheet')) }}

  <!--Slider Style-->
  {{ HTML::style('resources/assets/css/front/mp/swiper.min.css',array('rel'=>'stylesheet')) }}

  <!--Fonts Style-->
  {{ HTML::style('resources/assets/css/front/mp/custom-font.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/font-awesome.min.css',array('rel'=>'stylesheet')) }}


  <!--jquery.FlowupLabels Style-->
  {{ HTML::style('resources/assets/css/front/mp/jquery.FlowupLabels.css',array('rel'=>'stylesheet')) }}

  <!--bootstrap-datetimepicker Style-->
  {{ HTML::style('resources/assets/css/front/mp/bootstrap-datetimepicker.min.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/asRange.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/common/star-rating.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/bootstrap.css',array('rel'=>'stylesheet')) }}

  <!--Custom Style-->
  {{ HTML::style('resources/assets/css/front/mp/style.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/mp_custom.css',array('rel'=>'stylesheet')) }}
  {{ HTML::style('resources/assets/css/front/mp/responsive.css',array('rel'=>'stylesheet')) }}

  {{ HTML::script('resources/assets/js/common/angular.min.js') }}
  {{ HTML::script('resources/assets/js/common/angular-route.js') }}
  {{ HTML::script('resources/assets/js/common/angular-translate.min.js') }}
  {{ HTML::script('resources/assets/js/common/js.cookie.min.js') }}

    <!--[if lte IE 9]>
          <link href='assets/css/animations-ie-fix.css' rel='stylesheet'>
    <![endif]-->

  <!-- <script src="script.js"></script> -->
</head>
<body ng-controller="SellerController">
<div class="body-div buyer-and-seller-page">
  <!--Header section Started-->
  <header class="header-design3" data-spy="affix" data-offset-top="60">
    <div class="container">
      <nav class="navbar navbar-custom">
       <div class="container-fluid">
         <!-- Brand and toggle get grouped for better mobile display -->
         <div class="navbar-header">
           <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
             <span class="sr-only">Toggle navigation</span>
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
             <span class="icon-bar"></span>
           </button>
           <a class="navbar-brand" href="<?=url('/sixteen')?>"><img style="height: auto;" src="resources/assets/images/logo-white.png" width="170"  class="white-logo"><img style="height: auto;" src="resources/assets/images/logo.png" width="170" class="blue-logo"></a>
         </div>

         <!-- Collect the nav links, forms, and other content for toggling -->
         <div class="collapse navbar-collapse hMenu" id="bs-example-navbar-collapse-1">
           <ul class="nav navbar-nav">
             <li class="active"><a href="javascript:;" data-name="SKILLS">[['lbl_skills'|translate]]</a></li>
             <li><a href="javascript:;" data-name="PORTFOLIO">[['lbl_portfolio'|translate]]</a></li>
             <li><a href="javascript:;" data-name="REALJOBS">[['lbl_real_jobs'|translate]]</a></li>
             <li><a href="javascript:;" data-name="UNIQUEMATRIXSYSTEM">[['lbl_unique_matrix_system'|translate]]
             </a></li>
             <li><a href="javascript:;" data-name="INSPIRATIONBANK">[['lbl_inspiration_bank'|translate]]</a></li>
           </ul>
          
           <ul class="nav navbar-nav navbar-right">
              <li class="become-a-buyer"><a href="<?=url('/') . '/seller-signup'?>">[["lbl_become_seller" | translate]]</a></li>
              <li class="become-a-buyer">
                <a href="javascript:;" ng-if="!eng" ng-click="changeLang('chi')" class="uppercase login-btn">[['lbl_lang_chng'|translate]]</a>
                <a href="javascript:;" ng-if="eng == true" ng-click="changeLang('en')" class="uppercase login-btn">[['lbl_lang_chng'|translate]]</a>
                
              </li>
           </ul>

         </div><!-- /.navbar-collapse -->
       </div><!-- /.container-fluid -->
     </nav>

    </div>
  </header>

  <!--Header section End-->
  <div id="main">
    <section class="page-head">
      <div style="background-image:url(resources/assets/images/head-bg01.png); " class="img-div"></div>
      <div class="container">
        <h1 class="uppercase">[["lbl_become_seller" | translate]] </h1>
      </div>
    </section>
    <div>
      <img alt="" src="resources/assets/images/h-bg-design.png" width="100%">
    </div>
    <section class="block01-section" id="SKILLS">
    <div class="container">
      <div class="row">
        <div class="display-table">
          <div class="col-sm-6 display-table-cell">
            <div class="left-content">
              <h2>[['lbl_skills'|translate]]</h2>
              <h3>[['lbl_creative_skills' | translate]]</h3>
              <p><strong>[['lbl_sign_up' | translate]]</strong> - [['lbl_skill_text' | translate]]</p>
              <p><strong>[['lbl_wide_variety' | translate]]</strong> - [['lbl_wid_variety_text' | translate]]</p>

            </div>
          </div>
          <div class="col-sm-6 display-table-cell text-right">
            <img alt="" src="resources/assets/images/bsI-img01.png" >
          </div>
        </div>

      </div>
    </div>
    </section>
    <div>
      <img alt="" src="resources/assets/images/separator-01.png" width="100%">
    </div>
    <section class="block01-section" id="PORTFOLIO">
    <div class="container">
      <div class="row">
        <div class="display-table">
          <div class="col-sm-6 display-table-cell">
            <img alt="" src="resources/assets/images/bsI-img02.png" class="hidden-xs">
          </div>
          <div class="col-sm-6 display-table-cell text-right">
            <div class="left-content">
              <h2>[['lbl_portfolio'|translate]]</h2>
              <h3>[['lbl_build_portfolio'|translate]]</h3>
              <p><strong>[['lbl_beef_portfolio'|translate]] </strong> - [['lbl_portfolio_text'|translate]] </p>
            </div>
            <!--mobile display-->
            <div class="visible-xs">
              <img alt="" src="resources/assets/images/bsI-img02.png" >
            </div>
            <!--mobile display-->
          </div>

        </div>

      </div>
    </div>
    </section>
    <div>
      <img alt="" src="resources/assets/images/separator-02.png" width="100%">
    </div>
    <section class="block01-section" id="REALJOBS">
    <div class="container">
      <div class="row">
        <div class="display-table">
          <div class="col-sm-6 display-table-cell">
            <div class="left-content">
              <h2>[['lbl_real_jobs'|translate]]</h2>
              <h3>[['lbl_easy_connect_buyer'|translate]]</h3>
              <p><strong>[['lbl_suggestions'|translate]]</strong> - [['lbl_suggestions_text'|translate]]</p>
              <p><strong>[['lbl_grow_network'|translate]]</strong> - [['lbl_grow_network_text'|translate]]</p>
            </div>
          </div>
          <div class="col-sm-6 display-table-cell text-right">
            <img alt="" src="resources/assets/images/bsI-img03.png" >
          </div>
        </div>

      </div>
    </div>
    </section>
    <div>
      <img alt="" src="resources/assets/images/separator-01.png" width="100%">
    </div>
    <section class="block01-section" id="UNIQUEMATRIXSYSTEM">
    <div class="container">
      <div class="row">
        <div class="display-table">
          <div class="col-sm-6 display-table-cell">
            <img alt="" src="resources/assets/images/bsI-img04.png" class="hidden-xs">
          </div>
          <div class="col-sm-6 display-table-cell text-right">
            <div class="left-content">
              <h2>[['lbl_unique_matrix_system'|translate]]</h2>
              <h3>[['lbl_improve_your_service' | translate]]</h3>
              <p><strong>[['lbl_improvement' | translate]]</strong> -[['lbl_improvement_text' | translate]] </p>

            </div>
            <!--mobile display-->
            <div class="visible-xs">
              <img alt="" src="resources/assets/images/bsI-img04.png" >
            </div>
            <!--mobile display-->
          </div>

        </div>

      </div>
    </div>
    </section>
    <div>
      <img alt="" src="resources/assets/images/separator-02.png" width="100%">
    </div>
    <section class="block01-section" id="INSPIRATIONBANK">
    <div class="container">
      <div class="row">
        <div class="display-table">
          <div class="col-sm-6 display-table-cell">
            <div class="left-content">
              <h2>[['lbl_inspiration_bank'|translate]]</h2>
              <h3>[['lbl_inspired'|translate]]</h3>
              <p><strong>[['lbl_creative_juices'|translate]] </strong> - [['lbl_creative_juices_text'|translate]] </p>
              <p><strong>[['lbl_contribute'|translate]] </strong> - [['lbl_contribute_text'|translate]]</p>
            </div>

          </div>
          <div class="col-sm-6 display-table-cell text-right">
            <img alt="" src="resources/assets/images/bsI-img05.png">
          </div>


        </div>

      </div>
    </div>
    </section>
  </div>

  <!--Footer Start-->
  <footer class="animatedParent animateOnce">
    <div class="footer-bg-repeat animated fadeIn slowest"></div>
    <div class="main-footer animated fadeIn slowest">
      <div class="container">
        <div class="row">
          <div class="col-sm-4">
            <div class="h-div bs-copy-right">
              <div class="footer-logo"> <a href="sixteen"><img src="resources/assets/images/logo.png"></a> </div>
              <p><a target="_blank" href="<?=url('/')?>">www.sixclouds.[[ @if(strpos(url('/'), 'sixclouds.cn') !== false ||  strpos(url('/'), 'localhost') !== false) ]]cn[[ @else ]]net[[ @endif ]]</a>
              <div class="bottom-div"> <span class="copy">&copy;</span>[['lbl_allrights'|translate]]</div>
              <p>[[ @if(strpos(url('/'), 'sixclouds.cn') !== false ||  strpos(url('/'), 'localhost') !== false) ]] ICP号为：渝ICP备18001226号 [[  @endif ]]</p>
            </div>

          </div>
          <div class="col-sm-7">
            <table cellpadding="0" cellspacing="0" width="100%">
              <tr>
                <td width="50%" valign="top"><div class="h-div">
                    <h3>[["lbl_about_us" | translate]]</h3>
                    <ul>
                      <!-- <li><a target="_blank" href="<?=url('/') . '/about-us'?>">[["lbl_about_us" | translate]]</a></li> -->
                      <li><a target="_blank" href="<?=url('/') . '/about-us/the-company'?>">[['lbl_the_company'|translate]]</a></li>
                      <li><a target="_blank" href="<?=url('/') . '/about-us/core-values'?>">[['lbl_core_values'|translate]]</a></li>
                      <li><a target="_blank" href="<?=url('/') . '/about-us/our-team'?>">[['lbl_our_team'|translate]]</a></li>
                      <!-- <li><a href="javascript:;">[["lbl_ignite" | translate]] </a></li>
                      <li><a href="javascript:;">[["lbl_sixteen" | translate]] </a></li>
                      <li><a href="javascript:;">[["lbl_discover" | translate]] </a></li>
                      <li><a href="javascript:;">[["lbl_proof_reading" | translate]] </a></li> -->
                    </ul>
                  </div>
                </td>
                <td valign="top"><div class="h-div">
                    <h3>[["lbl_quick_links" | translate]]</h3>
                    <ul>
                      <li><a target="_blank" href="<?=url('/') . '/become-seller'?>">[["lbl_become_seller" | translate]]</a></li>
                      <li><a target="_blank" href="<?=url('/') . '/become-buyer'?>">[["lbl_become_buyer" | translate]]</a></li>
                      <li><a target="_blank" href="<?=url('/') . '/customer-support'?>">[["lbl_customer_support" | translate]] </a></li>
                      <li><a target="_blank" href="<?=url('/') . '/sixteen-terms-of-service'?>">[["lbl_terms_service" | translate]]  </a></li>
                      <li><a target="_blank" href="<?=url('/') . '/privacy-policy'?>">[["lbl_privacy_policy" | translate]]</a></li>
                    </ul>
                  </div>
                   </td>
              </tr>
            </table>
          </div>

        </div>
      </div>
    </div>
  </footer>
</div>

{{ HTML::script('resources/assets/js/front/mp/popper.min.js') }}
{{ HTML::script('resources/assets/js/front/mp/bootstrap.js') }}
{{ HTML::script('resources/assets/js/front/mp/swiper.min.js') }}
{{ HTML::script('resources/assets/js/front/mp/css3-animate-it.js') }}
{{ HTML::script('resources/assets/js/front/mp/jquery.FlowupLabels.js') }}
{{ HTML::script('resources/assets/js/common/star-rating.js') }}

  <!--bootstrap-datetimepicker Start-->
{{ HTML::script('resources/assets/js/front/mp/moment.js') }}
{{ HTML::script('resources/assets/js/front/mp/bootstrap-datetimepicker.js') }}
{{ HTML::script('resources/assets/js/front/mp/jquery-asRange.js') }}

{{ HTML::script('resources/assets/js/front/controllers/SellerController.js') }}

<!-- <script type="text/javascript" src="assets/js/script.js"></script> -->
<script type="text/javascript">
  $('body').css('background','#fff');

    $(window).load(function() {
     if($(window).width() > 767) {

     } else {
       $(function(){
           var navMain = $(".hMenu");
           navMain.on("click", "a", null, function () {
               navMain.collapse('hide');
           });
       });
     }
  });

    $('.hMenu a').click(function() {
      $('.hMenu li').removeClass('active');
          var datanameC = $(this).attr('data-name');
          var headerHei = $('header').height();
         $(this).parent('li').addClass('active');
          $('html, body').animate({
              scrollTop: $("#"+datanameC).offset().top-headerHei
            }, 1000);
    });
</script>

</body>
</html>
</body>
</html>

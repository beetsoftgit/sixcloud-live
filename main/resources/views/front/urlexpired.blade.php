<!DOCTYPE html>
<head>
    <title>SixClouds</title>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    {{ HTML::style('resources/assets/css/front/bootstrap.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/front/font-awesome.min.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/front/animate.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/front/style.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/plugins/owl-craousel/owl.carousel.min.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/plugins/boostrap-datepicker/css/daterangepicker.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/front/custom.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/front/six-clouds.css',array('rel'=>'stylesheet')) }}
    {{ HTML::script('resources/assets/js/common/jquery-3.2.1.min.js') }}
    <link rel="shortcut icon" href="<?php echo Config('constants.path.ASSETS_IMAGE') . 'logo.png' ?>"/>
    <base href="<?= url('/') . '/' ?>">
</head>
<body class="margin-zero">
    <div class=" user-signup-page">
        <div class="user-content">
            <div class="container">
                <div class="logo text-center">
                    <img src="resources/assets/images/Corporate_Color.png">
                </div>
                <div class="options text-center">
                    <div class="row">
                        @if(isset($admin_suspended_account)&&$admin_suspended_account==1)
                            @if($current_language==1)
                                <h2 class="title text-black margin-bt30">Your account has been suspended by Admin.</h2>
                            @else
                                <h2 class="title text-black margin-bt30">您的帐户已被暂停的管理终端。</h2>
                            @endif
                        @else
                            @if(isset($verification_again))
                                @if($current_language==1)
                                    <h2 class="title text-black margin-bt30">Check your mailbox</h2>
                                @else
                                    <h2 class="title text-black margin-bt30">检查您的邮箱</h2>
                                @endif
                            @elseif (isset($expired))
                                 @if($current_language==1)
                                    <h2 class="title text-black margin-bt30">Oops, the link has expired. <br>You have already registered your account</h2>
                                @else
                                    <h2 class="title text-black margin-bt30">哎呀，链接已过期。 <br>您已经注册了您的帐户。</h2>
                                @endif
                            @elseif (isset($resetExpired) && $resetExpired==1)
                                 @if($current_language==1)
                                    <h2 class="title text-black margin-bt30">Oops, the link has expired.</h2>
                                @else
                                    <h2 class="title text-black margin-bt30">哎呀，链接已过期。</h2>
                                @endif
                            @else
                                @if($current_language==1)
                                    <div class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6">
                                    
                                        <h2 class="title text-black margin-bt30">Oops, the link has expired.</h2>
                                   
                                        <h2 class="title text-black margin-bt30">Please click <a href="{{$verificationURL}}">Here</a><br>to have a new link sent to your mailbox.</h2>
                                   </div>
                                   @else
                                    <div class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6">
                                    
                                        <h2 class="title text-black margin-bt30">糟了，链接网址已过期了。</h2>
                                   
                                        <h2 class="title text-black margin-bt30">请点击<a href="{{$verificationURL}}">这里</a>新的链接网址将发送到您的邮箱。</h2>
                                   </div>
                                @endif
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{ HTML::script('resources/assets/js/front/basic.js') }}
    {{ HTML::script('resources/assets/js/common/sweetalert.min.js') }}
    {{ HTML::script('resources/assets/js/common/custom_alert.js') }}
    {{ HTML::script('resources/assets/js/front/core/bootstrap.min.js') }}
    {{ HTML::script('resources/assets/js/common/jquery.validate.min.js') }}
    <script>
        // set MODE for enable and disable console.log
        var project_mode = 'development'; //'production';
        var BASEURL = "<?= url('/') . '/' ?>";
    </script>
</body>
</html>
</body>
</html>
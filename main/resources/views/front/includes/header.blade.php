<header>
    <nav class="navbar margin-zero">
        <div class="container">
            <div class="navbar-header">
                <a class="logo" href="home">
                    <img src="resources/assets/images/Logo-large.png">
                </a>
            </div>
            <div class="navbar-right">
                <ul class="list-unstyled list-inline margin-zero before-login">
                    <li id="signin_link" class=""><a href="signin">[['lbl_signin'|translate]] </a></li>
                    <li id="signup_link"><a href="signup" class="btn">[['lbl_signup'|translate]]</a></li>
                    <!-- <div id="welcome_text"></div>
                    <li id="logout_link"><a href="logout" class="btn"> SIGN OUT </a></li>-->
                </ul>
                <div class="after-login dropdown" id="after-login">
                    <div class="media">
                        <div class="media-left">
                            <div class="img">
                                <img src="resources/assets/images/user.jpg">
                            </div>
                        </div>
                        <div class="media-body dropdown-toggle" type="button" data-toggle="dropdown">
                            <div class="name pull-left">
                                <b class="welcome_text"></b>
                                <div class="credits">
                                    [['lbl_credit_earned'|translate]] : &yen; 50
                                </div>
                            </div>
                            <span class="caret pull-right"></span>
                        </div>
                        <ul class="dropdown-menu">
                            <li class="language"   ng-controller="language">
                                <i class="sci-English icon"></i> English
                                <span class="checkbox-set">
                                    <input type="checkbox" id="chk_lang" ng-model="chk_lang" ng-checked="[[selectedLang]]">
                                    <span class="checkbox-btn">
                                    </span>
                                </span>
                                <i class="sci-English icon"></i>
                            </li>
                            <li><a href="elt"><i class="sci-English-Lerning-Training icon"></i>[['lbl_elt'|translate]]</a></li>
                            <li><a href="#"><i class="sci-Marketplace icon"></i>[['lbl_market'|translate]]</a></li>
                            <li><a href="my-cases"><i class="sci-Proof-Reading icon"></i>[['lbl_proof_reading'|translate]]</a></li>
                            <li><a href="#"><i class="sci-My-Account icon"></i>[['lbl_my_account'|translate]]</a></li>
                            <li><a href="#"><i class="sci-Notification icon"></i>[['lbl_notification'|translate]]</a></li>
                            <li id="logout_link"><a href="logout"><i class="sci-Log-Out icon"></i>[['lbl_signout'|translate]]</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</header>

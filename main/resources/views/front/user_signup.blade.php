<!DOCTYPE html>
<head>
    <title>SixClouds :: Signup</title>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    {{ HTML::style('resources/assets/css/front/bootstrap.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/front/font-awesome.min.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/front/animate.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/front/style.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/plugins/owl-craousel/owl.carousel.min.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/plugins/boostrap-datepicker/css/daterangepicker.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/common/sweet-alert.min.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/front/custom.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/front/six-clouds.css',array('rel'=>'stylesheet')) }}
    {{ HTML::script('resources/assets/js/common/jquery-3.2.1.min.js') }}
    <link rel="shortcut icon" href="<?php echo Config('constants.path.ASSETS_IMAGE') . 'Logo-large.png' ?>"/>
    <base href="<?= url('/') . '/' ?>">
</head>
<body class="margin-zero">


    <div class="main user-signup-page">
        <div class="box-bottom">
        </div>
        <div class="user-signup">
            <div class="bg-box">
                <div class="box-left">
                    <div class="addbox">
                    </div>
                </div>
                <div class="dots pull-right">
                    <img src="resources/assets/images/dots.png">
                </div>
                <div class="box-right">
                </div>
            </div>
        </div>
        <div class="user-content">
            <div class="container">
                <div class="logo text-center">
                    <img src="resources/assets/images/Logo-large.png">
                </div>
                <div class="text-black">
                    <h2 class="title text-black">REGISTER YOUR ACCOUNT</h2>
                    <span>TO ACCESS SIX CLOUDS' PRODUCTS</span>
                </div>
                <div class="progaram-section">
                    <div class="box-list">
                        <div class="box">
                            <div class="icon"><span class="sci-learning"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span></span></div>
                            <div class="text">ENGLISH LANGUAGE LEARNING</div>
                        </div>
                        <div class="box">
                            <div class="icon"><span class="sci-market-place"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span></span></div>
                            <div class="text">ENGLISH LANGUAGE LEARNING</div>
                        </div>
                        <div class="box">
                            <div class="icon"><span class="sci-proof-reading"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span></span></div>
                            <div class="text">ENGLISH LANGUAGE LEARNING</div>
                        </div>
                    </div>
                </div>
                <div class="options text-center">
                    <div class="row">
                       <div class="col-sm-6">
                            <div class="icon">
                                <span class="sci-wechat"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span></span>
                            </div>
                            <h2 class="title text-black">Sign In Using</h2>
                            <div class="sub-header text-black">Your WeChat Account</div>
                            <div class="barcode">
                                <span class="sci-barcode"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span><span class="path22"></span><span class="path23"></span><span class="path24"></span><span class="path25"></span><span class="path26"></span><span class="path27"></span><span class="path28"></span><span class="path29"></span><span class="path30"></span><span class="path31"></span><span class="path32"></span><span class="path33"></span><span class="path34"></span><span class="path35"></span><span class="path36"></span><span class="path37"></span><span class="path38"></span><span class="path39"></span><span class="path40"></span><span class="path41"></span><span class="path42"></span><span class="path43"></span><span class="path44"></span><span class="path45"></span><span class="path46"></span><span class="path47"></span><span class="path48"></span><span class="path49"></span><span class="path50"></span><span class="path51"></span><span class="path52"></span><span class="path53"></span><span class="path54"></span><span class="path55"></span><span class="path56"></span><span class="path57"></span><span class="path58"></span><span class="path59"></span><span class="path60"></span><span class="path61"></span><span class="path62"></span><span class="path63"></span><span class="path64"></span><span class="path65"></span><span class="path66"></span><span class="path67"></span><span class="path68"></span><span class="path69"></span><span class="path70"></span><span class="path71"></span></span>
                            </div>
                            <div class="text-center text">
                                Scan the barcode from your WeChat and register your account.
                            </div>
                            <div class="ordivider">
                                <span> OR </span>
                            </div>
                            <div class="text-center margin-bt40">
                                    <a href="http://contractorhost.com/six-clouds/Front/login.html" class=" btn text-white"><i class="sci-sign-in"></i><b> Sign In</b></a>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="icon">
                                <span class="sci-startup-1"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span></span>
                            </div>
                            <h2 class="title text-black">Sign In Using</h2>
                            <div class="sub-header text-black">Your  Email Address</div>
                            <div class="form typing-effect ">
                                <form method="post" action="javascript:;" id="formSignup" class="validation-form">
                                    <div class="form-group input-label">
                                        <input type="text" name="first_name" id="first_name" class="form-control">
                                        <span>First Name:</span>
                                    </div>
                                    <div class="form-group input-label">
                                        <input type="text" name="last_name" id="last_name" class="form-control">
                                        <span>Last Name:</span>
                                    </div>
                                    <div class="form-group input-label">
                                        <input type="text" name="email_address" id="email_address"  class="form-control">
                                        <span>Email Address:</span>
                                    </div>
                                    <div class="form-group input-label">
                                        <input type="password" name="password" id="password"  class="form-control">
                                        <span>Password:</span>
                                    </div>
                                    <div class="form-group input-label">
                                        <input type="text" name="contact" id="contact" class="form-control">
                                        <span>Contact Number:</span>
                                    </div>
                                    <div class="form-group">
                                        <select name="country_id" id="country_id" class="form-control">
                                            <option value="" selected>Country:</option>
                                            @foreach ($country as $key => $value)
                                            <option value="{{ $value['id'] }}">{{ $value['name'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select disabled="" name="province_id" id="province_id" class="form-control">
                                            <option value="" selected>Province:</option>
                                            @foreach ($province as $key => $value)
                                            <option value="{{ $value['id'] }}">{{ $value['proviance_name'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select name="city_id" id="city_id" class="form-control">
                                            <option value="" selected>City:</option>
                                            @foreach ($city as $key => $value)
                                            <option value="{{ $value['id'] }}">{{ $value['name'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group input-label">
                                        <input type="" name="" class="form-control">
                                        <span>Promo or Referral Code:</span>
                                    </div>
                                    <div class="text-center">
                                        <!--<a class="btn">REGISTER</a>-->
                                        <button class="btn" id="btnETLSignup" type="submit">
                                        <i class="sci-register"></i> REGISTER</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer>
        <div class="container">
            <div class="">
                <div class="list-section">
                    <ul class="list-unstyled">
                        <li class="ttl">WHO WE ARE</li>
                        <li><a href="">Company</a></li>
                        <li><a href="">Community</a></li>
                        <li><a href="">Careers</a></li>
                        <li><a href="">Terms of Use</a></li>
                    </ul>
                    <ul class="list-unstyled">
                        <li class="ttl">SUPPORT</li>
                        <li><a href="">Tutorials</a></li>
                        <li><a href="">Guides</a></li>
                        <li><a href="">Video</a></li>
                    </ul>
                    <ul class="list-unstyled">
                        <li class="ttl">PRODUCTS</li>
                        <li><a href="">SixClouds App</a></li>
                        <li><a href="">SixClouds Desktop</a></li>
                        <li><a href="">SixClouds Cloud</a></li>
                    </ul>
                    <ul class="list-unstyled">
                        <li class="ttl">CONTACT US</li>
                        <li><a href="">hello@123.com</a></li>
                        <li><a href="">880 Johns Route</a></li>
                        <li><a href="">China</a></li>
                    </ul>
                    <ul class="list-unstyled">
                        <li class="ttl">SUBSCRIBE</li>
                        <li class="email"><a class="a-uline-remove">Enter your email to get notified about our new Solutions</a></li>
                        <li class="mail-input">
                            <input type="" name="" placeholder="Email" class="form-control">
                            <a class="btn">Submit</a>
                        </li>
                    </ul>

                </div>
            </div>
            <div class="sub-footer margin-tp20">
                <div class="pull-left social-text">
                    &copy; <?php echo date('Y'); ?> SixClouds, All rights reserved.
                </div>
                <div class="pull-right">
                    <span class="social-text">Yes, We are social</span>
                    <ul class="nav navbar-nav social-media pull-right">
                        <li class="insta">
                            <a href="#">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                        <li class="fb">
                            <a href="#">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li class="twit">
                            <a href="#">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
     <a class="btn-top" style="">
        <i class="fa fa-arrow-up"></i>
    </a>
    <!--    <script src="script.js"></script>
        <script type="text/javascript" src="assets/js/jquery.min.js">-->
    <!--</script>-->
    {{ HTML::script('resources/assets/js/common/jquery.validate.min.js') }}
    {{ HTML::script('resources/assets/js/common/sweetalert.min.js') }}
    {{ HTML::script('resources/assets/js/common/custom_alert.js') }}
    {{ HTML::script('resources/assets/js/front/basic.js') }}
    <script>
        // set MODE for enable and disable console.log
        var project_mode = 'development'; //'production';
        var BASEURL = "<?= url('/') . '/' ?>";
    </script>
    <script type="text/javascript">
        $(window).on('load', function(){
          $(".loader").fadeOut('slow');
        });
        $(document).ready(function () {
            
           $(".input-label input").focus(function() {
           $(this).parent(".input-label").addClass('typing');

         });
       $('.input-label input').blur(function() {
           if ($(this).val() != '') {
               $(this).parent(".input-label").addClass('typing');
           } else {
               $(this).parent(".input-label").removeClass('typing');
           }
           });
           $('.btn-top').click(function () {
                $("html, body").animate({
                    scrollTop: 0
                }, 600);
                return false;
            });
            $("#main").scrollTop(900);
            $(".btn-top").css("opacity", "1");
            $(".btn-top").click(function () {
                $("#main").scrollTop();
            });
        });
    </script>
</body>
</html>
<html>
    <body>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,300' rel='stylesheet' type='text/css'>
        <table width="614" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:12px;color:#656565;background: #fff;-webkit-border-radius: 8px;-moz-border-radius: 8px;border-radius: 8px;-webkit-box-shadow: 0px -1px 5px #DDD;-moz-box-shadow: 0px -1px 3px #DDD;box-shadow: 0px -1px 5px #DDD;width: 168px;border: 1px solid #e2e2e2;">
            <tbody>
                <tr>
                    <td style="border-radius: 8px 8px 0 0; position: relative; text-align:center; background: #8972f0; padding: 10px; border-bottom: 2px solid #f3f3f3;">
                        <a href="<?php echo '#'; ?>" target="_blank">                            
                            <img src="{{ URL::to('resources/assets/images/logo-sixteen-white.png') }}" alt="Six-clouds" width="100" border="0" >
                        </a>
                    </td>
                </tr>
                <tr>
                    <td style="padding:10px; ">                        
                        <table width="576" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#656565">
                            <tbody>
                                <tr>
                                    <td style="padding:0 10px 20px 10px;">
                                        <table width="554" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        @if($current_language==1)
                                                        <div style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size: 14px; color: #6C6C6C; margin-bottom: 5px;">
                                                            <span style="font-size: 15px;">Hello {{ $buyerfirst_name }} {{ $buyerlast_name }},</span> <br><br> Thank you for using SixClouds Sixteen. <br><br>Please note that all copies of Project related files, will be permanently removed from our site, 14 days from the date of this notification.
                                                            <br><br>.<br><br>
                                                        </div>
                                                        @else
                                                        <!-- chinese -->
                                                        <div style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size: 14px; color: #6C6C6C; margin-bottom: 5px;">
                                                            <span style="font-size: 15px;">你好 {{ $buyerfirst_name }} {{ $buyerlast_name }},</span> <br><br> 感谢您使用六云十六. <br><br>请切记，项目所有相关的文件将在本通知14天后从我们的网站永久删除。
                                                            <br><br>.<br><br>
                                                        </div>
                                                        @endif
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>        
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table width="576" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#6c6c6c">
                            <tbody>
                                <tr>
                                    <td style="padding:0 10px 20px 10px;">
                                        <table width="554" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size: 14px; color: #6C6C6C;">
                                                            <?php //echo $html; ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>        
                                    </td>
                                </tr>
                                <tr>
                                    @if($current_language==1)
                                    <td style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:14px;padding:10px;border-top: 1px solid #e6e6e6;    font-weight: 600;">
                                        From,<br /> SixClouds Sixteen
                                    </td>
                                    @else
                                    <td style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:14px;padding:10px;border-top: 1px solid #e6e6e6;    font-weight: 600;">
                                       六云十六
                                    </td>
                                    @endif
                                </tr>
                            </tbody>
                        </table>                        
                    </td>
                </tr>
                <tr>
                    @if($current_language==1)
                    <td style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size:11px; line-height:16px; padding:15px 18px; text-align:center; border-radius: 0 0 8px 8px; background-color: #8972f0; border-top: 3px solid #d7a343; color: #fff;">
                        <?php echo date('Y'); ?>&copy; SixClouds            
                    </td>
                    @else
                    <td style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size:11px; line-height:16px; padding:15px 18px; text-align:center; border-radius: 0 0 8px 8px; background-color: #8972f0; border-top: 3px solid #d7a343; color: #fff;">
                        <?php echo date('Y'); ?>&copy; 六云            
                    </td>
                    @endif
                </tr>
            </tbody>
        </table>
    </body>
</html>
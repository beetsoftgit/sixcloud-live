<html>
    <body>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,300' rel='stylesheet' type='text/css'>
        <table width="614" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:12px;color:#656565;background: #fff;-webkit-border-radius: 8px;-moz-border-radius: 8px;border-radius: 8px;-webkit-box-shadow: 0px -1px 5px #DDD;-moz-box-shadow: 0px -1px 3px #DDD;box-shadow: 0px -1px 5px #DDD;width: 168px;border: 1px solid #e2e2e2;">
            <tbody>
                <tr>
                    <td style="border-radius: 8px 8px 0 0; position: relative; text-align:center; background: #8972f0; padding: 10px; border-bottom: 2px solid #f3f3f3;">
                        <a href="<?php echo '#'; ?>" target="_blank">                            
                            <img src="{{ URL::to('resources/assets/images/logo-sixteen-white.png') }}" alt="Six-clouds" width="100" border="0" >
                        </a>
                    </td>
                </tr>
                <tr>
                    <td style="padding:10px; ">                        
                        <table width="576" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#656565">
                            <tbody>
                                <tr>
                                    <td style="padding:0 10px 20px 10px;">
                                        <table width="554" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        @if($project_creator['current_language']==1)
                                                        <div style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size: 14px; color: #6C6C6C; margin-bottom: 5px;">
                                                            Hello {{$project_creator['first_name']}} {{$project_creator['last_name']}},
                                                            <br>
                                                            <br>
                                                            Thank you for posting a Project.
                                                            <br>
                                                            <br>
                                                            Your selected Seller is unable to take on your Project.
                                                            <br>
                                                            <br>
                                                            Please click <a href="{{ URL::to('/suggested-sellers/'.$slug) }}">here</a>  to select another Seller.
                                                            <br>
                                                        </div>
                                                        @else
                                                        <div style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size: 14px; color: #6C6C6C; margin-bottom: 5px;">
                                                            你好 {{$project_creator['first_name']}} {{$project_creator['last_name']}},
                                                            <br>
                                                            <br>
                                                            感谢您发布的项目。
                                                            <br>
                                                            <br>
                                                            您选择的卖家无法执行您的项目。
                                                            <br>
                                                            <br>
                                                            请点击 <a href="{{ URL::to('/suggested-sellers/'.$slug) }}">此处</a>选择另一个卖家。
                                                            <br>
                                                        </div>
                                                        @endif
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>        
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table width="576" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#6c6c6c">
                            <tbody>
                                <tr>
                                    <td style="padding:0 10px 20px 10px;">
                                        <table width="554" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size: 14px; color: #6C6C6C;">
                                                            <?php //echo $html; ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>        
                                    </td>
                                </tr>
                                <tr>
                                    @if($project_creator['current_language']==1)
                                    <td style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:14px;padding:10px;border-top: 1px solid #e6e6e6;    font-weight: 600;">
                                        From,<br /> SixClouds Sixteen
                                    </td>
                                    @else
                                    <td style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:14px;padding:10px;border-top: 1px solid #e6e6e6;    font-weight: 600;">
                                        六云十六
                                    </td>
                                    @endif
                                </tr>
                            </tbody>
                        </table>                        
                    </td>
                </tr>
                <tr>
                    @if($project_creator['current_language']==1)
                    <td style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size:11px; line-height:16px; padding:15px 18px; text-align:center; border-radius: 0 0 8px 8px; background-color: #8972f0; border-top: 3px solid #d7a343; color: #fff;">
                        <?php echo date('Y'); ?>&copy; SixClouds            
                    </td>
                    @else
                    <td style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size:11px; line-height:16px; padding:15px 18px; text-align:center; border-radius: 0 0 8px 8px; background-color: #8972f0; border-top: 3px solid #d7a343; color: #fff;">
                        <?php echo date('Y'); ?>&copy; 六云            
                    </td>
                    @endif
                </tr>
            </tbody>
        </table>
    </body>
</html>
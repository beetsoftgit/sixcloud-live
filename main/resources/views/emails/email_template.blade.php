<html style="height: 100%; margin: 0;">
    <body>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,300' rel='stylesheet' type='text/css'>
        <table width="614" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:12px;color:#656565;background: #FDFDFD;border: 1px solid #D6D5D5;-webkit-border-radius: 8px;-moz-border-radius: 8px;border-radius: 8px;-webkit-box-shadow: 0px -1px 5px #DDD;-moz-box-shadow: 0px -1px 3px #DDD;box-shadow: 0px -1px 5px #DDD;width: 168px;">
            <tbody style="border-color: #D3D3D3">
                <tr>
                    <td style="border-radius: 8px 8px 0 0; position: relative; text-align:center; background: #fff; padding: 10px">
                        <a href="<?php echo '#'; ?>" target="_blank"> 
                        @if(isset($is_ignite) && ($is_ignite == 1))
                            <img src="https://sixclouds.net/main/resources/assets/images/Corporate_Color.png"  alt="sixclouds" width="150" border="0" style="padding:0px;max-width: 200px">
                        @else
                            <img src="https://sixclouds.net/main/resources/assets/images/Corporate_Color.png"  alt="sixclouds" width="150" border="0" style="padding:0px;max-width: 200px">
                        @endif
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                    @if(isset($showImage) && $showImage==1)
                    <!-- <img src="{{ URL::to('resources/assets/images/new_email_image.png') }}"  alt="sixclouds" border="0" width="100%" style="padding:0px;height: 100px;">
                    <br>
                    <center>
                    <img src="{{ URL::to('resources/assets/images/sc108x108.png') }}"  alt="sixclouds" width="100%" border="0" style="padding:0px;width: 125px;height: 110px;">
                    </center> -->
                    @endif
                </td>
                </tr>
                @if(isset($showImageAfterVerification) && $showImageAfterVerification==1)
                <center>
                <img src="{{ URL::to('resources/assets/images/Lightbulb.png') }}"  alt="sixclouds" border="0" style="padding:0px;
                    height: 10%;" width="100px;" height="100px;">
                </center>
                @endif
                <tr>
                    <td style="padding:10px; background: #fff;">						
                        
                        <table width="576" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#656565">
                            <tbody>
                                <tr>
                                    <td style="padding:0 10px 20px 10px;">
                                        <table width="554" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size: 14px; color: #6C6C6C; margin-bottom: 5px;">
                                                            {!!$content!!}
                                                            <br>
                                                            <br>
                                                            <br>
                                                        </div>
                                                        
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>        
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table width="576" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#6c6c6c">
                            <tbody>
                                <tr>
                                    <td style="padding:0 10px 20px 10px;">
                                        <table width="554" border="0" align="center" cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size: 14px; color: #6C6C6C;">
                                                            <?php //echo $html; ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>        
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:14px;padding:10px;">
                                        {!!$footer_content!!}
                                    </td>
                                </tr>
                            </tbody>
                        </table>						
                    </td>
                </tr>
                <tr>
                    <td style="font-family:'Open Sans',Arial,Helvetica,sans-serif; font-size:11px; line-height:16px; padding:15px 18px; text-align:center; background-color: #fff; border-top: 3px solid #6c6c6c; color: #009eda;">
                        <?php echo date('Y'); ?>&copy; {{$footer}}
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>
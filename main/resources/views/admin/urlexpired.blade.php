<!DOCTYPE html>
<head>
    <title>Sixclouds</title>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    {{ HTML::style('resources/assets/plugins/bootstrap/css/bootstrap.min.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/plugins/font-awesome/css/font-awesome.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/admin/default.min.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/admin/style.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/common/sweet-alert.min.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/admin/custom.css',array('rel'=>'stylesheet')) }}
    {{ HTML::script('resources/assets/js/common/jquery-3.2.1.min.js') }}
    <link rel="shortcut icon" href="resources/assets/images/Logo-large.png"/>
    <base href="<?= url('/') . '/' ?>">
</head>
<body>
    <div class="login-bg">
        <div class="login">
            <div class="logo text-center margin-bt20 margin-tp30">
                <img src="resources/assets/images/Logo-large.png">
            </div>
            <h3 class="text-black ">Reset Password</h3>
            <div class="form">
                <h4>Link expired.</h4>
            </div>
        </div>
    </div>
    <div id="forgot-pwd" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">Ã—</button>
                    <h4 class="modal-title">Forgot Password</h4>
                </div>
                <div class="modal-body form">
                    <form class="text-center">
                        <div class="form-group input-label">
                            <input type="" name="" class="form-control" placeholder="Email Address">
                        </div>
                        <button type="button" class="btn" value="Submit">
                            Submit
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{ HTML::script('resources/assets/js/admin/core/bootstrap.min.js') }}
    {{ HTML::script('resources/assets/js/common/custom_alert.js') }}
    {{ HTML::script('resources/assets/js/common/jquery.validate.min.js') }}
    {{ HTML::script('resources/assets/js/common/sweetalert.min.js') }}
    {{ HTML::script('resources/assets/js/admin/basic.js') }}
    <script type="text/javascript">
        $(document).ready(function () {
            $('.login-bg').css({'height': (($(window).height())) + 'px'});
        });
    </script>
</body>
</html>
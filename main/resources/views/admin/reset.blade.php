<!DOCTYPE html>
<head>
    <title>Sixclouds</title>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    {{ HTML::style('resources/assets/plugins/bootstrap/css/bootstrap.min.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/plugins/font-awesome/css/font-awesome.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/admin/default.min.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/admin/style.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/common/sweet-alert.min.css',array('rel'=>'stylesheet')) }}
    {{ HTML::style('resources/assets/css/admin/custom.css',array('rel'=>'stylesheet')) }}
    {{ HTML::script('resources/assets/js/common/jquery-3.2.1.min.js') }}
    <link rel="shortcut icon" href="resources/assets/images/Logo-large.png"/>
    <base href="<?= url('/') . '/' ?>">
</head>
<body>
    <div class="login-bg">
        <div class="login">
            <div class="logo text-center margin-bt20 margin-tp30">
                <img src="resources/assets/images/Logo-large.png">
            </div>
            <h3 class="text-black ">Reset Password</h3>
            <div class="form">
                <form method="post" name="form_reset_pwd" id="form_reset_pwd">
                    <div class="form-group input-label">
                        <input class="hide" type="text" name="email" value="{{$email}}">
                        <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                    </div>
                    <div class="form-group input-label margin-bt10">
                        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Confirm Password">
                    </div>
                    <div class="text-center margin-tp15">
                        <button type="submit" class="btn">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{ HTML::script('resources/assets/js/admin/core/bootstrap.min.js') }}
    {{ HTML::script('resources/assets/js/common/custom_alert.js') }}
    {{ HTML::script('resources/assets/js/common/jquery.validate.min.js') }}
    {{ HTML::script('resources/assets/js/common/sweetalert.min.js') }}
    {{ HTML::script('resources/assets/js/admin/basic.js') }}
    <script>
        // set MODE for enable and disable console.log
            var project_mode = 'development'; //'production';
        var BASEURL = "<?= url('/') . '/' ?>";
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.login-bg').css({'height': (($(window).height())) + 'px'});
        });
    </script>
</body>
</html>
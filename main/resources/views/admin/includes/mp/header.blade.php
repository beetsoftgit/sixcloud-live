 <a class="mob-menu-sidebar">
    <span></span>
      <span></span>
        <span></span>
  </a>
<!--Header section Started-->
  <aside class="left-sidebar-menu">
    <a class="close-sidebar-menu visible-xs">
      <i class="fa fa-times-circle" aria-hidden="true"></i>
    </a>
    <div class="logo">
      <a ng-if="module =='mp'" href="#"><img alt="" src="resources/assets/images/logo.png" width="173" height="70"></a>
      <a ng-if="module =='ignite'" href="#"><img alt="" src="resources/assets/images/logo_ignite.png" width="173" height="70"></a>
      <a ng-if="module =='pr'" href="#"><img alt="" src="resources/assets/images/logo_pr.svg" width="173" height="70"></a>
    </div>     
    <ul class="menu-list admin-header" ng-if="module =='mp'">
      <li id="dashboard_mp"><a href="manage/sixteen/dashboard">Dashboard</a></li>
      <li id="all_jobs"><a href="manage/sixteen/all-projects">All Projects</a></li>
      <li id="designers"><a href="manage/sixteen/sixteen-sellers">Sellers</a></li>
      <li id="buyers"><a href="manage/sixteen/sixteen-buyers">Buyers</a></li>
      <!-- <li id="stats"><a href="javascript:;">Statistics</a></li>
      <li id="inspiration_board"><a href="javascript:;">Inspiration Board</a></li> -->
      <!-- <li><a href="javascript:;">Gallery</a></li> -->
      <li id="categories_menu"><a href="manage/sixteen/categories">Categories</a></li>
      <li id="customer_li_sixteen"><a href="manage/sixteen/tickets/2">Customer Support</a></li>
      <li id="countries"><a href="manage/sixteen/countries">Countries</a></li>
      <li id="transactions"><a href="manage/sixteen/transactions">Transactions</a></li>
    </ul>   
    <ul class="menu-list admin-header ignite-header" ng-if="module =='ignite'">
      <li id="dashboard"><a href="manage/ignite/dashboard">Dashboard</a></li>
      <li id="all_content"><a href="manage/ignite/all-content">All Content</a></li>
      <li id="zone_manger"><a href="manage/ignite/zone-manager">Zone Manager</a></li>
      <li id="video_category_manger"><a href="manage/ignite/category-manager">Category Manager</a></li>
      <li id="video_manger"><a href="manage/ignite/video-manager">Video Manager</a></li>
      <li id="quiz_manger"><a href="manage/ignite/quiz-manager">Quiz Manager</a></li>
      <li id="worksheet_manger"><a href="manage/ignite/worksheet-manager">WorkSheet Manager</a></li>
      <li id="ignite_distributors"><a href="manage/ignite/distributors">Distributors</a></li>
      <li id="ignite_subscribers"><a href="manage/ignite/subscribers">Subscribers</a></li>
      <li id="ignite_price_plan"><a href="manage/ignite/pricing-plans">Pricing Plans</a></li>
      <li id="ignite_promo_code"><a href="manage/ignite/promo-codes">Promo Codes</a></li>
      <li id="ignite_transactions"><a href="manage/ignite/ignite-transactions">Transactions</a></li>
      <li id="ignite_taxrate"><a href="manage/ignite/ignite-taxrate">Tax Rate</a></li>
      <li id="customer_li_ignite"><a href="manage/ignite/tickets/1">Customer Support</a></li>
      <li><a href="javascript:;">Usage Statistics</a></li>
    </ul>
    <ul class="menu-list admin-header pr-header" ng-if="module =='pr'">
      <li id="open_case"><a href="manage/pr/open-cases">Open Cases</a></li>
      <li id="special_case"><a href="manage/pr/special-cases">Special Cases</a></li>
      <li id="closed_case"><a href="manage/pr/closed-cases">Closed Cases</a></li>
      <li id="all_accesors"><a href="manage/pr/all-accessor">All Accessors</a></li> 
      <li id="customer_li_pr"><a href="manage/sixteen/tickets/3">Customer Support</a></li>
    </ul> 
  </aside>
  <header>
    <div class="container">
      <div class="row">
        <div class="display-table">
          <div class="col-sm-9 table-cell">
            <nav class="main-menu">
              <div class="navbar navbar-default" role="navigation">
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle my-toggle-menu" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                      <a class="navbar-brand" href="#"><img alt="" src="resources/assets/images/logo-w.png" width="173" height="70"></a>
                    </div>
                    <div class="navbar-collapse collapse">
                      <ul class="nav navbar-nav">
                        <li class="active"><a href="#">Home</a></li>
                        <li id="adminHeaderMP"><a href="manage/sixteen/all-projects">Sixteen</a></li>
                        <li id="adminHeaderIgnite"><a href="manage/ignite/all-content">Ignite</a></li>
                        <li id="adminHeaderPr"><a href="manage/pr/open-cases">Proof Reading</a></li>
                        <li><a href="#">Discover</a></li>
                      </ul>
                      </div><!--/.nav-collapse -->
                    </div>
            </nav>

          </div>
          <div class="col-sm-3 table-cell">
             <div class="dropdown head-right-dropdown admin-header-menu">
                <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <table  cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td width="50"><img alt="" ng-src="[[ADMINDATA.image]]" width="43" height="43"></td>
                      <td>[[ADMINDATA.first_name]] [[ADMINDATA.last_name]]<span class="caret"></span></td>
                    </tr>
                  </table>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dLabel">
                <li class="temp-show-header" ng-click="changePassword()"><a href="#"> Change Password</a></li>  
                <li class="temp-show-header" ng-click="logout()"><a href="#"> Log Out</a></li>
                </ul>
              </div>
              <div class="clearfix"></div>
          </div>
        </div>

      </div>
    </div>
  </header> 
  <!--Header section End-->
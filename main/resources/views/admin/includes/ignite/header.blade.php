 <a class="mob-menu-sidebar">
    <span></span>
      <span></span>
        <span></span>
  </a>
<!--Header section Started-->
  <aside class="left-sidebar-menu">
    <a class="close-sidebar-menu visible-xs">
      <i class="fa fa-times-circle" aria-hidden="true"></i>
    </a>
    <div class="logo">
      <a href="#"><img alt="" src="resources/assets/images/logo.png" width="173" height="70"></a>
    </div>
    <ul class="menu-list admin-header">
      <li id="dashboard" class="active"><a href="manage/ignite/dashboard">Dashboard</a></li>
      <li><a href="javascript:;">All Content</a></li>
      <li><a href="javascript:;">Quizzes</a></li>
      <li><a href="javascript:;">Distributors</a></li>
      <li><a href="javascript:;">Subscribers & Plans</a></li>
      <li><a href="javascript:;">Transactions</a></li>
      <li><a href="javascript:;">Usage Statistics</a></li>
    </ul>
  </aside>
  <header>
    <div class="container">
      <div class="row">
        <div class="display-table">
          <div class="col-sm-9 table-cell">
            <nav class="main-menu">
              <div class="navbar navbar-default" role="navigation">
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle my-toggle-menu" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                      <a class="navbar-brand" href="#"><img alt="" src="resources/assets/images/logo-w.png" width="173" height="70"></a>
                    </div>
                    <div class="navbar-collapse collapse">
                      <ul class="nav navbar-nav">
                        <li><a href="#">Home</a></li>
                        <li id="adminHeaderMP"><a href="manage/sixteen/all-projects">Sixteen</a></li>
                        <li id="adminHeaderIgnite" class="active" ><a href="manage/ignite/all-content">Ignite</a></li>
                        <li><a href="#">Proof Reading</a></li>
                        <li><a href="#">Discover</a></li>
                      </ul>
                      </div><!--/.nav-collapse -->
                    </div>
            </nav>

          </div>
          <div class="col-sm-3 table-cell">
             <div class="dropdown head-right-dropdown admin-header-menu">
                <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <table  cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                      <td width="50"><img alt="" ng-src="[[ADMINDATA.image]]" width="43" height="43"></td>
                      <td>[[ADMINDATA.first_name]] [[ADMINDATA.last_name]]<span class="caret"></span></td>
                    </tr>
                  </table>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dLabel">
                <li class="temp-show-header" ng-click="logout()"><a href="#"> Log Out</a></li>
                </ul>
              </div>
              <div class="clearfix"></div>
          </div>
        </div>

      </div>
    </div>
  </header>

  <!--Header section End-->

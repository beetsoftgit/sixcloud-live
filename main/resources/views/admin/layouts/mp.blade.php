<!DOCTYPE html>
<html ng-app="sixcloudAdminApp">
    <head>
        <title>
            [[pagetitle]]
        </title>
        <meta charset="utf-8"/>
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
            <meta content="width=device-width, initial-scale=1" name="viewport"/>
            {{ HTML::style('resources/assets/css/admin/mp/animations.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/css/admin/mp/swiper.min.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/css/admin/mp/custom-font.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/css/admin/mp/font-awesome.min.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/css/admin/mp/icon-moon.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/css/admin/mp/jquery.FlowupLabels.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/css/admin/mp/bootstrap-datetimepicker.min.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/css/admin/mp/asRange.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/css/admin/mp/drop-down.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/css/common/sweet-alert.min.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/css/common/bootstrap-multiselect.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/css/common/jasny-bootstrap.min.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/css/common/select2.min.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/css/admin/mp/star-rating.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/css/admin/mp/bootstrap.css',array('rel'=>'stylesheet')) }}

        <link rel="stylesheet" media="all" type="text/css" href="{{ url('/')}}/resources/assets/css/admin/mp/style.css" ng-if="module =='mp'">
        <link rel="stylesheet" media="all" type="text/css" href="{{ url('/')}}/resources/assets/css/admin/ignite/style.css" ng-if="module =='ignite'">
        <link rel="stylesheet" media="all" type="text/css" href="{{ url('/')}}/resources/assets/css/admin/pr/style.css" ng-if="module =='pr'">
        <link rel="stylesheet" media="all" type="text/css" href="{{ url('/')}}/resources/assets/css/admin/pr/icomoon.css" ng-if="module =='pr'">
        <link rel="stylesheet" media="all" type="text/css" href="{{ url('/')}}/resources/assets/css/admin/pr/admin_pr_custom.css" ng-if="module =='pr'">

        {{ HTML::style('resources/assets/css/admin/mp/responsive.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/css/admin/mp/mp_custom.css',array('rel'=>'stylesheet')) }}        
         
         

        {{ HTML::script('resources/assets/js/common/jquery-3.2.1.min.js') }}
        {{ HTML::script('resources/assets/js/common/angular.min.js') }}
        {{ HTML::script('resources/assets/js/common/angular-route.js') }}
        {{ HTML::script('resources/assets/js/common/angular-sanitize.min.js') }}
        {{ HTML::script('resources/assets/js/common/angular-page-loader.min.js') }}
        {{ HTML::script('resources/assets/js/common/ui-bootstrap-tpls-0.12.1.js') }}
        {{ HTML::script('resources/assets/js/admin/hls.js') }}

            <link href="resources/assets/images/favicon.ico" rel="shortcut icon"/>
            <base href="<?=url('/') . '/'?>">
                <style>
                    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
                display: none !important;
            }
                </style>
            </base>
        </meta>
    </head>
    <body ng-controller="MPMainController">
        <div class="body-div body-with-sidebar">
            @include('admin/includes/mp.header')
            <page-loader>
            </page-loader>
            <div id="main">
                <div ng-view="">
                </div>
            </div>
        </div>
    </body>
    <script>
        // set MODE for enable and disable console.log
    var project_mode = 'development'; //'production';
    var BASEURL = "<?=url('/') . '/'?>";
    var ADMIN_DEFAULT_URL = "<?=url('/') . config('constants.path.ADMIN_DEFAULT_IMAGE')?>";
    var MAX_SIZE = "<?=config('constants.messages.MAX_UPLOAD_FILE_SIZE')?>";
    var current_domain = "<?=config('constants.messages.CURRENT_DOMAIN')?>";
    </script>

<!-- Core files -->
{{ HTML::script('resources/assets/js/admin/customangular.js') }}
{{ HTML::script('resources/assets/js/common/init.js') }}
{{ HTML::script('resources/assets/js/common/ng-img-crop.js') }}
{{ HTML::script('resources/assets/js/common/classes/crop-area-circle.js') }}
{{ HTML::script('resources/assets/js/common/classes/crop-area-square.js') }}
{{ HTML::script('resources/assets/js/common/classes/crop-area.js') }}
{{ HTML::script('resources/assets/js/common/classes/crop-canvas.js') }}
{{ HTML::script('resources/assets/js/common/classes/crop-exif.js') }}
{{ HTML::script('resources/assets/js/common/classes/crop-host.js') }}
{{ HTML::script('resources/assets/js/common/classes/crop-pubsub.js') }}
{{ HTML::script('resources/assets/js/common/underscore.js') }}
{{ HTML::script('resources/assets/js/common/sweetalert.min.js') }}
{{ HTML::script('resources/assets/js/common/custom_alert.js') }}
{{ HTML::script('resources/assets/js/common/jquery.validate.min.js') }}
{{ HTML::script('resources/assets/js/common/draggable.js') }}
{{ HTML::script('resources/assets/js/common/bootstrap-multiselect.js') }}
{{ HTML::script('resources/assets/js/common/jasny-bootstrap.min.js') }}

{{ HTML::script('resources/assets/js/common/js.cookie.min.js') }}
{{ HTML::script('resources/assets/js/common/lang-label.js') }}
{{ HTML::script('resources/assets/js/common/angular-translate.min.js') }}
{{--  {{ HTML::script('resources/assets/js/common/ckeditor/ckeditor.js') }}
{{ HTML::script('resources/assets/js/common/ckeditor/ckeditorconfig.js') }}  --}}
{{ HTML::script('resources/assets/js/front/mp/ckeditor.js') }}
{{ HTML::script('resources/assets/js/front/mp/ckeditorconfig.js') }}

{{ HTML::script('resources/assets/js/common/file-progress/vendors/jquery.ui.widget.js') }}
{{ HTML::script('resources/assets/js/common/file-progress/load-image.all.min.js') }}
{{ HTML::script('resources/assets/js/common/file-progress/canvas-to-blob.min.js') }}
{{ HTML::script('resources/assets/js/common/file-progress/jquery.iframe-transport.js') }}
{{ HTML::script('resources/assets/js/common/file-progress/jquery.fileupload.js') }}
{{ HTML::script('resources/assets/js/common/file-progress/jquery.fileupload-process.js') }}
{{ HTML::script('resources/assets/js/common/file-progress/jquery.fileupload-image.js') }}
{{ HTML::script('resources/assets/js/common/file-progress/jquery.fileupload-video.js') }}
{{ HTML::script('resources/assets/js/common/file-progress/jquery.fileupload-audio.js') }}
{{ HTML::script('resources/assets/js/common/file-progress/jquery.fileupload-validate.js') }}


{{ HTML::script('resources/assets/js/common/uploadfile.min.js') }}
{{ HTML::script('resources/assets/js/common/select2.min.js') }}

{{ HTML::script('resources/assets/js/admin/ng-csv.js') }}
{{ HTML::script('resources/assets/js/admin/angular-sanitize.min.js') }}

{{ HTML::script('resources/assets/js/admin/mp/popper.min.js') }}
{{ HTML::script('resources/assets/js/admin/mp/bootstrap.min.js') }}
{{ HTML::script('resources/assets/js/admin/mp/moment.js') }}
{{ HTML::script('resources/assets/js/admin/mp/bootstrap-datetimepicker.js') }}
{{ HTML::script('resources/assets/js/admin/mp/swiper.min.js') }}
{{ HTML::script('resources/assets/js/admin/mp/css3-animate-it.js') }}
{{ HTML::script('resources/assets/js/admin/mp/jquery.FlowupLabels.js') }}
{{ HTML::script('resources/assets/js/admin/mp/star-rating.js') }}

{{ HTML::script('resources/assets/js/admin/mp/jquery-asRange.js') }}
{{ HTML::script('resources/assets/js/admin/mp/jquery-ui.js') }}
{{ HTML::script('resources/assets/js/admin/mp/script.js') }}



{{ HTML::script('resources/assets/js/admin/controllers/mp/MPMainController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/mp/MPDashboardController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/mp/MPBuyersController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/mp/MPDesignersController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/mp/MPAllJobsController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/mp/MPCategoriesController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/mp/MPProjectDetailsController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/mp/MPTicketListingController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/mp/MPTicketDetailController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/mp/MPCountriesListingController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/mp/MpDesignerInfoController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/mp/MpBuyerInfoController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/mp/MPTransactionListingController.js') }}


<!-- ignite : start -->
{{ HTML::script('resources/assets/js/admin/controllers/ignite/IgniteController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/ignite/AllContentController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/ignite/QuizManagerController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/ignite/VideoManagerController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/ignite/WorkSheetManagerController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/ignite/EditWorkSheetController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/ignite/NewContentController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/ignite/CreateNewQuizController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/ignite/CreateNewVideoController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/ignite/IgniteDistributorController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/ignite/IgniteDistributorDetailsController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/ignite/IgniteSubscriberController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/ignite/IgniteSubscriberDetailsController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/ignite/IgniteAddDistributorController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/ignite/IgniteAddSubscriberController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/ignite/IgniteEditDistributorController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/ignite/QuizInfoController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/ignite/VideoDetailsController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/ignite/EditVideoController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/ignite/EditQuizManagerController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/ignite/CreateNewWorkSheetController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/ignite/WorkSheetInfoController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/ignite/IgnitePricingPlansController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/ignite/IgnitePromoCodesController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/ignite/IgnitePromoCodesDetailsController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/ignite/IgniteAddPromoCodesController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/ignite/IgniteEditPromoCodesController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/ignite/IgniteDistributorCommissionsController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/ignite/IgniteTransactionsListingController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/ignite/ZoneManagerController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/ignite/CategoryManagerController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/ignite/IgniteChangePasswordController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/ignite/IgniteTaxRateController.js') }}
<!-- ignite : end -->
<!-- pr : start -->
{{ HTML::script('resources/assets/js/admin/controllers/pr/PrOpenCaseController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/pr/PrSpecialCaseController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/pr/PrClosedCaseController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/pr/PrAllAccessorController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/pr/PrAccountInfoController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/pr/PrAccessorMyAccountController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/pr/PrAccessorMyJobsController.js') }}
<!-- pr : end -->

{{ HTML::script('resources/assets/js/admin/factory/adminmpfactory.js') }}
{{ HTML::script('resources/assets/js/admin/factory/adminignitefactory.js') }}
{{ HTML::script('resources/assets/js/admin/factory/adminfactory.js') }}
{{ HTML::script('resources/assets/js/admin/factory/prfactory.js') }}
</html>
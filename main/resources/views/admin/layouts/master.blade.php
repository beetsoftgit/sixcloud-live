<!DOCTYPE html>
<html ng-app="sixcloudAdminApp">
    <head>
        <title>[[pagetitle]]</title>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        {{ HTML::style('resources/assets/plugins/bootstrap/css/bootstrap.min.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/plugins/font-awesome/css/font-awesome.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/css/admin/default.min.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/css/admin/style.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/plugins/summernote/summernote.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/plugins/highchart/highcharts.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/plugins/owl-craousel/owl.carousel.min.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/plugins/boostrap-select/css/bootstrap-select.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/plugins/boostrap-datepicker/css/daterangepicker.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/css/common/sweet-alert.min.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/plugins/boostrap-select/css/jasny-bootstrap.min.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/css/common/uploadfile.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/css/common/ng-img-crop.scss',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/css/admin/six-clouds.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/plugins/fullcalendar/fullcalendar.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/plugins/highchart/highcharts.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/css/common/angular-page-loader.css',array('rel'=>'stylesheet')) }}
        {{ HTML::style('resources/assets/css/admin/custom.css',array('rel'=>'stylesheet')) }}
        {{ HTML::script('resources/assets/js/common/jquery-3.2.1.min.js') }}
        {{ HTML::script('resources/assets/js/common/angular.min.js') }}
        {{ HTML::script('resources/assets/js/common/angular-route.js') }}
        {{ HTML::script('resources/assets/js/common/angular-sanitize.min.js') }}
        {{ HTML::script('resources/assets/js/common/angular-page-loader.min.js') }}
        {{ HTML::script('resources/assets/js/common/ui-bootstrap-tpls-0.12.1.js') }}
        <!-- <script data-require="ui-bootstrap@*" data-semver="0.12.1" src="http://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script> -->
        <link rel="shortcut icon" href="resources/assets/images/logo-large.png"/>
        <base href="<?=url('/') . '/'?>">
        <style>
            [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
                display: none !important;
            }
        </style>
    </head>
    <body ng-controller="MainController">
        @include('admin/includes.header')
    <page-loader></page-loader>
    <div id="main">
        <div ng-view></div>
    </div>
</body>
<script>
    // set MODE for enable and disable console.log
    var project_mode = 'development'; //'production';
    var BASEURL = "<?=url('/') . '/'?>";
    var ADMIN_DEFAULT_URL = "<?=url('/') . config('constants.path.ADMIN_DEFAULT_IMAGE')?>";
</script>
<!-- Core files -->
{{ HTML::script('resources/assets/js/admin/customangular.js') }}
{{ HTML::script('resources/assets/js/common/init.js') }}
{{ HTML::script('resources/assets/js/common/ng-img-crop.js') }}
{{ HTML::script('resources/assets/js/common/classes/crop-area-circle.js') }}
{{ HTML::script('resources/assets/js/common/classes/crop-area-square.js') }}
{{ HTML::script('resources/assets/js/common/classes/crop-area.js') }}
{{ HTML::script('resources/assets/js/common/classes/crop-canvas.js') }}
{{ HTML::script('resources/assets/js/common/classes/crop-exif.js') }}
{{ HTML::script('resources/assets/js/common/classes/crop-host.js') }}
{{ HTML::script('resources/assets/js/common/classes/crop-pubsub.js') }}

{{ HTML::script('resources/assets/js/admin/core/bootstrap.min.js') }}
{{ HTML::script('resources/assets/js/admin/core/wow.min.js') }}
{{ HTML::script('resources/assets/js/common/custom_alert.js') }}
{{ HTML::script('resources/assets/js/common/jquery.validate.min.js') }}
{{ HTML::script('resources/assets/js/common/sweetalert.min.js') }}
{{ HTML::script('resources/assets/js/common/js.cookie.min.js') }}
{{ HTML::script('resources/assets/js/common/lang-label.js') }}
{{ HTML::script('resources/assets/js/common/angular-translate.min.js') }}

{{ HTML::script('resources/assets/plugins/boostrap-select/js/jasny-bootstrap.min.js') }}
{{ HTML::script('resources/assets/js/common/uploadfile.min.js') }}
{{ HTML::script('resources/assets/js/admin/factory/eltfactory.js') }}
{{ HTML::script('resources/assets/js/admin/factory/subscriptionfactory.js') }}
{{ HTML::script('resources/assets/js/admin/factory/adminfactory.js') }}
<!-- Plugins files -->
{{ HTML::script('resources/assets/plugins/owl-craousel/owl.carousel.min.js') }}
{{ HTML::script('resources/assets/plugins/boostrap-datepicker/js/moment.min.js') }}
{{ HTML::script('resources/assets/plugins/boostrap-datepicker/js/daterangepicker.js') }}
{{ HTML::script('resources/assets/plugins/highchart/highcharts.js') }}
<!-- Controller files -->
{{ HTML::script('resources/assets/js/admin/controllers/HomeController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/MainController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/TransactionController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/SubscriptionController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/PrOpenCaseController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/PrSpecialCaseController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/PrClosedCaseController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/PrAllAccessorController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/PrAccountInfoController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/PrAccessorMyAccountController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/PrAccessorMyJobsController.js') }}
<!-- factory file -->
{{ HTML::script('resources/assets/js/admin/factory/userfactory.js') }}
{{ HTML::script('resources/assets/js/admin/factory/prfactory.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/MyAccountController.js') }}
{{ HTML::script('resources/assets/js/admin/controllers/AddAdminController.js') }}
<!--    <script type="text/javascript" src="assets/plugins/highchart/highcharts.js">
</script>-->
</html>
/*
 Author  : Komal Kapadi (komal@creolestudios.com)
 Date    : 19th December 2017
 Name    : MainController
 Purpose : All the functions for rootscope
 */
angular.module('sixcloudAdminApp').controller('MainController', ['adminfactory', '$http', '$scope', '$timeout', '$rootScope', '$filter', function (adminfactory, $http, $scope, $timeout, $rootScope, $filter) {
        console.log('main controller loaded');
        $timeout(function(){
            $rootScope.current_domain  = current_domain;
        },500);
        adminfactory.Userdata().then(function (response) {
                if (response.data.status == 1) {
                    $rootScope.ADMINDATA = response.data.data;
                    $rootScope.header_menu = 'admin';

                }
        });
        adminfactory.Pruserdata().then(function (response) {
                if (response.data.status == 1) {
                    $rootScope.PRADMINDATA = response.data.data;
                    $rootScope.header_menu = 'accessor';
                }
        });
        $rootScope.logout = function () {
            $.ajax({
                url: BASEURL + 'logout',
                type: "get",
                processData: false,
                contentType: false,
                beforeSend: function () {
                },
                success: function (res) {
                    if (res.status == 1)
                    {
                        simpleAlert('success', 'Logout', res.message + ' Redirecting...', false);
                        setTimeout(function () {//redirect to home after 3 seconds
                            location.href = BASEURL + 'manage/login';
                        }, 2500);
                    }
                }
            }, "json");
        }
        $rootScope.prlogout = function () {
            $.ajax({
                url: BASEURL + 'Prlogout',
                type: "get",
                processData: false,
                contentType: false,
                beforeSend: function () {
                },
                success: function (res) {
                    if (res.status == 1)
                    {
                        simpleAlert('success', 'Logout', res.message + ' Redirecting...', false);
                        setTimeout(function () {
                            location.href = BASEURL + 'manage/pr-login';
                        }, 1000);
                    }
                }
            }, "json");
        }

        $rootScope.test = function(){
            alert('m here in main');
        }
    }]);
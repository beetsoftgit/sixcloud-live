/*
 Author  : Komal Kapadi (komal@creolestudios.com)
 Date    : 25th December 2017
 Name    : SubscriptionController
 Purpose : All the functions for subscription
 */
angular.module('sixcloudAdminApp').controller('SubscriptionController', ['subscriptionfactory', 'Getsubscriptions', '$http', '$scope', '$timeout', '$rootScope', '$filter', function(subscriptionfactory, Getsubscriptions, $http, $scope, $timeout, $rootScope, $filter) {
    console.log('Subscription controller loaded');
    $scope.plans = Getsubscriptions.data.data;
    // subscriptionfactory.GetSubscriptionusers({
    //     'offset': 0
    // }).then(function(response) {
    //     $scope.chartData = response.data.data;
    // });
    $('#plans.owl-carousel').owlCarousel({
        loop: true,
        margin: 40,
        responsiveClass: true,
        navigation: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 3,
                nav: false
            },
            1000: {
                items: 3,
                nav: true,
                loop: false
            }
        },
        navText: ["<i class='sci-arrow-left'></i>", "<i class='sci-arrow-right'></i>"]
    });
    setTimeout(function() {
        angular.forEach($scope.plans, function(value, key) {
            console.log(value);
            chart = Highcharts.chart('highchart' + value.id, {
                chart: {
                    type: 'column'
                },
                title: {
                    text: false
                },
                yAxis: {
                    visible: false
                },
                xAxis: {
                    categories: value.chart.months
                },
                plotOptions: {
                    series: {
                        allowPointSelect: true
                    }
                },
                series: [{
                    data: value.chart.value
                }]
            });
        });
        $('.highcharts-credits').remove();
    }, 1000);
}]);
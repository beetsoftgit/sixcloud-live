/*
 Author  : Zalak Kapadia (zalak@creolestudios.com)
 Date    : 25th April 2018
 Name    : MPTicketListingController
 Purpose : To get all tickets data at admin side
 */
app.controller('MPCountriesListingController', ['$scope', 'adminmpfactory','$timeout', function($scope,adminmpfactory, $timeout) {
    console.log('MPCountriesListingController loaded.');
    $timeout(function() {
        $('.admin-header').find('li').removeClass('active');
        $('#countries').addClass('active');
    }, 500);    
    $scope.search='';
    $scope.others = "1";
    $scope.filterData=function(search=''){
        $timeout(function(){
            showLoader('.filter-btn','Please wait...');
        },500);    
        $scope.GetUserCountriesListing({'others':$scope.others,'search':search},1);
        $timeout(function(){
            hideLoader('.filter-btn','Submit');
        },1000);    
    }
    $scope.resetData=function(){
        $timeout(function(){
            showLoader('.reset-btn','Please wait...');
        },500);         
        $scope.search=''; 
        $scope.GetUserCountriesListing({'others':$scope.others,'search':$scope.search},1);   
        $timeout(function(){
            hideLoader('.reset-btn','Reset');
        },1000); 
    }
    
    $scope.GetUserCountriesListing=function(data,pageNumber){        
	    adminmpfactory.GetUserCountriesListing(data,pageNumber).then(function(response){                        
			if(response.data.status == 1){
				$scope.countryListing=response.data.data.data;
				$scope.totalCountries = response.data.data.total;
				$scope.paginate = response.data.data;
            	$scope.currentCountryPaging = response.data.data.current_page;
            	$scope.rangeCountrypage = _.range(1, response.data.data.last_page + 1); 
			}
        });
	}    
    $scope.GetUserCountriesListing({'others':$scope.others,'search':$scope.search},1);    
    //Code done by Ketan Solanki for EXCEL and CSV EXport DATE: 5th July 2018
    $scope.Exportdata = function (Exportvalue) {        
        var search = $scope.search;
        if ($scope.search === "") {
            var search = "no-search";
        }
        if ((Exportvalue) && Exportvalue != '' && Exportvalue != undefined) {
            window.location = BASEURL + 'ExportCountryData/' + Exportvalue + '/' + search;
        } else {
            simpleAlert('error', 'Error', "Please select Export type");
        }
    };
     
   $scope.uploadCSVFileData = function(){
    var fileData = new FormData($("#uploadDataFile")[0]);
    adminmpfactory.uploadCSVFileData(fileData).then(function(response){
        if (response.data.status == 1) {
            simpleAlert('success', 'Data Updated', response.data.message, true);
            $timeout(function(){
                $scope.GetUserCountriesListing({'others':$scope.others,'search':$scope.search},1);    
            },1000); 
        } else {
            simpleAlert('error', 'Error', response.data.message, true);
        }
    });            
   } 
}]);
/*
 Author  : Zalak Kapadia (zalak@creolestudios.com)
 Date    : 25th April 2018
 Name    : MPTicketListingController
 Purpose : To get all tickets data at admin side
 */
app.controller('MPTicketListingController', ['$scope','Gettickettypes','$routeParams', 'adminmpfactory','$timeout', function($scope,Gettickettypes,$routeParams,adminmpfactory, $timeout) {
    console.log('MPTicketListingController loaded.');

    $(".navbar-nav li").removeClass("active");
    $timeout(function () {
        if($scope.ticket_for==1){
            $('#adminHeaderIgnite').addClass("active");
            $(".ignite-header li").removeClass("active");
            $('#customer_li_ignite').addClass("active");
        } else if($scope.ticket_for==2){
            $('#adminHeaderMP').addClass("active");
            $(".admin-header li").removeClass("active");
            $('#customer_li_sixteen').addClass("active");
        } else{
            $('#adminHeaderPr').addClass("active");
            $(".pr-header li").removeClass("active");
            $('#customer_li_pr').addClass("active");
        }
    }, 1000);



    $scope.ticketTypes=Gettickettypes.data.data;
    $scope.tickettype='';
    $scope.ticketcategory='';
    $scope.search='';
    $scope.start_date='';
    $scope.end_date='';
    $scope.ticket_for = $routeParams.for;
    if($scope.ticket_for==1) {
        $scope.btn_color = 'btn-pink';
    } else if($scope.ticket_for==2) {
        $scope.btn_color = 'btn-purple';
    } else {
        $scope.btn_color = 'btn-dark-gray';
    }


    
    $('#datetimepicker_start').datetimepicker({
        pickTime: false,
        format: 'DD-MM-YYYY',
        maxDate: new Date(),
    });
    $('#datetimepicker_end').datetimepicker({
        pickTime: false,
        format: 'DD-MM-YYYY',
    });

    $scope.getTicketCategories=function(tickettype){
    	adminmpfactory.Getticketcategories({'tickettype':tickettype}).then(function(response){
    		if(response.data.status==1){
    			$scope.ticketCategories=response.data.data;
    		}
    	});
    }
    $scope.filterData=function(tickettype='',ticketcategory='',search='',start_date='',end_date=''){
        $timeout(function(){
            showLoader('.filter-btn','Please wait...');
        },500);    
        $scope.Getticketlisting({'type':tickettype,'category':ticketcategory,'search':search,'start_date':start_date,'end_date':end_date,'ticket_for':$scope.ticket_for},1);
        $timeout(function(){
            hideLoader('.filter-btn','Submit');
        },500);    
    }
    $scope.resetData=function(){
        $timeout(function(){
            showLoader('.reset-btn','Please wait...');
        },500); 
        $scope.tickettype='';
        $scope.ticketcategory='';
        $scope.search=''; 
        $scope.Getticketlisting({'type':$scope.tickettype,'category':$scope.ticketcategory,'search':$scope.search,'start_date':$scope.start_date,'end_date':$scope.end_date,'ticket_for':$scope.ticket_for},1);   
        $timeout(function(){
            hideLoader('.reset-btn','Reset');
        },500); 
    }
    $scope.Getticketlisting=function(data,pageNumber){
	    adminmpfactory.Getticketlisting(data,pageNumber).then(function(response){
			if(response.data.status==1){
				$scope.ticketListing=response.data.data.data;
				$scope.totalTickets = response.data.data.total;
				$scope.paginate = response.data.data;
            	$scope.currentTicketPaging = response.data.data.current_page;
            	$scope.rangeTicketpage = _.range(1, response.data.data.last_page + 1); 
			}
		});
	}
	$scope.Getticketlisting({'type':$scope.tickettype,'category':$scope.ticketcategory,'search':$scope.search,'start_date':$scope.start_date,'end_date':$scope.end_date,'ticket_for':$scope.ticket_for},1);
    
    $scope.Exportdata = function (Exportvalue) {
        
        if ((Exportvalue) && Exportvalue != '' && Exportvalue != undefined) {

            var tickettype=$scope.tickettype;
            var ticketcategory=$scope.ticketcategory;
            var search=$scope.search;
            var start_date=$scope.start_date;
            var end_date=$scope.end_date;

            if ($scope.tickettype === "") {
                var tickettype = "no-type";
            }
            if ($scope.ticketcategory === "") {
                var ticketcategory = "no-category";
            }
            if ($scope.search === "") {
                var search = "no-search";
            }
            if ($scope.start_date === "") {
                var start_date = "no-startdate";
            }
            if ($scope.end_date === "") {
                var end_date = "no-enddate";
            }
            //console.log(BASEURL + 'Exportticketdata/' + Exportvalue + '/' + tickettype + '/' + ticketcategory + '/' + search + '/' + start_date + '/' + end_date);
            window.location = BASEURL + 'Exportticketdata/' + Exportvalue + '/' + tickettype + '/' + ticketcategory + '/' + search + '/' + start_date + '/' + end_date;
        } else {
            simpleAlert('error', 'Error', "Please select Export type");
        }
    }; 

    

}]);
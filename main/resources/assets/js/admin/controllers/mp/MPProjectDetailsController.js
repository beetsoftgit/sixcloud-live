angular.module('sixcloudAdminApp').controller('MPProjectDetailsController', [ '$scope','adminmpfactory','$routeParams','$timeout','$sce', function ( $scope,adminmpfactory,$routeParams,$timeout,$sce) {
        console.log('Project Details controller loaded');

        $timeout(function() {
            $('.admin-header').find('li').removeClass('active');
            $('#all_jobs').addClass('active');
        }, 500);
        
        $scope.params = $routeParams;

        $scope.changeTabs=function(evt, jobsStatus) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
                for (i = 0; i < tabcontent.length; i++) {
                    tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(jobsStatus).style.display = "block";
            evt.currentTarget.className += " active";

            $timeout(function(){
                $scope.swiperfunction();
            }, 500);
        }
        $timeout(function() {
            angular.element('#defaultOpen').triggerHandler('click');
            $scope.swiperfunction();
        });
        // Get the element with id="defaultOpen" and click on it
        //document.getElementById("defaultOpen").click();
        
        adminmpfactory.Getprojectdetails({'id':$scope.params.slug}).then(function(response) {
            if(response.data.status==1){   
                $scope.projectDetails=response.data.data;
                if($scope.projectDetails.project_detail_info_for_admin_detail_page.length>0){
                    $scope.overall_rate=$scope.projectDetails.project_detail_info_for_admin_detail_page[0].designer_details_admin.avg_rate;    
                }
                if($scope.projectDetails.no_rate_review==0)
                    $scope.rating_matrix=jQuery.parseJSON($scope.projectDetails.rating_matrix);
               
                $scope.trustedHtml = $sce.trustAsHtml($scope.projectDetails.project_description);
            }
        });

        adminmpfactory.Getprojectpaymentlogs({'id':$scope.params.slug}).then(function(response) {
            if(response.data.status==1){   
                $scope.projectPaymentLogs=response.data.data;
            }
        });
        $scope.messagefunction=function(){
            adminmpfactory.Getmessagedata({'mp_project_id':$scope.params.slug}).then(function(response) {
                if(response.data.status==1){  
                    $scope.messageData=response.data.data.message_board;
                    
                }
            });    
        }
        
        $scope.messagefunction();

        $scope.sendMailBuyerForReviewRate= function(buyer_id){
            adminmpfactory.Sendmailforratereview({'id':buyer_id,'mp_project_id':$scope.params.slug}).then(function(response) {
                if(response.data.status==1){  
                    simpleAlert('success', 'Success', response.data.message);
                    $('.send-mail').hide();
                }
            });            
        }

        $scope.inFavorOfSeller=function(){
            $('#seller_payment').modal('show');
            
        }
        $scope.fundToSeller=function(amount,project_id,seller_id){
            adminmpfactory.Infavorofseller({'amount':amount,'mp_project_id':project_id,'seller_id':seller_id}).then(function(response) {
                if(response.data.status==1){  
                    simpleAlert('success', 'Success', response.data.message);
                    $('#seller_payment').modal('hide');
                    $('#favor_seller').attr("disabled", 'disabled');
                    $('#favor_buyer').attr("disabled", 'disabled');   
                }
                else{
                    simpleAlert('error', 'Error', response.data.message);  
                    $('#seller_payment').modal('hide'); 
                }
            });
        }
        $scope.inFavorOfBuyer=function(seller_id,buyer_id){
            adminmpfactory.Infavorofbuyer({'mp_project_id':$scope.params.slug,'seller_id':seller_id,'buyer_id':buyer_id}).then(function(response) {
                if(response.data.status==1){  

                    $('#favor_seller').attr("disabled", 'disabled');
                    $('#favor_buyer').attr("disabled", 'disabled');   
                    simpleAlert('success', 'Success', response.data.message);
                }
            });
        }
        $scope.cancelProjectByAdmin=function(project_id,designer_id,buyer_id){
            adminmpfactory.Cancelprojectbyadmin({'mp_project_id':project_id,'designer_id':designer_id,'buyer_id':buyer_id}).then(function(response) {
                if(response.data.status==1){  
                    simpleAlert('success', 'Success', response.data.message);
                    $('#cancel_project').attr("disabled", 'disabled');
                }
            });   
        }
        $scope.swiperfunction=function()
        {
            var swiper = new Swiper('.browse-designers-swiper', {
                slidesPerView: 1,
                spaceBetween: 0,
                loop: false,
                 navigation: {
                   nextEl: '.browse-designers-button-next',
                   prevEl: '.browse-designers-button-prev',
                 },
               });
        }
        $timeout(function(){
            $scope.swiperfunction();
        }, 2000);

   var fi = $('#fileupload'); //file input 
    var process_url = 'Uploadmessagefileadmin'; //PHP script
    var progressBar = $('<div/>').addClass('progress').append($('<div/>').addClass('progress-bar')); //progress bar
    uploadButton = $('<a/>').prop('href', 'javascript:;'); //upload button
    uploadButton.prop('id', "testid");
    uploadButton.append('<i class="fa fa-check-circle" aria-hidden="true"></i>');
    $timeout(function() {
        uploadButton.on('click', function(uploadButton) {
            var $this = $(uploadButton),
                data = $this.data();
            data.submit().always(function() {
                $this.parent().find('.progress').show();
                $this.parent().find('.remove').remove();
                $this.remove();
            });
        });
    }, 500);
    //initialize blueimp fileupload plugin
    fi.fileupload({
        url: process_url,
        dataType: 'json',
        autoUpload: false,
        acceptFileTypes: /(\.|\/)(pdf|doc|docx|ppt|psd|svg|ico|eps|icns|ai|png|jpeg|jpg|xlsx|PDF|DOC|DOCX|PPT|PSD|SVG|ICO|EPS|ICNS|AI|PNG|JPEG|JPG|XLSX'|gif)$/i,
        maxFileSize: 30000000, //1MB
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),
        previewMaxWidth: 50,
        previewMaxHeight: 50,
        previewCrop: true,
        dropZone: $('#dropzone')
    });
    $scope.orignionalFileName = [];
    var allowedFormats = ['pdf', 'doc', 'docx', 'ppt', 'psd', 'svg', 'ico', 'eps', 'icns', 'ai', 'png', 'jpeg', 'jpg', 'xlsx', 'PDF', 'DOC', 'DOCX', 'PPT', 'PSD', 'SVG', 'ICO', 'EPS', 'ICNS', 'AI', 'PNG', 'JPEG', 'JPG', 'XLSX'];
    fi.on('fileuploadadd', function(e, data) {
        console.log(data.files[0]);
        var fileName = data.files[0].name;
        var ext = fileName.split('.').pop();
        if (jQuery.inArray(ext, allowedFormats) !== -1) {
            data.context = $('<div/>').addClass('file-progress-bar').appendTo('.outer-progress-bar-div');
            // return false;
            $.each(data.files, function(index, file) {
                $scope.fileNameUploaded = file.name;
                if ($scope.orignionalFileName.indexOf(file.name) !== -1) {
                    simpleAlert('Error', 'Oops', 'File already added', true);
                    data.context.remove();
                    return false;
                } else {
                    var node = $('<div/>').addClass('file-extension');
                    // var h4 = $('<h4/>').addClass('uploaded_file');
                    var node1 = $('<div/>').addClass('H');
                    var removeBtn = $('<a/>').prop('href', 'javascript:;'); //upload button
                    // var removeBtn = $('<div/>').addClass('close-btn'); //
                    // .text('Remove');
                    removeBtn.append('<div class="close-btn"><strong></strong> <a href="javascript:;"><i class="fa fa-times-circle-o" aria-hidden="true"></i></a></div>');
                    // var removeBtn = $('<button/>').addClass('button btn-red remove').text('Remove');
                    removeBtn.on('click', function(e, data) {
                        var file_name = $(this).parent('.H').siblings('.realFileName').text();
                        mpfactory.Unlinkmsgbrdremovedfile({
                            'file_name': file_name
                        }).then(function(response) {
                            if (response.data.status == 1) {
                                var index = $scope.orignionalFileName.indexOf(file_name);
                                var indexfinal = $scope.finalFiles.indexOf(file_name);
                                $scope.orignionalFileName.splice(index, 1);
                                $scope.finalFiles.splice(indexfinal, 1);
                                simpleAlert('success', 'Removed', '');
                            } else {}
                        });
                        $(this).parent().parent().remove();
                    });
                    var file_txt = node.append('<span></span>');
                    // var file_txt = node.append('<span>' + file.name + '</span>');
                    var file_size = $('</div>');
                    // file_size.append('<h4 class="file_name">' + file.name + '<div class="close-btn"><strong></strong> <a href="javascript:;"><i class="fa fa-times-circle-o" aria-hidden="true"></i></a></div>' + '</h4>');
                    // nameformat_size(file.size)
                    node.append(file_txt);
                    // node1.appendTo(node);
                    node1.append(removeBtn);
                    // h4.append(file.name);
                    // h4.append(node1);
                    progressBar.clone().appendTo(data.context);
                    if (!index) {
                        node.prepend(file.preview);
                    }
                    node.appendTo(data.context);
                    data.context.append('<h4 class="uploaded_file">' + file.name + ' <div class="close-btn"></h4><strong class="realFileName hide"></strong><span>' + file.size + '</span></div>');
                    file_txt.prependTo(data.context).append(uploadButton.clone(true).data(data));
                    node1.appendTo(data.context);
                }
            });
        } else {
            simpleAlert("info", "Info", "File not allowed. Allowed file types \n\n (pdf, doc, docx, ppt, psd, svg, ico, eps, icns, ai, png, jpeg, jpg, xlsx)");
            return;
        }
    });
    fi.on('fileuploadprocessalways', function(e, data) {
        var fileName = data.files[0].name;
        var ext = fileName.split('.').pop();
        if (data.files[0].size > MAX_SIZE) {
            simpleAlert('Error', 'File is too big.', '', true);
            $('.file-progress-bar').last().remove();
            return false;
        }
        if (jQuery.inArray(ext, allowedFormats) !== -1) {
            var index = data.index,
                file = data.files[index],
                node = $(data.context.children()[index]);
            if (file.preview) {
                node.prepend(file.preview);
            }
            if (file.error) {
                node.append($('<span class="text-danger"/>').text(file.error));
            }
            if (index + 1 === data.files.length) {
                if (!!data.files.error) data.context.find('button.upload').prop('disabled');
                // data.context.find('button.upload').prop('disabled', !!data.files.error);
            }
            //uploadButton.click(uploadButton);
            /*var $this = $(uploadButton),
            data = $this.data();*/
            if ($scope.orignionalFileName.indexOf($scope.fileNameUploaded) !== -1) {
                return;
            } else {
                data.submit().always(function(response) {
                    $scope.orignionalFileName.push($scope.fileNameUploaded);
                    var getFileName = $(".uploaded_file:contains('" + file.name + "')"); //$('.uploaded_file').hasText();
                    getFileName.siblings('strong').text(response.data.message);
                });
            }
        } else {
            simpleAlert("info", "Info", "File not allowed. Allowed file types \n\n (pdf, doc, docx, ppt, psd, svg, ico, eps, icns, ai, png, jpeg, jpg, xlsx)");
            return;
        }
    });
    fi.on('fileuploadprogress', function(e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        if (data.context) {
            data.context.each(function() {
                $(this).find('.progress').attr('aria-valuenow', progress).children().first().css('width', progress + '%');
            });
        }
    });
    $scope.finalFiles = [];
    fi.on('fileuploaddone', function(e, data) {

        $scope.finalFiles.push(data.result.data);

        $.each(data.result.files, function(index, file) {
            if (file.url) {
                var link = $('<a>').attr('target', '_blank').prop('href', file.url);
                $(data.context.children()[index]).addClass('file-uploaded');
                $(data.context.children()[index]).find('canvas').wrap(link);
                $(data.context.children()[index]).find('.file-remove').hide();
                var done = $('<span class="text-success"/>').text('Uploaded');
                $(data.context.children()[index]).append(done);
            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index]).append(error);
            }
        });
    });
    fi.on('fileuploadfail', function(e, data) {
        $('#error_output').html(data.jqXHR.responseText);
    });

    function format_size(bytes) {
        if (typeof bytes !== 'number') {
            return '';
        }
        if (bytes >= 1000000000) {
            return (bytes / 1000000000).toFixed(2) + ' GB';
        }
        if (bytes >= 1000000) {
            return (bytes / 1000000).toFixed(2) + ' MB';
        }
        return (bytes / 1000).toFixed(2) + ' KB';
    }
    $scope.placeholder_messgae = "Type Here";

        $scope.Sendmessage = function(mp_project_id,seller_email_id,seller_id,buyer_email_id,buyer_id) {
        
        if($scope.send_buyer!=undefined || $scope.send_seller!=undefined){
            
            if($scope.message!=undefined||$scope.finalFiles.length>0){

                showLoader('.send_message','');
                var formData = new FormData($('#message_form')[0]);
                formData.append('files', $scope.finalFiles);
                adminmpfactory.Sendmessage({
                        'mp_project_id': mp_project_id,
                        'message': $scope.message,
                        'files': $scope.finalFiles,
                        'user_role': 3,
                        'seller_email_id': seller_email_id,
                        'seller_id': seller_id,
                        'buyer_email_id': buyer_email_id,
                        'buyer_id': buyer_id,
                        'send_buyer':$scope.send_buyer,
                        'send_seller':$scope.send_seller,
                    }).then(function(response) {
                        hideLoader('.send_message', '<i class="fa fa-paper-plane-o"></i>');
                        if (response.data.status == 1) {
                            $scope.orignionalFileName = [];
                            $scope.messagefunction();
                            $scope.finalFiles = [];
                            $scope.message = undefined;
                        } else {
                            $scope.orignionalFileName = [];
                            $scope.finalFiles = [];
                            simpleAlert('error', '', response.data.message);
                        }
                        $('.outer-progress-bar-div').html('');
                    });
            } else {
                simpleAlert('error','','Type message or select file');
            }
        }else{
            simpleAlert('error','','Select checkbox to send message to seller , buyer or both.');
        }
    }
    

}]);
/*
 Author  : Zalak Kapadia (zalak@creolestudios.com)
 Date    : 23 april 2018
 Name    : MpDashboardController
 Purpose : All the functions for rootscope
 */
angular.module('sixcloudAdminApp').controller('MPMainController', ['$rootScope', '$scope', 'adminfactory', '$timeout', '$interval','$translate', function($rootScope, $scope, adminfactory, $timeout, $interval, $translate) {
    console.log('MPMainController controller loaded');
    Cookies.set('language', 'en');
    $timeout(function(){
        $rootScope.current_domain  = current_domain;
    },500);
    $interval( function(){ 
        adminfactory.Userdata().then(function(response) {
            if (response.data.code == 2) {
                window.location.replace("manage/login");
            }
        });
    }, 3720000);
    $translate.use('en');
    $(".menu-list li").click(function() {
       $(".menu-list li").removeClass("active");
        $(this).addClass("active");
    });
    $(".navbar-nav li").removeClass("active");
    $('#adminHeaderMP').addClass("active");
    $rootScope.current_domain  = current_domain;
    adminfactory.Userdata().then(function(response) {
        if (response.data.status == 1) {
            $rootScope.ADMINDATA = response.data.data;
            $rootScope.header_menu = 'admin';
            
        }
    });
    $rootScope.logout = function() {
        $.ajax({
            url: BASEURL + 'logout',
            type: "get",
            processData: false,
            contentType: false,
            beforeSend: function() {},
            success: function(res) {
                if (res.status == 1) {
                    // simpleAlert('success', 'Logout', res.message + ' Redirecting...', false);
                    // setTimeout(function() { //redirect to home after 3 seconds
                    //     location.href = BASEURL + 'manage/login';
                    // }, 2500);
                    location.href = BASEURL + 'manage/login';
                }
            }
        }, "json");
    }

    $rootScope.changePassword = function() {
        window.location.replace("manage/ignite/change-password");
    }
}]);
/*
 Author  : Zalak Kapadia (zalak@creolestudios.com)
 Date    : 25th April 2018
 Name    : MPAllJobsController
 Purpose : To get all jobs data at admin side
 */
app.controller('MPAllJobsController', ['$scope', 'adminmpfactory', 'Getallcategories', '$timeout', function ($scope, adminmpfactory, Getallcategories, $timeout) {
    console.log('MPAllJobsController loaded.');
    /*create a message to display in our view*/
    $timeout(function () {
        $('.admin-header').find('li').removeClass('active');
        $('#all_jobs').addClass('active');
    }, 500);
    $(".navbar-nav li").removeClass("active");
    $('#adminHeaderMP').addClass("active");
    $scope.message = 'Everyone come and see how good I look';
    $scope.category = 'all';
    $scope.sort = '';
    $scope.search = '';
    $scope.price = '';
    $scope.timeleft = '';
    $scope.endto = $scope.startfrom + 1;
    $scope.currentPage = $scope.currentPaginate = 1;
    $scope.range = $scope.rangepage = $scope.rangePaging = [];
    $scope.categories = Getallcategories.data.data.categories;
    $scope.dispute_count = Getallcategories.data.data.dispute_count;

    $scope.resetData = function () {
        $scope.category = 'all';
        $scope.sort = '';
        $scope.search = '';
        $scope.price = '';
        $scope.timeleft = '';
        $scope.Getalljobsdata($scope.category, $scope.sort, $scope.search, $scope.price, $scope.timeleft, 1);
    }
    $scope.changeTabs = function (evt, jobsStatus, category, sort, search, price, timeleft, tab) {
        $scope.tab = tab;
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(jobsStatus).style.display = "block";
        evt.currentTarget.className += " active";
        if ($scope.tab == 1) {
            $scope.status = [2];
        } else if ($scope.tab == 2) {
            $scope.status = [1];
        } else if ($scope.tab == 3) {
            $scope.status = [3];
        } else if ($scope.tab == 4) {
            $scope.status = [5, 6, 8, 10];
        } else if ($scope.tab == 5) {
            $scope.status = [7];
        } else if ($scope.tab == 6) {
            $scope.status = [9];
        } else {
            $scope.status = [2];
        }
        $scope.Getalljobsdata(category, sort, search, price, timeleft, 1, $scope.status);
    }
    $timeout(function () {
        $scope.tab = 1;
        angular.element('#defaultOpen').triggerHandler('click');
    });

    $scope.filterData = function (category = 'all', sort = '', search = '', price = '', timeleft = '') {
        if ($scope.tab == 1) {
            $scope.status = [2];
        } else if ($scope.tab == 2) {
            $scope.status = [1];
        } else if ($scope.tab == 3) {
            $scope.status = [3];
        } else if ($scope.tab == 4) {
            $scope.status = [5, 6, 8];
        } else if ($scope.tab == 5) {
            $scope.status = [7];
        } else if ($scope.tab == 6) {
            $scope.status = [9];
        } else {
            $scope.status = [2];
        }
        $scope.Getalljobsdata(category, sort, search, price, timeleft, 1, $scope.status);
    }
    $scope.Getalljobsdata = function(category, sort, search, price, timeleft, pageNumber, status) {
        adminmpfactory.Getalljobsdata(category, sort, search, price,timeleft, pageNumber, status).then(function(response) {
            if(response.data.status==1){
                $scope.allJobs = response.data.data.data;
                $scope.openLength = response.data.data.total_open_jobs;
                $scope.activeLength = response.data.data.total_active_jobs;
                $scope.completeLength = response.data.data.total_completed_jobs;
                $scope.cancelledLength = response.data.data.total_cancelled_jobs;
                $scope.escrowLength = response.data.data.total_escrow_jobs;
                $scope.total_completed_project = response.data.data.total_completed_project;
                $scope.total_cancelled_project = response.data.data.total_cancelled_project;
                $scope.total_completed_project_month = response.data.data.total_completed_project_month;
                $scope.paginate = response.data.data;
                $scope.currentJobPaging = response.data.data.current_page;
                $scope.rangeJobpage = _.range(1, response.data.data.last_page + 1);
            }
        });
    }
    $scope.status = 2;
    $scope.Getalljobsdata($scope.category, $scope.sort, $scope.search, $scope.price, $scope.timeleft, 1, $scope.status);
    //Code done by Ketan Solanki for EXCEL and CSV EXport DATE: 27th June 2018
    $scope.Exportdata = function (Exportvalue) {
        var sort = $scope.sort;
        var price = $scope.price;
        var timeleft = $scope.timeleft;
        var search = $scope.search;
        if ($scope.sort === "") {
            var sort = "no-sort";
        }
        if ($scope.price === "") {
            var price = "no-price";
        }
        if ($scope.timeleft === "") {
            var timeleft = "no-timeleft";
        }
        if ($scope.search === "") {
            var search = "no-search";
        }
        if ((Exportvalue) && Exportvalue != '' && Exportvalue != undefined) {
            if (($scope.status) && $scope.status != "" && $scope.status != undefined) {
                window.location = BASEURL + 'Exportdata/' + Exportvalue + '/' + $scope.status + '/' + $scope.category + '/' + sort + '/' + search + '/' + price + '/' + timeleft;
            } else {
                simpleAlert('error', 'Error', "Please select Job type");
            }
        } else {
            simpleAlert('error', 'Error', "Please select Export type");
        }
    };
}]);
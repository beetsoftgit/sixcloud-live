/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 22nd December 2017
 Name    : PrSpecialCaseController
 Purpose : All the functions for PR module open cases
 */
angular.module('sixcloudAdminApp').controller('PrSpecialCaseController', ['$http', '$scope', 'prfactory', '$timeout', '$rootScope', '$filter', function ($http, $scope, prfactory, $timeout, $rootScope, $filter) {
        console.log('PrSpecialCase controller loaded');
    $timeout(function(){
        $(".navbar-nav li").removeClass("active");
        $('#adminHeaderPr').addClass("active");
        $(".menu-list li").removeClass("active");
        $('#special_case').addClass("active");
    },1000);
    $('.cases-list .expend-btn').click(function() { 
        $(this).parent('div').parent('li').toggleClass('open');
        $(this).parent('div').next('.expand-content').slideToggle();
    });
    $scope.openCase = function(caseId){
        $('#case_'+caseId).parent('div').parent('li').toggleClass('open');
        $('#case_'+caseId).parent('div').next('.expand-content').slideToggle(); 
    }
    $scope.Case = function(){
        prfactory.Getcases({'cases_type': 2}).then(function(response){
            if(response.data.status==1)
            {
              $scope.cases = response.data.data;
              $scope.unassignedCasesCount = $scope.cases.unassigned_cases.length;
              $scope.delayedCasesCount = $scope.cases.delayed_cases.length;
              $scope.higherWordCountCasesCount = $scope.cases.higher_word_count_cases.length;
            }
        });
    }
    $scope.Accessors = function() {
        prfactory.Getaccessors({'type':1,'whichPage':'special'}).then(function(response){
            if(response.data.status==1)
            {
              $scope.accessors = response.data.data;
              $scope.availableAccessorsLength = $scope.accessors.length;
            }
        });
    }
    $scope.Case();
    $scope.Accessors();
    $scope.Assignaccessor = function(accessor_id,accessor,case_id,case_status,from_where){
        prfactory.Assignaccessor({'accessor_id': accessor_id,'accessor': accessor,'user_pr_case_id':case_id,'case_status':case_status,'from_where':from_where}).then(function (response) {
                if (response.data.status == 1)
                {
                    simpleAlert('success','Success',response.data.message);
                    $('.close').click();
                    $scope.Case();
                    $scope.Accessors();
                }
            });
    }
    $scope.CancelCase = function(comment,user,case_id){
        var data = {};
        data.comment = comment;
        data.email = user.email_address;
        data.user_name = user.first_name + user.last_name;
        data.id = case_id;
        $(".case_cancel").click();
        $(".cancel_"+case_id).text(' ');
        $(".cancel_"+case_id).append("<i class='fa fa-spinner fa-spin'></i> Please wait");
        $(".cancel_"+case_id).attr("disabled", "disabled");
        prfactory.Cancelcase(data).then(function(response){
            if(response.data.status==1)
            {               
                simpleAlert('success','Success',response.data.message);
                $scope.Case();
            }
        });
    }
    $(document).ready(function()
    {
        $scope.download = function(pr_case,index,whichFile="",whichClass){
            pr_case['whichFile'] = whichFile;
            $("."+whichClass).text(' ');
            $("."+whichClass).append("<i class='fa fa-spinner fa-spin'></i> Downloading");
            $("."+whichClass).attr("disabled", "disabled");
            prfactory.Download(pr_case).then(function (response) {
                if (response.data.status == 1)
                {
                    var url = response.data.data.url;
                    $('#download_accessed_file_'+pr_case.id).attr('href',url);
                    $('#download_accessed_file_'+pr_case.id).attr('target','_self');
                    $('#download_accessed_file_'+pr_case.id)[0].click();
                    $("."+whichClass).text(' ');
                    if(whichFile == 1)
                        $("."+whichClass).append("<i class='sci-download'></i> Original file");
                    else
                        $("."+whichClass).append("<i class='sci-download'></i> Download file");
                    $("."+whichClass).removeAttr("disabled");
                    prfactory.Unlinkdownloadedfile({'file_name': response.data.data.file_name}).then(function (response) {
                        if (response.data.status == 1)
                        {
                            var file_unlinked_success = response.data.message;
                        }
                    });
                }
            });
        }
    });
}]);
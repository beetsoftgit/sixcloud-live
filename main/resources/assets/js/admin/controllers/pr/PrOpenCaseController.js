/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 15th December 2017
 Name    : PrOpenCaseController
 Purpose : All the functions for PR module open cases
 */
angular.module('sixcloudAdminApp').controller('PrOpenCaseController', ['$http', '$scope', 'prfactory', '$timeout', '$rootScope', '$filter', function ($http, $scope, prfactory, $timeout, $rootScope, $filter) {
    console.log('PrOpenCase controller loaded');
    $timeout(function(){
        $(".navbar-nav li").removeClass("active");
        $('#adminHeaderPr').addClass("active");
        $(".menu-list li").removeClass("active");
        $('#open_case').addClass("active");
    },1000);
    // $('.cases-list .expend-btn').click(function() { 
    //     $(this).parent('div').parent('li').toggleClass('open');
    //      $(this).parent('div').next('.expand-content').slideToggle();
    // });
    $scope.openCase = function(caseId){
        $('#case_'+caseId).parent('div').parent('li').toggleClass('open');
        $('#case_'+caseId).parent('div').next('.expand-content').slideToggle(); 
    }
    
    $(".icon-dd i").click(function(){
        $('.my-cases ul li .media').removeClass('remove'); 
        $(this).parents('.media').addClass('remove');
        if($(this).parents('.media').siblings(".collapse").hasClass('in'))
        {
            $(this).parents('.media').removeClass('remove');
        }
    });
    $scope.Cases = function(){
        prfactory.Getcases({'cases_type': 1}).then(function(response){
            if(response.data.status==1)
            {
              $scope.cases = response.data.data;
              $scope.openCasesCount = $scope.cases.length;
            }
        });    
    }
    $scope.Accessors = function(){
        prfactory.Getaccessors({'type':1,'whichPage':'special'}).then(function(response){
            if(response.data.status==1)
            {
              $scope.accessors = response.data.data;
              $scope.availableAccessorsLength = $scope.accessors.length;
            }
        });    
    }
    $scope.Cases();
    $scope.Accessors();
    $scope.Assignaccessor = function(accessor_id,accessor,case_id,case_status,from_where){
        $(".assign").attr("disabled", "disabled");
        prfactory.Assignaccessor({'accessor_id': accessor_id,'accessor': accessor,'user_pr_case_id':case_id,'case_status':case_status,'from_where':from_where}).then(function (response) {
                if (response.data.status == 1)
                {
                    simpleAlert('success','Success',response.data.message);
                    $('.close').click();
                    $(".assign").removeAttr("disabled");
                }
                $scope.Cases();
                $scope.Accessors();
            });
    }
    $scope.CancelCase = function(comment,user,case_id){
        var data = {};
        data.comment = comment;
        data.email = user.email_address;
        data.user_name = user.first_name + user.last_name;
        data.id = case_id;
        $(".case_cancel").click();
        $(".cancel_"+case_id).text(' ');
        $(".cancel_"+case_id).append("<i class='fa fa-spinner fa-spin'></i> Please wait");
        $(".cancel_"+case_id).attr("disabled", "disabled");
        prfactory.Cancelcase(data).then(function(response){
            if(response.data.status==1)
            {
                $scope.Cases();
                $scope.Accessors();
                simpleAlert('success','Success',response.data.message);
            }
        });
    }
    $(document).ready(function(){
        $scope.download = function(pr_case,index,whichFile="",whichClass){
            pr_case['whichFile'] = whichFile;
            $("."+whichClass+'_'+index).text(' ');
            $("."+whichClass+'_'+index).append("<i class='fa fa-spinner fa-spin'></i> Downloading");
            $("."+whichClass+'_'+index).attr("disabled", "disabled");
            prfactory.Download(pr_case).then(function (response) {
                if (response.data.status == 1)
                {
                    var url = response.data.data.url;
                    $('#download_accessed_file_'+pr_case.id).attr('href',url);
                    $('#download_accessed_file_'+pr_case.id).attr('target','_self');
                    $('#download_accessed_file_'+pr_case.id)[0].click();
                    $("."+whichClass+'_'+index).text(' ');
                    if(whichFile == 1)
                        $("."+whichClass+'_'+index).append("<i class='sci-download'></i> Original file");
                    else
                        $("."+whichClass+'_'+index).append("<i class='sci-download'></i> Download file");
                    $("."+whichClass+'_'+index).removeAttr("disabled");
                    prfactory.Unlinkdownloadedfile({'file_name': response.data.data.file_name}).then(function (response) {
                        if (response.data.status == 1)
                        {
                            var file_unlinked_success = response.data.message;
                        }
                    });
                } else {
                    simpleAlert('error', '', response.data.message,false);
                    $("."+whichClass+'_'+index).text(' ');
                    if(whichFile == 1)
                        $("."+whichClass+'_'+index).append("<i class='sci-download'></i> Original file");
                    else
                        $("."+whichClass+'_'+index).append("<i class='sci-download'></i> Download file");
                    $("."+whichClass+'_'+index).removeAttr("disabled");
                }
            });
        }
    });
}]);
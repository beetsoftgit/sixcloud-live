/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 28th December 2017
 Name    : PrAccessorMyAccountController
 Purpose : All the functions for PR module accessor my account
 */
angular.module('sixcloudAdminApp').controller('PrAccessorMyAccountController', ['$http', '$scope', 'prfactory', '$timeout', '$rootScope', '$routeParams', '$filter', function ($http, $scope, prfactory, $timeout, $rootScope, $routeParams, $filter) {
        console.log('Pr Account Info controller loaded');
    $scope.accessor_id = $routeParams.id;
    // $('input[name="dob"]').daterangepicker({
    //     singleDatePicker: true,
    //     showDropdowns: true,
    //     locale: {
    //       format: 'YYYY-MM-DD'
    //     },
    // });
    $( function() {
        $( 'input[name="dob"]' ).datetimepicker({
            pickTime: false,
            format: 'DD/MM/YYYY',
            maxDate: new Date(),
        });
    });
    prfactory.Getaccessorinfo({'accessor_id': 'login_id'}).then(function(response){
        if(response.data.status==1)
        {
          $scope.accessor = response.data.data;
        }
    });

    $(document).ready(function(){
        $("#image").change(function(){
            readURL(this);
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#profile_image').attr('src', e.target.result).width(100)
                        .height(100);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    });
    
    $scope.updateInfo = function(accessor){
        var newaccessor = new FormData($("#updateAccessor")[0]);
        newaccessor.append('update','update');
        newaccessor.append('validation_type','update');
        $.ajax({
            url: BASEURL + "Addaccessor",
            type: "post",
            processData: false,
            contentType: false,
            data: newaccessor,
            beforeSend: function () {
            },
            success: function (response) {
                if (response.status == 1)
                {
                    simpleAlert('success','Success',response.message);
                    location.reload();
                }
                else {
                    simpleAlert('error','Error',response.message);
                }
            },
        });
    }
    $scope.Updatepassword = function(passwords){
        if(passwords.password == passwords.cnf_password){
            prfactory.Updatepassword({'passwords':passwords}).then(function(response){
                if(response.data.status == 1){
                    simpleAlert('success','Success',response.data.message);
                    location.reload();
                }
                else{
                    simpleAlert('error','Error',response.data.message);
                    $scope.passwords = {};
                }
            });    
        }
        else{
            simpleAlert('error','Error','Password and confirm password does not match');
        }
    }
}]);
/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 2nd Jan, 2018
 Name    : PrAccessorMyJobsController
 Purpose : All the functions for PR module accessor my Jobs
 */
angular.module('sixcloudAdminApp').controller('PrAccessorMyJobsController', ['$http', '$scope', 'prfactory', '$timeout', '$rootScope', '$routeParams', '$filter', function ($http, $scope, prfactory, $timeout, $rootScope, $routeParams, $filter) {
    console.log('Pr Account My Jobs controller loaded');
    $('.left-sidebar-menu').remove();
    $('.main-menu').hide();
    $('.cases-list .expend-btn').click(function() { 
        $(this).parent('div').parent('li').toggleClass('open');
        $(this).parent('div').next('.expand-content').slideToggle();
    });
    prfactory.Myjobcases().then(function(response){
        if(response.data.status==1){
            if(response.data.message == 'New case'){
                $scope.case = response.data.data;
                $('.active-case').css('display','none');
                $('.new-case').css('display','inline-block');
            } else {
                $scope.case = response.data.data;
                $scope.active = true;
                $('.active-case').css('display','inline-block');
                $('.new-case').css('display','none');
                $(document).ready(function()
                {
                    $(document).ready(function()
                    {
                        var uploadObj = $("#fileuploader").uploadFile({
                                        url:"Accessedfile",
                                        showDone:false,
                                        multiple:false,
                                        dragDrop:false,
                                        maxFileCount:1,
                                        acceptFiles:".docx, .doc, .txt",
                                        fileName:"file_uploaded",
                                        formData: {"user_pr_case_id":$scope.case.user_pr_case_id,"created_at":$scope.case.created_at,"id":$scope.case.id,"case_details":$scope.case.pr_case},
                                        onSubmit:function(files){
                                            $(".upload_accessed").text(' ');
                                            $(".upload_accessed").append("<i class='fa fa-spinner fa-spin'></i> Uploading");
                                            $(".upload_accessed").attr("disabled", "disabled");
                                        },
                                        onSuccess:function(files,data,xhr,pd)
                                        {
                                            if(data.status == 1) {
                                                simpleAlert('success','Success',data.message);
                                                $('.active-case').css('display','none');
                                                uploadObj.reset();
                                            }
                                            if(data.status == 0) {
                                                simpleAlert('error','Error',data.message);
                                                uploadObj.reset();
                                            }
                                            $(".upload_accessed").text(' ');
                                            $(".upload_accessed").append("<i class='sci-Upload'></i> Upload File");
                                            $(".upload_accessed").removeAttr("disabled");
                                        }
                                    });
                    });
                });
            }
        } else {
            $('.active-case').css('display','none');
            $('.new-case').css('display','none');
            $scope.active = false;
        }
    });
    prfactory.Accessorcompletedjobs().then(function(response){
        if(response.data.status==1){
          $scope.completedCases = response.data.data;
          $scope.completedCasesCount = $scope.completedCases.length;
        }
    });
    prfactory.Myteaminfo().then(function(response){
        if(response.data.status==1){
          $scope.accessorTeam = response.data.data;
          $scope.totalItems = $scope.accessorTeam.length;
        }
    });
    $scope.viewby = 1;
    $scope.currentPage = 1;
    $scope.itemsPerPage = $scope.viewby;
    $scope.maxSize = 100;
    $scope.Rejectcase = function(user_pr_case_id,comment,status,prcaseassignment_id){
        var detail = {};
        detail.user_pr_case_id = user_pr_case_id;
        detail.comment = comment;
        detail.case_status = status;
        detail.action_status = 2;
        detail.prcaseassignment_id = prcaseassignment_id;
        prfactory.Acceptorrejectcase({'detail': detail}).then(function (response) {
            if (response.data.status == 1)
            {
                simpleAlert('success','Success',response.data.message);
                $('#reason-for-reject').modal('hide');
                $('.new-case').css('display','none');
                
            }
        });
    }
    $scope.Acceptcase = function(user_pr_case_id,status,prcaseassignment_id){
        var detail = {};
        detail.user_pr_case_id = user_pr_case_id;
        detail.case_status = status;
        detail.action_status = 1;
        detail.prcaseassignment_id = prcaseassignment_id;
        prfactory.Acceptorrejectcase({'detail': detail}).then(function (response) {
            if (response.data.status == 1)
            {
                $scope.active_case = response.data.data;
                simpleAlert('success','Success',response.data.message);
                $('.active-case').css('display','inline-block');
                $('.new-case').css('display','none');
            }
        });
    }
    $scope.Helpme = function(case_id,accessor_id,team){
        
        $(".help-button").text(' ');
        $(".help-button").append("<i class='fa fa-spinner fa-spin'></i> Sending");
        $(".help-button").attr("disabled", "disabled");
        prfactory.Helpme({'case_id': case_id, 'accessor_id': accessor_id, 'team': team}).then(function (response) {
            if (response.data.status == 1)
            {
                simpleAlert('success','Success',response.data.message);
                $('.active-case').css('display','inline-block');
                $('.new-case').css('display','none');
                $(".help-button").text(' ');
                $(".help-button").append("<i class='sci-notice'></i> HELP ME");
                $(".help-button").removeAttr("disabled");
            }
        });
    }
    $(document).ready(function(){
        $scope.download = function(pr_case,index,whichFile="",whichClass){
            pr_case['whichFile'] = whichFile;
            $("."+whichClass).text(' ');
            $("."+whichClass).append("<i class='fa fa-spinner fa-spin'></i> Downloading");
            $("."+whichClass).attr("disabled", "disabled");
            prfactory.Download(pr_case).then(function (response) {
                if (response.data.status == 1)
                {
                    var url = response.data.data.url;
                    $('#download_accessed_file_'+pr_case.id).attr('href',url);
                    $('#download_accessed_file_'+pr_case.id).attr('target','_self');
                    $('#download_accessed_file_'+pr_case.id)[0].click();
                    $("."+whichClass).text(' ');
                    if(whichFile == 1)
                        $("."+whichClass).append("<i class='sci-download'></i> Original file");
                    else
                        $("."+whichClass).append("<i class='sci-download'></i> Download file");
                    $("."+whichClass).removeAttr("disabled");
                    prfactory.Unlinkdownloadedfile({'file_name': response.data.data.file_name}).then(function (response) {
                        if (response.data.status == 1)
                        {
                            var file_unlinked_success = response.data.message;
                        }
                    });
                }
            });
        }
    });
}]);
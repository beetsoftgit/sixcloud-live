/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 26th December 2017
 Name    : PrClosedCaseController
 Purpose : All the functions for PR module Closed cases
 */
angular.module('sixcloudAdminApp').controller('PrClosedCaseController', ['$http', '$scope', 'prfactory', '$timeout', '$rootScope', '$filter', function ($http, $scope, prfactory, $timeout, $rootScope, $filter) {
    console.log('PrClosedCase controller loaded');
    $timeout(function(){
        $(".navbar-nav li").removeClass("active");
        $('#adminHeaderPr').addClass("active");
        $(".menu-list li").removeClass("active");
        $('#closed_case').addClass("active");
    },1000);
    $(".icon-dd i").click(function(){
        $('.my-cases ul li .media').removeClass('remove'); 
        $(this).parents('.media').addClass('remove');
        if($(this).parents('.media').siblings(".collapse").hasClass('in'))
        {
            $(this).parents('.media').removeClass('remove');
        }
    });
    $scope.openCase = function(caseId){
        $('#case_'+caseId).parent('div').parent('li').toggleClass('open');
        $('#case_'+caseId).parent('div').next('.expand-content').slideToggle(); 
    }
    prfactory.Getcases({'cases_type': 3}).then(function(response){
        if(response.data.status==1)
        {
          $scope.cases = response.data.data;
          $scope.closedCasesCount = $scope.cases.length;
        }
    });
    $(document).ready(function(){
        $scope.download = function(pr_case,index,whichFile="",whichClass){
            pr_case['whichFile'] = whichFile;
            $("."+whichClass+'_'+index).text(' ');
            $("."+whichClass+'_'+index).append("<i class='fa fa-spinner fa-spin'></i> Downloading");
            $("."+whichClass+'_'+index).attr("disabled", "disabled");
            prfactory.Download(pr_case).then(function (response) {
                if (response.data.status == 1)
                {
                    var url = response.data.data.url;
                    $('#download_accessed_file_'+pr_case.id).attr('href',url);
                    $('#download_accessed_file_'+pr_case.id).attr('target','_self');
                    $('#download_accessed_file_'+pr_case.id)[0].click();
                    $("."+whichClass+'_'+index).text(' ');
                    if(whichFile == 1)
                        $("."+whichClass+'_'+index).append("<i class='sci-download'></i> Original file");
                    else
                        $("."+whichClass+'_'+index).append("<i class='sci-download'></i> Download file");
                    $("."+whichClass+'_'+index).removeAttr("disabled");
                    prfactory.Unlinkdownloadedfile({'file_name': response.data.data.file_name}).then(function (response) {
                        if (response.data.status == 1)
                        {
                            var file_unlinked_success = response.data.message;
                        }
                    });
                } else {
                    simpleAlert('error', '', response.data.message,false);
                    $("."+whichClass+'_'+index).text(' ');
                    if(whichFile == 1)
                        $("."+whichClass+'_'+index).append("<i class='sci-download'></i> Original file");
                    else
                        $("."+whichClass+'_'+index).append("<i class='sci-download'></i> Download file");
                    $("."+whichClass+'_'+index).removeAttr("disabled");
                }
            });
        }
    });
}]);
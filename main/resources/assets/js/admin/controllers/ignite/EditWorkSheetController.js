/*
 Author  : Ketan Solanki (ketan@creolestudios.com)
 Date    : 13th August 2018
 Name    : EditWorkSheetController
 Purpose : realted to editing existing worksheet
 */
angular.module('sixcloudAdminApp').controller('EditWorkSheetController', [ '$http', 'adminignitefactory', '$scope', '$timeout', '$rootScope', '$routeParams', '$filter', function( $http, adminignitefactory, $scope, $timeout, $rootScope, $routeParams, $filter) {
    console.log('EditWorkSheetController controller loaded');
    $(".navbar-nav li").removeClass("active");
    $('#adminHeaderIgnite').addClass("active"); 
    $timeout(function () {
        $(".ignite-header li").removeClass("active");
        $('#worksheet_manger').addClass("active");        
    }, 500);

    $scope.slug = $routeParams.slug;    
    $scope.workSheetDetails = function(){
        adminignitefactory.GetworkSheetDetails({ 'id': $scope.slug }).then(function (response) {
            if (response.data.status == 1) {
                $scope.workSheetData = response.data.data;
                $scope.getSubjects($scope.workSheetData.selectedZonesCount[0]);
                $scope.Loadcategoriesforsubject($scope.workSheetData.subject_id);
                if($scope.workSheetData.subcategory_id!=''){
                    $scope.Loadprerequisites($scope.workSheetData.worksheet_category_id)
                }
                $scope.workSheetid = response.data.data.id;
            }
        });
    }
    $scope.workSheetDetails();

    /*adminignitefactory.Getvideocategories().then(function (response) {
        if (response.data.code == 2) window.location.replace("sixteen-login");
        if (response.data.status == 1) {
            $scope.videoCategories = response.data.data;
        }
    });*/
    // adminignitefactory.Getcategories().then(function (response) {
    //     if (response.data.code == 2) window.location.replace("manage/login");
    //     if (response.data.status == 1) {
    //         $scope.subjects = response.data.data.categories;
    //     }
    // });
    $scope.Loadcategoriesforsubject = function(subjectId){
        $scope.subCategories =  undefined;
        $scope.subCategoriesPresent = 0;
        $scope.video_ordering =  undefined;
        adminignitefactory.Loadcategoriesforsubject({ 'subject_id': subjectId }).then(function (response) {
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {
                $scope.videoCategories = response.data.data.categories;
            }
        });
    }
    $scope.Addvideolanguages = function(){
        $scope.selectedZones1 = [];
        $.each($('input[name="video_for_zones[]"]:checked'), function(){            
            $scope.selectedZones1.push($(this).val());
        });
        if($scope.selectedZones1.length==0){
            simpleAlert('error', '', 'Select Zone(s)');
            return;
        } else {
            adminignitefactory.Loadlanguagesforzones({ 'selected_zones': $scope.selectedZones1,'isEdit':true,'for':'worksheet','worksheet_id':$scope.workSheetData.id }).then(function (response) {
                if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    $scope.workSheetData.worksheet_language = response.data.data;

                }
            });
        }
    }
    $scope.Loadprerequisites = function (id) {
        $scope.workSheetData.category_id = id;
        adminignitefactory.Loadprerequisites({ 'video_category_id': id }).then(function (response) {
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {
                if(response.data.data.yesSubcategories){
                    $scope.subCategoriesPresent = 1;
                    $scope.subCategories = response.data.data.yesSubcategories.subCategories;
                } else {
                    $scope.workSheetData.subcategory_id = undefined;
                    $scope.subCategories = undefined;
                    $scope.subCategoriesPresent = 0;
                }
            }
        });
    }
    $scope.setSubCategory = function(subCatId) {
        $scope.workSheetData.subcategory_id = subCatId;
    }
    $scope.removeOldFile = function() {
        swal({
          title: "Are you sure?",
          text: "You will not be able to retrieve this file.",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes, remove it!",
          cancelButtonText: "No, cancel please!",
          closeOnConfirm: true,
          closeOnCancel: true
        },
        function(isConfirm){
            if (isConfirm) {
                $timeout(function() {
                    $scope.worksheet_file = undefined;
                    $scope.showFileBox = true;
                    $scope.hideFileBox = true;
                }, 100);
            }
        });
    }
    $scope.setFile = function() {
        $scope.worksheet_file = $('#worksheet_file').val()

    }
    // $scope.Loadprerequisites = function (id) {
    //     adminignitefactory.Loadprerequisites({ 'video_category_id': id }).then(function (response) {
    //         if (response.data.code == 2) window.location.replace("sixteen-login");
    //         if (response.data.status == 1) {
    //             $scope.prerequisitesCounter = 1;
    //             $scope.prerequisitesQuizCounter = 1;
    //             $('#prerequisitesDiv').empty();
    //             $('#preRequisitesQuizesDiv').empty();
    //             $scope.video_ordering = response.data.data.lastSequence;
    //             $scope.preRequisites = response.data.data.preRequisites;
    //             abc = response.data.data.preRequisites;
    //             if ($scope.preRequisites.length > 1) {
    //                 $scope.showMoreDisable = true;
    //             } else {
    //                 $scope.showMoreDisable = false;
    //             }
    //             $scope.preRequisitesBackup = response.data.data.preRequisites;
    //             $scope.quiz_ordering = response.data.data.lastSequence;
    //             $scope.preRequisitesQuizes = response.data.data.preRequisites;
    //             if ($scope.preRequisitesQuizes.length > 1) {
    //                 $scope.showMoreDisableQuiz = true;
    //             } else {
    //                 $scope.showMoreDisableQuiz = false;
    //             }
    //             $scope.workSheetData.worksheet_ordering = response.data.data.lastSequence;
    //             return false;
    //         }
    //     });
    // }    
    $scope.downloadWorksheetfile =function(fileName,originalFileName){
        var filePath = BASEURL + "public/uploads/worksheet_files/" + fileName;
        var http = new XMLHttpRequest();         
        http.open('HEAD', filePath, false);
        http.send();
        if (http.status == 200) {
            window.location = 'download-worksheet-file?filename=' + fileName+'&originalfilename='+originalFileName;
        } else {
             simpleAlert('error', 'Some Error Occured', 'File Not Found', true);
        }       
    }; 
    $scope.Editworksheet = function (status, buttonText, elementId) {                     
        var workSheetTitleText = $("#workSheetTitleText").val();        
        var workSheetDescription = $("#description").val();  
        var selectedCategory = $("input[name='worksheet_category_id']:checked").val();      
        var worksheetOrdering = $("#worksheet_ordering").val();  
        if($scope.showFileBox == true) {
            var fileElement = document.getElementById("worksheet_file");        
            var files = fileElement.files;                
            if(files.length > 0){                        
                if (files[0].type != 'application/pdf') {
                    simpleAlert('success', '', 'File format not allowed for worksheet file. (only PDF format allowed)');
                    return;
                }
            }
        }
        // if (workSheetTitleText == undefined || workSheetTitleText == "" || workSheetDescription == undefined || workSheetDescription == "" || selectedCategory == "" || selectedCategory == undefined || worksheetOrdering == undefined || worksheetOrdering == "") {
        if (selectedCategory == "" || selectedCategory == undefined) {
            // if (workSheetTitleText == undefined || workSheetTitleText == "") {
            //     simpleAlert('success', '', 'Worksheet "title" is required');
            //     return;
            // }
            // if (workSheetDescription == undefined || workSheetDescription == "") {
            //     simpleAlert('success', '', 'Worksheet "description" is required');
            //     return;
            // }            
            /*if (worksheetOrdering == undefined || worksheetOrdering == "") {
                simpleAlert('success', '', 'Worksheet "position" is required');
                return;
            }*/
            if (selectedCategory == undefined || selectedCategory == "") {
                simpleAlert('success', '', 'Worksheet "category" is required');
                return;
            }      
        } else if($scope.showFileBox == true && $scope.worksheet_file == undefined) {
            simpleAlert('success', '', 'Worksheet "file" is required');
        } else {
            newWorksheet = new FormData($("#EditWorkSheetForm")[0]);
            newWorksheet.append("status",status);
            newWorksheet.append("subCategoriesPresent",$scope.subCategoriesPresent);
            showLoader("#"+elementId);
            adminignitefactory.Editworksheet(newWorksheet).then(function (response) {                                 
                hideLoader("#"+elementId, buttonText);
                if (response.data.code == 2) window.location.replace("sixteen-login");
                if (response.data.status == 1) {
                    window.location.replace("manage/ignite/worksheet-manager/"+$scope.workSheetData.status+"/"+$scope.workSheetData.worksheet_category_id);
                } else {
                    simpleAlert('error','',response.data.message);
                }
            });
        }
    }
    $scope.UnpublishWorkSheet = function(status){
        var WorkSheetId = $("#hidWorkSheetId").val();        
        showLoader("#publish-unpublish");
        var data ={"WorkSheetId":WorkSheetId, "status":status};
        adminignitefactory.UnpublishWorkSheet(data).then(function (response) {                                             
            hideLoader("#publish-unpublish", 'Un Publish');
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {
                window.location.replace("manage/ignite/worksheet-manager/"+$scope.workSheetData.status+"/"+$scope.workSheetData.worksheet_category_id);
            } else {
                simpleAlert('error','',response.data.message);
            }
        });
    };
    $scope.getSubjects = function(zone_id, count) {
        adminignitefactory.Getcategories({'zone_id':zone_id}).then(function (response) {
            if (response.data.code == 2) window.location.replace("sixteen-login");
            if (response.data.status == 1) {
                if(count == 1) {
                    $scope.workSheetData.subject_id = undefined;
                    $scope.workSheetData.worksheet_category_id = undefined;
                    $scope.videoCategories = undefined;
                    $scope.subCategories = undefined;
                }
                $scope.subjects = response.data.data.categories;
                if($scope.subjects) {
                    $scope.subjects.forEach(function(e){
                        e.zones = e.zones.split(',');
                        var filtered = e.zones.filter(function(value){
                            return value != zone_id;
                        });
                        e.otherZones = filtered;
                    });
                }
            }
        });
    }
    $scope.setVariables = function(subjectId) {
        $scope.workSheetData.subject_id = subjectId;
        $scope.workSheetData.category_id = undefined;
        $scope.workSheetData.subcategory_id = undefined;
    }
}]);
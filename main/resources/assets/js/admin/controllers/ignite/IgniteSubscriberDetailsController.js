/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 5th Sept. 2018
 Name    : IgniteSubscriberDetailsController
 Purpose : All the functions for ignite subscriber detail page
 */
angular.module('sixcloudAdminApp').controller('IgniteSubscriberDetailsController', ['$http', '$scope', '$timeout', '$routeParams', '$rootScope', '$filter', 'adminignitefactory', 'Getcountry', 'adminfactory', function ($http, $scope, $timeout, $routeParams, $rootScope, $filter, adminignitefactory, Getcountry, adminfactory) {

    $(".navbar-nav li").removeClass("active");
    $('#adminHeaderIgnite').addClass("active");
    $scope.countryList = Getcountry.data.data.country;
    $timeout(function () {
        $(".ignite-header li").removeClass("active");
        $('#ignite_subscribers').addClass("active");
        $('.custom-accordion .panel-heading').click(function () {
            $(this).toggleClass('active');
            $(this).next('.panel-collapse').slideToggle();
        });
        // $rootScope.current_domain  = current_domain;
        $scope.current_domain = current_domain;
        console.log(current_domain);
    }, 500);

    // $( function() {
    //     $( ".datetimepicker" ).datetimepicker({
    //         pickTime: false,
    //         format: 'DD/MM/YYYY',
    //         maxDate: new Date(),
    //     });
    // });

    initSample();

    $scope.params = $routeParams;
    $scope.getDetailData = function(){
        adminignitefactory.GetSubscriberDetailsData({ 'slug': $scope.params.slug }).then(function (response) {
            if (response.data.status == 1) {
                $scope.distributorDetails = response.data.data.distributorData;
                $scope.Getstates($scope.distributorDetails.country_id);
                $scope.Getcities($scope.distributorDetails.provience_id);
                $scope.transactions = response.data.data.transactions;
                $scope.GetCurrentSubscription({ 'paid_by': $scope.distributorDetails.id }, 1);
                $scope.GetHistorySubscription({ 'paid_by': $scope.distributorDetails.id }, 1);
            }
        });
    }
    $scope.getDetailData();
    $scope.GetCurrentSubscription = function (data, pageNumber) {
        adminignitefactory.GetCurrentSubscription(data, pageNumber).then(function (response) {            
            if (response.data.status == 1) {
                $scope.currentSubscription      = response.data.data.data;                
                $scope.totalCurrentSubscription = response.data.data.total;
                $scope.paginateCurrent          = response.data.data;
                $scope.currentPaging            = response.data.data.current_page;
                $scope.rangeCurrentpage         = _.range(1, response.data.data.last_page + 1);
            }
        });
    }

    $scope.getAllSubscription = function (data) {
        adminignitefactory.getAllSubscription(data).then(function (response)
        {
            if(response.data.status == 1) {
                $scope.allSubscription = response.data.data;
                $.each($scope.allSubscription, function(key, value){
                    $( function() {
                        $( "#datetimepicker_effective_"+value.id).datetimepicker({
                            pickTime: false,
                            format: 'DD/MM/YYYY',
                            minDate: new Date(),
                        });
                    });
                });
                $('#removeSubscriptionPlace').modal('show');
            }
        })
    }

    $scope.removeSubscription = function () {
        ids = [];
        effective_dates = [];
        category_names = [];
        $.each($("input[name='removeCheckbox']:checked"), function(){
            ids.push($(this).val());
            effective_dates.push($("#datetimepicker_effective_"+($(this).val())).val());
            category_names.push($("#category_name_"+($(this).val())).val());
        });
        data = {};
        $.each(ids, function(key, value){
            data[key] = {
                'id': value,
                'subscription_end_date': effective_dates[key],
                'category_name': category_names[key],
                'subscriberId': $scope.distributorDetails.id
            }
        });
        adminignitefactory.removeSubscription(data).then(function(response)
        {
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {
                simpleAlert('success', '', response.data.message);
                $scope.getDetailData();
                $('#removeSubscriptionPlace').modal('hide');
            } else {
                simpleAlert('error', '', response.data.message);
            }
        })
    }

    $scope.GetHistorySubscription = function (data, pageNumber) {
        adminignitefactory.GetHistorySubscription(data, pageNumber).then(function (response) {            
            if (response.data.status == 1) {
                $scope.historySubscription      = response.data.data.data;                
                $scope.totalHistorySubscription = response.data.data.total;
                $scope.paginateHistory          = response.data.data;
                $scope.historyPaging            = response.data.data.current_page;
                $scope.rangeHistorypage         = _.range(1, response.data.data.last_page + 1);
            }
        });
    }

    

    $scope.EmailDistributor = function (id) {
        $('#emailDistributorModal').modal('show');
        CKEDITOR.instances.editor;
    }
    $scope.changePassword = function (id) {
        $('#changePasswordModal').modal('show');    
    }

    $scope.sendEmailToDistributor = function () {
        var message = CKEDITOR.instances.editor.getData();
        if ($scope.email_subject == undefined || message == "" || message == undefined) {
            if ($scope.email_subject == undefined) {
                simpleAlert('error', '', 'Subject is required.');
                return;
            }
            if (message == "" || message == undefined) {
                simpleAlert('error', '', 'Message is required.');
                return;
            }
        } else {
            var formData = new FormData($("#sendEmailForm")[0]);
            formData.append("message", CKEDITOR.instances.editor.getData());
            formData.append("emailTo", 'User');
            showLoader('.submit_bank', 'Please wait');
            adminignitefactory.sendEmailToDistributor(formData).then(function (response) {
                hideLoader(".submit_bank", 'Submit');
                if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    $('#emailDistributorModal').modal('hide');
                    simpleAlert('success', '', response.data.message);
                } else {
                    simpleAlert('error', '', response.data.message);
                }
            });
        }
    }

    $scope.changeDistributorPassword = function () {
        var passwordLength = 0;
        if($scope.change_password != undefined){
            var passwordLength = $scope.change_password.length;    
        }
        var confirmPasswordLength = 0;
        if($scope.change_confirm_password != undefined){
            var confirmPasswordLength = $scope.change_confirm_password.length;    
        }                        
        if ($scope.change_password == undefined || $scope.change_password == "" || $scope.change_confirm_password == undefined || $scope.change_confirm_password == "" || $scope.change_password != $scope.change_confirm_password || passwordLength < 8 || confirmPasswordLength < 8||$scope.admin_password == undefined || $scope.admin_password == ""||$scope.new_password_expression_error==true) {
            if ($scope.admin_password == undefined || $scope.admin_password == "") {
                simpleAlert('error', '', 'Admin password is required.');
                return;
            }
            if ($scope.change_password == undefined || $scope.change_password == "") {
                simpleAlert('error', '', 'Password is required.');
                return;
            }
            if (passwordLength < 8) {
                simpleAlert('error', '', 'Minimum 8 Charachters are required for Password.');
                return;
            }
            if($scope.new_password_expression_error==true){
                simpleAlert('error', '', 'Use 8 to 16 characters with at least 1 uppercase letter, 1 lowercase letter & numbers for new password.');
                return;
            }
            if ($scope.change_confirm_password == undefined || $scope.change_confirm_password == "") {
                simpleAlert('error', '', 'Confirm Password is required.');
                return;
            }
            if (confirmPasswordLength < 8) {
                simpleAlert('error', '', 'Minimum 8 Charachters are required for Confirm Password.');
                return;
            }
            if ($scope.change_password != $scope.change_confirm_password) {
                simpleAlert('error', '', 'Password and Confirm Password should be same.');
                return;
            }
        } else {
            var formData = new FormData($("#changePasswordForm")[0]);
            formData.append('updateFor','User');
            showLoader('.change_password', 'Please wait');
            adminignitefactory.changeDistributorPassword(formData).then(function (response) {
                hideLoader(".change_password", 'Submit');
                if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    $('#changePasswordModal').modal('hide');
                    simpleAlert('success', '', response.data.message);
                } else {
                    simpleAlert('error', '', response.data.message);
                }
            });
        }
    }

    function suspendredirect() {
            $('#adminPasswordModal').modal('show');
        // window.location.replace(BASEURL + '/manage/ignite/distributors');
    }
    $scope.suspendAccount = function (id,status,btn_text) {
        $scope.suspendReactivateButton = btn_text;
        $scope.suspendStatus = status;
        confirmDialogueDifferentButton('','','',suspendredirect);
    }
    $scope.suspendAccountConfirm = function(){
        if($scope.admin_password_confirm==undefined||$scope.admin_password_confirm==''){
            simpleAlert('error','','Enter your password');
        } else {
            adminignitefactory.suspendAccount({ 'distributorId': $scope.distributorDetails.id,'suspendFor':'User','admin_password':$scope.admin_password_confirm,'status':$scope.suspendStatus }).then(function (response) {
                showLoader('.admin_password_suspend');
                if (response.data.status == 1) {
                    hideLoader('.admin_password_suspend','Suspend');
                    // window.location.replace('manage/ignite/subscribers');
                    window.location.reload();
                } else {
                    $scope.admin_password_confirm=undefined;
                    hideLoader('.admin_password_suspend','Suspend');
                    simpleAlert('error', '', response.data.message);
                }
            });
        }
    }
    var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,16})");
    $scope.Checkexpression = function(password){
        if(strongRegex.test(password)) {
                $scope.new_password_expression_error = false;
        } else {
                $scope.new_password_expression_error = true;
        }
    }

    $scope.Getcategoriesforsubscription = function(subscriberId) {
        adminignitefactory.Getcategoriesforsubscription({'subscriberId':subscriberId}).then(function (response) {
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {
                if(response.data.code == 3) $scope.cannotAdd = 1;
                $scope.videoCategories = response.data.data;
                $.each($scope.videoCategories, function(key, value){
                    $( function() {
                        $( "#datetimepicker_end_"+value.id).datetimepicker({
                            pickTime: false,
                            format: 'DD/MM/YYYY',
                            minDate: new Date(),
                            defaultDate: value.subscriptionTill,
                        });
                        $( "#datetimepicker_suspend_"+value.id).datetimepicker({
                            pickTime: false,
                            format: 'DD/MM/YYYY',
                            minDate: new Date(),
                            defaultDate: new Date(),
                        });
                    });
                });
                //$('#addSubscriptionPlace').modal('show');
                $('#addSubscriptionPlace').modal({
                    show: true,
                    backdrop: 'static',
                    keyboard: false
                });
            } else {
                if(response.data.code==3)
                    simpleAlert('error', '', response.data.message);
            }
        });
    }

    $scope.AddSubscription = function(){
        subscription = new FormData($("#grantCategoryform")[0]);
        subscription.append('subscriberId',$scope.distributorDetails.id);
        adminignitefactory.AddSubscription(subscription).then(function (response) {
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {
                simpleAlert('success', '', response.data.message);
                $scope.getDetailData();
                $('#addSubscriptionPlace').modal('hide');
            } else {
                simpleAlert('error', '', response.data.message);
            }
        });
    }

    $scope.editDetails = function(id){
        $scope.calculateAge = function(dob) {
            var date = dob;
            var datearray = date.split("/");
            var newdate = datearray[1] + '/' + datearray[0] + '/' + datearray[2];
            var ageDifMs = Date.now() - new Date(newdate);
            var ageDate = new Date(ageDifMs);
            $scope.age = Math.abs(ageDate.getUTCFullYear() - 1970);
            // console.log($scope.age);
        }
        $scope.editDetail = {};
        $scope.current_domain_temp = current_domain;
        if(current_domain=='net'){
            $scope.editDetail.contact = $scope.distributorDetails.contact;
            $scope.editDetail.phonecode = $scope.distributorDetails.phonecode;
        }
        if($scope.distributorDetails.date_format) {
            $scope.editDetail.dob = $scope.distributorDetails.date_format;
            $scope.calculateAge($scope.editDetail.dob);
        }
        $scope.editDetail.first_name = $scope.distributorDetails.first_name;
        $scope.editDetail.last_name = $scope.distributorDetails.last_name;
        $scope.editDetail.country_id = $scope.distributorDetails.country_id;
        $scope.editDetail.provience_id = $scope.distributorDetails.state_id;
        $scope.editDetail.city_id = $scope.distributorDetails.city_id;
        // $scope.Getstates($scope.editDetail.country_id);
        $( function() {
            $( "#dob" ).datetimepicker({
                pickTime: false,
                format: 'DD/MM/YYYY',
                maxDate: new Date(),
                defaultDate: $scope.editDetail.dob,
            });
        });
            
        $('#editDetailsModal').modal('show');
    }
    


    $scope.editSubscriberDetails = function(details) {
        if(details==undefined){
            simpleAlert('error', '', 'Please fill up the details.');
            return;
        }
        if($scope.distributorDetails.country_id==undefined || $scope.distributorDetails.country_id=='') {
            simpleAlert('error', '', '"Country" is required.');
            return;
        }
        if($scope.distributorDetails.provience_id==undefined || $scope.distributorDetails.provience_id=='') {
            simpleAlert('error', '', '"State" is required.');
            return;
        }
        if($scope.distributorDetails.city_id==undefined || $scope.distributorDetails.city_id=='') {
            simpleAlert('error', '', '"City" is required.');
            return;
        }
        if(details.first_name==undefined||details.first_name==''||details.last_name==undefined||details.last_name=='') {
            if(details.first_name==undefined||details.first_name==''){
                simpleAlert('error', '', '"First Name" is required.');
                return;
            }
            if(details.last_name==undefined||details.last_name==''){
                simpleAlert('error', '', '"Last Name" is required.');
                return;
            }
            // if($scope.age<18){
            //     simpleAlert('error', '', '"DoB" should be above 18 years.');
            //     return;
            // }
        } else {
            /*if(current_domain=='net'){
                if($scope.distributorDetails.phonecode==undefined||$scope.distributorDetails.phonecode==''){
                    simpleAlert('error', '', '"Phonecode" is required.');
                    return;
                }
                if(details.contact==undefined||details.contact==''){
                    simpleAlert('error', '', '"Contact" is required.');
                    return;
                }
            }*/
            details.phonecode = $scope.distributorDetails.phonecode;
            details.subscriber_id = $scope.distributorDetails.id;
            details.country_id = $scope.distributorDetails.country_id;
            details.city_id = $scope.distributorDetails.city_id;
            details.provience_id = $scope.distributorDetails.provience_id;
            adminignitefactory.Editsubscriberdetails(details).then(function (response) {
                if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    /*$scope.distributorDetails.first_name = response.data.data.first_name;
                    $scope.distributorDetails.last_name = response.data.data.last_name;
                    $scope.distributorDetails.phonecode = response.data.data.phonecode;
                    $scope.distributorDetails.contact = response.data.data.contact;
                    $scope.distributorDetails.dob = response.data.data.dob;*/
                    // $scope.getDetailData();
                    swal({
                        title: "",
                        text: response.data.message,
                        type: ""
                    }, function() {
                        window.location.reload(true);
                    });
                    // simpleAlert('success', '', response.data.message);
                    $('#editDetailsModal').modal('hide');
                } else {
                    simpleAlert('error', '', response.data.message);
                }
            });
        }
    }
    $scope.Getstates = function(country_id) {
        $scope.stateList = undefined;
        $scope.cityList = undefined;
        if(country_id != '') {
            obj = $scope.countryList.find(o => o.id == $scope.distributorDetails.country_id);
            $scope.distributorDetails.phonecode = obj.phonecode;
            adminignitefactory.Getstatesforcountry({'id':country_id}).then(function (response) {
                if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    $scope.stateList = response.data.data;
                } else {
                    simpleAlert('error', '', response.data.message);
                }
            });
        }
    }
    $scope.Getcities = function(state_id) {
        $scope.cityList = undefined;
        if(state_id != '') {
            adminignitefactory.Getcitiesforstate({'id':state_id}).then(function (response) {
                if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    $scope.cityList = response.data.data;
                } else {
                    simpleAlert('error', '', response.data.message);
                }
            });
        }
    }

    adminfactory.Getcountry().then(function(response){
        if(response.data.status==1) {
            $scope.countries = response.data.data.country;
        }      
    });

    /*$scope.RemoveSubscription = function(video_category_id, date, cat_name){
        var paid_by = $scope.distributorDetails.id;
        var video_cat_id = video_category_id;
        adminignitefactory.removeSubscriptionOfSubscriber(
            {'paid_by': paid_by,'video_category_id': video_cat_id, 'date' : date, 'video_category_name' : cat_name}).then(function(response){
            if(response.data.status==1) {
                window.location.reload();
                // $('#addSubscriptionPlace').modal('hide');
                // $scope.selectedIndex=undefined;
                // $scope.GetCurrentSubscription({ 'paid_by': $scope.distributorDetails.id }, 1);
                // $scope.GetHistorySubscription({ 'paid_by': $scope.distributorDetails.id }, 1);
            }
        });
    }*/

    $scope.removeSubscription = function () {
       ids = [];
       effective_dates = [];
       category_names = [];
       $.each($("input[name='removeCheckbox']:checked"), function(){
           ids.push($(this).val());
           effective_dates.push($("#datetimepicker_effective_"+($(this).val())).val());
           category_names.push($("#category_name_"+($(this).val())).val());
       });
       data = {};
       $.each(ids, function(key, value){
           data[key] = {
               'id': value,
               'subscription_end_date': effective_dates[key],
               'category_name': category_names[key],
               'subscriberId': $scope.distributorDetails.id
           }
       });
       adminignitefactory.removeSubscription(data).then(function(response)
       {
           if (response.data.code == 2) window.location.replace("manage/login");
           if (response.data.status == 1) {
               simpleAlert('success', '', response.data.message);
               $scope.getDetailData();
               $('#removeSubscriptionPlace').modal('hide');
           } else {
               simpleAlert('error', '', response.data.message);
           }
       })
   }
}]);
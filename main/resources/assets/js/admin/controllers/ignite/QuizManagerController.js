/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 4th July 2017
 Name    : AllContentController
 Purpose : All the functions for home page
 */
angular.module('sixcloudAdminApp').controller('QuizManagerController', ['$http', '$scope', '$routeParams','$timeout', '$rootScope', '$filter', 'adminignitefactory', function ($http, $scope, $routeParams,$timeout, $rootScope, $filter, adminignitefactory) {
    // $scope.sections = Geteltsections.data.data;
    console.log('QuizManagerController loaded');
    $(".navbar-nav li").removeClass("active");
    $('#adminHeaderIgnite').addClass("active");
    $scope.status = $routeParams.status;
    $scope.category = $routeParams.category;
    $timeout(function () {
        $(".ignite-header li").removeClass("active");
        $('#quiz_manger').addClass("active");
    }, 500);
    $scope.publishTypes   = [{"value":2,"name":"Published Quizzes"},{"value":3,"name":"Unpublished Quizzes"}, {"value":1,"name":"Drafts"}];
    /* old version : start*/
    /*$scope.buzzTypes      = [{"value":1,"name":"Buzz 1"},{"value":2,"name":"Buzz 2"}, {"value":3,"name":"Buzz 3"}];
    if($scope.status==undefined && $scope.category==undefined){
        $scope.type           = $scope.publishTypes[0].value;
        $scope.buzzType       = $scope.buzzTypes[0].value;
    } else {
        $scope.type           = $scope.status;
        $scope.buzzType       = $scope.category;
    }
    $scope.GetAllQuizData = function (data, pageNumber) {
        adminignitefactory.GetAllQuizData(data, pageNumber).then(function (response) {
            if (response.data.status == 1) {
                $scope.allQuizContent = response.data.data.data;
                $scope.totalQuiz = response.data.data.total;
                $scope.paginate = response.data.data;
                $scope.currentQuizPaging = response.data.data.current_page;
                $scope.rangeQuizpage = _.range(1, response.data.data.last_page + 1);     
                $("#PublishedQuizzesTab").css("display", "block");           
                $(".tablinks").removeClass("active");
                $("#quizType_"+$scope.type).addClass("active");
                $("#buzzType_"+$scope.buzzType).addClass("active");
            }
        });
    }
    $scope.GetAllQuizData({ 'type': $scope.type,'categoty_id':$scope.buzzType }, 1);*/
    /* old version : end */
    /* New Version : Start*/
        $scope.getQuizzes = {};
        adminignitefactory.Getzones().then(function (response) {
            if (response.data.status == 1) {
                $scope.zones = response.data.data.zones;
                $scope.getQuizzes.type = $scope.publishTypes[0].value;
                $scope.type = $scope.getQuizzes.type;
                $scope.getQuizzes.zone = $scope.zones[0].id;
                $scope.getSubjects($scope.getQuizzes.zone);
                $scope.GetAllQuizData($scope.getQuizzes, 1);   
            }
        });
        // adminignitefactory.Getcategories().then(function (response) {
        //     if (response.data.status == 1) {
        //         $scope.categories = undefined;
        //         $scope.subCategories = undefined;
        //         // $scope.subjects = response.data.data.categories;
        //     }
        // });
        $scope.Getcategories = function(subject) {
                adminignitefactory.Loadcategoriesforsubject({'subject_id':subject}).then(function (response) {
                if (response.data.status == 1) {
                    $scope.categories = undefined;
                    $scope.subCategories = undefined;
                    $scope.getQuizzes.category = undefined;
                    $scope.getQuizzes.subcategory = undefined;
                    $scope.categories = response.data.data.categories;
                }
            });
        }
        $scope.Getsubcategories = function(category, subCatOrNot) {
            if(subCatOrNot == 1) {
                $scope.getQuizzes.subcategory = undefined;
            }
            if(category != null) {
                adminignitefactory.Getsubcategories({'id':category, 'for':'Quiz'}).then(function (response) {
                    if(response.data.status == 1){
                        $scope.subCategories = undefined;
                        $scope.getQuizzes.subcategory = undefined;
                        $scope.subCategories = response.data.data;
                        $scope.subCategoriesExist = $scope.subCategories.length>0?true:false;
                        $scope.selectSubcategory = true;
                    } else if(response.data.status == 0) {
                        if(response.data.code==2){
                            window.location.replace("manage/login");
                        } else if(response.data.code==308) {
                            if(subCatOrNot != 0) {
                                $scope.subCategoriesExist = false;
                            }
                            $scope.selectSubcategory = false;
                        } else {
                            if(subCatOrNot != 0) {
                                $scope.subCategoriesExist = false;
                            }
                            simpleAlert('error', '', response.data.message);
                        }
                    }
                });
            }
        }
        $scope.selectSubcategory = true;
        $scope.GetAllQuizData = function (data, pageNumber) {
            adminignitefactory.GetAllQuizData(data, pageNumber).then(function (response) {
                if (response.data.status == 1) {
                    $scope.allQuizContent = response.data.data.data;
                    $scope.totalQuiz = response.data.data.total;
                    $scope.paginate = response.data.data;
                    $scope.currentQuizPaging = response.data.data.current_page;
                    $scope.rangeQuizpage = _.range(1, response.data.data.last_page + 1);     
                    $("#PublishedQuizzesTab").css("display", "block");           
                    $(".tablinks").removeClass("active");
                    $("#quizType_"+$scope.type).addClass("active");
                    $("#buzzType_"+$scope.buzzType).addClass("active");
                }
            });
        }
        $scope.GetVideos = function(getQuizzes){
            // if($scope.status==undefined){
            //     getQuizzes.type = $scope.publishTypes[0].value;
            //     $scope.type = $scope.publishTypes[0].value;
            // }
            getQuizzes.subCategoryExists = $scope.selectSubcategory;
            $scope.GetAllQuizData(getQuizzes, 1);
        }
        $scope.filterData = function (type, page) {
            $scope.type = type;
            $scope.getQuizzes.type = type; 
            $scope.GetAllQuizData($scope.getQuizzes, page);
        }
    /* New Version : End */
    /*$scope.filterData = function (type, page) {
        $scope.type     = type;
        $rootScope.type = type;
        $scope.GetAllQuizData({ 'type': $scope.type,'categoty_id':$scope.buzzType }, page);
    }
    $scope.filterDataBuzz = function (buzz, page) {
        $scope.buzzType     = buzz;
        $rootScope.buzzType = buzz;
        $scope.GetAllQuizData({ 'type': $scope.type,'categoty_id':$scope.buzzType }, page);
    }*/
    $scope.getSubjects = function(zone) {
        if(zone != '') {
            adminignitefactory.Getcategories({'zone_id':zone}).then(function (response) {
            if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    $scope.subjects = response.data.data.categories;
                }
            });
        } else {
            $scope.subjects = undefined;
        }
    }
}]);
/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 24th Dec 2018
 Name    : IgniteTransactionsListingController
Purpose : All the functions for admin panel transactions page
 */
angular.module('sixcloudAdminApp').controller('IgniteTransactionsListingController', ['$sce', 'adminignitefactory', '$location', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, adminignitefactory, $location, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {
    $scope.url = $location.url();
    $(".navbar-nav li").removeClass("active");
    $('#adminHeaderIgnite').addClass("active");
    $timeout(function(){
        $(".ignite-header li").removeClass("active");
        $('#ignite_transactions').addClass("active");
    },500);
    
    $scope.sort = 'no';

    $scope.GetAllTransactions = function (pageNumber, propertyName) {
        adminignitefactory.Getignitetransactions({'page': pageNumber, 'propertyName': $scope.propertyName, 'sort' : $scope.sort}).then(function(response) {
            if(response.data.status==1){
                $scope.transactions = response.data.data.data;
                $scope.totalTransactions = response.data.data.total;
                $scope.paginateTransactions = response.data.data;
                $scope.currentTransactionsPaging = response.data.data.current_page;
                $scope.rangeTransactionspage = _.range(1, response.data.data.last_page + 1);
                // $scope.reverse = true;
            }
        });
    }
    $scope.GetAllTransactions(1);

    
    $scope.ordersort = function(propertyName) {
        /*$scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
        $scope.propertyName = propertyName;*/
        $scope.sort = $scope.sort == 'yes' ? 'no' : 'yes';
        $scope.propertyName = propertyName;
        $scope.GetAllTransactions(1, $scope.propertyName);
    };

    $scope.ExportPromos = function(export_type) {
        if(export_type!= undefined && export_type!= ''){
            window.location = BASEURL + 'ExportTransactions?export_type=' + export_type;
        } else {
            simpleAlert('error', '', 'Select export type');
        }
    }
}]);
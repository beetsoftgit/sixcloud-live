/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 3rd August 2018
 Name    : VideoManagerController
 Purpose : All the functions for video manager page
 */
angular.module('sixcloudAdminApp').controller('VideoManagerController', ['$http', '$scope', '$routeParams','$timeout', '$rootScope', '$filter', 'adminignitefactory', function ($http, $scope, $routeParams,$timeout, $rootScope, $filter, adminignitefactory) {
    // $scope.sections = Geteltsections.data.data;
    console.log('VideoManagerController loaded');
    $(".navbar-nav li").removeClass("active");
    $('#adminHeaderIgnite').addClass("active");
    $timeout(function () {
        $(".ignite-header li").removeClass("active");
        $('#video_manger').addClass("active");
    }, 500);
    $scope.status = $routeParams.status;
    $scope.category = $routeParams.category;
    $scope.current_domain = current_domain;
    $scope.publishTypes = [{"value":1,"name":"Published Videos"},{"value":0,"name":"Unpublished Videos"}, {"value":3,"name":"Drafts"}];
    /* old version : start */
    /*adminignitefactory.Getallbuzzcategories().then(function (response) {
        if (response.data.status == 1) {
            $scope.buzzTypes = response.data.data;
        }
    });*/
    // $scope.buzzTypes      = [{"value":1,"name":"Buzz 1"},{"value":2,"name":"Buzz 2"}, {"value":3,"name":"Buzz 3"}];
    /*$timeout(function () {
        if($scope.status==undefined && $scope.category==undefined){
            $scope.type           = $scope.publishTypes[0].value;
            // $scope.buzzType       = $scope.buzzTypes[0].value;
            $scope.buzzType       = $scope.buzzTypes[0].id;
        } else {
            $scope.type           = $scope.status;
            $scope.buzzType       = $scope.category;
        }
        $scope.GetAllVideoData({ 'type': $scope.type,'video_category_id':$scope.buzzType }, 1);
    }, 1000);
    $scope.GetAllVideoData = function (data, pageNumber) {
        adminignitefactory.GetAllVideoData(data, pageNumber).then(function (response) {
            if (response.data.status == 1) {
                $scope.allVideoContent = response.data.data.data;
                $scope.totalVideo = response.data.data.total;
                $scope.paginate = response.data.data;
                $scope.currentVideoPaging = response.data.data.current_page;
                $scope.rangeVideopage = _.range(1, response.data.data.last_page + 1);     
                $("#PublishedQuizzesTab").css("display", "block");           
                $(".tablinks").removeClass("active");
                $("#quizType_"+$scope.type).addClass("active");
                $("#buzzType_"+$scope.buzzType).addClass("active");
            }
        });
    }*/
    /*$scope.publishTypes = [{"value":1,"name":"Published Videos"},{"value":0,"name":"Unpublished Videos"}, {"value":3,"name":"Drafts"}];
    adminignitefactory.Getcategories().then(function (response) {
        if (response.data.status == 1) {
            $scope.subjects = response.data.data.categories;
        }
    });
    // $scope.buzzTypes      = [{"value":1,"name":"Buzz 1"},{"value":2,"name":"Buzz 2"}, {"value":3,"name":"Buzz 3"}];
    $timeout(function () {
        if($scope.status==undefined && $scope.category==undefined){
            $scope.type       = $scope.publishTypes[0].value;
            $scope.categories = $scope.subjects[0].categories;
            // $scope.buzzType   = $scope.buzzTypes[0].value;
        } else {
            $scope.type           = $scope.status;
            $scope.buzzType       = $scope.category;
        }
    }, 500);
    $scope.GetAllVideoData = function (data, pageNumber) {
        adminignitefactory.GetAllVideoData(data, pageNumber).then(function (response) {
            if (response.data.status == 1) {
                $scope.allVideoContent = response.data.data.data;
                $scope.totalVideo = response.data.data.total;
                $scope.paginate = response.data.data;
                $scope.currentVideoPaging = response.data.data.current_page;
                $scope.rangeVideopage = _.range(1, response.data.data.last_page + 1);     
                $("#PublishedQuizzesTab").css("display", "block");           
                $(".tablinks").removeClass("active");
                $("#quizType_"+$scope.type).addClass("active");
                $("#buzzType_"+$scope.buzzType).addClass("active");
            }
        });
    }*/
    // $scope.GetAllVideoData({ 'type': $scope.type,'video_category_id':$scope.buzzType }, 1);
    /*$scope.filterData = function (type, page) {
        $scope.type =type;
        $scope.GetAllVideoData({ 'type': $scope.type,'video_category_id':$scope.buzzType }, page);
    }
    // $scope.filterDataBuzz = function (buzz, page) {
    $scope.filterDataBuzz = function (page) {
        $scope.GetAllVideoData({ 'type': $scope.type,'video_category_id':$scope.buzzType }, page);
    }  */
    /* old version : End */

    /* New Version : Start*/
        $scope.getVideos = {};
        adminignitefactory.Getzones().then(function (response) {
            if (response.data.status == 1) {
                $scope.zones = response.data.data.zones;
                $scope.getVideos.type = $scope.publishTypes[0].value;
                $scope.type = $scope.getVideos.type;
                $scope.getVideos.zone = $scope.zones[0].id;
                $scope.getSubjects($scope.getVideos.zone);
                $scope.GetAllVideoData($scope.getVideos, 1);   
            }
        });
        // adminignitefactory.Getcategories().then(function (response) {
        //     if (response.data.status == 1) {
        //         $scope.categories = undefined;
        //         // $scope.subCategories = undefined;
        //         // $scope.subjects = response.data.data.categories;
        //     }
        // });
        $scope.Getcategories = function(subject) {
                adminignitefactory.Loadcategoriesforsubject({'subject_id':subject}).then(function (response) {
                if (response.data.status == 1) {
                    $scope.subCategories = undefined;
                    $scope.categories = response.data.data.categories;
                }
            });
        }
        $scope.Getsubcategories = function(category, subCatOrNot) {
            if(subCatOrNot == 1) {
                $scope.getVideos.subcategory = undefined;
            }
            if(category != null || category != undefined) {
                adminignitefactory.Getsubcategories({'id':category, 'for':'Video'}).then(function (response) {
                    if(response.data.status == 1){
                        $scope.subCategories = response.data.data;
                        $scope.subCategoriesExist = $scope.subCategories.length>0?true:false;
                        $scope.selectSubcategory = true;
                    } else if(response.data.status == 0) {
                        if(response.data.code==2){
                            window.location.replace("manage/login");
                        } else if(response.data.code==308) {
                            if(subCatOrNot != 0) {
                                $scope.subCategoriesExist = false;
                            }
                            $scope.selectSubcategory = false;
                        } else {
                            if(subCatOrNot != 0) {
                                $scope.subCategoriesExist = false;
                            }
                            simpleAlert('error', '', response.data.message);
                        }
                    }
                });
            }
        }
        $scope.selectSubcategory = true;
        $scope.GetAllVideoData = function (data, pageNumber) {
            adminignitefactory.GetAllVideoData(data, pageNumber).then(function (response) {
                if (response.data.status == 1) {
                    $scope.allVideoContent = response.data.data.data;
                    $scope.totalVideo = response.data.data.total;
                    $scope.paginate = response.data.data;
                    $scope.currentVideoPaging = response.data.data.current_page;
                    $scope.rangeVideopage = _.range(1, response.data.data.last_page + 1);     
                    $("#PublishedQuizzesTab").css("display", "block");           
                    $(".tablinks").removeClass("active");
                    $("#quizType_"+$scope.type).addClass("active");
                    $("#buzzType_"+$scope.buzzType).addClass("active");
                }
            });
        }
        $scope.GetVideos = function(getVideos){
            if($scope.status==undefined){
                getVideos.type = $scope.publishTypes[0].value;
                $scope.type = $scope.publishTypes[0].value;
            }
            getVideos.subCategoryExists = $scope.selectSubcategory;
            $scope.GetAllVideoData(getVideos, 1);
        }
        $scope.filterData = function (type, page) {
            $scope.type = type;
            $scope.getVideos.type = type; 
            $scope.GetAllVideoData($scope.getVideos, page);
        }
    /* New Version : End */

    $scope.ShowVideo = function(data){        
        $('#showVideoModal').modal('show');
        // var videoUrl = data.video_urls[0].video_oss_url;
        var videoUrl = data;
        if(Hls.isSupported()){         
            var video = document.getElementById('videoTag');
            var hls = new Hls();
            hls.loadSource(videoUrl);
            hls.attachMedia(video);
            video.play();
            hls.on(Hls.Events.MANIFEST_PARSED,function()
            {         
                video.play();
            });
        } else if (video.canPlayType('application/vnd.apple.mpegurl')){            
            video.src = videoUrl;
            video.addEventListener('canplay',function()
            {
                video.play();
            });
        }         
    }    
    $scope.getSubjects = function(zone) {
        if(zone != '') {
            adminignitefactory.Getcategories({'zone_id':zone}).then(function (response) {
            if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    $scope.subjects = response.data.data.categories;
                }
            });
        } else {
            $scope.subjects = undefined;
        }
    }
}]);
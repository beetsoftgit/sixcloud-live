/*
 Author  : Ketan Solanki (ketan@creolestudios.com)
 Date    : 14th August 2018
 Name    : WorkSheetInfoController
 Purpose : WorkSheet Information page
 */
angular.module('sixcloudAdminApp').controller('WorkSheetInfoController', ['$scope', 'adminignitefactory', '$routeParams', '$timeout', function ($scope, adminignitefactory, $routeParams, $timeout) {
	$(".navbar-nav li").removeClass("active");
	$('#adminHeaderIgnite').addClass("active");
	$timeout(function () {
		$(".ignite-header li").removeClass("active");
		$('#worksheet_manger').addClass("active");
	}, 500);
    $scope.slug = $routeParams.slug;    
    $scope.workSheetDetails = function(){
        adminignitefactory.GetworkSheetDetails({ 'id': $scope.slug }).then(function (response) {
            if (response.data.status == 1) {
                $scope.workSheetData = response.data.data;
                $scope.workSheetid = response.data.data.id;
            }
        });
    }
    $scope.workSheetDetails();	 
    $scope.downloadWorksheetfile =function(fileName){
        var filePath = BASEURL + "public/uploads/worksheet_files/" + fileName;
        var http = new XMLHttpRequest();         
        http.open('HEAD', filePath, false);
        http.send();
        if (http.status == 200) {
            window.location = 'download-worksheet-file?filename=' + fileName;
        } else {
             simpleAlert('error', 'Some Error Occured', 'File Not Found', true);
        }       
    };

    function redirectToWorksheetManager(){
        window.location.replace("manage/ignite/worksheet-manager/"+$scope.workSheetData.status+"/"+$scope.workSheetData.worksheet_category_id);
    }

    function deleteWorksheet(){
        adminignitefactory.Deleteworksheet({ 'id': $scope.worksheetId,'status':$scope.status}).then(function(response) {                                                         
            if (response.data.status == 1) {                                
                confirmDialogueAlert("Success",response.data.message,"success",false,'#8972f0','Ok','',true,'','',redirectToWorksheetManager);
            } 
        });
    }
    $scope.deleteWorksheet = function(id,status){       
        $scope.worksheetId = id;
        $scope.status = status;
        if(status==4)
            confirmDialoguePublish("Are you sure you want to Delete this worksheet ?", "Yes, Delete it", '', deleteWorksheet);
        else
            confirmDialoguePublish("Are you sure you want to Un-publish this worksheet ?", "Yes", '', deleteWorksheet);
    }
}]);
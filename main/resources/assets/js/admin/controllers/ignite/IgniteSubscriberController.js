/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 5th Sept. 2018
 Name    : IgniteSubscriberController
 Purpose : All the functions for ignite subscribers page
 */
angular.module('sixcloudAdminApp').controller('IgniteSubscriberController', ['$http', '$scope', '$timeout', '$rootScope', '$filter', 'adminignitefactory', function ($http, $scope, $timeout, $rootScope, $filter, adminignitefactory) {
    $(".navbar-nav li").removeClass("active");
    $('#adminHeaderIgnite').addClass("active");
    $timeout(function () {
        $(".ignite-header li").removeClass("active");
        $('#ignite_subscribers').addClass("active");
    }, 500);
    $( function() {
        $( "#created_from" ).datetimepicker({
            pickTime: false,
            format: 'YYYY/MM/DD',
            maxDate: new Date(),
        });
        $( "#created_to" ).datetimepicker({
            pickTime: false,
            format: 'YYYY/MM/DD',
            maxDate: new Date(),
        });
    });
    $scope.search = '';
    $scope.filterData = function (phone_number,email_address,created_from,created_to,type,elementId,buttonText) {
        if(type){
            $scope.ExportToExcel({         
                'phone_number': phone_number,
                'email_address': email_address,
                'created_from': created_from,
                'created_to': created_to,
                'type': type,
                'elementId': elementId,
                'buttonText': buttonText
            });
        }else{
            $scope.GetallSubscriberData({         
                'phone_number': phone_number,
                'email_address':email_address,
                'created_from': created_from,
                'created_to': created_to
            }, 1);
        }
    }
    $scope.ExportToExcel = function(data) {
        showLoader("#"+data.elementId);
        let url = window.location.href;
        let urlData = url.split( '/' );
        adminignitefactory.ExportToExcel(data).then(function (response) {
            let date = new Date();
            let month = date.getMonth()+1;
            hideLoader("#"+data.elementId, data.buttonText);
            var a = document.createElement('a');
            // a.target="_blank";
            a.download="Subscribers List_"+date.getFullYear()+'_'+month+'_'+date.getDate()+'.'+data.type;
            a.href='https://sixclouds.net/main/public/reports/Subscription_user_list.xlsx';
            if (location.hostname === "localhost"){
                a.href='http://localhost/sixclouds-web/main/'+response.data.full;
            }else{
                if(urlData.includes("beta")){
                    a.href='https://sixclouds.net/beta/sixclouds-sub'+response.data.full;  
                }else{
                    a.href='https://sixclouds.net/main/'+response.data.full;  
                }
            }
            a.click();
        });
    }

    $scope.GetallSubscriberData = function (data, pageNumber) {
        adminignitefactory.GetallSubscriberData(data, pageNumber).then(function (response) {

            if(response.data.status == 1){
                $scope.allSubscribers = response.data.data.data;
                $scope.totalDistributors = response.data.data.total;
                $scope.paginate = response.data.data;
                $scope.currentDistributorsPaging = response.data.data.current_page;
                $scope.rangeDistributorspage = _.range(1, response.data.data.last_page + 1);
            }else if(response.data.status == 0){
                $scope.allSubscribers = response.data.data;
                $scope.totalDistributors = 0;
                $scope.paginate = {};
                $scope.currentDistributorsPaging = 0;
                $scope.rangeDistributorspage = _.range(1, 1);
            }            
        });
    }
    $scope.GetallSubscriberData({ 'phone_number':$scope.phone_number,'email_address': $scope.email_address,'created_from': $scope.created_from,'created_to': $scope.created_to}, 1);
}]);
/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 13th August 2018
 Name    : EditVideoController
 Purpose : realted to creating new video
 */
angular.module('sixcloudAdminApp').controller('EditVideoController', [ '$http', 'adminignitefactory', '$routeParams', '$scope', '$timeout', '$rootScope', '$filter', function( $http, adminignitefactory, $routeParams, $scope, $timeout, $rootScope, $filter) {
    console.log('EditVideo controller loaded');
    $timeout(function () {
        $(".ignite-header li").removeClass("active");
        $('#video_manger').addClass("active");
        $scope.selectedZones = [];
        $.each($('input[name="video_for_zones[]"]:checked'), function(){            
            $scope.selectedZones.push($(this).val());
        });
    }, 500);

    $scope.params = $routeParams;   
    adminignitefactory.GetVideoDetails({ 'id': $scope.params.slug,'isEdit':true }).then(function (response) {
        if (response.data.status == 1) {
            $scope.videoData = response.data.data;
            $scope.getSubjects($scope.videoData.selectedZonesCount[0]);
            $scope.Loadcategoriesforsubject($scope.videoData.subject_id);
            $scope.Loadprerequisites($scope.videoData.video_category_id);
            $scope.video_ordering = $scope.videoData.video_ordering;
            $scope.applies = {};
            $scope.applies.applicableToAll = true;
            var videoUrl = response.data.data.video_urls[0].video_oss_url;
        }
    });
    $scope.NotapplicabletoAll = function(){
        $scope.applicableToAll = false;
    }
    // adminignitefactory.Getcategories().then(function (response) {
    //     if (response.data.code == 2) window.location.replace("sixteen-login");
    //     if (response.data.status == 1) {
    //         $scope.subjects = response.data.data.categories;
    //     }
    // });
    $scope.getSubjects = function(zone_id, count) {
        adminignitefactory.Getcategories({'zone_id':zone_id}).then(function (response) {
            if (response.data.code == 2) window.location.replace("sixteen-login");
            if (response.data.status == 1) {
                if(count == 1) {
                    $scope.videoData.subject_id = undefined;
                    $scope.videoData.video_category_id = undefined;
                    $scope.videoCategories = undefined;
                    $scope.subCategories = undefined;
                    $scope.videoData.subcategory_id = undefined;
                }
                $scope.subjects = response.data.data.categories;
                if($scope.subjects) {
                    $scope.subjects.forEach(function(e){
                        e.zones = e.zones.split(',');
                        // var index = e.zones.indexOf(zone_id);
                        // if (index !== -1) e.zones.splice(index, 1);
                        // e.otherZones = e.zones;
                        var filtered = e.zones.filter(function(value){
                            return value != zone_id;
                        });
                        e.otherZones = filtered;
                    });
                }
            }
        });
    }

    $scope.loadVideoInput = function(){
        $timeout(function(){
            document.querySelector("html").classList.add('js');

            var fileInput = document.querySelector(".input-file"),
                button = document.querySelector(".input-file-trigger"),
                the_return = document.querySelector(".file-return");

            button.addEventListener("keydown", function (event) {
                if (event.keyCode == 13 || event.keyCode == 32) {
                    fileInput.focus();
                }
            });
            button.addEventListener("click", function (event) {
                fileInput.focus();
                return false;
            });
            fileInput.addEventListener("change", function (event) {
                var filename = this.value.split('\\');
                filename = filename.pop();
            });
        },1500);
    }

    $scope.Loadcategoriesforsubject = function(subjectId){
        $scope.subCategories =  undefined;
        $scope.subCategoriesPresent = false;
        // $scope.video_ordering =  undefined;
        adminignitefactory.Loadcategoriesforsubject({ 'subject_id': subjectId }).then(function (response) {
            if (response.data.code == 2) window.location.replace("sixteen-login");
            if (response.data.status == 1) {
                $scope.videoCategories = response.data.data.categories;
                $scope.videos = {};
                let count;
                for (var i = 0; i < response.data.data.languages.length; i++) {
                    count = 0;
                    for(var j = 0; j < $scope.videoData.video_urls.length; j++) {
                        if(response.data.data.languages[i].id == $scope.videoData.video_urls[j].video_language_id) {
                            count++;
                        }
                    }
                    if(count == 0) {
                        $scope.videos[i] = response.data.data.languages[i];
                    } 
                }
                var isEmptyVideos = (Object.keys($scope.videos).length === 0 && $scope.videos.constructor === Object);
                if($scope.videos != undefined && $scope.videos != null && $scope.videos.length != $scope.videoData.video_urls.length && ! isEmptyVideos) {
                    $scope.showVideoDiv = true;
                }
                $scope.loadVideoInput();
            }
        });
    }

    $scope.prerequisitesCounter = 1;
    $scope.showMoreDisable = false;
    $scope.Loadprerequisites = function (id) {
        adminignitefactory.Loadprerequisites({ 'video_category_id': id }).then(function (response) {
            if (response.data.code == 2) window.location.replace("sixteen-login");
            if (response.data.status == 1) {
                if(response.data.data.yesSubcategories){
                    $scope.subCategoriesPresent = true;
                    $scope.subCategories = response.data.data.yesSubcategories.subCategories;
                } else {
                $scope.subCategories =  undefined;
                $scope.prerequisitesCounter = 1;
                $scope.prerequisitesQuizCounter = 1;
                $('#prerequisitesDiv').empty();
                $('#preRequisitesQuizesDiv').empty();
                $scope.video_ordering = response.data.data.noSubcategories.lastSequence;
                $scope.preRequisites = response.data.data.noSubcategories.preRequisites;
                if ($scope.preRequisites.length > 1 && $(".prerequisites_parent select").length<$scope.preRequisites.length) {
                    $scope.showMoreDisable = true;
                    $scope.showMoreSelect = true;
                } else {
                    $scope.showMoreDisable = false;
                    $scope.showMoreSelect = false;
                }
                $scope.preRequisitesBackup = response.data.data.noSubcategories.preRequisites;
                $scope.quiz_ordering = response.data.data.noSubcategories.lastSequence;
                $scope.preRequisitesQuizes = response.data.data.noSubcategories.preRequisites;
                if ($scope.preRequisitesQuizes.length > 1) {
                    $scope.showMoreDisableQuiz = true;
                } else {
                    $scope.showMoreDisableQuiz = false;
                }
                $scope.worksheet_ordering = response.data.data.noSubcategories.lastSequence;
                return false;
                }
            }
        });
    }

    $scope.Addmoreprerequisites = function () {
        category = $('input[name=video_category_id]:checked').val();
        $scope.prerequisitesCounter = $(".prerequisites_parent select").length;
        if ($scope.prerequisitesCounter < $scope.preRequisites.length) {
            if (category != undefined) {
                $scope.showMoreDisable = true;
                // var copy = $("#prerequisites_" + $scope.prerequisitesCounter).clone(true);
                var copy = $("#prerequisitesid").clone(true);
                $scope.prerequisitesCounter = $scope.prerequisitesCounter + 1;
                $('#prerequisitesDiv').append(copy);
                $timeout(function () {
                    // $("#prerequisites_" + $scope.prerequisitesCounter).addClass('upper-space');
                    $("#prerequisitesid").addClass('upper-space');
                }, 100);

            } else {
                if (category == undefined) {
                    simpleAlert('error', '', 'Select video category');
                }
                $scope.showMoreDisable = false;
            }
        } else {
            $scope.showMoreDisable = false;
        }
    }

    $scope.showVideoDiv = false;
    $(document).ready(function() {
        $('.languageselect').multiselect();
    });
    $(document).on('change', '.input-file', function(event) {
            var fileinputid = $(this).attr('id');
            fileinputid = fileinputid.split('_');
            whichFile = fileinputid[0];
            fileinputid = fileinputid.pop();
            if(this.value.length) {
                var filename = this.value.split('\\');
                filename = filename.pop();
                if(whichFile=='old')
                    $('#filereturn_'+fileinputid).html(filename);
                else
                    $('#newfilereturn_'+fileinputid).html(filename);
            }
            else {
                if(whichFile=='old')
                    $('#filereturn_'+fileinputid).html('Drop video file here');
                else
                    $('#newfilereturn_'+fileinputid).html('Drop video file here');
            }
            
        });

    // $scope.selectedZones = [];
    $scope.Addvideolanguages = function(){
        $scope.selectedZones1 = [];
        $.each($('input[name="video_for_zones[]"]:checked'), function(){            
            $scope.selectedZones1.push($(this).val());
        });
        if($scope.selectedZones1.length==0){
            simpleAlert('error', '', 'Select Zone(s)');
            return;
        } else {
            adminignitefactory.Loadlanguagesforzones({ 'selected_zones': $scope.selectedZones1,'isEdit':true,'removeVideo':$scope.removeVideo,'existingVideos':$scope.videoData.video_urls }).then(function (response) {
                if (response.data.code == 2) window.location.replace("sixteen-login");
                if (response.data.status == 1) {
                    $scope.videos = response.data.data;
                    // $scope.loadVideoInput();
                    $scope.showVideoDiv = true;
                }
            });
        }
    }

    $scope.Uploadnewvideo = function (status, button) {
        // console.log($scope.title); return;
        // if ($scope.title == undefined || $scope.description == undefined || $scope.video_ordering == undefined || $scope.title == '' || $scope.description == '' || $scope.video_ordering == '') {
        if ($scope.video_ordering == undefined|| $scope.video_ordering == '') {
            /*if ($scope.title == undefined||$scope.title == '') {
                simpleAlert('error', '', 'Video "title" is required');
                return;
            }
            if ($scope.description == undefined||$scope.description == '') {
                simpleAlert('error', '', 'Video "description" is required');
                return;
            }*/

            if ($scope.video_ordering == undefined || $scope.video_ordering == '') {
                simpleAlert('error', '', 'Video position is required');
                return;
            }
        } else {
            editVideo = new FormData($("#editVideoForm")[0]);
            (button=='draft'?showLoader(".draft"):showLoader(".publish"));
            editVideo.append('status',status);
            editVideo.append('old_data',JSON.stringify($scope.videoData));
            editVideo.append('video_ordering',$scope.video_ordering);
            adminignitefactory.Editvideo(editVideo).then(function (response) {
                (button=='draft'?hideLoader(".draft", 'Save as Draft'):hideLoader(".publish", 'Publish'));
                if (response.data.code == 2) window.location.replace("sixteen-login");
                if (response.data.status == 1) {
                    window.location.replace("manage/ignite/video-manager/"+$scope.videoData.status+"/"+$scope.videoData.video_category_id);
                } else {
                    simpleAlert('error','',response.data.message);
                }
            });
        }
    }
    $scope.removeVideo = [];
    function removeExistingVideoMethod(){
        $('<input>').attr({type: 'hidden',name: 'removeVideo[]', value:''+$scope.existingVideoId+''}).appendTo("#editVideoForm");
        $scope.removeVideo.push($scope.removingLanguage);
        $scope.videoData.video_urls.splice($scope.existingVideoIndex,1);
        $scope.$apply();
    }
    $scope.removeExistingVideo = function(id,index,language){
        $scope.existingVideoId = id;
        $scope.existingVideoIndex = index;
        $scope.removingLanguage = language;
        confirmDialogue('','','',removeExistingVideoMethod);
    }
    $(document).on('click', '#video_thumbnail_select', function(event) {
        $('#video_thumbnail').click();
    });
        $('#video_thumbnail').change(function() {
            if(event.target.files.length>0){
                if(event.target.files[0].type == 'image/jpeg' || event.target.files[0].type == 'image/png'||event.target.files[0].type == 'image/jpg')
                    readURL(this);
                else{
                    simpleAlert('error', '', 'Only jpg, png and jpeg file formats are allowed for "Video Thumbnail"');
                }
            } else {
                $('#video_thumbnail_select').css('background-image', "url(resources/assets/images/up-img.png)");
                $('#video_thumbnail_select').removeClass('quiz_image_option');
                $('#video_thumbnail').val('');
            }
        });

    function readURL(input) {
        if (input.files && input.files[0]&&input.files.length>0) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#video_thumbnail_select').css('background-image', "url("+e.target.result+")");
                $('#video_thumbnail_select').addClass('quiz_image_option');
            }
            reader.readAsDataURL(input.files[0]);
        } else {
            $('#video_thumbnail_select').css('background-image', "url(resources/assets/images/up-img.png)");
            $('#video_thumbnail_select').removeClass('quiz_image_option');
            $('#video_thumbnail').val('');
        }
    }
}]);
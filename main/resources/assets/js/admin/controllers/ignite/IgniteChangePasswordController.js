/*
 Author  : Ketan Solanki (ketan@creolestudios.com)
 Date    : 19th July 2017
 Name    : IgniteAddDistributorController
 Purpose : All the functions for adding ignite distributor
 */
angular.module('sixcloudAdminApp').controller('IgniteChangePasswordController', ['$http', '$scope', '$timeout', '$rootScope', '$filter', 'adminignitefactory', function ($http, $scope, $timeout, $rootScope, $filter, adminignitefactory) {    
    $(".navbar-nav li").removeClass("active");
    $('#adminHeaderIgnite').addClass("active");
    $timeout(function () {
        $(".ignite-header li").removeClass("active");
        // $('#ignite_distributors').addClass("active");
    }, 500);  
    console.log("IgniteChangePasswordController loaded");

    $scope.Passwordupdate = function(){
        $scope.language = Cookies.get('language');
            if($scope.language=='en'){
                var current_password_required = 'Please enter Current Password';
                var new_password_required     = 'Please enter New Password';
                var confirm_password_required = 'Please retype Password';
                var new_confirm_same          = 'New Password and Confirm Password should be the same';
                var new_current_same          = 'New Password and Current Password should not be the same';
                var regex_validation          = 'Please use a password which comprises of 8 to 16 characters, with at least 1 uppercase letter, 1 lowercase letter and 1 number.';
                var update_btn                = 'Update';
            } else {
                var current_password_required = '请输入当前密码';
                var new_password_required     = '请输入新密码';
                var confirm_password_required = '请确认密码';
                var new_confirm_same          = '新密码和旧密码不应该相同。';
                var new_current_same          = '新密码和旧密码不应该相同。';
                var regex_validation          = '请使用由8到16个字符组成的密码，其中至少有1个大写字母、1个小写字母和1个数字。';
                var update_btn                = '确认';
            }
        if($scope.old_password == undefined||$scope.old_password == ''|| $scope.new_password == undefined||$scope.new_password == ''|| $scope.confirm_password == undefined||$scope.confirm_password == ''||$scope.new_password_expression_error == true||$scope.confirm_password != $scope.new_password){
            if($scope.old_password == undefined||$scope.old_password == ''){
                    simpleAlert('error', '', current_password_required);
                return;
            }
            if($scope.new_password == undefined||$scope.new_password == ''){
                if($scope.new_password == undefined||$scope.new_password == '')
                    simpleAlert('error', '', new_password_required);
                return;
            }
            if($scope.new_password_expression_error == true){
                simpleAlert('error', '', regex_validation);
                return;
            }
            if($scope.confirm_password == undefined||$scope.confirm_password == ''){
                simpleAlert('error', '', confirm_password_required);
                return;
            }
            if($scope.confirm_password != $scope.new_password){
                simpleAlert('error', '', new_confirm_same);
                return;
            }
        } else {
            if($scope.new_password == $scope.old_password) {
                simpleAlert('error', '', new_current_same);
                return;
            }
            if($scope.new_password.length >= 8){
                showLoader('.password_update');
                var password                 = {};
                password['old_password']     = $scope.old_password;
                password['new_password']     = $scope.new_password;
                password['confirm_password'] = $scope.confirm_password;
                password['language'] = $scope.language;
                adminignitefactory.Updateadminpassword(password).then(function(response){
                    hideLoader('.password_update',update_btn);
                    if(response.data.status==1)
                    {
                        simpleAlert('success', '', response.data.message,false);
                        $timeout(function(){
                            window.location.reload();
                        },1000);
                    }
                    if(response.data.status==0)
                    {
                        simpleAlert('error','',response.data.message);
                    }
                });
            } else {
                simpleAlert('error', '', regex_validation);
            }
        }
    }

    var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,16})");
    $scope.Checkexpression = function(password,which){
        if(strongRegex.test(password)) {
                $scope.new_password_expression_error = false;
        } else {
                $scope.new_password_expression_error = true;
        }
    }
    $scope.Checkcnfpassword = function(password){
        if($scope.confirm_password != $scope.new_password){
            $scope.confirm_password_error_no_match = true;
            $scope.confirm_password_error = false;
        } else {
            $scope.confirm_password_error_no_match = false;
            $scope.confirm_password_error = false;
        }
    }
}]);

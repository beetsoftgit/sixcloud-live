/*
 Author  : Ketan Solanki (ketan@creolestudios.com)
 Date    : 19th July 2017
 Name    : IgniteAddDistributorController
 Purpose : All the functions for adding ignite distributor
 */
angular.module('sixcloudAdminApp').controller('IgniteAddDistributorController', ['$http', '$scope', '$timeout', '$rootScope', '$filter', 'adminignitefactory', function ($http, $scope, $timeout, $rootScope, $filter, adminignitefactory) {    
    $(".navbar-nav li").removeClass("active");
    $('#adminHeaderIgnite').addClass("active");
    $timeout(function () {
        $(".ignite-header li").removeClass("active");
        $('#ignite_distributors').addClass("active");
    }, 500);  
    adminignitefactory.Getcountry().then(function (response) {
        if (response.data.status == 1) {
            $scope.countrycitydata = response.data.data;
        }
    });
    $('#country_id').on('change', function () {
        var id = $(this).val();
        adminignitefactory.Getstatesforcountry({ 'id': id }).then(function (response) {
            if (response.data.status == 1) {
                $scope.states = response.data.data;
            }
        });
    });
    $(document).on('change', '#state_id', function () {
        var id = $(this).val();
        adminignitefactory.Getcitiesforstate({ 'id': id }).then(function (response) {
            if (response.data.status == 1) {
                $scope.cities = response.data.data;
            }
        });
    });    
    
    var countDecimals = function(value) {
        if (Math.floor(value) !== value)
        return value.toString().split(".")[1].length || 0;
        return 0;
    }
    $scope.price = {};
    $scope.addNewDistributor = function () {
        if ($scope.company_name == undefined || $scope.email_address == undefined || $scope.country_id == undefined || $scope.state_id == undefined || $scope.city_id == undefined || $scope.locality == undefined || $scope.commission_percentage == undefined || $scope.commission_percentage > 100 || $scope.renewal_commission_percentage == undefined || $scope.renewal_commission_percentage > 100) {           

            if ($scope.company_name == undefined) {
                simpleAlert('success', '', 'Company Name is required');
                return;
            }
            /*if ($scope.last_name == undefined) {
                simpleAlert('success', '', 'Last Name is required');
                return;
            }*/
            if ($scope.email_address == undefined) {
                simpleAlert('success', '', 'Email Address is required');
                return;
            }
            if ($scope.commission_percentage == undefined) {
                simpleAlert('success', '', 'Commission Percentage is required');
                return;
            }
            if ($scope.commission_percentage > 100) {
                simpleAlert('success', '', 'Commission Percentage should be less then 100');
                return;
            }  
            var count = countDecimals($scope.commission_percentage);    
            if (count > 2) {
                simpleAlert('success', '', 'Commission Percentage allows only 2 decimals');
                return;
            }
            if ($scope.renewal_commission_percentage == undefined) {
                simpleAlert('success', '', 'Renewal Commission Percentage is required');
                return;
            }
            if ($scope.renewal_commission_percentage > 100) {
                simpleAlert('success', '', 'Renewal Commission Percentage should be less then 100');
                return;
            }  
            var count = countDecimals($scope.renewal_commission_percentage);    
            if (count > 2) {
                simpleAlert('success', '', 'Renewal Commission Percentage allows only 2 decimals');
                return;
            }  
            if ($scope.country_id == undefined) {
                simpleAlert('success', '', 'Country is required');
                return;
            }
            if ($scope.state_id == undefined) {
                simpleAlert('success', '', 'Province is required');
                return;
            }
            if ($scope.city_id == undefined) {
                simpleAlert('success', '', 'City is required');
                return;
            }
            if ($scope.locality == undefined) {
                simpleAlert('success', '', 'Locality is required');
                return;
            }                      
        } else {
            if($scope.discount_type==0||$scope.discount_type==1){
                console.log($scope.price.discount_of);
                if($scope.price.discount_of==undefined||$scope.price.discount_of==''){
                    simpleAlert('error', '', 'Discount of is required');
                    return;
                }
            }
            newDistributor = new FormData($("#newDistributorForm")[0]);
            showLoader(".add-new-distributor");
            adminignitefactory.addNewDistributor(newDistributor).then(function (response) {                
                hideLoader(".add-new-distributor", 'Submit');
                if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    window.location.replace("manage/ignite/distributors");
                } else {
                    simpleAlert('error','',response.data.message);
                }
            });
        }
    }
}]);

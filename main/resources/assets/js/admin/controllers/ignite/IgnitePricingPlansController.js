/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 30th Oct 2018
 Name    : IgnitePricingPlansController
 Purpose : All the functions for home page
 */
angular.module('sixcloudAdminApp').controller('IgnitePricingPlansController', ['adminignitefactory','$http', '$scope', '$timeout', '$rootScope', '$filter', function(adminignitefactory,$http, $scope, $timeout, $rootScope, $filter) {
    // $scope.sections = Geteltsections.data.data;
    console.log('IgnitePricingPlans controller loaded');
    $(".navbar-nav li").removeClass("active");
    $('#adminHeaderIgnite').addClass("active");
    $timeout(function(){
        $(".ignite-header li").removeClass("active");
        $('#ignite_price_plan').addClass("active");
    },500);
    $scope.current_domain = current_domain;

    $scope.GetData = function(){
        adminignitefactory.Getallbuzzcategories().then(function (response) {
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {
                $scope.categories = response.data.data;
            } else {
                simpleAlert('error','',response.data.message);
            }
        });
    }
    $scope.GetData();
    $scope.Loaddata = function(category){
        if(category!=''&&category!=undefined){
            category                              = JSON.parse(category); 
            $scope.categoryData                   = {};
            $scope.categoryData.description       = category.description;
            $scope.categoryData.description_chi   = category.description_chi;
            $scope.categoryData.category_price    = category.category_price;
            $scope.categoryData.promotions        = category.promotions;
            $scope.categoryData.promotional_price = category.promotional_price;
            $scope.categoryData.category_id       = category.id;
            $scope.showPromotionBox               = ($scope.categoryData.promotions == 1?true:false);
            $scope.promo_code                     = category.promo_code;
        } else {
            $scope.categoryData = {};
        }
    }
    $(document).ready(function() {
        $("#category_price").on("keypress keyup blur", function(event) {
            //this.value = this.value.replace(/[^0-9\.]/g,'');
            $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
            $scope.project_budget_error = false;
        });
    });

    $scope.Savecategory = function(details){
        if(details==undefined){
            simpleAlert('error', '', 'Select Category.');
            return;
        }
        if(details.description==undefined||details.description==''||details.description_chi==undefined||details.description_chi==''||details.category_price==undefined||details.category_price==''||details.promotions==undefined||details.promotions=='') {
            if(details.description==undefined||details.description==''){
                simpleAlert('error', '', '"English Description" is required.');
                return;
            }
            if(details.description_chi==undefined||details.description_chi==''){
                simpleAlert('error', '', '"Chinese Description" is required.');
                return;
            }
            if(details.category_price==undefined||details.category_price==''){
                simpleAlert('error', '', '"Category Price" is required.');
                return;
            }
            if(details.promotion==undefined||details.promotion==''){
                simpleAlert('error', '', 'Select promotions.');
                return;
            }
        } else {
            if(details.promotions==1){
                if(details.promotional_price==''||details.promotional_price==undefined){
                    simpleAlert('error', '', '"Promotional Price" is required.');
                    return;       
                }
                if(details.promotional_price<=0){
                    simpleAlert('error', '', '"Promotional Price" cannot be 0.');
                    return;       
                }
            }
            showLoader('.saveCategory');
            adminignitefactory.Savecategory(details).then(function (response) {
                if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    hideLoader('.saveCategory','Save');
                    $scope.GetData();
                    $scope.showPromotionBox = false;
                    simpleAlert('success', '', response.data.message);
                } else {
                    hideLoader('.saveCategory','Save');
                    simpleAlert('error', '', response.data.message);
                }
            });
        }
    }
    $scope.showPromotionBox = false;
    $scope.Checkpromotion = function(promotion){
        $scope.showPromotionBox = (promotion==1?true:false);
        $timeout(function(){
            $("#promotional_price").on("keypress keyup blur", function(event) {
                $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
                if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                    event.preventDefault();
                }
                $scope.project_budget_error = false;
            });
        },500);
    }
    $scope.Savepromo = function(promo){
        if(promo==undefined||promo==''){
            simpleAlert('error', '', 'Enter Promo Code.');
                return;
        } else {
            showLoader('.savePromo');
            adminignitefactory.Savepromo({'promo':promo}).then(function (response) {
                if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    hideLoader('.savePromo','Save');
                    simpleAlert('success', '', response.data.message);
                } else {
                    hideLoader('.savePromo','Save');
                    simpleAlert('error', '', response.data.message);
                }
            });
        }
    }
}]);
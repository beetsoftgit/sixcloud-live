/*
 Author  : Ketan Solanki (ketan@creolestudios.com)
 Date    : 24th July 2018
 Name    : QuizInfoController
 Purpose : Quiz Information page
 */
angular.module('sixcloudAdminApp').controller('VideoDetailsController', ['$scope', 'adminignitefactory', '$routeParams', '$timeout', function ($scope, adminignitefactory, $routeParams, $timeout) {
	$(".navbar-nav li").removeClass("active");
	$('#adminHeaderIgnite').addClass("active");
	$timeout(function () {
		$(".ignite-header li").removeClass("active");
		$('#video_manger').addClass("active");
	}, 500);

	$scope.params = $routeParams;
	console.log($scope.params.slug, '$scope.params.slug');
	adminignitefactory.GetVideoDetails({ 'slug': $scope.params.slug }).then(function (response) {
		if (response.data.status == 1) {
			$scope.videoData = response.data.data;
			// var videoUrl = response.data.data.video_urls[0].video_oss_url;
			var videoUrl = response.data.data.video_oss_url;
			$timeout(function () {
				if(Hls.isSupported()){         
					var video = document.getElementById('videoTag');
					var hls = new Hls();
					hls.loadSource(videoUrl);
					hls.attachMedia(video);
					video.play();
					hls.on(Hls.Events.MANIFEST_PARSED,function()
					{         
						video.play();
					});
				} else if (video.canPlayType('application/vnd.apple.mpegurl')){            
					video.src = videoUrl;
					video.addEventListener('canplay',function()
					{
						video.play();
					});
				} 		
			}, 500);
		}
	});
	function redirectToAllContent(){
			window.location.replace("manage/ignite/video-manager/"+$scope.videoData.status+"/"+$scope.videoData.video_category_id);
	}	

	function publishVideo(){		
        adminignitefactory.PublishVideo({ 'id': $scope.videoId}).then(function(response) {                                	         	        	
            if (response.data.status == "1") {                                
                confirmDialogueAlert("Success",response.data.message,"success",false,'#8972f0','Ok','',true,'','',redirectToAllContent);
            } 
        });
    }

    function unpublishVideo(){
        adminignitefactory.UnPublishVideo({ 'id': $scope.videoId}).then(function(response) {                                	         	
            if (response.data.status == 1) {                
                confirmDialogueAlert("Success",response.data.message,"success",false,'#8972f0','Ok','',true,'','',redirectToAllContent);
            } 
        });
    }

    function deleteVideo(){
        adminignitefactory.DeleteVideo({ 'id': $scope.videoId}).then(function(response) {                                	         	        	
            if (response.data.status == 1) {                                
                confirmDialogueAlert("Success",response.data.message,"success",false,'#8972f0','Ok','',true,'','',redirectToAllContent);
            } 
        });
    }

	$scope.publishVideo = function(id){				
		$scope.videoId = id;		
		confirmDialoguePublish("Are you sure you want to Publish this video ?", "Yes, Publish it", '', publishVideo);
	}
	$scope.unpublishVideo = function(id){		
		$scope.videoId = id;
		confirmDialoguePublish("Are you sure you want to Un Publish this video ?", "Yes, Un Publish it", '', unpublishVideo);		
	}
	$scope.deleteVideo = function(id){		
		$scope.videoId = id;
		confirmDialoguePublish("Are you sure you want to Delete this video ?", "Yes, Delete it", '', deleteVideo);		
	}
}]);
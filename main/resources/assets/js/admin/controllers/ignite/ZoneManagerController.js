/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 18th Jan 2018
 Name    : ZoneManagerController
 Purpose : realted to creating/updating Zones
 */
angular.module('sixcloudAdminApp').controller('ZoneManagerController', [ '$http', 'adminignitefactory', '$scope', '$timeout', '$rootScope', '$filter', function( $http, adminignitefactory, $scope, $timeout, $rootScope, $filter) {
    console.log('ZoneManager controller loaded');
    $(".navbar-nav li").removeClass("active");
    $('#adminHeaderIgnite').addClass("active"); 
    $timeout(function () {
        $(".ignite-header li").removeClass("active");
        $('#zone_manger').addClass("active");
    }, 1000);
    $scope.Addnewzone = function(){
        $scope.nextFormNumber = $scope.forms[$scope.forms.length-1]+1;
        if(!isNaN($scope.nextFormNumber))
            $scope.forms.push($scope.nextFormNumber++);
        else
            $scope.forms.push(1);
        $timeout(function(){
            $('.select2-container').css('display','unset');
        },1500);
    }
    $scope.zoneExists = false;
    adminignitefactory.Getcountryforzones().then(function (response) {
        if (response.data.code == 2) window.location.replace("manage/login");
        if (response.data.status == 1) {
            $scope.countries = response.data.data;
        }
    });
    adminignitefactory.Getlanguagesforzones().then(function (response) {
        if (response.data.code == 2) window.location.replace("manage/login");
        if (response.data.status == 1) {
            $scope.languages = response.data.data;
        }
    });
    $scope.getZones = function(){
        adminignitefactory.Getzones().then(function (response) {
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {
                $scope.zones = response.data.data.zones;
                if($scope.zones.length==0){
                    $scope.forms = [1];
                    $timeout(function(){
                        $('.select2-container').css('display','unset');
                    },1500);
                } else {
                    $scope.zoneExists = true;
                    var formCount = $scope.zones.length+1;
                    $scope.forms = [];
                    $scope.zones.forEach(function(element) {
                        element.saveDisable = 1;
                        element.country_id        = element.country_id.split(',');
                        element.allow_subscription = element.allow_subscription;
                        // element.audio_language    = element.audio_language.split(',');
                        // element.subtitle_language = element.subtitle_language.split(',');
                        $timeout(function(){
                            // $('#country_zone_'+element.countries).val(element.country_id).trigger('change');
                            // $('#audio_language_'+element.countries).val(element.audio_language).trigger('change');
                            // $('#subtitle_language_'+element.countries).val(element.subtitle_language).trigger('change');
                            $timeout(function(){
                               $('#country_zone_'+element.countries).val(element.country_id);
                               // console.log($('#country_zone_'+element.countries).val())
                               $('#country_zone_'+element.countries).multiselect("refresh");
                            },800);
                            // $('.select2-container').css('display','unset');
                            $('#country_zone_'+element.countries).multiselect('disable');
                        },1000);
                    });
                    $timeout(function(){
                        $scope.forms.push(formCount);
                        $timeout(function(){
                            $('.select2-container').css('display','unset');
                        },1500);
                    },500);
                }
            }
        });
    }
    $scope.getZones();  
    $scope.Savenewzone = function(formNumber,whatType,zoneEdit=''){
        var zoneForm = new FormData($("#zoneform_"+formNumber)[0]);
        if(whatType==1)
            zoneForm.append('zoneNumber',formNumber.split('_')[1]);
        else
            zoneForm.append('zoneNumber',formNumber);
        zoneForm.append('whatType',whatType);
        adminignitefactory.Savenewzone(zoneForm).then(function (response) {
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {
                location.reload();
                $scope.getZones();
                if(whatType==1)
                    $scope.disables(zoneEdit);
            } else {
                simpleAlert('error', '', response.data.message);
            }
        });
    }
    $scope.Edit = function(zone,whatType,index){
        $('#saveZone_'+zone.countries).removeAttr('disabled');
        $('#zone_name_'+zone.countries).removeAttr('disabled');
        // $('#country_zone_'+zone.countries).removeAttr('disabled');
        $('#country_zone_'+zone.countries).multiselect('enable');
        $('#audio_language_'+zone.countries).removeAttr('disabled');
        $('#subtitle_language_'+zone.countries).removeAttr('disabled');
        $('#cancelZone_'+zone.countries).removeClass('hide');
        $('#editZone_'+zone.countries).addClass('hide');
        $('#allow_subscription_'+zone.countries).removeAttr('disabled');
        $('#notallow_subscription_'+zone.countries).removeAttr('disabled');
    }
    $scope.Cancel = function(zone,whatType,index){
        $scope.getZones();
        $scope.disables(zone);
    }
    $scope.disables = function(zone){
        $('#saveZone_'+zone.countries).attr('disabled','disabled');
        $('#zone_name_'+zone.countries).attr('disabled','disabled');
        // $('#country_zone_'+zone.countries).attr('disabled','disabled');
        $('#country_zone_'+zone.countries).multiselect('disable');
        $('#audio_language_'+zone.countries).attr('disabled','disabled');
        $('#subtitle_language_'+zone.countries).attr('disabled','disabled');
        $('#cancelZone_'+zone.countries).addClass('hide');
        $('#editZone_'+zone.countries).removeClass('hide');
        $('#allow_subscription_'+zone.countries).attr('disabled','disabled');
        $('#notallow_subscription_'+zone.countries).attr('disabled','disabled');
    }
    function deleteZone(){
        adminignitefactory.Deletezone({'zone_id':$scope.deleteId}).then(function (response) {
                if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    window.location.reload();
                }
            }); 
    }
    $scope.Deletezone = function(id,whatType){
        if(whatType==1) {
            $scope.deleteId = id;
            confirmDialogue('Are you sure you want to delete?','','Cancel',deleteZone);
            /*adminignitefactory.Deletezone({'zone_id':id}).then(function (response) {
                if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    $scope.getZones();
                }
            }); */
        } else {
            $scope.forms.splice($scope.forms.indexOf(id),1);
        }
    }
}]);
/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 4th July 2017
 Name    : NewContentController
 Purpose : All the functions for home page
 */
angular.module('sixcloudAdminApp').controller('NewContentController', ['$http', 'adminignitefactory', '$scope', '$timeout', '$rootScope', '$filter', function ($http, adminignitefactory, $scope, $timeout, $rootScope, $filter) {
    // $scope.sections = Geteltsections.data.data;
    console.log('NewContentController controller loaded');
    $(".navbar-nav li").removeClass("active");
    $('#adminHeaderIgnite').addClass("active");
    $timeout(function () {
        $(".ignite-header li").removeClass("active");
        $('#all_content').addClass("active");
    }, 500);


    adminignitefactory.GetVideoListing().then(function (response) {        
        if (response.data.status == 1) {
            $scope.videoList = response.data.data;
        } else {
            $scope.videoList = [];
        }
    });

    $scope.setVideoId = function (id,title) {        
        $("#hidVideoID").val(id);
        $("#hid_video_title").val(title);
    }


    $scope.openCity = function (jobsStatus) {        
        if (jobsStatus == 'NewVideoTab') {
            window.location.replace("manage/ignite/create-new-video");
        } else if(jobsStatus == 'NewWorkSheetTab'){
            window.location.replace("manage/ignite/create-new-worksheet");
        }else if (jobsStatus == 'NewQuizTab'){
            $('#NewQuizTab').show();
            $('#NewVideoTab').hide();
            $('#NewWorkSheetTab').hide();
            $(".tab-div button").removeClass("active");
            $('#quiztab').addClass("active");
        } else if (jobsStatus == 'NewWorkSheetTab'){
            $('#NewWorkSheetTab').show();
            $('#NewVideoTab').hide();
            $('#NewQuizTab').hide();
            $(".tab-div button").removeClass("active");
            $('#workSheettab').addClass("active");
        }
    }
    $timeout(function () {
        $scope.openCity('NewQuizTab');        
    }, 500);

    /*adminignitefactory.Getvideocategories().then(function (response) {
        if (response.data.code == 2) window.location.replace("sixteen-login");
        if (response.data.status == 1) {
            $scope.videoCategories = response.data.data;
        }
    });*/
    adminignitefactory.Getzones().then(function (response) {
        if (response.data.code == 2) window.location.replace("manage/login");
        if (response.data.status == 1) {
            $scope.zones = response.data.data.zones;
        }
    });
    // adminignitefactory.Getcategories().then(function (response) {
    //     if (response.data.code == 2) window.location.replace("manage/login");
    //     if (response.data.status == 1) {
    //         $scope.subjects = response.data.data.categories;
    //     }
    // });
    $scope.selectedZones = [];
    $scope.Addvideolanguages = function(){
        if($scope.selectedZones.length==0){
            simpleAlert('error', '', 'Select Zone(s)');
            return;
        } else {
            adminignitefactory.Loadlanguagesforzones({ 'selected_zones': $scope.selectedZones }).then(function (response) {
                if (response.data.code == 2) window.location.replace("manage/login");
                if (response.data.status == 1) {
                    $scope.worksheetLanguages = response.data.data;
                    console.log($scope.worksheetLanguages);
                }
            });
        }
    }
    $scope.Uploadnewvideo = function () {
        if ($scope.id == undefined || $scope.description == undefined) {
            if ($scope.id == undefined) {
                // simpleAlert('success', '', 'Video "title" is required');
                simpleAlert('success', '', 'Please Select Video');
                return;
            }
            if ($scope.description == undefined) {
                simpleAlert('success', '', 'Video "description" is required');
                return;
            }
            /*if ($scope.video_ordering == undefined) {
                simpleAlert('success', '', 'Video position is required');
                return;
            }*/
        } else {
            newVideo = new FormData($("#newVideoForm")[0]);
            showLoader(".upload-new-video");
            adminignitefactory.Uploadnewvideo(newVideo).then(function (response) {
                hideLoader(".upload-new-video", 'Submit');
                if (response.data.code == 2) window.location.replace("sixteen-login");
                if (response.data.status == 1) {
                    window.location.replace("manage/ignite/all-content");
                } else {
                    simpleAlert('error','',response.data.message);
                }
            });
        }
    }

    $scope.prerequisitesCounter = 1;
    $scope.showMoreDisable = false;
    $scope.Loadprerequisites = function (id) {
        adminignitefactory.Loadprerequisites({ 'video_category_id': id }).then(function (response) {
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {
                if(response.data.data.yesSubcategories){
                    $scope.video_ordering =  undefined;
                    $scope.subCategoriesPresent = true;
                    $scope.subCategories = response.data.data.yesSubcategories.subCategories;
                } else {
                    $scope.subCategories = undefined;
                    $scope.prerequisitesCounter = 1;
                    $scope.prerequisitesQuizCounter = 1;
                    $('#prerequisitesDiv').empty();
                    $('#preRequisitesQuizesDiv').empty();
                    $scope.video_ordering = response.data.data.noSubcategories.lastSequence;
                    $scope.preRequisites = response.data.data.noSubcategories.preRequisites;
                    abc = response.data.data.noSubcategories.preRequisites;
                    if ($scope.preRequisites.length > 1) {
                        $scope.showMoreDisable = true;
                    } else {
                        $scope.showMoreDisable = false;
                    }
                    $scope.preRequisitesBackup = response.data.data.noSubcategories.preRequisites;
                    $scope.quiz_ordering = response.data.data.noSubcategories.lastSequence;
                    $scope.preRequisitesQuizes = response.data.data.noSubcategories.preRequisites;
                    if ($scope.preRequisitesQuizes.length > 1) {
                        $scope.showMoreDisableQuiz = true;
                    } else {
                        $scope.showMoreDisableQuiz = false;
                    }
                    $scope.worksheet_ordering = response.data.data.noSubcategories.lastSequence;
                    return false;
                }
            }
        });
    }
    $scope.Loadcategoriesforsubject = function(subjectId){
        $scope.subCategories =  undefined;
        $scope.subCategoriesPresent = false;
        $scope.video_ordering =  undefined;
        adminignitefactory.Loadcategoriesforsubject({ 'subject_id': subjectId }).then(function (response) {
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {
                $scope.videoCategories = response.data.data.categories;
                $scope.worksheetLanguages = response.data.data.languages;
                if($scope.worksheetLanguages.length>0){
                    $scope.showTitleDescriptionDiv = true;
                }
            }
        });
    }
    /*$scope.Addmoreprerequisites = function () {
        category = $('input[name=video_category_id]:checked').val();
        if ($scope.prerequisitesCounter < $scope.preRequisites.length) {
            if (category != undefined) {
                $scope.showMoreDisable = true;
                var copy = $("#prerequisites_" + $scope.prerequisitesCounter).clone(true);
                selectedCategory = $('#prerequisites_' + $scope.prerequisitesCounter).val();
                // for (var i = $scope.preRequisites.length - 1; i >= 0; i--) {
                //     if(selectedCategory == $scope.preRequisites[i].id){
                //         $scope.preRequisites.splice($scope.preRequisites[i], 1);
                //     }
                // }
                $scope.prerequisitesCounter = $scope.prerequisitesCounter + 1;
                $('#prerequisitesDiv').append(copy);
                $timeout(function () {
                    $("#prerequisites_" + $scope.prerequisitesCounter).addClass('upper-space');
                }, 100);

            } else {
                if (category == undefined) {
                    simpleAlert('error', '', 'Select video category');
                }
                $scope.showMoreDisable = false;
            }
        } else {
            $scope.showMoreDisable = false;
        }
    }*/

    /*$(function(){
        $('.prerequisites').click(function(){
            var id = $(this).attr('id');
            value = $('#'+id).val();
            if(value!=''){
                for (var i = $scope.preRequisites.length - 1; i >= 0; i--) {
                    if(value == $scope.preRequisites[i].id){
                        $scope.preRequisites[i].;
                    }
                }
                console.log(value);
            }
        });
    });*/
    /*$scope.showVideoDiv = false;
    $(document).ready(function() {
        $('.languageselect').multiselect();
    });
    $(document).on('change', '.input-file', function(event) {
            var fileinputid = $(this).attr('id');
            fileinputid = fileinputid.split('_');
            fileinputid = fileinputid.pop();
            if(this.value.length) {
                var filename = this.value.split('\\');
                filename = filename.pop();
                $('#filereturn_'+fileinputid).html(filename);
            }
            else {
                $('#filereturn_'+fileinputid).html('Drop video file here');
            }
            
        });*/
    // $scope.Addvideolanguages = function(){
    //     $scope.videos = undefined;
    //     language = $('#videolanguage').val();
    //     if(!jQuery.isEmptyObject(language)){
    //         $scope.videos = language;
    //         $timeout(function(){
    //             document.querySelector("html").classList.add('js');

    //             var fileInput = document.querySelector(".input-file"),
    //                 button = document.querySelector(".input-file-trigger"),
    //                 the_return = document.querySelector(".file-return");

    //             button.addEventListener("keydown", function (event) {
    //                 if (event.keyCode == 13 || event.keyCode == 32) {
    //                     fileInput.focus();
    //                 }
    //             });
    //             button.addEventListener("click", function (event) {
    //                 fileInput.focus();
    //                 return false;
    //             });
    //             fileInput.addEventListener("change", function (event) {
    //                 var filename = this.value.split('\\');
    //                 filename = filename.pop();
    //                 // the_return.innerHTML = filename;
    //             });
    //         },500);
    //         $scope.showVideoDiv = true;
    //     } else {
    //         $scope.videos = undefined;
    //         simpleAlert('error','','Select language');
    //     }
    // }

    

    //Code done by ketan solanki for Adding new quiz

    adminignitefactory.GetQuizListing().then(function (response) {
        if (response.data.status == 1) {
            $scope.quizList = response.data.data;
        } else {
            $scope.quizList = [];
        }
    });
    $scope.setQuizId = function (id) {
        listData = JSON.parse(id);
        id = listData['id'];
        // if(listData['category_id']!=''&&listData['category_id']!=undefined&&listData['category_id']!=null) {
        //     console.log("#c"+listData['category_id']);
        //     $("#c"+listData['category_id']). prop("checked", true);
        //     $scope.Loadprerequisites(listData['category_id']);
        // } else {
        //     $('.radioCategory').prop('checked', false);
        //     $scope.quiz_ordering = undefined;
        // }
        $("#hidQuizID").val(id);
    }
    $scope.AddmoreprerequisitesQuizzes = function () {
        category = $('input[name=quiz_category_id]:checked').val();
        if ($scope.prerequisitesQuizCounter < $scope.preRequisitesQuizes.length) {
            if (category != undefined) {
                $scope.showMoreDisableQuiz = true;
                var copy = $("#prerequisitesquizes_" + $scope.prerequisitesQuizCounter).clone(true);
                selectedCategory = $('#prerequisitesquizes_' + $scope.prerequisitesQuizCounter).val();
                // for (var i = $scope.preRequisites.length - 1; i >= 0; i--) {
                //     if(selectedCategory == $scope.preRequisites[i].id){
                //         $scope.preRequisites.splice($scope.preRequisites[i], 1);
                //     }
                // }
                $scope.prerequisitesQuizCounter = $scope.prerequisitesQuizCounter + 1;
                $('#preRequisitesQuizesDiv').append(copy);
                $timeout(function () {
                    $("#prerequisitesquizes_" + $scope.prerequisitesQuizCounter).addClass('upper-space');
                }, 100);

            } else {
                if (category == undefined) {
                    simpleAlert('error', '', 'Select video category');
                }
                $scope.showMoreDisable = false;
            }
        } else {
            $scope.showMoreDisable = false;
        }
    }
    $scope.Uploadnewquiz = function () {
        // if ($scope.id == undefined || $scope.quiz_description == undefined || $scope.quiz_ordering == undefined || $scope.quiz_status == undefined || $scope.attempts_per_user == undefined) {
        if ($scope.id == undefined || $scope.quiz_status == undefined || $scope.attempts_per_user == undefined) {
            if ($scope.id == undefined) {
                simpleAlert('success', '', 'Please select Quiz');
                return;
            }
            // if ($scope.quiz_description == undefined) {
            //     simpleAlert('success', '', 'Quiz "description" is required');
            //     return;
            // }
            /*if ($scope.quiz_ordering == undefined) {
                simpleAlert('success', '', 'Quiz position is required');
                return;
            }*/
            if ($scope.quiz_status == undefined) {
                simpleAlert('success', '', 'Quiz status is required');
                return;
            } if ($scope.attempts_per_user == undefined) {
                simpleAlert('success', '', 'Quiz attempt is required');
                return;
            }
        } else {
            newQuiz = new FormData($("#newQuizForm")[0]);  
            newQuiz.append("subCategoriesPresent",$scope.subCategoriesPresent);           
            newQuiz.append('video_for_zones[]',$scope.selectedZones.selected);
            showLoader(".upload-new-quiz");
            adminignitefactory.Uploadnewquiz(newQuiz).then(function (response) {
                hideLoader(".upload-new-quiz", 'Publish Quiz');
                if (response.data.code == 2) window.location.replace("sixteen-login");
                if (response.data.status == 1) {
                    window.location.replace("manage/ignite/all-content");
                } else { 
                    simpleAlert('error','',response.data.message);
                }
            });
        }
    }
    // Added By Ketan Solanki to add new Work Sheet 
    $scope.Uploadnewworksheet = function (status) {                 
        var fileElement = document.getElementById("worksheet_file");
        var files = fileElement.files;                
        if ($scope.worksheet_title == undefined || $scope.description == undefined || $scope.worksheet_ordering == undefined || files[0].type != 'application/pdf') {
            if ($scope.worksheet_title == undefined) {
                simpleAlert('success', '', 'Worksheet "title" is required');
                return;
            }
            if ($scope.description == undefined) {
                simpleAlert('success', '', 'Worksheet "description" is required');
                return;
            }            
            /*if ($scope.worksheet_ordering == undefined) {
                simpleAlert('success', '', 'Worksheet "position" is required');
                return;
            }*/
            if (files[0].type != 'application/pdf') {
                simpleAlert('success', '', 'File format not allowed for worksheet file. (only PDF format allowed)');
                return;
            }
        } else {
            newWorksheet = new FormData($("#newWorkSheetForm")[0]);
            newWorksheet.append("status",status);
            showLoader(".upload-new-work-sheet");
            adminignitefactory.Uploadnewworksheet(newWorksheet).then(function (response) {                                 
                hideLoader(".upload-new-work-sheet", 'Submit');
                if (response.data.code == 2) window.location.replace("sixteen-login");
                if (response.data.status == 1) {
                    window.location.replace("manage/ignite/worksheet-manager");
                } else {
                    simpleAlert('error','',response.data.message);
                }
            });
        }
    }
    $scope.getSubjects = function(count) {
        adminignitefactory.Getcategories({'zone_id':$scope.selectedZones.selected}).then(function (response) {
            if (response.data.code == 2) window.location.replace("manage/login");
            if (response.data.status == 1) {
                if(count == 1) {
                    $scope.videoCategories = undefined;
                    $scope.subCategories = undefined;
                }
                $scope.subjects = response.data.data.categories;
                if($scope.subjects) {
                    $scope.subjects.forEach(function(e){
                        e.zones = e.zones.split(',');
                        var index = e.zones.indexOf($scope.selectedZones.selected);
                        if (index !== -1) e.zones.splice(index, 1);
                        e.otherZones = e.zones;
                    });
                }
            }
        });
    }
}]);
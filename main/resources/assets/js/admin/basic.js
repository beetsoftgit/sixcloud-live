/*
 Author  : Komal Kapadi (komal@creolestudios.com)
 Date    : 5th December 2017
 Name    : Basic js file for basic functionality like login , sign up,forgot password etc..
 Purpose : All the functions for home page
 */
var login_validation = '';
var forgot_validation = '';
var Login = function () {
    var handleLogin = function () {
        login_validation = $('#login-form').validate({
            errorElement: 'div',
            errorClass: 'help-block', 
            focusInvalid: true, 
            rules: {
                email_address: {
                    required: true,
                    email: true,
                    remote: {
                        url: "Checkadminemailexists",
                        type: "get"
                    },
                },
                password: {
                    required: true
                },
                remember: {
                    required: false
                }
            },
            messages: {
                email_address: {
                    required: "Email address is required.",
                    email: "Invalid email address.",
                    remote: "Email not exists in system."
                },
                password: {
                    required: "Password is required."
                }
            },
            invalidHandler: function (event, validator) { /*display error alert on form submit   */
                $('.alert-danger', $('#login-form')).show();
            },
            highlight: function (element) { /* hightlight error inputs*/
                $(element).closest('.form-group').addClass('has-error'); /* set error class to the control group*/
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            errorPlacement: function (error, element) {
                var container = $('<div />');
                container.addClass('Ntooltip'); /* add a class to the wrapper*/
                error.insertAfter(element);
                error.wrap(container);
                $("<div class='errorImage'></div>").insertAfter(error);
            },
            submitHandler: function (form) {
                var fd = new FormData(); 
                $.each($('#login-form').serializeArray(), function (i, obj) {
                    fd.append(obj.name, obj.value)
                })

                $.ajax({
                    url: BASEURL + 'manage/Dologin',
                    type: "post",
                    processData: false,
                    contentType: false,
                    data: fd,
                    beforeSend: function () {
                    },
                    success: function (res) {
                        if (res.status == '1')
                        {
                            simpleAlert('success', 'Login', res.message + ' Redirecting.....', false);
                            $('#login-form')[0].reset();
                            setTimeout(function () {
                                location.href = BASEURL + 'manage/ignite/all-content';
                            }, 2500);
                        } else { 
                            simpleAlert('error', 'Login', res.message, true);
                            return false;
                        }
                    },
                    error: function (e) {
                        swal({
                            title: "Error",
                            text: e.statusText,
                            type: "error",
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Try Again",
                        });
                        return false;
                    },
                    complete: function () {
                    }
                }, "json");
                return false;
            }
        });
    }


    var handleForgetPassword = function (e) {
        forgot_validation = $('.forget-form').validate({
            errorElement: 'div',
            errorClass: 'help-block',
            focusInvalid: false,
            ignore: "",
            rules: {
                email_fp: {
                    required: true,
                    email: true,
                    remote: {
                        url: "Checkadminemailexists",
                        type: "get"
                    },
                }
            },
            messages: {
                email_fp: {
                    required: "Email address is required.",
                    remote: "Email not exists."
                }
            },
            invalidHandler: function (event, validator) { 
                $('.alert-danger', $('#forget-form')).show();
            },
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error'); 
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            errorPlacement: function (error, element) {
                var container = $('<div />');
                container.addClass('Ntooltip');
                error.insertAfter(element);
                error.wrap(container);
                $("<div class='errorImage'></div>").insertAfter(error);
            },
            submitHandler: function (form) {
                //define form data
                var fd = new FormData();
                //append data                
                $.each($('#forget-form').serializeArray(), function (i, obj) {
                    fd.append(obj.name, obj.value)
                })

                $.ajax({
                    url: BASEURL + 'Adminforgotpassword?email=' + $('#email_fp').val(),
                    type: "get",
                    processData: false,
                    contentType: false,
                    beforeSend: function () {
                        $(".spinner-loader").show();
                        console.log('before send');
                    },
                    success: function (res) {
                        if (res.status == '1')
                        {
                            simpleAlert('success', 'Forgot Password', res.message, true);
                            $('.forget-form')[0].reset();
                            $('.close').trigger('click');
                            return false;
                        } else {
                            simpleAlert('error', 'Forgot Password', res.message, true);
                            return false;
                        }
                    },
                    error: function (e) {
                        simpleAlert('success', 'Forgot Password', 'Try ater.', true);
                        return false;
                    },
                    complete: function () {
                    }
                }, "json");
                return false;
            }
        });
    }

    $('#form_reset_pwd').validate({
        errorElement: 'div',
        errorClass: 'help-block', 
        focusInvalid: false,
        ignore: "",
        rules: {
            password: {
                required: true,
                minlength: 6
            },
            password_confirmation: {
                required: true,
                equalTo: "#password"
            }
        },
        messages: {
            password: {
                required: "Password is required."
            },
            password_confirmation: {
                required: "Confirm password is required."
            }
        },
        invalidHandler: function (event, validator) { 
            $('.alert-danger', $('#form_reset_pwd')).show();
        },
        highlight: function (element) { 
            $(element).closest('.form-group').addClass('has-error'); 
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
            label.remove();
        },
        errorPlacement: function (error, element) {
            var container = $('<div />');
            container.addClass('Ntooltip'); 
            error.insertAfter(element);
            error.wrap(container);
            $("<div class='errorImage'></div>").insertAfter(error);
        },
        submitHandler: function (form) {
            var fd = new FormData();
            $.each($('#form_reset_pwd').serializeArray(), function (i, obj) {
                fd.append(obj.name, obj.value);
            });
            $.ajax({
                url: BASEURL + 'Doadminresetpassword',
                type: "POST",
                processData: false,
                contentType: false,
                data: fd,
                beforeSend: function () {
                    console.log('before send');
                },
                success: function (res) {
                    if (res.status == '1')
                    {
                        simpleAlert('success', 'Forgot Password', res.message, false);
                        setTimeout(function () {
                            location.href = BASEURL + 'manage/login';
                        }, 2500);
                        return false;
                    } else {
                        simpleAlert('error', 'Forgot Password', res.message, true);
                        return false;
                    }
                },
                error: function (e) {
                    simpleAlert('error', 'Forgot Password', 'Try later.', true);
                    return false;
                },
                complete: function () {
                }
            }, "json");
            return false;
        }
    });
    $('#login-form input').keypress(function (e) {
        if (e.which == 13) {
            $('#login-form').submit();
            return false;
        }
    });
    $('.forget-form input').keypress(function (e) {
        if (e.which == 13) {
            $('.forget-form').submit();
            return false;
        }
    });
    $('#forgot_link').click(function () {
        $('.form-group').removeClass('has-error');
        login_validation.resetForm();
        forgot_validation.resetForm();
        forgot_validation.reset();
        login_validation.reset();
    });
    return {
        init: function () {
            handleLogin();
            handleForgetPassword();
        }
    };
}();

jQuery(document).ready(function () {
    Login.init();
});
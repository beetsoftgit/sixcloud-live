/* 
 * Author: Komal Kapadi
 * Date  : 15th Dec 2017
 * Custom angular file 
 */
/* JavaScript Document */
app = angular.module('sixcloudAdminApp', ['ngRoute', 'angular-page-loader', 'ui.bootstrap', 'pascalprecht.translate', 'ngImgCrop','ngSanitize', 'ngCsv']);
/*Check if user logout it redirect the login page.*/
function checklogin(param) {
    if (param.hasOwnProperty('logout')) {
        window.location = 'login';
    } else {
        return true;
    }
}
/* To change the interolate provider need to change it's originonal brackets.*/
app.config(['$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
}]);
/* for changing title */
app.run(['$location', '$rootScope', '$http', '$timeout', function($location, $rootScope, $http, $timeout) {
    $rootScope.$on('$routeChangeSuccess', function(event, current, previous) {
        if (current.$$route != undefined){
            $rootScope.pagetitle = current.$$route.title;
            $rootScope.module = current.$$route.module;
        } 
        document.body.scrollTop = document.documentElement.scrollTop = 0;
    });
}]);
app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $routeProvider.when('/manage/dashboard', {
        templateUrl: BASEURL + '/resources/views/admin/angular/elt/ELT-home.html',
        controller: 'HomeController',
        title: 'Six Cloud Control Panel :: Dashboard',
        resolve: {
            Geteltsections: function(eltfactory) {
                return eltfactory.Geteltsections();
            }
        }
    }).when('/manage/subscription-plans', {
        templateUrl: BASEURL + '/resources/views/admin/angular/elt/ELT-subscription-plan.html',
        controller: 'SubscriptionController',
        title: 'Six Cloud Control Panel :: Subscription plans',
        resolve: {
            Getsubscriptions: function(subscriptionfactory) {
                return subscriptionfactory.Getsubscriptions();
            }
        }
    })
    // .when('/manage/ignite', {
    //     templateUrl: BASEURL + '/resources/views/admin/angular/ignite/ELT-home.html',
    //     controller: 'HomeController',
    //     title: 'Six Cloud Control Panel :: Home',
    //     resolve: {
    //         Geteltsections: function(eltfactory) {
    //             return eltfactory.Geteltsections();
    //         }
    //     }
    // })
    .when('/manage/ignite/dashboard', {
        templateUrl: BASEURL + '/resources/views/admin/angular/ignite/ELT-home.html',
        controller: 'IgniteController',
        title: 'Six Cloud Ignite Control Panel :: Home',
        module:'ignite'        
    }).when('/manage/ignite/all-content', {
        templateUrl: BASEURL + '/resources/views/admin/angular/ignite/all-content.html',
        controller: 'AllContentController',
        title: 'Six Cloud Ignite Control Panel :: All Content',
        module:'ignite'
    }).when('/manage/ignite/video-details/:slug', {
        templateUrl: BASEURL + '/resources/views/admin/angular/ignite/video-learning.html',
        controller: 'VideoDetailsController',
        title: 'Six Cloud Ignite Control Panel :: Video Details',
        module:'ignite'
    }).when('/manage/ignite/edit-video/:slug', {
        templateUrl: BASEURL + '/resources/views/admin/angular/ignite/edit-video.html',
        controller: 'EditVideoController',
        title: 'Six Cloud Ignite Control Panel :: Edit Video',
        module:'ignite'
    }).when('/manage/ignite/quiz-manager/:status?/:category?', {
        templateUrl: BASEURL + '/resources/views/admin/angular/ignite/quiz-manager.html',
        controller: 'QuizManagerController',
        title: 'Six Cloud Ignite Control Panel :: Quiz Manager',
        module:'ignite'
    }).when('/manage/ignite/video-manager/:status?/:category?', {
        templateUrl: BASEURL + '/resources/views/admin/angular/ignite/video-manager.html',
        controller: 'VideoManagerController',
        title: 'Six Cloud Ignite Control Panel :: Video Manager',
        module:'ignite'
    }).when('/manage/ignite/zone-manager', {
        templateUrl: BASEURL + '/resources/views/admin/angular/ignite/zone-manager.html',
        controller: 'ZoneManagerController',
        title: 'Six Cloud Ignite Control Panel :: Zone Manager',
        module:'ignite'
    }).when('/manage/ignite/category-manager', {
        templateUrl: BASEURL + '/resources/views/admin/angular/ignite/category-manager.html',
        controller: 'CategoryManagerController',
        title: 'Six Cloud Ignite Control Panel :: Category Manager',
        module:'ignite'
    }).when('/manage/ignite/worksheet-manager/:status?/:category?', {
        templateUrl: BASEURL + '/resources/views/admin/angular/ignite/worksheet-manager.html',
        controller: 'WorkSheetManagerController',
        title: 'Six Cloud Ignite Control Panel :: Worksheet Manager',
        module:'ignite'
    }).when('/manage/ignite/edit-worksheet/:slug', {
        templateUrl: BASEURL + '/resources/views/admin/angular/ignite/edit-worksheet.html',
        controller: 'EditWorkSheetController',
        title: 'Six Cloud Ignite Control Panel :: Edit WorkSheet',
        module:'ignite'
    }).when('/manage/ignite/edit-quiz/:slug', {
        templateUrl: BASEURL + '/resources/views/admin/angular/ignite/edit-quiz.html',
        controller: 'EditQuizManagerController',
        title: 'Six Cloud Ignite Control Panel :: Edit Quiz',
        module:'ignite'
    }).when('/manage/ignite/add-new-distributor', {
        templateUrl: BASEURL + '/resources/views/admin/angular/ignite/add-distributor.html',
        controller: 'IgniteAddDistributorController',
        title: 'Six Cloud Ignite Control Panel :: Add Distributors',
        module:'ignite'
    }).when('/manage/ignite/add-new-subscriber', {
        templateUrl: BASEURL + '/resources/views/admin/angular/ignite/add-subscriber.html',
        controller: 'IgniteAddSubscriberController',
        title: 'Six Cloud Ignite Control Panel :: Add Subscriber',
        module:'ignite'
    }).when('/manage/ignite/edit-distributor/:slug', {
        templateUrl: BASEURL + '/resources/views/admin/angular/ignite/edit-distributor.html',
        controller: 'IgniteEditDistributorController',
        title: 'Six Cloud Ignite Control Panel :: Edit Distributors',
        module:'ignite'
    }).when('/manage/ignite/distributors', {
        templateUrl: BASEURL + '/resources/views/admin/angular/ignite/distributors-list-admin.html',
        controller: 'IgniteDistributorController',
        title: 'Six Cloud Ignite Control Panel :: Distributors',
        module:'ignite'
    }).when('/manage/ignite/ignite-distributor-details/:slug', {
        templateUrl: BASEURL + '/resources/views/admin/angular/ignite/distributors-details-admin.html',
        controller: 'IgniteDistributorDetailsController',
        title: 'Six Cloud Ignite Control Panel :: Distributors Detail',
        module:'ignite'
    }).when('/manage/ignite/distributor-commissions/:slug', {
        templateUrl: BASEURL + '/resources/views/admin/angular/ignite/distributors-commissions.html',
        controller: 'IgniteDistributorCommissionsController',
        title: 'Six Cloud Ignite Control Panel :: Distributor Commission',
        module:'ignite'
    }).when('/manage/ignite/subscribers', {
        templateUrl: BASEURL + '/resources/views/admin/angular/ignite/subscribers-list-admin.html',
        controller: 'IgniteSubscriberController',
        title: 'Six Cloud Ignite Control Panel :: Subscribers',
        module:'ignite'
    }).when('/manage/ignite/pricing-plans', {
        templateUrl: BASEURL + '/resources/views/admin/angular/ignite/pricing-plans.html',
        controller: 'IgnitePricingPlansController',
        title: 'Six Cloud Ignite Control Panel :: Price Plan',
        module:'ignite'
    }).when('/manage/ignite/promo-codes/:status?', {
        templateUrl: BASEURL + '/resources/views/admin/angular/ignite/promo-codes.html',
        controller: 'IgnitePromoCodesController',
        title: 'Six Cloud Ignite Control Panel :: Promo Codes',
        module:'ignite'
    }).when('/manage/ignite/promo-codes-details/:slug', {
        templateUrl: BASEURL + '/resources/views/admin/angular/ignite/promo-codes-details.html',
        controller: 'IgnitePromoCodesDetailsController',
        title: 'Six Cloud Ignite Control Panel :: Promo Codes Detail',
        module:'ignite'
    }).when('/manage/ignite/add-promo-codes', {
        templateUrl: BASEURL + '/resources/views/admin/angular/ignite/add-promo-codes.html',
        controller: 'IgniteAddPromoCodesController',
        title: 'Six Cloud Ignite Control Panel :: Promo Codes',
        module:'ignite'
    }).when('/manage/ignite/edit-promo-codes/:slug', {
        templateUrl: BASEURL + '/resources/views/admin/angular/ignite/edit-promo-codes.html',
        controller: 'IgniteEditPromoCodesController',
        title: 'Six Cloud Ignite Control Panel :: Promo Codes',
        module:'ignite'
    }).when('/manage/ignite/ignite-subscriber-details/:slug', {
        templateUrl: BASEURL + '/resources/views/admin/angular/ignite/subscribers-details-admin.html',
        controller: 'IgniteSubscriberDetailsController',
        title: 'Six Cloud Ignite Control Panel :: Subscribers Detail',
        module:'ignite',
        resolve: {
            Getcountry: function(adminignitefactory) {
                return adminignitefactory.Getcountry();
            }
        }
    }).when('/manage/ignite/new-content', {
        templateUrl: BASEURL + '/resources/views/admin/angular/ignite/quizzes.html',
        controller: 'NewContentController',
        title: 'Six Cloud Ignite Control Panel :: New Content',
        module:'ignite'
    }).when('/manage/ignite/create-new-quiz', {
        templateUrl: BASEURL + '/resources/views/admin/angular/ignite/create-new-quiz.html',
        controller: 'CreateNewQuizController',
        title: 'Six Cloud Ignite Control Panel :: Create New Quiz',
        module:'ignite'
    }).when('/manage/ignite/create-new-video', {
        templateUrl: BASEURL + '/resources/views/admin/angular/ignite/create-new-video.html',
        controller: 'CreateNewVideoController',
        title: 'Six Cloud Ignite Control Panel :: Create New Video',
        module:'ignite'
    }).when('/manage/ignite/create-new-worksheet', {
        templateUrl: BASEURL + '/resources/views/admin/angular/ignite/create-new-worksheet.html',
        controller: 'CreateNewWorkSheetController',
        title: 'Six Cloud Ignite Control Panel :: Create New Work Sheet',
        module:'ignite'
    }).when('/manage/ignite/quiz-info/:slug', {
        templateUrl: BASEURL + '/resources/views/admin/angular/ignite/quiz-info.html',
        controller: 'QuizInfoController',
        title: 'Six Cloud Ignite Control Panel :: Create New Quiz',
        module:'ignite'
    }).when('/manage/ignite/worksheet-info/:slug', {
        templateUrl: BASEURL + '/resources/views/admin/angular/ignite/worksheet-info.html',
        controller: 'WorkSheetInfoController',
        title: 'Six Cloud Ignite Control Panel :: Work Sheet Info Page',
        module:'ignite'
    }).when('/manage/ignite/ignite-transactions', {
        templateUrl : BASEURL + '/resources/views/admin/angular/ignite/transactions.html',
        controller  : 'IgniteTransactionsListingController',
        title: 'Six Cloud Control Panel:: Ignite Transactions',
        module:'ignite',        
    }).when('/manage/ignite/change-password', {
        templateUrl: BASEURL + '/resources/views/admin/angular/ignite/change-password.html',
        controller: 'IgniteChangePasswordController',
        title: 'Six Cloud Ignite Control Panel :: Change Password',
        module:'ignite'
    }).when('/manage/ignite/ignite-taxrate', {
        templateUrl : BASEURL + '/resources/views/admin/angular/ignite/taxrate.html',
        controller  : 'IgniteTaxRateController',
        title: 'Six Cloud Control Panel:: Ignite TaxRate',
        module:'ignite',        
    }).when('/manage/transactions', {
        templateUrl: BASEURL + '/resources/views/admin/angular/elt/ELT-all-transactions.html',
        controller: 'TransactionController',
        title: 'Six Cloud Control Panel :: Transactions',
        resolve: {
            Getsubscriptions: function(subscriptionfactory) {
                return subscriptionfactory.Getsubscriptions();
            }
        }
    }).when('/manage', {
        templateUrl: BASEURL + '/resources/views/admin/angular/elt/home.html',
        controller: 'HomeController',
        title: 'Six Cloud Control Panel:: Home'
    }).when('/manage/pr/open-cases', {
        templateUrl: BASEURL + '/resources/views/admin/angular/pr/open-cases.html',
        controller: 'PrOpenCaseController',
        title: 'Six Cloud Control Panel:: Open Cases',
        module: 'pr'
    }).when('/manage/pr/special-cases', {
        templateUrl: BASEURL + '/resources/views/admin/angular/pr/special-cases.html',
        controller: 'PrSpecialCaseController',
        title: 'Six Cloud Control Panel:: Special Cases',
        module: 'pr'
    }).when('/manage/pr/closed-cases', {
        templateUrl: BASEURL + '/resources/views/admin/angular/pr/closed-cases.html',
        controller: 'PrClosedCaseController',
        title: 'Six Cloud Control Panel:: Closed Cases',
        module: 'pr'
    }).when('/manage/pr/all-accessor', {
        templateUrl: BASEURL + '/resources/views/admin/angular/pr/all-accessors.html',
        controller: 'PrAllAccessorController',
        title: 'Six Cloud Control Panel:: All Accessors',
        module: 'pr'
    }).when('/manage/pr/account-info/:id', {
        // templateUrl: BASEURL + '/resources/views/admin/angular/pr/PR-account-info.html',
        templateUrl: BASEURL + '/resources/views/admin/angular/pr/accessor-account-info.html',
        controller: 'PrAccountInfoController',
        title: 'Six Cloud Control Panel:: Account Info',
        module: 'pr'
    }).when('/manage/pr/accessors-my-account', {
        templateUrl: BASEURL + '/resources/views/admin/angular/pr/pr-accessor-my-account.html',
        controller: 'PrAccessorMyAccountController',
        title: 'Six Cloud Control Panel:: My Account',
        module: 'pr'
    }).when('/manage/pr/my-jobs', {
        templateUrl: BASEURL + '/resources/views/admin/angular/pr/accessor-my-job.html',
        controller: 'PrAccessorMyJobsController',
        title: 'Six Cloud Control Panel:: My Jobs',
        module: 'pr'
    }).when('/manage/my-account', {
        templateUrl: BASEURL + '/resources/views/admin/angular/my-acc/my-account.html',
        controller: 'MyAccountController',
        title: 'Six Cloud Control Panel:: My account'
    }).when('/manage/admin-team', {
        templateUrl: BASEURL + '/resources/views/admin/angular/my-acc/admin-team.html',
        controller: 'AddAdminController',
        title: 'Six Cloud Control Panel:: Add Admin',
        resolve: {
            Adminlisting: function(adminfactory) {
                return adminfactory.Adminlisting();
            }
        }
    
    }).when('/manage/sixteen/dashboard', {
        templateUrl: BASEURL + '/resources/views/admin/angular/mp/home.html',
        controller: 'MPDashboardController',
        title: 'Six Cloud Control Panel:: Sixteen',
        module:'mp',
    }).when('/manage/sixteen/all-projects', {
        templateUrl: BASEURL + '/resources/views/admin/angular/mp/all-jobs.html',
        controller: 'MPAllJobsController',
        title: 'Six Cloud Control Panel:: Sixteen',
        module:'mp',
        resolve: {
            Getallcategories: function(adminmpfactory) {
                return adminmpfactory.Getallcategories();
            }
        }
    }).when('/manage/sixteen/sixteen-buyers', {
        templateUrl: BASEURL + '/resources/views/admin/angular/mp/marketplace-buyers.html',
        controller: 'MPBuyersController',
        title: 'Six Cloud Control Panel:: Sixteen',
        module:'mp',
    }).when('/manage/sixteen/sixteen-sellers', {
        templateUrl: BASEURL + '/resources/views/admin/angular/mp/marketplace-designers.html',
        controller: 'MPDesignersController',
        title: 'Six Cloud Control Panel:: Sixteen',
        module:'mp',
        resolve: {
            Getallcategories: function(adminmpfactory) {
                return adminmpfactory.Getallcategories();
            }
        }
    }).when('/manage/sixteen/seller-info/:slug', {
        templateUrl : BASEURL + '/resources/views/admin/angular/mp/marketplace-designer-info.html',
        controller  : 'MPDesignerInfoController',
        title: 'Six Cloud Control Panel:: Sixteen',
        module:'mp',
    }).when('/manage/sixteen/buyer-info/:slug', {
        templateUrl : BASEURL + '/resources/views/admin/angular/mp/marketplace-buyer-info.html',
        controller  : 'MpBuyerInfoController',
        title: 'Six Cloud Control Panel:: Sixteen',
        module:'mp',
    }).when('/manage/sixteen/project-details/:slug', {
        templateUrl : BASEURL + '/resources/views/admin/angular/mp/marketplace-project-details.html',
        controller  : 'MPProjectDetailsController',
        title: 'Six Cloud Control Panel:: Sixteen',
        module:'mp',
    }).when('/manage/sixteen/tickets/:for', {
        templateUrl : BASEURL + '/resources/views/admin/angular/mp/marketplace-ticket-listing.html',
        controller  : 'MPTicketListingController',
        title: 'Six Cloud Control Panel:: Sixteen',
        module:'mp',
        resolve: {
            Gettickettypes: function(adminmpfactory) {
                return adminmpfactory.Gettickettypes();
            }
        }
    }).when('/manage/ignite/tickets/:for', {
        templateUrl : BASEURL + '/resources/views/admin/angular/mp/marketplace-ticket-listing.html',
        controller  : 'MPTicketListingController',
        title: 'Six Cloud Control Panel:: Sixteen',
        module:'ignite',
        resolve: {
            Gettickettypes: function(adminmpfactory) {
                return adminmpfactory.Gettickettypes();
            }
        }
    }).when('/manage/sixteen/countries', {
        templateUrl : BASEURL + '/resources/views/admin/angular/mp/marketplace-countries-listing.html',
        controller  : 'MPCountriesListingController',
        title: 'Six Cloud Control Panel:: Sixteen',
        module:'mp',        
    }).when('/manage/sixteen/ticket-detail/:slug', {
        templateUrl : BASEURL + '/resources/views/admin/angular/mp/marketplace-ticket-detail.html',
        controller  : 'MPTicketDetailController',
        title: 'Six Cloud Control Panel:: Sixteen',
        module:'mp',
    }).when('/manage/ignite/ticket-detail/:slug', {
        templateUrl : BASEURL + '/resources/views/admin/angular/mp/marketplace-ticket-detail.html',
        controller  : 'MPTicketDetailController',
        title: 'Six Cloud Control Panel:: Sixteen',
        module:'ignite',
    }).when('/manage/sixteen/transactions', {
        templateUrl : BASEURL + '/resources/views/admin/angular/mp/marketplace-transaction.html',
        controller  : 'MPTransactionListingController',
        title: 'Six Cloud Control Panel:: Sixteen',
        module:'mp',        
    }).when('/manage/sixteen/categories', {
        templateUrl: BASEURL + '/resources/views/admin/angular/mp/categories.html',
        controller: 'MPCategoriesController',
        title: 'Six Cloud Control Panel:: Sixteen',
        module:'mp',
        resolve: {
            Getallcategories: function(adminmpfactory) {
                return adminmpfactory.Getallcategories();
            }
        }
    });
    // .otherwise({
    //     redirectTo: '/manage/ignite'
    // });
    $locationProvider.html5Mode(true);
}]);
/*CONFIG*/
app.run(function($rootScope, $location, $route, $timeout) {
    $rootScope.layout = {};
    $rootScope.$on('$routeChangeStart', function() {
        $timeout(function() {
            $rootScope.layout.loading = true;
        });
    });
    $rootScope.$on('$routeChangeSuccess', function() {
        $timeout(function() {
            $rootScope.layout.loading = false;
        }, 500);
    });
    $rootScope.$on('$routeChangeError', function() {
        $rootScope.layout.loading = false;
    });
});
//app.config(function ($httpProvider) {
//    console.log($httpProvider.responseInterceptors);
//    $httpProvider.responseInterceptors.push('myHttpInterceptor');
//    var spinnerFunction = function spinnerFunction(data, headersGetter) {
//        return data;
//    };
//    $httpProvider.defaults.transformRequest.push(spinnerFunction);
//});
app.factory('myHttpInterceptor', function($q, $window) {
    return function(promise) {
        return promise.then(function(response) {
            return response;
        }, function(response) {
            return $q.reject(response);
        });
    };
});
app.directive('scrollTrigger', function($window) {
    return {
        link: function(scope, element, attrs) {
            var offset = parseInt(attrs.threshold) || 0;
            var e = jQuery(element[0]);
            var doc = jQuery(document);
            angular.element(document).bind('scroll', function() {
                if (doc.scrollTop() + $window.innerHeight + offset > e.offset().top) {
                    scope.$apply(attrs.scrollTrigger);
                }
            });
        }
    };
});

app.directive("owlCarousel", function() {
    return {
        restrict: 'E',
        transclude: false,
        link: function(scope) {
            scope.initCarousel = function(element) {
                s
                /* provide any default options you want */
                var defaultOptions = {};
                var customOptions = scope.$eval($(element).attr('data-options'));
                /*combine the two options objects*/
                for (var key in customOptions) {
                    defaultOptions[key] = customOptions[key];
                }
                /*init carousel*/
                $(element).owlCarousel(defaultOptions);
            };
        }
    };
})
app.directive('owlCarouselItem', [function() {
    return {
        restrict: 'A',
        transclude: false,
        link: function(scope, element) {
            /*wait for the last item in the ng-repeat then call init*/
            if (scope.$last) {
                scope.initCarousel(element.parent());
            }
        }
    };
}]);
// app.directive('validFile', function() {
//     return {
//         require: 'ngModel',
//         link: function(scope, el, attrs, ngModel) {
//             /*change event is fired when file is selected*/
//             el.bind('change', function() {
//                 scope.$apply(function() {
//                     ngModel.$setViewValue(el.val());
//                     ngModel.$render();
//                 });
//             });
//         }
//     }
// });
app.directive('validFile', function() {
    return {
        require: 'ngModel',
        link: function(scope, el, attrs, ctrl) {
            ctrl.$setValidity('validFile', el.val() != '');
            //change event is fired when file is selected
            el.bind('change', function() {
                ctrl.$setValidity('validFile', el.val() != '');
                scope.$apply(function() {
                    ctrl.$setViewValue(el.val());
                    ctrl.$render();
                });
            });
        }
    }
});
app.directive('defaultPagination', function() {
    return {
        restrict: 'E',
        scope: {
            paginate: '=',
            currentpage: '=',
            range: '=',
            like: '&'
        },
        templateUrl: BASEURL + '/resources/views/front/pagination.html'
    };
});
app.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }            
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});
/* Disable console.log if mode is production.*/
if (project_mode == 'production') console.log = function() {}


app.directive('toggledirective', function(){
    return {
        restrict: 'C',
        link: function($scope, iElm, iAttr, controller){
            iElm.bind('click', function(){
                iElm.toggleClass('active');
                iElm.next('.panel-collapse').slideToggle();
            });
        }
    };
});
app.directive('select2Directive', function($timeout){
    return {
        restrict: 'A',
        link: function($scope, iElm, iAttr, controller){
            $timeout(function () {
                iElm.select2();
            }, 1000);
        }
    };
});
app.directive('countdown', [
        'Util',
        '$interval',
        function (Util, $interval) {
            return {
                restrict: 'A',
                scope: { date: '@' },
                link: function (scope, element) {
                    var future;
                    future = new Date(scope.date.replace(/-/g, "/"));
                    var newDate = new Date(future.getTime()+future.getTimezoneOffset()*60*1000);
                    var offset = future.getTimezoneOffset() / 60;
                    var hours = future.getHours();
                    newDate.setHours(hours - offset);
                    
                    $interval(function () {
                        var diff;
                        /*var today = new Date();
                        var str = today.toGMTString();  // deprecated! use toUTCString()*/
                        diff = Math.floor((newDate.getTime() - new Date().getTime()) / 1000);
                        return element.text(Util.dhms(diff));
                    }, 1000);
                }
            };
        }
    ]).factory('Util', [function () {
            return {
                dhms: function (t) {
                    var days, hours, minutes, seconds;
                    days = Math.floor(t / 86400);
                    t -= days * 86400;
                    hours = Math.floor(t / 3600) % 24;
                    t -= hours * 3600;
                    minutes = Math.floor(t / 60) % 60;
                    t -= minutes * 60;
                    seconds = t % 60;
                    if(days > 0)
                    return [
                        days + 'd',
                        hours + 'h',
                        minutes + 'm',
                        seconds + 's'
                    ].join(' ');
                    else 
                       return [
                        hours + 'hrs',
                        minutes + 'mins',
                        seconds + 'secs'
                    ].join(' ');
                }
            };     
        }]);

app.directive('multiselect2Directive', function($timeout){
    return {
        restrict: 'A',
        link: function($scope, iElm, iAttr, controller){
            $timeout(function () {
                iElm.multiselect({
                    includeSelectAllOption: true,
                    disabled: true,
                });
            }, 1000);
        }
    };
});
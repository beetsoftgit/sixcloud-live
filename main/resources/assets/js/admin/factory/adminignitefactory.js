/* 
 * Author: Ketan Solanki
 * Date  : 11th July 2018
 * Admin Ignite factory file
 */
angular.module('sixcloudAdminApp').factory('adminignitefactory', ['$http', '$rootScope',
    function ($http, $rootScope) {
        return {
            // check if user logged in or not
            GetAllContentData: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'GetAllContentData',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            GetAllQuizData: function (data,pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                } 
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'GetAllQuizData?page='+ pageNumber,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            UpdateContentOrder: function (idArray) {                
                return $http({
                    method: 'POST',                     
                    data: {'idArray': idArray},
                    url: 'UpdateContentOrder',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },

            Getvideocategories: function () {
                return $http({
                    method: 'POST',
                    url: 'Getvideocategories',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Uploadnewvideo: function (data) {
                return $http({
                    method: 'POST',
                    url: 'Uploadnewvideo',
                    data: data,
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Loadprerequisites: function (data) {
                return $http({
                    method: 'POST',
                    url: 'Loadprerequisites',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            //Code added by Ketan Solanki for Adding Quiz data
            GetQuizListing: function () {
                return $http({
                    method: 'POST',
                    url: 'GetQuizListing',                    
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Uploadnewquiz: function (data) {
                return $http({
                    method: 'POST',
                    url: 'Uploadnewquiz',
                    data: data,
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },             
            GetallDistributorData: function (data, pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'GetallDistributorData?page=' + pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            ExportToExcel: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'ExportToExcel'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            GetallSubscriberData: function (data, pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'GetallSubscriberData?page=' + pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            GetDistributorDetailsData: function (data) {                
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'GetDistributorDetailsData'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            GetSubscriberDetailsData: function (data) {                
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'GetSubscriberDetailsData'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            }, 
            GetDistributorMembers: function (data, pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'GetDistributorMembers?page=' + pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },

            GetCurrentSubscription: function (data, pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'GetCurrentSubscription?page=' + pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },

            getAllSubscription: function (data) {
                return $http({
                    method: "POST",
                    data: data,
                    url: 'getAllSubscription'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },

            removeSubscription: function(data) {
                return $http({
                    method: "POST",
                    data: data,
                    url: 'removeSubscription'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },

            GetHistorySubscription: function (data, pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'GetHistorySubscription?page=' + pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },

            GetDistributorReferalls: function (data, pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'GetDistributorReferalls?page=' + pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },            
            Addnewquestion: function (data) {
                return $http({
                    method: 'POST',
                    url: 'Addnewquestion',
                    data: data,
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },   
            Getcountry: function(data) {
                return $http({
                    method: 'GET',
                    data: data,
                    url: 'Getcountry'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Addquiztitle: function (data) {
                return $http({
                    method: 'POST',
                    url: 'Addquiztitle',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Editquiztitle: function (data) {
                return $http({
                    method: 'POST',
                    url: 'Addquiztitle',
                    data: data,
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getstatesforcountry: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getstatesforcountry'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getcitiesforstate: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getcitiesforstate'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            addNewDistributor: function (data) {
                return $http({
                    method: 'POST',
                    url: 'addNewDistributor',
                    data: data,
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            CheckOutstandingCommisions: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'CheckOutstandingCommisions'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            suspendAccount: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'suspendAccount'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            sendEmailToDistributor: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'sendEmailToDistributor',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Quizpublishdraft: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Quizpublishdraft',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },            
            GetQuizDetails: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'GetQuizDetails'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Uploadnewworksheet: function (data) {
                return $http({
                    method: 'POST',
                    url: 'Uploadnewworksheet',
                    data: data,
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            GetVideoDetails: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'GetVideoDetails'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            PublishVideo: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'PublishVideo'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            DeleteVideo: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'DeleteVideo'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Deletequiz: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Deletequiz'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Deleteworksheet: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Deleteworksheet'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            UnPublishVideo: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'UnPublishVideo'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            UnPublishQuiz: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'UnPublishQuiz'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            PublishQuiz: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'PublishQuiz'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            GetVideoListing: function () {
                return $http({
                    method: 'POST',
                    url: 'GetVideoListing',                    
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            GetAllVideoData: function (data,pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                } 
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'GetAllVideoData?page='+ pageNumber,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Editquestion: function (data) {
                return $http({
                    method: 'POST',
                    url: 'Editquestion',
                    data: data,
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            GetAllWorkSheetData: function (data,pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                } 
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'GetAllWorkSheetData?page='+ pageNumber,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            GetworkSheetDetails: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'GetworkSheetDetails'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Editworksheet: function (data) {
                return $http({
                    method: 'POST',
                    url: 'Editworksheet',
                    data: data,
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            UnpublishWorkSheet: function (data) {
                return $http({
                    method: 'POST',
                    url: 'UnpublishWorkSheet',
                    data: data                    
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Editvideo: function (data) {
                return $http({
                    method: 'POST',
                    url: 'Editvideo',
                    data: data,
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            ChangeQuizStatus: function (data) {
                return $http({
                    method: 'POST',
                    url: 'ChangeQuizStatus',
                    data: data,
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },             
            GetDistributorDetails: function (data) {
                return $http({
                    method: 'POST',
                    url: 'GetDistributorDetails',
                    data: data                    
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },                         
            editDistributor: function (data) {
                return $http({
                    method: 'POST',
                    url: 'editDistributor',
                    data: data,
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }                 
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            changeDistributorPassword: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'changeDistributorPassword',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            RegenerateQRCode: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'RegenerateQRCode'                     
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            RegenerateReferalCode: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'RegenerateReferalCode'                     
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            RegenerateUniqueLink: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'RegenerateUniqueLink'                     
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            teamAccountAction: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'teamAccountAction'                     
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            addNewSubscriber: function (data) {
                return $http({
                    method: 'POST',
                    url: 'addNewSubscriber',
                    data: data,
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getcategoriesforsubscription: function (data) {
                return $http({
                    method: 'POST',
                    url: 'Getcategoriesforsubscription',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            AddSubscription: function (data) {
                return $http({
                    method: 'POST',
                    url: 'AddSubscription',
                    data: data,
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            UpdateQuizQuestionOrder: function (idArray) {                
                return $http({
                    method: 'POST',                     
                    data: {'idArray': idArray},
                    url: 'UpdateQuizQuestionOrder',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Deletequestion: function (data) {
                return $http({
                    method: 'POST',
                    url: 'Deletequestion',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Editsubscriberdetails: function (data) {
                return $http({
                    method: 'POST',
                    url: 'Editsubscriberdetails',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getallbuzzcategories: function (data) {
                return $http({
                    method: 'POST',
                    url: 'Getallbuzzcategories',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Savecategory: function (data) {
                return $http({
                    method: 'POST',
                    url: 'Savecategory',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Savepromo: function (data) {
                return $http({
                    method: 'POST',
                    url: 'Savepromo',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Showteammemberinfo: function (data) {
                return $http({
                    method: 'POST',
                    url: 'Showteammemberinfo',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getalldistributors: function (data) {
                return $http({
                    method: 'POST',
                    url: 'Getalldistributors',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Generatepromo: function (data) {
                return $http({
                    method: 'POST',
                    url: 'Generatepromo',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            generatePassword: function (data) {
                return $http({
                    method: 'GET',
                    url: 'generatePassword',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Savepromocode: function (data) {
                return $http({
                    method: 'POST',
                    url: 'Savepromocode',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            GetAllPromoData: function (data,pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                } 
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'GetAllPromoData?page='+ pageNumber,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getpromodetaildata: function (data) {
                return $http({
                    method: 'POST',
                    url: 'Getpromodetaildata',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Suspendpromo: function (data) {
                return $http({
                    method: 'POST',
                    url: 'Suspendpromo',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            GetPromoDetail: function (data,pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                } 
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'GetPromoDetail?page='+ pageNumber,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getteamforcommissions: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Getteamforcommissionsforadmin',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getcommissions: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Getdistributorcommissionsforadmin',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Gettypeandyear: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Gettypeandyearforadmin',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getignitetransactions: function(pageNumber) {
                if (pageNumber.page === undefined) {
                    pageNumber.page = '1';
                }
                return $http({
                    method: 'POST',
                    url: 'Getignitetransactions?page='+ pageNumber.page,
                    data: pageNumber,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getcountryforzones: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Getcountryforzones',
                    data: data
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getlanguagesforzones: function() {
                return $http({
                    method: 'GET',
                    url: 'Getlanguagesforzones',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getzones: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Getzones',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Savenewzone: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Savenewzone',
                    data: data,
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Deletezone: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Deletezone',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Savenewcategory: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Savenewcategory',
                    data: data,
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getcategories: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Getcategories',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            addOrEditBundleCategoryId: function() {
                return $http({
                    method: 'GET',
                    url: 'addOrEditBundleCategoryId',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Deletesubject: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Deletesubject',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Loadcategoriesforsubject: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Loadcategoriesforsubject',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Loadlanguagesforzones: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Loadlanguagesforzones',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getsubcategories: function(data) {
               return $http({
                   method: 'POST',
                   url: 'Getsubcategoriesadmin',
                   data: data,
               }).then(function successCallback(response) {
                   return response;
               }, function errorCallback(response) {
                   return response;
               });
           },
           removeSubscription: function(data) {
               return $http({
                   method: "POST",
                   data: data,
                   url: 'removeSubscription'
               }).then(function successCallback(response) {
                   return response;
               }, function errorCallback(response) {
                   return response;
               });
           },
           removeSubscriptionOfSubscriber: function(data) {
               return $http({
                   method: 'POST',
                   url: 'removeSubscriptionOfSubscriber',
                   data: data,
               }).then(function successCallback(response) {
                   return response;
               }, function errorCallback(response) {
                   return response;
               });
           },
           Updateadminpassword: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Updateadminpassword'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            GetIgnitehistory: function (data) {
                return $http({
                    method: 'POST',
                    url: 'GetIgnitehistory',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getignitetaxrate: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getignitetaxrate'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            GetTaxrate: function(pageNumber) {
                if (pageNumber.page === undefined) {
                    pageNumber.page = '1';
                }
                return $http({
                    method: 'POST',
                    url: 'GetTaxrate?page='+ pageNumber.page,
                    data: pageNumber,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            }
       }
   }
]);
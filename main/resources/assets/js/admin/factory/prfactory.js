/* 
 * Author: Komal Kapadi
 * Date  : 4th Dec 2017
 * Common factory file
 */
angular.module('sixcloudAdminApp').factory('prfactory', ['$http',
    function ($http) {
        return {
            Getcases: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getadmincases'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getaccessors: function (data) {
                $('.spinner-loader').show();
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getaccessors'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
                $('.spinner-loader').hide();
            },
            Getaccessorinfo: function (data) {
                $('.spinner-loader').show();
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getaccessorinfo'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
                $('.spinner-loader').hide();
            },
            Assignaccessor: function (data) {
                $('.spinner-loader').show();
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Assignaccessor'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
                $('.spinner-loader').hide();
            },
            Addaccessor: function (data) {
                $('.spinner-loader').show();
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Addaccessor'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
                $('.spinner-loader').hide();
            },
            Updatepassword: function (data) {
                $('.spinner-loader').show();
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Updatepassword'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
                $('.spinner-loader').hide();
            },
            Myjobcases: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Myjobcases'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Accessorcompletedjobs: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Accessorcompletedjobs'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Myteaminfo: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Myteaminfo'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Acceptorrejectcase: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Acceptorrejectcase'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Helpme: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Helpme'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Download: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Download'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Unlinkdownloadedfile: function (data) {
                $('.spinner-loader').show();
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Unlinkdownloadedfile'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
                $('.spinner-loader').hide();
            },
            Cancelcase: function (data) {
                $('.spinner-loader').show();
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Cancelcase'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
                $('.spinner-loader').hide();
            },
        };
    }
]);
/* 
 * Author: Komal Kapadi
 * Date  : 21st Dec 2017
 * subscription factory file
 */
angular.module('sixcloudAdminApp').factory('subscriptionfactory', ['$http', '$rootScope',
    function ($http, $rootScope) {
        return {
            // check if user logged in or not
            Getsubscriptions: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getsubscriptions'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getpayments: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getpayments'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            GetSubscriptionusers: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'GetSubscriptionusers'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            }
        };
    }
]);
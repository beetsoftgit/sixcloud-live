/*
 Author  : Komal Kapadi (komal@creolestudios.com)
 Date    : 5th December 2017
 Name    : Basic js file for basic functionality like login , sign up,forgot password etc..
 Purpose : All the functions for home page
 */
var login_validation = '';
var forgot_validation = '';
var Login = function () {
    var handleLogin = function () {
        login_validation = $('#login-form').validate({
            errorElement: 'div', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: true, // do not focus the last invalid input
            rules: {
                email_address: {
                    required: true,
                    email: true,
                    remote: {
                        url: "checkuseremailexists",
                        type: "get"
                    },
                },
                password: {
                    required: true
                },
                remember: {
                    required: false
                }
            },
            messages: {
                email_address: {
                    required: "Email address is required.",
                    email: "Invalid email address.",
                    remote: "Email not exists."
                },
                password: {
                    required: "Password is required."
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit   
                $('.alert-danger', $('#login-form')).show();
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            errorPlacement: function (error, element) {
                var container = $('<div />');
                container.addClass('Ntooltip'); // add a class to the wrapper
                error.insertAfter(element);
                error.wrap(container);
                $("<div class='errorImage'></div>").insertAfter(error);
//                if (element.is(':checkbox')) {
//                    error.insertAfter(element.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline"));
//                } else if (element.is(':radio')) {
//                    error.insertAfter(element.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline"));
//                } else {
//                    error.insertAfter(element); // for other inputs, just perform default behavior
//                }
            },
            submitHandler: function (form) {
                //define form data
                var fd = new FormData();
                //append data                
                $.each($('#login-form').serializeArray(), function (i, obj) {
                    fd.append(obj.name, obj.value)
                })

                $.ajax({
                    url: BASEURL + 'Dosignin',
                    type: "post",
                    processData: false,
                    contentType: false,
                    data: fd,
                    beforeSend: function () {
                    },
                    success: function (res) {
                        if (res.status == '1')// in case genre added successfully
                        {
                            simpleAlert('success', 'Login', res.message + ' Redirecting.....', false);
                            $('#login-form')[0].reset();
                            //redirect to dashboard
                            setTimeout(function () {//redirect to dashboard after 3 seconds
                                location.href = BASEURL + 'home';
                            }, 2500);
                        } else { // in case error occuer
                            simpleAlert('error', 'Login', res.message, true);
                            return false;
                        }
                    },
                    error: function (e) {

//                        App.stopPageLoading();
                        swal({
                            title: "Error",
                            text: e.statusText,
                            type: "error",
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Try Again",
                        });
                        //return false
                        return false;
                    },
                    complete: function () {
//                        App.stopPageLoading();
                    }
                }, "json");
                return false;
            }
        });
    }


    var handleForgetPassword = function (e) {
        forgot_validation = $('#forget-form').validate({
            errorElement: 'div', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                email_fp: {
                    required: true,
                    email: true,
                    remote: {
                        url: "checkuseremailexists",
                        type: "get"
                    },
                }
            },
            messages: {
                email_fp: {
                    required: "Email address is required.",
                    remote: "Email not exists."
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit 
                $('.alert-danger', $('#forget-form')).show();
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            errorPlacement: function (error, element) {
                var container = $('<div />');
                container.addClass('Ntooltip'); // add a class to the wrapper
                error.insertAfter(element);
                error.wrap(container);
                $("<div class='errorImage'></div>").insertAfter(error);
            },
            submitHandler: function (form) {
                //define form data
                var fd = new FormData();
                //append data                
                $.each($('#forget-form').serializeArray(), function (i, obj) {
                    fd.append(obj.name, obj.value)
                })

                $.ajax({
                    url: BASEURL + 'forgotpassword?email=' + $('#email_fp').val(),
                    type: "get",
                    processData: false,
                    contentType: false,
//                    data: {'email_fp': $('#email_fp').val()},
                    beforeSend: function () {
                        $(".spinner-loader").show();
                        console.log('before send');
                    },
                    success: function (res) {
                        if (res.status == '1')// in case genre added successfully
                        {
                            //redirect to dashboard
                            simpleAlert('success', 'Forgot Password', res.message, true);
                            $('#forget-form')[0].reset();
                            $('.close').click();
                            return false;
                        } else { // in case error occue
                            simpleAlert('error', 'Forgot Password', res.message, true);
                            return false;
                        }
                    },
                    error: function (e) {
                        //called when there is an error
                        simpleAlert('success', 'Forgot Password', 'Try ater.', true);
                        return false;
                    },
                    complete: function () {
                    }
                }, "json");
                return false;
            }
        });
    }
    jQuery.validator.addMethod("alphanumeric", function(value, element) {
        return this.optional(element) || /^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+*!=]).*$/i.test(value);
    }, "Please use a password which comprises of 8 to 16 characters, with at least 1 uppercase letter, 1 lowercase letter and 1 special characters and 1 number.");

    $('#form_reset_pwd').validate({
        errorElement: 'div', //default input error message container
        errorClass: 'help-block', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            password: {
                required: true,
                minlength: 8,
                alphanumeric: true,
            },
            password_confirmation: {
                required: true,
                equalTo: "#password",
                alphanumeric: true,
            }
        },
        messages: {
            email_fp: {
                required: "Password is required.",
                alphanumeric: "Please use a password which comprises of 8 to 16 characters, with at least 1 uppercase letter, 1 lowercase letter and 1 special characters and 1 number.",
            },
            password_confirmation: {
                required: "Confirm password is required.",
                alphanumeric: "Please use a password which comprises of 8 to 16 characters, with at least 1 uppercase letter, 1 lowercase letter and 1 special characters and 1 number.",
            }
        },
        invalidHandler: function (event, validator) { //display error alert on form submit 
            $('.alert-danger', $('#form_reset_pwd')).show();
        },
        highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
            label.remove();
        },
        errorPlacement: function (error, element) {
            var container = $('<div />');
            container.addClass('Ntooltip'); // add a class to the wrapper
            error.insertAfter(element);
            error.wrap(container);
            $("<div class='errorImage'></div>").insertAfter(error);
        },
        submitHandler: function (form) {
            //define form data
            var fd = new FormData();
            //append data                
            $.each($('#form_reset_pwd').serializeArray(), function (i, obj) {
                fd.append(obj.name, obj.value)
            })
            $.ajax({
                url: BASEURL + 'Doresetpassword',
                type: "POST",
                processData: false,
                contentType: false,
                data: fd,
                beforeSend: function () {
                    console.log('before send');
                },
                success: function (res) {
                    if (res.status == '1')// in case genre added successfully
                    {
                        //redirect to dashboard
                        simpleAlert('success', 'Forgot Password', res.message, false);
                        setTimeout(function () {//redirect to home after 3 seconds
                            if(res.data=='DistributorsTeamMembers'){
                                location.href = BASEURL + 'distributor/team-login';
                            } else if (res.data=='Distributors') {
                                location.href = BASEURL + 'distributor';
                            } else if (res.data=='User') {
                                location.href = BASEURL + 'ignite-login';
                            } else {
                                location.href = BASEURL + 'sixteen-login';
                            }
                        }, 2500);
                        return false;
                    } else { // in case error occue
                        simpleAlert('error', 'Forgot Password', res.message, true);
                        return false;
                    }
                },
                error: function (e) {
                    //called when there is an error
                    simpleAlert('error', 'Forgot Password', 'Try later.', true);
                    return false;
                },
                complete: function () {
//                        App.stopPageLoading();
                }
            }, "json");
            return false;
        }
    });
    $('#form_reset_pwd_ignite').validate({
        errorElement: 'div', //default input error message container
        errorClass: 'help-block', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            password: {
                required: true,
                minlength: 8,
            },
            password_confirmation: {
                required: true,
                equalTo: "#password",
            }
        },
        messages: {
            password: {
                required: "Password is required.",
                minlength:"8 - 16 characters.",
                alphanumeric: "Please use a password which comprises of 8 to 16 characters, with at least 1 uppercase letter, 1 lowercase letter and 1 number.",
            },
            password_confirmation: {
                required: "Confirm password is required.",
                equalTo:"Password should be the same.",
                alphanumeric: "Please use a password which comprises of 8 to 16 characters, with at least 1 uppercase letter, 1 lowercase letter and 1 number.",
            }
        },
        invalidHandler: function (event, validator) { //display error alert on form submit 
            $('.alert-danger', $('#form_reset_pwd_ignite')).show();
        },
        highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
            label.remove();
        },
        errorPlacement: function (error, element) {
            var container = $('<div />');
            container.addClass('Ntooltip'); // add a class to the wrapper
            error.insertAfter(element);
            error.wrap(container);
            $("<div class='errorImage'></div>").insertAfter(error);
        },
        submitHandler: function (form) {
            //define form data
            var fd = new FormData();
            //append data                
            $.each($('#form_reset_pwd_ignite').serializeArray(), function (i, obj) {
                fd.append(obj.name, obj.value)
            })
            $.ajax({
                url: BASEURL + 'Doresetpassword',
                type: "POST",
                processData: false,
                contentType: false,
                data: fd,
                beforeSend: function () {
                    console.log('before send');
                },
                success: function (res) {
                    if (res.status == '1')// in case genre added successfully
                    {
                        //redirect to dashboard
                        simpleAlert('success', 'Forgot Password', res.message, false);
                        setTimeout(function () {//redirect to home after 3 seconds
                            if(res.data=='DistributorsTeamMembers'){
                                location.href = BASEURL + 'distributor/team-login';
                            } else if (res.data=='Distributors') {
                                location.href = BASEURL + 'distributor';
                            } else if (res.data=='User') {
                                location.href = BASEURL + 'ignite-login';
                            } else {
                                location.href = BASEURL + 'sixteen-login';
                            }
                        }, 2500);
                        return false;
                    } else { // in case error occue
                        simpleAlert('error', 'Forgot Password', res.message, true);
                        return false;
                    }
                },
                error: function (e) {
                    //called when there is an error
                    simpleAlert('error', 'Forgot Password', 'Try later.', true);
                    return false;
                },
                complete: function () {
//                        App.stopPageLoading();
                }
            }, "json");
            return false;
        }
    });
    $('#form_reset_pwd_ignite_chinese').validate({
        errorElement: 'div', //default input error message container
        errorClass: 'help-block', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            password: {
                required: true,
                minlength: 8,
                alphanumeric: true,
            },
            password_confirmation: {
                required: true,
                equalTo: "#password",
                alphanumeric: true,
            }
        },
        messages: {
            password: {
                required: "需要密码 。",
                minlength:"8-16个字符",
                alphanumeric: "请使用由8到16个字符组成的密码，其中至少有1个大写字母、1个小写字母和1个数字。",
            },
            password_confirmation: {
                required: "密码应该相同。",
                equalTo:"密码应该相同。",
                alphanumeric: "请使用由8到16个字符组成的密码，其中至少有1个大写字母、1个小写字母和1个数字。",
            }
        },
        invalidHandler: function (event, validator) { //display error alert on form submit 
            $('.alert-danger', $('#form_reset_pwd_ignite_chinese')).show();
        },
        highlight: function (element) { // hightlight error inputs
            $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
            label.remove();
        },
        errorPlacement: function (error, element) {
            var container = $('<div />');
            container.addClass('Ntooltip'); // add a class to the wrapper
            error.insertAfter(element);
            error.wrap(container);
            $("<div class='errorImage'></div>").insertAfter(error);
        },
        submitHandler: function (form) {
            //define form data
            var fd = new FormData();
            //append data                
            $.each($('#form_reset_pwd_ignite_chinese').serializeArray(), function (i, obj) {
                fd.append(obj.name, obj.value)
            })
            $.ajax({
                url: BASEURL + 'Doresetpassword',
                type: "POST",
                processData: false,
                contentType: false,
                data: fd,
                beforeSend: function () {
                    console.log('before send');
                },
                success: function (res) {
                    if (res.status == '1')// in case genre added successfully
                    {
                        //redirect to dashboard
                        simpleAlert('success', 'Forgot Password', res.message, false);
                        setTimeout(function () {//redirect to home after 3 seconds
                            if(res.data=='DistributorsTeamMembers'){
                                location.href = BASEURL + 'distributor/team-login';
                            } else if (res.data=='Distributors') {
                                location.href = BASEURL + 'distributor';
                            } else if (res.data=='User') {
                                location.href = BASEURL + 'ignite-login';
                            } else {
                                location.href = BASEURL + 'sixteen-login';
                            }
                        }, 2500);
                        return false;
                    } else { // in case error occue
                        simpleAlert('error', 'Forgot Password', res.message, true);
                        return false;
                    }
                },
                error: function (e) {
                    //called when there is an error
                    simpleAlert('error', 'Forgot Password', 'Try later.', true);
                    return false;
                },
                complete: function () {
//                        App.stopPageLoading();
                }
            }, "json");
            return false;
        }
    });
    $('#login-form input').keypress(function (e) {
        if (e.which == 13) {
            $('#login-form').submit();
            return false;
        }
    });
    $('.forget-form input').keypress(function (e) {
        if (e.which == 13) {
            $('.forget-form').submit();
            return false;
        }
    });
    $('#forgot_link').click(function () {
        login_validation.resetForm();
        forgot_validation.resetForm();
    });
    return {
        //main function to initiate the module
        init: function () {
            handleLogin();
            handleForgetPassword();
        }
    };
}();
jQuery(document).ready(function () {
    Login.init();
    $('#btnETLSignup').click(function (e) {
        $('#formSignup').validate({
            errorElement: 'div', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: true, // do not focus the last invalid input
            rules: {
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                email_address: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                    minlength: 6
                },
                contact: {
                    required: true,
                    number: true
                },
                country_id: {
                    required: true
                },
                province_id: {
                    required:
                            function () {
                                if ($('#country_id').val() == '44') {
                                    $('#province_id').removeAttr('disabled');
                                    return true;
                                } else {
                                    $('#province_id').attr('disabled', 'true');
                                    return false;
                                }
                            }
                },
                city_id: {
                    required: true
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit   
                $('.alert-danger', $('#formSignup')).show();
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
//            errorLabelContainer: '.errorTxt',
            errorPlacement: function (error, element) {
                var container = $('<div />');
                container.addClass('Ntooltip'); // add a class to the wrapper
                error.insertAfter(element);
                error.wrap(container);
                $("<div class='errorImage'></div>").insertAfter(error);
//                if (element.is(':checkbox')) {
//                    error.insertAfter(element.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline"));
//                } else if (element.is(':radio')) {
//                    error.insertAfter(element.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline"));
//                } else {
//                    error.insertAfter(element); // for other inputs, just perform default behavior
//                }
            },
            submitHandler: function (form) {


            }
        });
        if ($('#formSignup').valid()) {
//define form data
            var fd = new FormData();
            //append data                
            $.each($('#formSignup').serializeArray(), function (i, obj) {
                fd.append(obj.name, obj.value)
            })

            $.ajax({
                url: BASEURL + 'Dosignup',
                type: "post",
                processData: false,
                contentType: false,
                data: fd,
                beforeSend: function () {
                },
                success: function (res) {
                    if (res.status == 1)// in case genre added successfully
                    {
                        simpleAlert('success', 'Login', res.message, false);
                        setTimeout(function () {//redirect to home after 3 seconds
                            location.href = BASEURL + 'signin';
                        }, 2500);
                    } else { // in case error occuer
                        simpleAlert('error', 'Login', res.message, true);
                        return false;
                    }
                },
                error: function (e) {
                    simpleAlert('error', 'Login', 'Try later.', true);
                    return false;
                },
                complete: function () {
                }
            }, "json");
            return false;
        }
    });
    $("#country_id").change(function () {
        if ($("#country_id").val() != '') {
            if ($('#country_id').val() == 44) {
                $('#province_id').removeAttr('disabled');
            } else {
                $('#province_id').attr('disabled', 'true');
            }
            $.ajax({
                url: BASEURL + 'Getcities?id=' + $("#country_id").val(),
                type: "get",
                processData: false,
                contentType: false,
                beforeSend: function () {
                },
                success: function (res) {
                    if (res.status == 1)// in case genre added successfully
                    {
                        $('#city_id')
                                .empty()
                                .append('<option selected="selected" value="">City:</option>');
                        $.each(res.data, function (i, item) {
                            $('#city_id').append($('<option>', {
                                value: item.id,
                                text: item.name
                            }));
                        });
                    }
                }
            }, "json");
        } else {
            $("#city_id").val('');
        }
    });
    $(".logout_link").click(function () {
        $.ajax({
            url: BASEURL + 'Logout',
            type: "get",
            processData: false,
            contentType: false,
            beforeSend: function () {
            },
            success: function (res) {
                console.log(res);
                if (res.status == 1)// in case genre added successfully
                {
                    location.href = BASEURL + 'sixteen-login';
                }
            }
        }, "json");
    });
    $("#signup_link").click(function () {
        location.href = BASEURL + 'signup';
    });
    $("#signin_link").click(function () {
        location.href = BASEURL + 'signin';
    });
});


app = angular.module('sixcloudsellerApp', ['ngRoute','pascalprecht.translate']);
/*var myApp = angular.module('sixcloudAboutApp', []);*/

// if (Cookies.get('language') == 'undefined' || Cookies.get('language') == '' || Cookies.get('language') == undefined) Cookies.set('language', 'en');
app.config(function($routeProvider, $translateProvider) {
    $translateProvider.translations('en', {
    	lbl_lang_chng:'中文',
        lbl_sixclouds:            'SixClouds',
        lbl_skills:               'SKILLS',
        lbl_portfolio:            'PORTFOLIO',
        lbl_real_jobs:            "REAL JOBS",
        lbl_unique_matrix_system: 'UNIQUE MATRIX SYSTEM',
        lbl_inspiration_bank:     'INSPIRATION BANK',
        lbl_creative_skills:      'Showcase your creative skills',
        lbl_sign_up:              'Sign Up',
        lbl_skill_text:           'Don’t let your creative skills go to waste. Let SIXTEEN be your bridge to a professional career.',
        lbl_wide_variety:         'Wide Variety',
        lbl_wid_variety_text:     'SIXTEEN provides numerous creative categories for you to showcase your skills.',
        lbl_build_portfolio:      'Build your Portfolio',
        lbl_beef_portfolio:       'Beef up your Portfolio',
        lbl_portfolio_text:       'Get opportunities to perform real creative jobs while you study.',
        lbl_easy_connect_buyer:   'SIXTEEN makes it easy to connect with Buyers',
        lbl_suggestions:          'Effective Suggestions',
        lbl_suggestions_text:     'Our proprietary algorithm matches you to Projects that best fit your skill sets.',
        lbl_grow_network:         'Grow your network',
        lbl_grow_network_text:    'SIXTEEN features top Sellers on a regular basis, generating greater exposure to prospective Buyers.',
        lbl_improve_your_service: 'Know where to improve your service',
        lbl_improvement:          'Continuous improvement',
        lbl_improvement_text:     ' Our unique matrix system covering the 5 key aspects of Quality, Creativity, Comprehension, Timeliness and Responsiveness, tells you where you stand and which areas you need to improve to provide better service.',
        lbl_inspired:             'Get inspired',
        lbl_creative_juices:      'Ran out of creative juices?',
        lbl_creative_juices_text: 'Look into our Inspiration Bank to develop your project ideation.',
        lbl_contribute:           'Contribute',
        lbl_contribute_text:      'You can also share your creative ideas to help other Sellers in their project ideation for prospective Buyers.',
        lbl_about_us:             'About Us',
        lbl_ignite:               'Ignite',
        lbl_sixteen:              'SIXTEEN',
        lbl_discover:             'Discover',
        lbl_proof_reading:        'Proof Reading',
        lbl_quick_links:          'Quick Links',
        lbl_become_seller:        'Become a Seller',
        lbl_become_buyer:         'Become a Buyer',
        lbl_customer_support:     'Customer Support',
        lbl_terms_service:        'Terms of Service',
        lbl_privacy_policy:       'Privacy Policy',
        lbl_all_rights_reserved:  'All rights reserved',
        lbl_the_company:          'The Company',
        lbl_core_values:          'Core Values',
        lbl_our_team:             'Our Team',
        lbl_allrights:            'All rights reserved',


    }).translations('chi', {
        lbl_lang_chng:            'English',
        lbl_sixclouds:            '六云',
        lbl_skills:               '技能',
        lbl_portfolio:            '作品',
        lbl_real_jobs:            '优质工作',
        lbl_unique_matrix_system: '独特矩阵系统',
        lbl_inspiration_bank:     '灵感库',
        lbl_creative_skills:      '   展示您的创意技能',
        lbl_sign_up:              '注册     ',
        lbl_skill_text:           '别浪费您的创意技能。 让十六岁成为您职业生涯的桥梁。',
        lbl_wide_variety:         '多种类别',
        lbl_wid_variety_text:     '十六提供许多创意类别让您展示您的技能。',
        lbl_build_portfolio:      '建立您的作品',
        lbl_beef_portfolio:       '加强您的作品',
        lbl_portfolio_text:       '在求学的同时获得机会去完成真正的工作。',
        lbl_easy_connect_buyer:   '十六让您轻松与买家联系',
        lbl_suggestions:          '建议',
        lbl_suggestions_text:     '我们专有的匹配算法会匹配您与最适合您技能的项目。',
        lbl_grow_network:         '扩大您的人脉 ',
        lbl_grow_network_text:    '十六定期会在我们的布告栏推广畅销卖家，这能让更多人看到您的服务。',
        lbl_improve_your_service: '了解如何改善您的服务',
        lbl_improvement:          '持续进步',
        lbl_improvement_text:     '我们独特的矩阵系统涵盖了质量，创造力，理解力，及时性和响应性五个关键点。这会告诉您现有的状态以及您需要改进哪些领域以提供更好的服务。',
        lbl_inspired:             '获得灵感',
        lbl_creative_juices:      '在寻找灵感？',
        lbl_creative_juices_text: '看看我们的灵感库来开发您的项目构想。',
        lbl_contribute:           '回馈',
        lbl_contribute_text:      '您也可以分享您的作品进灵感库，帮助其他卖家。',
        lbl_about_us:             '关于我们',
        lbl_ignite:               '点燃',
        lbl_sixteen:              '十六',
        lbl_discover:             '发现',
        lbl_proof_reading:        '证明阅读',
        lbl_quick_links:          '快速链接',
        lbl_become_seller:        '成为卖家',
        lbl_become_buyer:         '成为买家',
        lbl_customer_support:     '客服中心',
        lbl_terms_service:        '服务条款',
        lbl_privacy_policy:       '隐私政策',
        lbl_all_rights_reserved:  '版权所有',      
        lbl_the_company:          '六云简介',
        lbl_core_values:          '核心理念',
        lbl_our_team:             '我们的团队', 
        lbl_allrights:            '版权所有',



    });
    $translateProvider.preferredLanguage(Cookies.get('language'));
});
/* To change the interolate provider need to change it's originonal brackets.*/
app.config(['$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
}]);
/* for changing title */
app.run(['$location', '$rootScope', '$http', '$timeout', function($location, $rootScope, $http, $timeout) {
    $rootScope.$on('$routeChangeSuccess', function(event, current, previous) {
        if (current.$$route != undefined) $rootScope.title = current.$$route.title;
        document.body.scrollTop = document.documentElement.scrollTop = 0;
    });
}]);
app.controller('SellerController', ['$scope', '$translate', '$rootScope', '$timeout', function ($scope, $translate, $rootScope, $timeout){
	console.log("Seller Controller");
	
	var ctrl = this;
    if (Cookies.get('language') == 'undefined' || Cookies.get('language') == '' || Cookies.get('language') == undefined) {
        $scope.url = window.location.host;
        $scope.url = $scope.url.split('.');
        $scope.url = $scope.url[2];
        $timeout(function() {
            if($scope.url==undefined){
                $timeout(function() {
                    $scope.changeLang('en');
                },500);
            }
            if($scope.url == 'net' && $scope.url!=undefined){
                $timeout(function() {
                    $scope.changeLang('en');
                },500);
            }
            if($scope.url == 'cn' && $scope.url!=undefined){
                $timeout(function() {
                    $scope.changeLang('chi');
                },500);
            }
        },1000);
    }
    $scope.changeLang = function(lang) {
        if (lang == 'en') $rootScope.eng = false;
        else if (lang == 'chi') $rootScope.eng = true;
        var getLanguage = lang;
        ctrl.language = getLanguage;
        Cookies.set('language', ctrl.language);
        $translate.use(ctrl.language);
    }
}]);

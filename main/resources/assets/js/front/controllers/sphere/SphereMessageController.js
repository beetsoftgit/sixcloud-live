/*
 Author  : Jainam Shah
 Date    : 15th March, 2019
 Name    : SphereMessageController
 Purpose : All the functions for sphere messages
 */
angular.module('sixcloudApp').controller('SphereMessageController', ['spherefactory', '$location', '$rootScope', '$http', '$scope', '$timeout', 'userfactory', function(spherefactory, $location, $rootScope, $http, $scope, $timeout, userfactory) {
    
    // $scope.url = $location.url();

    $timeout(function(){
        $scope.loginUser = $rootScope.loginUserData;
    }, 1000);
    
    $scope.teachersList = [];
    $scope.paginateTeachers = [];
    $scope.getTeachers = function(pageNumber) {
        if(pageNumber===undefined) {
            pageNumber = '1';
        }
        spherefactory.Getteacherslist({'pageNumber' : pageNumber, 'per_page' : 7}).then(function(response){
            if(response.data.status==1) {
                $scope.paginateTeachers = response.data.data;
                $scope.teachersList = $scope.teachersList.concat(response.data.data.data);
                $scope.teachersList = _.uniq($scope.teachersList, 'id');

                if($scope.teachersList.length < $scope.paginateTeachers.total) {
                    $('#teacherClass').css('height', '390px');
                    $('#teacherClass').css('overflow-y', 'scroll');
                }
            }
        });
    }
    $scope.getTeachers();
        
    $scope.chatData = [];
    $scope.paginate = [];
    $scope.unreadCount = 1;
    $scope.paginate.total = 1;
    $scope.getTeacherChat = function(id, pageNumber, isNew) {
        if(pageNumber===undefined) {
            pageNumber = '1';
        }
        if($scope.id != id || isNew == true) {
            $scope.chatData = [];
        }
        if($scope.paginate.total == 0) {
            $scope.paginate.total = 1;
        }
        $scope.id = id;
        index = $scope.teachersList.findIndex(x => x.id==id);
        $scope.selectedTeacher = $scope.teachersList[index];

        if($scope.paginate.total > $scope.chatData.length || isNew == true) {
            spherefactory.Getteacherchat({'teacher_id': $scope.selectedTeacher.id, 'pageNumber' : pageNumber}).then(function(response){
                if(response.data.status==1) {
                    $scope.chatData = response.data.data.details.data.concat($scope.chatData);
                    $scope.chatData = _.sortBy($scope.chatData, 'created_at');
                    $scope.chatData = _.uniq($scope.chatData, 'id');
                    $scope.paginate = response.data.data.details;
                    $scope.boardId = response.data.data.message_board_id;
                    $scope.unreadCount = response.data.data.total_unread;
                } else {
                    simpleAlert('error', '', response.data.message);
                }

                $('#chatBox').css('height', '300px');
                $('.teacher-messaging-block ul').css('overflow', '');
                $('#loader').hide();

                if($scope.paginate.total < 5) {
                    $('#chatBox').css('overflow-x', '');
                } else {
                    $('#chatBox').css('overflow-x', 'scroll');
                }

                setTimeout(function() {
                    if($scope.chatData.length < $scope.paginate.total) {
                       $('#chatBox').css('overflow-y', 'scroll');
                    } else {
                        $('#chatBox').css('overflow-y', '');
                    }
                    if($scope.paginate.current_page == 1) {
                        $("#chatBox").scrollTop($("#chatBox")[0].scrollHeight);
                    } else if(isNew == true) {
                        $("#chatBox").scrollTop($("#chatBox")[0].scrollHeight);
                        $scope.readMsg();
                    }
                    else
                        $("#chatBox").scrollTop(80);
                },100);
            });
        }
    }

    $scope.readMsg = function() {
        spherefactory.Makemessageread({'sphere_message_board_id': $scope.boardId}).then(function(response){
            if(response.data.status==1) {
                $scope.getTeachers();
            }
        });
    }

    $('#chatBox').scroll(function() {
        if($scope.selectedTeacher) {
            if($scope.selectedTeacher.unread_msg_count >= 1) {
                $scope.readMsg();
            }
        }
        if ($('#chatBox').scrollTop() == 0) {
            $('#loader').show();
            setTimeout(function() {
                if($scope.paginate.last_page > $scope.paginate.current_page) {
                    $scope.getTeacherChat($scope.id, $scope.paginate.current_page + 1);
                    // $('#chatBox').scrollTop(80);
                }
                $('#loader').hide();
            },100);
        }
    });

    $('#teacherClass').scroll(function() {
        if ($('#teacherClass').scrollTop() != 0) {
            $('#loader1').show();
      
            setTimeout(function() {
                if($scope.paginateTeachers.last_page > $scope.paginateTeachers.current_page) {
                    $scope.getTeachers($scope.paginateTeachers.current_page + 1);
                    // $('#teacherClassList').scrollTop(130);
                }
                $('#loader1').hide();
            },100);
        }
    });

    $("#msgbox").on("keydown", function (e) {
        if (e.keyCode === 13) {  //checks whether the pressed key is "Enter"
            if($scope.msgbox != undefined && $scope.msgbox != '') {
                $scope.sendMessage($scope.msgbox);
            }
        }
    });
    
    $scope.sendMessage = function(msg) {
        $scope.chatData = [];
        $scope.paginate = [];
        $("#msgbox").val('');
        
        var msgData = {'sphere_message_board_id' : $scope.boardId,
                       'teacher_id' : $scope.selectedTeacher.id,
                       'message' : msg};
        var json = JSON.stringify(msgData);
        spherefactory.Sendmessage(json).then(function(response){
            if(response.data.status==1) {
                $scope.getTeacherChat($scope.selectedTeacher.id, $scope.paginate.current_page, true);
            } else {
                simpleAlert('error', '', response.data.message);
            }
        });
    }

    $scope.closeChat = function() {
        $scope.selectedTeacher = undefined;
        $scope.chatData = [];
        $('#chatBox').css('height', '400px');
        $('#chatBox').css('overflow', '');
    }

}]);

app.filter('duration', function($filter) {
    return function(input) {
        if (input == null) {
            return '';
        }

        let currentDate = moment()
        let finishDate = moment.utc(input).local();

        let difference = currentDate.diff(finishDate)
        let diff = moment.duration(difference)
        let statusMsg = "Just now"

        if (diff.years() >= 1) {
          statusMsg = finishDate.format('DD MMM, YYYY, h:mm a')
        }else if (diff.days() > 1) {
          statusMsg = finishDate.format('DD MMM, h:mm a')
        }else if (diff.days() == 1) {
          statusMsg = "yesterday "+finishDate.format('h:mm a')
        }else if (diff.hours() > 1 || diff.minutes() > 3) {
          statusMsg = finishDate.format('h:mm a')
        }else if (diff.minutes() > 1) {
          statusMsg = diff.minutes() +" minutes ago"
        }else if (diff.minutes() == 1) {
          statusMsg = diff.minutes() +" minute ago"
        }else if (diff.seconds() > 1) {
          statusMsg = diff.seconds() +" seconds ago"
        }
      return statusMsg
    }
});
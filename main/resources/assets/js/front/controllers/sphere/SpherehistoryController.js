/*
 Author  : Senil Shah
 Date    : 9th April, 2019
 Name    : SpherehistoryController
 Purpose : All the functions for sphere history
 */
angular.module('sixcloudApp').controller('SpherehistoryController', ['spherefactory', '$location', '$rootScope', '$http', '$scope', '$timeout', 'Getmycourses', function(spherefactory, $location, $rootScope, $http, $scope, $timeout, Getmycourses) {

    $scope.myCourses = Getmycourses.data.data;
    
    $scope.filter = {
        searchText : undefined,
        course : 0,
        fromDate : undefined,
        toDate : undefined,
    };

    $scope.gethistories = function(pageNumber) {
        if(pageNumber===undefined) {
            pageNumber = '1';
        }
        spherefactory.Getspherehistories({'filters' : $scope.filter},pageNumber).then(function(response){
            if(response.data.status==1) {
                $scope.histories     = response.data.data.data;
                $scope.total         = response.data.data.total;
                $scope.paginate      = response.data.data;
                $scope.currentPaging = response.data.data.current_page;
                $scope.rangepage     = _.range(1, response.data.data.last_page + 1);
            }
        });
    }
    $scope.gethistories();

    $scope.filterList = function() {
      $scope.searchText = ($scope.searchText == undefined || $scope.searchText == '')?undefined:$scope.searchText;
      $scope.gethistories();
    }
    $(function(){
        $( ".datepicker" ).datetimepicker({
            pickTime: false,
            format: 'DD/MM/YYYY',
            maxDate: new Date(),
        });
    });
}]);
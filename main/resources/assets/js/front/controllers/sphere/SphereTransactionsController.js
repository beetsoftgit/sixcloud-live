/*
 Author  : Senil Shah
 Date    : 9th April, 2019
 Name    : SphereTransactionsController
 Purpose : All the functions for sphere transactions
 */
angular.module('sixcloudApp').controller('SphereTransactionsController', ['spherefactory', '$location', '$rootScope', '$http', '$scope', '$timeout', function(spherefactory, $location, $rootScope, $http, $scope, $timeout) {
    
    $scope.searchText = undefined;

    $scope.getTransactions = function(pageNumber) {
        if(pageNumber===undefined) {
            pageNumber = '1';
        }
        spherefactory.Getspheretransactions({'filters' : $scope.searchText},pageNumber).then(function(response){
            if(response.data.status==1) {
                $scope.transactions  = response.data.data.data;
                $scope.total         = response.data.data.total;
                $scope.paginate      = response.data.data;
                $scope.currentPaging = response.data.data.current_page;
                $scope.rangepage     = _.range(1, response.data.data.last_page + 1);
            }
        });
    }
    $scope.getTransactions();

    $scope.filterList = function() {
      $scope.searchText = ($scope.searchText == undefined || $scope.searchText == '')?undefined:$scope.searchText;
      $scope.getTransactions();
    }

}]);
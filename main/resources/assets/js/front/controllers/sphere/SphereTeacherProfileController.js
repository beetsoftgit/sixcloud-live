/*
 Author  : Jainam Shah
 Date    : 22nd March, 2019
 Name    : SphereTeacherProfileController
 Purpose : All the functions for sphere teachers
 */
angular.module('sixcloudApp').controller('SphereTeacherProfileController', ['spherefactory', '$location', '$rootScope', '$http', '$scope', '$timeout', '$routeParams', function(spherefactory, $location, $rootScope, $http, $scope, $timeout, $routeParams) {
    
  $teacher_slug = $routeParams.slug;

  $('.tabclickme').click(function(){  
    $('.tabclickme').removeClass('active');
    $(this).addClass('active');
    var tagid = $(this).data('tag');
    $('.tabcontentdiv').removeClass('current').addClass('hide');
    $('#'+tagid).addClass('current').removeClass('hide');
  });
  
  $('.tabclickmeCA').click(function(){  
    $('.tabclickmeCA').removeClass('active');
    $(this).addClass('active');
    var tagid = $(this).data('tag');
    $('.tabcontentdivCA').removeClass('current').addClass('hide');
    $('#'+tagid).addClass('current').removeClass('hide');
  });
  
  $scope.getTeacherData = function() {
    spherefactory.Getteacherdata({'slug' : $teacher_slug}).then(function(response){
      if(response.data.status==1) {
        $scope.teacherData = response.data.data;
      }
    });
  }
  $scope.getTeacherData();

  $scope.redirectToTeachers = function() {
    window.location.href = "teachers";
  }

}]);
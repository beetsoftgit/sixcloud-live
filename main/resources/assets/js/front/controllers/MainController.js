/*
 Author  : Komal Kapadi (komal@creolestudios.com)
 Date    : 5th December 2017
 Name    : MainController
 Purpose : All the functions for main common functionalities which are required to be global
 */
angular.module('sixcloudApp').controller('MainController', ['$translate','mpfactory','userfactory','ignitefactory', '$location', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($translate,mpfactory,userfactory,ignitefactory, $location, $http, $scope, $timeout, $rootScope, $filter) {
    console.log('main controller loaded');
    $('.logout_link').hide();
    $rootScope.ELT_PLACEHOLDER = VIDEO_PLACEHOLDER;
    $rootScope.BASEURL         = BASEURL;
    $rootScope.domainURL       = completeUrl+'alipay-antfinancial/pay.php';
    $rootScope.domainIgniteURL = completeUrl+'alipay-antfinancial/pay-ignite.php';
    $rootScope.current_domain  = current_domain;
    $rootScope.transLang       = Cookies.get('language');
    $rootScope.nodatafound = '';
    $scope.changeLangs = Cookies.get('language');
    $timeout(function() {
        $scope.changeLangs = Cookies.get('language');
        if (Cookies.get('language') == 'undefined' || Cookies.get('language') == '' || Cookies.get('language') == undefined) {
            if(current_domain=='net'){
                $translate.use('en');
                $scope.changeLangs = 'en';
                Cookies.set('language', 'en');
                $scope.changeLangs = Cookies.get('language');
            } else {
                $translate.use('chi');
                $scope.changeLangs = 'chi';
                Cookies.set('language', 'chi');
                $scope.changeLangs = Cookies.get('language');
            }
        } else {
            $timeout(function() {
                $scope.changeLanguage($scope.changeLangs);
                if($scope.changeLangs=='en'){
                    $scope.selectedLang = true;
                    $scope.changeLangs = 'en';
                    Cookies.set('language', 'en');
                    $scope.changeLangs = Cookies.get('language');
                } else if($scope.changeLangs=='chi') {
                    $scope.selectedLang = false;
                    $scope.changeLangs = 'chi';
                    Cookies.set('language', 'chi');
                    $scope.changeLangs = Cookies.get('language');
                } else {
                    $scope.selectedLang = false;
                    $scope.changeLangs = 'ru';
                    Cookies.set('language', 'ru');
                    $scope.changeLangs = Cookies.get('language');
                }
            }, 1000);
        }
    }, 1000);
    $scope.url = $location.url();
    
    $('#after-login').hide();
    $('.mp_without_login').show();
    $('.mp_with_login').hide();
    //$rootScope.isLoading = true; 
    $timeout(function() {
        $('.after-login-new-head').removeClass('hide');
    }, 500);
    
    $('.header-profile-dropdown .dropdown-menu').on('click', function(e) {
        if($(this).hasClass('dropdown-menu-form')) {
            e.stopPropagation();
        }
    });

    $('.header-profile-dropdown .dropdown-menu a').on('click', function (event) {
        $('.header-profile-dropdown .dropdown').removeClass('open')
    });

    $('#sub-menu-item li').click(function () {
        $('#sub-menu-item li').removeClass('active')
        $(this).addClass('active');

    });
    $timeout(function(){
        if($scope.module!='ignite'){
            userfactory.Getloginuserdata().then(function(response) {
                if (response.data.status == 1) {
                    $rootScope.loginUserData = response.data.data;
                    if ($rootScope.loginUserData != '') {
                        $("#after-login").show();
                        $('#signup_link').hide();
                        $('#signin_link').hide();
                        $('.logout_link').show();
                        //$('.welcome_text').text($rootScope.loginUserData.first_name + ' ' + $rootScope.loginUserData.last_name);
                        $('.welcome_text').text($rootScope.loginUserData.display_name);
                        $('.mp_without_login').hide();
                        $('.mp_with_login').show();
                        $rootScope.selectedRole = ($rootScope.loginUserData.marketplace_current_role == 1);
                        /* to get seller outstanding balance */
                        userfactory.Getoutstandingbalance().then(function(response) {
                            $scope.outstandingbalance = response.data;
                        }); 
                        userfactory.Getbuyercredit().then(function(response) {
                            $scope.credit = response.data;
                        });
                        mpfactory.Getunreadnotificationscount().then(function(response) { 
                            if (response.data.status === 0) {
                                return false;
                            }
                            $("#notification_number").text(response.data.data.totalCount);
                        });
                    }

                } else {
                    $rootScope.loginUserData = response.data;
                }
            });
        }
    },1000);
    $('.after-login-new-head').removeClass('hide');
    $timeout(function(){
        $('#role_switch').change(function() {
            if($rootScope.loginUserData.marketplace_current_role==1){
                $scope.switch_role_to = 2;
            } else {
                $scope.switch_role_to = 1;
            }
            mpfactory.Switchrole({'marketplace_current_role':$scope.switch_role_to}).then(function(response) {
                if (response.data.status == 1)
                {
                    if(response.data.data.marketplace_current_role==1){
                        location.replace('seller-dashboard');
                    } else {
                        location.replace('buyer-dashboard');
                    }
                }
                else {
                    simpleAlert('error','','Please try again');
                }
            });
        });
    },1000);

    $timeout(function(){
        if($scope.module=='ignite'){
            /*get distributor's detail*/ 
            /* this method is being used for ignte login for 1) distributors 2) distributors team members (table : ) and 3) ignite normal users (table : users) */   
            ignitefactory.Getloginuserdatadistributor().then(function(response) {
                if (response.data.status == 1) {
                    $rootScope.loginDistributorData = response.data.data;
                    $scope.video_preference = $rootScope.loginDistributorData.video_url;
                } else {
                    $rootScope.loginDistributorData = response.data;
                }
            });

            ignitefactory.getCategoriesWithLanguages().then(function(response) {
                if (response.data.status == 1) {
                    $rootScope.categoryLanguages = response.data.data;
                    $scope.ignitePageId = $rootScope.categoryLanguages[0].id;
                    angular.forEach($rootScope.categoryLanguages, function(value, key) {
                        $scope.subject_name = value.subject_name;
                    if($scope.subject_name != undefined && $scope.subject_name != '')
                    {
                        $scope.substring = $scope.subject_name.substring(0, 4);
                        if($scope.substring.toLowerCase().includes('buzz'))
                        {
                            $rootScope.Eng = 'English';
                            $scope.subengId = $rootScope.categoryLanguages.staticget[0].id;
                            // $scope.igniteId = 'English';
                        }
                        if($scope.substring.toLowerCase().includes('smil') || $scope.substring.toLowerCase().includes('maze'))
                        {
                            $rootScope.Math = 'Mathematics';
                            $scope.submathId = $rootScope.categoryLanguages.static[0].id;
                            // $scope.igniteId = 'Mathematics';
                        }

                        $scope.url = window.location.href.split('/')[6];
                        if($scope.url == undefined || $scope.url == '')
                        {
                            if(value.seeVideosFor == $scope.video_preference)
                            {
                                if($scope.substring.toLowerCase().includes('buzz'))
                                {   
                                    $rootScope.Eng = 'English';
                                    $rootScope.igniteId = 'English';
                                    $scope.ignitePageId = value.id;
                                }
                                if($scope.substring.toLowerCase().includes('smil') || $scope.substring.toLowerCase().includes('maze'))
                                {
                                    $rootScope.Math = 'Mathematics';
                                    $rootScope.igniteId = 'Mathematics';
                                    $scope.ignitePageId = value.id;
                                }
                            }
                        }
                        if($scope.url == value.seeVideosFor)
                        {
                            if($scope.substring.toLowerCase().includes('buzz'))
                            {
                                $rootScope.Eng = 'English';
                                $rootScope.igniteId = 'English';
                                $scope.ignitePageId = value.id;
                            }
                            if($scope.substring.toLowerCase().includes('smil') || $scope.substring.toLowerCase().includes('maze'))
                            {
                                $rootScope.Math = 'Mathematics';
                                $rootScope.igniteId = 'Mathematics';
                                $scope.ignitePageId = value.id;
                            }
                        }
                    }
                    });
                } else {
                    $rootScope.categoryLanguages = response.data;
                }
            });

            ignitefactory.Getuserzone().then(function (response){
                $rootScope.userZone = response.data.data
            });
        }
    },500);
    $rootScope.subheader = 'subheader';
    /*logout distributor start*/
    $scope.Logoutdistributor = function() {
        ignitefactory.Logoutdistributor().then(function(response) {
            if (response.data.status == 1) {
                $rootScope.loginDistributorData = null;
                if(response.data.data=="distributor")
                    window.location.replace("distributor");
                if(response.data.data=="team")
                    window.location.replace("distributor/team-login");
                if(response.data.data=="user")
                    window.location.replace("ignite-login");
            } else {
                simpleAlert('error','',response.data.message);
            }
        });
    }
    /*logout distributor end*/

    // Save user preference
    $scope.Seevideos = function(language, subjectId) {
        index = _.findKey($scope.categoryLanguages, function(o) { return o.id == subjectId; });
        ignitefactory.Savecategoryandlanguage({'see_videos_in':language,'see_videos_for':$scope.categoryLanguages[index].seeVideosFor}).then(function (response) {
            if(response.data.status == 1){
                window.location.replace('ignite-categories/'+$scope.categoryLanguages[index].seeVideosFor);
            } else {
                if(response.data.code==2)
                    window.location.replace("ignite-login");
                else
                    simpleAlert('error', '', response.data.message);
            }
        });
    }

    if($rootScope.loginDistributorData == ''){
        let url = window.location.href.split('/');
        if(url.includes('ignite')){
            $rootScope.ignitePage = "Home";
        }else if(url.includes('ignite-buzz') || url.includes('ignite-categories')){
            $rootScope.ignitePage = "English";
        }else if(url.includes('ignite-mathematics')){
            $rootScope.ignitePage = "Mathematics";
        }else if(url.includes('ignite-smile')){
            $rootScope.Mathematics = "Mathematics";
            $rootScope.ignitePage = "Smile";
        }else if(url.includes('ignite-maze')){
            $rootScope.Mathematics = "Mathematics";
            $rootScope.ignitePage = "Math";
        }else{
            $rootScope.Mathematics = "";
            $rootScope.ignitePage = "";
        }
    }
    // $scope.page = 'Home';
    $scope.switchTab = function(page) {
        $rootScope.ignitePage = page;
        // $rootScope.igniteId = '';
        $("#english").removeClass('active');
        $("#mathematics").removeClass('active'); 
        $("#Twinkle").removeClass('active');  
    }
    $scope.igniteCategory = function (category) {
        if($rootScope.loginDistributorData != ''){
            if($rootScope.loginDistributorData.userType == "ignite_user"){
                if($rootScope.loginDistributorData.video_language_preference != null){
                    
                    ignitefactory.Mysubscriptiondata().then(function (response) {
                        var tm = response.data.data.find(data => data.video_category_id == category.id)
                        if(tm && tm.video_category_id == category.id){
                            $scope.categoryLang = $rootScope.categoryLanguages.filter(data=> {return data.subject_display_name == "BUZZ"});
                            if($scope.categoryLang==0){
                                $scope.categoryLang = $rootScope.categoryLanguages.filter(data=> {return data.subject_display_name == "BUZZ RU"});
                            }
                            $scope.categoryLang = $scope.categoryLang[0].categories[0];
                            index = _.findKey($scope.categoryLanguages, function(o) { return o.id == $scope.categoryLang.subject_id; });
                            ignitefactory.Savecategoryandlanguage({'see_videos_in':$scope.categoryLang.language_name,'see_videos_for':$scope.categoryLanguages[index].seeVideosFor}).then(function (response) {
                                if(response.data.status == 1){
                                    $scope.page = "";
                                    $rootScope.categoryId = category.id;
                                    ignitefactory.Getuserzone().then(function (response){
                                        $scope.userZone = response.data.data
                                        if($scope.userZone.is_subscription_allowed == 0) {
                                            if($scope.changeLangs=='en'){
                                                title = "";
                                                text = "Please contact customer support for more information.";
                                                confirmButtonText = "Ok";
                                            }else if($scope.changeLangs=='chi'){
                                                title = "";
                                                text = "请联系客服中心以获取更多详情。";
                                                confirmButtonText = "确定";
                                            }else if($scope.changeLangs=='ru'){
                                                title = "";
                                                text = "Пожалуйста, свяжитесь со службой поддержки для получения дополнительной информации.";
                                                confirmButtonText = "Ладно";
                                            }
                                            if(tm.video_category_name == category.category_name){
                                                var now = new Date();
                                                var end = new Date(tm.subscription_end_date);
                                                if(((end.getTime() - now.getTime()) / 1000) > 0){
                                                    $location.path('ignite-categories/'+$scope.categoryLanguages[index].seeVideosFor);
                                                }else{
                                                    swal({
                                                        title: title,
                                                        text: text,
                                                        confirmButtonText: confirmButtonText,
                                                        allowEscapeKey:false
                                                    }, function() {
                                                        window.location.href = "customer-support";
                                                    });
                                                }
                                            }else{
                                                swal({
                                                    title: title,
                                                    text: text,
                                                    confirmButtonText: confirmButtonText,
                                                    allowEscapeKey:false
                                                }, function() {
                                                    window.location.href = "customer-support";
                                                });
                                            }
                                        }else{
                                            if(category.subject_zones.zones != $scope.userZone.id){
                                                if($scope.changeLangs=='en'){
                                                    title = "";
                                                    text = "Please contact customer support for more information.";
                                                    confirmButtonText = "Ok";
                                                }else if($scope.changeLangs=='chi'){
                                                    title = "";
                                                    text = "请联系客服中心以获取更多详情。";
                                                    confirmButtonText = "确定";
                                                }else if($scope.changeLangs=='ru'){
                                                    title = "";
                                                    text = "Пожалуйста, свяжитесь со службой поддержки для получения дополнительной информации.";
                                                    confirmButtonText = "Ладно";
                                                }
                                                swal({
                                                    title: title,
                                                    text: text,
                                                    confirmButtonText: confirmButtonText,
                                                    allowEscapeKey:false
                                                }, function() {
                                                    window.location.href = "customer-support";
                                                });
                                            }else{
                                                $location.path('ignite-categories/'+$scope.categoryLanguages[index].seeVideosFor);
                                            }
                                        }
                                    });

                                    // $location.path('ignite-categories/'+$scope.categoryLanguages[index].seeVideosFor);
                                } else {
                                    if(response.data.code==2)
                                        window.location.replace("ignite-login");
                                    else
                                        simpleAlert('error', '', response.data.message);
                                }
                            });
                        }else{
                            ignitefactory.Getuserzone().then(function (response){
                                $scope.userZone = response.data.data
                                if($scope.userZone.is_subscription_allowed == 0) {
                                    if($scope.changeLangs=='en'){
                                        title = "";
                                        text = "Please contact customer support for more information.";
                                        confirmButtonText = "Ok";
                                    }else if($scope.changeLangs=='chi'){
                                        title = "";
                                        text = "请联系客服中心以获取更多详情。";
                                        confirmButtonText = "确定";
                                    }else if($scope.changeLangs=='ru'){
                                        title = "";
                                        text = "Пожалуйста, свяжитесь со службой поддержки для получения дополнительной информации.";
                                        confirmButtonText = "Ладно";
                                    }
                                    swal({
                                        title: title,
                                        text: text,
                                        confirmButtonText: confirmButtonText,
                                        allowEscapeKey:false
                                    }, function() {
                                        window.location.href = "customer-support";
                                    });
                                    // simpleAlert('error', '', text);
                                }else{
                                    if(category.subject_zones.zones != $scope.userZone.id){
                                        if($scope.changeLangs=='en'){
                                            title = "";
                                            text = "Please contact customer support for more information.";
                                            confirmButtonText = "Ok";
                                        }else if($scope.changeLangs=='chi'){
                                            title = "";
                                            text = "请联系客服中心以获取更多详情。";
                                            confirmButtonText = "确定";
                                        }else if($scope.changeLangs=='ru'){
                                            title = "";
                                            text = "Пожалуйста, свяжитесь со службой поддержки для получения дополнительной информации.";
                                            confirmButtonText = "Ладно";
                                        }
                                        swal({
                                            title: title,
                                            text: text,
                                            confirmButtonText: confirmButtonText,
                                            allowEscapeKey:false
                                        }, function() {
                                            window.location.href = "customer-support";
                                        });
                                    }else{
                                        window.location.replace('my-subscription/'+window.btoa('category')+':'+window.btoa(category.id))
                                    }
                                }
                            });
                            // window.location.replace('my-subscription/'+window.btoa('category')+':'+window.btoa(category.id))
                        }
                    });
                }else{
                    window.location.replace("select-categories");
                }
            }
        }else{
            window.location.replace("ignite-login");
        }
    }
    $rootScope.showiframe = false;
    $scope.twinkleRedirect = function(){
        $rootScope.showiframe = true;
        $("#content").addClass('hide');
        $("#Twinkle").addClass('active');
        $("#english").removeClass('active');
        $("#mathematics").removeClass('active');
        $rootScope.ignitePage = '';
        $rootScope.subheader = '';
        ignitefactory.GetUserTokenForTwinkle().then(function (response) {
            if(response.data.status == 1){
                $scope.twinkleRedirectToken = response.data.data;
                $scope.user_md5str = $scope.twinkleRedirectToken[0];
                $scope.url_iframe = "https://www.proprofs.com/classroom/?ID=2757837&cid=0&embed=1&w=450&username="+$scope.loginDistributorData.email_address+"&passkey="+$scope.user_md5str;
                $('#proprofs').attr('src',$scope.url_iframe);
                $timeout(function() {
                $("#twinkleRedirectForm").submit();
            }, 500);
            } else {
                simpleAlert('error', '', 'Please try again');
            }
        });
    }

    $scope.switchCategory = function(userData, subjectId) {
        $rootScope.ignitePage = '';
        $rootScope.showiframe = false;
        $("#content").removeClass('hide');
        $("#Twinkle").removeClass('active');    
        if($rootScope.loginDistributorData != ''){
            if($rootScope.loginDistributorData.userType == "ignite_user"){
                if($rootScope.loginDistributorData.video_language_preference != null){
                        angular.forEach($rootScope.categoryLanguages, function(value, key) {
                            if(value.id == subjectId)
                            {
                                $scope.subjectname = value.subject_display_name;
                                $scope.substring =  $scope.subjectname.substring(0, 4);
                                if($scope.substring.toLowerCase().includes('buzz'))
                                {
                                    $("#english").addClass('active');
                                    $rootScope.Eng = 'English';
                                    $rootScope.igniteId = 'English';
                                    $rootScope.subheader = 'subheader';
                                }
                                else if($scope.substring.toLowerCase().includes('smil') || $scope.substring.toLowerCase().includes('maze'))
                                {
                                    $("#mathematics").addClass('active');
                                    $rootScope.Math = 'Mathematics';
                                    $rootScope.igniteId = 'Mathematics';
                                    $rootScope.subheader = 'subheader';
                                }
                                $scope.seeVideosFor = value.seeVideosFor;
                                $scope.categoryLang = value.categories[0].language_name;
                            }
                        });
                        $scope.ignitePageId = subjectId;
                        ignitefactory.Savecategoryandlanguage({'see_videos_in':$scope.categoryLang,'see_videos_for':$scope.seeVideosFor}).then(function (response) {
                            if(response.data.status == 1){
                                $location.path('ignite-categories/'+$scope.seeVideosFor);                           
                            } else {
                                if(response.data.code==2)
                                    window.location.replace("ignite-login");
                                else
                                    simpleAlert('error', '', response.data.message);
                            }
                        });
                }else{
                    window.location.replace("select-categories");
                }
            }
        }else{
            window.location.replace("ignite-login");
        }
    }

    $scope.changeLanguage = function(lang) {
        if (lang == 'en') $scope.changeLangs = 'en';
        else if (lang == 'chi') $scope.changeLangs = 'chi';
        else $scope.changeLangs = 'ru';
        Cookies.set('language', lang);
        $rootScope.transLang = $scope.changeLangs;
        $translate.use(lang);
    }
}]);
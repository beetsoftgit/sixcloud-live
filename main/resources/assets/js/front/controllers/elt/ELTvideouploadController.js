/*
 Author  : Nivedita (nivedita@creolestudios.com)
 Date    : 30th April 2018
 Name    : ELTvideouploadController
 Purpose : Upload video
 */
angular.module('sixcloudApp').controller('ELTvideouploadController', ['$sce', 'mpfactory', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, mpfactory, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {
    console.log('ELT video upload controller loaded');
    $(".animatedParent").hide();
    $(".after-login-new-head").hide();
    $(".container").hide();
    $scope.submitForm = function() {
        // return false;
        var data = new FormData($("#videoUploadform")[0]);
        console.log(data);
        showLoader('.mts-btn', 'Submit');
        mpfactory.Uploadvideodemo(data).then(function(response) {
            if (response) {
                hideLoader('.mts-btn', 'Submit');
                simpleAlert('Success', 'Signup', 'Video uploaded and transcoded', true);
            } else {
                hideLoader('.mts-btn', 'Submit');
                simpleAlert('error', 'Signup', 'Something Went Wrong ', true);
            }
        });
        //
    }

}]);
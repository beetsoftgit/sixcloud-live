/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 28th Aug 2018
 Name    : IgniteMySubscriptionController
 Purpose : All the functions for ignite users subscription page
 */
angular.module('sixcloudApp').controller('IgniteMySubscriptionController', ['$sce', 'ignitefactory', '$location', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', 'Getuserzone', function($sce, ignitefactory, $location, $routeParams, $http, $scope, $timeout, $rootScope, $filter, Getuserzone) {
    /*urlObj = JSON.parse(localStorage.getItem('urlObj'));
    url_number_static = localStorage.getItem('url_number_static');
    url_number_static++;
    url = [{
        "url_number": url_number_static,
        "url": $location.url()
    }];
    last_url = urlObj.pop();
    localStorage.setItem('urlObj',JSON.stringify(urlObj));
    if(last_url[0].url_number !== url[0].url_number) {
        urlObj.push(last_url);
    }
    urlObj.push(url);
    localStorage.setItem("urlObj",JSON.stringify(urlObj));
    localStorage.setItem("url_number_static",url_number_static);*/
    console.log('IgniteMySubscriptionController loaded');
    $("#content").removeClass('hide');
    $rootScope.showiframe = false;
    $rootScope.Twinklemenu = false;
    $scope.url = $location.url();
    // getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
    getLanguage = Cookies.get('language');
    $scope.current_domain = current_domain;
    $scope.userZone = Getuserzone.data.data;
    $scope.todayDate = new Date();
    if($scope.userZone.zone_name.toUpperCase() == 'CIS') {
        $scope.plan_duration = 3;
        $scope.price_type = 2;
    } else {
        $scope.plan_duration = 3;
        $scope.price_type = 1;
    }
    if($scope.userZone.is_subscription_allowed == 0) {
        swal({
            title: "",
            text: "Please contact to customer support",
            allowEscapeKey:false
        }, function() {
            window.location.href = "customer-support";
        });
    }
    ignitefactory.Getloginuserdatadistributor().then(function(response) {
        if (response.data.status == 1) {
            $rootScope.loginDistributorData = response.data.data;
        } else {
            $rootScope.loginDistributorData = response.data;
        }
    });

    ignitefactory.Getuserzone().then(function (response){
        $rootScope.userZone = response.data.data
    });
    $timeout(function(){
        changeLanguage(getLanguage);
        $('#chk_lang').change(function() {
            getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
            changeLanguage(getLanguage);
        });
    },500);
    function changeLanguage(lang) {
        if (lang == "chi") {
            $timeout(function(){
                $scope.changeLangs = 'chi';
            },500);
        } else if(lang == "en") {
            $timeout(function(){
                $scope.changeLangs = 'en';
            },500);
        } else {
            $timeout(function(){
                $scope.changeLangs = 'ru';
            },500);
        }
    }
    $scope.changeTab = function(whichTab){
        if(whichTab == 'SubscriptionPlanTab'){
            $('#SubscriptionPlanTabButton').addClass('active');
            $('#SubscriptionPlanTabButton').removeClass('hidden-arrow');
            $('.subjects').addClass('active');
            $('#TransactionsTabButton').removeClass('active');
            $scope.SubscriptionPlanTab = true;
            $scope.TransactionsTab = false;
        } else {
            $('#TransactionsTabButton').addClass('active');
            $('#TransactionsTabButton').removeClass('hidden-arrow');
            $('#SubscriptionPlanTabButton').removeClass('active');
            $scope.TransactionsTab = true;
            $scope.SubscriptionPlanTab = false;
        }
        $('#ActiveSubscriptionTabButton').removeClass('hidden-arrow');
        $('#PastSubscriptionTabButton').removeClass('hidden-arrow');
        $('#TransactionsSecondTabButton').removeClass('hidden-arrow');
    }
    var url = $scope.url.split('/');
    if(url[2] == 'active'){
        $scope.changeTab('TransactionsTab');
    } else {
        $scope.changeTab('SubscriptionPlanTab');
    }

    $scope.changesubTab = function(whichTab){
        // $('#TransactionsTabButton').addClass('active');
        // $('#TransactionsTabButton').removeClass('hidden-arrow');
        if(whichTab == 'ActiveSubscriptionTab'){
            $('#ActiveSubscriptionTabButton').addClass('active');
            // $('#ActiveSubscriptionTabButton').removeClass('hidden-arrow');
            $('#PastSubscriptionTabButton').removeClass('active');
            $('#TransactionsSecondTabButton').removeClass('active');
            $('.subjects').addClass('active');
            $scope.ActiveSubscriptionTab    = true;
            $scope.PastSubscriptionTab      = false;
            $scope.TransactionsSecondTab    = false;
        } else if(whichTab == 'PastSubscriptionTab'){
            $('#PastSubscriptionTabButton').addClass('active');
            // $('#PastSubscriptionTabButton').removeClass('hidden-arrow');
            $('#ActiveSubscriptionTabButton').removeClass('active');
            $('#TransactionsSecondTabButton').removeClass('active');
            $scope.ActiveSubscriptionTab    = false;
            $scope.PastSubscriptionTab      = true;
            $scope.TransactionsSecondTab    = false;
        } else {
            $('#TransactionsSecondTabButton').addClass('active');
            // $('#TransactionsSecondTabButton').removeClass('hidden-arrow');
            $('#ActiveSubscriptionTabButton').removeClass('active');
            $('#PastSubscriptionTabButton').removeClass('active');
            $scope.ActiveSubscriptionTab    = false;
            $scope.PastSubscriptionTab      = false;
            $scope.TransactionsSecondTab    = true;
        }
    }
    $scope.changesubTab('ActiveSubscriptionTab');

    $scope.GettaxrateDetails = function(){
        ignitefactory.GettaxrateDetails().then(function(response) {
            if (response.data.status == 1) {
                $scope.taxrateData = response.data.data.tax_rate;
            } else {
                $scope.taxrateData = 0;
            }
        });
    }
    $scope.GettaxrateDetails();

    $scope.cartTotal = 0;
    $scope.cartSubTotal = 0;
    $scope.taxRate = 0;
    $scope.selectedCategory = [];
    $scope.changecartCategory = [];
    $scope.Changecart = function(price_type,data,id,plan_duration){
        var amount = 0;
        if(price_type==1){
            if(plan_duration==3){
                amount = data.category_price_3month
            } else if(plan_duration==6){
                amount = data.category_price_6month
            } else if(plan_duration==12)
                amount = data.category_price
        }else{
            if(plan_duration==3)
                amount = data.category_price_sgd_3month
            else if(plan_duration==6)
                amount = data.category_price_sgd_6month
            else if(plan_duration==12)
                amount = data.category_price_sgd
        }
        $scope.cartTotal = $scope.cartTotal + $scope.discount;
        $scope.promo_code = undefined;
        $scope.discount = 0;
        $scope.changecartCategory.forEach(function(element,index) {
            if(element.subject_id == data.subject_id){
                $scope.selectedCategory.forEach(function(ele, ind) {
                    if(ele.id == element.id){
                        $scope.plan = ele.plan_duration;
                    }
                });
                var amt = 0;
                if($scope.price_type == 1) {
                    if($scope.plan == 3){
                        amt = element.category_price_3month; 
                    } else if ($scope.plan == 6) {
                        amt = element.category_price_6month;
                    } else if ($scope.plan == 12) {
                        amt = element.category_price;
                    }
                } else if($scope.price_type == 2) {
                    if($scope.plan == 3){
                        amt = element.category_price_sgd_3month; 
                    } else if ($scope.plan == 6) {
                        amt = element.category_price__sgd_6month;
                    } else if ($scope.plan == 12) {
                        amt = element.category_price_sgd;
                    }
                }
                $('#buzz_'+element.id).prop("checked",false);
                // if($scope.cartSubTotal > 0){
                    var taxRates  = amt*$scope.taxrateData/100;
                    $scope.promo_code = undefined;
                    $scope.discount = 0;
                    if(taxRates > 0){
                        var total = amt + taxRates;
                        $scope.cartTotal = $scope.cartTotal - total;
                        $scope.cartSubTotal -= amt;
                        $scope.taxRate -= taxRates;
                    } else {
                        $scope.cartTotal -= amt;
                        $scope.cartSubTotal -= amt;
                        $scope.taxRate -= taxRates;
                    }
                    // $scope.cartTotal = $scope.cartSubTotal;
                    if($scope.cartTotal < 0){
                        $scope.cartSubTotal = 0;
                        $scope.cartTotal = $scope.cartSubTotal;
                        $scope.taxRate = 0;
                    }
                    // $scope.selectedCategory.splice($scope.selectedCategory.indexOf(element.id),1);
                    // $scope.changecartCategory.splice($scope.changecartCategory.indexOf(element.id),1);
                    $scope.changecartCategory.splice(index, 1);
                    $scope.selectedCategory.forEach(function (eles, inds) {
                        if (element.id == eles.id) {
                            $scope.selectedCategory.splice(inds, 1);
                        }
                    });
                // }
            }
        });

        // Added new 
        $scope.cartTotal = 0;
        $scope.cartSubTotal = 0;
        $scope.taxRate = 0;
        // End New
        if($scope.subscriberBuzzes&&!$scope.subscriberBuzzes.includes(id)){
            $scope.noPromoMatch = false;

            // Added new
            if($('#buzz_'+id).prop("checked")){
                // $scope.cartSubTotal += amount;
                // $scope.promo_code = undefined;
                // $scope.cartTotal = $scope.cartSubTotal;
                // $scope.selectedCategory.push(id);
                $scope.cartSubTotal += amount;
                $scope.promo_code = undefined;
                // $scope.cartTotal = $scope.cartSubTotal;
                var taxRate  = amount*$scope.taxrateData/100;
                if(taxRate > 0){
                    var total = amount + taxRate
                    $scope.cartTotal += total ;
                    $scope.taxRate += taxRate;
                } else {
                    $scope.taxRate += taxRate;
                    $scope.cartTotal += amount;
                }
                $scope.selectedCategory = [];
                $scope.changecartCategory = [];
                $scope.selectedCategory.push({'id':id,'plan_duration':plan_duration});
                $scope.changecartCategory.push(data);
            }
            if(!$('#buzz_'+id).prop("checked")){
                if($scope.cartTotal > 0){
                    // $scope.cartSubTotal -= amount;
                    // $scope.promo_code = undefined;
                    // $scope.cartTotal = $scope.cartSubTotal;
                    // $scope.selectedCategory.splice($scope.selectedCategory.indexOf(id),1);
                    var taxRates  = amt*$scope.taxrateData/100;
                    $scope.promo_code = undefined;
                    $scope.discount = 0;
                    if(taxRates > 0){
                        var total = amt + taxRates;
                        $scope.cartTotal = $scope.cartTotal - total;
                        $scope.cartSubTotal -= amt;
                        $scope.taxRate -= taxRates;
                    } else {
                        $scope.cartTotal -= amt;
                        $scope.cartSubTotal -= amt;
                        $scope.taxRate -= taxRates;
                    }
                    // $scope.cartTotal = $scope.cartSubTotal;
                    if($scope.cartTotal < 0){
                        $scope.cartSubTotal = 0;
                        $scope.cartTotal = $scope.cartSubTotal;
                        $scope.taxRate = 0;
                    }
                    $scope.selectedCategory.splice($scope.selectedCategory.indexOf(id),1);
                    $scope.changecartCategory.splice($scope.changecartCategory.indexOf(id),1);
                }

            }
            // End new

            // Old
            // if($('#buzz_'+id).prop("checked")){
            //     $scope.cartSubTotal += amount;
            //     $scope.promo_code = undefined;
            //     // $scope.cartTotal = $scope.cartSubTotal;
            //     var taxRate  = amount*$scope.taxrateData/100;
            //     if(taxRate > 0){
            //         var total = amount + taxRate
            //         $scope.cartTotal += total ;
            //         $scope.taxRate += taxRate;
            //     } else {
            //         $scope.taxRate += taxRate;
            //         $scope.cartTotal += amount;
            //     }
            //     $scope.selectedCategory.push({'id':id,'plan_duration':plan_duration});
            //     $scope.changecartCategory.push(data);
            // }
            // End old

            // if(!$('#buzz_'+id).prop("checked")){
            //     if($scope.cartTotal > 0){
            //         $scope.cartSubTotal -= amount;
            //         $scope.promo_code = undefined;
            //         $scope.cartTotal = $scope.cartSubTotal;
            //         $scope.selectedCategory.splice($scope.selectedCategory.indexOf(id),1);
            //         $scope.changecartCategory.splice($scope.changecartCategory.indexOf(id),1);
            //     }
            // }
            if($scope.wantPurchase){
                if($scope.selectedCategory.indexOf($scope.wantPurchase.id) == -1) {
                    $scope.wantPurchase.id = '';
                }
            }
        }
    }

    $scope.Changecartamount = function(price_type,data,id,plan_duration){
        var amount = 0;
        $scope.change = undefined;
        $scope.select = undefined;
        $scope.changecartCategory.forEach(function (element, index) {
          if (element.id == id) {
            $scope.change = $scope.changecartCategory.splice(index, 1);
          }
        });
        $scope.selectedCategory.forEach(function (ele, ind) {
          if (ele.id == id) {
            $scope.select = $scope.selectedCategory.splice(ind, 1);
          }
        });
        // var selectCategory = $scope.selectedCategory.splice($scope.selectedCategory.indexOf(id),1);
        // var changeCategory = $scope.changecartCategory.splice($scope.changecartCategory.indexOf(id),1);
        if($scope.select != undefined && $scope.change != undefined){
            if(price_type==1){
                if($scope.select[0].plan_duration==3)
                    amountCancel = $scope.change[0].category_price_3month
                else if($scope.select[0].plan_duration==6)
                    amountCancel = $scope.change[0].category_price_6month
                else if($scope.select[0].plan_duration==12)
                    amountCancel = $scope.change[0].category_price
            } else if(price_type==2) {
                if($scope.select[0].plan_duration==3)
                    amountCancel = $scope.change[0].category_price_sgd_3month
                else if($scope.select[0].plan_duration==6)
                    amountCancel = $scope.change[0].category_price_sgd_6month
                else if($scope.select[0].plan_duration==12)
                    amountCancel = $scope.change[0].category_price_sgd
            }
            $scope.cartSubTotal = $scope.cartSubTotal - amountCancel;
            var taxRate  = amountCancel*$scope.taxrateData/100;
            if(taxRate > 0){
                var total = amountCancel + taxRate
                $scope.cartTotal -= total ;
                $scope.taxRate -= taxRate;
            } else {
                $scope.cartTotal -= amountCancel;
                $scope.taxRate -= taxRate;
            }
            if($scope.cartSubTotal < 0){
                $scope.cartSubTotal = 0;
            }
            if(price_type==1){
                if(plan_duration==3)
                    amount = data.category_price_3month
                else if(plan_duration==6)
                    amount = data.category_price_6month
                else if(plan_duration==12)
                    amount = data.category_price
            }else if(price_type==2){
                if(plan_duration==3)
                    amount = data.category_price_sgd_3month
                else if(plan_duration==6)
                    amount = data.category_price_sgd_6month
                else if(plan_duration==12)
                    amount = data.category_price_sgd
            }
            if($('#buzz_'+id).prop("checked")){
                $scope.cartSubTotal += amount;
                $scope.promo_code = undefined;
                $scope.discount = 0;
                var taxRate  = amount*$scope.taxrateData/100;
                if(taxRate > 0){
                    var total = amount + taxRate;
                    $scope.cartTotal += total;
                    $scope.taxRate += taxRate;
                } else {
                    $scope.taxRate += taxRate;
                    $scope.cartTotal += amount;
                }
                // $scope.cartTotal = $scope.cartSubTotal;
                $scope.selectedCategory.push({'id':id,'plan_duration':plan_duration});
                $scope.changecartCategory.push(data);
            }
        }
    };

    $scope.changeCartValue = function() {
        $scope.cartTotal = 0;
        $scope.cartSubTotal = 0;
        $scope.taxRate = 0;
        for (var i = $scope.selectedCategory.length - 1; i >= 0; i--) {
            if($scope.price_type == 1) {
                $scope.changecartCategory.forEach(function(element,index) {
                    var cartSubTotal = 0;
                    if(element.id == $scope.selectedCategory[i].id){
                        if($scope.selectedCategory[i].plan_duration == 3){
                            var cartSubTotal = element.category_price_3month;            
                        } else if($scope.selectedCategory[i].plan_duration == 6){
                            var cartSubTotal = element.category_price_6month;
                        } else if($scope.selectedCategory[i].plan_duration == 12){
                            var cartSubTotal = element.category_price;
                        }
                        $scope.cartSubTotal += cartSubTotal;
                        $scope.promo_code = undefined;
                        $scope.discount = 0;
                        var taxRate  = cartSubTotal*$scope.taxrateData/100;
                        if(taxRate > 0){
                            var total = cartSubTotal + taxRate;
                            $scope.cartTotal += total;
                            $scope.taxRate += taxRate;
                        } else {
                            $scope.taxRate += taxRate;
                            $scope.cartTotal = $scope.cartSubTotal;
                        }
                    }
                });
            } else if($scope.price_type == 2) {
                $scope.changecartCategory.forEach(function(element,index) {
                    var cartSubTotal = 0;
                    if(element.id == $scope.selectedCategory[i].id){
                        if($scope.selectedCategory[i].plan_duration == 3){
                            var cartSubTotal = element.category_price_sgd_3month;            
                        } else if($scope.selectedCategory[i].plan_duration == 6){
                            var cartSubTotal = element.category_price_sgd_6month;
                        } else if($scope.selectedCategory[i].plan_duration == 12){
                            var cartSubTotal = element.category_price_sgd;
                        }
                        $scope.cartSubTotal += cartSubTotal;
                        $scope.promo_code = undefined;
                        $scope.discount = 0;
                        var taxRate  = cartSubTotal*$scope.taxrateData/100;
                        if(taxRate > 0){
                            var total = cartSubTotal + taxRate;
                            $scope.cartTotal += total;
                            $scope.taxRate += taxRate;
                        } else {
                            $scope.taxRate += taxRate;
                            $scope.cartTotal = $scope.cartSubTotal;
                        }
                    }
                });
            }
        }
        // $scope.cartSubTotal = $scope.cartTotal;
        setTimeout(function() {
            var temp = [];
            for (var i = 0; i < $scope.subscriptionData.length; i++) {
                temp = $scope.subscriptionData[i].video_category_id.split(',');
                for (var j = 0; j < temp.length; j++) {    
                    $('#plan_'+temp[j]).val("number:"+$scope.subscriptionData[i].plan_duration.toString()).attr('selected',true);
                    $('#plan_'+temp[j]).attr('disabled', true);

                    $scope.buzzData.forEach(function (element, index) {
                        if(element.id == temp[j]){
                            if($scope.subscriptionData[i].payment_amount_type == 1){
                                if($scope.subscriptionData[i].plan_duration == 3){
                                    price = element.category_price_3month;
                                } else if($scope.subscriptionData[i].plan_duration == 6){
                                    price = element.category_price_6month;
                                } else if($scope.subscriptionData[i].plan_duration == 12){
                                    price = element.category_price;
                                }
                                $('#plansprice_'+temp[j]).text('$ '+price);
                            } else if($scope.subscriptionData[i].payment_amount_type == 2){
                                if($scope.subscriptionData[i].plan_duration == 3){
                                    price = element.category_price_sgd_3month;
                                } else if($scope.subscriptionData[i].plan_duration == 6){
                                    price = element.category_price_sgd_6month;
                                } else if($scope.subscriptionData[i].plan_duration == 12){
                                    price = element.category_price_sgd;
                                }
                                $('#plansprice_'+temp[j]).text('S$ '+price);
                            }
                        }
                    });
                }
            }
        }, 500);
    }

    $scope.GetloginuserandBuzzdata = function(){
        if($scope.userZone.is_subscription_allowed != 0) {
            ignitefactory.Mysubscriptiondata().then(function(response) {
                $scope.subscriberBuzzes = [];
                if(response.data.data.length > 0){
                    $scope.subscriptionData = response.data.data;
                    var temp = [];
                    for (var i = 0; i < $scope.subscriptionData.length; i++) {
                        temp = $scope.subscriptionData[i].video_category_id.split(',');
                        for (var j = 0; j < temp.length; j++) {    
                            $scope.subscriberBuzzes.push(parseInt(temp[j]));
                        }
                    }
                    jQuery.unique($scope.subscriberBuzzes);
                }
                else{
                    $scope.subscriptionData = [];
                }
            });
            $rootScope.subheader = '';
            $rootScope.logoutTwinkle = false;
            ignitefactory.Getsubjects().then(function(response) {
                if(response.data.data.length > 0){
                    $scope.subjects = response.data.data;

                    $rootScope.isBUZZ=$scope.subjects.find(data=>{ return data.id==1 });
                    $rootScope.isBUZZRU=$scope.subjects.find(data=>{ return data.id==2 });
                    $rootScope.isMAZE=$scope.subjects.find(data=>{ return data.id==3 });
                    $rootScope.isSMILE=$scope.subjects.find(data=>{ return data.id==4 });
                    $rootScope.isSMILESG=$scope.subjects.find(data=>{ return data.id==7 });

                    $rootScope.isMAZE=$rootScope.isMAZE?true:false;
                    $rootScope.isSMILE=$rootScope.isSMILE?true:false;
                    $rootScope.isSMILESG=$rootScope.isSMILESG?true:false;
                    $rootScope.isBUZZ=$rootScope.isBUZZ?true:false;
                    $rootScope.isBUZZRU=$rootScope.isBUZZRU?true:false;

                    $scope.subjectsClass = 12/$scope.subjects.length;
                    $scope.Getcategories($scope.subjects[0].encoded_string, $scope.subjects[0].id);
                }
            });
        }
    }

    $scope.Getcategories = function(enc_id,id,frm=''){
        if($rootScope.subject_id!=undefined && $rootScope.url!=undefined){
            if($scope.userZone.is_subscription_allowed != 0) {
                ignitefactory.Getcategoriesforsubject({'id':$rootScope.url}).then(function(response) {
                    if(response.data.data.length > 0){
                        $scope.dis = false;
                        $scope.cartTotal = 0;
                        $scope.cartSubTotal = 0;
                        $scope.selectedCategory = [];
                        $('.subjects').removeClass('hidden-arrow');
                        $('.subjects').removeClass('active');
                        $('#subjectButton_'+$rootScope.subject_id).addClass('active');
                        $scope.subject_id = $rootScope.subject_id;
                        $scope.showCart = true;
                        $scope.buzzData = response.data.data;
                        var temp = [];
                        for (var i = 0; i < $scope.subscriptionData.length; i++) {
                            for (var j = 0; j < $scope.buzzData.length; j++) {    
                                if($scope.subscriptionData[i].video_category_id == $scope.buzzData[j].id)
                                {
                                    $scope.dis = true;
                                }
                            }
                        }
                    }
                });
            }
        }else{
            if($scope.userZone.is_subscription_allowed != 0) {
                ignitefactory.Getcategoriesforsubject({'id':enc_id}).then(function(response) {
                    if(response.data.data.length > 0){
                        $scope.dis = false;
                        if(frm=='wantPurchase'){
                            setTimeout(function() {
                                $('#buzz_'+$scope.wantPurchase.id).prop('checked', true);
                            }, 2000);
                            $scope.cartTotal = $scope.cartTotal!=0?$scope.cartTotal:0;
                            $scope.cartSubTotal = $scope.cartSubTotal!=0?$scope.cartSubTotal:0;
                            $scope.selectedCategory = $scope.selectedCategory.length!=0?$scope.selectedCategory:[];
                            if($scope.selectedCategory.length!=0){
                                setTimeout(function() {
                                    for (var i = 0; i < $scope.selectedCategory.length; i++) {
                                        var temp = $scope.selectedCategory[i].id;
                                        $('#buzz_'+temp).prop('checked', true);
                                        var sel = $scope.selectedCategory[i].plan_duration;
                                        $('#plan_'+temp).val("number:"+sel.toString());
                                        $scope.changecartCategory.forEach(function(element,index) {
                                            if(element.id == temp){
                                                if($scope.price_type == 1){
                                                    if(sel == 3){
                                                        var prices = element.category_price_3month;
                                                    } else if(sel == 6){
                                                        var prices = element.category_price_6month;
                                                    } else if(sel == 12){
                                                        var prices = element.category_price;
                                                    }
                                                    $('#plansprice_'+temp).text('$ '+prices);
                                                } else if($scope.price_type == 2){
                                                    if(sel == 3){
                                                        var prices = element.category_price_sgd_3month;
                                                    } else if(sel == 6){
                                                        var prices = element.category_price_sgd_3month;
                                                    } else if(sel == 12){
                                                        var prices = element.category_price_sgd;
                                                    }
                                                    $('#plansprice_'+temp).text('S$ '+prices);
                                                }
                                            }
                                        });
                                    }
                                }, 500);
                            }
                        }else{
                            if($scope.wantPurchase){
                                $scope.wantPurchase.id = '';
                            }
                            $scope.cartTotal = $scope.cartTotal!=0?$scope.cartTotal:0;
                            $scope.cartSubTotal = $scope.cartSubTotal!=0?$scope.cartSubTotal:0;
                            $scope.selectedCategory = $scope.selectedCategory.length!=0?$scope.selectedCategory:[];
                            if($scope.selectedCategory.length!=0){
                                setTimeout(function() {
                                    for (var i = 0; i < $scope.selectedCategory.length; i++) {
                                        var temp = $scope.selectedCategory[i].id;
                                        $('#buzz_'+temp).prop('checked', true);
                                        var sel = $scope.selectedCategory[i].plan_duration;
                                        $('#plan_'+temp).val("number:"+sel.toString());
                                        $scope.changecartCategory.forEach(function(element,index) {
                                            if(element.id == temp){
                                                if($scope.price_type == 1){
                                                    if(sel == 3){
                                                        var prices = element.category_price_3month;
                                                    } else if(sel == 6){
                                                        var prices = element.category_price_6month;
                                                    } else if(sel == 12){
                                                        var prices = element.category_price;
                                                    }
                                                    $('#plansprice_'+temp).text('$ '+prices);
                                                } else if($scope.price_type == 2){
                                                    if(sel == 3){
                                                        var prices = element.category_price_sgd_3month;
                                                    } else if(sel == 6){
                                                        var prices = element.category_price_sgd_3month;
                                                    } else if(sel == 12){
                                                        var prices = element.category_price_sgd;
                                                    }
                                                    $('#plansprice_'+temp).text('S$ '+prices);
                                                }
                                            }
                                        });
                                    }
                                }, 500);
                            }
                        }
                        $('#SubscriptionPlanTabButton').removeClass('hidden-arrow');
                        $('.subjects').removeClass('hidden-arrow');
                        $('.subjects').removeClass('active');
                        $('#subjectButton_'+id).addClass('active');
                        $scope.subject_id = id;
                        $scope.showCart = true;
                        $scope.buzzData = response.data.data;
                        var temp = [];
                        for (var i = 0; i < $scope.subscriptionData.length; i++) {
                            for (var j = 0; j < $scope.buzzData.length; j++) {    
                                if($scope.subscriptionData[i].video_category_id == $scope.buzzData[j].id)
                                {
                                    $scope.dis = true;
                                }
                            }
                        }
                    }
                });
            }
        }
        setTimeout(function() {
            var temp = [];
            for (var i = 0; i < $scope.subscriptionData.length; i++) {
                temp = $scope.subscriptionData[i].video_category_id.split(',');
                for (var j = 0; j < temp.length; j++) {    
                    $('#plan_'+temp[j]).val("number:"+$scope.subscriptionData[i].plan_duration.toString()).attr('selected',true);
                    $('#plan_'+temp[j]).attr('disabled', true);
                    $scope.buzzData.forEach(function (element, index) {
                        if(element.id == temp[j]){
                            if($scope.subscriptionData[i].payment_amount_type == 1){
                                if($scope.subscriptionData[i].plan_duration == 3){
                                     var price = element.category_price_3month;
                                } else if($scope.subscriptionData[i].plan_duration == 6){
                                    var price = element.category_price_6month;
                                } else if($scope.subscriptionData[i].plan_duration == 12){
                                    var price = element.category_price;
                                }
                                $('#plansprice_'+temp[j]).text('$ '+price);
                            } else if($scope.subscriptionData[i].payment_amount_type == 2){
                                if($scope.subscriptionData[i].plan_duration == 3){
                                    var price = element.category_price_sgd_3month;
                                } else if($scope.subscriptionData[i].plan_duration == 6){
                                    var price = element.category_price_sgd_6month;
                                } else if($scope.subscriptionData[i].plan_duration == 12){
                                    var price = element.category_price_sgd;
                                }
                                $('#plansprice_'+temp[j]).text('S$ '+price);
                            }
                        }   
                    });
                }
            }
        }, 500);
        $('[data-toggle="tooltip"]').tooltip();
    }

    // Tooltip for show the message
    // $(function () {
        // $('[data-toggle="tooltip"]').tooltip();
    //     $('#language_tooltip').tooltip('open');
        $timeout(function(){
            $('#language_tooltip').tooltip('close');
        },3000);
    // });

    $scope.GetloginuserandBuzzdata();
    $scope.Ignitepayemnt = function(){
        var ignitePayemnt = new FormData($("#ignite_payment_data")[0]);
        if(getLanguage=='en'){
            showLoader('.ignite_payment_data','Please wait');
        }else{
            showLoader('.ignite_payment_data','请稍候');
        }
        ignitePayemnt.append('promoStatus',$scope.promoStatus);
        ignitefactory.Ignitepayemnt(ignitePayemnt).then(function(response) {

            if(getLanguage=='en'){hideLoader('.ignite_payment_data','Add to Cart');}else{hideLoader('.ignite_payment_data','添加到购物车');}
            if (response.data.status == 2) window.location.replace("ignite-login");
            if (response.data.status == 1 && response.data.data.slug != '') {
                window.location = 'make-payment-ignite/'+response.data.data.slug;
            } else {
                if(getLanguage=='en'){hideLoader('.ignite_payment_data','Make Payment');}else{hideLoader('.ignite_payment_data','添加到购物车');}
            }
        });
    }
    $scope.Stripepayemnt = function(){
        $scope.language = Cookies.get('language');
        // showLoader('.stripe_payment_btn',$scope.language=='en'?'Please wait':'请稍候')
        // confirmDialogueAlert("Success","We will follow recurring payments for selected categories until to cancel the subscription. Do you want to continue?","success",false,'#f17598','Ok','Cancel',true,true,'',payment);
        // confirmDialogueAlertHtml('Success', '<p>Your card will be charged when your subscription automatically renews at the end of your subscription period unless you opt out of auto-renewal at least 48 hours prior to the end of the current period.</p></br><p>To learn more about renewal options, please visit our <a href="/faq" target="_blank">FAQs</a>.</p>', 'success', true, '#f17598', 'I Accept', 'Cancel', true, true, '', payment);
        $('#signuptermsandconditionsmodal').modal('show');
    }

    $scope.payment = function() {
        $('#signuptermsandconditionsmodal').modal('hide');
        ignitefactory.Encryptdataforstripepayment({'price_type':$scope.price_type,'for_category':$scope.selectedCategory,'language':$scope.language=='en'?1:2,'promoStatus':$scope.promoStatus,'promo':$scope.promo_code,'module':0}).then(function(response) {
            hideLoader('.stripe_payment_btn',$scope.language=='en'?'Make Payment':'添加到购物车');
            if(response.data.status==1){
                window.location.replace('ignite-payment/'+response.data.data);
            } else {
                simpleAlert('error', '', response.data.message);
            }
        });
    }

    $scope.changeSelectedLang = function (currLang) {
        $scope.selectedLang = !currLang;
    }

    $scope.promoStatus = 0;
    $scope.discount     = 0;
    $scope.checkPromo = false;
    $scope.Checkpromo = function(promo){
        if($scope.cartSubTotal==0){
            if(Cookies.get('language') == 'en') {
                simpleAlert('error', '', "Please select at least one plan.");
            } else if(Cookies.get('language') == 'chi') {
                simpleAlert('error', '', "未选购订阅计划。");
            }
            return;
        } else {
            showLoader('.apply_promo');
            $scope.checkforCategory = [];
            $('input[type=checkbox]:checked').each(function () {
                $scope.checkforCategory.push($(this).val());
            });
            if($scope.selectedLang==true)
                $scope.language = 1;
            else
                $scope.language = 0;
            ignitefactory.Checkpromo({'price_type':$scope.price_type,'promo':promo,'for_category':$scope.selectedCategory,'language':$scope.language,'already_paid':$scope.subscriberBuzzes,'cart_total':$scope.cartTotal,'module':0}).then(function(response) {
                hideLoader('.apply_promo',$scope.selectedLang==true?'Apply':'应用');
                if(response.data.code==2) window.location.replace("ignite-login");
                $scope.promoStatus = response.data.status;
                if(response.data.status==1){
                    $scope.cartTotal = response.data.data.finalPrice;
                    $scope.discount = response.data.data.discount_of;
                    $scope.noPromoMatch = false;
                    $scope.promo_code = promo;
                    $scope.checkPromo = true;
                } else {
                    $scope.cartTotal = $scope.cartSubTotal;
                    $scope.noPromoMatch = true;
                    $scope.message = response.data.message;
                    $scope.checkPromo = false;
                }
            });
        }
    }

    if($scope.url.split('/').length > 2) {
        var cat_id = $scope.url.split('/').pop();
        ignitefactory.WantToPurchaseData({'slug': cat_id}).then(function(response) {
            if(response.data.status){
                $scope.wantPurchase = response.data.data.purchase;
                $scope.cartTotal = 0;
                $scope.cartSubTotal = 0;
                $scope.selectedCategory = [];
                $scope.Getcategories($scope.wantPurchase.encoded_string,$scope.wantPurchase.subject_id,'wantPurchase')
                // $('.subjects').removeClass('active');  
                $('#subjectButton_'+$scope.wantPurchase.subject_id).addClass('active');
                $('#buzz_'+$scope.wantPurchase.id).prop('checked', true);
                $scope.showCart = true;

                $scope.promo_code = undefined;
                
                $timeout(function(){
                    $('#buzz_'+$scope.wantPurchase.id).prop('checked', true);
                    $('#buzzNew_'+$scope.wantPurchase.id).addClass('shake_custom');    
                    // $scope.cartTotal = $scope.wantPurchase.category_price;
                    // $scope.cartSubTotal = $scope.wantPurchase.category_price;
                    // $scope.cartTotal = $scope.wantPurchase.category_price_3month;
                    $scope.cartSubTotal = $scope.wantPurchase.category_price_3month;
                    var taxRate  = $scope.cartSubTotal*$scope.taxrateData/100;
                    if(taxRate > 0){
                        var total = $scope.cartSubTotal + taxRate;
                        $scope.cartTotal += total;
                        $scope.taxRate += taxRate;
                    } else {
                        $scope.taxRate += taxRate;
                        $scope.cartTotal = $scope.cartSubTotal;
                    }
                    // $scope.selectedCategory.push($scope.wantPurchase.id);
                    $scope.selectedCategory.push({'id':$scope.wantPurchase.id,'plan_duration':3});
                    $scope.changecartCategory.push($scope.wantPurchase);
                }, 500);
                
                setTimeout(function(){
                    $('#buzzNew_'+$scope.wantPurchase.id).removeClass('shake_custom');
                }, 2500);

            }
        });
    }

    $scope.IgniteMysubscriptiondata = function(){
        if($scope.userZone.is_subscription_allowed != 0) {
            ignitefactory.IgniteMysubscriptiondata().then(function(response) {
                if(response.data.data){
                    $scope.IgniteSubscriptionData = response.data.data;
                } else {
                    $scope.IgniteSubscriptionData = [];
                }
            });
        }
    };
    $scope.IgniteMysubscriptiondata();

    $scope.IgniteHistorysubscriptiondata = function(){
        if($scope.userZone.is_subscription_allowed != 0) {
            ignitefactory.IgniteHistorysubscriptiondata().then(function(response) {
                if(response.data.data){
                    $scope.IgniteHistorySubscriptionData = response.data.data;
                } else {
                    $scope.IgniteHistorySubscriptionData = [];
                }
            });
        }
    };
    $scope.IgniteHistorysubscriptiondata();

    $scope.IgniteTransactionsnData = function(){
        if($scope.userZone.is_subscription_allowed != 0) {
            ignitefactory.IgniteTransactionsnData().then(function(response) {
                if(response.data.data){
                    $scope.IgniteTransactionsnData = response.data.data;
                } else {
                    $scope.IgniteTransactionsnData = [];
                }
            });
        }
    };
    $scope.IgniteTransactionsnData();

    $scope.SwitchPlan = function (dropdown_id,purchase_category_id) {
        var switch_categoryid =  $('#switch_'+dropdown_id).val();
        if(switch_categoryid == null || switch_categoryid == undefined || switch_categoryid == ''){
            simpleAlert('error', '', 'Please Select Category');
        } else{
            ignitefactory.SwitchPlan({'switch_categoryid':switch_categoryid,'purchase_category_id':purchase_category_id,'main_category_id':dropdown_id}).then(function(response) {
                if(response.data.status == 1){
                    if(response.data.code == 3){
                        simpleAlert('error', '', response.data.data.error);
                    } else {
                        window.location.replace('my-subscription');
                    }
                } else {
                    if(response.data.status == 0){
                        simpleAlert('error', '', response.data.message.error);
                    }
                    if(response.data.code==2) {
                        window.location.replace("ignite-login");
                    }
                }
            });
        }   
    }

    $scope.getSubjectCategory = function (subject_id,category_id) {
        ignitefactory.getSubjectCategory({'subject_id':subject_id}).then(function(response) {
            if(response.data.status == 1){
                $scope.category_list = response.data.data;
                $scope.option = '<option value="">select..</option>';
                angular.forEach($scope.category_list, function(key, value){
                    $scope.option += '<option value="'+key.id+'">'+key.category_name+'</option>';
                });
                $("#switch_"+category_id).html($scope.option);
            } else {
                if(response.data.code==2) {
                    window.location.replace("ignite-login");
                }
            }
        });
    }

    $scope.CancelSubscription = function (plan_purchase_id, main_category_id) {
        $scope.plan_id = plan_purchase_id;
        $scope.main_id = main_category_id;
        if($('#toggle_'+$scope.plan_id).prop("checked")){
            $scope.status = 1;
            // confirmDialogueAlertHtml("Success", "<p>You have opted in to automatically renew your subscription plan.</p></br><p>Your card will be charged when your subscription automatically renews at the end of your subscription period unless you opt out of auto-renewal at least 48 hours prior to the end of the current period.</p></br><p>Subscription automatically renew for same grade.</p></br><p>12-month subscription - Use your 2 free switches to change grades.</p></br><p>3/6-month subscription period - Make grade changes 48 hours before subscription ends in <b>Renewal Options</b>.</p>", "success", false, '#f17598', 'I Accept', '', true, '', '', redirectCancelsubscription);
            $('#optInmodal').modal('show');
        } else if(!($('#toggle_'+$scope.plan_id).prop("checked"))) {
            $scope.status = 0;
            // confirmDialogueAlertHtml("Success", "<p>You have opted out and chosen not to renew your subscription plan automatically.</p></br><p>To avoid losing access to your subscription plan, we strongly encourage you to opt in to have your subscription plan renew automatically.</p><br><p>If you will like to upgrade to a higher level or change your subscription period, you may go to <b>Renewal Options</b> or visit our <a href='/faq' target='_blank'>FAQs</a> to find out more.</p>", "success", false, '#f17598', 'I Accept', '', true, '', '', redirectCancelsubscription);
            $('#optOutmodal').modal('show');
        }
        // $scope.status  = status;
        // confirmDialogueAlert('Are you sure?', 'You want to cancel the subscription?', 'error', true, '#f17598', 'Ok', 'Cancel', true, true, '', redirectCancelsubscription);
        // confirmDialogueAlert("Success", "You have switched on/off your renewal of subscription plan.", "success", false, '#f17598', 'Ok', '', true, '', '', redirectCancelsubscription);
    }

    $('#optOutmodal').on('hide.bs.modal', function () {
        $('#toggle_'+$scope.plan_id).prop("checked",true);
    });
    $('#optInmodal').on('hide.bs.modal', function () {
        $('#toggle_'+$scope.plan_id).prop("checked",false);
    });

    $scope.redirectCancelsubscription = function() {
        if($('#toggle_'+$scope.plan_id).prop("checked")){
            $('#toggle_'+$scope.plan_id).prop("checked",true);
        } else if(!($('#toggle_'+$scope.plan_id).prop("checked"))) {
            $('#toggle_'+$scope.plan_id).prop("checked",false);
        }
        $('#optInmodal').modal('hide');
        $('#optOutmodal').modal('hide');
        var plan_purchase_id = $scope.plan_id;
        var main_category_id = $scope.main_id;
        var status           = $scope.status;
        ignitefactory.CancelSubscription({'plan_purchase_id':plan_purchase_id, 'main_category_id':main_category_id, 'status':status}).then(function(response) {
            if(response.data.status == 1){
                if(response.data.code == 3){
                    simpleAlert('error', '', response.data.data.error);
                } else {
                    $scope.IgniteMysubscriptiondata();
                }
            } else {
                if(response.data.code==2) {
                    window.location.replace("ignite-login");
                }
            }
        });
    }

    $scope.Applypromocode = function (promocode,subscription_id,category_id,video_category_id) {
        var promocode = $('#promocode_'+video_category_id).val();
        if(promocode == null || promocode == undefined || promocode == ''){
            simpleAlert('error', '', 'Please enter promocode');
        } else {
            ignitefactory.Applypromocode({'promocode':promocode,'subscription_id':subscription_id,'main_category_id':category_id}).then(function(response) {
                if(response.data.status == 1){
                    if(response.data.data.payment_amount_type == 2){
                        $('#applytext_'+video_category_id).html('Discount Price : SGD '+response.data.data.amount);
                    } else {
                        $('#applytext_'+video_category_id).html('Discount Price : USD '+response.data.data.amount);
                    }
                    $('#promocode_'+video_category_id).val(response.data.data.promocode);
                    $('#promocode_'+video_category_id).attr('disabled','disabled');
                    $scope.IgniteMysubscriptiondata();
                } else {
                    if(response.data.code==2) {
                        window.location.replace("ignite-login");
                    } else if(response.data.code==400){
                        $('#applytext_'+video_category_id).html(response.data.message);
                    }
                }
            });
        }   
    }

    $scope.Removepromocode = function (promocode,subscription_id,category_id,video_category_id) {
        if(promocode == null || promocode == undefined || promocode == ''){
            simpleAlert('error', '', 'Please enter promocode');
        } else {
            ignitefactory.Removepromocode({'promocode':promocode,'subscription_id':subscription_id,'main_category_id':category_id}).then(function(response) {
                if(response.data.status == 1){
                    $scope.IgniteMysubscriptiondata();
                } else {
                    if(response.data.code==2) {
                        window.location.replace("ignite-login");
                    }
                }
            });
        }   
    }

    $scope.getPromocodedetails = function (payment_amount_type,plan_duration,category_id) {
        ignitefactory.getPromocodedetails({'payment_amount_type':payment_amount_type,'plan_duration':plan_duration,'main_category_id':category_id}).then(function(response) {
            if(response.data.status == 1){
                if(response.data.data.payment_amount_type == 2){
                    $('#applytext_'+category_id).html('Discount Price : SGD '+response.data.data.discount_price);
                } else {
                    $('#applytext_'+category_id).html('Discount Price : USD '+response.data.data.discount_price);
                }
                $('#promocode_'+category_id).val(response.data.data.promocode);
                $('#promocode_'+category_id).attr('disabled','disabled');
            } else {
                if(response.data.code==2) {
                    window.location.replace("ignite-login");
                } else if (response.data.code==400){
                    $('#applytext_'+category_id).html();
                    $('#promocode_'+category_id).val('');
                }
            }
        });   
    }    

}]);
/*
 Author  : Nivedita (nivedita@creolestudios.com)
 Date    : 14th Sept 2018
 Name    : MakePaymentIgniteController
 Purpose : All functions mrelated to payments
 */
angular.module('sixcloudApp').controller('MakePaymentIgniteController', ['$sce', 'ignitefactory', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, ignitefactory, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {
    console.log('MakePaymentIgniteController Controller loaded');
    $('body').css('background', '#e3eefd');
   
    /*$timeout(function() {
        if ($rootScope.loginUserData.status == 0) {
            window.location.replace("sixteen-login");
        }
        $('.buyer-dashboard-menu').find('li').removeClass('active');
        $('#dashboard').addClass('active');
    }, 500);*/
    // $scope.make_payment_id = $routeParams.slug.substring(0,$routeParams.slug.length - 1);
    $scope.make_payment_id = $routeParams.slug;
    getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
    changeLanguage(getLanguage);
    $('#chk_lang').change(function() {
        getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
        changeLanguage(getLanguage);
    });
    
    
    function changeLanguage(lang){ 
        if(lang=="chi"){
            $scope.english = false;
            $scope.chinese = true;
            getLanguage=='chi';
        }    
        else{
            $scope.english = true;
            $scope.chinese = false;
            getLanguage=='en';
      }
    }
    $scope.Paymentinfodata = function() {
        
        ignitefactory.Paymentinfodata({'payment_id': $scope.make_payment_id}).then(function(response) {
            
            if (response.data.code == 2) window.location.replace("ignite-login");
            if (response.data.status == 1) {
                $scope.paymentInfo = response.data.data[0];
                console.log($scope.paymentInfo);
                $scope.domainInfo  = response.data.data.domain;
                $scope.invoice     = response.data.data.invoice; 
                
            } else {}
        });
    }
    $scope.Paymentinfodata();

    $timeout(function () {
        $('#create_and_pay').click();
    }, 2000);      
    
}]);
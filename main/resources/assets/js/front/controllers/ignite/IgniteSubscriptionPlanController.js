/*
 Author  : Karan Kantesariya (karan@creolestudios.com)
 Date    : 16th March 2021
 Name    : IgniteSubscriptionPlanController
 Purpose : All the functions for Subscription-plan page
 */
angular.module('sixcloudApp').controller('IgniteSubscriptionPlanController', ['$sce', 'ignitefactory', 'mpfactory', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', '$location', 'Getuserzone', function($sce, ignitefactory, mpfactory, $routeParams, $http, $scope, $timeout, $rootScope, $filter, $location, Getuserzone) {

    console.log("IgniteSubscriptionPlanController");
    $("#content").removeClass('hide');
    $rootScope.showiframe = false;
    $rootScope.Twinklemenu = false;
    $rootScope.ignitePage='Switch-Plan';
    $rootScope.Mathematics = "";
    $scope.language = Cookies.get('language');
    $scope.subject_id   = $routeParams.subject_id;

    getLanguage = Cookies.get('language');
    $scope.current_domain = current_domain;
    $scope.userZone = Getuserzone.data.data;
    if($scope.userZone.zone_name.toUpperCase() == 'CIS') {
        $scope.price_type = 2;
    } else {
        $scope.price_type = 1;
    }
    if($scope.userZone.is_subscription_allowed == 0) {
        swal({
            title: "",
            text: "Please contact to customer support",
            allowEscapeKey:false
        }, function() {
            window.location.href = "customer-support";
        });
    }
    ignitefactory.Getloginuserdatadistributor().then(function(response) {
        if (response.data.status == 1) {
            $rootScope.loginDistributorData = response.data.data;
        } else {
            $rootScope.loginDistributorData = response.data;
        }
    });

    $timeout(function(){
        $timeout(function(){
            getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
            changeLanguage(getLanguage);
            $('#chk_lang').change(function() {
                getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
                changeLanguage(getLanguage);
            });
        },500);
        function changeLanguage(lang) {
            if (lang == "chi") {
                $timeout(function(){
                    $scope.english = false;
                },500);
            } else {
                $timeout(function(){
                    $scope.english = true;
                },500);
            }
        }

        $scope.GettaxrateDetails = function(){
            ignitefactory.GettaxrateDetails().then(function(response) {
                if (response.data.status == 1) {
                    $scope.taxrateData = response.data.data.tax_rate;
                } else {
                    $scope.taxrateData = 0;
                }
            });
        }
        $scope.GettaxrateDetails();
        
        $('.ignite-menu').find('li').removeClass('active');
        $rootScope.subheader = '';
        $rootScope.logoutTwinkle = false;
        ignitefactory.Getsubjects().then(function(response) {
            if(response.data.data.length > 0){
                $scope.subjects = response.data.data;

                $rootScope.isBUZZ=$scope.subjects.find(data=>{ return data.id==1 });
                $rootScope.isBUZZRU=$scope.subjects.find(data=>{ return data.id==2 });
                $rootScope.isMAZE=$scope.subjects.find(data=>{ return data.id==3 });
                $rootScope.isSMILE=$scope.subjects.find(data=>{ return data.id==4 });
                $rootScope.isSMILESG=$scope.subjects.find(data=>{ return data.id==7 });

                $rootScope.isMAZE=$rootScope.isMAZE?true:false;
                $rootScope.isSMILE=$rootScope.isSMILE?true:false;
                $rootScope.isSMILESG=$rootScope.isSMILESG?true:false;
                $rootScope.isBUZZ=$rootScope.isBUZZ?true:false;
                $rootScope.isBUZZRU=$rootScope.isBUZZRU?true:false;
            }
        });
    });

    ignitefactory.Getpurchasecategorydetails({'subject_id':$scope.subject_id}).then(function(response) {
        if(response.data.status == 1){
            $scope.upcomingPlanCount= response.data.data['upcoming_plan_count'];
            if($scope.upcomingPlanCount >= 1){
                $scope.check = true;
            } else {
                $scope.check = false;
            }
            $scope.purchaseCategory = response.data.data['purchaseCategory'][0]; 
            // $scope.radio            = [3, 6, 12];
            $scope.from             = $scope.purchaseCategory.plan_duration;
            // $scope.to               = [];
            // angular.forEach($scope.radio, function(value, key) {
            //   if($scope.from != value){
            //     $scope.to.push(value);
            //   }
            // });
        } else {
            if(response.data.status == 0){
                simpleAlert('error', '', 'Please Purchase Plan');
            }
            if(response.data.code==2) {
                window.location.replace("ignite-login");
            }
        }
    });

    $scope.GetCategory = function(categoryData) {
        $scope.cartTotal    = 0;
        $scope.cartSubTotal = 0;
        $scope.promoStatus = 0;
        $scope.discount     = 0;
        $scope.taxRate = 0;
        $scope.subscriberBuzzes = [];
        $scope.checkPromo = false;
        $('input[name="toCategory"]').prop('checked', false);
        $scope.category = categoryData;
        if($scope.category == null || $scope.category == undefined || $scope.category == ''){
            simpleAlert('error', '', 'Please Select Category');
        } else {
            $scope.radio = [];
            $scope.radio            = [3, 6, 12];
            $scope.froms = [];
            if($scope.category == $scope.purchaseCategory.video_category_id){
                $scope.froms             = $scope.purchaseCategory.plan_duration;
            }
            $scope.to               = [];
            angular.forEach($scope.radio, function(value, key) {
                if($scope.froms != value){
                    $scope.to.push(value);
                }
            });
        }
    }   

    $scope.encoded      = btoa($scope.subject_id);
    var enc_id          = 'subject_'+$scope.encoded;
    ignitefactory.Getcategoriesforsubject({'id':enc_id}).then(function(response) {
        if(response.data.status == 1){
            $scope.categoryList = response.data.data;
        } else {
            if(response.data.status == 0){
                simpleAlert('error', '', 'Please Purchase Plan');
            }
            if(response.data.code==2) {
                window.location.replace("ignite-login");
            }
        }
    });

    $scope.cartTotal    = 0;
    $scope.cartSubTotal = 0;

    $scope.promoStatus = 0;
    $scope.discount     = 0;
    $scope.taxRate = 0;
    $scope.subscriberBuzzes = [];
    $scope.checkPromo = false;
    $scope.Checkpromo = function(promo){
        if($scope.cartSubTotal==0){
            if(Cookies.get('language') == 'en') {
                simpleAlert('error', '', "Please select at least one plan.");
            } else if(Cookies.get('language') == 'chi') {
                simpleAlert('error', '', "未选购订阅计划。");
            }
            return;
        } else {
            showLoader('.apply_promo');
            // $scope.checkforCategory = [];
            // $('input[type=radio]:checked').each(function () {
            //     $scope.checkforCategory.push($(this).val());
            // });
            if($scope.selectedLang==true)
                $scope.language = 1;
            else
                $scope.language = 0;
            ignitefactory.Checkpromo({'price_type':$scope.price_type,'promo':promo,'for_category':$scope.selectedCategory,'language':$scope.language,'already_paid':$scope.subscriberBuzzes,'cart_total':$scope.cartTotal,'module':0}).then(function(response) {
                hideLoader('.apply_promo',$scope.selectedLang==true?'Apply':'应用');
                if(response.data.code==2) window.location.replace("ignite-login");
                $scope.promoStatus = response.data.status;
                if(response.data.status==1){
                    $scope.cartTotal = response.data.data.finalPrice;
                    $scope.discount = response.data.data.discount_of;
                    $scope.noPromoMatch = false;
                    $scope.promo_code = promo;
                    $scope.checkPromo = true;
                } else {
                    $scope.cartTotal = $scope.cartSubTotal;
                    $scope.noPromoMatch = true;
                    $scope.message = response.data.message;
                    $scope.checkPromo = false;
                }
            });
        }
    }

    $scope.GetData = function (plan_duration, category_id) {
        $scope.cartSubTotal = 0;
        $scope.cartTotal = 0;
        $scope.taxRate = 0;
        $("#promo_code").val('');
        $scope.promo_code = '';
        $scope.discount   = 0;
        $scope.toCategory   = plan_duration;
        $scope.category_id = category_id; 
        angular.forEach($scope.categoryList, function(value, key) {
            if($scope.category_id == null || $scope.category_id == undefined || $scope.category_id == ''){
                simpleAlert('error', '', 'Please Select Category');
            } else if($scope.toCategory == null || $scope.toCategory == undefined || $scope.toCategory == ''){
                simpleAlert('error', '', 'Please Select Category plan duration');
            } else {
                if(value.id == $scope.category_id){
                    if($scope.price_type == 1){
                        if($scope.toCategory == 3){
                            $scope.cartSubTotal = value.category_price_3month;
                        } else if($scope.toCategory == 6){
                            $scope.cartSubTotal = value.category_price_6month;
                        } else if($scope.toCategory == 12){
                            $scope.cartSubTotal = value.category_price;
                        }
                    } else if($scope.price_type == 2){
                        if($scope.toCategory == 3){
                            $scope.cartSubTotal = value.category_price_sgd_3month;
                        } else if($scope.toCategory == 6){
                            $scope.cartSubTotal = value.category_price_sgd_6month;
                        } else if($scope.toCategory == 12){
                            $scope.cartSubTotal = value.category_price_sgd;
                        }
                    }
                    var taxRate  = $scope.cartSubTotal*$scope.taxrateData/100;
                    if(taxRate > 0){
                        var total = $scope.cartSubTotal + taxRate;
                        $scope.cartTotal += total;
                        $scope.taxRate += taxRate;
                    } else {
                        $scope.taxRate += taxRate;
                        $scope.cartTotal = $scope.cartSubTotal;
                    }
                    // $scope.cartTotal = $scope.cartSubTotal;
                    $scope.selectedCategory = [];
                    $scope.changecartCategory = [];
                    $scope.selectedCategory.push({'id':$scope.category_id, 'plan_duration':$scope.toCategory});
                    $scope.changecartCategory.push(value);
                }
            }
        });
    }

    $scope.changeCartValue = function() {
        $scope.cartTotal = 0;
        $scope.cartSubTotal = 0;
        $scope.taxRate = 0;
        for (var i = $scope.selectedCategory.length - 1; i >= 0; i--) {
            if($scope.price_type == 1) {
                $scope.changecartCategory.forEach(function(element,index) {
                    var cartSubTotal = 0;
                    if(element.id == $scope.selectedCategory[i].id){
                        if($scope.selectedCategory[i].plan_duration == 3){
                            var cartSubTotal = element.category_price_3month;            
                        } else if($scope.selectedCategory[i].plan_duration == 6){
                            var cartSubTotal = element.category_price_6month;
                        } else if($scope.selectedCategory[i].plan_duration == 12){
                            var cartSubTotal = element.category_price;
                        }
                        $scope.cartSubTotal += cartSubTotal;
                        $scope.promo_code = undefined;
                        $scope.discount = 0;
                        var taxRate  = cartSubTotal*$scope.taxrateData/100;
                        if(taxRate > 0){
                            var total = cartSubTotal + taxRate;
                            $scope.cartTotal += total;
                            $scope.taxRate += taxRate;
                        } else {
                            $scope.taxRate += taxRate;
                            $scope.cartTotal = $scope.cartSubTotal;
                        }
                    }
                });
            } else if($scope.price_type == 2) {
                $scope.changecartCategory.forEach(function(element,index) {
                    var cartSubTotal = 0;
                    if(element.id == $scope.selectedCategory[i].id){
                        if($scope.selectedCategory[i].plan_duration == 3){
                            var cartSubTotal = element.category_price_sgd_3month;            
                        } else if($scope.selectedCategory[i].plan_duration == 6){
                            var cartSubTotal = element.category_price_sgd_6month;
                        } else if($scope.selectedCategory[i].plan_duration == 12){
                            var cartSubTotal = element.category_price_sgd;
                        }
                        $scope.cartSubTotal += cartSubTotal;
                        $scope.promo_code = undefined;
                        $scope.discount = 0;
                        var taxRate  = cartSubTotal*$scope.taxrateData/100;
                        if(taxRate > 0){
                            var total = cartSubTotal + taxRate;
                            $scope.cartTotal += total;
                            $scope.taxRate += taxRate;
                        } else {
                            $scope.taxRate += taxRate;
                            $scope.cartTotal = $scope.cartSubTotal;
                        }
                    }
                });
            }
        }
    }

    $scope.MakePayment = function () {
        if($('#valid').prop("checked")){
            if($scope.toCategory == null || $scope.toCategory == undefined || $scope.toCategory == ''){
                simpleAlert('error', '', 'Please Select Category');
            } else if($scope.check) {
                simpleAlert('error', '', 'You already have one upcoming subscription plan available.');
            } else{ 
                $scope.language = Cookies.get('language');
                // confirmDialogueAlertHtml('Success', '<p>Your card will be charged when your subscription automatically renews at the end of your subscription period unless you opt out of auto-renewal at least 48 hours prior to the end of the current period.</p></br><p>To learn more about renewal options, please visit our <a href="/faq" target="_blank">FAQs</a>.</p>', 'success', true, '#f17598', 'I Accept', 'Cancel', true, true, '', payment);
                $('#signuptermsandconditionsmodal').modal('show');
            }
        } else {
            simpleAlert('error', '', 'Please check details confirmation');
        }   
    }

    $scope.myPayment = function() {
        ignitefactory.Encryptdataforstripepayment({'price_type':$scope.price_type,'for_category':$scope.selectedCategory,'language':$scope.language=='en'?1:2,'promoStatus':$scope.promoStatus,'promo':$scope.promo_code,'module':0}).then(function(response) {
            if(response.data.status==1){
                window.location.replace('subscription-payment/'+response.data.data);
            } else {
                simpleAlert('error', '', response.data.message);
            }
        });
    }

    ignitefactory.GetsubscriptionplanHistory({'subject_id':$scope.subject_id}).then(function(response) {
        if(response.data.status == 1){
            $scope.SubscriptionPlan = response.data.data;
        } else {
            // if(response.data.status == 0){
            //     simpleAlert('error', '', 'Please Purchase Plan');
            // }
            if(response.data.code==2) {
                window.location.replace("ignite-login");
            }
        }
    });

}]);
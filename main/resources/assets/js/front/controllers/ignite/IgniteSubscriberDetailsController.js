/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 5th Sept. 2018
 Name    : IgniteSubscriberDetailsController
 Purpose : All the functions for ignite subscriber detail page
 */
angular.module('sixcloudApp').controller('IgniteSubscriberDetailsController', ['$http', '$scope', '$timeout', '$routeParams', '$rootScope', '$filter', 'ignitefactory', '$location', function ($http, $scope, $timeout, $routeParams, $rootScope, $filter, ignitefactory, $location) {
    /*urlObj = JSON.parse(localStorage.getItem('urlObj'));
    url_number_static = localStorage.getItem('url_number_static');
    url_number_static++;
    url = [{
        "url_number": url_number_static,
        "url": $location.url()
    }];
    last_url = urlObj.pop();
    localStorage.setItem('urlObj',JSON.stringify(urlObj));
    if(last_url[0].url_number !== url[0].url_number) {
        urlObj.push(last_url);
    }
    urlObj.push(url);
    localStorage.setItem("urlObj",JSON.stringify(urlObj));
    localStorage.setItem("url_number_static",url_number_static);*/
    $timeout(function () {
        $('.ignite-menu').find('li').removeClass('active');
        $('#all-referral').addClass('active');
        $('#my-referral').addClass('active');
        $('.custom-accordion .panel-heading').click(function () {
            $(this).toggleClass('active');
            $(this).next('.panel-collapse').slideToggle();
        });
    }, 1000);
    // initSample();

    // $scope.params = $routeParams;
    $scope.slug = $routeParams.slug;
    ignitefactory.GetIgniteSubscriberDetailsData({ 'slug': $scope.slug }).then(function (response) {
        if (response.data.status == 1) {
            $scope.distributorDetails = response.data.data.distributorData;
            // $scope.transactions       = response.data.data.transactions;
            $scope.GetCurrentSubscription({ 'paid_by': $scope.distributorDetails.id }, 1);
            $scope.GetHistorySubscription({ 'paid_by': $scope.distributorDetails.id }, 1);
            $scope.GetSubscriberTransaction({ 'slug': $scope.slug }, 1);
        }
    });
    $scope.GetSubscriberTransaction = function (data, pageNumber) {
        console.log(data);
        ignitefactory.GetSubscriberTransaction(data, pageNumber).then(function (response) {
            if (response.data.status == 1) {
                $scope.transactions         = response.data.data.data;
                $scope.totalTransaction     = response.data.data.total;
                $scope.paginateTransaction  = response.data.data;
                $scope.transactionPaging    = response.data.data.current_page;
                $scope.rangeTransactionpage = _.range(1, response.data.data.last_page + 1);
            }
        });
    }
    $scope.GetCurrentSubscription = function (data, pageNumber) {
        ignitefactory.GetSubscriberCurrentSubscription(data, pageNumber).then(function (response) {            
            if (response.data.status == 1) {
                $scope.currentSubscription      = response.data.data.data;                
                $scope.totalCurrentSubscription = response.data.data.total;
                $scope.paginateCurrent          = response.data.data;
                $scope.currentPaging            = response.data.data.current_page;
                $scope.rangeCurrentpage         = _.range(1, response.data.data.last_page + 1);
            }
        });
    }
    $scope.GetHistorySubscription = function (data, pageNumber) {
        ignitefactory.GetSubscriberHistorySubscription(data, pageNumber).then(function (response) {            
            if (response.data.status == 1) {
                $scope.historySubscription      = response.data.data.data;                
                $scope.totalHistorySubscription = response.data.data.total;
                $scope.paginate                 = response.data.data;
                $scope.historyPaging            = response.data.data.current_page;
                $scope.rangeHistorypage         = _.range(1, response.data.data.last_page + 1);
            }
        });
    }

}]);
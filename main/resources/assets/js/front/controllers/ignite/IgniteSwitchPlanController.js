/*
 Author  : Karan Kantesariya (karan@creolestudios.com)
 Date    : 16th March 2021
 Name    : IgniteSwitchPlanController
 Purpose : All the functions for switch-plan page
 */
angular.module('sixcloudApp').controller('IgniteSwitchPlanController', ['$sce', 'ignitefactory', 'mpfactory', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', '$location', function($sce, ignitefactory, mpfactory, $routeParams, $http, $scope, $timeout, $rootScope, $filter, $location) {

    console.log("IgniteSwitchPlanController");
    $("#content").removeClass('hide');
    $rootScope.showiframe = false;
    $rootScope.Twinklemenu = false;
    $rootScope.ignitePage='Switch-Plan';
    $rootScope.Mathematics = "";
    $scope.language = Cookies.get('language');
    $scope.subject_id   = $routeParams.subject_id;

    $timeout(function(){
        $timeout(function(){
            getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
            changeLanguage(getLanguage);
            $('#chk_lang').change(function() {
                getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
                changeLanguage(getLanguage);
            });
        },500);
        function changeLanguage(lang) {
            if (lang == "chi") {
                $timeout(function(){
                    $scope.english = false;
                },500);
            } else {
                $timeout(function(){
                    $scope.english = true;
                },500);
            }
        }

        $('.ignite-menu').find('li').removeClass('active');
        $rootScope.subheader = '';
        $rootScope.logoutTwinkle = false;
        ignitefactory.Getsubjects().then(function(response) {
            if(response.data.data.length > 0){
                $scope.subjects = response.data.data;

                $rootScope.isBUZZ=$scope.subjects.find(data=>{ return data.id==1 });
                $rootScope.isBUZZRU=$scope.subjects.find(data=>{ return data.id==2 });
                $rootScope.isMAZE=$scope.subjects.find(data=>{ return data.id==3 });
                $rootScope.isSMILE=$scope.subjects.find(data=>{ return data.id==4 });
                $rootScope.isSMILESG=$scope.subjects.find(data=>{ return data.id==7 });

                $rootScope.isMAZE=$rootScope.isMAZE?true:false;
                $rootScope.isSMILE=$rootScope.isSMILE?true:false;
                $rootScope.isSMILESG=$rootScope.isSMILESG?true:false;
                $rootScope.isBUZZ=$rootScope.isBUZZ?true:false;
                $rootScope.isBUZZRU=$rootScope.isBUZZRU?true:false;
            }
        });
    });

    ignitefactory.Getcategorydetails({'subject_id':$scope.subject_id}).then(function(response) {
        if(response.data.status == 1){
            $scope.purchaseCategory = response.data.data.purchaseCategory;
            $scope.switchCount      = response.data.data.switchCount;
            $scope.getSubjectCategory($scope.subject_id);
        } else {
            if(response.data.status == 0){
                simpleAlert('error', '', 'Please Purchase Plan');
            }
            if(response.data.code==2) {
                window.location.replace("ignite-login");
            }
        }
    });

    $scope.getSubjectCategory = function (subject_id) {
        ignitefactory.getSubjectCategory({'subject_id':subject_id}).then(function(response) {
            if(response.data.status == 1){
                $scope.category_list = response.data.data;        
            } else {
                if(response.data.code==2) {
                    window.location.replace("ignite-login");
                }
            }
        });
    }

    $scope.getData = function (toCategory) {
        $scope.toCategory = toCategory;
    }

    $scope.MakeSwitch = function (toCategory) {
        if($('#valid').prop("checked")){
            if($scope.toCategory == null || $scope.toCategory == undefined || $scope.toCategory == ''){
                simpleAlert('error', '', 'Please Select Category');
            } else{
                ignitefactory.SwitchPlan({'switch_categoryid':$scope.toCategory,'purchase_category_id':$scope.purchaseCategory[0].id,'main_category_id':$scope.purchaseCategory[0].id}).then(function(response) {
                    if(response.data.status == 1){
                        if(response.data.code == 3){
                            simpleAlert('error', '', response.data.data.error);
                        } else {
                            window.location.replace('my-subscription');
                        }
                    } else {
                        if(response.data.status == 0){
                            simpleAlert('error', '', response.data.message.error);
                        }
                        if(response.data.code==2) {
                            window.location.replace("ignite-login");
                        }
                    }
                });
            }
        } else {
            simpleAlert('error', '', 'Please check details confirmation');
        }   
    }

    if($scope.subject_id != null || $scope.subject_id != undefined || $scope.subject_id != ''){
        ignitefactory.SwitchhistoryDetails({'subject_id':$scope.subject_id}).then(function(response) {
            if(response.data.status == 1){
                $scope.SwitchhistoryDetails = response.data.data;
            } else {
                // if(response.data.status == 0){
                //     simpleAlert('error', '', 'Please Purchase Plan');
                // }
                if(response.data.code==2) {
                    window.location.replace("ignite-login");
                }
            }
        });
    }

}]);
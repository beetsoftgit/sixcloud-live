/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 6th Sept 2018
 Name    : IgniteQuizResultController
 Purpose : All the functions for subscribers quiz result
 */
angular.module('sixcloudApp').controller('IgniteQuizResultController', ['$sce', 'ignitefactory', '$location', '$routeParams', '$anchorScroll', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, ignitefactory, $location, $routeParams, $anchorScroll, $http, $scope, $timeout, $rootScope, $filter) {
    /*urlObj = JSON.parse(localStorage.getItem('urlObj'));
    url_number_static = localStorage.getItem('url_number_static');
    url_number_static++;
    url = [{
        "url_number": url_number_static,
        "url": $location.url()
    }];
    last_url = urlObj.pop();
    localStorage.setItem('urlObj',JSON.stringify(urlObj));
    if(last_url[0].url_number !== url[0].url_number) {
        urlObj.push(last_url);
    }
    urlObj.push(url);
    localStorage.setItem("urlObj",JSON.stringify(urlObj));
    localStorage.setItem("url_number_static",url_number_static);*/
    
    $scope.url = $location.url();
    $scope.urlEncoded   = $routeParams.slug;
    $scope.urlDecoded   = atob($scope.urlEncoded);
    var data            = $scope.urlDecoded.split("/");
    $scope.id           = data[0];
    $scope.index        = data[1];
    // $scope.id = $routeParams.slug;
    $("#content").removeClass('hide');
    $rootScope.showiframe = false;
    $rootScope.subheader = '';
    ignitefactory.Getsubjects().then(function (response) {
        if(response.data.status == 1){
            $rootScope.subjects = response.data.data;
            $rootScope.isBUZZ=$rootScope.subjects.find(data=>{ return data.id==1 });
            $rootScope.isBUZZRU=$rootScope.subjects.find(data=>{ return data.id==2 });
            $rootScope.isMAZE=$rootScope.subjects.find(data=>{ return data.id==3 });
            $rootScope.isSMILE=$rootScope.subjects.find(data=>{ return data.id==4 });
            $rootScope.isSMILESG=$rootScope.subjects.find(data=>{ return data.id==7 });
            
            $rootScope.isMAZE=$rootScope.isMAZE?true:false;
            $rootScope.isSMILE=$rootScope.isSMILE?true:false;
            $rootScope.isSMILESG=$rootScope.isSMILESG?true:false;
            $rootScope.isBUZZ=$rootScope.isBUZZ?true:false;
            $rootScope.isBUZZRU=$rootScope.isBUZZRU?true:false;
            
            $rootScope.subjectsCount = 12/response.data.data.length;
        }else if(response.data.status == 0){
            $scope.message = response.data.message;
            if(response.data.code==2){
                window.location.replace("ignite-login");
            }
        }
    });

    ignitefactory.Getquizresult({'id':$scope.id}).then(function (response) {
        if(response.data.status == 1){
            $scope.quizResult = response.data.data.attempts;
            let lookup = {};
            let result1 = [];
            let result2 = [];
            $scope.quizResult.forEach(element => {
                if(!lookup[element['question_id']]) {
                    lookup[element['question_id']] = true;
                    result1.push(element);
                }else{
                    lookup[element['question_id']] = true;
                    result2.push(element);
                }
            });
            $scope.quizResult=result2.length>0?result2:result1
            $scope.quizScore=0;
            $scope.quizResult.forEach(ele =>{
                if(ele.is_correct==1){
                    $scope.quizScore=$scope.quizScore+1;
                }
            })
            // $scope.quizScore = response.data.data.score;
            $scope.quizDetails = response.data.data.quiz_details;
            $scope.category_id = $scope.quizDetails.subcategory_id?$scope.quizDetails.subcategory_id:$scope.quizDetails.category_id
            $scope.score_per_attempts = response.data.data.score_per_attempts;
        }else if(response.data.status == 0){
            if(response.data.code==3){
                window.location.replace("ignite-categories");
            }
            if(response.data.code==2){
                window.location.replace("ignite-login");
            }         
        }            
    });
    function redirectPlan() {
        window.location.href = "my-subscription/" + $scope.slug;
    }
    $scope.cantView = function(slug){
        //check if subscription is allowed or not, shown only if subscription is allowed
        if($rootScope.loginDistributorData.is_subscription_allowed == 1) {
            // simpleAlert('error', '', "You need to first buy the subscription to access '"+data+".'");
            if($rootScope.transLang=='en')
                $scope.purchasePlan = 'Please purchase this plan';
            else
                $scope.purchasePlan = '您还未订阅这计划';
            // simpleAlert('error', '', $scope.purchasePlan);
            $scope.slug = slug;
            // simpleAlert('error', '', "You need to first buy the subscription to access.");
            confirmDialogueAlert("", $scope.purchasePlan, "", false, '#f17598', 'Ok', '', true, '', '', redirectPlan);
        }
    }
    $timeout(function(){
        // $scope.category_id = $scope.quizDetails.subcategory_id?$scope.quizDetails.subcategory_id:$scope.quizDetails.category_id
        ignitefactory.Getallcontent({'categoryId':$scope.category_id}).then(function (response) {                      
            if(response.data.status == 1){
                $scope.allData = response.data.data.allData;
                angular.forEach($scope.allData, function(value, key) {
                    if(value.slug == $scope.id){
                        $scope.content_type = value.content_type;
                        if(key!=$scope.allData.length-1){
                            if($scope.allData[key+1].can_view!=0){
                                $scope.nextSlug = $scope.allData[key+1].slug;
                                $scope.nextContentType = $scope.allData[key+1].content_type;
                                $scope.showNext = 'show';
                            }else{
                                $scope.nextSlug = $scope.allData[key+1].slug;
                                $scope.showNext = 'hide';
                            }
                        } else {
                            $scope.nextSlug = undefined;
                            $scope.nextContentType = undefined;
                        }
                    }
                });
            }else if(response.data.status == 0){
                if(response.data.code==2){
                    window.location.replace("ignite-login");
                }         
            }            
        });

    },1500);

    $scope.redirect = function(slug,content_type,index){
        var string          = slug+"/"+content_type+"/"+index;
        $scope.urlEncoded   = btoa(string);
        window.location.replace("content-detail/"+$scope.urlEncoded); 
    }
}]);

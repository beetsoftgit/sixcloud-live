/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 31st Aug 2018
 Name    : IgniteQuizController
 Purpose : All the functions for subscribers quiz attempt
 */
angular.module('sixcloudApp').controller('IgniteQuizController', ['$sce', 'ignitefactory', '$location', '$routeParams', '$anchorScroll', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, ignitefactory, $location, $routeParams, $anchorScroll, $http, $scope, $timeout, $rootScope, $filter) {
    /*urlObj = JSON.parse(localStorage.getItem('urlObj'));
    url_number_static = localStorage.getItem('url_number_static');
    url_number_static++;
    url = [{
        "url_number": url_number_static,
        "url": $location.url()
    }];
    last_url = urlObj.pop();
    localStorage.setItem('urlObj',JSON.stringify(urlObj));
    if(last_url[0].url_number !== url[0].url_number) {
        urlObj.push(last_url);
    }
    urlObj.push(url);
    localStorage.setItem("urlObj",JSON.stringify(urlObj));
    localStorage.setItem("url_number_static",url_number_static);*/
    $scope.url = $location.url();
    $scope.urlEncoded   = $routeParams.slug;
    $scope.urlDecoded   = atob($scope.urlEncoded);
    var data            = $scope.urlDecoded.split("/");
    $scope.id           = data[0];
    $scope.index        = data[1];
    // $scope.id = $routeParams.slug;
    $("#content").removeClass('hide');
    $rootScope.showiframe = false;
    // $(function() {
    //     $('audio').audioPlayer();
    // });
    /*var distance = $('.quiz-scroll-to-fix').offset().top; 
    $('.quiz-scroll-to-fix').affix({ 
      offset: {
        top: distance,
         
      } 
    });*/
    console.log('quiz controller');
    $scope.quiz_start_time = new Date();
    $scope.currentQuestionAttempting = 0;


    function startAgainQuiz(){
        $scope.quizscore = 0
        ignitefactory.Getquizdetail({'id':$scope.id,'isDetele':1}).then(function (response) {
            if(response.data.status == 1){
                $scope.quiz = response.data.data.data;             
                $scope.alreadyattempted = $scope.quiz.alreadyattempted
                $scope.no_of_attempt = response.data.data.no_of_attempt;
                $scope.totalQuestion = $scope.quiz.quizquestions.length;          
                $scope.resume_quiz = response.data.data.resume_quiz;
                
                $scope.quizscore = response.data.data.quiz_count
                $scope.ansAttempted = 0
                
            }else if(response.data.status == 0){
                // if(response.data.code==3){
                //     simpleAlert('error', '', response.data.message, false);
                //     $timeout(function(){
                //         window.location.replace("score-card/"+response.data.data.slug);
                //     },1500);
                // }
                if(response.data.code==2){
                    window.location.replace("ignite-login");
                }         
            }            
        });        
    }
    function resumeQuiz(){
        $scope.currentQuestionAttempting = 0
        $scope.quizscore = 0;
        angular.forEach($scope.quiz.quizquestions, function(value, key) {
            if(value.alreadyattempted.length>0){
                angular.forEach(value.quizquestionsoptions, function(option, optionindex) {
                    setTimeout(function() {
                        $('#label_'+option.id).css('pointer-events','none');
                    }, 1000);
                });
                value.alreadyattempted.forEach(function (ele,index) {
                    $scope.ansAttempted++;
                    $scope.currentQuestionAttempting++;
                    if(ele.is_correct){
                        $scope.quizscore = $scope.quizscore + 1
                        setTimeout(function() {
                            $('#numberDiv_'+value.id).addClass('green');
                            $('#spanId_'+ele.attempted_answer).removeClass('check_custom');
                            $('#spanId_'+ele.attempted_answer).addClass('alreadyattemptedtrue');
                        }, 1000);
                    }else{
                        setTimeout(function() {
                            $('#numberDiv_'+value.id).addClass('red');
                            $('#spanId_'+ele.attempted_answer).removeClass('check_custom');
                            $('#spanId_'+ele.attempted_answer).addClass('alreadyattemptedfalse');
                        }, 1000);
                    }
                })
            }    
        });
    }

    ignitefactory.Getquizdetail({'id':$scope.id,'isDetele':0}).then(function (response) {
        if(response.data.status == 1){
            $scope.quiz = response.data.data.data;             
            $scope.alreadyattempted = $scope.quiz.alreadyattempted
            $scope.no_of_attempt = response.data.data.no_of_attempt;
            $scope.totalQuestion = $scope.quiz.quizquestions.length;          
            $scope.resume_quiz = response.data.data.resume_quiz;
            
            $scope.quizscore = response.data.data.quiz_count
            $scope.currentQuestionAttempting = response.data.data.attemptQuestioncount;
            $scope.ansAttempted = 0
            if($scope.resume_quiz==1){
                confirmDialogueForResumeQuiz('Continue where you left off?','','Cancel',resumeQuiz,startAgainQuiz)
            }
            
        }else if(response.data.status == 0){
            // if(response.data.code==3){
            //     simpleAlert('error', '', response.data.message, false);
            //     $timeout(function(){
            //         window.location.replace("score-card/"+response.data.data.slug);
            //     },1500);
            // }
            if(response.data.code==2){
                window.location.replace("ignite-login");
            }         
        }            
    });
    $rootScope.subheader = '';
    ignitefactory.Getsubjects().then(function (response) {
        if(response.data.status == 1){
            $rootScope.subjects = response.data.data;
            $rootScope.isBUZZ=$rootScope.subjects.find(data=>{ return data.id==1 });
            $rootScope.isBUZZRU=$rootScope.subjects.find(data=>{ return data.id==2 });
            $rootScope.isMAZE=$rootScope.subjects.find(data=>{ return data.id==3 });
            $rootScope.isSMILE=$rootScope.subjects.find(data=>{ return data.id==4 });
            $rootScope.isSMILESG=$rootScope.subjects.find(data=>{ return data.id==7 });
            
            $rootScope.isMAZE=$rootScope.isMAZE?true:false;
            $rootScope.isSMILE=$rootScope.isSMILE?true:false;
            $rootScope.isSMILESG=$rootScope.isSMILESG?true:false;
            $rootScope.isBUZZ=$rootScope.isBUZZ?true:false;
            $rootScope.isBUZZRU=$rootScope.isBUZZRU?true:false;
            
            $rootScope.subjectsCount = 12/response.data.data.length;
        }else if(response.data.status == 0){
            $scope.message = response.data.message;
            if(response.data.code==2){
                window.location.replace("ignite-login");
            }
        }
    });

    $scope.userCorrectAnswer = 0;
    setTimeout(function() {
        if($scope.ansAttempted){
            $scope.currentQuestionAttempting = ($scope.ansAttempted!=0)?($scope.ansAttempted+1):1;
        }else{
            $scope.currentQuestionAttempting = 0;
        }
    }, 1000);
    /*$timeout(function(){
        $scope.totalQuestion = $scope.quiz.quizquestions.length;
    },3000);*/

    $scope.Checkandchangequestion = function(question,option){
        if(question.alreadyattempted.length > 0){
            swal({
                title: "",
                text: "This question is already attempted.",
                confirmButtonText: "Ok",
                allowEscapeKey:false
            });
        }else{
            var answerlength = 0;
            answerlength = $('#question_'+question.id+' :input[type="checkbox"]:checked').length;
            if(question.number_of_correct_answers==answerlength){
                document.getElementById("overlay").style.display = "block";
                $timeout(function(){
                    userAnswerArray = [];
                    $('#question_'+question.id+' :input[type="checkbox"]:checked').each(function(){
                        userAnswerArray.push($(this).val());
                    });
                    // $anchorScroll('question_'+$scope.currentQuestionAttempting);
                    $scope.correct_answers_array = [];
                    angular.forEach(question.quizquestionsoptions, function(value, key) {
                        if(value.correct_answer==1)
                            $scope.correct_answers_array.push(value.id);
                    });
                    if($scope.correct_answers_array.length==userAnswerArray.length){
                        userAnsweredTrue  = [];
                        userAnsweredFalse = [];
                        $scope.number_of_correct_answers_given = $scope.quizscore;
                        angular.forEach($scope.correct_answers_array, function(value, key) {
                            angular.forEach(userAnswerArray, function(valueInner, keyInner) {
                                if(value == valueInner){
                                    userAnsweredTrue.push(value);
                                    $scope.number_of_correct_answers_given++;
                                    $scope.quizscore++;
                                }
                                else
                                    userAnsweredFalse.push(valueInner);
                            });
                        });
                    }
                    if(userAnsweredFalse.length==0){
                        $scope.is_correct = 1;
                        $scope.userCorrectAnswer++;
                        $('#numberDiv_'+question.id).addClass('green');
                        $('#spanId_'+option.id).removeClass('check_custom');
                        audioElementRight.play();
                    } else {
                        $('#numberDiv_'+question.id).addClass('red');
                        audioElementWrong.play();
                        $scope.is_correct = 0;
                        angular.forEach(userAnsweredFalse, function(value, key) {
                            $('#label_'+value).addClass('cancle-btn');
                            $('#spanId_'+value).removeClass('check_custom');
                        });
                        angular.forEach($scope.correct_answers_array, function(value, key) {
                            $('#answer_'+value).attr('checked','checked');
                            $('#label_'+value).removeClass('cancle-btn');
                            $('#spanId_'+value).removeClass('check_custom');
                        });
                    }
                    if(question.number_of_correct_answers>1){
                        if($scope.number_of_correct_answers_given==question.number_of_correct_answers){
                            $scope.is_correct = 1;
                            $scope.userCorrectAnswer++;
                            $scope.quizscore++;
                            $('#numberDiv_'+question.id).addClass('green');
                            audioElementRight.play();
                        } else{
                            $scope.is_correct = 0;
                            $('#numberDiv_'+question.id).addClass('red');
                            audioElementWrong.play();
                        }
                    }
                    
                    ignitefactory.Storeattempt({'quiz_id':question.quiz_id,'question_id':question.id,'attempted_answer':userAnswerArray,'correct_answers_array':$scope.correct_answers_array,'platform':1,'quiz_completed_status':2,'is_correct':$scope.is_correct,'no_of_attempt':$scope.no_of_attempt}).then(function (response) {
                        if(response.data.status == 1){
                            $timeout(function(){
                                $('#question_'+question.id).find('input:checkbox').attr("disabled", "disabled");
                                if($scope.currentQuestionAttempting<=$scope.totalQuestion)
                                    $scope.currentQuestionAttempting++;
                                
                                document.getElementById("overlay").style.display = "none";
                                angular.forEach(response.data.data.unselectedAnswer, function(value, key) {
                                    $('#spanId_'+value).addClass('check_box_blink');
                                    $timeout(function(){
                                        $('#spanId_'+value).removeClass('check_box_blink');
                                    },1200);
                                });
                                if($scope.currentQuestionAttempting>=$scope.totalQuestion){
                                    $timeout(function(){
                                        $('#end_quiz_td').addClass('shake_custom');
                                         $anchorScroll();
                                    },1200);
                                    
                                }
                                $timeout(function(){
                                    $('#end_quiz_td').removeClass('shake_custom');
                                },3000);
                            },1000);

                        } else if(response.data.status == 0) {
                            if(response.data.code==2){
                                window.location.replace("ignite-login");
                            }         
                        }            
                    });
                    
                },1000);
            }
        }
        
    }
    var audioElementRight = document.createElement('audio');
    audioElementRight.setAttribute('src', 'resources/assets/images/ignite/right.mp3');

    var audioElementWrong = document.createElement('audio');
    audioElementWrong.setAttribute('src', 'resources/assets/images/ignite/wrong.mp3');
    $scope.Endquiz = function(){
        if($scope.currentQuestionAttempting<$scope.totalQuestion){
            window.location.replace('ignite-categories');
        } else {
            let start_time = $scope.quiz_start_time;
            let end_time = new Date();
            ignitefactory.Endquiz({'id':$scope.id,'quiz_completed_status':1,'no_of_attempt':$scope.no_of_attempt,'start_time':start_time,'end_time':end_time}).then(function (response) {
                if(response.data.status == 1){
                    var str     = $scope.id+"/"+$scope.index;
                    $scope.url  = btoa(str); 
                    window.location.replace("score-card/"+$scope.url);
                    // window.location.replace("score-card/"+$scope.id);
                } else if(response.data.status == 0) {
                    if(response.data.code==2){
                        window.location.replace("ignite-login");
                    }         
                }            
            });
        }
    }
    // history.pushState(null, null, location.href);
    // window.onpopstate = function () {
    //     alert("You need to answer all the questions.");
    //     history.go(1);
    // };
}]);

/*
 Author  : Nivedita Mitra (nivedita@creolestudios.com)
 Date    : 20th Aug 2018
 Name    : IgniteCategoriesController
 Purpose : All the functions for distributors login
 */
angular.module('sixcloudApp').controller('IgniteCategoriesController', ['$sce', 'ignitefactory', '$location', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', '$window', function($sce, ignitefactory, $location, $routeParams, $http, $scope, $timeout, $rootScope, $filter, $window) {
    /*  For Back Button for moving back to select-categories from ignite-categories  */
    $("#content").removeClass('hide');
    $rootScope.showiframe = false;
    $("body").removeClass("home-page");
    $(".category-list").hide();
        $(".tabs-div .tablinks").addClass("hidden-arrow");
    $scope.goBack = function () {
        window.location.href = 'select-categories';
    }

    console.log('IgniteCategories Controller loaded');
    $scope.url = $location.url();
    $scope.changeLangs = Cookies.get('language');
    /*$scope.urlCategory = $routeParams.category;*/
    if (Cookies.get('language') == 'undefined' || Cookies.get('language') == '' || Cookies.get('language') == undefined) {
        if(current_domain=='net'){
            //$translate.use('en');
            $scope.whatLanguageSelect = 'en';
            Cookies.set('language', 'en');
            $scope.changeLangs = Cookies.get('language');
        } else {
            //$translate.use('chi');
            $scope.whatLanguageSelect = 'chi';
            Cookies.set('language', 'chi');
            $scope.changeLangs = Cookies.get('language');
        }
    } else {
        $timeout(function() {
            if($scope.changeLangs=='en'){
                Cookies.set('language', 'en');
                $scope.changeLangs = Cookies.get('language');
            } else if($scope.changeLangs == 'chi') {
                Cookies.set('language', 'chi');
                $scope.changeLangs = Cookies.get('language');
            }else{
                Cookies.set('language', 'ru');
                $scope.changeLangs = Cookies.get('language');
            }
        }, 1000);
    }
    let url = window.location.href.split('/');
    getLanguage = Cookies.get('language');
    $scope.current_domain = current_domain;
    $scope.dataAvailableMessage = "";
    if(Cookies.get('language') == 'undefined' || Cookies.get('language') == '' || Cookies.get('language') == undefined){
        $timeout(function() {
            if($scope.url==undefined){
                $timeout(function() {
                    $scope.changeLanguage('en');
                },500);
            }
            if($scope.url == 'net' && $scope.url!=undefined){
                $timeout(function() {
                    $scope.changeLanguage('en');
                },500);
            }
            if($scope.url == 'cn' && $scope.url!=undefined){
                $timeout(function() {
                    $scope.changeLanguage('chi');
                },500);
            }
        },1000);
    }

    changeLanguage(getLanguage);
    function changeLanguage(lang) {
        if (lang == 'en') $scope.changeLangs = 'en';
        else if (lang == 'chi') $scope.changeLangs = 'chi';
        else $scope.changeLangs = 'ru';
        Cookies.set('language', lang);
        //$translate.use(lang);
    }
    $rootScope.subheader = 'subheader';
    ignitefactory.Getsubjects().then(function (response) {
        if(response.data.status == 1){
            $rootScope.subjects = response.data.data;
            $rootScope.isBUZZ=$rootScope.subjects.find(data=>{ return data.id==1 });
            $rootScope.isBUZZRU=$rootScope.subjects.find(data=>{ return data.id==2 });
            $rootScope.isMAZE=$rootScope.subjects.find(data=>{ return data.id==3 });
            $rootScope.isSMILE=$rootScope.subjects.find(data=>{ return data.id==4 });
            $rootScope.isSMILESG=$rootScope.subjects.find(data=>{ return data.id==7 });
            
            $rootScope.isMAZE=$rootScope.isMAZE?true:false;
            $rootScope.isSMILE=$rootScope.isSMILE?true:false;
            $rootScope.isSMILESG=$rootScope.isSMILESG?true:false;
            $rootScope.isBUZZ=$rootScope.isBUZZ?true:false;
            $rootScope.isBUZZRU=$rootScope.isBUZZRU?true:false;
            
            $rootScope.subjectsCount = 12/response.data.data.length;
        }else if(response.data.status == 0){
            $scope.message = response.data.message;
            if(response.data.code==2){
                window.location.replace("ignite-login");
            }
        }
    });

    $scope.ChangeSubject = function(tabClass){
        $scope.dataAvailableMessage = "";
        $scope.subCategoriesExist = false;
        ignitefactory.getCategoriesForSubjectFromSubjectId({'id':$routeParams.subject_id,'language':Cookies.get('language')}).then(function (response) {
            if(response.data.status == 1){
                $scope.categories = response.data.data;
                $scope.categoriesExist = $scope.categories.length>0?true:false;
                $scope.categoriesCount = 12/$scope.categories.length;
                if(response.data.data[0].subject_id==3 || response.data.data[0].subject_id==4 || response.data.data[0].subject_id==7){
                    $rootScope.Mathematics = "Mathematics";
                    if(response.data.data[0].subject_id==3){
                        $rootScope.ignitePage = "Math";
                    }else if(response.data.data[0].subject_id==4){
                        $rootScope.ignitePage = "Smile";
                    }else{
                        $rootScope.ignitePage = "SmileSG";
                    }
                }else if(response.data.data[0].subject_id==1 || response.data.data[0].subject_id==2){
                    $rootScope.ignitePage = "English";
                }
                // $('.'+tabClass).removeClass('active');
                $timeout(function() {
                    $(".owl-stage-outer").css("height",50);
                    $('#'+tabClass+'_'+response.data.data[0].id).addClass('active');
                }, 1000);
            } else if(response.data.status == 0) {
                if(response.data.code==2){
                    window.location.replace("ignite-login");
                }
            } else if(response.data.code == 308) {
                window.location.replace('select-categories');
            }
        });
    }
    $scope.ChangeSubject('categoryTab');

    $scope.ChangeCategory = function(whichTab,tabClass,changeWhat,id){
        $(".category-list").slideUp();
        $(".tabs-div .tablinks.active").addClass("hidden-arrow");
        $scope.dataAvailableMessage = "";
        if(changeWhat == 'category') {
            $scope.subCategories = undefined;
            $scope.allData = null;
        }
        $(".owl-stage-outer").css("height",50);
        $scope.selectLanguage = false;
        $('.'+tabClass).removeClass('active');
        $('#'+tabClass+'_'+id).addClass('active');
        ignitefactory.Getsubcategories({'id':whichTab,'language':Cookies.get('language')}).then(function (response) {
            if(response.data.status == 1){
                $(".tabs-div .tablinks.active").removeClass("hidden-arrow");
                $scope.subCategories = response.data.data;
                $rootScope.selectedCategoryname = $scope.subCategories[0].category_name;
                $scope.subCategoriesExist = $scope.subCategories.length>0?true:false;
                
                // $scope.categoryId = $rootScope.selectedTabData?$rootScope.selectedTabData.id:$scope.subCategories[0].id
                $scope.getData($scope.subCategories[0].id);
                if($scope.subCategories.length > 0) {
                    $(".owl-stage-outer").css("height",(50 + 50 * $scope.subCategories.length));
                }
            } else if(response.data.status == 0) {
                if(response.data.code==2) {
                    window.location.replace("ignite-login");
                } else {
                    if(changeWhat == 'category') {
                        $(".tabs-div ").find("button").addClass("hidden-arrow");
                    }
                    if($rootScope.categoryId){
                        $scope.getData($rootScope.categoryId);
                    }else{
                        $scope.getData(id);
                    }
                }
            }
        });
    }

    $scope.getData = function(categoryId) {
        ignitefactory.Getallcontent({'categoryId':categoryId,'lang':$scope.changeLangs}).then(function (response) {                      
            if(response.data.status == 1){
                // $(".category-list").hide();
                // $(".tabs-div .tablinks").addClass("hidden-arrow");
                $(".owl-stage-outer").css("height",50);
                $rootScope.selectedTabData = response.data.data.selectedCategory[0];
                console.log($scope.categoriesExist);
                $scope.allData = response.data.data;
                console.log($scope.categoriesExist);
                $rootScope.selectedcategoryID = $scope.allData.allData[0].video_category_id;
            }else if(response.data.status == 0){
                if(response.data.code==2){
                    window.location.replace("ignite-login");
                } else if(response.data.code==308){
                    window.location.replace('select-categories');
                } else {
                    $rootScope.selectedTabData = response.data.code.topic[0];
                    $scope.allData = null;
                    $scope.dataAvailableMessage = response.data.message;
                }
            }            
        }); 
    }
    function redirect() {
        window.location.href = "my-subscription/" + $scope.slug;
    }
    $rootScope.content_type=0;
    $scope.Cantview = function(data, slug,content_type){
        //check if subscription is allowed or not, shown only if subscription is allowed
        if($rootScope.loginDistributorData.is_subscription_allowed == 1) {
            // simpleAlert('error', '', "You need to first buy the subscription to access '"+data+".'");
            if(getLanguage=='en')
                $scope.purchasePlan = 'Please purchase this plan';
            else
                $scope.purchasePlan = '您还未订阅这计划';
            // simpleAlert('error', '', $scope.purchasePlan);
            $scope.slug = slug;
            $rootScope.content_type=(content_type==1)?1:(content_type==2)?2:3;
            $scope.slug = $scope.slug+':'+$rootScope.content_type;
            confirmDialogueAlert("", $scope.purchasePlan, "", false, '#f17598', 'Ok', '', true, '', '', redirect);
        }
    }
    $scope.downloadWorksheet = function(data){
        ignitefactory.DownloadWorksheet({'id': data.id}).then(function (response) {                      
            if(response.data.code === 1) {
                let url = window.location.href;
                let urlData = url.split( '/' );
                var a = document.createElement('a');
                a.target="_blank";
                let downloadURL = data.category.category_name +" - "+data.title+'.pdf';
                a.download = downloadURL
                if (location.hostname === "localhost"){
                    a.href=response.data.data.url+response.data.data.path+data.file_name;
                    //a.href='http://localhost/sixclouds-web'+response.data.data.path+data.file_name;
                }else{
                    if(urlData.includes("beta")){
                        a.href=response.data.data.url+response.data.data.path+data.file_name;
                        //a.href='https://sixclouds.net/beta'+response.data.data.path+data.file_name;
                    }else{
                        a.href=response.data.data.url+response.data.data.path+data.file_name;
                        //a.href='https://sixclouds.net'+response.data.data.path+data.file_name;
                    }
                }
                a.click();
            }
        });
    }
    $scope.WatchPrevious = function(data){
        if(getLanguage=='en'){
            $scope.pleaseAttemptWorksheet  = 'Please attempt previous';
            $scope.pleaseAttemptQuiz = 'Please attempt previous';
            $scope.pleaseDownload = 'Please download previous';
            $scope.video          = 'video';
            $scope.quiz           = 'quiz';
            $scope.worksheet      = 'worksheet';
        }
        else{
            $scope.pleaseAttemptWorksheet = '请下载';
            $scope.pleaseAttemptQuiz = '请参加';
            $scope.pleaseDownload = '请观看';
            $scope.video          = '视频';
            $scope.quiz           = '测验';
            $scope.worksheet      = '作业';
        }
        if(data.content_type==1)
            simpleAlert('error', '', $scope.pleaseAttemptQuiz+' '+data.title+' '+$scope.video);
        if(data.content_type==2)
            simpleAlert('error', '', $scope.pleaseAttemptQuiz+' '+data.title+' '+$scope.quiz);
        if(data.content_type==3)
            simpleAlert('error', '', $scope.pleaseAttemptWorksheet+' '+data.title+' '+$scope.worksheet);
    }
    setTimeout(function() {
        $(".category-list").slideUp();
        $(".tabs-div .tablinks.active").addClass("hidden-arrow");
    }, 2000);
    $(".tabs-div ").find("button").addClass("hidden-arrow");
    setTimeout(function(){
        $('#owl-carousel').owlCarousel({
            loop:false,
            margin:10,
            nav:true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:2
                },
                767:{
                    items:3
                },
                1000:{
                    items:3
                }
            }
        });
        var owl = $('.owl-carousel');
        owl.on('translate.owl.carousel', function(event) {
            $(".category-list").slideUp();
            // $(".categories_dropdown button").removeClass("active");
            $(".tabs-div .tablinks.active").addClass("hidden-arrow");
        })
    }, 1000);

    $(document).mouseup(function(e){
        var container = $("#categories");
        if(!container.is(e.target) && container.has(e.target).length === 0){
            container.slideUp();
            // $(".categories_dropdown button").removeClass("active");
            $(".tabs-div .tablinks.active").addClass("hidden-arrow");
        }
    });

    $scope.redirect = function(slug,content_type,index){
        var string          = slug+"/"+content_type+"/"+index;
        $scope.urlEncoded   = btoa(string);
        window.location.replace("content-detail/"+$scope.urlEncoded); 
    }
}]);

/*
 Author  : Nivedita Mitra (nivedita@creolestudios.com)
 Date    : 30th Aug 2018
 Name    : IgniteHomeController
 Purpose : All the functions for Ignite home page
 */
angular.module('sixcloudApp').controller('IgniteHomeController', ['$sce', 'ignitefactory', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter','MetaService', '$location', function($sce, ignitefactory, $routeParams, $http, $scope, $timeout, $rootScope, $filter,MetaService, $location, $translate) {
    // create a message to display in our view
    var ctrl = this;
    console.log('IgniteHomeController loaded');

    $("#content").removeClass('hide');
    $rootScope.showiframe = false;

    $scope.changeLangs = Cookies.get('language');
    $rootScope.transLang = Cookies.get('language');
    if($scope.changeLangs == 'en'){
        $scope.changeLanguage('en');
        $scope.changeLangs = 'en';
    }else if($scope.changeLangs == 'chi'){
        $scope.changeLanguage('chi');
        $scope.changeLangs = 'chi';
    }else if($scope.changeLangs == 'ru'){
        $scope.changeLanguage('ru');
        $scope.changeLangs = 'ru';
    }else{
        $scope.changeLanguage('en');
        $scope.changeLangs = 'en';
    }
    ignitefactory.Getloginuserdatadistributor().then(function(response) {
        if (response.data.status == 1) {
            $rootScope.loginDistributorData = response.data.data;
        } else {
            $rootScope.loginDistributorData = response.data;
        }
    });
    // ignitefactory.Mysubscriptiondata().then(function (response){
    //     $scope.customersupport=response.data.data.date;
    // });

    // $scope.CustomerSupport = function() {
    //     window.location.href = "customer-support";
    // };

    $('body').addClass('home-page');
    let url = window.location.href.split('/');
    if(url.includes('ignite') || url.includes('ignite-categories')){
        $rootScope.ignitePage = "Home";
        $rootScope.Mathematics = "";
    }else if(url.includes('ignite-buzz')){
        $rootScope.ignitePage = "English";
        $rootScope.Mathematics = "";
        
    }else if(url.includes('ignite-mathematics')){
        $rootScope.ignitePage = "Mathematics";
    }else if(url.includes('ignite-smile-international')){
        $rootScope.Mathematics = "Mathematics";
        $rootScope.ignitePage = "";
    }else if(url.includes('ignite-smile-singapore')){
        $rootScope.Mathematics = "Mathematics";
        $rootScope.ignitePage = "";
    }else if(url.includes('ignite-maze')){
        $rootScope.Mathematics = "Mathematics";
        $rootScope.ignitePage = "";
    }else{
        $rootScope.Mathematics = "";
        $rootScope.ignitePage = "";
    }
    
    $rootScope.subheader = '';
    $rootScope.igniteId = '';

    ignitefactory.Getsubjects().then(function (response){
        $scope.userSub = response.data.data
        $rootScope.isBUZZ=$scope.userSub.find(data=>{ return data.id==1 });
        $rootScope.isBUZZRU=$scope.userSub.find(data=>{ return data.id==2 });
        $rootScope.isMAZE=$scope.userSub.find(data=>{ return data.id==3 });
        $rootScope.isSMILE=$scope.userSub.find(data=>{ return data.id==4 });
        $rootScope.isSMILESG=$scope.userSub.find(data=>{ return data.id==7 });
        $rootScope.isSMILEPH=$scope.userSub.find(data=>{ return data.id==16 });

        $rootScope.isMAZE=$rootScope.isMAZE?true:false;
        $rootScope.isSMILE=$rootScope.isSMILE?true:false;
        $rootScope.isSMILESG=$rootScope.isSMILESG?true:false;
        $rootScope.isBUZZ=$rootScope.isBUZZ?true:false;
        $rootScope.isBUZZRU=$rootScope.isBUZZRU?true:false;
    });

    ignitefactory.Getapkdownloadlink().then(function (response){
        $scope.downloadLink = response.data.data.link;
    });

    $scope.reqSwitch = function(){
         window.location.href = "customer-support";
    }
    $scope.mySubscription = function(lang, page){
        if(lang=='en'){
            title = "";
            text = "Please contact customer support for more information.";
            confirmButtonText = "Ok";
        }else if(lang=='chi'){
            title = "";
            text = "请联系客服中心以获取更多详情。";
            confirmButtonText = "确定";
        }else if(lang=='ru'){
            title = "";
            text = "Пожалуйста, свяжитесь со службой поддержки для получения дополнительной информации.";
            confirmButtonText = "Ладно";
        }
        $scope.subject_id = "";
        if(page=="SMILE (INTERNATIONAL)"){
            $scope.subject_id = "SMILE (INT'L)";  
            $scope.isSubAllowed = $scope.userSub.find(data=>{ return data.subject_display_name==$scope.subject_id });  

            $scope.redirectSubject=$scope.userSub.find(data => { return data.subject_name == page; });
            $rootScope.subject_id = $scope.redirectSubject!=undefined?$scope.redirectSubject.id:undefined;
            $rootScope.url = 'subject_'+btoa($rootScope.subject_id);

        }else if(page=="SMILE (SG)"){
            $scope.subject_id = "SMILE (SG)";
            $scope.isSubAllowed = $scope.userSub.find(data=>{ return data.subject_display_name == $scope.subject_id });  
            $scope.redirectSubject=$scope.userSub.find(data=>{ return data.subject_display_name == $scope.subject_id });
            $rootScope.subject_id = $scope.redirectSubject!=undefined?$scope.redirectSubject.id:undefined;
            $rootScope.url = 'subject_'+btoa($rootScope.subject_id);
        }else{
            $scope.from = page[0].subject_id==1?'BUZZ':(page[0].subject_id==2?'BUZZ RU':'MAZE');
            $scope.subject_id = page[0].subject_id;
            $scope.isSubAllowed = $scope.userSub.find(data=>{ return data.id==$scope.subject_id });
            

            $scope.redirectSubject=$scope.userSub.find(data => { return data.subject_name == $scope.from; });
            $rootScope.subject_id = $scope.redirectSubject!=undefined?$scope.redirectSubject.id:undefined;
            $rootScope.url = 'subject_'+btoa($rootScope.subject_id);
        }

         ignitefactory.Getuserzone().then(function (response){
                $scope.userZone = response.data.data
                if($scope.userZone.is_subscription_allowed == 0) {
                    swal({
                        title: title,
                        text: text,
                        confirmButtonText: confirmButtonText,
                        allowEscapeKey:false
                    }, function() {
                        window.location.href = "customer-support";
                    });
                }else if($scope.isSubAllowed==undefined) {
                    swal({
                        title: title,
                        text: text,
                        confirmButtonText: confirmButtonText,
                        allowEscapeKey:false
                    }, function() {
                        window.location.href = "customer-support";
                    });
                }else{
                    ignitefactory.Getcategoriesforsubject({'id':$rootScope.url}).then(function(response) {
                        if(response.data.data.length > 0){
                            $rootScope.cartTotal = 0;
                            $rootScope.cartSubTotal = 0;
                            $rootScope.selectedCategory = [];
                            $('.subjects').removeClass('active');
                            $('#subjectButton_'+$rootScope.subject_id).addClass('active');
                            $rootScope.showCart = true;
                            $rootScope.buzzData = response.data.data;
                        }
                    });
                    $location.path('my-subscription');
                }
         });
    }

    $scope.ShowVideo = function(){
        $('#showVideoModal').modal('show');
        /*var videoUrl = 'http://localhost/sixclouds-web/public/uploads/promotional-videos/testplayer3.mp4';
        if(Hls.isSupported()){         
            var video = document.getElementById('videoTag');
            var hls = new Hls();
            hls.loadSource(videoUrl);
            hls.attachMedia(video);
            video.play();
            hls.on(Hls.Events.MANIFEST_PARSED,function()
            {         
                video.play();
            });
        } else if (video.canPlayType('application/vnd.apple.mpegurl')){            
            video.src = videoUrl;
            video.addEventListener('canplay',function()
            {
                video.play();
            });
        } */        
    }
    $scope.getCategories = function(){
        // document.getElementById("overlay").style.display = "block";
        ignitefactory.getCategories().then(function (response) {
            if(response.data.status == 1){
                $scope.allCatData = response.data.data;
                if(response.data.message != null){
                    $scope.buzzData = $scope.allCatData.filter(data => { return data.subject_zones.zones == response.data.message.zone_id && data.category_name.startsWith('BUZZ')  });   
                    $scope.buzz = "BUZZ RU";
                    if($scope.buzzData.length <= 0){
                        $scope.buzzData = $scope.allCatData.filter(data => { return data.subject_id == 1 }); 
                        $scope.buzz = "BUZZ";   
                    }
                }else{
                    $scope.buzzData = $scope.allCatData.filter(data => { return data.subject_id == 1 });
                    $scope.buzz = "BUZZ";
                }
                $scope.mazeData = $scope.allCatData.filter(data => { return data.subject_id == 3 });
                $scope.smileData = $scope.allCatData.filter(data => { return data.subject_id == 4 });
                $scope.smileSGData = $scope.allCatData.filter(data => { return data.subject_id == 7 });
                $scope.smilePHData = $scope.allCatData.filter(data => { return data.subject_id == 16 });
                // document.getElementById("overlay").style.display = "none";
            }
        });
        ignitefactory.Getsubcategoriesforlandingpage().then(function (response) {
            if(response.data.status == 1){
                $scope.allSubCategory = response.data.data;
            }
        });
    }
    $scope.getCategories();
    $scope.Getsubcategories = function(mazeData){
        $scope.subCategory = $scope.allSubCategory.filter(data => { return data.parent_id == mazeData.id })
        if($rootScope.transLang=='en'){
            $scope.Category = mazeData.category_name;
        }else if($rootScope.transLang == 'chi'){
            $scope.Category = mazeData.category_name_chi;
        }else if($rootScope.transLang == 'ru'){
            $scope.Category = mazeData.category_name_ru;
        }else{
            $scope.Category = mazeData.category_name;
        }
    }
    
    $('body').on('hidden.bs.modal', '.modal', function () {
        $('video').trigger('pause');
    });
    $scope.changeLanguage = function(lang) {
        if (lang == 'en') $scope.changeLangs = 'en';
        else if (lang == 'chi') $scope.changeLangs = 'chi';
        else $scope.changeLangs = 'ru';
        Cookies.set('language', lang);
        $rootScope.transLang = $scope.changeLangs;
        $translate.use(lang);
    }
}]);
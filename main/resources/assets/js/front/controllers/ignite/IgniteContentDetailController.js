/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 29th Aug 2018
 Name    : IgniteContentDetailController
 Purpose : All the functions for video/quiz/worksheet details
 */
angular.module('sixcloudApp').controller('IgniteContentDetailController', ['$sce', 'ignitefactory', '$location', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, ignitefactory, $location, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {
    /*  For Back Button for moving back to ignite-categories from content-detail  */
    $scope.goBack = function () {
        window.location.href = 'ignite-categories';
    }
    $("#content").removeClass('hide');
    $rootScope.showiframe = false;
    console.log('IgniteContentDetails Controller loaded');
    $rootScope.ignitePage='content-detail';
    $rootScope.Mathematics = "";
    $scope.url = $location.url();
    $scope.urlEncoded   = $routeParams.slug;
    $scope.urlDecoded   = atob($scope.urlEncoded);
    var data            = $scope.urlDecoded.split("/");
    $scope.slug         = data[0];
    $scope.content_type = data[1];
    $scope.index        = data[2];
    // $scope.slug = $routeParams.slug;
    // $scope.content_type = $routeParams.content_type;
    $scope.data = $routeParams.data;

    // $scope.index = $routeParams.index;
    if (Cookies.get('language') == 'undefined' || Cookies.get('language') == '' || Cookies.get('language') == undefined) {
        $scope.url = window.location.host;
        $scope.url = $scope.url.split('.');
        $scope.url = $scope.url[2];
        $timeout(function() {
            if($scope.url==undefined){
                $timeout(function() {
                    $scope.changeLanguage('en');
                },500);
            }
            if($scope.url == 'net' && $scope.url!=undefined){
                $timeout(function() {
                    $scope.changeLanguage('en');
                },500);
            }
            if($scope.url == 'cn' && $scope.url!=undefined){
                $timeout(function() {
                    $scope.changeLanguage('chi');
                },500);
            }
        },1000);
    }
    
    $rootScope.subheader = '';
    ignitefactory.Getsubjects().then(function (response) {
        if(response.data.status == 1){
            $rootScope.subjects = response.data.data;
            $rootScope.isBUZZ=$rootScope.subjects.find(data=>{ return data.id==1 });
            $rootScope.isBUZZRU=$rootScope.subjects.find(data=>{ return data.id==2 });
            $rootScope.isMAZE=$rootScope.subjects.find(data=>{ return data.id==3 });
            $rootScope.isSMILE=$rootScope.subjects.find(data=>{ return data.id==4 });
            $rootScope.isSMILESG=$rootScope.subjects.find(data=>{ return data.id==7 });
            
            $rootScope.isMAZE=$rootScope.isMAZE?true:false;
            $rootScope.isSMILE=$rootScope.isSMILE?true:false;
            $rootScope.isSMILESG=$rootScope.isSMILESG?true:false;
            $rootScope.isBUZZ=$rootScope.isBUZZ?true:false;
            $rootScope.isBUZZRU=$rootScope.isBUZZRU?true:false;
            
            $rootScope.subjectsCount = 12/response.data.data.length;
        }else if(response.data.status == 0){
            $scope.message = response.data.message;
            if(response.data.code==2){
                window.location.replace("ignite-login");
            }
        }
    });
    $scope.getDetail =function(){
        ignitefactory.Contentdetail({'id':$scope.slug,'content_type':$scope.content_type,'data':$scope.data}).then(function (response) {                      
            if(response.data.status == 1){
                $scope.detailData = response.data.data;
                $scope.resumeQuiz = false
                if($scope.detailData.alreadyattemptedquiz && $scope.detailData.alreadyattemptedquiz.length>0){
                    $scope.resumeQuiz = true
                }
                ignitefactory.CategoryExist({'id':$scope.detailData.subcategory_id}).then(function (response) {
                    if(response.data.status == 1){
                        $scope.categoryExist = response.data.data;
                        if($scope.categoryExist != undefined && $scope.categoryExist != 0 && $scope.categoryExist != null)
                            $scope.getData($scope.detailData.subcategory_id);
                        else
                            $scope.getData($scope.detailData.category_id);    
                    }else if(response.data.status == 0){
                        if(response.data.code==2)
                            window.location.replace("ignite-login");
                    }
                });
                // if($scope.detailData.subcategory_id != undefined && $scope.detailData.subcategory_id != 0 && $scope.detailData.subcategory_id != null)
                //     $scope.getData($scope.detailData.subcategory_id);
                // else
                //     $scope.getData($scope.detailData.category_id);
                // $scope.getData($scope.detailData.encoded_string);
            }else if(response.data.status == 0){
                if(response.data.code==2)
                    window.location.replace("ignite-login");
            }            
        });
    }
    $scope.getDetail();
    $scope.getData = function(categoryId){
        ignitefactory.Getallcontent({'categoryId':categoryId}).then(function (response) {                      
            if(response.data.status == 1){
                $scope.allData = response.data.data.allData;
                $scope.category_purchase_status = response.data.data.category_purchase_status;
                angular.forEach($scope.allData, function(value, key) {
                    if(value.slug == $scope.slug){
                        $scope.content_type = value.content_type;
                        if(key!=0){
                            if($scope.allData[key-1].can_view==1){
                                $scope.previousSlug = $scope.allData[key-1].slug;
                                $scope.previousContentType = $scope.allData[key-1].content_type;
                                $scope.noPrevious = false;
                            } else {
                                $scope.previousSlug = $scope.allData[key-1].slug;
                                $scope.previousContentType = $scope.allData[key-1].content_type;

                                $scope.noPreviousKey = key-1;
                                $scope.noPrevious = true;
                            }
                        } else {
                            $scope.noPrevious = false;
                            $scope.previousSlug = undefined;
                            $scope.previousContentType = undefined;
                        }
                        if(key!=$scope.allData.length-1){
                            if($scope.allData[key+1].can_view==1){
                                $scope.nextSlug = $scope.allData[key+1].slug;
                                $scope.nextContentType = $scope.allData[key+1].content_type;
                                $scope.noNext = false;

                                $scope.noNextKey = key+1;
                            } else {
                                // added for redirect to next page as previous watch is removed now
                                $scope.nextSlug = $scope.allData[key+1].slug;
                                $scope.nextContentType = $scope.allData[key+1].content_type;

                                $scope.noNextKey = key+1;
                                $scope.noNext = true;
                            }
                        } else {
                            $scope.nextSlug = undefined;
                            $scope.nextContentType = undefined;
                        }
                    }
                });
            }else if(response.data.status == 0){
                if(response.data.code==2){
                    window.location.replace("ignite-login");
                }         
            }            
        }); 
    }
    $scope.flag = function(){
        simpleAlert('success','','Video flagged.');
    }
    $scope.Showvideo = function(thumbnail,data){
        $('#showVideoModal').modal({backdrop: 'static', keyboard: false});
        var videoUrl = data.video_oss_url;
        var video = document.getElementById('videoTag');
        if(Hls.isSupported()){
            var hls = new Hls();
            hls.loadSource(videoUrl);
            hls.attachMedia(video);
            if(data.views_detail.view_status==2)
                video.currentTime = data.views_detail.resume_from_seconds
            video.play();
            video.onplay = function() {
                $scope.start_time1 = new Date();
            }
            video.onpause = function() {
                let start_time = $scope.start_time1;
                let end_time = new Date();
                $scope.pauseHandle(data,2,video.currentTime,start_time,end_time);
            }
            video.onended = function() {
                let start_time = $scope.start_time1;
                let end_time = new Date();
                $scope.endedHandle(data,1,video.currentTime,start_time,end_time);
            }
            /*hls.on(Hls.Events.MANIFEST_PARSED,function()
            {         
                video.play();
            });*/
        } else if (video.canPlayType('application/vnd.apple.mpegurl')){            
            video.src = videoUrl;
            video.addEventListener('canplay',function()
            {
                video.play();
                video.onplay = function() {
                    $scope.start_time = new Date();
                    $scope.start_time2 = video.currentTime;
                }
                video.onpause = function() {
                    let start_time = $scope.start_time;
                    let end_time = video.currentTime;
                    $scope.pauseHandle(data,2,video.currentTime,start_time,end_time);
                }
                video.onended = function() {
                    let start_time = $scope.start_time;
                    let end_time = new Date();
                    $scope.endedHandle(data,1,video.currentTime,start_time,end_time);
                }
            });
        }
    }
    $scope.pauseHandle = function(data,watchStatus,currentTime,start_time,end_time) {
            ignitefactory.Videoseen({'id':$scope.slug,'videoUrlId':$scope.detailData.video_url.id,'wholeData':data,'watchStatus':watchStatus,'currentTime':currentTime,'start_time':start_time,'end_time':end_time}).then(function (response) {                      
                if(response.data.status == 1){
                    $scope.getDetail();
                }else if(response.data.status == 0){
                    if(response.data.code==2)
                        window.location.replace("ignite-login");
                }            
            });
        // $scope.Closemodal();
    }
    $scope.endedHandle = function(data,watchStatus,currentTime,start_time,end_time) {
            ignitefactory.Videoseen({'id':$scope.slug,'videoUrlId':$scope.detailData.video_url.id,'wholeData':data,'watchStatus':watchStatus,'currentTime':currentTime,'start_time':start_time,'end_time':end_time}).then(function (response) {                      
                if(response.data.status == 1){
                    $scope.getDetail();
                }else if(response.data.status == 0){
                    if(response.data.code==2)
                        window.location.replace("ignite-login");
                }            
            });
        $scope.Closemodal();
    }
    $scope.Closemodal = function(){
        var video = document.getElementById('videoTag');
        video.pause();
        $('#showVideoModal').modal('hide');
    }
    $scope.Cantview = function(key,which){
        if($scope.allData[key].can_view==0&&$scope.allData[key].watch_previous==0){
            $scope.CantviewError($scope.allData[key]);
        }
        if($scope.allData[key].can_view==0&&$scope.allData[key].watch_previous==1){
            $scope.WatchPrevious($scope.allData[key-1]);
        }
        if($scope.category_purchase_status == 0) {
            $scope.CantviewError($scope.allData[key]);
        }
    }

    $scope.CantviewError = function(data){
        if(Cookies.get('language')=='en')
            $scope.purchasePlan = 'Please purchase this plan';
        else
            $scope.purchasePlan = '您还未订阅这计划';
        simpleAlert('error', '', $scope.purchasePlan);
    }
    $scope.WatchPrevious = function(data){
        if(Cookies.get('language')=='en'){
            $scope.pleaseAttempt  = 'Please attempt';
            $scope.pleaseDownload = 'Please download';
            $scope.video          = 'video';
            $scope.quiz           = 'quiz';
            $scope.worksheet      = 'worksheet';
        }
        else{
            $scope.pleaseAttempt  = '请尝试以前';
            $scope.pleaseDownload = '请下载上一个';
            $scope.video          = '视频';
            $scope.quiz           = '测验';
            $scope.worksheet      = '作业';
        }
        if(data.content_type==1||data.content_type==2)
            simpleAlert('error', '', $scope.pleaseAttempt+' '+data.title+' '+$scope.video);
        if(data.content_type==2)
            simpleAlert('error', '', $scope.pleaseAttempt+' '+data.title+' '+$scope.quiz);
        if(data.content_type==3)
            simpleAlert('error', '', $scope.pleaseDownload+' '+data.title+' '+$scope.worksheet);
    }
    $scope.downloadWorksheet = function(data){
        ignitefactory.DownloadWorksheet({'id': data.id}).then(function (response) {                      
            if(response.data.code === 1) {
                let url = window.location.href;
                let urlData = url.split( '/' );

                var a = document.createElement('a');
                a.target="_blank";
                let downloadURL = data.category.category_name +" - "+data.title+'.pdf';
                a.download = downloadURL
                if (location.hostname === "localhost"){
                    a.href=response.data.data.url+response.data.data.path+data.file_name;
                }else{
                    if(urlData.includes("beta")){
                        a.href=response.data.data.url+response.data.data.path+data.file_name;
                        //a.href='https://sixclouds.net/beta'+response.data.data.path+data.file_name;
                    }else{
                        a.href=response.data.data.url+response.data.data.path+data.file_name;
                        //a.href='https://sixclouds.net'+response.data.data.path+data.file_name;
                    }
                }
                //a.href='http://localhost/sixclouds-web/public/uploads/worksheet_files/'+data.file_name;
                a.click();
            }
        });
    }

    $scope.redirect = function(slug,content_type,index){
        var string          = slug+"/"+content_type+"/"+index;
        $scope.urlEncoded   = btoa(string);
        window.location.replace("content-detail/"+$scope.urlEncoded); 
    }

    $scope.takeQuiz = function(slug,index){
        var string          = slug+"/"+index;
        $scope.urlEncoded   = btoa(string);
        window.location.replace("quiz/"+$scope.urlEncoded);   
    }
}]);

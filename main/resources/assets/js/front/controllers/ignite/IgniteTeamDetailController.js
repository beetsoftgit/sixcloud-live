/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 21st Aug 2018
 Name    : IgniteTeamDetailController
 Purpose : All the functions for Ignite Distributors team members
 */
angular.module('sixcloudApp').controller('IgniteTeamDetailController', ['$sce', 'ignitefactory', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', '$location', function($sce, ignitefactory, $routeParams, $http, $scope, $timeout, $rootScope, $filter, $location) {
    /*urlObj = JSON.parse(localStorage.getItem('urlObj'));
    url_number_static = localStorage.getItem('url_number_static');
    url_number_static++;
    url = [{
        "url_number": url_number_static,
        "url": $location.url()
    }];
    last_url = urlObj.pop();
    localStorage.setItem('urlObj',JSON.stringify(urlObj));
    if(last_url[0].url_number !== url[0].url_number) {
        urlObj.push(last_url);
    }
    urlObj.push(url);
    localStorage.setItem("urlObj",JSON.stringify(urlObj));
    localStorage.setItem("url_number_static",url_number_static);*/
    console.log('Ignite my team controller loaded');
    $timeout(function(){
        /*if($rootScope.loginDistributorData == '' || $rootScope.loginDistributorData == undefined){
            window.location.replace("ignite");
        }*/
        $('.ignite-menu').find('li').removeClass('active');
        $('#my-team').addClass('active');
        $('.custom-accordion .panel-heading').click(function () {
            $(this).toggleClass('active');
            $(this).next('.panel-collapse').slideToggle();
        });
    },1000);
    $scope.slug = $routeParams.slug;
    ignitefactory.Teammemberdetail({'slug':$scope.slug}).then(function (response) {                      
        if(response.data.status == 1){
            $scope.teamMember = response.data.data;
            $scope.GetallReferalData($scope.teamMember.id,1);
            $scope.GetsubscribersData($scope.teamMember.id,1);
        }else if(response.data.status == 0){
            if(response.data.code==2)
                window.location.replace("distributor");
        }            
    });

    $scope.GetallReferalData = function (data,pageNumber) {
        ignitefactory.Getteammembersubscribers({'team_member_id':data},pageNumber).then(function (response) {
            if(response.data.status == 1){
                $scope.allReferals = response.data.data.data;                           
                $scope.totalReferals = response.data.data.total;
                $scope.paginate = response.data.data;
                $scope.currentReferalsPaging = response.data.data.current_page;
                $scope.rangeReferalspage = _.range(1, response.data.data.last_page + 1);                
            }else if(response.data.status == 0){
                if(response.data.code==2){
                    if($scope.url == '/subscribers')
                        window.location.replace("distributor");
                    if($scope.url == '/my-referral')
                        window.location.replace("distributor/team-login");
                }         
            }            
        });
    }
    // $scope.GetallReferalData(1);
    $scope.GetsubscribersData = function (data,pageNumber) {
        ignitefactory.GetallReferalData({'getDataFor':data},pageNumber).then(function (response) {                        
            if(response.data.status == 1){
                $scope.allSubscriber = response.data.data.data;                           
                $scope.totalSubscriber = response.data.data.total;
                $scope.paginateSubscriber = response.data.data;
                $scope.currentSubscriberPaging = response.data.data.current_page;
                $scope.rangeSubscriberpage = _.range(1, response.data.data.last_page + 1);                
            }else if(response.data.status == 0){
                if(response.data.code==2){
                    if($scope.url == '/subscribers')
                        window.location.replace("distributor");
                    if($scope.url == '/my-referral')
                        window.location.replace("distributor/team-login");
                }         
            }            
        });
    }
    
}]);
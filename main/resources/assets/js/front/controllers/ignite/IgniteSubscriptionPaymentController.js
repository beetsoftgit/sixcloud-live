/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 18th Dec 2018
 Name    : IgniteStripePaymentController
 Purpose : All the functions for ignite Stripe Payment
 */
angular.module('sixcloudApp').controller('IgniteSubscriptionPaymentController', ['$sce', 'ignitefactory', '$location', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, ignitefactory, $location, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {
    /*urlObj = JSON.parse(localStorage.getItem('urlObj'));
    url_number_static = localStorage.getItem('url_number_static');
    url_number_static++;
    url = [{
        "url_number": url_number_static,
        "url": $location.url()
    }];
    last_url = urlObj.pop();
    localStorage.setItem('urlObj',JSON.stringify(urlObj));
    if(last_url[0].url_number !== url[0].url_number) {
        urlObj.push(last_url);
    }
    urlObj.push(url);
    localStorage.setItem("urlObj",JSON.stringify(urlObj));
    localStorage.setItem("url_number_static",url_number_static);*/
    // console.log(IgniteStripePaymentController);
    $scope.url = $location.url();   
    $scope.token = $routeParams.token;
    $scope.months = [1,2,3,4,5,6,7,8,9,10,11,12];
    $scope.payment = {};
    $scope.payment.exp_month = 0;
    $scope.payment.exp_year = 0;
    $scope.years = [];
    for (var i = new Date().getFullYear(); i < new Date().getFullYear()+20; i ++) {
        $scope.years.push(i);
    }
    $scope.language = Cookies.get('language');
    ignitefactory.Getloginuserdatadistributor().then(function(response) {
        if (response.data.status == 1) {
            $rootScope.loginDistributorData = response.data.data;
        } else {
            $rootScope.loginDistributorData = response.data;
        }
    });
    $("#content").removeClass('hide');
    $rootScope.showiframe = false;
    $rootScope.Twinklemenu = false;
    $rootScope.subheader = '';
    $rootScope.logoutTwinkle = false;
    ignitefactory.Getsubjects().then(function (response) {
        if(response.data.status == 1){
            $rootScope.subjects = response.data.data;
            $rootScope.isBUZZ=$rootScope.subjects.find(data=>{ return data.id==1 });
            $rootScope.isBUZZRU=$rootScope.subjects.find(data=>{ return data.id==2 });
            $rootScope.isMAZE=$rootScope.subjects.find(data=>{ return data.id==3 });
            $rootScope.isSMILE=$rootScope.subjects.find(data=>{ return data.id==4 });
            $rootScope.isSMILESG=$rootScope.subjects.find(data=>{ return data.id==7 });
            
            $rootScope.isMAZE=$rootScope.isMAZE?true:false;
            $rootScope.isSMILE=$rootScope.isSMILE?true:false;
            $rootScope.isSMILESG=$rootScope.isSMILESG?true:false;
            $rootScope.isBUZZ=$rootScope.isBUZZ?true:false;
            $rootScope.isBUZZRU=$rootScope.isBUZZRU?true:false;
            
            $rootScope.subjectsCount = 12/response.data.data.length;
        }else if(response.data.status == 0){
            $scope.message = response.data.message;
            if(response.data.code==2){
                window.location.replace("ignite-login");
            }
        }
    });
    $scope.Makepayment = function(payment){
        if(payment==undefined){
            simpleAlert('error', '', $scope.language=='en'?'Please fill data':'Please fill data');
        }
        if(payment.card_number==undefined||payment.card_number==''||payment.exp_month==0||payment.exp_year==0||payment.cvv==undefined||payment.cvv==''){
            if(payment.card_number==undefined||payment.card_number==''){
                simpleAlert('error', '', $scope.language=='en'?'Enter card number':'输入卡号');
                return;
            }
            if(payment.exp_month==0){
                simpleAlert('error', '', $scope.language=='en'?'Select month':'选择月份');
                return;
            }
            if(payment.exp_year==0){
                simpleAlert('error', '', $scope.language=='en'?'Select year':'选择年份');
                return;
            }
            if(payment.cvv==undefined||payment.cvv==''){
                simpleAlert('error', '', $scope.language=='en'?'Enter CVV/CVC':'输入安全码');
                return;
            }
        } else {
            showLoader('.ignite_payment_data',$scope.language=='en'?'Please wait':'请稍候')
            payment.token = $scope.token;
            ignitefactory.Subscriptionpayemnt(payment).then(function(response) {
                if($scope.language=='en'){hideLoader('.ignite_payment_data','Make Payment');}else{hideLoader('.ignite_payment_data','添加到购物车');}
                if (response.data.status == 2) window.location.replace("ignite-login");
                if (response.data.status == 1) {
                    window.location.replace('my-subscription');
                } else {
                    if($scope.language=='en'){hideLoader('.ignite_payment_data','Make Payment');}else{hideLoader('.ignite_payment_data','添加到购物车');}
                    if(response.data.message!=undefined){
                        simpleAlert('error', '', response.data.message);
                    }else{
                        if($scope.changeLangs=='en'){
                            error = 'Oops, Something went wrong.'
                        }else if($scope.changeLangs=='chi'){
                            error = '哎呀,出了些问题。'
                        }else{
                            error = 'Ой,что-то пошло не так.'
                        }
                        simpleAlert('error', '', error);
                    }
                }
            });
        }
    }
    
}]);
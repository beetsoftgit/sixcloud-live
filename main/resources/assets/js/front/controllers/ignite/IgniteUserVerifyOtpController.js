/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 12th Dec 2018
 Name    : IgniteUserVerifyOtpController
 Purpose : All the functions for user of Cn server phone verification
 */
angular.module('sixcloudApp').controller('IgniteUserVerifyOtpController', ['$sce', 'ignitefactory', '$location', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', '$interval', function($sce, ignitefactory, $location, $routeParams, $http, $scope, $timeout, $rootScope, $filter, $interval) {
    /*urlObj = JSON.parse(localStorage.getItem('urlObj'));
    url_number_static = localStorage.getItem('url_number_static');
    url_number_static++;
    url = [{
        "url_number": url_number_static,
        "url": $location.url()
    }];
    last_url = urlObj.pop();
    localStorage.setItem('urlObj',JSON.stringify(urlObj));
    if(last_url[0].url_number !== url[0].url_number) {
        urlObj.push(last_url);
    }
    urlObj.push(url);
    localStorage.setItem("urlObj",JSON.stringify(urlObj));
    localStorage.setItem("url_number_static",url_number_static);*/
    console.log('IgniteCategories Controller loaded');
    $scope.url = $location.url();

    $scope.country_code = $routeParams.phonecode;
    $scope.phone_number = $routeParams.contact;
    $scope.email_address = $routeParams.email_address;
    if($scope.country_code!=undefined&&$scope.country_code!=''&&$scope.phone_number!=undefined&&$scope.phone_number!=''&&$scope.email_address!=undefined&&$scope.email_address!=''){
        $scope.otpSent = true;
        $scope.resendText = Cookies.get('language')=='en'?"Resend otp":"重发验证码";
        $scope.appendresend = '';
        $scope.appendresendDot = '';
    }

    $scope.Sendotp = function(country_code,phone_number){
        if(country_code==undefined||country_code==''||phone_number==undefined||phone_number==''){
            if(country_code==undefined||country_code==''){
                // simpleAlert('error', '', 'Enter country code');
                $scope.country_code_error = true;
            }
            if(phone_number==undefined||phone_number==''){
                // simpleAlert('error', '', 'Enter phone number');
                $scope.phone_number_error = true;
            }
        } else {
            showLoader('.send_otp');
            $('.disable_input').attr('disabled','disabled');
            var data = {};
            data.country_code = country_code;
            data.phone_number = phone_number;
            if($scope.email_address!=undefined&&$scope.email_address!='')
                data.email_address = $scope.email_address;
            data.language = Cookies.get('language');
            ignitefactory.Senduserotp(data).then(function (response) {
                hideLoader('.send_otp',Cookies.get('language')=='en'?"Resend otp":"重发验证码");
                if(response.data.status == 1){
                    $scope.otpSent = true;
                    var intervalTime = 60000;
                    $scope.resendText = 60;
                    $scope.appendresend = Cookies.get('language')=='en'?"Resend otp":"重发验证码";
                    $scope.appendresendDot = '...';
                    $scope.timer = $interval( function(){ 
                        $('.resend_otp').attr('disabled','disabled');
                        intervalTime--;
                        $scope.resendText--;
                        if($scope.resendText==0){
                            $interval.cancel($scope.timer);
                            $scope.resendText = Cookies.get('language')=='en'?"Resend otp":"重发验证码";
                            $scope.appendresend = '';
                            $scope.appendresendDot = '';
                            $('.resend_otp').removeAttr('disabled');
                        }
                    }, 1000);
                }else if(response.data.status == 0){
                    if(response.data.code==2){
                        window.location.replace("ignite-login");
                    }
                    $scope.otpSent = false;
                    hideLoader('.send_otp',Cookies.get('language')=='en'?'Send otp':'发验证码');
                    simpleAlert('error', '', response.data.message);
                    $('.disable_input').removeAttr('disabled');
                }            
            });
        }
    }
    $scope.otp = {otp:''};
    $scope.Verifyotp = function(country_code,phone_number,otp){
        if(otp.otp==undefined||otp.otp==''){
            simpleAlert('error', '', Cookies.get('language')=='en'?'Enter OTP':'Enter OTP');
        } else {
            var data = {};
            data.country_code = country_code;
            data.phone_number = phone_number;
            if($scope.email_address!=undefined&&$scope.email_address!='')
                data.email_address = $scope.email_address;
            data.otp = $scope.otp.otp;
            data.language = Cookies.get('language');
            showLoader('.verify_otp');
            $('.disable_otp').attr('disabled','disabled');
            ignitefactory.Verifyuserotp(data).then(function (response) {
                // console.log(response.data); return;
                hideLoader('.verify_otp',Cookies.get('language')=='en'?'Verify otp':'确认验证码');
                if(response.data.status == 1){
                    window.location.replace('ignite-login');
                }else if(response.data.status == 0){
                    $('.disable_otp').removeAttr('disabled');
                    $scope.otp = {otp:''};
                    if(response.data.code==401||response.data.code==0){
                        simpleAlert('error', '', response.data.message);
                    }
                    if(response.data.code==2){
                        window.location.replace("ignite-login");
                    }         
                }            
            });
        }
    }


}]);
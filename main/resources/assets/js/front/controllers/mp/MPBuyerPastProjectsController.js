/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 7th April 2018
 Name    : MPBuyerPastProjectsController
 Purpose : All the functions for MP browse designers page
 */
angular.module('sixcloudApp').controller('MPBuyerPastProjectsController', ['$sce', 'mpfactory', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, mpfactory, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {
    console.log('MPBuyerPastProjects Controller loaded');
    $('body').css('background','#e3eefd');
    $timeout(function(){
        if($rootScope.loginUserData.status == 0){
            window.location.replace("sixteen-login");
        }
        $('.buyer-dashboard-menu').find('li').removeClass('active');
        $('#past_projects').addClass('active');
    },500);

    getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
    $('#chk_lang').change(function() {
        getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
    });

    var rating_array = ['quality_rating','creativity_rating','buyers_requirement_rating','timeliness_rating','responsiveness_rating'];
    var rating_array_value = [];
    for(var i=0;i<rating_array.length;i++){
        $('.kv-gly-star_'+rating_array[i]).on('change', function () {
            rating_array_value.push({['key']:$(this).attr('id'),
              ['value']: $(this).val(),
            })
        });
        $('.kv-gly-star_'+rating_array[i]).rating({
            containerClass: 'is-star',
            showCaption: false
        });
    }
    
    $scope.endto = $scope.startfrom + 1;
    $scope.currentPage = $scope.currentPaginate = 1;
    $scope.range = $scope.rangepage = $scope.rangePaging = [];
    $scope.Projects = function(pageNumber){
        mpfactory.Buyerprojects({'whichPage':'buyer-past-project'},pageNumber).then(function(response) {
            if(response.data.code == 2)
                window.location.replace("sixteen-login");
            if (response.data.status == 1)
            {
                $scope.buyerProjects = response.data.data.data;
                $scope.total_projects = response.data.data.total;
                if($scope.total_projects==0){
                    if(getLanguage=='en'){
                        $("#no_data_msg").text("There are no past projects available");    
                    }else{
                        $("#no_data_msg").text("没有过去的项目可用");    
                    }
                    
                }
                $scope.pagination = response.data.data;
                $scope.currentActivePaging = response.data.data.current_page;
                $scope.rangeActivepage = _.range(1, response.data.data.last_page + 1);
                } else {
            }
        });
    }
    $scope.Projects(1);

    $scope.Mutualcancelprojectlisting = function(pageNumber) {
        mpfactory.Mutualcancelprojectlisting(pageNumber).then(function(response) {
            $scope.cancelledprojects = response.data.data.data;
            $scope.total_cancel_project = response.data.data.total;
            $scope.paginatePastCancel = response.data.data;
            $scope.currentPastCancelPaging = response.data.data.current_page;
            $scope.rangePastCancelpage = _.range(1, response.data.data.last_page + 1);
        });
    };
    $scope.Mutualcancelprojectlisting(1);

    $scope.Openreviewmodal = function(project){
        $(".kv-gly-star").rating("clear");
        $scope.showError = false;
        $scope.reviewForProject = project;
        $('#reviewModal').modal('show');
    }
    $scope.quality_rating_error            = false;
    $scope.creativity_rating_error         = false;
    $scope.buyers_requirement_rating_error = false;
    $scope.timeliness_rating_error         = false;
    $scope.responsiveness_rating_error     = false;
    $scope.review_error                    = false;
    $scope.review_placeHolder              = undefined;

    $scope.Doreviewrating = function(){
         if($scope.quality_rating != undefined && $scope.creativity_rating != undefined && $scope.buyers_requirement_rating != undefined && $scope.timeliness_rating != undefined && $scope.responsiveness_rating != undefined && $scope.review != undefined){
            $(".save-rating-review").text(' ');
            $(".save-rating-review").append("<i class='fa fa-spinner fa-spin'></i> Please wait");
            $(".save-rating-review").attr("disabled", "disabled");
            mpfactory.Adddesignerreview({'rating':rating_array_value,'review':$scope.review,'whichProject':$scope.reviewForProject}).then(function(response) {
                if (response.data.status == 1)
                {
                    $scope.Projects(1);
                    $('#reviewModal').modal('hide');
                    $(".save-rating-review").text(' ');
                    $(".save-rating-review").append("Save");
                    $(".save-rating-review").removeAttr("disabled");
                    if(getLanguage=='en'){
                        simpleAlert('success','',response.data.message);    
                    }else{
                        simpleAlert('success','',response.data.message);
                    }    
                    $scope.review = undefined;
                    $scope.rating = '';
                    $scope.reviewForProject = undefined;
                    $scope.showError = false;
                }
                else {
                }
            });
         } else {
            if($scope.quality_rating == undefined){
                $scope.quality_rating_error = true;
            }
            if($scope.creativity_rating == undefined){
                $scope.creativity_rating_error = true;  
            }
            if($scope.buyers_requirement_rating == undefined){
                $scope.buyers_requirement_rating_error = true;
            }
            if($scope.timeliness_rating == undefined){
                $scope.timeliness_rating_error = true;
            }
            if($scope.responsiveness_rating == undefined){
                $scope.responsiveness_rating_error = true;
            }
            if($scope.review == undefined){
                $scope.review_error = true;
                $scope.review_placeHolder = "Write review here";
            }
        }
    }
    $scope.returnRatingArray = function(count){
        var ratings = []; 

        for (var i = 0; i < count; i++) { 
            ratings.push(i) 
        } 

        return ratings;
    }
    $scope.strLimit = 100;
    // Event trigger on click of the Show more button.
    $scope.showMore = function(getReview) {
        $scope.strLimit = getReview.length;
    };

    // Event trigger on click on the Show less button.
    $scope.showLess = function() {
        $scope.strLimit = 100;
    };
}]);
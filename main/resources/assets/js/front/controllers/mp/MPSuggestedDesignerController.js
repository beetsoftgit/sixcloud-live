/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 29th March 2018
 Name    : MPSuggestedDesignerController
 Purpose : All the functions for MP suggested designer page
 */
angular.module('sixcloudApp').controller('MPSuggestedDesignerController', ['$sce', 'mpfactory', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, mpfactory, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {
    console.log('MPSuggestedDesigner controller loaded');

    $('body').css('background','#e3eefd');
    /*if($rootScope.loginUserData.status == 0){
        window.location.replace("mp-login");
    }*/
    $scope.current_domain = current_domain;
    $scope.sliderfunction = function(){
        // alert();
        $timeout(function(){
            var swiper = new Swiper('.browse-designers-swiper', {
                slidesPerView: 1,
                spaceBetween: 0,
                loop: true,
                // observer: true,
                 navigation: {
                   nextEl: '.browse-designers-button-next',
                   prevEl: '.browse-designers-button-prev',
                 },
            });
        }, 1000);
    }
    getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
    
    $('#chk_lang').change(function() {
        getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
    });
    $scope.sliderfunction();
    $scope.mp_project_id = $routeParams.slug;
    $scope.assigned_designer_id = $routeParams.assigned_designer_id;

    mpfactory.Designersuggestions({'mp_project_id':$scope.mp_project_id, 'assigned_designer_id':$scope.assigned_designer_id}).then(function(response) {
        if (response.data.status == 1)
        {
          $scope.designers = response.data.data.suggestedDesigners;
          $scope.project = response.data.data.projectInfo;
          $scope.mp_project_id = response.data.message;
        }
        else {
        }
    });

    $scope.Browsesellersuggestion = function(mp_project_id,assigned_designer_id){
    	if(assigned_designer_id != undefined){
        window.location = "browse-sellers/"+mp_project_id+"/"+assigned_designer_id;
    	} else {
        window.location = "browse-sellers/"+mp_project_id;
    	}
    }

    $(function(){
       $('.search-btn').click(function(){
            $('#searchDesigner').tooltip('show');
		    $timeout(function(){
		      $('#searchDesigner').tooltip('hide');
		    },5000);
       });
    });

    $scope.Asssigndesigner = function(designer,projectInfo){
        showLoader(".select_"+designer.id);
		    mpfactory.Asssigndesigner({'mp_project_id':$scope.mp_project_id, 'designer':designer, 'projectInfo':projectInfo}).then(function(response) {
          if(getLanguage=='en'){
              text                  = 'Select';
              var confirmButtonText = 'Ok';
              var text              = "Success";
            }else{
              text                  = '选择';
              var confirmButtonText = '好。';
              var text              = "成功";
            }
	        hideLoader(".select_"+designer.id,text);
          if (response.data.status == 1)
	        {
    	      simpleAlert('success',text,response.data.message,true);
    	      swal({
    					title: text,
    				  	text:                response.data.message,
    				  	type:                "success",
    				  	showCancelButton:    false,
    				  	closeOnConfirm:      true,
    				  	showLoaderOnConfirm: true,
    				  	confirmButtonText:   confirmButtonText,
    				  	confirmButtonColor:  '#8a75d0',
    				}, function () {
                /* Normal flow : original */
                /*window.location.replace("buyer-dashboard");*/
                /* Static flow : demo */
    			    	window.location.replace("seller-profile/"+designer.slug+"/"+projectInfo.slug+"");
    				});
	        }
	        else {
            simpleAlert('info','',response.data.message);
	        }
	    });    	
    }
    $scope.min_timeline = 3;
    $scope.min_budget = 30;
    $(document).ready(function(){
          $("#project_budget").on("keypress keyup blur",function (event) {
            //this.value = this.value.replace(/[^0-9\.]/g,'');
            $(this).val($(this).val().replace(/[^0-9\.]/g,''));
              if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
              }
              $scope.project_budget_error = false;
          });
          $("#project_timeline").on("keypress keyup blur",function (event) {    
             $(this).val($(this).val().replace(/[^\d].+/, ""));
              if ((event.which < 48 || event.which > 57)) {
                  event.preventDefault();
              }
              $scope.project_timeline_error = false;
          });
      });
    $scope.searchbyname = function(){
    	if($scope.searchDesigner.length>3){
	    	mpfactory.Searchdesigner({'name':$scope.searchDesigner}).then(function(response) {
		        if (response.data.status == 1) {
		          $scope.designerSearch = response.data.data;
		        } else { }
		    });
    	} else {
    		$(".suggested_designer_list").remove();
    	}
    }

    $scope.Updatebudgettimeline = function(data,updateWhat,mp_project_id,whichButton){
    	showLoader('.'+whichButton);
  //   	swal({
		// 	title: "Update "+updateWhat,
		//   	text: "Are you sure you want to update "+updateWhat+" ?\nIf you select any designer after this, you agree the project in updated "+updateWhat,
		//   	type: "info",
		//   	showCancelButton: true,
		//   	closeOnConfirm: true,
		//   	showLoaderOnConfirm: true,
		//   	confirmButtonColor: '#8a75d0',
		//   	confirmButtonText: "Yes, Update",
		// }, function (isConfirm) {
		// 	if(!isConfirm){
		// 		hideLoader('.'+whichButton,'Update');
		// 	} else {
		    	if(updateWhat == 'budget'){
		    		var data1 = {};
		    		data1['project_budget'] = data;
		    		data1['updateWhat'] = 'project_budget';
		    		data1['mp_project_id'] = mp_project_id;
		    		if($scope.assigned_designer_id != undefined || $scope.assigned_designer_id != '')
		    			data1['assigned_designer_id'] = $scope.assigned_designer_id;
		    	}
		    	if(updateWhat == 'timeline'){
		    		var data1 = {};
		    		data1['project_timeline'] = data;
		    		data1['updateWhat'] = 'project_timeline';
		    		data1['mp_project_id'] = mp_project_id;
		    		if($scope.assigned_designer_id != undefined || $scope.assigned_designer_id != '')
		    			data1['assigned_designer_id'] = $scope.assigned_designer_id;
		    	}
		    	mpfactory.Updatebudgettimeline(data1).then(function(response) {
            if(getLanguage=='en'){
              text='Update';
            }else{
              text='更新';
            }
		    		hideLoader('.'+whichButton,text);
			        if (response.data.status == 1)
			        {
			        	$scope.sliderfunction();
			          	$scope.designers = response.data.data.suggestedDesigners;
			          	$scope.project = response.data.data.projectInfo;
			          	$scope.mp_project_id = response.data.message;
			        }
			        else {
			        }
			    });
		// 	}
		// });
    }
}]);


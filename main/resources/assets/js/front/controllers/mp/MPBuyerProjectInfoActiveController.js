/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 9th April 2018
 Name    : MPBuyerProjectInfoActiveController
 Purpose : All the functions for MP buyer active project pages
 */
angular.module('sixcloudApp').controller('MPBuyerProjectInfoActiveController', ['$sce', 'mpfactory', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, mpfactory, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {    
    console.log('MPBuyerProjectsInfo Controller loaded');
    $('body').css('background', '#e3eefd');
    $timeout(function() {
        if ($rootScope.loginUserData.status == 0) {
            window.location.replace("sixteen-login");
        }
        $('.buyer-dashboard-menu').find('li').removeClass('active');
        $('#dashboard').addClass('active');
    }, 500);
    $('.ProjectInfoBtn').click(function() {
        $('.projectInfoText').slideToggle();
    });
    getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
    if(getLanguage=='en'){
        $scope.placeholder_messgae = "Type Here";    
    }else{
        $scope.placeholder_messgae = "在此输入";
    }
    changeLanguage(getLanguage);
    $('#chk_lang').change(function() {
        getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
        changeLanguage(getLanguage);
        if(getLanguage=='en'){
            $scope.placeholder_messgae = "Type Here";    
        }else{
            $scope.placeholder_messgae = "在此输入";
        }
    });

    function changeLanguage(lang) {
        if (lang == "chi") {
            $timeout(function(){
                $scope.chinese = true;
                $scope.english = false;
                $scope.language='chi';
            },500);
        } else {
            $timeout(function(){
                $scope.chinese = false;
                $scope.english = true;
                $scope.language='en';
            },500);
        }
    }
    $scope.limit = 2;
    $scope.scrollEventCallback = function() {
        if (($(window).scrollTop() + 200) >= $(document).height() - $(window).height() - 200) {
            $scope.limit = $scope.limit + 2;
        }
    }
    $scope.mp_project_id = $routeParams.slug;
    $scope.Projectfunction = function() {
        mpfactory.Attentiongained({'slug': $scope.mp_project_id}).then(function(response) {
            if (response.data.code == 2) window.location.replace("sixteen-login");
            if (response.data.status == 1) {} else {}
        });
        mpfactory.Designerprojectinfo({'slug': $scope.mp_project_id}).then(function(response) {
            if (response.data.code == 2) window.location.replace("sixteen-login");
            if (response.data.status == 1) {
                $scope.projectInfo = response.data.data;
                console.log($scope.projectInfo.message_board);
                $scope.skill_array=[];
                $scope.skill_array_id=[];
                angular.forEach($scope.projectInfo.skills, function (skill, index) {
                    if (skill.skill_detail.status==1) {
                        $scope.skill_array.push(skill);
                        $scope.skill_array_id.push(skill.skill_set_id);
                    }
                });

                $scope.messages_total = response.data.data.message_board.length;
                $scope.Getunreadnotificationscount();
            } else {}
        });
        mpfactory.Projectinfo({'mp_project_id': $scope.mp_project_id,'language':getLanguage}).then(function(response) {
            $scope.detailProject = response.data.data;
        });

        mpfactory.Readmessages({
            'slug': $scope.mp_project_id
        }).then(function(response) {
            if (response.data.status == 1) {
            } else {
                // simpleAlert('error', 'Signup', response.data.message, true);
            }
        });
        $scope.Getunreadnotificationscount=function(){
            mpfactory.Getunreadnotificationscount().then(function(response) { 
                if (response.data.status === 0) {
                    return false;
                }
                $("#notification_number").text(response.data.data.totalCount);
            });    
        }
        

        /*mpfactory.Readnotifications({
            'slug': $scope.mp_project_id
        }).then(function(response) {
            if (response.data.status == 1) {
                
            } else {
                // simpleAlert('error', 'Signup', response.data.message, true);
            }
        });*/
    }
    $scope.Projectfunction();

$scope.current_domain = current_domain;
    $scope.paymentStatusChange=function(id,amount,status,loaderButton,disableButton,buttonText){
        
        $scope.id=id;
        $scope.status=status;
        $scope.amount=amount;
        showLoader('.'+loaderButton);
        Disable('.'+disableButton);
        mpfactory.paymentStatusChange({'id':id,'amount':amount,'status':status,'language':$scope.language}).then(function(response) {
            if($scope.language=='chi'){
                if(buttonText=='Accept'){
                    buttonText='接受';
                }else{
                    buttonText='拒绝';
                }
            }
            hideLoader('.'+loaderButton,buttonText);
            Enable('.'+disableButton);
            if (response.data.status == 1)
            {
                $scope.Projectfunction();
                if($scope.language=='en'){
                    simpleAlert('success', 'Success', response.data.message);    
                }else{
                    simpleAlert('成功', '成功', response.data.message);    
                }
                
                $("#accept").hide();
                $("#reject").hide();
            }
        });   
    }
    $scope.addReason=function(){
        mpfactory.addReason({'reason':$scope.reason,'id':$scope.id,'status':$scope.status,'amount':$scope.amount,'language':$scope.language}).then(function(response) {
            $('#reason_modal').modal('hide');
            if (response.data.status == 1)
            {
                if($scope.language=='en'){
                    simpleAlert('error', '', response.data.message);    
                }else{
                    simpleAlert('错误', '', response.data.message);    
                }
               $("#accept").hide();
               $("#reject").hide();
            }
        });   
    }

    var fi = $('#fileupload'); //file input 
    var process_url = 'Uploadmessagefile'; //PHP script
    var progressBar = $('<div/>').addClass('progress').append($('<div/>').addClass('progress-bar')); //progress bar
    uploadButton = $('<a/>').prop('href', 'javascript:;'); //upload button
    uploadButton.prop('id', "testid");
    uploadButton.append('<i class="fa fa-check-circle" aria-hidden="true"></i>');
   
    $timeout(function() {
        uploadButton.on('click', function(uploadButton) {
            var $this = $(uploadButton),
                data = $this.data();
            data.submit().always(function() {
                $this.parent().find('.progress').show();
                $this.parent().find('.remove').remove();
                $this.remove();
            });
        });
    }, 500);
    
    //initialize blueimp fileupload plugin
    fi.fileupload({
        url: process_url,
        dataType: 'json',
        autoUpload: false,
        acceptFileTypes: /(\.|\/)(pdf|doc|docx|ppt|psd|svg|ico|eps|icns|ai|png|jpeg|jpg|xlsx|PDF|DOC|DOCX|PPT|PSD|SVG|ICO|EPS|ICNS|AI|PNG|JPEG|JPG|XLSX'|gif)$/i,
        maxFileSize: 30000000, //1MB
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),
        previewMaxWidth: 50,
        previewMaxHeight: 50,
        previewCrop: true,
        dropZone: $('#dropzone')
    });
    $scope.orignionalFileName = [];
    var allowedFormats = ['pdf', 'doc', 'docx', 'ppt', 'psd', 'svg', 'ico', 'eps', 'icns', 'ai', 'png', 'jpeg', 'jpg', 'xlsx', 'PDF', 'DOC', 'DOCX', 'PPT', 'PSD', 'SVG', 'ICO', 'EPS', 'ICNS', 'AI', 'PNG', 'JPEG', 'JPG', 'XLSX'];
    fi.on('fileuploadadd', function(e, data) {
        var fileName = data.files[0].name;
        var ext = fileName.split('.').pop();
        if (jQuery.inArray(ext, allowedFormats) !== -1) {
            data.context = $('<div/>').addClass('file-progress-bar').appendTo('.outer-progress-bar-div');
            $.each(data.files, function(index, file) {
                $scope.fileNameUploaded = file.name;
                if ($scope.orignionalFileName.indexOf(file.name) !== -1) {
                    //simpleAlert('Error', 'Oops', 'File already added', true);
                    if($scope.language=='en'){
                        simpleAlert('Error', 'Oops', 'File already added', true);
                    }else{
                        simpleAlert('错误', '哎呀', '文件已添加', true);
                    }
                    data.context.remove();
                    return false;
                } else {
                    var node = $('<div/>').addClass('file-extension');
                    // var h4 = $('<h4/>').addClass('uploaded_file');
                    var node1 = $('<div/>').addClass('H');
                    var removeBtn = $('<a/>').prop('href', 'javascript:;'); //upload button
                    // var removeBtn = $('<div/>').addClass('close-btn'); //
                    // .text('Remove');
                    removeBtn.append('<div class="close-btn"><strong></strong> <a href="javascript:;"><i class="fa fa-times-circle-o" aria-hidden="true"></i></a></div>');
                    // var removeBtn = $('<button/>').addClass('button btn-red remove').text('Remove');
                    removeBtn.on('click', function(e, data) {
                        var file_name = $(this).parent('.H').siblings('.realFileName').text();
                        mpfactory.Unlinkmsgbrdremovedfile({
                            'file_name': file_name
                        }).then(function(response) {
                            if (response.data.status == 1) {
                                var index = $scope.orignionalFileName.indexOf(file_name);
                                var indexfinal = $scope.finalFiles.indexOf(file_name);
                                $scope.orignionalFileName.splice(index, 1);
                                $scope.finalFiles.splice(indexfinal, 1);
                                //simpleAlert('success', 'Removed', '');
                                if($scope.language=='en'){
                                    simpleAlert('Error', 'Removed', true);
                                }else{
                                    simpleAlert('错误', '删除', '', true);
                                }
                            } else {}
                        });
                        $(this).parent().parent().remove();
                    });
                    var file_txt = node.append('<span></span>');
                    // var file_txt = node.append('<span>' + file.name + '</span>');
                    var file_size = $('</div>');
                    // file_size.append('<h4 class="file_name">' + file.name + '<div class="close-btn"><strong></strong> <a href="javascript:;"><i class="fa fa-times-circle-o" aria-hidden="true"></i></a></div>' + '</h4>');
                    // nameformat_size(file.size)
                    node.append(file_txt);
                    // node1.appendTo(node);
                    node1.append(removeBtn);
                    // h4.append(file.name);
                    // h4.append(node1);
                    progressBar.clone().appendTo(data.context);
                    if (!index) {
                        node.prepend(file.preview);
                    }
                    node.appendTo(data.context);
                    data.context.append('<h4 class="uploaded_file">' + file.name + ' <div class="close-btn"></h4><strong class="realFileName hide"></strong><span>' + file.size + '</span></div>');
                    file_txt.prependTo(data.context).append(uploadButton.clone(true).data(data));
                    node1.appendTo(data.context);
                }
            });
        } else {
            //simpleAlert("info", "Info", "File not allowed. Allowed file types \n\n (pdf, doc, docx, ppt, psd, svg, ico, eps, icns, ai, png, jpeg, jpg, xlsx)");
            if($scope.language=='en'){
                simpleAlert("info", "Info", "File not allowed. Allowed file types \n\n (pdf, doc, docx, ppt, psd, svg, ico, eps, icns, ai, png, jpeg, jpg, xlsx)");
            }else{
                simpleAlert('信息', '信息', "文件不被允许。允许的文件类型  \n\n (pdf, doc, docx, ppt, psd, svg, ico, eps, icns, ai, png, jpeg, jpg, xlsx)");    
            }
            return;
        }
    });
    fi.on('fileuploadprocessalways', function(e, data) {
        var fileName = data.files[0].name;
        var ext = fileName.split('.').pop();
        if (data.files[0].size > MAX_SIZE) {
            if($scope.language=='en'){
                simpleAlert('Error', 'File is too big.', '', true);
            }else{
                simpleAlert('错误', '文件太大了', '', true);
            }
            $('.file-progress-bar').last().remove();
            return false;
        }
        if (jQuery.inArray(ext, allowedFormats) !== -1) {
            var index = data.index,
                file = data.files[index],
                node = $(data.context.children()[index]);
            if (file.preview) {
                node.prepend(file.preview);
            }
            if (file.error) {
                node.append($('<span class="text-danger"/>').text(file.error));
            }
            if (index + 1 === data.files.length) {
                if (!!data.files.error) data.context.find('button.upload').prop('disabled');
                // data.context.find('button.upload').prop('disabled', !!data.files.error);
            }
            //uploadButton.click(uploadButton);
            /*var $this = $(uploadButton),
            data = $this.data();*/
            if ($scope.orignionalFileName.indexOf($scope.fileNameUploaded) !== -1) {
                return;
            } else {
                data.submit().always(function(response) {
                    $scope.orignionalFileName.push($scope.fileNameUploaded);
                    var getFileName = $(".uploaded_file:contains('" + file.name + "')"); //$('.uploaded_file').hasText();
                    getFileName.siblings('strong').text(response.data.message);
                });
            }
        } else {
            if($scope.language=='en'){
                simpleAlert("info", "Info", "File not allowed. Allowed file types \n\n (pdf, doc, docx, ppt, psd, svg, ico, eps, icns, ai, png, jpeg, jpg, xlsx)");
            }else{
                simpleAlert('信息', '信息', "文件不被允许。允许的文件类型  \n\n (pdf, doc, docx, ppt, psd, svg, ico, eps, icns, ai, png, jpeg, jpg, xlsx)");    
            }
            
            return;
        }
    });
    fi.on('fileuploadprogress', function(e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        if (data.context) {
            data.context.each(function() {
                $(this).find('.progress').attr('aria-valuenow', progress).children().first().css('width', progress + '%');
            });
        }
    });
    $scope.finalFiles = [];
    fi.on('fileuploaddone', function(e, data) {
        $scope.finalFiles.push(data.result.data);
        $.each(data.result.files, function(index, file) {
            if (file.url) {
                var link = $('<a>').attr('target', '_blank').prop('href', file.url);
                $(data.context.children()[index]).addClass('file-uploaded');
                $(data.context.children()[index]).find('canvas').wrap(link);
                $(data.context.children()[index]).find('.file-remove').hide();
                var done = $('<span class="text-success"/>').text('Uploaded');
                $(data.context.children()[index]).append(done);
            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index]).append(error);
            }
        });
    });
    fi.on('fileuploadfail', function(e, data) {
        $('#error_output').html(data.jqXHR.responseText);
    });

    function format_size(bytes) {
        if (typeof bytes !== 'number') {
            return '';
        }
        if (bytes >= 1000000000) {
            return (bytes / 1000000000).toFixed(2) + ' GB';
        }
        if (bytes >= 1000000) {
            return (bytes / 1000000).toFixed(2) + ' MB';
        }
        return (bytes / 1000).toFixed(2) + ' KB';
    }
    
    
    $scope.Sendmessage = function(mp_project_id, user_id,email_id_to_send_mail,receiver_id) {
        if($scope.message!=undefined||$scope.finalFiles.length>0){

        showLoader('.send_message','');
        var formData = new FormData($('#message_form')[0]);
        formData.append('files', $scope.finalFiles);
        // alert($scope.message);
        mpfactory.Sendmessageform({
                'user_id': user_id,
                'mp_project_id': mp_project_id,
                'message': $scope.message,
                'files': $scope.finalFiles,
                'user_role': 2,
                'receiver_emailid': email_id_to_send_mail,
                'receiver_id': receiver_id
            }).then(function(response) {
                hideLoader('.send_message', '<i class="fa fa-paper-plane-o"></i>');
                if (response.data.status == 1) {
                    $scope.Projectfunction();
                    $scope.orignionalFileName = [];
                    $scope.finalFiles = [];
                    $scope.message = undefined;
                } else {
                    $scope.orignionalFileName = [];
                    $scope.finalFiles = [];
                    //simpleAlert('error', '', response.data.message);
                    if($scope.language=='en'){
                        simpleAlert('error', 'Error', response.data.message);    
                    }else{
                        simpleAlert('错误', '错误', response.data.message);    
                    }
                }
                $('.outer-progress-bar-div').html('');
            });
        } else {
            if($scope.language=='en'){
                simpleAlert('error','','Type message or select file');    
            }else{
                simpleAlert('错误','','输入消息或选择文件');
            }
            
        }
    }


    $scope.Showmodal = function(whichModal){
        $scope.whatAction = whichModal;
        $scope.modalTitle = whichModal;
        $scope.placeholder = whichModal;
        $scope.reason = undefined;
        $('#Cancel_flag_modal').modal('show');
    }
    $scope.Cancelwithoutseller = function(reason,modalTitle,projectInfo){
        showLoader('cancelwithoutsellerbtn');
        mpfactory.Cancelwithoutseller({'reason':reason,'modalTitle':modalTitle,'mp_project_id':projectInfo,'language':$scope.language}).then(function(response) {
            hideLoader('cancelwithoutsellerbtn');
            if(response.data.code == 2)
                window.location.replace("sixteen-login");
            if (response.data.status == 1)
            {
                window.location.replace("buyer-dashboard");
            }
            else {
                simpleAlert('error','',response.data.message);
            }
        });
    }
    $scope.Actions = function(reason,modalTitle,reported_by_id,reported_for_id,mp_project_id,projectInfo){
        if($scope.language=='en'){
            var waitText          = 'Please wait';
            var loadertext        = 'Submit';
        }else{
            var waitText          = '请稍候';
            var loadertext        = '提交';
        }
        showLoader('.flag_seller',waitText);

        mpfactory.Actions({'mp_project_id':mp_project_id,'reported_by_id':reported_by_id,'reported_for_id':reported_for_id,'reason':reason,'modalTitle':modalTitle,'projectInfo':projectInfo,'language':$scope.language}).then(function(response) {
            hideLoader('.flag_seller',loadertext);
            if (response.data.status == 1)
            {
                if(response.data.message == "Seller Reported" || response.data.message == "卖家报告"){
                    $('#Cancel_flag_modal').modal('hide');
                    $scope.reason = undefined;
                    //simpleAlert('success','Success',response.data.message);
                    if($scope.language=='en'){
                        var confirmButtonText = 'Ok';
                        simpleAlert('success', 'Success', response.data.message);    
                    }else{
                        var confirmButtonText = '好。';
                        simpleAlert('成功', '成功', response.data.message);    
                    }
                } else {
                    swal({
                        title: "Success",
                        text: response.data.message,
                        type: "success",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        showLoaderOnConfirm: true,
                        confirmButtonText: confirmButtonText,
                        confirmButtonColor: '#8a75d0',
                    }, function () {
                        window.location = "buyer-dashboard";
                    });
                }
            }
            else {
                //simpleAlert('error','',response.data.message);
                if($scope.language=='en'){
                    simpleAlert('error', 'Error', response.data.message);    
                }else{
                    simpleAlert('错误', '错误', response.data.message);    
                }
            }
        });
    }


    $scope.ShowmodalAceeptReject = function(whichModal, id, status, designer_id, project) {
        $scope.project = project;
        $scope.whatAction = whichModal;
        $scope.modalTitle = whichModal;
        if($scope.language=='en'){
            $scope.placeholder = whichModal + " Reason";    
        }else{
            
            if(whichModal=="Accept Request"){
                $scope.placeholder = "接受请求原因";  
            }else{
                $scope.placeholder = "拒绝请求";  
                $scope.modalTitle = "拒绝请求";
            }
            
        }
        
        $scope.reason = undefined;
        $('#' + id).modal('show');
        if (status == 3) $scope.Openreviewmodal(designer_id);
        $scope.ActionsAcceptReject(status, designer_id);
    }
    $scope.ActionsAcceptReject = function(status, designer_id) {
        mpfactory.Acceptprojectcomplitionrequest({
            'project_id': $scope.mp_project_id,
            'status': status,
            'designer_id': designer_id
        }).then(function(response) {
            $scope.Projectfunction();
            if (response.data.status == 1) {
                $('.markcomplete').hide();
                if (response.data.message == "Seller Reported") {
                    $('#Cancel_flag_modal').modal('hide');
                    $scope.reason = undefined;
                    // simpleAlert('success', 'Success', response.data.message);
                } else {
                    // swal({
                    //     title: "Success",
                    //     text: response.data.message,
                    //     type: "success",
                    //     showCancelButton: false,
                    //     closeOnConfirm: true,
                    //     showLoaderOnConfirm: true,
                    //     confirmButtonText: "Ok",
                    //     confirmButtonColor: '#8a75d0',
                    // }, function() {
                    //     window.location.replace("buyer-dashboard");
                    // });
                }
            } else {
                //simpleAlert('error', '', response.data.message);
                if($scope.language=='en'){
                    simpleAlert('error', 'Error', response.data.message);    
                }else{
                    simpleAlert('错误', '错误', response.data.message);    
                }
            }
        });
    }
    $scope.Extensionacceptreject = function(acceptReject, extensionDetails, whichClass){
        showLoader('.'+whichClass);
        Disable('.Reject');
        Disable('.Accept');
        mpfactory.Acceptrejectextension({'acceptReject':acceptReject,'extensionDetails':extensionDetails,'project':$scope.projectInfo,'language':$scope.language}).then(function(response) {
            if(whichClass=='Accept'){
                text='接受';
            }else{
                text='拒绝';
            }
            hideLoader('.'+whichClass,text);
            Enable('.Reject');
            Enable('.Accept');
            if(response.data.code == 2)
                window.location.replace("sixteen-login");
            if (response.data.status == 1)
            {
                $scope.Projectfunction();
                if($scope.language=='en'){
                    simpleAlert('success', 'Success', response.data.message);    
                }else{
                    simpleAlert('成功', '成功', response.data.message);    
                }
            }
            else {
                if($scope.language=='en'){
                    simpleAlert('error', 'Error', response.data.message);    
                }else{
                    simpleAlert('错误', '错误', response.data.message);    
                }
            }
        });
    }
    $scope.Openreviewmodal = function(project) {
        $(".kv-gly-star").rating("clear");
        $scope.showError = false;
        $scope.reviewForProject = project;
        $('#reviewModal').modal('show');
    }
    var rating_array = ['quality_rating', 'creativity_rating', 'buyers_requirement_rating', 'timeliness_rating', 'responsiveness_rating'];
    var rating_array_value = [];
    for (var i = 0; i < rating_array.length; i++) {
        $('.kv-gly-star_' + rating_array[i]).on('change', function() {
            rating_array_value.push({
                ['key']: $(this).attr('id'),
                ['value']: $(this).val(),
            })
        });
        $('.kv-gly-star_' + rating_array[i]).rating({
            containerClass: 'is-star',
            showCaption: false
        });
    }
    // $('.kv-gly-star').on('change', function() {
    //     $scope.rating = $(this).val();
    // });
    // $('.kv-gly-star').rating({
    //     containerClass: 'is-star'
    // });
    $scope.quality_rating_error            = false;
    $scope.creativity_rating_error         = false;
    $scope.buyers_requirement_rating_error = false;
    $scope.timeliness_rating_error         = false;
    $scope.responsiveness_rating_error     = false;
    $scope.review_error                    = false;
    $scope.review_placeHolder              = undefined;
    
    $scope.Doreviewrating = function() {
        if($scope.quality_rating != undefined && $scope.creativity_rating != undefined && $scope.buyers_requirement_rating != undefined && $scope.timeliness_rating != undefined && $scope.responsiveness_rating != undefined && $scope.review != undefined)
        {
            $(".save-rating-review").text(' ');
            $(".save-rating-review").append("<i class='fa fa-spinner fa-spin'></i> Please wait");
            $(".save-rating-review").attr("disabled", "disabled");
            console.log($scope.rating);
            mpfactory.Adddesignerreview({
                'rating': rating_array_value,
                'review': $scope.review,
                'whichProject': $scope.project,
                'language':$scope.language,
            }).then(function(response) {
                console.log(response.data.status);
                if (response.data.status == 1) {
                    $('#reviewModal').modal('hide');
                    $(".save-rating-review").text(' ');
                    $(".save-rating-review").append("Save");
                    $(".save-rating-review").removeAttr("disabled");
                    //simpleAlert('success', 'Success', response.data.message);
                    if($scope.language=='en'){
                        simpleAlert('success', 'Success', response.data.message);
                    }else{
                        simpleAlert('success', '成功', response.data.message);
                    }
                    $scope.review = undefined;
                    $scope.rating = '';
                    $scope.reviewForProject = undefined;
                    $scope.showError = false;
                } else {}
            });
        } else {
            if($scope.quality_rating == undefined){
                $scope.quality_rating_error = true;
            }
            if($scope.creativity_rating == undefined){
                $scope.creativity_rating_error = true;  
            }
            if($scope.buyers_requirement_rating == undefined){
                $scope.buyers_requirement_rating_error = true;
            }
            if($scope.timeliness_rating == undefined){
                $scope.timeliness_rating_error = true;
            }
            if($scope.responsiveness_rating == undefined){
                $scope.responsiveness_rating_error = true;
            }
            if($scope.review == undefined){
                $scope.review_error = true;
                if($scope.language=='en'){
                    $scope.review_placeHolder = "Write review here";
                }else{
                    $scope.review_placeHolder = "在这里写评论";
                }
            }
        }
        // } else {
        //     $scope.showError = true;
        // }
    }
    $scope.submitReason = function(reason, designer_id) {
        mpfactory.Addprojectcompletionrequestrejectreason({'designer_id': designer_id,'project_id': $scope.mp_project_id,'reason': reason,'language':$scope.language}).then(function(response) {
            if (response.data.status == 1) {
                //simpleAlert('success', 'Reject request', response.data.message, true);
                if($scope.language=='en'){
                    simpleAlert('success', 'Reject request', response.data.message, true);
                }else{
                    simpleAlert('成功', '拒绝请求', response.data.message);    
                }
            } else {}
            $('.close').click();
        });
    }
    $scope.resetForm = function() {
        $('#reasonForm')[0].reset();
    }
    $scope.Changecancelprojectstatus=function(accept_or_reject,mp_project_status,mp_project_detail_status,project_id,designer_id,creator_id,reason){
         
      mpfactory.Changecancelprojectstatus({'accept_or_reject':accept_or_reject,'mp_project_status':mp_project_status,'mp_project_detail_status':mp_project_detail_status,'project_id':project_id,'designer_id':designer_id,'creator_id':creator_id,'reason':reason,'language':$scope.language}).then(function(response) {
            
            if(response.data.status==1){
                if($scope.language=='en'){
                    simpleAlert('success', 'Success', response.data.message);    
                }else{
                    simpleAlert('成功', '成功', response.data.message);    
                }
            }
            /*if(accept_or_reject=='accept'){
                response.data.message="Successfully Accepted";
            }
            else{
                response.data.message="Successfully Rejected";   
            }
            simpleAlert('success', 'Success', response.data.message);*/
              $("#accept").hide();
              $("#reject").hide();
              $('#cancel_request_reason_modal').modal('hide');
        });  
    }

    $scope.getData = function(project_id,designer_id,creator_id){
     
        $scope.project_id=project_id;
        $scope.designer_id=designer_id;
        $scope.creator_id=creator_id;
    }

    $scope.Viewsuggestions = function(mp_project_id,assigned_designer_id){
        if(assigned_designer_id == undefined){
           window.location = "suggested-sellers/"+mp_project_id;
        }
        else{
            window.location = "suggested-sellers/"+mp_project_id+"/"+assigned_designer_id;
        }
    }

    $scope.Showprojectextra = function(){
        mpfactory.Getprojectextras({'skills':$scope.skill_array_id,'mp_project_id': $routeParams.slug,'seller_id':$scope.projectInfo.selected_designer.assigned_designer_id}).then(function(response) {
            if (response.data.status == 1) {
                $scope.projectExtras = response.data.data;
                $('#project_extra_section').removeClass('hide');
            } else {}
        });
    }
    $scope.Hideprojectextra = function(){
        $scope.projectExtras = undefined;
        $scope.detailData = undefined;
        $scope.cartTotal = 0;
        $('#project_extra_section').addClass('hide');
    }
    
    $scope.detailData = undefined;
    $scope.cartTotal = 0;
    $scope.Addprojectextra = function(){
        var projectExtra = new FormData($("#projectextraform")[0]);
        if(getLanguage=='en'){showLoader('.add_to_cart_button','Please wait');}else{showLoader('.add_to_cart_button','请稍候');}
        mpfactory.Addprojectextra(projectExtra).then(function(response) {
            if(getLanguage=='en'){hideLoader('.add_to_cart_button','Add to Cart');}else{hideLoader('.add_to_cart_button','添加到购物车');}
            if (response.data.code == 2) window.location.replace("sixteen-login");
            if (response.data.status == 1) {
                $scope.detailData = response.data.data.detailData;
                $scope.cartTotal = response.data.data.cartTotal;
            } else {
                if(getLanguage=='en'){hideLoader('.add_to_cart_button','Add to Cart');}else{hideLoader('.add_to_cart_button','添加到购物车');}
            }
        });
    }

    $scope.Makeextrapayment = function(){
        if(getLanguage=='en'){showLoader('.make_payment','Please wait');}else{showLoader('.make_payment','请稍候');}
        mpfactory.Makeextrapayment({'cart':$scope.detailData,'mp_project_id':$scope.mp_project_id,'cartTotal':$scope.cartTotal}).then(function(response) {
            if(getLanguage=='en'){hideLoader('.make_payment','Make payment');}else{hideLoader('.make_payment','付款');}
            if (response.data.code == 2) window.location.replace("sixteen-login");
            if (response.data.status == 1) {
                $scope.project_info = response.data.data;
                console.log($scope.cartTotal);
                $timeout(function(){
                    $('#create_and_pay').click();
                },500);
            } else {
                if(getLanguage=='en'){hideLoader('.make_payment','Make payment');}else{hideLoader('.make_payment','付款');}
            }
        });   
    }

    $scope.Escrowpaymentdata = function(){
        var escrowPaymentData = new FormData($("#escrow_payment_data")[0]);
        if(getLanguage=='en'){
            showLoader('.escrow_payment_data','Please wait');
        }else{
            showLoader('.escrow_payment_data','请稍候');
        }
        mpfactory.Escrowpaymentdata(escrowPaymentData).then(function(response) {

            if(getLanguage=='en'){hideLoader('.escrow_payment_data','Add to Cart');}else{hideLoader('.escrow_payment_data','添加到购物车');}
            if (response.data.code == 2) window.location.replace("sixteen-login");
            if (response.data.status == 1 && response.data.data.slug != '') {
                if(response.data.data.credit_status==1){
                    window.location = 'make-payment/'+response.data.data.slug+response.data.data.credit_status;        
                }
                else{
                    window.location = 'make-payment/'+response.data.data.slug;        
                }
                
            } else {
                if(getLanguage=='en'){hideLoader('.escrow_payment_data','Make Payment');}else{hideLoader('.escrow_payment_data','添加到购物车');}
            }
        });
    }
}]);
/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 3rd April 2018
 Name    : MPBuyerDashboardController
 Purpose : All the functions for MP buyer dashboard
 */
angular.module('sixcloudApp').controller('MPBuyerDashboardController', ['$sce', 'mpfactory', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, mpfactory, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {
    console.log('MPBuyerDashboard controller loaded');
    $('body').css('background','#e3eefd');

    $timeout(function(){
        if($rootScope.loginUserData.status == 0){
            window.location.replace("sixteen-login");
        }
        $('.buyer-dashboard-menu').find('li').removeClass('active');
        $('#dashboard').addClass('active');
    },1000);

    getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
    
    $('#chk_lang').change(function() {
        getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
    
    });
    $scope.endto = $scope.startfrom + 1;
    $scope.currentPage = $scope.currentPaginate = 1;
    $scope.range = $scope.rangepage = $scope.rangePaging = [];
    $scope.Projects = function(pageNumber){
        mpfactory.Buyerprojects({'whichPage':'buyer-dashboard','language':getLanguage},pageNumber).then(function(response) {
            if(response.data.code == 2)
                window.location.replace("sixteen-login");           
            if (response.data.status == 1)
            {
              $scope.buyerProjects = response.data.data.data;
              $scope.total_projects = response.data.data.total;
              $scope.pagination = response.data.data;
              $scope.currentActivePaging = response.data.data.current_page;
              $scope.rangeActivepage = _.range(1, response.data.data.last_page + 1);
            }
            else {
            }
        });
    }
    $scope.Projects(1);
    $scope.current_domain = current_domain;
   
    $scope.cancelledprojects = function(pageNumber) {
        mpfactory.Buyercancelledprojects(pageNumber).then(function(response) {
            $scope.cancelledprojects = response.data.data.data;
            $scope.total_cancel_projects = response.data.data.total;
            $scope.paginateCancel = response.data.data;
            $scope.currentCancelPaging = response.data.data.current_page;
            $scope.rangeCancelpage = _.range(1, response.data.data.last_page + 1);
        });
    };
    $scope.cancelledprojects(1);

    $scope.Disputedprojectlisting = function(pageNumber) {
        mpfactory.Disputedprojectlisting(pageNumber).then(function(response) {
            $scope.disputedprojects = response.data.data.data;
            $scope.total_dispute_projects = response.data.data.total;
            $scope.paginateDispute = response.data.data;
            $scope.currentDisputePaging = response.data.data.current_page;
            $scope.rangeDisputepage = _.range(1, response.data.data.last_page + 1);
        });
    };
    $scope.Disputedprojectlisting(1);

    $scope.Changecancelprojectstatus=function(accept_or_reject,mp_project_status,mp_project_detail_status,project_id,designer_id,creator_id,reason){

        if(getLanguage=='en'){
            var text              = "Success";
        }else{
            var text              = "成功";
        }
      mpfactory.Changecancelprojectstatus({'accept_or_reject':accept_or_reject,'mp_project_status':mp_project_status,'mp_project_detail_status':mp_project_detail_status,'project_id':project_id,'designer_id':designer_id,'creator_id':creator_id,'reason':reason}).then(function(response) {
            
            if(accept_or_reject=='accept'){
                response.data.message="Successfully Accepted";
            }
            else{
                response.data.message="Successfully Rejected";   
            }
            simpleAlert('success', text, response.data.message);
              $("#accept").hide();
              $("#reject").hide();
              $('#cancel_request_reason_modal').modal('hide');
        });  
    }

    $scope.Viewsuggestions = function(mp_project_id,assigned_designer_id){

        if(assigned_designer_id == undefined)
    	   window.location = "suggested-sellers/"+mp_project_id;
        else
            window.location = "suggested-sellers/"+mp_project_id+"/"+assigned_designer_id;
    }

    $scope.Processescrow = function(){
        mpfactory.Processescrow().then(function(response) {
        });
    }
    
    $scope.getData = function(project_id,designer_id,creator_id){
        $scope.project_id=project_id;
        $scope.designer_id=designer_id;
        $scope.creator_id=creator_id;
    }
}]);
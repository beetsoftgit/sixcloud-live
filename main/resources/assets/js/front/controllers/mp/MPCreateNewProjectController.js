/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 28th March 2018
 Name    : MPCreateNewProjectController
 Purpose : All the functions for MP create new project page
 */
angular.module('sixcloudApp').controller('MPCreateNewProjectController', ['$sce', 'Getskills', 'mpfactory', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, Getskills, mpfactory, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {
    console.log('MPCreateNewProject controller loaded');
    $('.buyer-dashboard-menu').find('li').removeClass('active');
    initSample();
    $timeout(function() {
        if ($rootScope.loginUserData.status == 0) {
            window.location.replace("sixteen-login");
        }
    }, 500);
    (function() {
        $('.FlowupLabels').FlowupLabels({
            feature_onInitLoad: true,
            class_focused: 'focused',
            class_populated: 'populated'
        });
    })();
    $('body').css('background', '#e3eefd');
    getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
    changeLanguage(getLanguage);
    $('#chk_lang').change(function() {
        getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
        changeLanguage(getLanguage);
    });
    
  $scope.current_domain = current_domain;
 
    function changeLanguage(lang) {
        if (lang == "chi") {
            $scope.chinese = true;
            $scope.english = false;
        } else {
            $scope.chinese = false;
            $scope.english = true;
        }
    }
    
    /* static demo senil : start */
    $scope.mp_project_id    = $routeParams.slug;
    $scope.whatAction       = $routeParams.whatAction;
    $scope.cartId           = $routeParams.cartId;
    $scope.selectedDesigner = $routeParams.selectedDesigner;
    if($scope.selectedDesigner==undefined){
        $scope.flow = 'reverse';
    } else {
        $scope.flow = 'forward';
    }


    if($routeParams.slug!=undefined&&$routeParams.whatAction!=undefined){
        if($scope.mp_project_id!=undefined && $scope.whatAction == 'project-created'){
            $scope.keepDisabled = true;
            mpfactory.Getprojectinfo({'slug':$scope.mp_project_id}).then(function(response) {
                if (response.data.code == 2) window.location.replace("sixteen-login");
                if (response.data.status == 1) {
                    $scope.project_info = response.data.data;
                    $scope.project_title = $scope.project_info.project_title;
                    $scope.project_budget = $scope.project_info.project_budget;
                    $scope.project_timeline = $scope.project_info.project_timeline;
                    $scope.currentDomain = response.data.current_domain;
                    console.log($scope.currentDomain);
                    $timeout(function(){
                      CKEDITOR.instances.editor.setData($scope.project_info.project_description);
                    },500);
                    $scope.skills = $scope.project_info.skills_set;
                } else {
                    simpleAlert('error', '', response.data.message);
                }
            });
        } else {

            $scope.keepDisabled = false;

        }
    } else {
        $scope.whatAction = 'new-project';
    }
    $scope.show_showmore = false;
    if($scope.mp_project_id!=undefined){
        $scope.show_showmore = true;
        $scope.CartServiceFunction = function(){
            mpfactory.Getcartandservices({'cartId':$routeParams.cartId,'selectedDesigner':$routeParams.selectedDesigner,'mp_project_id':$routeParams.slug,'flow':$scope.flow}).then(function(response) {
                if(getLanguage=='en'){
                    var text              = "Success";
                    var confirmButtonText = 'Ok';
                }else{
                    var text              = "成功";
                    var confirmButtonText = '好。';
                }
                if (response.data.code == 2) window.location.replace("sixteen-login");
                if (response.data.status == 1) {
                    $scope.added_tO_cart = response.data.data.added_tO_cart;
                    $scope.totalPrice    = 0;
                    $scope.totalTime = 0;
                    for(var i = 0; i < $scope.added_tO_cart.length; i++){
                        $scope.totalPrice += $scope.added_tO_cart[i].price;
                        $scope.totalTime += $scope.added_tO_cart[i].package_details.delivery_time_in_days;
                    }
                    $scope.remaining_services = response.data.data.remaining_services;
                    $scope.purchsed_services  = response.data.data.purchsed_services;
                    $scope.selected_skills    = response.data.data.selected_skills;
                    $scope.skills             = $scope.selected_skills;
                    $scope.projectSkills      = response.data.data.projectSkill;
                    $scope.project_budget     = $scope.totalPrice;
                    $scope.budget_disable     = true;
                    $scope.project_timeline   = $scope.totalTime;
                    $scope.timeline_disable   = true;
                } else {
                    // simpleAlert('error', '', response.data.message);
                    confirmDialogueAlert(text,response.data.message,'info',false,'#8a75d0',confirmButtonText,'',true,'','',redirectToProfile);
                }
            });
        }
        $scope.CartServiceFunction();
    }

    $scope.showMore = false;
    $scope.showMoreText = 'lbl_add_more_button';
    $scope.Addmoreservice = function(){
        if($scope.showMore == false){
            $scope.showMore = true;
            $scope.showMoreText = 'lbl_add_more_hide';
        } else {
            $scope.showMore = false;
            $scope.showMoreText = 'lbl_add_more_button';
        }
    }

    function deletefunction(){
        mpfactory.Removefromcart($scope.toDeleteService).then(function(response) {
            if(getLanguage=='en'){
                var text              = "Success";
                var confirmButtonText = 'Ok';
            }else{
                var text              = "成功";
                var confirmButtonText = '好。';
            }
            // hideLoader('.create-project','Submit');
            if (response.data.code == 2) window.location.replace("sixteen-login");
            if (response.data.status == 1) {
                $scope.toDeleteService = undefined;
                $scope.CartServiceFunction();
                simpleAlert('success','',response.data.message);
            } else {
                simpleAlert('error','',response.data.message);
                confirmDialogueAlert(text,response.data.message,'info',false,'#8a75d0',confirmButtonText,'',true,'','',redirectToProfile);
                // hideLoader('.create-project','Submit');
            }
        });
    }

    function addfunction(){
        if($scope.flow == 'forward') {
            mpfactory.Addtocart({'service':$scope.toAddService,'mp_project_id':$scope.mp_project_id,'cart_id':$scope.cartId}).then(function(response) {

                if(getLanguage=='en'){
                    var text              = "Success";
                    var confirmButtonText = 'Ok';
                }else{
                    var text              = "成功";
                    var confirmButtonText = '好。';
                }
                // hideLoader('.create-project','Submit');
                if (response.data.code == 2) window.location.replace("sixteen-login");
                if (response.data.status == 1) {
                    $scope.toAddService = undefined;
                    $scope.CartServiceFunction();
                    simpleAlert('success','',response.data.message);
                } else {
                    confirmDialogueAlert(text,response.data.message,'info',false,'#8a75d0',confirmButtonText,'',true,'','',redirectToProfile);
                    // hideLoader('.create-project','Submit');
                }
            });
        } else {
            mpfactory.Addtocart({'service':$scope.toAddService,'cart_id':$scope.cartId}).then(function(response) {

                if(getLanguage=='en'){
                    var text              = "Success";
                    var confirmButtonText = 'Ok';
                }else{
                    var text              = "成功";
                    var confirmButtonText = '好。';
                }
                // hideLoader('.create-project','Submit');
                if (response.data.code == 2) window.location.replace("sixteen-login");
                if (response.data.status == 1) {
                    $scope.toAddService = undefined;
                    $scope.CartServiceFunction();
                    simpleAlert('success','',response.data.message);
                } else {
                    confirmDialogueAlert(text,response.data.message,'info',false,'#8a75d0',confirmButtonText,'',true,'','',redirectToProfile);
                    // hideLoader('.create-project','Submit');
                }
            });
        }
    }

    function redirectToProfile(){
        if($scope.flow == 'reverse') {
            window.location.replace("seller-profile/"+ $scope.mp_project_id +"");
        } else {
            window.location.replace("seller-profile/"+$scope.selectedDesigner+"/"+$scope.mp_project_id+"");
        }
    }

    $scope.Removefromcart = function(service){
        $scope.toDeleteService = service;
        // showLoader('.create-project');
        // confirmDialogue('Are You sure you want to remove '+ service.service_title +'?','Package removed','',deletefunction); 
        confirmDialogue('','Package removed','',deletefunction); 
    }

    $scope.Addtocart = function(service){
        $scope.toAddService = service;
        // showLoader('.create-project');
        confirmDialogueToAdd('Are You sure you want to add '+ service.service_title +'?','Package removed','',addfunction); 
    }
    
    $scope.updatedescriptionandpay = function(){
        $scope.project_description= CKEDITOR.instances.editor.getData();
        showLoader('.create-project');
        mpfactory.Asssigndesigner({'project_description':$scope.project_description,'only_change_description':1,'slug':$routeParams.slug,'user_id':$scope.selectedDesigner,'project_budget':$scope.totalPrice,'project_timeline':$scope.totalTime,'language':getLanguage}).then(function(response) {
            if(getLanguage=='en'){
               var loadertext='Submit';
            }else{
                var loadertext='提交';
            }
            hideLoader('.create-project',loadertext);
            if (response.data.code == 2) window.location.replace("sixteen-login");
            if (response.data.status == 1) {
                if(getLanguage=='en')
                {
                    var text="Success";
                    var confirmButtonText = 'Ok';

                }else{
                    var text="成功";
                    var confirmButtonText = '好。';
                }

                confirmDialogueAlert(text,response.data.message,'success',false,'#8a75d0',confirmButtonText,'',true,'','',redirect);
            } else {
                hideLoader('.create-project',loadertext);
                confirmDialogueAlert("",response.data.message,'info',false,'#8a75d0',confirmButtonText,'',true,'','',redirect);
            }
        });
    }
    function redirect(){
      window.location.replace('buyer-dashboard');
    }
    /* static demo senil : end */
    $scope.multipleInput = 1;
    
    var filesToUpload = [];
    var filesToRemove = [];
    $scope.min_timeline = 3;
    $scope.min_budget = 30;
    $(document).ready(function() {
        $("#project_budget").on("keypress keyup blur", function(event) {
            //this.value = this.value.replace(/[^0-9\.]/g,'');
            $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
            $scope.project_budget_error = false;
        });
        $("#project_timeline").on("keypress keyup blur", function(event) {
            $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
            $scope.project_timeline_error = false;
        });
    });
    $scope.skills = Getskills.data.data;
    $scope.Createnewproject = function(whatAction) {
        $scope.project_description= CKEDITOR.instances.editor.getData();
        if($scope.project_description=='')
        {
            $scope.project_description=undefined;
        }
        if ($scope.project_title == undefined || $scope.project_description == undefined || $scope.project_budget == undefined || $scope.project_timeline == undefined  || $scope.project_timeline < $scope.min_timeline) {
            if ($scope.project_title == undefined) {
                $scope.project_title_error = true;
            }
            if ($scope.project_description == undefined) {
                $scope.project_description_error = true;
            }
            if ($scope.project_budget == undefined) {
                $scope.project_budget_error = true;
            }
            if($scope.project_timeline == undefined){
            $scope.project_timeline_error = true;
            }
            /*if($scope.project_budget < $scope.min_budget){
                $scope.project_budget_error = true;
            }*/
            if($scope.project_timeline < $scope.min_timeline){
                $scope.project_timeline_error = true;
            }
          } else {
            if($scope.project_timeline < $scope.min_timeline){
              /*if($scope.project_budget < $scope.min_budget){
                $('#project_budget').tooltip('show');
                $scope.project_budget_error = true;
                $timeout(function(){
                  $('#project_budget').tooltip('hide');
                },5000);
              }*/
              if($scope.project_timeline < $scope.min_timeline){
                $('#project_timeline').tooltip('show');
                $scope.project_timeline_error = true;
                $timeout(function(){
                  $('#project_timeline').tooltip('hide');
                },5000);
              }
            } else {
                showLoader(".create-project");
                var newProject = new FormData($("#CreateProjectForm")[0]);
                newProject.append("project_description", CKEDITOR.instances.editor.getData());
                newProject.append('remove_attachments', filesToRemove);
                newProject.append('whatAction', whatAction);
                newProject.append('cart_id', $scope.cartId);
                newProject.append('language',getLanguage);

                if(whatAction!='new-project'&&(whatAction=='project-created'||whatAction=='project-pending')){
                    newProject.append('project_budget', $scope.totalPrice);
                    newProject.append('project_timeline', $scope.totalTime);
                } else {
                    newProject.append('project_budget', $scope.project_budget);
                    newProject.append('project_timeline', $scope.project_timeline);
                }
                
                newProject.append('language', getLanguage);
                if(whatAction=='project-pending' && whatAction!='new-project' && whatAction!='project-created'){
                    newProject.append('skills', $scope.projectSkills);
                    newProject.append('assigned_designer_id', $routeParams.slug);
                }
                mpfactory.Createnewproject(newProject).then(function(response) {
                    if(getLanguage=='en'){
                        var loadertext        = 'Submit';
                        var text              = "Success";
                        var confirmButtonText = 'Ok';
                    }else{
                        var loadertext        = '提交';
                        var text              = "成功";
                        var confirmButtonText = '好。';
                    }
                    
                    hideLoader(".create-project",loadertext);
                    if (response.data.code == 2) window.location.replace("sixteen-login");
                    if (response.data.status == 1) {
                        if(whatAction=='new-project'){
                            window.location.replace("suggested-sellers/" + response.data.data);
                        } else {
                            $scope.project_info = response.data.data;
                            confirmDialogueAlert(text,response.data.message,'success',false,'#8a75d0',confirmButtonText,'',true,'','',redirect);
                        }
                        // location.href = BASEURL + 'suggested-designers/'+response.data.data;
                    } else {
                        hideLoader(".create-project",loadertext);
                        simpleAlert('error', '', response.data.message);
                    }
                });
            }
        }
       
    }
    
    $("#addFileDynamic").click(function() {
        var nextID = 'id' + (new Date()).getTime();
        var html = "<div class='hide' id=\"div" + nextID + "\">";
        html += "<br><input multiple name=\"attachment_name[]\" id='" + nextID + "' type=\"file\" accept=\"*\" class=\"filestyle\" >";
        html += "<a href=\"javascript:void(0);\" id='rem" + nextID + "' class=\"remImg\">Remove</a></div></div>";
        $("#divImages").append(html);
        $('#' + nextID).click();
    });
    $(".upload-files-list").on('click', '.remImg', function() {
        var fileName = $(this).attr('id');
        // alert(fileName);
        $(this).parent('.removeFile').remove();
        filesToRemove.push(fileName);
    });
    $(document).on("change", ".filestyle", function(event) {
        var files = [];
        for (var i = 0; i < event.target.files.length; i++) {
            files.push(event.target.files[i]);
            $('.upload-files-list').append("<li class='removeFile'>" + event.target.files[i].name + "<a class='remImg' href=\"javascript:void(0);\" id='" + event.target.files[i].name + "'><i class='fa fa-times'></i></a></li><br/>");
        }
        // if (jQuery.inArray(ext, allowedFormats) !== -1) {
        //     var index = data.index,
        //         file = data.files[index],
        //         node = $(data.context.children()[index]);
        //     if (file.preview) {
        //         node.prepend(file.preview);
        //     }
        //     if (file.error) {
        //         node.append($('<span class="text-danger"/>').text(file.error));
        //     }
        //     if (index + 1 === data.files.length) {
        //         if (!!data.files.error) data.context.find('button.upload').prop('disabled');
        //         // data.context.find('button.upload').prop('disabled', !!data.files.error);
        //     }
        //     //uploadButton.click(uploadButton);
            
        //     if ($scope.orignionalFileName.indexOf($scope.fileNameUploaded) !== -1) {
        //         return;
        //     } else {
                
        //     }
        // } else {
        //     simpleAlert("error", "", "Compressed files not allowed");
        //     return;
        // }
    });
}]);
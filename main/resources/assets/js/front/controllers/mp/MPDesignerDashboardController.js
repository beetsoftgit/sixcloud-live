/*
 Author  : Komal Kapadi (komal@creolestudios.com)
 Date    : 4th April 2018
 Name    : MPDesignerDashboardController
 Purpose : All the functions for MP designer dashboard
 */
angular.module('sixcloudApp').controller('MPDesignerDashboardController', ['$sce', 'userfactory','mpfactory', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, userfactory,mpfactory, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {
    console.log('MPDesignerDashboardController controller loaded');
    $('body').css('background', '#e3eefd');
   
    $timeout(function(){
        $('.designer-dashboard-menu').find('li').removeClass('active');
        $('#designer_dashboard').addClass('active');
    },700);

    getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
    $('#chk_lang').change(function() {
        getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
    });

    $scope.endto = $scope.startfrom + 1;
    $scope.currentPage = $scope.currentPaginate = 1;
    $scope.range = $scope.rangepage = $scope.rangePaging = [];
    $scope.customActiveProjectList = function(data,pageNumber) {
        mpfactory.Designeractiveprojects(data,pageNumber).then(function(response) { // get category list
            if (response.data.status === 0) {
                return false;
            }
            $scope.activeProjects = response.data.data.data;
            $scope.pagination = response.data.data;
            $scope.currentActivePaging = response.data.data.current_page;
            $scope.rangeActivepage = _.range(1, response.data.data.last_page + 1);
        });
    };
    $scope.customActiveProjectList({'language': getLanguage}, 1);
    $scope.projectInvitation = function(pageNumber) {
        mpfactory.Designerprojectsinvitation(pageNumber).then(function(response) {
            $scope.invitations = response.data.data.data;
            $scope.paginate = response.data.data;
            $scope.currentInvitePaging = response.data.data.current_page;
            $scope.rangeInvitepage = _.range(1, response.data.data.last_page + 1);
            //$scope.getCountDown(response.data.data.data[0].days_remaining);
        });
    };
    $scope.projectInvitation(1);
     $scope.current_domain = current_domain ;
    
    $scope.cancelledprojectrequests = function(pageNumber) {
        mpfactory.Sellercanceldprojectrequestlisting(pageNumber).then(function(response) {
            $scope.cancelprojectrequestprojects = response.data.data.data;
            $scope.total_cancel_request_projects = response.data.data.total;
            $scope.paginateCancelRequest = response.data.data;
            $scope.currentCancelRequestPaging = response.data.data.current_page;
            $scope.rangeCancelRequestpage = _.range(1, response.data.data.last_page + 1);
        });
    };
    $scope.cancelledprojectrequests(1);

    $scope.disputedprojects = function(pageNumber) {
        mpfactory.Sellerdisputedprojectlisting(pageNumber).then(function(response) {
            $scope.disputedprojects = response.data.data.data;
            $scope.total_dispute_projects = response.data.data.total;
            $scope.paginateDispute = response.data.data;
            $scope.currentDisputePaging = response.data.data.current_page;
            $scope.rangeDisputepage = _.range(1, response.data.data.last_page + 1);
        });
    };
    $scope.disputedprojects(1);
}]);
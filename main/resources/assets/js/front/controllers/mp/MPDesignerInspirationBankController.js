/*
 Author  : Komal Kapadi (komal@creolestudios.com)
 Date    : 25th April 2018
 Name    : MPDesignerInspirationBankController
 Purpose : All the functions to get MP inspiration bank
 */
angular.module('sixcloudApp').controller('MPDesignerInspirationBankController', ['Designerinspirationbankcategorylistsing', '$scope', '$rootScope' ,'mpfactory', '$timeout', function(Designerinspirationbankcategorylistsing, $scope, $rootScope, mpfactory, $timeout) {
    console.log("MP designer inspiration bank controller loaded");
    $scope.message = 'Look! I am an about page.';
    $('body').css('background', '#e3eefd');

    $('.upload-files-list1').html('');
    $('.upload-files-list').html('');
    $timeout(function () {
        $('.filter-buttons').find('li').removeClass('active');
        $('#exploreAll').addClass('active');
    }, 200); 
    getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
    changeLanguage(getLanguage);
    $('#chk_lang').change(function() {
        getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
        changeLanguage(getLanguage);
    });

    function changeLanguage(lang) {
        if (lang == "chi") {
            $timeout(function(){
                $scope.english = false;
            },500);
        } else {
            $timeout(function(){
                $scope.english = true;
            },500);
        }
    }
    $scope.inspirationBankCategoryListing = Designerinspirationbankcategorylistsing.data.data;  
    $scope.status = 0;
    $scope.categoryId = 'all';
    $scope.searchedCategory = 'all';
    $scope.ExploreType = 'all';
    $scope.searchInspirationBank = '';
    $scope.endto = $scope.startfrom + 1;
    $scope.currentPage = $scope.currentPaginate = 1;
    $scope.range = $scope.rangepage = $scope.rangePaging = [];
    $scope.filterData = {
        'categoryId': 'all',
        'type':$scope.ExploreType
    };

    getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');    
    $('#chk_lang').change(function() {
        getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
      
    });

    getInspirationBankListing($scope.filterData);
    $scope.filterDataByCategory = function(categoryId) {
        $scope.searchedCategory = categoryId;
        $scope.filterData = {
            'categoryId': categoryId,
            'search': $scope.searchInspirationBank,
            'type':$scope.ExploreType
        };
        getInspirationBankListing($scope.filterData);
    }
    $scope.searchData = function(categoryId, searchInspirationBank) {
        $scope.filterData = {
            'categoryId': categoryId,
            'search': searchInspirationBank,
            'type':$scope.ExploreType
        };
        getInspirationBankListing($scope.filterData);
    }

    function getInspirationBankListing(data, pageNumber) {
        mpfactory.Designerinspirationbanklisting(data, pageNumber).then(function(response) {
            $scope.status = response.data.status;
            if (response.data.status == 1) {
                $scope.inspirationbankListing = response.data.data.data;                
                $scope.originalData = response.data.data.data;
                $scope.pagination = response.data.data;
                $scope.currentGalleryPaging = response.data.data.current_page;
                $scope.rangeGallerypage = _.range(1, response.data.data.last_page + 1);
                $timeout(function(){
                    $scope.swiperfunction();
                }, 1000);
            } else {
                $scope.message = response.data.message;
                console.log($scope.message);
            }
        });
    }
    $timeout(function(){
    var grid = document.querySelector('.grid');
         var msnry;

         imagesLoaded( grid, function() {
           // init Isotope after all images have loaded
           msnry = new Masonry( grid, {
             itemSelector: '.grid-item'
           });
         });
         imagesLoaded( grid ).on( 'progress', function() {
           // layout Masonry after each image loads
           msnry.layout();
         });
          }, 100);
    $scope.customProductList = function(pageNumber) {
        getInspirationBankListing($scope.filterData, pageNumber);
    }
    $scope.resetFilter = function() {
        $scope.categoryId = 'all';
        $scope.searchInspirationBank = '';
        $scope.filterData = {
            'categoryId': 'all', 
            'type':$scope.ExploreType
        };
        getInspirationBankListing($scope.filterData);
    }
    $(document).ready(function(){
        $scope.downloadZip = function(whichFile,whichClass,whichId){

            whichClass=whichClass+whichId;
            $("."+whichClass).text(' ');
            $("."+whichClass).append("<span class='font'><i class='fa fa-spinner fa-spin'></i></span>");
            $("."+whichClass).attr("disabled", "disabled");
            mpfactory.Download({'file_name':whichFile,'inspiration_bank':1,'id':whichId}).then(function (response) {

                if (response.data.status == 1)
                {
                    var url = response.data.data.url;
                    
                    $('#attach_file_dynamic'+whichId).attr('href',url);
                    $('#attach_file_dynamic'+whichId).attr('target','_self');
                    $('#attach_file_dynamic'+whichId).addClass('btn btn-white btn-sm');
                    $('#attach_file_dynamic'+whichId)[0].click();
                    $('#attach_file_dynamic'+whichId).html('<i aria-hidden="true" class="fa fa-download"></i> '+response.data.data.count);
                    $("."+whichClass).hide();

                    $timeout(function(){
                        mpfactory.Unlinkdownloadedfile({'file_name': response.data.data.file_name,'inspiration_bank':1}).then(function (response) {
                          
                            if (response.data.status == 1)
                            {
                                var file_unlinked_success = response.data.message;
                            }
                        });
                    },2000);
                }
            });
        }
    });

    $scope.showImagePreview = function(getImageData){        
        // $scope.Images = getImageData.cover_image_attachments_files;
        $scope.Images = getImageData.all_images;
        $('#previewModal').modal('show');
        $timeout(function(){
           $scope.popupSwiperFunction();
        }, 1000);
    }   
    
    $scope.swiperfunction = function(){
        $timeout(function(){
            var swiper = new Swiper('.browse-designers-swiper', {
                slidesPerView: 1,
                spaceBetween: 0,
                loop: true,
                 navigation: {
                   nextEl: '.browse-designers-button-next',
                   prevEl: '.browse-designers-button-prev',
                 },
            });
        }, 1000);
    }
    $timeout(function(){
        $scope.swiperfunction();      
    }, 1000);
    
    $('.designer-inpiration-bank-section .swiper-container').click(function() {
        $timeout(function(){
         $scope.popupSwiperFunction();
      }, 1000);     
      
    });
    
    $scope.popupSwiperFunction=function(){
        var swiper = new Swiper('.bank-popup-swiper-container', {
           slidesPerView: 1,
           spaceBetween: 0,
           loop: false,
            navigation: {
              nextEl: '.popup-designers-button-next',
              prevEl: '.popup-designers-button-prev',
            },
          });
    }    
    $scope.showExplore = function(type){        
        $scope.ExploreType = type;
        if(type == 'all'){
            $timeout(function () {
                $('.filter-buttons').find('li').removeClass('active');
                $('#exploreAll').addClass('active');
            }, 200);            
        }else if(type == 'favourites'){
            $timeout(function () {
                $('.filter-buttons').find('li').removeClass('active');
                $('#favourtieItem').addClass('active');
            }, 200);            
        }else if(type == 'uploads'){
            $timeout(function () {
                $('.filter-buttons').find('li').removeClass('active');
                $('#uploadedItems').addClass('active');
            }, 200);
        }
        $scope.filterData = {
            'categoryId': $scope.searchedCategory,
            'type': type
        };
        getInspirationBankListing($scope.filterData,$scope.page);        
    };
    $scope.toggleFavourite = function(id, ExploreType){      
        var inspirationBankID = id;         
        var data = {"inspirationBankID": inspirationBankID,'language':getLanguage};
        mpfactory.MakeUserFavourite(data).then(function(response) {                                               
            if (response.data.status == 1) {
                $scope.filterData = {
                    'categoryId': $scope.searchedCategory,
                    'type': ExploreType
                };
                getInspirationBankListing($scope.filterData,$scope.page);        
                $('.make-favourite-class').toggleClass( "active" );                
                // simpleAlert('success', '', response.data.message);
            } else {
                simpleAlert('error', '', response.data.message);
            }
        });        
    }
}]);
/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 30th March 2018
 Name    : MPDesignerPortfolioController
 Purpose : All the functions for MP login page
 */
angular.module('sixcloudApp').controller('MPDesignerPortfolioController', ['$sce', 'mpfactory', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, mpfactory, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {
    console.log('MPDesignerPortfolio controller loaded');
    $('body').css('background', '#e3eefd');
    initSample();
    $timeout(function() {
        if ($rootScope.loginUserData.status == 0) {
            window.location.replace("sixteen-login");
        }
        $('.designer-dashboard-menu').find('li').removeClass('active');
        $('#designer_portfolio').addClass('active');
    }, 500);
    $('.dPortfolioTab li').click(function() {
        $('.dPortfolioTab li').removeClass('active');
        $('.dPortfolioTab').toggleClass('dp-switch-btn');
        $(this).addClass('active');
    });
    getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
    changeLanguage(getLanguage);
    $('#chk_lang').change(function() {
        getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
        changeLanguage(getLanguage);
    });
    
    function changeLanguage(lang){ 
        if(lang=="chi"){
            $scope.english = false;
            $scope.chinese = true;
            getLanguage=='chi';
        }    
        else{
            $scope.english = true;
            $scope.chinese = false;
            getLanguage=='en';
      }
    }
    (function() {
        $('.FlowupLabels').FlowupLabels({
            feature_onInitLoad: true,
            class_focused: 'focused',
            class_populated: 'populated'
        });
    })();
    $scope.multipleInput = 1;
    mpfactory.Getcompletedprojectsforportfolio().then(function(response) {
        if (response.data.code == 2) window.location.replace("sixteen-login");
        if (response.data.status == 1) {
            $scope.completedProjectsList = response.data.data;
        } else {}
    });
    $scope.endto = $scope.startfrom + 1;
    $scope.currentPage = $scope.currentPaginate = 1;
    $scope.range = $scope.rangepage = $scope.rangePaging = [];
    $scope.Portfoliofunction = function(data,pageNumber) {
        mpfactory.Getportfolio(data,pageNumber).then(function(response) {
            if (response.data.code == 2) window.location.replace("sixteen-login");
            if (response.data.status == 1) {
                $scope.designerPortfolios = response.data.data.data;
                $scope.availableSlots = response.data.data.available_slots;
                $scope.totalPortfolio = response.data.data.total;
                $scope.pagination = response.data.data;
                $scope.currentActivePaging = response.data.data.current_page;
                $scope.rangeActivepage = _.range(1, response.data.data.last_page + 1);
            } else {
                $scope.availableSlots = response.data.data.available_slots;
            }
        });
    }
    $scope.Portfoliofunction({'language':getLanguage},1);
    $scope.Nomoreportfolio = function(){
        //simpleAlert('info','','No spots available');
        if(getLanguage=='en'){
            simpleAlert('info','','No spots available');
        }else{
            simpleAlert('信息','','没有景点');
        }
    }
    mpfactory.Portfoliocategory({'language':getLanguage}).then(function(response) {
        if (response.data.status == 1) {
            $scope.categories = response.data.data;
        } else {}
    });
    $(document).ready(function() {
        $("#price").on("keypress keyup blur", function(event) {
            $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
    });
    $('#completedProjectsSelect').on('change', function(e) {
        var valueSelected = this.value;
        mpfactory.Getprojectinfo({
            'id': valueSelected
        }).then(function(response) {
            if (response.data.status == 1) {
                $scope.project = response.data.data;
                // $timeout(function(){
                $scope.upload_title = $scope.project.project_title;
                $scope.upload_title_value = $scope.project.project_title;
                 $timeout(function(){
                  CKEDITOR.instances.editor.setData($scope.project.project_description);
                },500);
                $scope.upload_description = $scope.project.project_description;
                $scope.selectedProject = $scope.project;
                //$('#upload_title').attr('disabled', 'disabled');
                $('#label_upload_title').addClass('populated');
                // },500);
            } else {}
        });
    });
    $scope.category_count = 0;
    $scope.category_error = false;
    $scope.Categoryselected = function() {
        $scope.category_count = 1;
        $scope.category_error = false;
    }
    $scope.showlist = function() {
        $('#completedProjectsSelect').val('');
        $scope.internalPortfolio = true;
    }
    $scope.hidelist = function() {
        $scope.internalPortfolio = false;
        $scope.upload_title = undefined;
        $scope.upload_description = undefined;
        $scope.upload_title_value = undefined;
        $('#upload_title').removeAttr('disabled');
        $('#label_upload_title').removeClass('populated');
    }

    var filesPortfolioToRemove = [];
    var fileSize = 0;
    filesPortfolio = [];
    file_attachments=[];
    var coverImageArray = [];
    $("#addFileDynamic").click(function(event) {
        event.stopPropagation();
        event.preventDefault();
        var nextID = 'id' + (new Date()).getTime();
        var html = "<div class='hide' id=\"div" + nextID + "\">";
        html += "<br><input multiple name=\"attachments[]\" id='" + nextID + "' type=\"file\" accept=\"*\" class=\"filestyle\" >";
        html += "<a href=\"javascript:void(0);\" id='rem" + nextID + "' class=\"remImg\">Remove</a></div></div>";
        $("#divImagesportfolio").append(html);
        $('#' + nextID).click();
    });
    $(".portfolio_li").on('click', '.remImg', function(event) {
        event.stopPropagation();
        event.preventDefault();
        var fileName = $(this).attr('id');
        angular.forEach(filesPortfolio, function(value, key){
          if(value.name == fileName){
                fileSize = fileSize-value.size;
                filesPortfolio.splice(key,1);
          }
        });
        angular.forEach(file_attachments, function(value, key){            
            if(value == fileName){
                file_attachments.splice(key,1);
            }
        });
        $(this).parent('.removeFile').remove();
        $(this).closest("br");
        filesPortfolioToRemove.push(fileName);        
    });
    $(document).on("change", ".filestyle", function(event) {
    //$(".filestyle").on('change',function(event){
        event.stopImmediatePropagation();
        event.preventDefault();
        for (var i = 0; i < event.target.files.length; i++) {
            filesPortfolio.push(event.target.files[i]);
            file_attachments.push(event.target.files[i].name);
            fileSize = fileSize+filesPortfolio[i].size;
            $('.portfolio_li').append("<li class='removeFile'>" + event.target.files[i].name + "<a class='remImg' href=\"javascript:void(0);\" id='" + event.target.files[i].name + "'><i class='fa fa-times'></i></a></li>");
        }
    });
    $(".cover-image-container").on('click', '.remImg', function() {
        var fileName = $(this).attr('id');
        $(this).parent('.removeFile').remove();
        angular.forEach(coverImageArray, function(value, key){
          if(value.name == fileName){
             fileSize = fileSize-value.size;
             coverImageArray.splice(key,1);
          }
        });
        $('#cover_image').val('');
    });
    $(document).on("change", "#cover_image", function(event) {
        for (var i = 0; i < event.target.files.length; i++) {
            coverImageArray.push(event.target.files[i]);
            fileSize = fileSize+event.target.files[i].size;
        }
        $('.cover-image-container').html("<li class='removeFile'>" + event.target.files[0].name + "<a class='remImg cancel-css' href=\"javascript:void(0);\" id='" + event.target.files[0].name + "'><i class='fa fa-times'></i></a></li>");
    });

    $scope.showNewPortfolioModel = function(){ 

        $scope.portfolioType=false;
        $scope.upload_title='';
        CKEDITOR.instances.editor.setData('');
        $("#formupload").trigger("reset");
        $('.portfolio_li').empty();
        $('.cover-image-container').empty();      
        $('#GalleryModal').modal('show');
    } 

    $scope.Uploadnewportfolio = function() {
        var categoryClickCount = 0;
        if ($scope.portfolioType == undefined||!$scope.portfolioType) {
            if(getLanguage=='en'){
                simpleAlert('error', '', 'Select Portfolio type.');
            }else {
                simpleAlert('error', '', '选择作品类别');
            }
            return;
        } else if ($scope.portfolioType == 1) {
            if ($('#completedProjectsSelect').val() == '') {
                if($scope.completedProjectsList.length>1){
                    if(getLanguage=='en'){
                        simpleAlert('error', '', 'Select Project');
                    }else {
                        simpleAlert('error', '', '选择项目');
                    }
                    return;
                } else {
                    if(getLanguage=='en'){
                        simpleAlert('error', '', 'No Projects available for Portfolio. You can create "Other" Portfolio');
                    }else {
                        simpleAlert('error', '', '没有项目可用于投资组合。您可以创建“其他”投资组合');
                    }
                    return;
                }
            }
        }
        $scope.upload_description= CKEDITOR.instances.editor.getData();
        
        if($scope.upload_description=='')
        {
            $scope.upload_description=undefined;
        }
        if ($scope.upload_title == '' || $scope.upload_title == undefined || $scope.upload_description == undefined) {
            if ($scope.upload_title == '' || $scope.upload_title == undefined) {
                if(getLanguage=='en'){
                  text='The "Title" is required.';         
                }else{
                  text='请输入“标题”。';          
                }
              simpleAlert('error', '', text);
              return;
            }
            if ($scope.upload_description == undefined) {
                if(getLanguage=='en'){
                    simpleAlert('error','','The "Description" is required.');
                } else {
                    simpleAlert('error','','“描述”是必需的。');
                }
                return;
            }
        } else {
            $("input[name='skills[]']").each( function () {
                if($(this)[0].checked == true){
                    categoryClickCount++;
                }
            });  
            if(categoryClickCount==0){
                if(getLanguage=='en'){
                    simpleAlert('error','','Please Select Atleast 1 Category.');
                } else {
                    simpleAlert('error','','请选择至少1个类别。');
                }
                return;
            }
            
            var totalFiles = 0;             
            totalFiles = filesPortfolio.length + coverImageArray.length;
            if(totalFiles==0){                
                if(getLanguage=='en'){
                    hideLoader(".create-portfolio",'Submit');
                }else{
                    hideLoader(".create-portfolio",'提交');
                }                
                if(getLanguage=='en'){
                    simpleAlert('error','','The "Thumbnail" and "Image(s)" are required.');
                }else{
                    simpleAlert('错误','','需要“缩略图”和“图像”。');
                }
                return;
            }
            if(coverImageArray.length == 0 ){                
                if(getLanguage=='en'){
                    hideLoader(".create-portfolio",'Submit');
                }else{
                    hideLoader(".create-portfolio",'提交');
                }                
                if(getLanguage=='en'){
                    simpleAlert('error','','The "Thumbnail" is required.');
                }else{
                    simpleAlert('错误','','“缩略图”是必需的。');
                }
                return;
            }
            if(filesPortfolio.length == 0 ){                
                if(getLanguage=='en'){
                    hideLoader(".create-portfolio",'Submit');
                }else{
                    hideLoader(".create-portfolio",'提交');
                }                
                if(getLanguage=='en'){
                    simpleAlert('error','','The "Image(s)" is/are required.');
                }else{
                    simpleAlert('错误','','需要“图像”。');
                }

                return;
            }
            
            //if(totalFiles<=5){
                if(fileSize<30000000){
                    if(getLanguage=='en'){
                        showLoader(".create-portfolio");
                    }else{
                        showLoader(".create-portfolio",'请稍候');
                    }
                    var newPortfolio = new FormData($("#formupload")[0]);
                    newPortfolio.append('upload_of', 3);
                    newPortfolio.append('upload_title', $scope.upload_title);
                    newPortfolio.append('remove_attachments', filesPortfolioToRemove);
                    newPortfolio.append('portfolio_type', $scope.portfolioType);
                    newPortfolio.append('language', getLanguage);
                    newPortfolio.append("upload_description", CKEDITOR.instances.editor.getData());
                    newPortfolio.append('file_attachments', file_attachments);
                    
                    if ($scope.portfolioType == 1) {
                        //newPortfolio.append('upload_title', $scope.upload_title_value);
                        newPortfolio.append('upload_title', $scope.upload_title);
                        newPortfolio.append('project_details', JSON.stringify($scope.selectedProject));
                    }
                    mpfactory.Portfolio(newPortfolio).then(function(response) {
                        //hideLoader(".create-portfolio",'Submit');
                        if(getLanguage=='en'){
                            hideLoader(".create-portfolio",'Submit');
                        }else{
                            hideLoader(".create-portfolio",'提交');
                        }
                        if (response.data.status == 1) {
                            $scope.Portfoliofunction(1);
                            simpleAlert('success', '', response.data.message);
                            $('#GalleryModal').modal('hide');
                            $scope.category_id        = undefined;
                            $scope.upload_title       = undefined;
                            $scope.upload_description = undefined;
                            $scope.portfolioType      = undefined;
                            $("#formupload").trigger("reset");
                            $('.portfolio_li').empty();
                            $('.cover-image-container').empty();
                            filesPortfolio = [];
                            coverImageArray = [];
                            CKEDITOR.instances.editor.setData('');
                            $scope.category_count = 0;
                        } else {
                            //hideLoader(".create-portfolio",'Submit');
                            if(getLanguage=='en'){
                                hideLoader(".create-portfolio",'Submit');
                            }else{
                                hideLoader(".create-portfolio",'提交');
                            }
                            simpleAlert('error', '', response.data.message);
                        }
                    });
                } else {
                    //hideLoader(".create-portfolio",'Submit');
                    if(getLanguage=='en'){
                        hideLoader(".create-portfolio",'Submit');
                    }else{
                        hideLoader(".create-portfolio",'提交');
                    }
                    //simpleAlert('error','','Files size limit exceeded.\n(Only 30mb allowed combining all files)');
                    if(getLanguage=='en'){
                        simpleAlert('error','','Files size limit exceeded.\n(Only 30mb allowed combining all files)');
                    }else{
                        simpleAlert('error','','超出文件大小限制。\n(只有30mb允许组合所有文件)');
                    }
                    return;
                }
            /*} else{
                hideLoader(".create-portfolio",'Submit');
                simpleAlert('error','','File count limit exceeded.\n(Total of 5 images are allowed)');
                return;
            }*/
        }
    }
}]);
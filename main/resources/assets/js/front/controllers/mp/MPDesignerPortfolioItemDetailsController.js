/*
 Author  : Senil Shah (senil@creolestudios.com)
 Date    : 30th March 2018
 Name    : MPDesignerPortfolioItemDetailsController
 Purpose : All the functions for MP login page
 */
angular.module('sixcloudApp').controller('MPDesignerPortfolioItemDetailsController', ['$sce', 'mpfactory', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, mpfactory, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {
    console.log('MPDesignerPortfolioItemDetails controller loaded');
    $('body').css('background','#e3eefd');
    $timeout(function(){
      $('.designer-dashboard-menu').find('li').removeClass('active');
      $('#designer_portfolio').addClass('active');
    },500);
    $timeout(function(){
      var galleryTop = new Swiper('.gallery-top', {
            spaceBetween: 10,
            observer: true,
            navigation: {
              nextEl: '.swiper-button-next',
              prevEl: '.swiper-button-prev',
            },
      });
      var galleryThumbs = new Swiper('.gallery-thumbs', {
        spaceBetween: 10,
        centeredSlides: true,
        slidesPerView: 3,
        touchRatio: 0.2,
        slideToClickedSlide: true,
        observer: true,
        breakpoints: {

        767: {
            direction:'horizontal',
            slidesPerView: 1.5,
            spaceBetween: 15,
          },
        480: {
           direction:'horizontal',
            slidesPerView: 1.5,
            spaceBetween: 15,
          }
        }
      });
      galleryTop.controller.control = galleryThumbs;
      galleryThumbs.controller.control = galleryTop;
    },700);


     
    
    var getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
    changeLanguage(getLanguage);
    $('#chk_lang').change(function() {
      var getLanguage = ($('#chk_lang').prop('checked') ? 'en' : 'chi');
        changeLanguage(getLanguage);
    });

    if(getLanguage=='en'){
      confirmButtonText = 'Ok';
    }else{
      confirmButtonText = '好。';
    }
    
    function changeLanguage(lang){ 
        if(lang=="chi"){
            $("#chi_portfolio_data").show();
            $("#en_portfolio_data").hide();
            getLanguage='chi';

        }    
        else{
            $("#en_portfolio_data").show();
            $("#chi_portfolio_data").hide();
            getLanguage='en';
      }
    }

    $scope.getSlug         = $routeParams.slug;
    $scope.getSlugArray    = $routeParams.slug.split('|')
    $scope.mp_portfolio_id = $scope.getSlugArray[0];
    $scope.mp_designer_id  = $scope.getSlugArray[1];
    
    $scope.Portfoliofunction = function(){
        mpfactory.Portfolioinfo({'mp_portfolio_id':$scope.mp_portfolio_id, 'designer_id' : $scope.mp_designer_id, 'detailPage':true}).then(function(response) {
            if(response.data.code == 2)
                window.location.replace("sixteen-login");
            if (response.data.status == 1)
            {
              $scope.portfolioInfo = response.data.data;
            }
            else {
              confirmDialogueAlert("Error",response.data.message,"error",false,'#8972f0',confirmButtonText,'',true,'','',deleteredirect);
            }
        });
    }
    $scope.Portfoliofunction();
    function deleteredirect(){
      window.location.replace('seller-portfolio');
    }
    function deletefunction(){
      mpfactory.Deleteportfolio({'mp_portfolio_id':$scope.slug,'language':getLanguage}).then(function(response) {
          if(response.data.code == 2)
              window.location.replace("sixteen-login");
          if (response.data.status == 1)
          {
            if(getLanguage=='en'){
              confirmDialogueAlert("Success",response.data.message,"success",false,'#8972f0',confirmButtonText,'',true,'','',deleteredirect);
            }else{
              confirmDialogueAlert("成功",response.data.message,"success",false,'#8972f0',confirmButtonText,'',true,'','',deleteredirect);
            }
          }
          else {
            simpleAlert('error','',response.data.message);
          }
      });
    }
    $scope.Deleteportfolio = function(slug,title){
        $scope.slug = slug;
        //confirmDialogue('Are You sure you want to delete '+ title +'?','Portfolio Deleted','',deletefunction);
        if(getLanguage=='en'){
          confirmDialogue('Are You sure you want to delete '+ title +'?','Portfolio Deleted','',deletefunction);
        }else{
          confirmDialogue('你确定你要删除 '+ title +'?','投资组合被删除','',deletefunction);
        }
        
    }
}]);


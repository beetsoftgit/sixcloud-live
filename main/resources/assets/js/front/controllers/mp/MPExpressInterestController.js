/*
 Author  : Zalak Kapadia (zalak@creolestudios.com)
 Date    : 24 July 2018
 Name    : MPExpressInterestController
 Purpose : Express Interest
 */
angular.module('sixcloudApp').controller('MPExpressInterestController', [ '$http', '$scope','mpfactory','$timeout','$rootScope','MetaService',function( $http, $scope,mpfactory,$timeout,$rootScope,MetaService) {
    console.log('MPExpressInterestController controller loaded');
    // $rootScope.metaservice = MetaService;
    // $rootScope.metaservice.set("Sign Up Sixteen | Become A Seller or Buyer","Sign Up SixClouds’s Sixteen to get quality knowledge-based programmes through a variety of bespoke multi-media platforms.");
    var getLanguage;
    var id = $('.lang_button').attr('id');
    if(id == 'eng'){
        $scope.isEnglish = false;
        getLanguage='chi';
    }
    else{
        $scope.isEnglish = true;
        getLanguage='en';
    }
    $scope.engbutton = function(){
        $('#eng').click(function(){
            $timeout(function(){
                $scope.isEnglish = true;
                getLanguage='en';
            },500);
            $scope.cnbutton();
        });
    }
    $scope.cnbutton = function(){
        $('#cn').click(function(){
            $timeout(function(){
                $scope.isEnglish = false;
                getLanguage='chi';
            },500);
            $scope.engbutton();
        });
    }
    $scope.engbutton();
    $scope.cnbutton();

    $scope.createExpressInterestUser=function() {
    	
    	if($scope.email_address == undefined || $scope.first_name == undefined || $scope.last_name == undefined || $scope.express_interest_accpet==undefined){
    		if($scope.email_address == undefined){
                $scope.email_address_error = true;
            }
            if($scope.first_name == undefined){
                $scope.first_name_error = true;
            }
            if($scope.last_name == undefined){
                $scope.last_name_error = true;
            }
            if($scope.email_address != undefined && $scope.first_name != undefined && $scope.last_name != undefined && $scope.express_interest_accpet==undefined){
            	if(getLanguage=='en'){
                    simpleAlert('success','','Please accept our terms of service and privacy policy.');    
                }else{
                    simpleAlert('success','','请接受我们的服务条款和隐私政策。');
                }
                
            }
    	}else{

    		showLoader('.submit_call');
    		var expressData = new FormData($("#mpEpressForm")[0]);
    		expressData.append('email_address',$scope.email_address);
    		expressData.append('first_name',$scope.first_name);
    		expressData.append('last_name',$scope.last_name);
            expressData.append('language',getLanguage);

	    		mpfactory.Createexpressinterestuser(expressData).then(function(response){
                    if(getLanguage=='en'){
	    			    hideLoader('.submit_call','Submit');
                    }else{
                        hideLoader('.submit_call','提交');
                    }
			        if(response.data.status==1)
			        {
                        if(getLanguage=='en'){
                            text='Sixclouds Team';
                        }else{
                            text='Sixclouds团队';
                        }
			            simpleAlert('success',text,response.data.message);
                        $timeout(function(){
                            window.location.href="sixteen";
                        },500);
			        }  
                    else{
                        simpleAlert('Error','',response.data.message); 
                    }    
			    });
    	}
    }
}]); 
/*
 Author  : Komal Kapadi (komal@creolestudios.com)
 Date    : 16th April 2018
 Name    : MPDesignerPastProjectInfoController
 Purpose : All the functions for MP designer past project info
 */
angular.module('sixcloudApp').controller('MPDesignerPastProjectInfoController', ['$sce', 'mpfactory', '$routeParams', '$http', '$scope', '$timeout', '$rootScope', '$filter', function($sce, mpfactory, $routeParams, $http, $scope, $timeout, $rootScope, $filter) {
    console.log('MPDesignerProjectInfoController controller loaded');
    $project_slug = $routeParams.slug;
    $scope.message = 'Type here.';
    $timeout(function(){
        if($rootScope.loginUserData.status == 0){
            window.location.replace("sixteen-login");
        }
        $('.designer-dashboard-menu').find('li').removeClass('active');
        $('#designer_past_project').addClass('active');
    },500);
    $('body').css('background', '#e3eefd');
    $('.projectDataBtn').click(function() {
        $('#projectDataText').slideToggle();
    });
    $('.designer-dashboard-menu').find('li').removeClass('active');
    $('#designer_past_project').addClass('active');
    $scope.limit = 2;
    mpfactory.Designerprojectinfo({
        'slug': $project_slug
    }).then(function(response) {
        if (response.data.status == 1) {
            $scope.projectData = response.data.data;
            $scope.skill_array=[];
                angular.forEach($scope.projectData.skills, function (skill, index) {
                    if (skill.skill_detail.status==1) {
                        $scope.skill_array.push(skill);
                    }
                });
        } else {
            simpleAlert('error', 'Signup', response.data.message, true);
        }
    });
    $scope.scrollEventCallback = function() {
        if (($(window).scrollTop() + 200) >= $(document).height() - $(window).height() - 200) {
            $scope.limit = $scope.limit + 2;
        }
    }
    $scope.current_domain = current_domain;
}]);
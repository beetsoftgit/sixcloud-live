/*
 Author  : Komal Kapadi (komal@creolestudios.com)
 Date    : 5th December 2017
 Name    : HomeController
 Purpose : All the functions for signup
 */
angular.module('sixcloudApp').controller('SignupController', ['$http', '$scope', '$timeout', '$rootScope', '$filter', function ($http, $scope, $timeout, $rootScope, $filter) {
        console.log('signup controller loaded');
        $scope.update = function (user) {
            console.log(user);
        }
        $(document).ready(function () {
            $(".input-label input").focus(function ()
            {
                $(this).parent(".input-label").addClass('typing');
            });
            $('.input-label input').blur(function () {
                if ($('.input-label input').val() != '') {
                    $(this).parent(".input-label").addClass('typing');
                } else {
                    $(this).parent(".input-label").removeClass('typing');
                }
            });
        });
        $(".single-datepicker").daterangepicker({
            singleDatePicker: true
        });
    }]);
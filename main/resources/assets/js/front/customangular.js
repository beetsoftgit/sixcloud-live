/* 
 * Author: Komal Kapadi
 * Date  : 4th Dec 2017
 * Custom angular file 
 */
/* JavaScript Document */
app = angular.module('sixcloudApp', ['ngRoute', 'angular-page-loader', 'ngSanitize', 'pascalprecht.translate', 'ngImgCrop']);
/*Check if user logout it redirect the login page.*/
function checklogin(param) {
    if (param.hasOwnProperty('logout')) {
        /*localStorage.removeItem("urlObj");
        localStorage.removeItem("url_number_static");*/
        window.location = 'login';
    } else {
        return true;
    }
}
/* To change the interolate provider need to change it's originonal brackets.*/
app.config(['$interpolateProvider',  function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
    /*localStorage.setItem("url_number_static",0);*/
}]);
/* for changing title */
app.run(['$location', '$rootScope', '$http', '$timeout', '$window', function($location, $rootScope, $http, $timeout, $window) {
    $rootScope.$on('$routeChangeSuccess', function(event, current, previous) {
        if (current.$$route != undefined){
            $rootScope.title = current.$$route.title;
            $rootScope.module = current.$$route.module;
            let url = window.location;
            let urlData = url.href.split( '/' );
            var locationURL = "";
            if(url.hostname === "localhost"){
                locationURL = $location.url();
            }else{
                if(urlData.includes("beta")){
                    locationURL = "beta"+ $location.url();
                }else{
                    locationURL = $location.url();
                }
            }
            $window.ga('send', 'pageview', { page: locationURL });

        }
        document.body.scrollTop = document.documentElement.scrollTop = 0;
    });
}]);
app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider){
    $routeProvider
        // .when('/', {
        //     templateUrl: BASEURL + '/resources/views/front/angular/elt/home.html',
        //     controller: 'HomeController',
        //     title: 'SixClouds :: Home',
        //     resolve: {
        //         Geteltsections: function(eltfactory) {
        //             return eltfactory.Geteltsections();
        //         }
        //     }
        // })
        // .when('/home', {
        //     templateUrl: BASEURL + '/resources/views/front/angular/elt/home.html',
        //     controller: 'HomeController',
        //     title: 'SixClouds :: Home',
        //     resolve: {
        //         Geteltsections: function(eltfactory) {
        //             return eltfactory.Geteltsections();
        //         }
        //     }
        // })
        .when('/subscription-plan', {
            templateUrl: BASEURL + '/resources/views/front/angular/elt/after-signup.html',
            controller: 'SubscriptionPlanController',
            title: 'SixClouds :: Signup'
        }).when('/proof-reading', {
            templateUrl: BASEURL + '/resources/views/front/angular/pr/home.html',
            controller: 'PRHomeController',
            title: 'SixClouds :: Proof Reading',
            module:'pr',
        }).when('/my-cases', {
            templateUrl: BASEURL + '/resources/views/front/angular/pr/my-cases.html',
            controller: 'PRMyCasesController',
            title: 'SixClouds :: My Cases',
            module:'pr',
        }).when('/elt', {
            templateUrl: BASEURL + '/resources/views/front/angular/elt/user-dashboard.html',
            controller: 'EltSectionDetailController',
            title: 'SixClouds :: ELT',
            resolve: {
                Geteltsections: function(eltfactory) {
                    return eltfactory.Geteltsections();
                }
            }
        }).when('/elt-video/:id', {
            templateUrl: BASEURL + '/resources/views/front/angular/elt/video-viewing.html',
            controller: 'EltVideoController',
            title: 'SixClouds :: ELT Videos'
        }).when('/distributor', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/ignite-login.html',
            title: 'SixClouds :: Ignite Login',
            controller: 'IgniteLoginController',
            module:'ignite',
        }).when('/distributor/team-login', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/ignite-login.html',
            title: 'SixClouds :: Ignite Login',
            controller: 'IgniteLoginController',
            module:'ignite',
        }).when('/ignite-login/:encodedStr?', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/ignite-login.html',
            // title: 'Sign In – Ignite Login',
            // description: 'Sign in SixClouds Ignite and learn english with animated videos, interactive quizzes and worksheets.',
            controller: 'IgniteLoginController',
            module:'ignite',
        }).when('/verify-distributor', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/verify-distributor.html',
            title: 'SixClouds :: Verify Distributor',
            controller: 'IgniteVerifyDistributorController',
            module:'ignite',
        }).when('/subscribers', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/all-referral.html',
            title: 'SixClouds :: All Referral',
            controller: 'IgniteAllReferralController',
            module:'ignite',
        }).when('/my-codes', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/my-codes.html',
            title: 'SixClouds :: My Codes',
            controller: 'IgniteMyCodesController',
            module:'ignite',
        }).when('/ignite-subscriber-details/:slug', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/subscribers-details-admin.html',
            controller: 'IgniteSubscriberDetailsController',
            title: 'SixClouds :: Subscribers Detail',
            module:'ignite'
        }).when('/my-referral', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/all-referral.html',
            title: 'SixClouds :: All Referral',
            controller: 'IgniteAllReferralController',
            module:'ignite',
        }).when('/verify-phone-number', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/verify-phone-number.html',
            title: 'SixClouds :: Verify Phone Number',
            controller: 'IgniteVerifyPhoneNumberController',
            module:'ignite',
        }).when('/my-team', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/my-team.html',
            title: 'SixClouds :: My Team',
            controller: 'IgniteMyTeamController',
            module:'ignite',
        }).when('/team-detail/:slug', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/team-detail.html',
            title: 'SixClouds :: Team Member Detail',
            controller: 'IgniteTeamDetailController',
            module:'ignite',
        }).when('/distributor-my-account', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/my-account.html',
            title: 'SixClouds :: My Account',
            controller: 'IgniteMyAccountController',
            module:'ignite',
        }).when('/team-my-account', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/my-account.html',
            title: 'SixClouds :: My Account',
            controller: 'IgniteMyAccountController',
            module:'ignite',
        }).when('/my-account', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/my-account.html',
            title: 'SixClouds :: My Account',
            controller: 'IgniteMyAccountController',
            module:'ignite',
        }).when('/ignite', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/ignite-home.html',
            controller: 'IgniteHomeController',
            // title: 'SixClouds Ignite - Learning English Online Platfrom',
            // description: 'SixClouds Ignite is a learning english online platform where kids can learn through fun and exciting animated videos, interactive quizzes and worksheets.',
            module:'ignite',
        }).when('/ignite-buzz', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/ignite-english.html',
            controller: 'IgniteHomeController',
            // title: 'SixClouds Ignite - Learning English Online Platfrom',
            // description: 'SixClouds Ignite is a learning english online platform where kids can learn through fun and exciting animated videos, interactive quizzes and worksheets.',
            module:'ignite',
            resolve: {
                Getuserzone: function(ignitefactory) {
                    return ignitefactory.Getuserzone();
                }
            }
        }).when('/ignite-maze', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/ignite-mathematics.html',
            controller: 'IgniteHomeController',
            // title: 'SixClouds Ignite - Learning English Online Platfrom',
            // description: 'SixClouds Ignite is a learning english online platform where kids can learn through fun and exciting animated videos, interactive quizzes and worksheets.',
            module:'ignite',
        }).when('/ignite-smile-international', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/ignite-smile.html',
            controller: 'IgniteHomeController',
            // title: 'SixClouds Ignite - Learning English Online Platfrom',
            // description: 'SixClouds Ignite is a learning english online platform where kids can learn through fun and exciting animated videos, interactive quizzes and worksheets.',
            module:'ignite',
        }).when('/ignite-smile-singapore', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/ignite-smile-sg.html',
            controller: 'IgniteHomeController',
            // title: 'SixClouds Ignite - Learning English Online Platfrom',
            // description: 'SixClouds Ignite is a learning english online platform where kids can learn through fun and exciting animated videos, interactive quizzes and worksheets.',
            module:'ignite',
        }).when('/ignite-mathematics', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/ignite-math.html',
            controller: 'IgniteHomeController',
            // title: 'SixClouds Ignite - Learning English Online Platfrom',
            // description: 'SixClouds Ignite is a learning english online platform where kids can learn through fun and exciting animated videos, interactive quizzes and worksheets.',
            module:'ignite',
        }).when('/ignite-smile-philippines', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/ignite-smile-ph.html',
            controller: 'IgniteHomeController',
            // title: 'SixClouds Ignite - Learning English Online Platfrom',
            // description: 'SixClouds Ignite is a learning english online platform where kids can learn through fun and exciting animated videos, interactive quizzes and worksheets.',
            module:'ignite',
        }).when('/ignite-signup/:slug?/:mode?', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/sign-up.html',
            title: 'SixClouds :: SignUp',
            controller: 'IgniteSignUpController',
            module:'ignite',
        }).when('/user-verify-otp/:phonecode?/:contact?/:email_address?', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/user-verify-otp.html',
            title: 'SixClouds :: Verify OTP',
            controller: 'IgniteUserVerifyOtpController',
            module:'ignite',
        }).when('/ignite-categories/:subject_id?', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/categories.html',
            title: 'SixClouds :: Categories',
            controller: 'IgniteCategoriesController',
            module:'ignite',
        }).when('/select-categories', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/select-categories.html',
            title: 'SixClouds :: Select Categories',
            controller: 'IgniteSelectCategoriesController',
            module:'ignite',
        }).when('/content-detail/:slug', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/video-info.html',
            title: 'SixClouds :: Content Details',
            controller: 'IgniteContentDetailController',
            module:'ignite',
        }).when('/quiz/:slug', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/quiz.html',
            title: 'SixClouds :: Quiz',
            controller: 'IgniteQuizController',
            module:'ignite',
        }).when('/score-card/:slug', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/quiz-result.html',
            title: 'SixClouds :: Quiz Result',
            controller: 'IgniteQuizResultController',
            module:'ignite',
        }).when('/my-subscription/:slug?', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/my-subscription.html',
            title: 'SixClouds :: My Subscription',
            controller: 'IgniteMySubscriptionController',
            module:'ignite',
            resolve: {
                Getuserzone: function(ignitefactory) {
                    return ignitefactory.Getuserzone();
                }
            }
        }).when('/ignite-payment/:token', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/stripe-ignite-payment.html',
            title: 'SixClouds :: Ignite Payment',
            controller: 'IgniteStripePaymentController',
            module:'ignite',
        }).when('/distributors-commissions', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/commissions.html',
            title: 'SixClouds :: commissions',
            controller: 'IgniteCommissionsController',
            module:'ignite',
        }).when('/team-member-commissions', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/commissions.html',
            title: 'SixClouds :: commissions',
            controller: 'IgniteCommissionsController',
            module:'ignite',
        }).when('/my-performance', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/my-performance.html',
            title: 'SixClouds :: Performances',
            controller: 'IgnitePerformancesController',
            module:'ignite',
        }).when('/switch-plan/:subject_id', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/switch-plan.html',
            title: 'SixClouds :: SwitchPlan-Dashbord',
            controller: 'IgniteSwitchPlanController',
            module:'ignite',
        }).when('/subscription-plan/:subject_id', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/subscription-plan.html',
            title: 'SixClouds :: SubscriptionPlan-Dashbord',
            controller: 'IgniteSubscriptionPlanController',
            module:'ignite',
            resolve: {
                Getuserzone: function(ignitefactory) {
                    return ignitefactory.Getuserzone();
                }
            }
        }).when('/subscription-payment/:token', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/subscription-payment.html',
            title: 'SixClouds :: Ignite Subscription Payment',
            controller: 'IgniteSubscriptionPaymentController',
            module:'ignite',
        }).when('/sixteen', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/mp-home.html',
            controller: 'MPHomeController',
            // title: 'SixClouds Sixteen – A Creative Platfrom To Hire Freelancers & Get Freelance Jobs Online',
            // description:'Sixteen is a creative platfrom to show your creative skills and hire the best talents. Explore services like logo, web, 2D/3D, app design, video animation and more.',
            module:'mp',
        }).when('/mts-video-upload', {
            templateUrl: BASEURL + '/resources/views/front/angular/elt/video-viewing.html',
            controller: 'ELTvideouploadController',
            title: 'SixClouds :: Video Upload'
        }).when('/seller-signup/:email', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/designer-sign-up.html',
            controller: 'MPDesignerSignupController',
            title: 'SixClouds :: Designer Signup',
            module:'mp',
            resolve: {
                Getskills: function(mpfactory) {
                    return mpfactory.Getskills();
                },
                Getcountrycitypro: function(mpfactory) {
                    return mpfactory.Getcountrycitypro();
                },
                Getschools: function(mpfactory) {
                    return mpfactory.Getschools();
                },
                Getlanguages: function(mpfactory) {
                    return mpfactory.Getlanguages();
                },
            }
        }).when('/seller-signup', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/designer-sign-up.html',
            controller: 'MPDesignerSignupController',
            title: 'SixClouds :: Designer Signup',
            module:'mp',
            resolve: {
                Getskills: function(mpfactory) {
                    return mpfactory.Getskills();
                },
                Getcountrycitypro: function(mpfactory) {
                    return mpfactory.Getcountrycitypro();
                },
                Getschools: function(mpfactory) {
                    return mpfactory.Getschools();
                },
                Getlanguages: function(mpfactory) {
                    return mpfactory.Getlanguages();
                },
            }
        }).when('/buyer-sign-up', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/mp-buyer-signup.html',
            title: 'SixClouds :: Sixteen Buyer SignUp',
            controller: 'MPBuyerSignUpController',
            module:'mp',
        }).when('/express-interest', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/express-interest.html',
            // title: 'Sign Up Sixteen | Become A Seller or Buyer',
            // description: 'Sign Up SixClouds’s Sixteen to get quality knowledge-based programmes through a variety of bespoke multi-media platforms.',
            controller: 'MPExpressInterestController',
            module:'mp',
        }).when('/sixteen-login', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/login.html',
            // title: 'Sign In – SixClouds Sixteen',
            // description: 'Log in at SixClouds Sixteen, the world\'s top multi-media platforms – Sixteen.',
            controller: 'MPLoginController',
            module:'mp',
        }).when('/create-new-project', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/new-project.html',
            title: 'SixClouds :: Create Now Project',
            controller: 'MPCreateNewProjectController',
            module:'mp',
            resolve: {
                Getskills: function(mpfactory) {
                    return mpfactory.Getskills();
                },
            }
        }).when('/suggested-sellers/:slug/:assigned_designer_id?', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/designer-suggestions.html',
            title: 'SixClouds :: Suggested Sellers for project',
            controller: 'MPSuggestedDesignerController',
            module:'mp',
        }).when('/buyer-dashboard', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/buyer-dashboard.html',
            title: 'SixClouds :: Buyer Dashboard',
            controller: 'MPBuyerDashboardController',
            module:'mp',
        }).when('/seller-dashboard', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/designer-dashboard.html',
            title: 'SixClouds :: Seller Dashboard',
            controller: 'MPDesignerDashboardController',
            module:'mp',
        }).when('/browse-sellers/:slug?/:assigned_designer_id?', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/browse-designers.html',
            title: 'SixClouds :: Browse sellers',
            controller: 'MPBrowseDesignersController',
            module:'mp',
        }).when('/buyer-past-projects', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/buyer-past-projects.html',
            title: 'SixClouds :: Buyer Past Projects',
            controller: 'MPBuyerPastProjectsController',
            module:'mp',
        })
        /*.when('/gallery', {
                templateUrl: BASEURL + '/resources/views/front/angular/mp/gallery-coming-soon.html',
                title: 'SixClouds :: Gallery',
                controller: 'MPBuyerGalleryController',
            })*/
        .when('/buyer-project-info/:slug', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/buyer-project-info-active.html',
            title: 'SixClouds :: Buyer Project Info',
            controller: 'MPBuyerProjectInfoActiveController',
            module:'mp',
        }).when('/seller-my-uploads', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/designer-my-uploads.html',
            controller: 'MPDesignerMyUploadsController',
            module:'mp',
            resolve: {
                Designermyuploadscount: function(mpfactory) {
                    return mpfactory.Designermyuploadscount();
                }
            }
        }).when('/buyer-sign-up', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/mp-buyer-signup.html',
            // title: 'Sign Up Sixteen | Become A Seller or Buyer',
            // description: 'Sign Up SixClouds’s Sixteen to get quality knowledge-based programmes through a variety of bespoke multi-media platforms.',
            controller: 'MPBuyerSignUpController',
            module:'mp',
        }).when('/sixteen-login', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/login.html',
            // title: 'Sign In – SixClouds Sixteen',
            // description: 'Log in at SixClouds Sixteen, the world\'s top multi-media platforms – Sixteen.',
            controller: 'MPLoginController',
            module:'mp',
        }).when('/create-new-project/:slug?/:whatAction?/:cartId?/:selectedDesigner?', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/new-project.html',
            title: 'SixClouds :: Create Now Project',
            controller: 'MPCreateNewProjectController',
            module:'mp',
            resolve: {
                Getskills: function(mpfactory) {
                    return mpfactory.Getskills();
                },
            }
        }).when('/suggested-sellers/:slug/:assigned_designer_id?', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/designer-suggestions.html',
            title: 'SixClouds :: Suggested Sellers for project',
            controller: 'MPSuggestedDesignerController',
            module:'mp',
        }).when('/buyer-dashboard', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/buyer-dashboard.html',
            title: 'SixClouds :: Buyer Dashboard',
            controller: 'MPBuyerDashboardController',
            module:'mp',
        }).when('/seller-dashboard', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/designer-dashboard.html',
            title: 'SixClouds :: Seller Dashboard',
            controller: 'MPDesignerDashboardController',
            module:'mp',
        }).when('/browse-sellers', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/browse-designers.html',
            title: 'SixClouds :: Browse sellers',
            controller: 'MPBrowseDesignersController',
            module:'mp',
        }).when('/buyer-past-projects', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/buyer-past-projects.html',
            title: 'SixClouds :: Buyer Past Projects',
            controller: 'MPBuyerPastProjectsController',
            module:'mp',
        })
        /*.when('/gallery', {
                templateUrl: BASEURL + '/resources/views/front/angular/mp/gallery-coming-soon.html',
                title: 'SixClouds :: Gallery',
                controller: 'MPBuyerGalleryController',
            })*/
        .when('/buyer-project-info/:slug', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/buyer-project-info-active.html',
            title: 'SixClouds :: Buyer Project Info',
            controller: 'MPBuyerProjectInfoActiveController',
            module:'mp',
        }).when('/make-payment/:slug', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/make-payment.html',
            title: 'SixClouds :: Make Payment',
            controller: 'MakePaymentController',
            module:'mp',
        }).when('/make-payment-ignite/:slug', {
            templateUrl: BASEURL + '/resources/views/front/angular/ignite/make-payment-ignite.html',
            title: 'SixClouds :: Make Payment',
            controller: 'MakePaymentIgniteController',
            module:'ignite',
        }).when('/seller-my-uploads', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/designer-my-uploads.html',
            controller: 'MPDesignerMyUploadsController',
            module:'mp',
            resolve: {
                Designermyuploadscount: function(mpfactory) {
                    return mpfactory.Designermyuploadscount();
                }
            }
        }).when('/buyer-past-project-info/:slug', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/buyer-past-project-info.html',
            title: 'SixClouds :: Buyer Past Project Info',
            controller: 'MPBuyerProjectInfoPastController',
            module:'mp',
        }).when('/seller-portfolio', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/designer-portfolio.html',
            title: 'SixClouds :: Seller Portfolio',
            controller: 'MPDesignerPortfolioController',
            module:'mp',
        }).when('/seller-portfolio-item-details/:slug', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/designer-portfolio-item-details.html',
            title: 'SixClouds :: Seller Portfolio Item Details',
            controller: 'MPDesignerPortfolioItemDetailsController',
            module:'mp',
        }).when('/inspiration-bank-item-details/:slug', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/designer-inspiration-bank-item-details.html',
            title: 'SixClouds :: Inspiration Bank Item Details',
            controller: 'MPDesignerInspirationBankItemDetailsController',
            module:'mp',
        }).when('/seller-portfolio-item-edit/:slug', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/edit-album-portfolio.html',
            title: 'SixClouds :: Seller Portfolio Edit',
            controller: 'MPDesignerPortfolioItemEditController',
            module:'mp',
        }).when('/inspiration-bank-item-edit/:slug', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/edit-inspiration-bank.html',
            title: 'SixClouds :: Seller Portfolio Edit',
            controller: 'MPDesignerInspirationBankEditController',
            module:'mp',
        }).when('/seller-project-info/:slug', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/designer-project-info.html',
            controller: 'MPDesignerProjectInfoController',
            module:'mp',
        }).when('/seller-project-info-invitation/:slug', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/designer-project-info-invitation.html',
            controller: 'MPDesignerProjectInfoInvitationController',
            module:'mp',
        }).when('/seller-past-projects', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/designer-past-projects.html',
            controller: 'MPDesignerPastProjectsController',
            module:'mp',
        }).when('/seller-past-project-info/:slug', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/designer-past-project-info.html',
            title: 'SixClouds :: Buyer Past Project Info',
            controller: 'MPDesignerPastProjectInfoController',
            module:'mp',
        }).when('/seller-inspiration-bank', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/designer-inspiration-bank.html',
            controller: 'MPDesignerInspirationBankController',
            module:'mp',
            resolve: {
                Designerinspirationbankcategorylistsing: function(mpfactory) {
                    return mpfactory.Portfoliocategory();
                }
            }
        }).when('/seller-profile/:id?/:mp_project_id?', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/designer-profile.html',
            controller: 'designerProfileController',
            module:'mp',
            resolve: {
                /*Designerprofileinfo: function(mpfactory) {
                    return mpfactory.Designerprofileinfo();
                },
                Designerworkhistoryandreviews: function(mpfactory) {
                    return mpfactory.Designerworkhistoryandreviews();
                }*/
            }
        }).when('/seller-my-account', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/designer-my-account.html',
            title: 'SixClouds :: Seller My Account',
            controller: 'MPDesignerMyAccountController.js',
            module:'mp',
            resolve: {
                Getcountry: function(mpfactory) {
                    return mpfactory.Getcountry();
                },
                Getschools: function(mpfactory) {
                    return mpfactory.Getschools();
                },
                Getlanguages: function(mpfactory) {
                    return mpfactory.Getlanguages();
                },
            }
        }).when('/buyer-my-account', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/designer-my-account.html',
            title: 'SixClouds :: Buyer My Account',
            controller: 'MPDesignerMyAccountController.js',
            module:'mp',
            resolve: {
                Getcountry: function(mpfactory) {
                    return mpfactory.Getcountry();
                },
                Getschools: function(mpfactory) {
                    return mpfactory.Getschools();
                },
                Getlanguages: function(mpfactory) {
                    return mpfactory.Getlanguages();
                },
            }
        }).when('/my-services', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/services.html',
            title: 'SixClouds :: Seller My services',
            controller: 'MPSellerMyServicesController',
            module:'mp',
        }).when('/notifications', {
            templateUrl: BASEURL + '/resources/views/front/angular/mp/marketplace-notifications.html',
            title: 'SixClouds :: Notifications',
            controller: 'MPNotificationsController',
            module:'mp',
        }).when('/sphere', {
            templateUrl: BASEURL + '/resources/views/front/angular/sphere/home.html',
            title: 'SixClouds :: Sphere',
            controller: 'SphereLandingPageController',
            module:'sphere',
        }).when('/sphere-signup', {
            templateUrl: BASEURL + '/resources/views/front/angular/sphere/sign-up.html',
            title: 'SixClouds :: Sphere SignUp',
            controller: 'SphereSignupController',
            module:'sphere',
        }).when('/sphere-messaging-teacher', {
            templateUrl: BASEURL + '/resources/views/front/angular/sphere/messaging-teacher.html',
            title: 'SixClouds :: Sphere Messages',
            controller: 'SphereMessageController',
            module:'sphere',
        }).when('/teachers', {
            templateUrl: BASEURL + '/resources/views/front/angular/sphere/teachers.html',
            title: 'SixClouds :: Sphere Teachers',
            controller: 'SphereTeacherController',
            module:'sphere',
            resolve: {
                Getspheresubjects: function(spherefactory) {
                    return spherefactory.Getspheresubjects();
                },
            }
        }).when('/teacher-profile/:slug', {
            templateUrl: BASEURL + '/resources/views/front/angular/sphere/teacher-profile.html',
            title: 'SixClouds :: Sphere Teacher\'s Profile',
            controller: 'SphereTeacherProfileController',
            module:'sphere',
        }).when('/sphere-transactions', {
            templateUrl: BASEURL + '/resources/views/front/angular/sphere/transactions.html',
            title: 'SixClouds :: Sphere Transactions',
            controller: 'SphereTransactionsController',
            module:'sphere',
        }).when('/sphere-history', {
            templateUrl: BASEURL + '/resources/views/front/angular/sphere/history.html',
            title: 'SixClouds :: Sphere Histories',
            controller: 'SpherehistoryController',
            module:'sphere',
            resolve: {
                Getmycourses: function(spherefactory) {
                    return spherefactory.Getmycourses();
                },
            }
        }).otherwise({
            redirectTo: '/ignite-login'
        });
    $locationProvider.html5Mode(true);
}]);
/*CONFIG*/
app.run(function($rootScope, $location, $route, $timeout, userfactory) {
    $rootScope.layout = {};
    $rootScope.$on('$routeChangeStart', function() {
        var list = ['/elt', '/elt-video'];
        if (list.indexOf($location.path()) >= 0) {
            userfactory.Getloginuserdata().then(function(response) {
                if (response.data.success) {} else {
                    // simpleAlert('Unauthorized', '', ' Redirecting for login.....', false);
                    // setTimeout(function () {//redirect to dashboard after 3 seconds
                    // location.href = BASEURL + 'signin';
                    // }, 2500);
                }
            });
        } else {
        }
        $timeout(function() {
            $rootScope.isLoading = true;
        });
    });
    $rootScope.$on('$routeChangeSuccess', function() {
        $timeout(function() {
            $rootScope.isLoading = false;
        }, 200);
    });
    $rootScope.$on('$routeChangeError', function() {
        $rootScope.isLoading = false;
    });
});
//app.config(function ($httpProvider) {
//    console.log($httpProvider.responseInterceptors);
//    $httpProvider.responseInterceptors.push('myHttpInterceptor');
//    var spinnerFunction = function spinnerFunction(data, headersGetter) {
//        return data;
//    };
//    $httpProvider.defaults.transformRequest.push(spinnerFunction);
//});
app.factory('myHttpInterceptor', function($q, $window) {
    return function(promise) {
        return promise.then(function(response) {
            /* checklogin(response.data);*/
            return response;
        }, function(response) {
            return $q.reject(response);
        });
    };
});
app.directive('scrollTrigger', function($window) {
    return {
        link: function(scope, element, attrs) {
            var offset = parseInt(attrs.threshold) || 0;
            var e = jQuery(element[0]);
            var doc = jQuery(document);
            angular.element(document).bind('scroll', function() {
                if (doc.scrollTop() + $window.innerHeight + offset > e.offset().top) {
                    scope.$apply(attrs.scrollTrigger);
                }
            });
        }
    };
});
app.filter('trusted', ['$sce', function($sce) {
    return $sce.trustAsResourceUrl;
}]);
app.directive('defaultPagination', function() {
    return {
        restrict: 'E',
        scope: {
            paginate: '=',
            currentpage: '=',
            range: '=',
            like: '&'
        },
        templateUrl: BASEURL + '/resources/views/front/pagination.html'
    };
});
app.directive('validFile', function() {
    return {
        require: 'ngModel',
        link: function(scope, el, attrs, ctrl) {
            ctrl.$setValidity('validFile', el.val() != '');
            //change event is fired when file is selected
            el.bind('change', function() {
                ctrl.$setValidity('validFile', el.val() != '');
                scope.$apply(function() {
                    ctrl.$setViewValue(el.val());
                    ctrl.$render();
                });
            });
        }
    }
});
/*==================================
= paginator (client side)          =
==================================*/
/* paginator (client side) */
app.service('Paginator', function() {
    this.page = 0; /* current page */
    this.rowsPerPage = 10; /* default rows per page */
    this.itemCount = 0; /* count of total items */
    this.currentRecord = 0; /* count of current records */
    this.setPage = function(page) { /* to set page */
        if (page > this.pageCount()) {
            return;
        }
        this.page = page;
    };
    this.nextPage = function() { /* for next age click */
        if (this.isLastPage()) {
            return;
        }
        this.page++;
    };
    this.perviousPage = function() { /* for previous page click */
        if (this.isFirstPage()) {
            return;
        }
        this.page--;
    };
    this.firstPage = function() { /* to go on first page */
        this.page = 0;
    };
    this.lastPage = function() { /* to go on last page */
        this.page = this.pageCount() - 1;
    };
    this.isFirstPage = function() { /* check if it's first page */
        return this.page == 0;
    };
    this.isLastPage = function() { /* check if it's last page */
        return this.page == this.pageCount() - 1;
    };
    this.pageCount = function() { /* count total number of pages */
        return Math.ceil(parseInt(this.itemCount) / parseInt(this.rowsPerPage));
    };
});
app.directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }            
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});
app.directive('datePicker', function($timeout){
    return {
        restrict: 'A',
        link: function($scope, iElm, iAttr, controller){
            $( function() {
                iElm.datetimepicker({
                    pickTime: false,
                    format: 'DD/MM/YYYY',
                    maxDate: new Date(),
                });
            });
        }
    };
});
app.directive('cardFormat', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^\dA-Z]/g, '').replace(/(.{4})/g, '$1 ').trim();

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }            
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});
app.service('MetaService', function() {
   var title = 'SixClouds Web';
   var metaDescription = '';
   return {
      set: function(newTitle, newMetaDescription) {
          metaDescription = newMetaDescription;
          title = newTitle; 
      },
      metaTitle: function(){ return title; },
      metaDescription: function() { return metaDescription; }
   }
});
app.directive('countdown', [
        'Util',
        '$interval',
        function (Util, $interval) {
            return {
                restrict: 'A',
                scope: { date: '@' },
                link: function (scope, element) {
                    var future;
                    future = new Date(scope.date.replace(/-/g, "/"));
                    var newDate = new Date(future.getTime()+future.getTimezoneOffset()*60*1000);
                    var offset = future.getTimezoneOffset() / 60;
                    var hours = future.getHours();
                    newDate.setHours(hours - offset);
                    
                    $interval(function () {
                        var diff;
                        /*var today = new Date();
                        var str = today.toGMTString();  // deprecated! use toUTCString()*/
                        diff = Math.floor((newDate.getTime() - new Date().getTime()) / 1000);
                        return element.text(Util.dhms(diff));
                    }, 1000);
                }
            };
        }
    ]).factory('Util', [function () {
            return {
                dhms: function (t) {
                    var days, hours, minutes, seconds;
                    days = Math.floor(t / 86400);
                    t -= days * 86400;
                    hours = Math.floor(t / 3600) % 24;
                    t -= hours * 3600;
                    minutes = Math.floor(t / 60) % 60;
                    t -= minutes * 60;
                    seconds = t % 60;
                    if(days > 0)
                    return [
                        days + 'd',
                        hours + 'h',
                        minutes + 'm',
                        seconds + 's'
                    ].join(' ');
                    else 
                       return [
                        hours + 'hrs',
                        minutes + 'mins',
                        seconds + 'secs'
                    ].join(' ');
                }
            };     
        }]);
app.filter('monthYear', function($filter) {
    return function(input) {
        if (input == null) {
            return '';
        }
        var date = moment(input).format('MMM YYYY');
        return date;
    }
});

app.filter('date', function($filter) {
    return function(input) {
        if (input == null) {
            return '';
        }
        var date = moment(input).format('D/MM/YY HH:mm:ss');
        return date;
    }
});

app.filter('translateSubject', function($filter) {
    return function(input) {
        if (input == null) {
            return '';
        }
        if(input.toLowerCase() == 'mandarin') {
            return '细探针';
        } else {
            return input;
        }
    }
});

/* Disable console.log if mode is production.*/
if (project_mode == 'production') console.log = function() {}
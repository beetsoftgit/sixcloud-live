/* 
 * Author: Komal Kapadi
 * Date  : 11th Dec 2017
 * ELT factory file
 */
angular.module('sixcloudApp').factory('eltfactory', ['$http', '$rootScope',
    function ($http, $rootScope) {
        return {
            // check if user logged in or not
            Geteltsections: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Geteltsections'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            GetVideodata: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'GetVideodata'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Addnote: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Addnote'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Deletenote: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Deletenote'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            }/*,
            Uploadvideodemo: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Uploadvideodemo',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    console.log('m here');
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            }*/
        };
    }
]);
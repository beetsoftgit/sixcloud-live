/* 
 * Author: Komal Kapadi
 * Date  : 4th Dec 2017
 * Common factory file
 */
angular.module('sixcloudApp').factory('userfactory', ['$http',
    function ($http) {
        return {
            Getcases: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getcases'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Addcase: function (data) {
                $('.spinner-loader').show();
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Addcase'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
                $('.spinner-loader').hide();
            },
            Rejectcase: function (data) {
                $('.spinner-loader').show();
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Rejectcase'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
                $('.spinner-loader').hide();
            },
            Download: function (data) {
                $('.spinner-loader').show();
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Downloadcase'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
                $('.spinner-loader').hide();
            },
            Unlinkdownloadedfile: function (data) {
                $('.spinner-loader').show();
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Unlinkdownloadedfile'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
                $('.spinner-loader').hide();
            },
            /*check if user logged in or not*/
            Getloginuserdata: function (data) {
                return $http({
                    method: 'GET',
                    data: data,
                    url: 'Getloginuserdata'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Logout: function (data) {
                return $http({
                    method: 'GET',
                    data: data,
                    url: 'Logout'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            }
        };
    }
]);
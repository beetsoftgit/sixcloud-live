/* 
 * Author: Komal Kapadi
 * Date  : 11th Dec 2017
 * Marketplace factory file
 */
angular.module('sixcloudApp').factory('mpfactory', ['$http', '$rootScope',
    function($http, $rootScope) {
        return {
            Getskills: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getskills'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Uploadvideodemo: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Uploadvideodemo',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    console.log('m here');
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getcountrycitypro: function(data) {
                return $http({
                    method: 'GET',
                    data: data,
                    url: 'Getcountrycitypro'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getcountry: function(data) {
                return $http({
                    method: 'GET',
                    data: data,
                    url: 'Getcountry'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getstatesforcountry: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getstatesforcountry'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getcitiesforstate: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getcitiesforstate'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Dosignup: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Dosignup',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Createnewdesigner: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Createnewdesigner',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Createnewproject: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Createnewproject',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            /* Static demo change senil : start */
            Updateproject: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Createnewproject',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            /* Static demo change senil : end */
            Designersuggestions: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Designersuggestions',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Asssigndesigner: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Asssigndesigner',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Updatebudgettimeline: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Updatebudgettimeline',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Dosignin: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Dosignin',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            forgotpassword: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'forgotpassword'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getschools: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getschools'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Buyerprojects: function(data, pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Buyerprojects?page=' + pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getdesigners: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getdesigners'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Dofilter: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Dofilter',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getlanguages: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getlanguages'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Designeractiveprojects: function(data,pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'GET',
                    data:data,
                    url: 'Activeprojects?page=' + pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Designerprojectsinvitation: function(pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    url: 'Designerprojectsinvitation?page=' + pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Adddesignerreview: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Adddesignerreview'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Projectinfo: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Projectinfo'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Sendmessage: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Sendmessage'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Sendmessageform: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Sendmessage'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Actions: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Actions'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Cancelwithoutseller: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Cancelwithoutseller'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Designergalleryuploads: function(pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    url: 'Designergalleryuploads?page=' + pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Designerinspirationbankuploads: function(pageNumber,searchBy) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                // if(searchBy==undefined){
                //     return $http({
                //         method: 'GET',
                //         url: 'Designerinspirationbankuploads?page=' + pageNumber
                //     }).then(function successCallback(response) {
                //         return response;
                //     }, function errorCallback(response) {
                //         return response;
                //     });
                // } else {
                    return $http({
                        method: 'GET',
                        url: 'Designerinspirationbankuploads?page=' + pageNumber + '&searchBy=' + searchBy
                    }).then(function successCallback(response) {
                        return response;
                    }, function errorCallback(response) {
                        return response;
                    });
                // }
            },
            Designermyuploadscount: function(pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    url: 'Designermyuploadscount?page=' + pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Designerinsbankupload: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Designerinsbankupload'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Designergallerynewupload: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Designergallerynewupload'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Designerprojectinfo: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Designerprojectinfo'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Designermarkprojectcomplete: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Designermarkprojectcomplete'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            DesignerCancelProject: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'DesignerCancelProject'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Designerinvitation: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Designerinvitation'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Designerinvitationresponse: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Designerinvitationresponse'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Portfolio: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Portfolio',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Editportfolio: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Editportfolio',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Portfoliocategory: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Portfoliocategory'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getportfolio: function(data,pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    data:data,
                    url: 'Getportfolio?page=' + pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getcompletedprojectsforportfolio: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getcompletedprojectsforportfolio'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Portfolioinfo: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Portfolioinfo'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Deleteportfolio: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Deleteportfolio'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Deleteportfolioimage: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Deleteportfolioimage'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            DeleteportfolioVideo: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'DeleteportfolioVideo'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Designerpastprojects: function(pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    url: 'Designerpastprojects?page=' + pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Designerpastprojectinfo: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Designerpastprojectinfo'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            
            Designerinspirationbankcategorylistsing:function()
            {
                return $http({
                    method: 'POST',
                    url: 'Designerinspirationbankcategorylistsing'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Designerinspirationbanklisting:function(data)
            {
                return $http({
                    method: 'POST',
                    data:data,
                    url: 'Designerinspirationbanklisting'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Unlinkmsgbrdremovedfile: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Unlinkmsgbrdremovedfile'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Designerinspirationbankcategorylistsing: function() {
                return $http({
                    method: 'POST',
                    url: 'Designerinspirationbankcategorylistsing'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Designerinspirationbanklisting: function(data, pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Designerinspirationbanklisting?page=' + pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Addextensionforproject: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Addextensionforproject'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Designerprofileinfo: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Designerprofileinfo'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Designerworkhistoryandreviews:function(data,pageNumber){
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Designerworkhistoryandreviews?page='+pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getportfoliodata: function(data,pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    data:data,
                    url: 'Getportfolio?page=' + pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Acceptrejectextension: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Acceptrejectextension'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Searchdesigner: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Searchdesigner'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Makeprojectportfolio: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Makeprojectportfolio',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Portfolioimages: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Portfolioimages'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Acceptprojectcomplitionrequest: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Acceptprojectcomplitionrequest'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Addprojectcompletionrequestrejectreason: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Addprojectcompletionrequestrejectreason'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Sendinvitationfordesignersignup: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Sendinvitationfordesignersignup'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Updateuserdata: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Updateuserdata',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Decodeemail: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Decodeemail'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getsentinvitationdetails: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getsentinvitationdetails'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getprojectinfo: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getprojectinfo'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Addwithdrawrequest: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Addwithdrawrequest'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            paymentStatusChange: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'paymentStatusChange'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            addReason: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'paymentStatusChange'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Designercancelledprojects: function(pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    url: 'Designercancelledprojects?page='+pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Buyercancelledprojects: function(pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    url: 'Buyercancelledprojects?page='+pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Changecancelprojectstatus: function(data) {
                
                return $http({
                    method: 'POST',
                    data:data,
                    url: 'Changecancelprojectstatus'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Mutualcancelprojectlisting: function(pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    url: 'Mutualcancelprojectlisting?page=' + pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Disputedprojectlisting: function(pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    url: 'Disputedprojectlisting?page=' + pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Sellercanceldprojectrequestlisting: function(pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    url: 'Sellercanceldprojectrequestlisting?page=' + pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Sellerdisputedprojectlisting: function(pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    url: 'Sellerdisputedprojectlisting?page=' + pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Readmessages: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Readmessages'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Readnotifications: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Readnotifications'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Attentiongained: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Attentiongained'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Designerprojectinvitationrejected: function(pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    url: 'Designerprojectinvitationrejected?page=' + pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Addlanguage: function(data) {
                return $http({
                    method: 'POST',
                    data:data,
                    url: 'Addlanguage',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Checkoldpassword: function(data) {
                return $http({
                    method: 'POST',
                    data:data,
                    url: 'Checkoldpassword',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Updateuserpassword: function(data) {
                return $http({
                    method: 'POST',
                    data:data,
                    url: 'Updateuserpassword',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Addsellerunavailability: function(data) {
                return $http({
                    method: 'POST',
                    data:data,
                    url: 'Addsellerunavailability',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Deleteunavailablity: function(data) {
                return $http({
                    method: 'POST',
                    data:data,
                    url: 'Deleteunavailablity',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Updatesellerdetails: function(data) {
                return $http({
                    method: 'POST',
                    data:data,
                    url: 'Updatesellerdetails',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Deletelanguage: function(data) {
                return $http({
                    method: 'POST',
                    data:data,
                    url: 'Deletelanguage',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            
            Addserviceskills: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Addserviceskills',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getserviceskills: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getserviceskills',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Addservices: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Addservices',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Addservice: function() {
                return $http({
                    method: 'POST',
                    url: 'Addservices',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Deleteservices: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Deleteservices',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getexistingservices: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getexistingservices'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Changecurrentlanguage: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Changecurrentlanguage'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getnotifications: function(pageNumber) { 
                return $http({
                    method: 'POST',
                    // data: data,
                    url: 'Getnotifications?page=' + pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getunreadnotificationscount: function() { 
                return $http({
                    method: 'POST',                    
                    url:    'Getunreadnotificationscount'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Editbankdetails: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Editbankdetails',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getuserpaymenthistory: function (pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'GET',
                    url: 'Getuserpaymenthistory?page=' + pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Download: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'DownloadId'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Unlinkdownloadedfile: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'UnlinkdownloadedfileIdProof'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Switchrole: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Switchrole'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getservices: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getservices'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Storecart: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Storecart'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getcartandservices: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getcartandservices'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Removefromcart: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Removefromcart'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Addtocart: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Addtocart'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getbankfields: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getbankfields'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getbankdetails: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getbankdetails'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getprojectextras: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getprojectextras'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Addprojectextra: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Addprojectextra',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Makeextrapayment: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Makeextrapayment',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Escrowpaymentdata: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Escrowpaymentdata',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Paymentinfodata: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Paymentinfodata',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            //Added By Ketan Solanki for Inspiration Bank item details
            InspirationBankinfo: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'InspirationBankinfo'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            MakeUserFavourite: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'MakeUserFavourite'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Deleteinspirationbankimage: function(data) {                 
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Deleteinspirationbankimage'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Editinspirationbank: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Editinspirationbank',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            DeleteinspirationBank: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'DeleteinspirationBank'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Returnfromcreditpayment: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Returnfromcreditpayment'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Createexpressinterestuser: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Createexpressinterestuser',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Showsellersonlandingpage: function() {
                return $http({
                    method: 'POST',
                    url: 'Showsellersonlandingpage',
              
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Myservicepageshown: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Myservicepageshown',
                    data:data
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Checkemailexist: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Checkemailexist',
                    data:data
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
        };
    }
]);
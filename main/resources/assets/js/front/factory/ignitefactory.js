/* 
 * Author: Nivedita Mitra
 * Date  : 20th Aug 2018
 * Ignite factory file frontend
 */
angular.module('sixcloudApp').factory('ignitefactory', ['$http', '$rootScope',
    function($http, $rootScope) {
        return {
            Getcountrycitypro: function(data) {
                return $http({
                    method: 'GET',
                    data: data,
                    url: 'Getcountrycitypro'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getcountry: function(data) {
                return $http({
                    method: 'GET',
                    data: data,
                    url: 'Getcountry'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getstatesforcountry: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getstatesforcountry'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getcitiesforstate: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getcitiesforstate'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Addteammember: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Addteammember',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Dosignup: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Dosignup',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getloginuserdatadistributor: function (data) {
                return $http({
                    method: 'GET',
                    data: data,
                    url: 'Getloginuserdatadistributor'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Logoutdistributor: function (data) {
                return $http({
                    method: 'GET',
                    data: data,
                    url: 'Logoutdistributor'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Createnewdesigner: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Createnewdesigner',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Dodistributorsignin: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Dodistributorsignin',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            forgotpassword: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'forgotpassword'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getlanguages: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getlanguages'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Checkemailexist: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Checkemailexist',
                    data:data
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getallteamdata: function(data,pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    url: 'Getallteamdata?page='+ pageNumber,
                    data:data
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            GetallReferalData: function(data,pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    url: 'GetallReferalData?page='+ pageNumber,
                    data:data
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Deleteteammember: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Deleteteammember'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Updatedistributorninfo: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Updatedistributorninfo',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Updatedistributorpassword: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Updatedistributorpassword'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            // Ignitesignupuser: function(data) {
            //     return $http({
            //         method: 'POST',
            //         data: data,
            //         url: 'Ignitesignupuser',
            //         transformRequest: angular.identity,
            //         headers: {
            //             'Content-Type': undefined
            //         }
            //     }).then(function successCallback(response) {
            //         return response;
            //     }, function errorCallback(response) {
            //         return response;
            //     });
            // },
            Ignitesignupuser: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Dosignup',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getallcontent: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getallcontent'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            DownloadWorksheet: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'DownloadWorksheet'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Contentdetail: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Contentdetail'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            CategoryExist: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'CategoryExist'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getquizdetail: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getquizdetail'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Storeattempt: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Storeattempt'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Endquiz: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Endquiz'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getquizresult: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Getquizresult'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            GetallPerformanceData: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'GetallPerformanceData'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getbuzzdata: function(data) {
                return $http({
                    method: 'GET',
                    data: data,
                    url: 'Getbuzzdata'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Ignitepayemnt: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Ignitepayemnt',
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Paymentinfodata: function (data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Paymentinfodata',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Mysubscriptiondata: function(data) {
                return $http({
                    method: 'GET',
                    data: data,
                    url: 'Mysubscriptiondata'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Videoseen: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'Videoseen'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            GetIgniteSubscriberDetailsData: function (data) {                
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'GetIgniteSubscriberDetailsData'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            GetSubscriberCurrentSubscription: function (data, pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'GetSubscriberCurrentSubscription?page=' + pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            GetSubscriberHistorySubscription: function (data, pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'GetSubscriberHistorySubscription?page=' + pageNumber
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            ExportSubscribers: function(data) {
                return $http({
                    method: 'GET',
                    data: data,
                    url: 'ExportSubscribers'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Teammemberdetail: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Teammemberdetail',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getteammembersubscribers: function(data, pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    url: 'Getteammembersubscribers?page=' + pageNumber,
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Sendotp: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Sendotp',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Senduserotp: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Senduserotp',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Verifyotp: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Verifyotp',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Verifyuserotp: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Verifyuserotp',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getcommissions: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Getcommissions',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Checkpromo: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Checkpromo',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Gettypeandyear: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Gettypeandyear',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getteamforcommissions: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Getteamforcommissions',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            GetSubscriberTransaction: function(data, pageNumber) {
                if (pageNumber === undefined) {
                    pageNumber = '1';
                }
                return $http({
                    method: 'POST',
                    url: 'GetSubscriberTransaction?page=' + pageNumber,
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Encryptdataforstripepayment: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Encryptdataforstripepayment',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Stripepayemnt: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Stripepayemnt',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getsubjects: function() {
                return $http({
                    method: 'GET',
                    url: 'Getsubjects'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getcategoriesforsubject: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Getcategoriesforsubject',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            getCategories: function() {
                return $http({
                    method: 'GET',
                    url: 'getCategories'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            getCategoriesForSubjectFromSubjectId: function(data) {
                return $http({
                    method: 'POST',
                    url: 'getCategoriesForSubjectFromSubjectId',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getsubcategories: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Getsubcategories',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getsubcategoriesforlandingpage: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Getsubcategoriesforlandingpage',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Activeamember: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Activeamember',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            selectLanguageForSubject: function(data) {
                return $http({
                    method: 'POST',
                    data: data,
                    url: 'selectLanguageForSubject'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            WantToPurchaseData: function(data) {
                return $http({
                    method: 'POST',
                    url: 'WantToPurchaseData',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Savecategoryandlanguage: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Savecategoryandlanguage',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getzones: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Getzones',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getzonelanguage: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Getzonelanguage',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getuserzone: function() {
                return $http({
                    method: 'GET',
                    url: 'Getuserzone',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            GetUserTokenForTwinkle: function() {
                return $http({
                    method: 'GET',
                    url: 'GetUserTokenForTwinkle',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            getCategoriesWithLanguages: function() {
                return $http({
                    method: 'GET',
                    url: 'getCategoriesWithLanguages',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getapkdownloadlink: function() {
                return $http({
                    method: 'GET',
                    url: 'Getapkdownloadlink',
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            GettaxrateDetails: function(data) {
                return $http({
                    method: 'GET',
                    data: data,
                    url: 'GettaxrateDetails'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            IgniteMysubscriptiondata: function(data) {
                return $http({
                    method: 'GET',
                    data: data,
                    url: 'IgniteMysubscriptiondata'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            IgniteHistorysubscriptiondata: function(data) {
                return $http({
                    method: 'GET',
                    data: data,
                    url: 'IgniteHistorysubscriptiondata'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            IgniteTransactionsnData: function(data) {
                return $http({
                    method: 'GET',
                    data: data,
                    url: 'IgniteTransactionsnData'
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            SwitchPlan: function(data) {
                return $http({
                    method: 'POST',
                    url: 'SwitchPlan',
                    data: data,
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            getSubjectCategory: function(data) {
                return $http({
                    method: 'POST',
                    url: 'getSubjectCategory',
                    data: data
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            CancelSubscription: function(data) {
                return $http({
                    method: 'POST',
                    url: 'CancelSubscription',
                    data: data
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Applypromocode: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Applypromocode',
                    data: data
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Removepromocode: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Removepromocode',
                    data: data
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            getPromocodedetails: function(data) {
                return $http({
                    method: 'POST',
                    url: 'getPromocodedetails',
                    data: data
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getcategorydetails: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Getcategorydetails',
                    data: data
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            SwitchhistoryDetails: function(data) {
                return $http({
                    method: 'POST',
                    url: 'SwitchhistoryDetails',
                    data: data
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Getpurchasecategorydetails: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Getpurchasecategorydetails',
                    data: data
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            GetsubscriptionplanHistory: function(data) {
                return $http({
                    method: 'POST',
                    url: 'GetsubscriptionplanHistory',
                    data: data
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
            Subscriptionpayemnt: function(data) {
                return $http({
                    method: 'POST',
                    url: 'Subscriptionpayemnt',
                    data: data
                }).then(function successCallback(response) {
                    return response;
                }, function errorCallback(response) {
                    return response;
                });
            },
        };
    }
]);

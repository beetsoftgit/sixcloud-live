// source --> https://www.sixclouds.net/beta/wp-content/plugins/js-support-ticket/includes/js/common.js?ver=5.7.2 
jQuery(document).ready(function(n){
    jQuery('.specialClass').closest("div.js-form-custm-flds-wrp").removeClass('visible');
    jQuery('.specialClass').closest("div.js-ticket-from-field-wrp").removeClass('visible');
});

function fillSpaces(string){
	string = string.replace(" ", "%20");
	return string;
}

function getDataForDepandantField(parentf, childf, type) {
    if (type == 1) {
        var val = jQuery("select#" + parentf).val();
    } else if (type == 2) {
        var val = jQuery("input[name=\'" + parentf + "\']:checked").val();
    }

    jQuery.post(ajaxurl, {action: 'jsticket_ajax', jstmod: 'fieldordering', task: 'DataForDepandantField', fvalue: val, child: childf}, function (data) {
        if (data) {
            var d = jQuery.parseJSON(data);
            jQuery("select#" + childf).replaceWith(d);
        }
    });
}

function getDataForVisibleField(val, parentf, childf, pvalue, cond) {
    var type = jQuery('[name="'+childf+'"]').attr("type");
    if (type == 'text' ||type == 'email' || type == 'password' || type == 'file') {
        jQuery('[name="'+childf+'"]').val('');
    }
    else if (type == 'checkbox') {
        jQuery('[name="'+childf+'[]"]').removeAttr('checked');
        jQuery('[name="'+childf+'"]').removeAttr('checked');
    }
    else if (type == 'radio') {
        jQuery('[name="'+childf+'"]').prop('checked', false);
    }
    else if (jQuery('[name="'+childf+'"]').hasClass("js-ticket-custom-textarea")) {
        jQuery('[name="'+childf+'"]').val("");
    }
    else if (jQuery('[name="'+childf+'"]').hasClass("js-ticket-custom-select")) {
        jQuery('[name="'+childf+'"]').prop('selectedIndex', 0);
    }
    else{
        type = "checkboxOrMultiple"
        if (jQuery('[name="'+childf+'[]"]').attr("multiple")) {
            jQuery('[name="'+childf+'[]"]').children().prop('selected', false);
            jQuery('[name="'+childf+'[]"]').prop('selectedIndex', 0);
        } else {
            jQuery('[name="'+childf+'[]"]').removeAttr('checked');
        }
    }
    if (val.length != 0){
        if (type == 'checkboxOrMultiple') {
            if (cond == 1) {
                if (pvalue == val) {
                    jQuery('[name="'+childf+'[]"]').closest("div.js-form-custm-flds-wrp").removeClass('visible');
                    jQuery('[name="'+childf+'[]"]').closest("div.js-ticket-from-field-wrp").removeClass('visible');
                }else{
                    jQuery('[name="'+childf+'[]"]').closest("div.js-form-custm-flds-wrp").addClass('visible');
                    jQuery('[name="'+childf+'[]"]').closest("div.js-ticket-from-field-wrp").addClass('visible');
                }
            }else if (cond ==0) {
                if (pvalue != val) {
                    jQuery('[name="'+childf+'[]"]').closest("div.js-form-custm-flds-wrp").removeClass('visible');
                    jQuery('[name="'+childf+'[]"]').closest("div.js-ticket-from-field-wrp").removeClass('visible');
                }else{
                    jQuery('[name="'+childf+'[]"]').closest("div.js-form-custm-flds-wrp").addClass('visible');
                    jQuery('[name="'+childf+'[]"]').closest("div.js-ticket-from-field-wrp").addClass('visible');
                }
            }
        }else{
            if (cond == 1) {
                if (pvalue == val) {
                    jQuery('[name="'+childf+'"]').closest("div.js-form-custm-flds-wrp").removeClass('visible');
                    jQuery('[name="'+childf+'"]').closest("div.js-ticket-from-field-wrp").removeClass('visible');
                }else{
                    jQuery('[name="'+childf+'"]').closest("div.js-form-custm-flds-wrp").addClass('visible');
                    jQuery('[name="'+childf+'"]').closest("div.js-ticket-from-field-wrp").addClass('visible');
                }
            }else if (cond ==0) {
                if (pvalue != val) {
                    jQuery('[name="'+childf+'"]').closest("div.js-form-custm-flds-wrp").removeClass('visible');
                    jQuery('[name="'+childf+'"]').closest("div.js-ticket-from-field-wrp").removeClass('visible');
                }else{
                    jQuery('[name="'+childf+'"]').closest("div.js-form-custm-flds-wrp").addClass('visible');
                    jQuery('[name="'+childf+'"]').closest("div.js-ticket-from-field-wrp").addClass('visible');
                }
            }
        }
    }
    else{
        if (type == 'checkboxOrMultiple') {
            jQuery('[name="'+childf+'[]"]').closest("div.js-form-custm-flds-wrp").addClass('visible');
            jQuery('[name="'+childf+'[]"]').closest("div.js-ticket-from-field-wrp").addClass('visible');
        } else {
            jQuery('[name="'+childf+'"]').closest("div.js-form-custm-flds-wrp").addClass('visible');
            jQuery('[name="'+childf+'"]').closest("div.js-ticket-from-field-wrp").addClass('visible');
        }
    }
}

function deleteCutomUploadedFile (field1) {
    jQuery("input#"+field1).val(1);
    jQuery("span."+field1).hide();
    
};
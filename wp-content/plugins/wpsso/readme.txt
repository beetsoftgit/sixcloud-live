=== WPSSO Core | Open Graph, Google Rich Results, Pinterest Rich Pins, Schema JSON-LD, Twitter Cards, WooCommerce SEO and More ===
Plugin Name: WPSSO Core
Plugin Slug: wpsso
Text Domain: wpsso
Domain Path: /languages
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl.txt
Assets URI: https://surniaulula.github.io/wpsso/assets/
Tags: woocommerce, open graph, meta tags, schema, rich results, seo, image seo, video seo, google search, knowledge graph, xml sitemap
Contributors: jsmoriss
Requires PHP: 7.0
Requires At Least: 5.0
Tested Up To: 5.8.1
WC Tested Up To: 5.8.0
Stable Tag: 9.1.1

Advanced WordPress SEO structured data plugin to present your content at its best on social sites and in search results.

== Description ==

<blockquote class="top">
	<p><strong>The <a href="https://surniaulula.com/2021/apps/wordpress/plugins/wordpress-seo-plugin-performance-report-for-q4-2021/">SEO Plugin Performance Report for Q4 2021</a> ranks all the popular plugins for 2021 and 2022 - see where the WPSSO Core plugin ranks compared with all other popular WordPress SEO plugins!</strong></p>
</blockquote>

<h3>The Most Advanced WordPress SEO Structured Data Plugin</h3>

<p><img class="readme-icon" src="https://surniaulula.github.io/wpsso/assets/icon-256x256.png"> WPSSO helps you rank higher and improves click through rates by presenting your content at its best on <strong>social sites</strong> and in <strong>search results</strong> - no matter how URLs are shared, reshared, messaged, posted, embedded, or crawled.</p>

<p><strong>Provides structured data markup for:</strong></p>

<ul>
	<li>Facebook / Open Graph</li>
	<li>Google Knowledge Graph</li>
	<li>Google Rich Results (aka Rich Snippets)</li>
	<li>LinkedIn / oEmbed Data</li>
	<li>Mobile Web Browsers</li>
	<li>Pinterest Rich Pins</li>
	<li>Twitter Cards</li>
	<li>Schema.org Markup (for 500+ Schema Types)</li>
	<li>Slack</li>
	<li>WhatsApp and Messaging Apps</li>
	<li>WordPress REST API</li>
	<li>WordPress XML Sitemaps</li>
	<li><strong>&#91;Premium&#93;</strong> WooCommerce SEO and More!</li>
</ul>

<p><strong>Uses WordPress, plugin, and remote API data:</strong></p>

There's no need to select or create templates, manually reenter descriptions, titles, product information, or reselect images and videos.

WPSSO can use your existing WordPress content, including custom post types and taxonomies, enhanced user profile data, Media Library image and video information, third-party plugin data, and remote service APIs (Bitly, Facebook, Shopper Approved, Stamped.io, Vimeo, Wistia, YouTube, and many more).

<p><strong>Supports over 500 different Schema types:</strong></p>

WPSSO provides comprehensive Schema markup for posts, pages, custom post types, terms (category, tags, etc.), custom taxonomies, user profile pages, search result pages, archive pages, and Accelerated Mobile Pages (AMP) pages - including image SEO, video SEO, local business, organization, publisher, person, author and co-authors, extensive e-Commerce product markup, product variations, product ratings, aggregate ratings, reviews, recipe information, event details, collection pages, profile pages, search pages, FAQ pages, item lists for Google's Rich Results Carousel, and much more.

<p><strong>Fixes Google Search and Schema Markup Validator errors, including:</strong></p>

* A value for the headline field is required.
* A value for the image field is required.
* A value for the logo field is required.
* A value for the publisher field is required.
* The aggregateRating field is recommended.
* The brand field is recommended.
* The headline field is recommended.
* The image field is recommended.
* The mainEntityOfPage field is recommended.
* The review field is recommended.
* This Product is missing a global identifier (e.g. isbn, mpn or gtin8).
* No global identifier provided (e.g. gtin mpn isbn).
* Not a known valid target type for the itemReviewed property.

<p><strong>Use WPSSO by itself or extend another SEO plugin:</strong></p>

WPSSO can be your only social and search optimization plugin, or can improve the structured data of another third-party SEO plugin (like All in One SEO Pack, Jetpack SEO Tools, Rank Math SEO, SEO Ultimate, SEOPress, The SEO Framework, WP Meta SEO, Yoast SEO, and more).

<h3>Users Love the WPSSO Core Plugin</h3>

&#x2605;&#x2605;&#x2605;&#x2605;&#x2605; - "Unlike competitors, you can literally customize just about every aspect of SEO and Social SEO if you desire to. &#91;...&#93; This plugin has the most complete JSON-LD markup out of any of them, so you won’t have any errors and warnings in search console for WordPress or WooCommerce sites. You can go crazy customizing everything, or you can just set and forget. There aren’t many plugins that allow the best of both worlds." - [kw11](https://wordpress.org/support/topic/most-responsive-developer-ive-ever-seen/)

&#x2605;&#x2605;&#x2605;&#x2605;&#x2605; - "This plugin makes getting sites structured data ready extremely easy, and it works flawlessly without any issues. It shows messages on the top bar every step of the way to alert you of any issues until everything is set up correctly. It made all my ecommerce products pass Google's validation tests. Great work." - [marguy1](https://wordpress.org/support/topic/excellent-plugin-6825/)

&#x2605;&#x2605;&#x2605;&#x2605;&#x2605; - "What a fantastic plugin. If you want to fix all the errors in search console for structured data, this is the plugin to use. Love it." - [goviral](https://wordpress.org/support/topic/makes-schema-so-easy/)

&#x2605;&#x2605;&#x2605;&#x2605;&#x2605; - "This plugin saves me so much time, and it has really lifted my SERP rankings. Most of my keywords I now rank 1-3 position. I also noticed after about a week that my impressions have gone up at least 75%. I upgraded to the pro version which gave me even more options." - [playnstocks](https://wordpress.org/support/topic/excellent-plugin-and-support-200/)

<h3>WPSSO Core Plugin Features</h3>

Provides accurate and comprehensive meta tags and Schema markup for WordPress posts / pages, custom post types, taxonomies, search results, shop pages, and much more.

Enhances the WordPress oEmbed data for LinkedIn, Discord, Drupal, Squarespace, and others.

Offers optimized image sizes for social sites and search engines:

* Open Graph (Facebook and oEmbed)
* Pinterest Pin It
* Schema 1:1 (Google)
* Schema 4:3 (Google)
* Schema 16:9 (Google)
* Schema Thumbnail
* Twitter Summary Card
* Twitter Large Image Summary Card

Provides Schema ImageObject markup with image data from the WordPress Media Library (name, alternateName, alternativeHeadline, caption, description, fileFormat, uploadDate, and more).

Provides Schema VideoObject markup with video data from WPSSO Core Premium service APIs (Facebook, Slideshare, Soundcloud, Vimeo, Wistia, YouTube) including the 'embedUrl' and 'contentUrl' properties for Google.

Built-in compatibility with AMP plugins:

* [AMP](https://wordpress.org/plugins/amp/)
* [AMP for WP](https://wordpress.org/plugins/accelerated-mobile-pages/)

Built-in compatibility with caching and optimization plugins:

* Autoptimize
* Cache Enabler
* Comet Cache
* Hummingbird Cache
* LiteSpeed Cache
* Pagely Cache
* Perfect Images + Retina
* SiteGround Cache
* W3 Total Cache (aka W3TC)
* WP Engine Cache
* WP Fastest Cache
* WP Rocket Cache
* WP Super Cache

Built-in compatibility with popular SEO plugins:

* All in One SEO Pack
* Jetpack SEO
* Rank Math SEO
* SEO Ultimate
* SEOPress
* Slim SEO
* Squirrly SEO
* The SEO Framework
* WP Meta SEO
* Yoast SEO
* Yoast WooCommerce SEO

Built-in compatibility for advanced WordPress configurations:

* WordPress MU Domain Mapping
* Network / Multisite Installations

The WPSSO Core Standard plugin is designed to satisfy the requirements of most standard WordPress sites. If your site requires additional third-party plugin and service API integration, like WooCommerce shops, embedded video support, or advanced customization features, then you may want to get the [WPSSO Core Premium plugin](https://wpsso.com/extend/plugins/wpsso/) for those additional features.

**&#91;Premium&#93;** Support for the Twitter [Player Card](https://dev.twitter.com/cards/types/player) for embedded videos.

**&#91;Premium&#93;** Detection of embedded videos in content text with API support for Facebook, Slideshare, Vimeo, Wistia, and Youtube videos.

**&#91;Premium&#93;** Upscaling of smaller images to satisfy minimum size requirements for social sites and Google Rich Results (aka aka Rich Snippets).

**&#91;Premium&#93;** URL shortening with Bitly, DLMY.App, Google, Ow.ly, TinyURL, or YOURLS.

**&#91;Premium&#93;** Customize default Open Graph and Schema types for posts, pages, custom post types, taxonomy terms, and user profile pages.

**&#91;Premium&#93;** Customize post and taxonomy types included in the WordPress sitemap XML.

**&#91;Premium&#93;** Complete Schema JSON-LD markup for WooCommerce SEO:

> The WooCommerce plugin does not provide sufficient Schema JSON-LD markup for Google Rich Results.

> The WPSSO Core Premium plugin reads WooCommerce product data and provides complete Schema Product JSON-LD markup for Google Rich Results, including additional product images, product variations, product information (brand, color, condition, EAN, dimensions, GTIN-8/12/13/14, ISBN, material, MPN, size, SKU, volume, weight, etc), product reviews, product ratings, sale start / end dates, sale prices, pre-tax prices, VAT prices, shipping rates, shipping times, and much, much more.

**&#91;Premium&#93;** Customizable Schema options in the Document SSO metabox:

* All Schema Types
	* Name / Title
	* Alternate Name
	* Description
	* Microdata Type URLs
	* Same-As URLs
* Creative Work Information
	* Is Part of URL
	* Headline
	* Full Text
	* Keywords
	* Language
	* Family Friendly
	* Copyright Year
	* License URL
	* Publisher Org.
	* Publisher Person
	* Service Provider Org.
	* Service Provider Person
* Event Information
	* Event Language
	* Event Attendance
	* Event Online URL
	* Event Physical Venue
	* Event Organizer Org.
	* Event Organizer Person
	* Event Performer Org.
	* Event Performer Person
	* Event Start (date, time, timezone)
	* Event End (date, time, timezone)
	* Event Offers Start (date, time, timezone)
	* Event Offers End (date, time, timezone)
	* Event Offers (name, price, currency, availability)
* How-To
	* How-To Makes 
	* How-To Preparation Time 
	* How-To Total Time 
	* How-To Supplies 
	* How-To Tools 
	* How-To Steps (section name, section description, step name, direction text and image)
* Job Posting Information
	* Job Title
	* Hiring Organization
	* Job Location
	* Job Location Type
	* Base Salary
	* Employment Type
	* Jpb Posting Expires
* Movie Information
	* Cast Names
	* Director Names
	* Production Company
	* Movie Runtime
* Organization Information
	* Select an Organization
* Person Information
	* Select a Person
* Product Information
	* Product Availability
	* Product Brand
	* Product Color
	* Product Condition
	* Product Depth
	* Product Fluid Volume
	* Product GTIN-14
	* Product GTIN-13 (EAN)
	* Product GTIN-12 (UPC)
	* Product GTIN-8
	* Product GTIN
	* Product ISBN
	* Product Length
	* Product Material
	* Product MPN
	* Product Price
	* Product Size
	* Product SKU
	* Product Target Gender
	* Product Type
	* Product Weight
	* Product Width
* QA Page Information
	* QA Heading
* Recipe Information
	* Recipe Cuisine 
	* Recipe Course 
	* Recipe Makes 
	* Cooking Method 
	* Preparation Time 
	* Cooking Time 
	* Total Time 
	* Recipe Ingredients 
	* Recipe Instructions 
	* Nutrition Information per Serving 
		* Serving Size
		* Calories
		* Protein
		* Fiber
		* Carbohydrates
		* Sugar
		* Sodium
		* Fat
		* Saturated Fat
		* Unsaturated Fat
		* Trans Fat
		* Cholesterol
* Review Information
	* Review Rating 
	* Rating Value Name
	* Subject of the Review
		* Subject Webpage Type 
		* Subject Webpage URL 
		* Subject Same-As URL 
		* Subject Name 
		* Subject Description 
		* Subject Image ID or URL 
		* Claim Subject Information (for Claim Review)
			* Short Summary of Claim
			* First Appearance URL
		* Creative Work Subject Information
			* C.W. Author Type
			* C.W. Author Name
			* C.W. Author URL
			* C.W. Published Date
			* C.W. Created Date
		* Book Subject Information
			* Book ISBN
		* Movie Subject Information
			* Movie Cast Names
			* Movie Director Names
		* Product Subject Information
			* Product Brand
			* Product Offers (name, price, currency, availability)
			* Product SKU
			* Product MPN
		* Software App Subject Information
			* Operating System
			* Application Category
			* Software App Offers (name, price, currency, availability)
* Software Application Information
	* Operating System
	* Application Category

**&#91;Premium&#93;** Integrates with other plugins and service APIs for additional image, video, e-Commerce product details, SEO settings, etc. The following modules are included with the Premium version and automatically loaded if/when the supported plugins and/or services are required.

* **Reads data from 30 third-party plugins:** 

	* All in One SEO Pack
	* bbPress
	* BuddyPress
	* Co-Authors Plus
	* Easy Digital Downloads
	* Gravity Forms + GravityView
	* NextCellent Gallery - NextGEN Legacy
	* NextGEN Gallery
	* Perfect WooCommerce Brands
	* Polylang
	* Product GTIN (EAN, UPC, ISBN) for WooCommerce
	* Rate my Post
	* SEOPress
	* Simple Job Board
	* The Events Calendar
	* The SEO Framework
	* WooCommerce
	* WooCommerce Brands
	* WooCommerce Currency Switcher
	* WooCommerce UPC, EAN, and ISBN
	* WooCommerce Show Single Variations
	* WP Job Manager
	* WP Meta SEO
	* WP-PostRatings
	* WP Product Review
	* WP Recipe Maker
	* WPML
	* YITH WooCommerce Brands Add-on
	* Yoast SEO
	* Yotpo Social Reviews for WooCommerce

* **Reads data from 15 remote service APIs:**

	* Bitly
	* DLMY.App
	* Facebook Embedded Videos
	* Gravatar (Author Image)
	* Ow.ly
	* Shopper Approved (Ratings and Reviews)
	* Slideshare Presentations
	* Soundcloud Tracks (for the Twitter Player Card)
	* Stamped.io (Ratings and Reviews)
	* TinyURL
	* Vimeo Videos
	* Wistia Videos
	* WordPress Video Shortcode (and Self-Hosted Videos)
	* Your Own URL Shortener (YOURLS)
	* YouTube Videos and Playlists

<h3>Free Complementary Add-ons</h3>

Do you need even more advanced features? Activate any of the free complementary add-on(s) you require:

* [WPSSO FAQ Manager](https://wordpress.org/plugins/wpsso-faq/) to manage FAQ categories with Question and Answer pages.
* [WPSSO Inherit Parent Metadata](https://wordpress.org/plugins/wpsso-inherit-parent-meta/) to inherit featured and custom images.
* [WPSSO Mobile App Meta Tags](https://wordpress.org/plugins/wpsso-am/) to manage mobile App information.
* [WPSSO Organization Markup](https://wordpress.org/plugins/wpsso-organization/) to manage multiple organizations.
* [WPSSO Place and Local SEO Markup](https://wordpress.org/plugins/wpsso-plm/) to manage multiple places and locations.
* [WPSSO Product Metadata for WooCommerce SEO](https://wordpress.org/plugins/wpsso-wc-metadata/) to add GTIN, GTIN-8, GTIN-12 (UPC), GTIN-13 (EAN), GTIN-14, ISBN, MPN, depth, and volume for WooCommerce products and variations.
* [WPSSO Ratings and Reviews](https://wordpress.org/plugins/wpsso-ratings-and-reviews/) to add ratings in WordPress comments.
* [WPSSO REST API](https://wordpress.org/plugins/wpsso-rest-api/) to add meta tags and Schema markup in REST API queries.
* [WPSSO Ridiculously Responsive Social Sharing Buttons](https://wordpress.org/plugins/wpsso-rrssb/) to add responsive social sharing buttons.
* [WPSSO Schema Breadcrumbs Markup](https://wordpress.org/plugins/wpsso-breadcrumbs/) to add Breadcrumbs markup for Google.
* [WPSSO Shipping Delivery Time for WooCommerce SEO](https://wordpress.org/plugins/wpsso-wc-shipping-delivery-time/) to provide Google with shipping rates and delivery time estimates for WooCommerce products and variations.
* [WPSSO Strip Schema Microdata](https://wordpress.org/plugins/wpsso-strip-schema-microdata/) to strip incorrect markup from templates.
* [WPSSO Tune WP Image Editors](https://wordpress.org/plugins/wpsso-tune-image-editors/) for better looking WordPress thumbnail images.
* [WPSSO User Locale Selector](https://wordpress.org/plugins/wpsso-user-locale/) to switch languages quickly and easily.

== Installation ==

<h3 class="top">Install and Uninstall</h3>

* [Install the WPSSO Core Plugin](https://wpsso.com/docs/plugins/wpsso/installation/install-the-plugin/)
* [Uninstall the WPSSO Core Plugin](https://wpsso.com/docs/plugins/wpsso/installation/uninstall-the-plugin/)

<h3>Plugin Setup</h3>

* [Setup Guide](https://wpsso.com/docs/plugins/wpsso/installation/setup-guide/)
* [Much Better Schema Markup for WooCommerce SEO](https://wpsso.com/docs/plugins/wpsso/installation/better-schema-for-woocommerce/)
* [Integration Notes](https://wpsso.com/docs/plugins/wpsso/installation/integration/)
	* [BuddyPress Integration Notes](https://wpsso.com/docs/plugins/wpsso/installation/integration/buddypress-integration/)
	* [WooCommerce Integration Notes](https://wpsso.com/docs/plugins/wpsso/installation/integration/woocommerce-integration/)
* [Troubleshooting Guide](https://wpsso.com/docs/plugins/wpsso/installation/troubleshooting-guide/)
* [Developer Special: Buy one, Get one Free](https://wpsso.com/docs/plugins/wpsso/installation/developer-special-buy-one-get-one-free/)

== Frequently Asked Questions ==

<h3 class="top">Frequently Asked Questions</h3>

* [Does LinkedIn read Facebook / Open Graph meta tags?](https://wpsso.com/docs/plugins/wpsso/faqs/does-linkedin-read-the-open-graph-meta-tags/)
* [How can I fix a ERR_TOO_MANY_REDIRECTS error?](https://wpsso.com/docs/plugins/wpsso/faqs/how-can-i-fix-a-err_too_many_redirects-error/)
* [How can I fix a generic HTTP 500 error?](https://wpsso.com/docs/plugins/wpsso/faqs/how-can-i-fix-a-generic-http-500-error/)
* [How can I fix a PHP fatal "out of memory" error?](https://wpsso.com/docs/plugins/wpsso/faqs/how-can-i-fix-a-php-fatal-out-of-memory-error/)
* [How can I fix an HTTP error when uploading images?](https://wpsso.com/docs/plugins/wpsso/faqs/how-can-i-fix-an-http-error-when-uploading-images/)
* [How can I have smaller dimensions for the default image?](https://wpsso.com/docs/plugins/wpsso/faqs/how-can-i-have-smaller-dimensions-for-the-default-image/)
* [How can I see what the Facebook crawler sees?](https://wpsso.com/docs/plugins/wpsso/faqs/how-can-i-see-what-the-facebook-crawler-sees/)
* [How do I create a Schema FAQPage?](https://wpsso.com/docs/plugins/wpsso/faqs/how-do-i-create-a-schema-faqpage/)
* [How do I enable WordPress WP_DEBUG?](https://wpsso.com/docs/plugins/wpsso/faqs/how-do-i-enable-wordpress-wp_debug/)
* [How do I fix Google Structured Data &gt; hatom errors?](https://wpsso.com/docs/plugins/wpsso/faqs/how-do-i-fix-google-structured-data-hatom-errors/)
* [How do I remove duplicate meta tags?](https://wpsso.com/docs/plugins/wpsso/faqs/how-do-i-remove-duplicate-meta-tags/)
* [How does WPSSO Core find and select images?](https://wpsso.com/docs/plugins/wpsso/faqs/how-does-wpsso-find-detect-select-images/)
* [How does WPSSO Core find and select videos?](https://wpsso.com/docs/plugins/wpsso/faqs/how-does-wpsso-find-detect-select-videos/)
* [W3C says "there is no attribute 'property'"](https://wpsso.com/docs/plugins/wpsso/faqs/w3c-says-there-is-no-attribute-property/)
* [Why are some HTML elements missing or misaligned?](https://wpsso.com/docs/plugins/wpsso/faqs/why-are-some-html-elements-missing-misaligned-different/)
* [Why does Facebook show the wrong image / text?](https://wpsso.com/docs/plugins/wpsso/faqs/why-does-facebook-show-the-wrong-image-text/)
* [Why does the Schema Markup Validator show errors?](https://wpsso.com/docs/plugins/wpsso/faqs/why-does-google-structured-data-testing-tool-show-errors/)
* [Why shouldn't I upload small images to the media library?](https://wpsso.com/docs/plugins/wpsso/faqs/why-shouldnt-i-upload-small-images-to-the-media-library/)

<h3>Notes and Documentation</h3>

* [Developer Resources](https://wpsso.com/docs/plugins/wpsso/notes/developer/)
	* [Constants](https://wpsso.com/docs/plugins/wpsso/notes/developer/constants/)
	* [Filters](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/)
		* [All Filters](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/all/)
		* [Filter Examples](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/)
			* [Detect YouTube URL Links as Videos](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/detect-youtube-url-links-as-videos/)
			* [Exclude Schema Markup by Post Type](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/exclude-schema-markup-by-post-type/)
			* [Fix 'hentry' Errors in your Theme Templates](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/fix-hentry-errors-in-your-theme-templates/)
			* [Modify the aggregateRating Property](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/modify-the-aggregaterating-property/)
			* [Modify the Default Article Section List](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/modify-the-default-article-sections-list/)
			* [Set a Custom Field Value to a Schema Property](https://wpsso.com/docs/plugins/wpsso-schema-json-ld/notes/developer/filters/examples/set-a-custom-field-value-to-a-schema-property/)
	* [The $mod Variable](https://wpsso.com/docs/plugins/wpsso/notes/developer/the-mod-variable/)
* [Inline Variables](https://wpsso.com/docs/plugins/wpsso/notes/inline-variables/)
* [Multisite / Network Support](https://wpsso.com/docs/plugins/wpsso/notes/multisite-network-support/)

== Screenshots ==

01. The Essential and General Settings pages provide a simple, fast, and easy setup.
02. The Document SSO metabox allows you to easily customize the details of articles, events, products, recipes, reviews, and much more.
03. The Preview and oEmbed tabs allow you to quickly preview an example social share.

== Changelog ==

<h3 class="top">Release Schedule</h3>

<p>New versions of the plugin are released approximately every week (more or less). New features are added, tested, and released incrementally, instead of grouping them together in a major version release. When minor bugs fixes and/or code improvements are applied, new versions are also released. This release schedule keeps the code stable and reliable, at the cost of more frequent updates.</p>

<p>See <a href="https://en.wikipedia.org/wiki/Release_early,_release_often">release early, release often (RERO) software development philosophy</a> on Wikipedia for more information on the benefits of smaller / more frequent releases.</p>

<h3>Version Numbering</h3>

Version components: `{major}.{minor}.{bugfix}[-{stage}.{level}]`

* {major} = Major structural code changes / re-writes or incompatible API changes.
* {minor} = New functionality was added or improved in a backwards-compatible manner.
* {bugfix} = Backwards-compatible bug fixes or small improvements.
* {stage}.{level} = Pre-production release: dev &lt; a (alpha) &lt; b (beta) &lt; rc (release candidate).

<h3>Standard Version Repositories</h3>

* [GitHub](https://surniaulula.github.io/wpsso/)
* [WordPress.org](https://plugins.trac.wordpress.org/browser/wpsso/)

<h3>Development Version Updates</h3>

<p><strong>WPSSO Core Premium customers have access to development, alpha, beta, and release candidate version updates:</strong></p>

<p>Under the SSO &gt; Update Manager settings page, select the "Development and Up" (for example) version filter for the WPSSO Core plugin and/or its add-ons. Save the plugin settings and click the "Check for Plugin Updates" button to fetch the latest version information. When new development versions are available, they will automatically appear under your WordPress Dashboard &gt; Updates page. You can always reselect the "Stable / Production" version filter at any time to reinstall the latest stable version.</p>

<h3>Changelog / Release Notes</h3>

**Version 9.1.2-dev.2 (2021/10/13)**

* **New Features**
	* None.
* **Improvements**
	* Added a compatibility filter for Yoast WooCommerce SEO presenters.
* **Bugfixes**
	* None.
* **Developer Notes**
	* None.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v5.0.

**Version 9.1.1 (2021/10/11)**

* **New Features**
	* None.
* **Improvements**
	* None.
* **Bugfixes**
	* Fixed unnecessary loading of the 'sucom-block-editor-admin' script in the WordPress widgets editing page.
* **Developer Notes**
	* None.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v5.0.

**Version 9.1.0 (2021/10/06)**

* **New Features**
	* None.
* **Improvements**
	* Added Schema Event, Place, and Product 'subjectOf' property for videos.
* **Bugfixes**
	* Fixed the Document SSO &gt; Publisher Org. option value.
	* Fixed the Advanced Settings &gt; Default Publisher Org. option value.
* **Developer Notes**
	* Standardized `get_table_rows()` calls and filters in 'submenu' and 'sitesubmenu' classes.
	* Refactored the `WpssoProMediaGravatar->filter_user_image_urls()` method.
	* Removed the unused `WpssoWpMeta->get_table_rows()` method.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v5.0.

**Version 9.0.1 (2021/09/30)**

* **New Features**
	* None.
* **Improvements**
	* Updated the SSO &gt; Setup Guide text.
	* Updated information text in the Document SSO metabox.
* **Bugfixes**
	* None.
* **Developer Notes**
	* Optimized the metabox container update process in the block editor.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v5.0.

**Version 9.0.0 (2021/09/24)**

* **New Features**
	* Discontinued / deprecated the WPSSO JSON add-on:
		* The <code>&#91;schema&#93;</code> shortcode was migrated to a new WPSSO Schema Shortcode add-on.
		* All other features of the WPSSO Schema JSON-LD Markup add-on were integrated into the WPSSO Core v9.0.0 plugin.
	* Updated the SSO &gt; Advanced Settings &gt; Integration &gt; Enable Tags for Pages feature to register a non-public Page Tags taxonomy.
* **Improvements**
	* Added a Document SSO &gt; Customize &gt; Twitter Card Title option.
	* Refactored the Rank Math integration module to read Google, Facebook, and Twitter metadata (Premium version).
	* Deprecated the Schema link and meta itemprop tags.
	* Deprecated the head attributes check in theme header templates.
	* Removed the SSO &gt; Advanced Settings &gt; HTML Tags &gt; Schema tab.
	* Moved the Schema data filters from the WPSSO JSON add-on to the WPSSO Core plugin.
	* Moved the Document SSO &gt; Schema JSON-LD Markup / Google Rich Results sections from the WPSSO JSON add-on to the WPSSO Core plugin.
* **Bugfixes**
	* Fixed caching of option defaults array once plugin objects have been initialized.
	* Fixed missing 'areaServed' property in Schema LocalBusiness markup for places / locations.
	* Fixed the saving of selected option colors using `wpColorPicker()`.
	* Fixed an undefined `$mt_ret` variable error in lib/util.php.
	* Fixed reading of transient cache in `WpssoSchema->get_schema_types_array()`.
	* Fixed saving of the "Primary Category" option value when the default category ID is 1.
* **Developer Notes**
	* Added a new 'wpsso_{post_type}_tag_taxonomy' filter.
	* Added a new `WpssoOptionsFilters` class.
	* Added a new `WpssoConfig::get_social_accounts()` method.
	* Removed the `WpssoMetaItem` class.
	* Removed the `WpssoSchemaNoScript` class.
	* Removed the 'wpsso_add_schema_head_attributes' filter.
	* Removed the 'wpsso_add_schema_meta_array' filter.
	* Removed the 'wpsso_add_schema_noscript_aggregaterating' filter.
	* Removed the 'wpsso_add_schema_noscript_array' filter.
	* Removed the 'wpsso_schema_meta_itemprop' filter.
	* Removed the `WPSSO_HEAD_ATTR_FILTER_NAME` constant.
	* Removed the `WPSSO_HEAD_ATTR_FILTER_PRIO` constant.
	* Deprecated the `WpssoAdmin->check_tmpl_head_attributes()` method.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v5.0.

== Upgrade Notice ==

= 9.1.2-dev.2 =

(2021/10/13) Added a compatibility filter for Yoast WooCommerce SEO presenters.

= 9.1.1 =

(2021/10/11) Fixed unnecessary loading of the 'sucom-block-editor-admin' script in the WordPress widgets editing page.

= 9.1.0 =

(2021/10/06) Fixed the Document SSO &gt; Publisher Org. and the Advanced Settings &gt; Default Publisher Org. option value. Added Schema Event, Place, and Product 'subjectOf' property for videos.

= 9.0.1 =

(2021/09/30) Updated the SSO &gt; Setup Guide text. Updated information text in the Document SSO metabox.

= 9.0.0 =

(2021/09/24) Added a Document SSO &gt; Twitter Card Title option. Refactored the Rank Math integration module (Premium version). Moved Schema data filters from WPSSO JSON to WPSSO Core.


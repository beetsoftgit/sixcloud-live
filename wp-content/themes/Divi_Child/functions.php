<?php

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );}
	

//======================================================================
// CUSTOM DASHBOARD
//======================================================================
// ADMIN FOOTER TEXT
add_action( 'wpcf7_before_send_mail', 'sc_modify_token_number', 90, 1 );
    
function sc_modify_token_number( $WPCF7_ContactForm ){
	global $wpdb;
	$data = array();
	$submission = WPCF7_Submission :: get_instance();
	$tablename=$wpdb->prefix.'token_number';
	$mail = $WPCF7_ContactForm->prop( 'mail' );

	if ( strpos($mail['body'],'[token-number]') && strpos($mail['subject'],'[token-number]') ){
		
		$posted_data = $submission->get_posted_data();      
		$str = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $tablename ORDER BY submission_date DESC" ) );
		$str = $str->token_number;
		$domain = end(explode(".", parse_url(home_url(), PHP_URL_HOST)));
		
		if($str == ''){
			$num=sprintf("%04d", 1);
			$char='A';
			$domain_prefix = $domain == 'net' ? 'CSN' : 'CSC';
			$str= $domain_prefix.date('y').date('m').$num.$char;
		}
		else{
			$num=substr( $str, -5,-1 );
			$num=$num+1;
			$num=sprintf("%04d", $num);
			$char=substr( $str, -1 );
			if($num>9999){
				$num=sprintf("%04d", 1);
				$char++;
			}
			$domain_prefix = $domain == 'net' ? 'CSN' : 'CSC';
			$str= $domain_prefix.date('y').date('m').$num.$char;
		}

		$data = array(
			'token_number'    => $str,
			'email_address'   => $_POST['Email'],
			'submission_date' => date("Y-m-d H:i:s")
		);
	
		$wpdb->insert( $tablename, $data );

		$new_mail = str_replace( '[token-number]', $str, $mail );

		$WPCF7_ContactForm->set_properties( array( 'mail' => $new_mail ) );
		
		return $WPCF7_ContactForm;
	}
}
add_action("wp_head", "sc_create_token_number_table");

function sc_create_token_number_table(){

	global $wpdb;

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	
    $table_name = $wpdb->prefix . "token_number";
	
    $sql = "CREATE TABLE $table_name (
      token_id int(10) unsigned NOT NULL AUTO_INCREMENT,
      token_number varchar(255) NOT NULL,
      email_address varchar(255) NOT NULL,
	  submission_date DATETIME,
      PRIMARY KEY ( token_id )
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

    dbDelta( $sql );
}